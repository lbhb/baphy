https://portland.craigslist.org/mlt/apa/d/portland-1br-1ba-fully-remodeled/7631875082.html% function runids=dbbatchfill(batchid,matchbatchid,yestoall,cellid_match);
function runids=dbbatchfill(batchid,matchbatchid,yestoall,cellid_match)

MINLEN=0; % minimum total length of respfiles allowed into
          % batch. 0 lets everyone in.
          
if ~exist('yestoall','var'),
   yestoall=0;
end

dbopen;

sql=['SELECT * FROM sBatch WHERE id=',num2str(batchid)];
params=mysql(sql);
if ~isempty(params.parmstring),
   eval(char(params.parmstring));
end

if exist('cellid_match','var')
 
   [cellfiledata,cellids]=dbbatchcells(batchid, cellid_match);
else
   [cellfiledata,cellids]=dbbatchcells(batchid);
end

rejectcells={'R240A','R247B','modelss'};
cellids=setdiff(cellids,rejectcells);

if exist('matchbatchid','var') && (~isempty(matchbatchid)) && (sum(matchbatchid)>0)
   sql=['SELECT cellid FROM sRunData where batch=',num2str(matchbatchid)];
   matchdata=mysql(sql);
   matchcellid={matchdata.cellid};

   cellids=intersect(cellids,matchcellid);
end

%rcsetstrings;

% switch from jlg to nsl labs
if batchid<108
   respath=['/auto/k5/david/data/batch',num2str(batchid),'/'];
else
   respath=['/auto/data/nsl/users/svd/data/batch',num2str(batchid),'/'];
end

% find current entries in sRunData that don't have any sCellFiles
sql=['SELECT * FROM sRunData WHERE batch=',num2str(batchid)];
rundata=mysql(sql);

for ii=1:length(rundata)
   scdata=dbbatchcells(batchid,rundata(ii).cellid);
   if isempty(scdata)
      fprintf('cell %s missing... deleting sRunData entry\n',...
              rundata(ii).cellid);
      yn=input('Delete (y/[n])? ','s');
      if ~isempty(yn) && strcmp(yn(1),'y')
         dbopen;
         sql=['DELETE FROM sRunData WHERE id=', ...
              num2str(rundata(ii).id)];
         mysql(sql);
         sql=['DELETE FROM sResults WHERE runid=', ...
              num2str(rundata(ii).id)];
         mysql(sql);
      end
   end
end

sql=['SELECT * FROM gRunClass WHERE id=',num2str(params.runclassid(1))];
runclassdata=mysql(sql);
runclass=runclassdata.name;

resbase=['.',runclass,'.',params.kernfmt, ...
         '.',num2str(batchid),'.res.mat'];
kernbase=['.',runclass,'.',params.kernfmt, ...
          '.',num2str(batchid),'.kern.mat'];

fprintf('respath: %s\nresbase: %s\nkernbase: %s\n',...
        respath,resbase,kernbase);
%disp('paused before adding new runs');
%keyboard
runids=[];
runcount=0;
for ii=1:length(cellids),
   
   sql=['SELECT * FROM sRunData WHERE cellid="',cellids{ii},'"',...
        ' AND batch=',num2str(batchid)];
   rundata=mysql(sql);
   
   if ~isempty(rundata),
      %fprintf('Entry exists for cellid %s. Skipping.\n',cellids{ii});
      
   elseif (strcmp(cellids{ii}(1:5),'sir00') || ...
         strcmp(cellids{ii}(1:5),'sir01') || ...
         strcmp(cellids{ii}(1:5),'sir02') || ...
         strcmp(cellids{ii}(1:5),'sir03') || ...
         strcmp(cellids{ii}(1:5),'sir04') || ...
         strcmp(cellids{ii}(1:5),'sir05')) && ...
         ismember(batchid,[133:138 185:188 194]),
      
      fprintf('Skipping cellid %s for technical reason.\n',cellids{ii});
         
   else
      if yestoall,
         fprintf('Auto-adding entry for cellid %s\n',cellids{ii});
         yn='y';
      else
         yn=input(sprintf('Add entry for cellid %s (y/[n]) ',...
                          cellids{ii}),'s');
      end
      if ~isempty(yn) && strcmp(yn(1),'y'),
         sql=['SELECT * FROM gSingleCell WHERE cellid="',cellids{ii},'"'];
         singledata=mysql(sql);
         if isempty(singledata),
            clear singledata
            singledata.id=0;
            singledata.masterid=0;
            warning('gSingleCell entry missing.');
         end
         %keyboard
         sql=['INSERT INTO sRunData ',...
              ' (singleid,masterid,cellid,batch,respath,resfile,kernfile)',...
              ' VALUES (',num2str(singledata.id),',',...
              num2str(singledata.masterid),',',...
              '"',cellids{ii},'",',...
              num2str(batchid),',',...
              '"',respath,'",',...
              '"',[cellids{ii},resbase],'",',...
              '"',[cellids{ii},kernbase],'")'];
         [aa,bb,newrunid]=mysql(sql);
         runids=[runids;newrunid];
         runcount=runcount+1;
      end
   end
end

fprintf('batchid=%d added %d runs, resbase=%s\n',...
        batchid,runcount,resbase);

if yestoall
    yn='y';
else
    yn=input(sprintf('Refresh NarfBatches for batch %d (y/[n]) ', batchid),'s');
end

dbopen;
if ~isempty(yn) && strcmp(yn(1),'y')
   sql=['SELECT * FROM sBatch WHERE id=',num2str(batchid)];
   batchparams=mysql(sql);
   
   % queries sRunData to find list of cells in batch
   % (up to date, based on what happened above) but then pulls metadata for
   % each of those cells from NarfBatches if they happen to be listed in
   % NarfBatches
   cells = request_celldb_batch(batchid); 
   RDT_batches=[269,273,284,285];
   
   batchdata=struct();
   prev_stimfiles = {};
   for ii = 1:length(cells)  % go through each cell, 
       
      cell = cells{ii};
      cellid=cell.cellid;
      
      % find any meta data that has been stored in NarfBatches for this
      % cell. This can basically be ignored
      batchdata(ii).cellid = cell.cellid;
      batchdata(ii).est_set = write_readably(cell.training_set);
      batchdata(ii).val_set = write_readably(cell.test_set);
      batchdata(ii).filecode=cell.filecode;
      
      %[snr_val, snr_est] = db_get_snr(cellid, batchid, batchdata);
      [batchdata(ii).val_snr, batchdata(ii).est_snr, ~, ~,...
         batchdata(ii).iso_val,batchdata(ii).iso_est] = db_get_snr(cellid, batchid, true);
      batchdata(ii).min_isolation=min(batchdata(ii).iso_val,batchdata(ii).iso_est);
      batchdata(ii).min_snr_index=min(batchdata(ii).val_snr,batchdata(ii).est_snr);
      s = request_celldb_batch(batchid, cellid);
      batchdata(ii).est = dbgetscellfile('cellid', cellid, 'rawid', s{1}.training_rawid);
      batchdata(ii).val = dbgetscellfile('cellid', cellid, 'rawid', s{1}.test_rawid);
      
      %if length(est) > 1 || length(val) > 1
      %    fprintf('Warning: %s had multiple results founds for reps!?\n', cellid);
      %end
      
      if isempty(batchdata(ii).est_snr) || isnan(batchdata(ii).est_snr) batchdata(ii).est_snr = 0.0; end
      if isempty(batchdata(ii).val_snr) batchdata(ii).val_snr = 0.0; end
      if isempty(batchdata(ii).min_isolation) batchdata(ii).min_isolation=0; end
      if isempty(batchdata(ii).min_snr_index) batchdata(ii).min_snr_index=0; end
      if isempty(batchdata(ii).est(end).reps) batchdata(ii).est(end).reps=0; end
      if isempty(batchdata(ii).val(end).reps) batchdata(ii).val(end).reps=0; end
      
      % now for the current cell find all valid entries in sCellFile
      if ismember(batchid,[241,244,251,252,253,254,255,258,268,274,275,279,280,286,287])
         cfd=dbgettspfiles(cellid,batchid);
      else
         cfd=dbbatchcells(batchid,cellid);
      end
      batchdata(ii).cfd=struct();
      for jj=1:length(cfd)
         batchdata(ii).cfd(jj).rawid=cfd(jj).rawid;
         spkfile=[cfd(jj).path,cfd(jj).respfile];
         batchdata(ii).cfd(jj).spkfile_original=spkfile;
         stimfile=[cfd(jj).stimpath,cfd(jj).stimfile];
         batchdata(ii).cfd(jj).stimfile_original=stimfile;
         
         batchdata(ii).cfd(jj).cellid=cellid;
         if any(strcmp(prev_stimfiles,stimfile))
            fprintf('.');
         elseif ~ismember(batchid, RDT_batches)
            prev_stimfiles{end+1} = stimfile; 
            % Make sure that stimulus files have been generated in a form
            % that NEMS can read
            if ismember(batchid,[301,303,305,307,309,293,295,311,312,315,317])
               [~,stimparam]=loadstimfrombaphy(stimfile,[],[],'parm',40,0,0,1,'ReferenceHandle',1,1);
               [~,stimparam]=loadstimfrombaphy(stimfile,[],[],'parm',100,0,0,1,'ReferenceHandle',1,1);
               [~,stimparam]=loadstimfrombaphy(stimfile,[],[],'none',100,0,0,1,'ReferenceHandle',1,1);
            end
            if ismember(batchid,[294]), 
               try
                  [~,stimparam]=loadstimfrombaphy(stimfile,[],[],'ozgf',100,18,0,1,'TargetHandle',1,1);
               catch
                  [~,stimparam]=loadstimfrombaphy(stimfile,[],[],'ozgf',100,18,0,1,'ReferenceHandle',1,1);
               end
            end
            if ismember(batchid,[242,259,283,274,275,278,279,280,296,240,288,306])
               [~,stimparam]=loadstimfrombaphy(stimfile,[],[],'envelope',100,0,0,1);
               [~,stimparam]=loadstimfrombaphy(stimfile,[],[],'envelope',200,0,0,1);
            end
            if ismember(batchid,[271,289,272,291,308,290,316,318,319,322,326,327,328,329,333])
               %[~,stimparam]=loadstimfrombaphy(stimfile,[],[],'ozgf',200,18,0,1);
               [~,stimparam]=loadstimfrombaphy(stimfile,[],[],'ozgf',100,18,0,1);
               %[~,stimparam]=loadstimfrombaphy(stimfile,[],[],'ozgf',100,64,0,1);
               [~,stimparam]=loadstimfrombaphy(stimfile,[],[],'ozgf',50,18,0,1);
               %[~,stimparam]=loadstimfrombaphy(stimfile,[],[],'none',20,0,0,1);
               %[~,stimparam]=loadstimfrombaphy(stimfile,[],[],'ozgf',10,18,0,1);
               %[~,stimparam]=loadstimfrombaphy(stimfile,[],[],'ozgf',4,18,0,1);

            end
            if ismember(batchid,[310])
               %[stim,stimparam]=loadstimfrombaphy(parmfile, startbin,
               %stopbin, filtfmt, fsout, chancount, forceregen,
               %includeprestim, SoundHandle, repcount, testonly)
               [~,stimparam]=loadstimfrombaphy(stimfile,[],[],'envelope',100,0,0,1,'ReferenceHandle',1,1);
               [~,stimparam]=loadstimfrombaphy(stimfile,[],[],'envelope',200,0,0,1,'ReferenceHandle',1,1);
            end
         else
            stimlabels={'stim','stim1','stim2','repeating_phase','singlestream','targetid'};
            resplabels={'raster'};
            stimfile=cfd(jj).stimfile;
            outpath=sprintf('/auto/data/code/nems_in_cache/batch%d/',batchid);
            batchdata(ii).cfd(jj).spkfile=sprintf('%s%s_%s_b%d_resp_pertrial',...
               outpath,stimfile,cellid,batchid);
            batchdata(ii).cfd(jj).stimfile=sprintf('%s%s_b%d_stim_pertrial',...
               outpath,stimfile,batchid);
            batchdata(ii).cfd(jj).pupfile='';
         end
         
         if batchid == 306
             % special code for creating pseudo-cellid for SPO data
             baphyparms=dbReadData(batchdata(ii).cfd(jj).rawid);
             F0string=sprintf('%d-%d',baphyparms.Ref_F0s);
             batchdata(ii).cfd(jj).cellid=[batchdata(ii).cfd(jj).cellid '_' F0string];
         end

      end
   end
   
   r=dbGetConParms();
   rtemp=r;
   rtemp.DB_SERVER='hyrax.ohsu.edu:1234';
   
   % temporarily connect to NEMS db
   %dbSetConParms(rtemp);
   
   sql = ['DELETE from NarfBatches WHERE batch="' num2str(batchid) '"'];
   mysql(sql);
   sql = ['DELETE from NarfData WHERE batch="' num2str(batchid) '"'];
   mysql(sql);
   sql = ['DELETE from sBatch WHERE id="' num2str(batchid) '"'];
   mysql(sql);
   
   sqlinsert('sBatch', ...
         'id', num2str(batchid), ...
         'name',batchparams.name, ...
         'details',batchparams.details, ...
         'runclassid', num2str(batchparams.runclassid), ...
         'parmstring', char(batchparams.parmstring));
   
     %NarfBatches one row for each (pseudo)cell, batch (used by NEMS web, contains metadata)
     %NarfData one row for each file, (pseudo)cell, batch combo (used by NEMS)
   for ii = 1:length(batchdata)
       
       % create an entry in NarfBatches for each pseudocellid
       pcellids=unique({batchdata(ii).cfd.cellid});
       for jj = 1:length(pcellids),
          sqlinsert('NarfBatches', ...
             'batch',      num2str(batchid), ...
             'cellid',     pcellids{jj}, ...
             'est_snr',    batchdata(ii).est_snr, ...
             'val_snr',    batchdata(ii).val_snr, ...
             'min_isolation',batchdata(ii).min_isolation,...
             'min_snr_index',batchdata(ii).min_snr_index,...
             'est_reps',   batchdata(ii).est(end).reps, ...
             'val_reps',   batchdata(ii).val(end).reps, ...
             'filecodes',  write_readably(batchdata(ii).filecode));
%             'est_set',    batchdata(ii).est_set, ...
%             'val_set',    batchdata(ii).val_set, ...
       end
       
       % now make an entry in NarfData for each (batch,pcellid,rawid)
      for jj=1:length(batchdata(ii).cfd)
         sqlinsert('NarfData', ...
            'batch',      num2str(batchid), ...
            'cellid',     batchdata(ii).cfd(jj).cellid, ...
            'rawid',      batchdata(ii).cfd(jj).rawid, ...
            'groupid',    num2str(jj),...
            'label',      'parm', ...
            'filepath',   batchdata(ii).cfd(jj).stimfile_original,...
            'notes',      '',...
            'labgroup',   'lbhb',...
            'username',   'nems');
%          for kk=1:length(resplabels)
%             sqlinsert('NarfData', ...
%                'batch',      num2str(batchid), ...
%                'cellid',     batchdata(ii).cellid, ...
%                'rawid',      batchdata(ii).cfd(jj).rawid, ...
%                'groupid',    num2str(jj),...
%                'label',      resplabels{kk}, ...
%                'filepath',   batchdata(ii).cfd(jj).spkfile,...
%                'notes',      batchdata(ii).cfd(jj).spkfile_original,...
%                'labgroup',   'lbhb',...
%                'username',   'nems');
%          end
%          for kk=1:length(stimlabels)
%             sqlinsert('NarfData', ...
%                'batch',      num2str(batchid), ...
%                'cellid',     batchdata(ii).cellid, ...
%                'rawid',      batchdata(ii).cfd(jj).rawid, ...
%                'groupid',    num2str(jj),...
%                'label',      stimlabels{kk}, ...
%                'filepath',   batchdata(ii).cfd(jj).stimfile,...
%                'notes',      batchdata(ii).cfd(jj).stimfile_original,...
%                'labgroup',   'lbhb',...
%                'username',   'nems');
%          end
%          if ~isempty(batchdata(ii).cfd(jj).pupfile)
%             sqlinsert('NarfData', ...
%                'batch',      num2str(batchid), ...
%                'cellid',     batchdata(ii).cellid, ...
%                'rawid',      batchdata(ii).cfd(jj).rawid, ...
%                'groupid',    num2str(jj),...
%                'label',      'pupil', ...
%                'filepath',   batchdata(ii).cfd(jj).pupfile,...
%                'labgroup',   'lbhb',...
%                'username',   'nems');
%          end
      end
   end
   
   % restore old db connection
   dbSetConParms(r);

end
   





    
