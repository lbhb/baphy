function [snr_val,snr_est, z_val, z_est,iso_val,iso_est, seconds_val, seconds_est]=db_get_snr(cellid, batch, repstransform)
% [snr_val,snr_est, z_val, z_est,iso_val,iso_est, seconds_val, seconds_est]=db_get_snr(cellid, batch, repstransform)
% [snr_val,snr_est, z_val, z_est,iso_val,iso_est, seconds_val, seconds_est]=db_get_snr(cellid, batch)

% if repstransform is true, snr=reps*snr^2, else, snr=snr
%  repstransform is false by default
% for snr, z, and iso, returned value is the minimum across categories (ex. clean/noise) after taking the max within category.

if ~exist('repstransform', 'var')
    repstransform = false;
end

dbopen;
batchdata=request_celldb_batch(batch, cellid);
edata= dbgetscellfile('cellid', cellid, 'rawid', batchdata{1}.test_rawid);
if isempty(batchdata{1}.test_filecode),
   for ii=1:length(batchdata{1}.training_set),
      batchdata{1}.filecode{ii}='X';
   end
   for ii=1:length(batchdata{1}.test_set),
      batchdata{1}.test_filecode{ii}='X';
   end
end
   
ufcs=unique([batchdata{1}.filecode,batchdata{1}.test_filecode]);

seps=strfind(edata(1).respfile,'_');
spk=strfind(edata(1).respfile,'.spk');
runclass=edata(1).respfile(seps(2)+1:spk(1)-1);


if(strcmp(runclass,'VOC_VOC'))
    batch=263;
    use_cache=true;
    cache_path='/auto/users/luke/_narf/VOC in Noise/caches/interleaved_snr_z/';
    pth=[cache_path,edata.respfile(1:end-8),'-',edata.cellid,'.mat'];
    cache_options=struct('channel',edata.channum,...
        'unit',edata.unit,...
        'rasterfs',25);
    [status,Data,failed_vars]=UTcached_file('load',pth,cache_options);
    if(~status)
        error('Need to calculate SNR for %s',edata.cellid)
    end
    if(repstransform)
        snr_val=min(Data.reps(:,2) .* Data.snr(:,2).^2);
    else
        snr_val=min(Data.snr(:,2));
    end
else
    % svd hack to prevent problems with old DMS data that doesn't have all the
    % fields filled in
    for ii=1:length(edata),
        if isempty(edata(ii).reps), edata(ii).reps=1; end
        if isempty(edata(ii).respSNR), edata(ii).respSNR=0; end
    end
    
    %for each unique file code (category), find the max SNR over all matching
    %filecodes, then find the min across unique file codes
    if(repstransform)
        snr_val=min(cellfun(@(x) max(arrayfun(@(y) y.reps * y.respSNR.^2 ,edata(strcmp(batchdata{1}.test_filecode,x)))) , ufcs));
    else
        snr_val=min(cellfun(@(x) max([edata(strcmp(batchdata{1}.test_filecode,x)).respSNR]) , ufcs));
    end
end
if nargout>1,
    % only execute if second parameter requested
    if(strcmp(runclass,'VOC_VOC'))
        z_val=min(Data.z(:,2));
        iso_val=edata.isolation;
        if(repstransform)
            snr_est=min(Data.reps(:,1) .* Data.snr(:,1).^2);
        else
            snr_est=min(Data.snr(:,1));
        end
        z_est=min(Data.z(:,1));
        iso_est=edata.isolation;
    else
        z_val=min(cellfun(@(x) max([edata(strcmp(batchdata{1}.test_filecode,x)).respZ]) , ufcs));
        iso_val=min(cellfun(@(x) max([edata(strcmp(batchdata{1}.test_filecode,x)).isolation]) , ufcs));
        
        edata= dbgetscellfile('cellid', cellid, 'rawid', batchdata{1}.training_rawid);
        if(repstransform)
            snr_est=min(cellfun(@(x) max([edata(strcmp(batchdata{1}.filecode,x)).respSNR]) , ufcs));
        else
            snr_est=min(cellfun(@(x) max([edata(strcmp(batchdata{1}.filecode,x)).respSNR]) , ufcs));
        end
        z_est=min(cellfun(@(x) max([edata(strcmp(batchdata{1}.filecode,x)).respZ]) , ufcs));
        iso_est=min(cellfun(@(x) max([edata(strcmp(batchdata{1}.test_filecode,x)).isolation]) , ufcs));
    end
end

% experiment length calculations
if nargout>6,
    if(strcmp(runclass,'VOC_VOC'))
        error('Write compuation of seconds_val and seconds_est for interleaved VOC_VOC here if needed.')
    else
        if isempty(edata)
            fprintf('Whaaaaaa!?\n');
            Spikefs = 25000;
        else
            ed = edata(1);
            evpfile=[ed.stimpath ed.stimfile '.evp'];
            try
                [~,~,~,Spikefs]=evpgetinfo(evpfile); % Luke: removed 1 flag [~,~,~,Spikefs]=evpgetinfo(evpfile,1);
            catch
                fprintf('WARNING: Defaulting to 25kHz sampling rate\n');
                Spikefs=25000;
            end
        end
        
        %this ignores categories, should it?
        seconds_val=sum(cat(1, edata.resplen))./Spikefs;
        seconds_est=sum(cat(1, edata.resplen))./Spikefs;
    end
end

