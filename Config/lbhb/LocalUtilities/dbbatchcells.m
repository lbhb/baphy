%function [cellfiledata,cellids,params]=dbbatchcells(batchid,cellid);
%
% SVD 2013
function [cellfiledata,cellids,params]=dbbatchcells(batchid,cellid)

dbopen;

if isnumeric(batchid)
   sql=['SELECT * FROM sBatch WHERE id=',num2str(batchid)];
   params=mysql(sql);
   if ~isempty(params.parmstring)
      eval(char(params.parmstring));
   end
else
   params=batchid;
end

cellargs={};

if exist('cellid','var')
   cellargs=cat(2,cellargs,{'cellid'},{cellid});
end
params.miniso=getparm(params,'miniso',0);
params.minrespSNR=getparm(params,'minrespSNR',-Inf);
params.minrespZ=getparm(params,'minrespZ',-Inf);
params.activeonly=getparm(params,'activeonly',0);

if 1 || params.stimspeedid==0,
   % disp('disabling speed test');
elseif params.stimspeedid>=60,
   cellargs=cat(2,cellargs,{'speedgt'},{params.stimspeedid-1});
else
   cellargs=cat(2,cellargs,{'speed'},{params.stimspeedid});
end

if isfield(params,'cellid') && ~isempty(params.cellid),
   cellargs=cat(2,cellargs,{'cellid'},{params.cellid});
end
if isfield(params,'minrepcount') && params.minrepcount>0,
   cellargs=cat(2,cellargs,{'minrepcount'},{params.minrepcount});
end


if isfield(params,'ReferenceClass') && ~isempty(params.ReferenceClass),
   cellargs=cat(2,cellargs,{'ReferenceClass'},{params.ReferenceClass});
end
if isfield(params,'stimsnr') && ~isempty(params.stimsnr),
   cellargs=cat(2,cellargs,{'stimsnr'},{params.stimsnr});
end

if isfield(params,'area') && ~isempty(params.area),
   %fprintf('restricted to cells matching area="%s"\n',params.area);
   cellargs=cat(2,cellargs,{'area'},{params.area});
end

if isfield(params,'dataparm'),
   cellargs=cat(2,cellargs,params.dataparm);
end

if strcmp(params.resploadcmd,'loadgammaraster'),
   % lfp data must exist.  also only need one unit per channel!
   cellargs=cat(2,cellargs,{'lfp'},{1});
end

if ismember(batchid,[140 143]),
   cellargs=cat(2,cellargs,{'runclassid'},{32});
else
   cellargs=cat(2,cellargs,{'runclassid'},{params.runclassid});
end

cellargs=cat(2,cellargs,{'respfmtcode'},{params.respfmtcode});
%cellargs=cat(2,cellargs,{'stimfmtcode'},{params.stimfmtcode});

if ~isfield(params,'specialbatch') && params.miniso>0
   cellargs=cat(2,cellargs,{'isolation'},{['>=' num2str(params.miniso)]});
end

switch batchid,
 case {139,140,143,173,174,198,199,215,216,218,219,220,229},%230
  fprintf('batch %d: special, only cells with active gDataRaw\n',...
          batchid);
  cellargs=cat(2,cellargs,{'behavior'},{1});
  cellargs=cat(2,cellargs,{'speedgt'},{0.5});

end

[cellfiledata,cellids,cellfileids]=dbgetscellfile(cellargs{:});

switch batchid,

 case {206,207,221,222,223,224,225,226,227,228,231,232},
   fprintf('batch %d: special, only one cell per site\n',...
           batchid);
   masterid=cat(1,cellfiledata.masterid);
   singleid=cat(1,cellfiledata.singleid);
   try
       [masterlist,uidx]=unique(masterid,'first');
   catch
       [masterlist,uidx]=unique(masterid);  % older versions of matlab
   end
   singlelist=singleid(uidx);
   keepidx=find(ismember(singleid,singlelist));

   cellids=unique({cellfiledata(keepidx).cellid});
   cellfileids=cellfileids(keepidx);

   cellfiledata=cellfiledata(keepidx);
  case 308
     % fixed subset of A1 NAT cells.
     sql=['SELECT DISTINCT cellid FROM Results WHERE batch=289',...
        ' AND modelname like "%loadpop%tf.n.rb30"',...
        ' AND (cellid like "AMT%" OR',...
        ' cellid like "bbl104%" OR cellid like "BRT026%" OR',...
        ' cellid like "BRT033%" OR cellid like "BRT034%" OR',...
        ' cellid like "BRT038%")'];
     %cellid like "BRT026%" OR
     %cellid like "bbl099%" OR
     celldata=mysql(sql);
     celllist = {celldata.cellid};
     keepfiles=zeros(size(cellfiledata));
     keepcellids={};

     for ii=1:length(cellfiledata)
        cellid = cellfiledata(ii).cellid;
        idx = find(strcmp(celllist, cellid), 1);
        if ~isempty(idx)
           keepfiles(ii)=1;
           keepcellids=union(keepcellids,cellfiledata(ii).cellid);
        end
     end
     keepidx=find(keepfiles);
     cellfileids=cellfileids(keepidx);
     cellfiledata=cellfiledata(keepidx);
     cellids=keepcellids;


end

if isfield(params,'specialbatch'),
    % note if specialbatch, need to deal with any miniso constraint here
    switch lower(params.specialbatch),

      case {'spo'}

        keepfiles=zeros(size(cellfiledata));
        keepcellids={};
        for ii=1:length(cellfiledata)
            parms=dbReadData(cellfiledata(ii).rawid);
            %If Ref_RemoveIdx is 3, stimuli indexes 3 and above were
            %removed, so this likely contains only data suitable for PSTH
            %analysis, not STRF analysis -> remove it.
            %If Ref_RemoveIdx is 0 this means down't remove anythign so
            %keep it, this likely contains data useable for STRF analysis.
            if (parms.Ref_RemoveIdx == 0 ||  parms.Ref_RemoveIdx > 3)  && ...
                    cellfiledata(ii).isolation>=params.miniso
                keepfiles(ii)=1;
                keepcellids=union(keepcellids,cellfiledata(ii).cellid);
            end
        end

        keepidx=find(keepfiles);
        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=keepcellids;

      case {'olp','olp-ferret'}

        keepfiles=zeros(size(cellfiledata));
        keepcellids={};
        for ii=1:length(cellfiledata)
            parms=dbReadData(cellfiledata(ii).rawid);
            % require 'Manual' Ref_Combos
            if strcmpi(strtrim(parms.Ref_Combos),'Manual') && (cellfiledata(ii).isolation>=params.miniso) && ...
               (~contains(cellfiledata(ii).cellid,'HOD')) && (~contains(cellfiledata(ii).cellid,'TBR'))
                keepfiles(ii)=1;
                keepcellids=union(keepcellids,cellfiledata(ii).cellid);
            end
        end

        keepidx=find(keepfiles);
        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=keepcellids;


      case {'voc'}

        keepfiles=zeros(size(cellfiledata));
        cleanfiles=zeros(size(cellfiledata));
        keepcellids={};
        for ii=1:length(cellfiledata)
            parms{ii}=dbReadData(cellfiledata(ii).rawid);
        end

        for ii=1:length(cellfiledata)
            if ~isfield(parms{ii},'Ref_Subsets') || parms{ii}.Ref_Subsets~=5
                keepfiles(ii)=1;
                keepcellids=union(keepcellids,cellfiledata(ii).cellid);
            end
        end

        for ii=1:length(cellfiledata)
            if ismember(cellfiledata(ii).cellid,keepcellids) &&...
                ((isfield(parms{ii},'Ref_SNR') && parms{ii}.Ref_SNR>=100) ...
                     || ~isfield(parms{ii},'Ref_SNR'))
                cleanfiles(ii)=1;
            end
        end

        keepidx=find(cleanfiles);
        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=keepcellids;

      case 'nti+cpn'

        % must have fit subset (not 5) and val subset (5)
        [ucells,~,f2c]=unique({cellfiledata.cellid});
        matches=zeros(length(ucells),2);

        for ii=1:length(cellfiledata),
            ucid=find(strcmp(cellfiledata(ii).cellid,ucells));
            if cellfiledata(ii).runclassid==121
                matches(ucid,1)=1;
            elseif cellfiledata(ii).runclassid==123
                matches(ucid,2)=1;
            end
        end
        
        matchcells=find(sum(matches,2)==2);
        keepidx=find(ismember(f2c,matchcells));

        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=ucells(matchcells);

      case 'voc+val'

        % must have fit subset (not 5) and val subset (5)
        [ucells,~,f2c]=unique({cellfiledata.cellid});
        matches=zeros(length(ucells),2);

        parms={};
        for ii=1:length(cellfiledata)
            parms{ii}=dbReadData(cellfiledata(ii).rawid);
        end

        for ii=1:length(cellfiledata),
            ucid=find(strcmp(cellfiledata(ii).cellid,ucells));
            if isfield(parms{ii},'Ref_Subsets') && ...
                    cellfiledata(ii).stimsnr>=100 && ...
                    parms{ii}.Ref_Subsets==5,
                matches(ucid,2)=1;
            elseif cellfiledata(ii).stimsnr>=100,
                matches(ucid,1)=1;
            else
                f2c(ii)=0;
            end
        end

        matchcells=find(sum(matches,2)==2);
        keepidx=find(ismember(f2c,matchcells));

        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=ucells(matchcells);

      case {'vocnoise','spcnoise'},

        keepfiles=zeros(size(cellfiledata));
        cleanfiles=zeros(size(cellfiledata));
        keepcellids={};
        cleancellids={};

        subset=zeros(size(cellfiledata));
        noisecond=zeros(size(cellfiledata));
        for ii=1:length(cellfiledata),

           if not(strcmp(cellfiledata(ii).cellid(1:3),'BOL')) || ...
                 cellfiledata(ii).isolation>=params.miniso
            parms=dbReadData(cellfiledata(ii).rawid);
            if ~isempty(parms) && isfield(parms,'Ref_Subsets') && isfield(parms,'Tar_Subsets'),
               % possible interleaved file!
               subset(ii)=parms.Ref_Subsets;
               SNR=[parms.Ref_SNR parms.Tar_SNR];
               if any(SNR>=100) && any(SNR==0),
                  keepfiles(ii)=1;
                  keepcellids=union(keepcellids,cellfiledata(ii).cellid);
                  cleanfiles(ii)=1;
                  cleancellids=union(cleancellids,cellfiledata(ii).cellid);
                  noisecond(ii)=3;
               end

            else
               if ~isempty(parms) && isfield(parms,'Ref_Subsets'),
                  subset(ii)=parms.Ref_Subsets;
                  SNR_field='Ref_SNR';
                  Type_field='Ref_NoiseType';
                  Reverb_field='Ref_ReverbTime';
               elseif ~isempty(parms) && isfield(parms,'Tar_Subsets')
                  subset(ii)=parms.Tar_Subsets;
                  SNR_field='Tar_SNR';
                  Type_field='Tar_NoiseType';
                  Reverb_field='Tar_ReverbTime';
               end

               if isfield(parms,SNR_field) && parms.(SNR_field)==0 && strcmpi(strtrim(parms.(Type_field)),'White'),
                  keepfiles(ii)=1;
                  keepcellids=union(keepcellids,cellfiledata(ii).cellid);
                  noisecond(ii)=2;
               end
               if (isfield(parms,SNR_field) && parms.(SNR_field)>=100 && parms.(Reverb_field)==0) || ~isfield(parms,SNR_field),
                  cleanfiles(ii)=1;
                  cleancellids=union(cleancellids,cellfiledata(ii).cellid);
                  noisecond(ii)=1;
               end
            end
           end
        end
        allcellids={cellfiledata.cellid};
        keepcellids=intersect(keepcellids,cleancellids);
        keepidx=find(ismember(allcellids,keepcellids));
        keepidx=intersect(keepidx,find(keepfiles+cleanfiles));

        if length(unique(keepcellids))==1,
           isolation=cat(1,cellfiledata.isolation);
           if any(noisecond==3),
              ff=find(noisecond==3);
              if length(ff)>1,
                 maxisoidx=ff(min(find(isolation(ff)==max(isolation(ff)))));
                 keepidx=maxisoidx;
              else
                 keepidx=ff;
              end

           else
              isolation=cat(1,cellfiledata.isolation);
              for nidx=1:2,
                 for subidx=unique(subset(noisecond==nidx & subset>0))',
                    ff=find(subset==subidx & noisecond==nidx);
                    if length(ff)>1,
                       maxisoidx=ff(min(find(isolation(ff)==max(isolation(ff)))));
                       ff=setdiff(ff,maxisoidx);
                       keepidx=setdiff(keepidx,ff);
                    end
                 end
              end
           end
        end

        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=keepcellids;

      case {'voc+tor'},

        % must have fit subset (not 5) and val subset (5)
        [ucells,~,f2c]=unique({cellfiledata.cellid});
        matches=zeros(length(ucells),2);

        parms=cell(length(cellfiledata),1);
        for ii=1:length(cellfiledata),
           if cellfiledata(ii).isolation>=params.miniso
              parms{ii}=dbReadData(cellfiledata(ii).rawid);
           end
        end

        for ii=1:length(cellfiledata),
            ucid=find(strcmp(cellfiledata(ii).cellid,ucells));
            if isempty(parms{ii}),
               %error, skip
               f2c(ii)=0;
            elseif strcmpi(parms{ii}.ReferenceClass,'FerretVocal') && ...
                  (~isfield(parms{ii},'Ref_Subsets') || parms{ii}.Ref_Subsets~=5) && ...
                cellfiledata(ii).stimsnr>=100,
                % valid VOC set
                matches(ucid,2)=1;
            elseif strcmpi(parms{ii}.ReferenceClass,'Torc'),
                % valid TOR set
                matches(ucid,1)=1;
            elseif strcmpi(parms{ii}.ReferenceClass,'FerretVocal') && ...
                cellfiledata(ii).stimsnr>=100,
                % valid accessory VOC
                % -- don't set f2c to zero
            else
                f2c(ii)=0;
            end
        end

        matchcells=find(sum(matches,2)==2);
        keepidx=find(ismember(f2c,matchcells));

        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=ucells(matchcells);

      case {'nat-tor-pupil'},

        % must have fit subset (not 5) and val subset (5)
        [ucells,~,f2c]=unique({cellfiledata.cellid});
        matches=zeros(length(ucells),2);

        parms=cell(length(cellfiledata),1);
        for ii=1:length(cellfiledata),
           if ~isempty(cellfiledata(ii).isolation) && ...
                   cellfiledata(ii).isolation>=params.miniso && ...
                   ~isempty(cellfiledata(ii).pupil),
              parms{ii}=dbReadData(cellfiledata(ii).rawid);
           end
        end

        for ii=1:length(cellfiledata),
            ucid=find(strcmp(cellfiledata(ii).cellid,ucells));
            if isempty(parms{ii}),
               %error, skip
               f2c(ii)=0;
            elseif strcmpi(parms{ii}.ReferenceClass,'NaturalSounds') && ...
                cellfiledata(ii).stimsnr>=100,
                % valid NAT set
                matches(ucid,2)=1;
            elseif strcmpi(parms{ii}.ReferenceClass,'Torc2'),
                % valid TOR set
                matches(ucid,1)=1;
            else
                f2c(ii)=0;
            end
        end

        matchcells=find(sum(matches,2)==2);
        keepidx=find(ismember(f2c,matchcells));

        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=ucells(matchcells);

      case 'cs',

        % spn / center-surround
        keepfiles=zeros(size(cellfiledata));
        keepcellids={};
        for ii=1:length(cellfiledata),
            parms=dbReadData(cellfiledata(ii).rawid);
            if (~isfield(parms,'Ref_SplitChannels') ||...
                    strcmpi(strtrim(parms.Ref_SplitChannels),'No')) &&...
                    length(parms.Ref_LowFreq)>1 &&...
                    ismember(parms.Ref_LowFreq(end),[125 250 500 1000]) &&...
                    ismember(parms.Ref_HighFreq(end),[4000 8000 16000 32000]),
                keepfiles(ii)=1;
                keepcellids=union(keepcellids,cellfiledata(ii).cellid);
            end
        end

        keepidx=find(keepfiles);
        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=keepcellids;
       case 'lr',

        % spn / left-right same spectral features batch
        keepfiles=zeros(size(cellfiledata));
        keepcellids={};
        for ii=1:length(cellfiledata),
            parms=dbReadData(cellfiledata(ii).rawid);
            if isfield(parms,'Ref_SplitChannels') &&...
                    strcmpi(strtrim(parms.Ref_SplitChannels),'Yes') &&...
                    length(parms.Ref_LowFreq)>1 &&...
                    diff(parms.Ref_LowFreq)==0 &&...
                    diff(parms.Ref_HighFreq)==0 && ...
                    ~strcmpi(cellfiledata(ii).behavior,'active') ,
                keepfiles(ii)=1;
                keepcellids=union(keepcellids,cellfiledata(ii).cellid);
            end
        end

        keepidx=find(keepfiles);
        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=keepcellids;
       case 'hl',

         % spn / high-low, non-overlapping bands in single speaker
         keepfiles=zeros(size(cellfiledata));
         keepcellids={};
         for ii=1:length(cellfiledata),
            parms=dbReadData(cellfiledata(ii).rawid);
            if (~isfield(parms,'Ref_SplitChannels') ||...
                    strcmpi(strtrim(parms.Ref_SplitChannels),'No')) &&...
                    length(parms.Ref_LowFreq)>1 &&...
                    (parms.Ref_HighFreq(1)-10<=parms.Ref_LowFreq(2) || ...
                     parms.Ref_HighFreq(2)-10<=parms.Ref_LowFreq(1)),
                keepfiles(ii)=1;
                keepcellids=union(keepcellids,cellfiledata(ii).cellid);
            end
        end

        keepidx=find(keepfiles);
        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=keepcellids;

       case {'spn-array','nat-array','nat-array4', 'pps-array','array'}
         
          siteids = cellfun(@get_site,cellids,'uni',false);
          
          % nat-array4  requits NAT subsets 3 and 4
          [ucells,~,f2c]=unique({cellfiledata.cellid});
          matches=zeros(length(ucells),2);
       
          keepfiles=zeros(size(cellfiledata));
          keepcellids={};
          for ii=1:length(cellfiledata)
             siteid = get_site(cellfiledata(ii).cellid);
             
             if strcmpi(params.specialbatch,'nat-array4')
                parms=dbReadData(cellfiledata(ii).rawid);
                ucid=find(strcmp(cellfiledata(ii).cellid,ucells));
                if isfield(parms,'Ref_Subsets') && parms.Ref_Subsets==3
                   matches(ucid,1)=1;
                elseif isfield(parms,'Ref_Subsets') && parms.Ref_Subsets==4
                   matches(ucid,2)=1;
                end
             end
             
             if length(unique(siteids))==1
                cellcount=10;
             elseif strcmpi(siteid,'zee015h')  % exclude tungsten array
                cellcount=0;
             else
                cellcount = sum(strcmpi(siteids, siteid));
             end
             % don't require pupil
             %if ~isempty(cellfiledata(ii).pupil) && ...
             %      cellfiledata(ii).pupil>1 && ...
             
             if cellcount >= 10 && cellfiledata(ii).isolation>=params.miniso
                keepfiles(ii)=1;
                keepcellids=union(keepcellids,cellfiledata(ii).cellid);
             end
          end
          
        
          keepidx=find(keepfiles);
          if strcmpi(params.specialbatch,'nat-array4')
             matchcells=find(sum(matches,2)==2);
             keepidx=intersect(keepidx,find(ismember(f2c,matchcells)));
             
             keepcellids = intersect(keepcellids, ucells(matchcells));
          end
          
          cellfileids=cellfileids(keepidx);
          cellfiledata=cellfiledata(keepidx);
          cellids=keepcellids;

       case {'bnt-mono'}
         
          siteids = cellfun(@get_site,cellids,'uni',false);
          
          % requires bnt with binaural, can also include binaural olp
          % (others?)
          [ucells,~,f2c]=unique({cellfiledata.cellid});
          matches=zeros(length(ucells),1);
       
          keepfiles=zeros(size(cellfiledata));
          keepcellids={};
          for ii=1:length(cellfiledata)
             siteid = get_site(cellfiledata(ii).cellid);
             ucid=find(strcmp(cellfiledata(ii).cellid,ucells));
             parms=dbReadData(cellfiledata(ii).rawid);
             
             if strcmpi(parms.ReferenceClass,'BigNat') && ...
                   strcmpi(strtrim(getparm(parms,'Ref_FitBinaural','None')),'None')
                keepfiles(ii)=1;
                matches(ucid,1)=1;
             end
          end
        
          keepidx=find(keepfiles);
          matchcells=find(matches(:,1));
          keepidx=intersect(keepidx,find(ismember(f2c,matchcells)));
          keepcellids=ucells(matches(:,1)==1);
	  
          cellfileids=cellfileids(keepidx);
          cellfiledata=cellfiledata(keepidx);
          cellids=keepcellids;

        case {'bnt-bin'}
         
          siteids = cellfun(@get_site,cellids,'uni',false);
          
          % requires bnt with binaural, can also include binaural olp
          % (others?)
          [ucells,~,f2c]=unique({cellfiledata.cellid});
          matches=zeros(length(ucells),1);
       
          keepfiles=zeros(size(cellfiledata));
          keepcellids={};
          for ii=1:length(cellfiledata)
             siteid = get_site(cellfiledata(ii).cellid);
             ucid=find(strcmp(cellfiledata(ii).cellid,ucells));
             parms=dbReadData(cellfiledata(ii).rawid);
             
             if strcmpi(parms.ReferenceClass,'OverlappingPairs') && ...
                   strcmpi(getparm(parms,'Ref_Binaural','No'),'Yes')
                keepfiles(ii)=1;
                matches(ucid,2)=1;
             elseif strcmpi(parms.ReferenceClass,'BigNat') && ...
                   ~strcmpi(strtrim(getparm(parms,'Ref_FitBinaural','None')),'None')
                keepfiles(ii)=1;
                matches(ucid,1)=1;
             end
          end
        
          keepidx=find(keepfiles);
          matchcells=find(matches(:,1));
          keepidx=intersect(keepidx,find(ismember(f2c,matchcells)));
          keepcellids=ucells(matches(:,1)==1);

          cellfileids=cellfileids(keepidx);
          cellfiledata=cellfiledata(keepidx);
          cellids=keepcellids;
         
       case {'bnt+olp', 'bnt+olp-bin', 'bnt+ntd', 'bnt+ntd-bin'}
         
          siteids = cellfun(@get_site,cellids,'uni',false);
          if strcmpi(params.specialbatch,'bnt+olp-bin') || strcmpi(params.specialbatch,'bnt+ntd-bin')
              binaural=1;
          else
              binaural=0;
          end
          
          % requires bnt with binaural, can also include binaural olp
          % (others?)
          [ucells,~,f2c]=unique({cellfiledata.cellid});
          matches=zeros(length(ucells),2);
       
          keepfiles=zeros(size(cellfiledata));
          keepcellids={};
          for ii=1:length(cellfiledata)
             siteid = get_site(cellfiledata(ii).cellid);
             ucid=find(strcmp(cellfiledata(ii).cellid,ucells));
             parms=dbReadData(cellfiledata(ii).rawid);
             if binaural
                 % No NAT (NaturalSounds) with binaural config
                 if cellfiledata(ii).runclassid==132
                     % NTD
                     keepfiles(ii)=1;
                     matches(ucid,2)=1;

                 elseif strcmpi(parms.ReferenceClass,'OverlappingPairs') && ...
                       strcmpi(strtrim(getparm(parms,'Ref_Binaural','No')),'Yes') && ...
                       ~strcmpi(strtrim(getparm(parms,'Ref_BG_Folder','Background1')),'Background4')
                    keepfiles(ii)=1;
                    matches(ucid,2)=1;
                 elseif strcmpi(parms.ReferenceClass,'BigNat') && ...
                       ~strcmpi(strtrim(getparm(parms,'Ref_FitBinaural','None')),'None') && ...
                       ~strcmpi(strtrim(getparm(parms,'Ref_TestBinaural','None')),'None')
                    keepfiles(ii)=1;
                    matches(ucid,1)=1;
                 end
             else
                 if cellfiledata(ii).runclassid==132
                     % NTD
                     keepfiles(ii)=1;
                     matches(ucid,2)=1;

                 elseif strcmpi(parms.ReferenceClass,'OverlappingPairs') && ...
                       strcmpi(strtrim(getparm(parms,'Ref_Binaural','No')),'No') && ...
                       ~strcmpi(strtrim(getparm(parms,'Ref_BG_Folder','Background1')),'Background4')
                    keepfiles(ii)=1;
                    matches(ucid,2)=1;
                 elseif strcmpi(parms.ReferenceClass,'BigNat') && ...
                       strcmpi(strtrim(getparm(parms,'Ref_FitBinaural','None')),'None') && ...
                       strcmpi(strtrim(getparm(parms,'Ref_TestBinaural','None')),'None')
                    keepfiles(ii)=1;
                    matches(ucid,1)=1;
                 elseif strcmpi(parms.ReferenceClass,'NaturalSounds')
                    keepfiles(ii)=1;
                    matches(ucid,1)=1;
                 end
             end
          end
          keepidx=find(keepfiles);
          matchcells=find(matches(:,1) & matches(:,2));
          keepidx=intersect(keepidx,find(ismember(f2c,matchcells)));
          keepcellids=ucells(matches(:,1) & matches(:,2));

          cellfileids=cellfileids(keepidx);
          cellfiledata=cellfiledata(keepidx);
          cellids=keepcellids;

       case {'spn-pupil','nat-pupil', 'voc-pupil', 'pps-pupil','pupil', 'cpn-pupil'}

         % spn / high-low, non-overlapping bands in single speaker
         keepfiles=zeros(size(cellfiledata));
         keepcellids={};
         for ii=1:length(cellfiledata)
            if ~isempty(cellfiledata(ii).pupil) && ...
                  cellfiledata(ii).pupil>1 && ...
                  cellfiledata(ii).isolation>=params.miniso

%                  ~strcmpi(cellfiledata(ii).behavior,'active') &&...
                if cellfiledata(ii).runclassid==4
                   % special check for VOC
                   parms=dbReadData(cellfiledata(ii).rawid);

                   if ((isfield(parms,'Ref_Subsets') && parms.Ref_Subsets==5) ||...
                         (isfield(parms,'Tar_Subsets') && parms.Tar_Subsets==5))
                      if batchid==294
                         % want subset 5 for repeated VOC batch
                         keepfiles(ii)=1;
                         keepcellids=union(keepcellids,cellfiledata(ii).cellid);
                      end
                   elseif batchid~=294
                      keepfiles(ii)=1;
                      keepcellids=union(keepcellids,cellfiledata(ii).cellid);
                   end
                elseif cellfiledata(ii).runclassid==121
                   parms=dbReadData(cellfiledata(ii).rawid);
                   if strcmpi(getparm(parms, 'Ref_SequenceStructure', 'XX'), 'AllPermutations')
                     keepfiles(ii)=1;
                     keepcellids=union(keepcellids,cellfiledata(ii).cellid);
                   end
                else
                  keepfiles(ii)=1;
                  keepcellids=union(keepcellids,cellfiledata(ii).cellid);
                end
            end
        end

        keepidx=find(keepfiles);
        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=keepcellids;

      case 'lr-behavior'

        % spn / left-right same spectral features batch
        keepfiles=zeros(size(cellfiledata));
        keepcellids={};
        for ii=1:length(cellfiledata)
            parms=dbReadData(cellfiledata(ii).rawid);
            if isfield(parms,'Ref_SplitChannels') &&...
                    strcmpi(strtrim(parms.Ref_SplitChannels),'Yes') &&...
                    length(parms.Ref_LowFreq)>1 &&...
                    diff(parms.Ref_LowFreq)==0 &&...
                    diff(parms.Ref_HighFreq)==0 && ...
                    strcmpi(cellfiledata(ii).behavior,'active') &&...
                    cellfiledata(ii).isolation>=params.miniso
                keepfiles(ii)=1;
                keepcellids=union(keepcellids,cellfiledata(ii).cellid);
            end
        end
        keepidx=find(keepfiles);
        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=keepcellids;
      case 'lrhl-behavior'

        % spn / left-right same spectral features batch
        keepfiles=zeros(size(cellfiledata));
        keepcellids={};
        for ii=1:length(cellfiledata)
            parms=dbReadData(cellfiledata(ii).rawid);
            if isfield(parms,'Ref_SplitChannels') &&...
                    strcmpi(strtrim(parms.Ref_SplitChannels),'Yes') &&...
                    length(parms.Ref_LowFreq)>1 &&...
                    (diff(parms.Ref_LowFreq)~=0 ||...
                     diff(parms.Ref_HighFreq)~=0) && ...
                    strcmpi(cellfiledata(ii).behavior,'active') &&...
                    cellfiledata(ii).isolation>=params.miniso && ...
                    (~isfield(parms,'Trial_TargetChannel') ||...
                     length(unique(parms.Trial_TargetChannel(...
                         parms.Trial_TargetIdxFreq+parms.Trial_CatchIdxFreq>0)))>1),
                keepfiles(ii)=1;
                keepcellids=union(keepcellids,cellfiledata(ii).cellid);
            end
        end
        keepidx=find(keepfiles);

        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=keepcellids;
      case 'hl-behavior',

        % spn / left-right same spectral features batch
        keepfiles=zeros(size(cellfiledata));
        keepcellids={};
        for ii=1:length(cellfiledata),
            parms=dbReadData(cellfiledata(ii).rawid);
            if (~isfield(parms,'Ref_SplitChannels') ||...
                strcmpi(strtrim(parms.Ref_SplitChannels),'No')) &&...
                    length(parms.Ref_LowFreq)>1 &&...
                    (diff(parms.Ref_LowFreq)~=0 ||...
                     diff(parms.Ref_HighFreq)~=0) && ...
                    strcmpi(cellfiledata(ii).behavior,'active') &&...
                    cellfiledata(ii).isolation>=params.miniso
                keepfiles(ii)=1;
                keepcellids=union(keepcellids,cellfiledata(ii).cellid);
            end
        end
        keepidx=find(keepfiles);
        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=keepcellids;
      case 'lev-behavior',

        % spn / left-right same spectral features batch
        keepfiles=zeros(size(cellfiledata));
        keepcellids={};
        for ii=1:length(cellfiledata),
            parms=dbReadData(cellfiledata(ii).rawid);
            if strcmpi(parms.ReferenceClass,'SpNoise') &&...
                    strcmpi(parms.TrialObjectClass,'MultiRefTar') &&...
                    (length(parms.Ref_LowFreq)==1 ||...
                     (isfield(parms,'Trial_CatchIdxFreq') &&...
                      length(unique(parms.Trial_TargetChannel(...
                         parms.Trial_TargetIdxFreq+parms.Trial_CatchIdxFreq>0)))==1)) &&...
                    length(parms.Trial_RelativeTarRefdB)>1 &&...
                    strcmpi(cellfiledata(ii).behavior,'active') &&...
                    cellfiledata(ii).isolation>=params.miniso
                keepfiles(ii)=1;
                keepcellids=union(keepcellids,cellfiledata(ii).cellid);
            end
        end
        keepidx=find(keepfiles);

        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=keepcellids;

      case {'tsp-pass'},
        % basically a pass-through

        keepfiles=zeros(size(cellfiledata));
        keepcellids0={};
        % first pass, is there an active TSP file?
        for ii=1:length(cellfiledata),
           if strcmpi(cellfiledata(ii).behavior,'active'),
              keepcellids0=union(keepcellids0,cellfiledata(ii).cellid);
           end
        end

        keepcellids={};
        for ii=1:length(cellfiledata)
            if ~strcmpi(cellfiledata(ii).behavior,'active') &&...
                    cellfiledata(ii).isolation>=params.miniso && ...
                    ismember(cellfiledata(ii).cellid,keepcellids0)
                keepfiles(ii)=1;
                keepcellids=union(keepcellids,cellfiledata(ii).cellid);
            end
        end
        keepidx=find(keepfiles);
        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=keepcellids;


      case {'tsp-onbf','tsp-offbf' 'tsp-onbfpass','tsp-offbfpass'},

        % tsp - any behavior with target on BF
        keepfiles=zeros(size(cellfiledata));
        keepcellids={};

        for ii=1:length(cellfiledata),
            parms=dbReadData(cellfiledata(ii).rawid);

            % intial screen: require SpNoise reference and MultiRefTar
            % trialobject as well as behavior.
            if strcmpi(parms.ReferenceClass,'SpNoise') &&...
                    (strcmpi(parms.TargetClass,'JitterTone') ||...
                    strcmpi(parms.TargetClass,'Tone')) &&...
                    strcmpi(parms.TrialObjectClass,'MultiRefTar') &&...
                    strcmpi(cellfiledata(ii).behavior,'active') &&...
                    cellfiledata(ii).isolation>=params.miniso
                 [blo,bhi,tt]=spn_tuning(cellfiledata(ii).cellid);

                 if isfield(parms,'Tar_Frequencies'),
                    maxtar=length(parms.Tar_Frequencies);
                 else
                    maxtar=length(parms.Trial_TargetIdxFreq);
                 end
                 snr=parms.Trial_RelativeTarRefdB;
                 targetfreq=parms.Trial_TargetIdxFreq(1:maxtar);
                 commontargetid=find(targetfreq==max(targetfreq), 1 );
                 targetHz=parms.Tar_Frequencies(commontargetid);

                 % find target band in spn
                 spnlo=parms.Ref_LowFreq;
                 spnhi=parms.Ref_HighFreq;
                 tarband=find(targetHz>=spnlo & targetHz<=spnhi);
                 bandsdiffer=length(spnlo)>1 & abs(diff(spnlo))>0;

                 lblo=log2(blo);
                 lbhi=log2(bhi);
                 if isempty(lbhi),
                    lbhi=0;
                    lblo=0;
                 end
                 bandcount=length(parms.Ref_LowFreq);
                 match_bands=zeros(1,bandcount);
                 for bandidx=1:bandcount,
                    lslo=log2(spnlo(bandidx));
                    lshi=log2(spnhi(bandidx));

                    % fraction overlap==portion of lof-hif encompassed by band
                    if lshi<lblo || lslo>lbhi,
                       match_bands(bandidx)=0;
                    elseif lblo>=lslo && lbhi<=lshi,
                       match_bands(bandidx)=1;
                    elseif lblo<lslo && lbhi<=lshi,
                       match_bands(bandidx)=1-(lslo-lblo)./(lbhi-lblo);
                    elseif lblo>=lslo && lbhi>lshi,
                       match_bands(bandidx)=1-(lbhi-lshi)./(lbhi-lblo);
                    else
                       match_bands(bandidx)=1-(lshi-lslo)./(lbhi-lblo);
                    end
                 end

                 match_bands=spn_tuning_match2(cellfiledata(ii).cellid,286);
                 if isempty(bandsdiffer) || ~bandsdiffer
                    % skip : only one band
                 elseif (strcmpi(params.specialbatch,'tsp-onbf') ||...
                       strcmpi(params.specialbatch,'tsp-onbfpas')) &&...
                       cellfiledata(ii).isolation>=params.miniso &&...
                       match_bands(tarband)>=0.5
                    % ok common target matched tuning of cell.

                    keepfiles(ii)=1;
                    keepcellids=union(keepcellids,cellfiledata(ii).cellid);
                    fprintf('inside match for %s (%d-%d tar=%d)\n',...
                       cellfiledata(ii).cellid,blo,bhi,targetHz);
                elseif (strcmpi(params.specialbatch,'tsp-offbf') ||...
                       strcmpi(params.specialbatch,'tsp-offbfpas')) && ...
                       cellfiledata(ii).isolation>=params.miniso && ...
                       match_bands(tarband)<0.5
                    % ok common target outside tuning of cell.
                    % require tuning in an spn band that doesn't contain
                    % the target
                    fprintf('tarband=%d\n',tarband);
                    if sum(match_bands([1:(tarband-1) (tarband+1):end]))>match_bands(tarband)
                       keepfiles(ii)=1;
                       keepcellids=union(keepcellids,cellfiledata(ii).cellid);
                       fprintf('outside match for %s (%d-%d tar=%d)\n',...
                          cellfiledata(ii).cellid,blo,bhi,targetHz);
                    else
                       fprintf('no spn overlap for %s (%d-%d tar=%d)\n',...
                          cellfiledata(ii).cellid,blo,bhi,targetHz);
                    end
                    disp('');
                 elseif isempty(blo),
                    fprintf('no tuning for cell %s\n',cellfiledata(ii).cellid);
                 end
            end
        end
        keepidx=find(keepfiles);

        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=keepcellids;

      case {'behavior','active-only'},
        % spn / left-right same spectral features batch
        keepfiles=zeros(size(cellfiledata));
        keepcellids={};
        for ii=1:length(cellfiledata),
            if strcmpi(cellfiledata(ii).behavior,'active'),
                keepfiles(ii)=1;
                keepcellids=union(keepcellids,cellfiledata(ii).cellid);
            end
        end
        keepidx=find(keepfiles);
        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=keepcellids;

      case 'passive-only',
        % must be at least one passive file
        keepfiles=zeros(size(cellfiledata));
        keepcellids={};

        for ii=1:length(cellfiledata),
            if ~strcmpi(cellfiledata(ii).behavior,'active'),
                keepfiles(ii)=1;
                keepcellids=union(keepcellids,cellfiledata(ii).cellid);
            end
        end
        keepidx=find(keepfiles);
        cellfileids=cellfileids(keepidx);
        cellfiledata=cellfiledata(keepidx);
        cellids=keepcellids;

       case {'ptd-pr-pupil'},
          % includes 301,303,304,307,309
          cell_set={cellfiledata.cellid};
          
          cellhasactive=zeros(size(cellfiledata));
          goodactive=zeros(size(cellfiledata));
          cellhaspassive=zeros(size(cellfiledata));
          goodpassive=zeros(size(cellfiledata));
          for ii=1:length(cellfiledata),
             parms=dbReadData(cellfiledata(ii).rawid);
             cellmatches=find(strcmp(cellfiledata(ii).cellid,cell_set));
             if ~isempty(cellfiledata(ii).pupil) && ...
                   cellfiledata(ii).pupil>1 && ...
                   strcmpi(cellfiledata(ii).behavior,'active') &&...
                   cellfiledata(ii).isolation>=params.miniso &&...
                   isfield(parms,'ReferenceClass')

%                 &&...
%                    (~isfield(parms,'TrialObjectClass') ||...
%                    (strcmpi(parms.TrialObjectClass,'MultiRefTar') &&...
%                    strcmpi(parms.BehaveObjectClass,'RewardTargetLBHB') &&...
%                    ~strcmpi(parms.Trial_OverlapRefTar,'Yes'))) && ...
%                    ,
                goodactive(ii)=1;
                cellhasactive(cellmatches)=1;
             end
             if ~isempty(cellfiledata(ii).pupil) && ...
                   cellfiledata(ii).pupil>1 && ...
                   ~strcmpi(cellfiledata(ii).behavior,'active') && ...
                   cellfiledata(ii).isolation>=params.miniso
                goodpassive(ii)=1;
                cellhaspassive(cellmatches)=1;
             end
          end
          a1=min(find(goodactive));
          p1=find(goodpassive(1:a1));
          if ~isempty(p1), goodpassive(1:(p1(end)-1))=0; end
          a2=max(find(goodactive));
          p2=find(goodpassive((a2+1):end))+a2;
          if ~isempty(p2), goodpassive((p2(1)+1):end)=0; end

          keepidx=find(cellhaspassive & cellhasactive & ...
             (goodactive | goodpassive));
          cellids=unique({cellfiledata(keepidx).cellid});
          cellfileids=cellfileids(keepidx);
          cellfiledata=cellfiledata(keepidx);

       case {'ptd-cc'}
          rawfiles={'PNB019c04_a_PTD','PNB019g04_a_PTD',...
             'PNB019i04_a_PTD','PNB020a07_a_PTD','PNB020a10_a_PTD',...
             'PNB021o03_a_PTD','PNB021o06_a_PTD','PNB022g04_a_PTD',...
             'PNB022g08_a_PTD','PNB023e04_a_PTD','PNB023e08_a_PTD',...
             'PNB024a05_a_PTD','PNB024c04_a_PTD'};
          gcellids={'PNB019c-a1','PNB019g-a1','PNB019ia2',...
             'PNB019i-a1','PNB019i-a2','PNB020a-a1','PNB021o-1',...
             'PNB022g-a1','PNB023e-b1','PNB024a-b1','PNB024c-a1','PNB024c-b1'};
          gcellids={'PNB019c-a1','PNB019i-a1','PNB020a-a1','PNB020a-a1',...
             'PNB021o-1','PNB021o-1','PNB022g-a1','PNB022g-a1',...
             'PNB023e-b1','PNB023e-b1','PNB024a-b1','PNB024c-a1',...
             'PNB024c-b1','PNB025a-a1','PNB040h-b1','PNB042j-a1',...
             'PNB042k-a1','SIK006b-01-1','SIK006b-02-1','SIK009b-a1',...
             'SIK010c-b1','SIK010c-a1','SIK013c-15-1','SIK013c-29-1',...
             'NMK003c-02-1','NMK003c-02-1','NMK003c-03-1','NMK003c-03-1',...
             'NMK003c-03-2','NMK003c-03-2','NMK003c-04-1','NMK003c-04-1',...
             'NMK003c-06-1','NMK003c-06-1','NMK003c-09-1','NMK003c-09-1',...
             'NMK003c-16-1','NMK003c-16-1','NMK003c-26-1','NMK003c-26-1',...
             'NMK003c-27-1','NMK003c-27-1','NMK003c-30-1','NMK003c-30-1',...
             'NMK003c-33-1','NMK003c-33-1','NMK003c-33-2','NMK003c-33-2',...
             'NMK003c-54-1','NMK003c-54-1','NMK020c-29-1','NMK020c-29-1',...
             'NMK020c-32-1','NMK020c-32-1','NMK020c-61-1','NMK020c-61-1',...
             'NMK026b-10-1','NMK026b-10-1','NMK026b-14-2','NMK026b-14-2',...
             'NMK026b-15-2','NMK026b-15-3','NMK026b-18-1','NMK026b-22-1',...
             'NMK026b-43-1','NMK026b-43-1','NMK026b-55-1','NMK026b-28-1',...
             'NMK026b-28-1','NMK026b-59-1','NMK026b-59-1','NMK026b-61-1',...
             'NMK026b-63-1','NMK026b-63-1','NMK027b-43-1','NMK027b-43-1',...
             'NMK027b-58-2','NMK027b-58-2','NMK027b-58-1','NMK027b-58-1',...
             'NMK029b-2-1','NMK029b-4-1','NMK029b-4-1','NMK029b-14-1',...
             'NMK029b-14-1','NMK029b-23-1','NMK029b-23-1','NMK029b-23-2',...
             'NMK029b-23-2','NMK029b-26-2','NMK029b-26-2','NMK029b-29-1',...
             'NMK029b-29-1','NMK029b-29-2','NMK029b-29-2','NMK029b-42-1',...
             'NMK029b-42-1','NMK029b-44-1','NMK029b-45-1','NMK029b-45-1',...
             'NMK029b-49-1','NMK029b-54-1','NMK029b-54-1','NMK029b-58-1',...
             'NMK029b-58-1'};

          keepidx=zeros(size(cellfiledata));
          for ii=1:length(cellfiledata)
             if sum(strcmp(cellfiledata(ii).cellid, gcellids))
                if ~isempty(cellfiledata(ii).pupil) && ...
                      cellfiledata(ii).pupil>1
                   keepidx(ii)=1;
                end
             end
          end
          keepidx=find(keepidx);
          cellids=unique({cellfiledata(keepidx).cellid});
          cellfileids=cellfileids(keepidx);
          cellfiledata=cellfiledata(keepidx);

       case {'tin-pr'}
          cell_set={cellfiledata.cellid};

          cellhasactive=zeros(size(cellfiledata));
          goodactive=zeros(size(cellfiledata));
          cellhaspassive=zeros(size(cellfiledata));
          goodpassive=zeros(size(cellfiledata));
          for ii=1:length(cellfiledata)
             parms=dbReadData(cellfiledata(ii).rawid);
             cellmatches=find(strcmp(cellfiledata(ii).cellid,cell_set));
             if strcmpi(cellfiledata(ii).behavior,'active') &&...
                   cellfiledata(ii).isolation>=params.miniso &&...
                   isfield(parms,'ReferenceClass') && ...
                   strcmpi(parms.ReferenceClass,'torc2')
                goodactive(ii)=1;
                cellhasactive(cellmatches)=1;
             end
             if ~strcmpi(cellfiledata(ii).behavior,'active') && ...
                   cellfiledata(ii).isolation>=params.miniso
                goodpassive(ii)=1;
                cellhaspassive(cellmatches)=1;
             end
          end
          a1=min(find(goodactive));
          p1=find(goodpassive(1:a1));
          if ~isempty(p1), goodpassive(1:(p1(end)-1))=0; end
          a2=max(find(goodactive));
          p2=find(goodpassive((a2+1):end))+a2;
          if ~isempty(p2), goodpassive((p2(1)+1):end)=0; end

          keepidx=find(cellhaspassive & cellhasactive & ...
             (goodactive | goodpassive));
          cellids=unique({cellfiledata(keepidx).cellid});
          cellfileids=cellfileids(keepidx);
          cellfiledata=cellfiledata(keepidx);


       case {'ptd-pr'},
          cell_set={cellfiledata.cellid};

          if ismember(batchid, [295, 317])
             [ICcellids,rawids]=SleeICcells();

             testcellids={cellfiledata.cellid};
             testrawids=cat(2,cellfiledata.rawid);
             keepidx_slee=find(ismember(testrawids,rawids) & ...
                ismember(testcellids,ICcellids));
             B = cellfun(@(x) strcmpi(x(1:3),'mgc') | strcmpi(x(1:3),'btn'),cell_set);
             keepidx_saderi = find(~B);

             if batchid==295
                % slee only
                keepidx = keepidx_slee;
             else
                % slee and saderi PTD/TIN data
                keepidx = union(keepidx_slee, keepidx_saderi);
             end
             cellfileids=cellfileids(keepidx(:));
             cellfiledata=cellfiledata(keepidx(:));
             cell_set=cell_set(keepidx);

          elseif ismember(batchid, [311, 312])
             % 311=on BF
             % 312=off BF

             find_valid_dms_cells;
             cfd_count=0;
             for jj=1:length(celldata),
                for ii=1:length(celldata(jj).filetype),
                   if celldata(jj).filetype(ii),
                      if cfd_count==0
                         cfd=celldata(jj).cellfiledata(ii);
                      else
                         cfd(length(cfd)+1)=celldata(jj).cellfiledata(ii);
                      end
                      cfd_count=cfd_count+1;
                   end
                end
             end

             % screen for on vs. off bf, if specified
             tardist = [celldata.tardist];
             if batchid==311,
                valid_cellids={celldata(abs(tardist)<0.5).cellid};
             elseif batchid==312,
                valid_cellids={celldata(abs(tardist)>=0.5).cellid};
             else
                valid_cellids = unique({celldata.cellid});
             end

             % list of all matching rawids
             valid_rawids = [cfd.rawid];

             testcellids={cellfiledata.cellid};
             testrawids=cat(2,cellfiledata.rawid);

             keepidx=find(ismember(testrawids, valid_rawids) & ...
                ismember(testcellids, valid_cellids));
             cellfileids=cellfileids(keepidx(:));
             cellfiledata=cellfiledata(keepidx(:));
             cell_set=cell_set(keepidx);

          else
             error('Should this execute?');
             [ICcellids,rawids]=SleeICcells();
             disp('excluding SleeIC');
             testcellids={cellfiledata.cellid};
             testrawids=cat(2,cellfiledata.rawid);
             keepidx=find(~ismember(testrawids,rawids) & ...
                ~ismember(testcellids,ICcellids));
             cellfileids=cellfileids(keepidx(:));
             cellfiledata=cellfiledata(keepidx(:));
             cell_set=cell_set(keepidx);

          end

          % screen for at least one active and one valid passive
          cellhasactive=zeros(size(cellfiledata));
          goodactive=zeros(size(cellfiledata));
          cellhaspassive=zeros(size(cellfiledata));
          goodpassive=zeros(size(cellfiledata));
          for ii=1:length(cellfiledata),
             parms=dbReadData(cellfiledata(ii).rawid);
             cellmatches=find(strcmp(cellfiledata(ii).cellid,cell_set));
             if strcmpi(cellfiledata(ii).behavior,'active') &&...
                   isfield(parms,'ReferenceClass') &&...
                   (strcmpi(parms.ReferenceClass,'torc') ||...
                    strcmpi(parms.ReferenceClass,'torc2')) &&...
                   isfield(parms,'TargetClass') &&...
                   (~isfield(parms,'TrialObjectClass') ||...
                   (strcmpi(parms.TrialObjectClass,'MultiRefTar') &&...
                   strcmpi(parms.BehaveObjectClass,'RewardTargetLBHB'))),
                goodactive(ii)=1;
                cellhasactive(cellmatches)=1;
             end
             if ~strcmpi(cellfiledata(ii).behavior,'active') && ...
                   isfield(parms,'ReferenceClass') &&...
                   (strcmpi(parms.ReferenceClass,'torc') ||...
                    strcmpi(parms.ReferenceClass,'torc2')),
                goodpassive(ii)=1;
                cellhaspassive(cellmatches)=1;
             end
          end
          a1=min(find(goodactive));
          p1=find(goodpassive(1:a1));
          if ~isempty(p1), goodpassive(1:(p1(end)-1))=0; end
          a2=max(find(goodactive));
          p2=find(goodpassive((a2+1):end))+a2;
          if ~isempty(p2), goodpassive((p2(1)+1):end)=0; end

          keepidx=find(cellhaspassive & cellhasactive & ...
             (goodactive | goodpassive));
          cellids=unique({cellfiledata(keepidx).cellid});
          cellfileids=cellfileids(keepidx);
          cellfiledata=cellfiledata(keepidx);

    end

    if params.activeonly,
        % require at least two active files if activeonly==1
        uniquecellids=cellids;
        allcellids={cellfiledata.cellid};

        keepcells=zeros(size(uniquecellids));
        keepfiles=zeros(size(cellfiledata));
        for ii=1:length(uniquecellids),
            ff=find(strcmp(uniquecellids{ii},allcellids));
            if length(ff)>1,
                keepfiles(ff)=1;
                keepcells(ii)=1;
            end
        end
        cellids=cellids(find(keepcells));
        cellfiledata=cellfiledata(find(keepfiles));
        cellfileids=cellfileids(find(keepfiles));
    end
end


if isempty(cellfiledata),
   %keyboard
end
