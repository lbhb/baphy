function RefTarO = InterleaveSounds(varargin)
% ReferenceTarget Creates a reference-target base class. All the experiments in this
% paradigm inherit from this class (including passive experiments). 
% The properties of referenceTarget objects are:
%   descriptor: The name of experiment
%   ReferenceClass: Class(es) of reference
%   TargetClass: Class(es) of Target
%   NumberOfTrials : number of trials in each experiment
%   NumberOfReferencesPerTrial : numberf of of references in each trial.
%   NumberOfTargetsPerTrial : number of targets in each trial. Set to zero for
%       passive
%   RefereneMaxIndex: maximum possible index for reference object. 
%   TargetMaxIndex: maximum possible index for target object.
%   ReferenceIndex: is an array of arrays. ReferenceIndex is a one-by-NumberOfTrials array, 
%       with each element being an array of
%       one-by-NumberOfReferencesPerTrial indication the index of
%       References in that trial
%   TargetIndex: is an array of arrays. TargetIndex is a
%       one-by-NumberOfTrials array, whit each element being an array of
%       one-by-NumberOfTargetsPerTrial indicating the index of Targets in that
%       trial
%   ReferenceMaxIndex: maximum index for reference
%   TargetMaxIndex: maximum index for targets
%   ShamPercentage: percentage of sham trials
%   OnsetGap: is the gap before individual sounds(objects) in the trial
%   DelayGap: is the gap after individual sounds(objects) in the trial
% Methods of referenceTarget objects are:
%   generateRandomIndex

% Nima Mesgarani, Oct 2005

switch nargin
case 0
    % if no input arguments, create a default object
    RefTarO.descriptor = 'InterleaveSounds';
    RefTarO.ReferenceClass = 'none';
    RefTarO.ReferenceHandle = [];
    RefTarO.TargetClass = 'none';
    RefTarO.TargetHandle = [];
    RefTarO.SamplingRate = 40000;
    RefTarO.OveralldB = 65;
    RefTarO.RelativeTarRefdB = 0;
    RefTarO.NumberOfTrials = 30;  % In memory of Torcs!
    RefTarO.NumberOfRefPerTrial = []; 
    RefTarO.NumberOfTarPerTrial = 1; % default 
    RefTarO.ReferenceMaxIndex = 0;
    RefTarO.TargetMaxIndex = 0;
    RefTarO.StimIndices = '[]'; 
%    RefTarO.ShamPercentage = 0;
%    RefTarO.NoPreStimForFirstRef = 0; % if 1, the first reference will not 
        %have a prestim silence. Good for extended shock for example. 
    RefTarO.NumOfEvPerStim = 3;  % how many stim each event produces??
    % having the two following fields is enough, we actually do not need
    % NumOfEvPerStim anymore, but for backward compatibility we keep it!
    RefTarO.NumOfEvPerRef = 3;  % how many stim each reference produces??
    RefTarO.NumOfEvPerTar = 3;  % how many stim each Target produces??
    RefTarO.RunClass = '[]';
    RefTarO.UserDefinableFields = {'OveralldB','edit',65,...
       'RelativeTarRefdB','edit',0,...
       'RefCountPerRep','edit',1,...
       'TarCountPerRep','edit',1,...
       'SamplingRate','edit',40000,...
       'ConsecutiveTrials','edit',[1 1]};
    RefTarO.RefCountPerRep = 1; %new
    RefTarO.TarCountPerRep = 1; %new
    RefTarO.ConsecutiveTrials = [1 1];%new, the number of times to consecutively present stimuli from a given class [ref,tar]. For  [n,m] draws n refs randomly from the full set, then m targets, then n refs, etc.
    RefTarO = class(RefTarO,'InterleaveSounds');
    RefTarO = ObjUpdate(RefTarO);
otherwise 
    error('Wrong number of input arguments');
end
