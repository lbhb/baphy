function o = ObjUpdate (o)
ref = get(o,'ReferenceHandle');
tar = get(o,'TargetHandle');
SR=get(o,'SamplingRate');

if ~isempty(ref)
    ref = ObjUpdate(ref);
    o = set(o,'ReferenceHandle',ref);
    o = set(o,'ReferenceClass',class(ref));
    o = set(o,'ReferenceMaxIndex',get(ref,'MaxIndex'));
    [w,e] = waveform(ref,1);
    o = set(o,'NumOfEvPerStim',length(e));
    o = set(o,'NumOfEvPerRef',length(e));
    SR=max(SR,get(ref,'SamplingRate'));
end

if ~isempty(tar)
    tar = ObjUpdate(tar);
    o = set(o,'TargetHandle',tar);
    o = set(o,'TargetClass',class(tar));
    o = set(o,'TargetMaxIndex',get(tar,'MaxIndex'));
    [w,e] = waveform(tar,1);
    o = set(o,'NumOfEvPerTar',length(e));
    if isfield(get(tar),'NumOfEvPerTar')
        o = set(o,'NumOfEvPerTar',get(tar,'NumOfEvPerTar'));
    end
    SR=max(SR,get(tar,'SamplingRate'));
else
    o = set(o,'TargetClass','None');
end

% svd moved runclass functionality to a stand-alone command in the Config
% directory so that different trial objects can call the same function.
RunClass1 = RunClassTable(ref,[]);
RunClass2 = RunClassTable(tar,[]);
o = set(o,'RunClass', [RunClass1 '_' RunClass2]);

% SVD update sampling rates of sound objects!!!!
ref=set(ref,'SamplingRate',SR);
tar=set(tar,'SamplingRate',SR);
o = set(o,'ReferenceHandle',ref);
o = set(o,'TargetHandle',tar);
