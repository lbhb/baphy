function [exptparams] = RandomizeSequence (o, exptparams, globalparams, RepIndex, RepOrTrial)
% Function Trial Sequence is one of the methods of class ReferenceTarget
% This function is responsible for producing the random sequence of
% references, targets, trials and etc. 

% Nima Mesgarani, October 2005
if nargin<4, RepOrTrial = 0;end   % default is its a trial call
if nargin<3, RepIndex = 1;end
% ReferenceTarget is not an adaptive learning, so we don't change anything
% for each trial, we do it once for the entire repetition. So, if its
% trial, return:
if RepOrTrial == 0, return; end
% read the trial parameters
par = get(o);

%get relevant parameters
% ReferenceMaxIndex = par.ReferenceMaxIndex;
% TargetMaxIndex = par.TargetMaxIndex;
% RefCountPerRep=par.RefCountPerRep;
% TarCountPerRep=par.TarCountPerRep;
% % generate ReferenceIndices, cell array, with matrix indicating the set of sounds to play on each trial, eg,
% % ReferenceIndices={[0 1], [0 2], [1 1;1 2], [1 30]};
% % ReferenceIndices=shuffle(ReferenceIndices);
% % 
% % o=set(o,'ReferenceIndices',ReferenceIndices);
% % exptparams.TrialObject=o;
% % 
% % return
% 
% 
% % if strcmpi(exptparams.
%  NumRef = par.NumberOfRefPerTrial(:);
%  IsLookup = 0;
% % % for now, lets assume Lookup table:
% % 
% temp = [];
% ReferenceMaxIndex = par.ReferenceMaxIndex;
% % here, we try to specify the real number of references per trial, and
% % determine how many trials are needed to cover all the references. If its
% % a detect case, its easy. Add from NumRef to trials until the sum of
% % references becomes equal to maxindex. 
% % in discrim, if its not sham, the number of references per trial is added
% % by one because one reference goes into target.
% while sum(temp) < ReferenceMaxIndex      % while not all the references are covered
%     if isempty(NumRef)  % if not and if NumRef is empty just finish it
%         temp = [temp ReferenceMaxIndex-sum(temp)]; % temp holds the number of references in each trial
% %     elseif sum(temp)+NumRef(1)+(IsDiscrim & ~IsSham(1)) <= ReferenceMaxIndex % can we add NumRef(1)?
%     elseif sum(temp)+NumRef(1) <= ReferenceMaxIndex % can we add NumRef(1)?
%         temp = [temp NumRef(1)]; % if so, add it and circle NumRef
%         NumRef = circshift (NumRef, -1);
%     else
%         NumRef(1)=[]; % otherwise remove this number from NumRef
%     end
% end
% if ~IsLookup  % if its a lookup table, dont randomize, if not randomize them
%     RefNumTemp = temp(randperm(length(temp))); % randomized number of references in each trial
% else
%     RefNumTemp = temp;
% end
% % Lets specify which trials are sham:
% TotalTrials = length(RefNumTemp);
% if (get(o,'NumberOfTarPerTrial') ~= 0) && (~strcmpi(get(o,'TargetClass'),'None')) 
%     if ~IsLookup
%         NotShamNumber = floor((100-par.ShamPercentage) * TotalTrials / 100); % how many shams do we have??
%         allTrials  = randperm(TotalTrials); %
%         NotShamTrials = allTrials (1:NotShamNumber);
%     else
%         NotShamTrials = find(RefNumTemp < max(LookupTable));
%     end
%     TargetIndex = cell(1,length(RefNumTemp));
%     for cnt1=1:length(NotShamTrials)
%         TargetIndex{NotShamTrials(cnt1)} = 1+floor(rand(1)*par.TargetMaxIndex);
%     end
% else
%     TargetIndex = [];
% end
% % at this point, we know how many references in each trial we have. If its
% % a detect case, we just need to choose randomly from references and put
% % them in the trial. But in discrim case, we put one index in the target
% % also, if its not a sham. 
% % Now generate random sequences for each trial
% RandIndex = randperm(par.ReferenceMaxIndex);
% for cnt1=1:length(RefNumTemp)
%     RefTrialIndex {cnt1} = RandIndex (1:RefNumTemp(cnt1));
%     RandIndex (1:RefNumTemp(cnt1)) = [];
% end

for i=1:par.ReferenceMaxIndex
    refs{i}=[1 i];
end
for i=1:par.TargetMaxIndex
    tars{i}=[2 i];
end
%tars=[2*ones(par.TargetMaxIndex,1),(1:par.TargetMaxIndex)'];
refs=repmat(refs,1,par.RefCountPerRep);
tars=repmat(tars,1,par.TarCountPerRep);
StimIndices=[refs,tars];

if(all(par.ConsecutiveTrials==1))
    rand_order=randperm(size(StimIndices,2));
else
    refi=1:length(refs);
    Nref=length(refs);
    tari=1:length(tars);
    i=1;
    while ~isempty(refi) && ~isempty(tari)
        n=min(par.ConsecutiveTrials(1),length(refi));
        ro=randperm(length(refi),n);
        rand_order(i:i+n-1)=refi(ro);
        refi(ro)=[];
        i=i+n;
        
        n=min(par.ConsecutiveTrials(2),length(tari));
        ro=randperm(length(tari),n);
        rand_order(i:i+n-1)=tari(ro)+Nref;
        tari(ro)=[];
        i=i+n;
    end
end

StimIndices=StimIndices(rand_order);
    
o = set(o,'StimIndices',StimIndices);
o = set(o,'NumberOfTrials',length(StimIndices));
% the following line eliminates the first prestim silence.
% if get(exptparams.BehaveObject,'ExtendedShock')
%     o = set(o,'NoPreStimForFirstRef',1);
% end
exptparams.TrialObject = o;
if exist('LastLookup','var')
    LastLookup = LastLookup+TotalTrials;
    tt = what(class(o));
    save ([tt.path filesep 'LastLookup.mat'],'LastLookup');
end
