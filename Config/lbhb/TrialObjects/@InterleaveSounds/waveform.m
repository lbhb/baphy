function [TrialSound, events , o] = waveform (o,TrialIndex,TrialTotal)
% This function generates the actual waveform for each trial from
% parameters specified in the fields of object o. This is a generic script
% that works for all passive cases, all active cases that use a standard
% SoundObject (e.g. tone). You can overload it by writing your own waveform.m
% script and copying it in your object's folder.

% Nima Mesgarani, October 2005

par = get(o); % get the parameters of the trial object
TrialSamplingRate=par.SamplingRate;
RefObject = par.ReferenceHandle; % get the reference handle
if get(RefObject, 'SamplingRate')~=TrialSamplingRate,
  RefObject = set(RefObject, 'SamplingRate', TrialSamplingRate);
end
TarObject = par.TargetHandle; % get the reference handle
if get(TarObject, 'SamplingRate')~=TrialSamplingRate,
  TarObject = set(TarObject, 'SamplingRate', TrialSamplingRate);
end
if any(strcmp(fieldnames(RefObject),'OverrideAutoScale')),
  OverrideAutoScale=get(RefObject,'OverrideAutoScale');
else
  OverrideAutoScale=0;
end

%
StimIndex = par.StimIndices{TrialIndex}; % get the index of reference sounds for current trial

StimThisTrial=size(par.StimIndices,1);

% maybe allow Ref and Tar objects to have different levels. not supported yet
RelativeTarRefdB = get(o,'RelativeTarRefdB');

RelativeTarRefScale = (10^(RelativeTarRefdB/20));
if RelativeTarRefdB,
  error('differential Ref and Tar levels not supported');
end

TrialSound=[];
LastEvent = 0;
events=[];

for cnt1 = 1:StimThisTrial  % go through all the reference sounds in the trial
   stimcategory=StimIndex(cnt1,1); % ref or tar?
   stimindex=StimIndex(cnt1,2);

   if stimcategory==1,
     [w, ev] = waveform(RefObject, stimindex); % 1 means its reference
   else
     [w, ev] = waveform(TarObject, stimindex); % 2 means its target
   end
   if size(w,2)>size(w,1),
      w=w';
   end
   % now, add reference to the note, correct the time stamp in respect to
   % last event, and add Trial
   for cnt2 = 1:length(ev)
       ev(cnt2).Note = [ev(cnt2).Note ' , Reference' num2str(stimcategory)];
       ev(cnt2).StartTime = ev(cnt2).StartTime + LastEvent;
       ev(cnt2).StopTime = ev(cnt2).StopTime + LastEvent;
       ev(cnt2).Trial = TrialIndex;
   end
   LastEvent = ev(end).StopTime;
   TrialSound = [TrialSound ;w];
   events = [events ev];
end

if ~OverrideAutoScale,
   TrialSound = 5 * TrialSound / max(abs(TrialSound(:)));
end

if OverrideAutoScale && max(abs(TrialSound(:)))>5,
  warning('max(abs(TrialSound))>5!  May having clipping problem!')
end

return


Refloudness = get(RefObject,'Loudness');
if isobject(TarObject)
    Tarloudness = get(TarObject,'Loudness');
else
    Tarloudness = 0;
end

loudness = max(Refloudness,Tarloudness);
if loudness(min(RefTrialIndex(1),length(loudness)))>0
    o = set(o,'OveralldB', loudness(min(RefTrialIndex(1),length(loudness))));    
end
o = set(o, 'SamplingRate', TrialSamplingRate);
