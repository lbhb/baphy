function RefTarO = RefTarOIS(varargin)
% RefTarOIS 
%
% RefTar for intrinsic imaging test
%
% Hacked from ReferenceTarget
%
% SVD 2017-09-20

switch nargin
case 0
    % if no input arguments, create a default object
    RefTarO.descriptor = 'none';
    RefTarO.ReferenceClass = 'none';
    RefTarO.ReferenceHandle = [];
    RefTarO.TargetClass = 'none';
    RefTarO.TargetHandle = [];
    RefTarO.SamplingRate = 40000;
    RefTarO.OveralldB = 65;
    RefTarO.RelativeTarRefdB = 0;
    RefTarO.NumberOfTrials = 30;  % In memory of Torcs!
    RefTarO.NumberOfRefPerTrial = []; 
    RefTarO.NumberOfTarPerTrial = 1; % default 
    RefTarO.ReferenceMaxIndex = 0;
    RefTarO.TargetMaxIndex = 0;
    RefTarO.ReferenceIndices = '[]'; 
    RefTarO.ReferenceIndexLog = {}; 
    RefTarO.TargetIndices = '[]';
    RefTarO.ShamPercentage = 0;
    RefTarO.ITI=0; % intertrial interval
    RefTarO.SilentTrials=1; % number of silent trials to insert
    RefTarO.ShuffleReference='No ';
    RefTarO.NoPreStimForFirstRef = 0; % if 1, the first reference will not 
        %have a prestim silence. Good for extended shock for example. 
    RefTarO.NumOfEvPerStim = 3;  % how many stim each event produces??
    % having the two following fields is enough, we actually do not need
    % NumOfEvPerStim anymore, but for backward compatibility we keep it!
    RefTarO.NumOfEvPerRef = 3;  % how many stim each reference produces??
    RefTarO.NumOfEvPerTar = 3;  % how many stim each Target produces??
    RefTarO.RunClass = '[]';
    RefTarO.UserDefinableFields = {'OveralldB','edit',65,...
       'RelativeTarRefdB','edit',0,...
       'SamplingRate','edit',40000,...
       'ShuffleReference','popupmenu','Yes|No ',...
       'ITI','edit',0,...
       'SilentTrials','edit',1};
    RefTarO = class(RefTarO,'RefTarOIS');
    RefTarO = ObjUpdate(RefTarO);
otherwise 
    error('Wrong number of input arguments');
end
