function [w, event, e]=waveform (o,index,IsRef, TarObject, ThisTarParams)
%
% generate waveform for ContectProbe object
%

par = get(o);

Frequencies = par.Frequencies;
Bandwidth = par.Bandwidth;
stimuli = par.stimuli;
sequences = par.sequences;

if nargin<4, TarObject=[]; end
if nargin<3, IsRef = 1; end


% checks that index does not go over max, otherwise reuses indexes
%index = mod(index, MaxIndex);
%if index == 0
%    index = MaxIndex;
%end

% save random seeds for reproducibility
saveseed=rand('seed');
savenseed=randn('seed');
rand('seed',index*20);
randn('seed',index*20);

% parses ContextProbe object parameters into SpNoise object
s = SpNoise;
su = get(s, 'UserDefinableFields');
su = su(1:3:end); % select names of parameters 
for ii = 1:length(su)
    s = set(s, su{ii}, par.(su{ii}));
end
s = set(s, 'PreStimSilence', 0);
s = set(s, 'PostStimSilence', 0);

% Duration of single stim (default 3 secs) times number of stims pertrial
sound_samples = round(par.Duration * par.SamplingRate);
N= sound_samples * size(sequences,2);
w=zeros(N,1);
e=zeros(N,length(par.Frequencies));

sequence = sequences(index,:);

% initialize the event struct with the right number of elements
stim_num = length(sequence);  
struct_size = stim_num * 3 + 3; % plus one per sequences, two pre pre and post silence
event(struct_size) = struct('Note', [], 'StartTime', [], 'StopTime', [], ...
                            'Trial', []);
current_time_sample = 0;
for ii = 1:stim_num
    current_stim_idx = sequence(ii);
    stim = stimuli(current_stim_idx);
    % sets up high and low freqs
    center_freq = Frequencies(stim.carrier_idx);
    
    lf = round(2.^(log2(center_freq) - Bandwidth./2));
    hf = round(2.^(log2(center_freq) + Bandwidth./2)); 
    s = set(s, 'LowFreq', lf);
    s = set(s, 'highFreq', hf);
    
    EnvID = stim.envelope_idx;
    
    if ~isempty(TarObject)
        if ThisTarParams.TargetChannel==1
            [tw,~,te]=waveform(s, EnvID, IsRef, TarObject, ThisTarParams);
        end
    
    else 
        [tw,~,te]=waveform(s,EnvID, IsRef);
    end
    %[tw, ~, te] = waveform(s, EnvID, IsRef,TarObject, ThisTarParams);
    
    w((1:sound_samples) + current_time_sample) = tw;  
    e((1:sound_samples) + current_time_sample, stim.carrier_idx) = te;
    
    % set SubRef names and add to event structure
    if current_stim_idx == 1
        context = 'silence';
    else
        context = stimuli(current_stim_idx-1).stim_name;
    end
    probe = stim.stim_name;

    note = [' probe:', probe, '_context:', context]; 
    %note = probe;
    
    prefixes = {'SubPreStimSilence , '; 'SubStim , '; 'SubPostStimSilence , '};
    %prefixes = {'PreStimSilence , '; 'Stim , '; 'PostStimSilence , '}
     
    start_time = (current_time_sample/par.SamplingRate) + par.PreStimSilence;
    stop_time = start_time + par.Duration;   
    
    event((ii-1)*3 + 4) = struct('Note', [prefixes{1} note],...
                         'StartTime', start_time, 'StopTime', start_time,...
                         'Trial', []);
    event((ii-1)*3 + 5) = struct('Note', [prefixes{2} note],...
                          'StartTime', start_time, 'StopTime', stop_time,...
                         'Trial', []);  
    event((ii-1)*3 + 6) = struct('Note', [prefixes{3} note],...
                         'StartTime', stop_time, 'StopTime', stop_time,...
                         'Trial', []);  
                   
    current_time_sample = current_time_sample + sound_samples;
end

% Normalizes to 5 ~ 80 db 
w = 5 ./ max(abs(w(:))) .* w;
e = e ./ max(abs(e(:)));

% Now, put it in the silence:
w = [zeros(par.PreStimSilence * par.SamplingRate, 1);...
     w(:);...
     zeros(par.PostStimSilence * par.SamplingRate, 1)];
e = [zeros(par.PreStimSilence * par.SamplingRate, length(par.Frequencies));...
     e;...
     zeros(par.PostStimSilence * par.SamplingRate, length(par.Frequencies))]; 

% generate the event structure:
event(1) = struct('Note',['PreStimSilence , ' par.Names{index}],...
               'StartTime',0,...
               'StopTime',par.PreStimSilence,...
               'Trial',[]);
event(2) = struct('Note',['Stim , ' par.Names{index}],...
                  'StartTime',par.PreStimSilence,...
                  'StopTime', par.PreStimSilence + par.Duration * stim_num,...
                  'Trial',[]);
event(3) = struct('Note',['PostStimSilence , ' par.Names{index}],...
                  'StartTime',par.PreStimSilence + par.Duration * stim_num, ...
                  'StopTime',par.PreStimSilence+ par.Duration * stim_num +...
                  par.PostStimSilence,...
                  'Trial',[]);

% return random seed to previous state
rand('seed',saveseed);
randn('seed',savenseed);
end
