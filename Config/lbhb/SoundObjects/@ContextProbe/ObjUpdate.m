function o = ObjUpdate (o)
%
% piggyback on top of SpNoise object to get waveform

par = get(o);

% parses parameters from ContextProbe to SpNoise
s = SpNoise;
spar = get(s);
su = get(s, 'UserDefinableFields');
su = su(1:3:end); % select names of parameters 
for ii = 1:length(su)
    s = set(s, su{ii}, par.(su{ii}));
end
spar = get(s);

saveseed=rand('seed');
rand('seed',par.Subsets*20);
    
% checks for pilot script for backwards compatibility
if strcmp('Yes', strtrim(par.Pilot))
    o = set(o, 'BaseSound', 'FerretVocal');
    o = set(o, 'Subsets', 5); 
    o = set(o, 'EnvelopeIndexes', [1, 2]);
    if length(par.Frequencies) >= 2
        o = set(o, 'Frequencies', par.Frequencies(1, 1:2));
    else
        error('Frequencies sould be to values for Pilot mode')
    end
elseif strcmp('No', strtrim(par.Pilot))
    
else
    error('pilot should be yes or no')
end

% creates a struct with the proper combinations of vocalization and
% carrier
envelope_count = length(par.EnvelopeIndexes);
carriers = par.Frequencies;
stimuli_count = envelope_count * length(carriers);
stimuli(stimuli_count) = struct('envelope_idx', [], 'carrier_idx', [],...
                                'stim_name', []);

% generates names for individual sounds (contexts or probes)
counter = 1;
Names{stimuli_count} = '';
for ee = 1:envelope_count
    env_idx = par.EnvelopeIndexes(ee);
    for cc = 1:length(carriers)
        stimuli(counter).envelope_idx = env_idx;
        stimuli(counter).carrier_idx = cc;
        
        % creates unique name for stimulus: soundNumber + CenterFrequency +
        % BNB + EnvelopeName
        unique_name = sprintf('sound%03d+%d+%s', counter,...
                      carriers(cc), spar.Names{env_idx});
        Names{counter} = unique_name;
        stimuli(counter).stim_name = unique_name;
        counter = counter + 1;
    end
end

% set a sequence of the differnt stimuli previosuly generated.
if strcmp('Yes', strtrim(par.Pilot))
    sequences = [1 3 2 4 4;
                 3 4 1 1 2;
                 4 2 3 3 1;
                 2 2 1 4 3];
elseif strcmp('No', strtrim(par.Pilot))
    % TODO, implement the cyclic code of Azari and Zador.
    error('not yet implemented')
else
    error('pilot should be yes or no')
end

% generate names for the trials i.e. sequences
Names{size(sequences,1)} = '';
for ii=1:size(sequences,1)
    Names{ii} = sprintf('sequence%03d: %s', ii, num2str(sequences(ii, :)));
end


% Define MaxIndex
MaxIndex = length(par.Frequencies) .* length(par.EnvelopeIndexes);

%o = set(o, 'mean', env_mean);
o = set(o, 'Names',Names);
o = set(o, 'sequences', sequences);
o = set(o, 'stimuli', stimuli);
o = set(o,'MaxIndex',MaxIndex);
o = set(o,'SamplingRateEnv', spar.SamplingRate);
o = set(o, 'Frequencies', carriers);
    
% return random seed to previous state
rand('seed',saveseed);

end