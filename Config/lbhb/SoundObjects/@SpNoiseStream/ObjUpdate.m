function o = ObjUpdate (o);
%
% piggyback on top of speech object to get waveform

params=get(o);

s=SpNoise;
u=get(s,'UserDefinableFields');
u=u(1:3:end);
u=setdiff(u,{'LowFreq','HighFreq'});

for ii=1:length(u),
    s=set(s,u{ii},params.(u{ii}));
end

switch o.Sub_Subset
    case 1
        SpId=[(1:10)' zeros(10,1);
            zeros(10,1) (1:10)';
            ([1:5 11:15])' ([1:5 11:15])';
            ([1:5 16:20])' ([1:5 16:20])';
            ([1:5 21:25])' ([2:5 1 22:25 21])';
            ([1:5 26:30])' ([2:5 1 27:30 26])';
            ];
        dbScale=[zeros(10,2);
            zeros(10,2);
            zeros(10,1) zeros(10,1);
            zeros(10,1) -10*ones(10,1);
            zeros(10,1) zeros(10,1);
            zeros(10,1) -10*ones(10,1);
            ];
        repinds=[0 10 20 40];
    case 2
        SpId=[(1:10)' zeros(10,1);
            zeros(10,1) (1:10)';
            ];
        dbScale=[zeros(10,2);
            zeros(10,2);
            ];
        repinds=[0 10 20 30];
        for i=1:length(o.RelAttenuatedB)
            dbScale=[dbScale;
                zeros(10,1) -1*o.RelAttenuatedB(i)*ones(10,1);
                zeros(10,1) -1*o.RelAttenuatedB(i)*ones(10,1);
                ];
            if(mod(i,2)==1)
                ssi=[11 21];%second start inds
            else
                ssi=[16 26];
            end
            SpId=[SpId;
                ([1:5 ssi(1)+(0:4)])' ([1:5 ssi(1)+(0:4)])';
                ([1:5 ssi(2)+(0:4)])' ([2 1 4 5 3 ssi(2)+([1:4 0])])';
                ];
        end
end
RepIdx=params.RepIdx;
if length(RepIdx)>=2 && RepIdx(1)>0 && RepIdx(2)>1,
    RepSet=repmat(1:RepIdx(1),length(repinds),1)+repmat(repinds',1,RepIdx(1));
    RepSet=RepSet(:)';
    %RepSet=[1:RepIdx(1) (1:RepIdx(1))+10 (1:RepIdx(1))+20 (1:RepIdx(1))+40];
    RepCount=RepIdx(2)-1;
    idrep=repmat(SpId(RepSet,:),[RepCount 1]);
    dbrep=repmat(dbScale(RepSet,:),[RepCount 1]);
end
if length(o.RemoveIdx)>=0 && o.RemoveIdx>0
    rmi=o.RemoveIdx:10;
    Ntypes=size(SpId,1)/10;
    rmi=repmat(rmi',1,Ntypes)+repmat((0:10:(Ntypes-1)*10),length(rmi),1);
    rmi=rmi(:)';
    SpId(rmi,:)=[];
    dbScale(rmi,:)=[];
end
if length(RepIdx)>=2 && RepIdx(1)>0 && RepIdx(2)>1,    
    SpId=[SpId; idrep];
    dbScale=[dbScale; dbrep];
end
    
MaxIndex=size(SpId,1);
StreamCount=size(SpId,2);

NameSet=cell(MaxIndex,StreamCount);
for ff=1:StreamCount,
    s=set(s,'LowFreq',params.LowFreq(ff));
    s=set(s,'HighFreq',params.HighFreq(ff));
    n=get(s,'Names');
    for ii=1:MaxIndex,
        if SpId(ii,ff)
            NameSet{ii,ff}=[n{SpId(ii,ff)} ':' num2str(dbScale(ii,ff))];
        else
            NameSet{ii,ff}='NULL';
        end
    end
end
Names=cell(1,MaxIndex);
for ii=1:MaxIndex
    Names{ii}=[NameSet{ii,1} '+' NameSet{ii,2}];
end

o=set(o,'SpId',SpId);
o=set(o,'dbScale',dbScale);
o=set(o,'MaxIndex',MaxIndex);
o=set(o,'StreamCount',StreamCount);
o=set(o,'Names',Names);
SamplingRate=get(s,'SamplingRate');
if SamplingRate>params.SamplingRate,
    o=set(o,'SamplingRate',SamplingRate);
end

return

    
    
LowFreq = get(o,'LowFreq');
HighFreq = get(o,'HighFreq');
bmax=min(length(LowFreq),length(HighFreq));
SamplingRate = get(o,'SamplingRate');
TonesPerOctave = get(o,'TonesPerOctave');
TonesPerBurst=round(log2(HighFreq(1:bmax)./LowFreq(1:bmax)).*TonesPerOctave);
BaseSound=strtrim(get(o,'BaseSound'));
Subsets = get(o,'Subsets');
SetSizeMult = get(o,'SetSizeMult');
CoherentFrac = get(o,'CoherentFrac');
SingleBandFrac = get(o,'SingleBandFrac');
ShuffleOnset=get(o,'ShuffleOnset');  % if 1, randomly rotate stimulus waveforms in time
RepIdx=get(o,'RepIdx');
Duration=get(o,'Duration');  % if 1, randomly rotate stimulus waveforms in time

if ~TonesPerOctave,
    UseBPNoise=1;
else
    UseBPNoise=0;
end

object_spec = what(class(o));
envpath = [object_spec(1).path filesep 'envelope' filesep];

% eg "Speech.subset1.mat"   and "Speech1"
EnvFileName=[envpath BaseSound '.subset',num2str(Subsets),'.mat'];
EnvVarName=[BaseSound,num2str(Subsets)];
load(EnvFileName);

bandcount=min(length(HighFreq),length(LowFreq));
if bandcount>1,
    % force same carrier signal each time!!!!
    saveseed=rand('seed');
    rand('seed',Subsets*20);
    MaxIndex=round(Env.MaxIndex.*SetSizeMult);
    idxset=repmat((1:Env.MaxIndex)',[ceil(SetSizeMult) bandcount]);
    ShuffledOnsetTimes=zeros(MaxIndex,bandcount);
    coherentcount=round(MaxIndex.*CoherentFrac);
    incoherentset=1:(size(idxset,1)-coherentcount);
    for jj=2:bandcount,
        kk=0;
        % slight kludge, keep shuffling until there are no matches between
        % index values in columns 1 and column jj, except where intended.
        while sum(idxset(incoherentset,1)==idxset(incoherentset,jj))>0 && kk<20,
            ff=find(idxset(incoherentset,1)==idxset(incoherentset,jj));
            if length(ff)==1;
                ff=union(ff,[1;2]);
            end
            idxset(ff,jj)=shuffle(idxset(ff,jj));
            kk=kk+1;
        end
    end
    MaxIncoherent=MaxIndex-coherentcount;
    idxset=idxset([1:MaxIncoherent (end-coherentcount+1):end],:);
    
    % shift signals in the different bands randomly to avoid correlations
    % from average cadence of sentences
    if ShuffleOnset,
        if ShuffleOnset==1,
            d=Duration;
        elseif ShuffleOnset==2,
            % don't make it depend on the stimulus length!!!!
            % fixes bug in MultiRefTar that changes reference Duration
            d=3;
        end
        ShuffledOnsetTimes=floor(rand(size(ShuffledOnsetTimes))*d*2)./2;
        ShuffledOnsetTimes(MaxIncoherent:end,2:end)=...
            repmat(ShuffledOnsetTimes(MaxIncoherent:end,1),[1 bandcount-1]);
    end

    % set some ids to zero so that only one band has sound
    if SingleBandFrac>0,
       SingleBandCount=round(MaxIndex.*SingleBandFrac./bandcount);
       SingleBandIdx=repmat(1:SingleBandCount,[bandcount+1 1]);
       LeftoverIdx=SingleBandCount+(1:(MaxIndex-length(SingleBandIdx(:))));
       idxset=idxset([SingleBandIdx(:);LeftoverIdx(:)],:);
       ShuffledOnsetTimes=ShuffledOnsetTimes([SingleBandIdx(:);LeftoverIdx(:)],:);
       for kk=1:SingleBandCount,
          for jj=1:bandcount,
             idxset((kk-1)*(bandcount+1)+jj+1,[1:(jj-1) (jj+1):end])=0;
          end
       end
    end
    
    % return random seed to previous state
    rand('seed',saveseed);
else
    MaxIndex=Env.MaxIndex;
    idxset=(1:MaxIndex)';
    ShuffledOnsetTimes=zeros(size(idxset));
end

if length(RepIdx)>=2 && RepIdx(1)>0 && RepIdx(2)>1,
    RepSet=1:RepIdx(1);
    RepCount=RepIdx(2);
    idxset=cat(1,repmat(idxset(RepSet,:),[RepCount 1]),...
               idxset((RepIdx(1)+1):end,:));
    ShuffledOnsetTimes=cat(1,repmat(ShuffledOnsetTimes(RepSet,:),[RepCount 1]),...
               ShuffledOnsetTimes((RepIdx(1)+1):end,:));
    MaxIndex=size(idxset,1);
end
    

SPNOISE_EMTX.(EnvVarName)=Env.emtx;
%o=set(o,'emtx',Env.emtx);
o=set(o,'idxset',idxset);
o=set(o,'ShuffledOnsetTimes',ShuffledOnsetTimes);
o=set(o,'MaxIndex',MaxIndex);
% removed phoneme labels to save space in parmfiles
%o=set(o,'Phonemes',Env.Phonemes);
o=set(o,'SamplingRateEnv',Env.fs);
o=set(o,'UseBPNoise',UseBPNoise);
Names=Env.Names;

Names=cell(1,MaxIndex);
for ii=1:MaxIndex,
    Names{ii}=['BNB'];
    for bb=1:bandcount,
       if idxset(ii,bb)
          Names{ii}=[Names{ii} '+' Env.Names{idxset(ii,bb)}];
       else
          Names{ii}=[Names{ii} '+null'];
       end
    end
end

o = set(o,'Names',Names);
o = set(o,'TonesPerBurst',TonesPerBurst);

if isempty(FORCESAMPLINGRATE)
    while SamplingRate<max(HighFreq)*4,
        SamplingRate=SamplingRate+50000;
        o = set(o,'SamplingRate',SamplingRate);
    end
end
