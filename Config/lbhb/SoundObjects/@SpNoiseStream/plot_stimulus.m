function plot_stimulus(o,TarObject,TrialSound,TrialSamplingRate,RefTrialIndex,TrialIndex,TarIdx,events,TrialEnv)
params=get(o);
s=SpNoise;
u=get(s,'UserDefinableFields');
u=u(1:3:end);

for ii=1:length(u),
    s=set(s,u{ii},params.(u{ii}));
end

plot_stimulus(s,TarObject,TrialSound,TrialSamplingRate,RefTrialIndex,TrialIndex,TarIdx,events,TrialEnv);