function [w,event,e]=waveform (o,index,IsRef,TarObject,ThisTarParams)
% function w=waveform(o, index,IsRef);
%
% generate waveform for SpNoise object
%
if nargin<4, TarObject=[]; end
params=get(o);
Names=params.Names;

s=SpNoise;
u=get(s,'UserDefinableFields');
u=u(1:3:end);
u=setdiff(u,{'LowFreq','HighFreq','RepIdx'});

for ii=1:length(u),
    s=set(s,u{ii},params.(u{ii}));
end

ff=1;
if params.SpId(index,ff),
    s=set(s,'LowFreq',params.LowFreq(ff));
    s=set(s,'HighFreq',params.HighFreq(ff));

    do_waveform=true;
    if ~isempty(TarObject)
        if ThisTarParams.TargetChannel==1
            [w1,~,e(:,1)]=waveform(s,params.SpId(index,ff),IsRef,TarObject,ThisTarParams);
            do_waveform=false;
        end
    end
    if do_waveform
        [w1,~,e(:,1)]=waveform(s,params.SpId(index,ff),IsRef);
    end
    level_scale=10.^(params.dbScale(index,ff)./20);
    w1=w1*level_scale;
else
    totaldur=params.PreStimSilence+params.Duration+params.PostStimSilence;
    w1=zeros(round(params.SamplingRate*(totaldur)),1);
end

ff=2;
if params.SpId(index,ff),
    s=set(s,'LowFreq',params.LowFreq(ff));
    s=set(s,'HighFreq',params.HighFreq(ff));
    do_waveform=true;
    if ~isempty(TarObject)
        if ThisTarParams.TargetChannel==2
            ThisTarParams.TargetChannel=1;
            [w2,~,e(:,2)]=waveform(s,params.SpId(index,ff),IsRef,TarObject,ThisTarParams);
            do_waveform=false;
        end
    end
    if do_waveform
        [w2,~,e(:,2)]=waveform(s,params.SpId(index,ff),IsRef);
    end
    level_scale=10.^(params.dbScale(index,ff)./20);
    w2=w2*level_scale;
else
    w2=zeros(size(w1));
    e(:,2)=0;
end

w=w1+w2;

% generate the event structure:
event = struct('Note',['PreStimSilence , ' Names{index}],...
               'StartTime',0,...
               'StopTime',params.PreStimSilence,...
               'Trial',[]);
event(2) = struct('Note',['Stim , ' Names{index}],...
                  'StartTime',params.PreStimSilence,...
                  'StopTime', params.PreStimSilence+params.Duration,...
                  'Trial',[]);
event(3) = struct('Note',['PostStimSilence , ' Names{index}],...
                  'StartTime',params.PreStimSilence+params.Duration, ...
                  'StopTime',params.PreStimSilence+params.Duration+...
                  params.PostStimSilence,...
                  'Trial',[]);
