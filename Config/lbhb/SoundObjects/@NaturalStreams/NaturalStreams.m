function o = NaturalStreams(varargin)
% Speech is a child of SoundObject class. It manages all the necessary
% routines for object speech.
%
% properties:
%   PreStimSilcence
%   PostStimSilence
%   SamplingRate
%   Loudness
%   Subsets: can be 1, 2, 3 or 4. each contains 30 different sentences from
%       Timit database. each subset is 30 sentences spoken by 30 different
%       speakers (15 male 15 female). sentences in subset 1 and 4 are three
%       seconds long, subset 2 and 3 are four seconds. Subset 4 is all the
%       same sentence: "She had your dark suit in greasy wash water all
%       year"
%   Phonemes: contains the phoneme events for the specified names.
%   Words: contains the word events for the specified names.
%   Sentences: contains the sentece events for the specified names.
%
% methods: waveform, LabelAxis, set, get
%

% Nima Mesgarani, Oct 2005

switch nargin
    case 0
        % if no input arguments, create a default object
        s = SoundObject ('NaturalStreams', 100000, 0, 0, 0, {}, 1, ...
            {'StreamA','popupmenu','Speech|Marmo',...
            'StreamASubset','edit',1,...
            'StreamARepeatInds','edit',[10 20 30 40],...
            'StreamARepeats','edit',10,...
            'StreamATarInds','edit',8,...
            'StreamASNRs','edit',10,...
            'StreamATarSlotFreq','edit',[0 0 0 0 0.3 0.3 .2 0.1 0.1],...
            'StreamB','popupmenu','Speech|Marmo',...
            'StreamBSubset','edit',1,...
            'StreamBRepeatInds','edit',[1 30 60 90],...
            'StreamBRepeats','edit',10,...
            'StreamBTarInds','edit',50,...
            'StreamBSNRs','edit',0,...
            'StreamBTarSlotFreq','edit',[0 0 0 0 0.3 0.3 .2 0.1 0.1],...
            'RelAttenuatedB','edit',[0 0],...
            'Seeds','edit',[1 2 0],...
            'SingleTokenShift','edit',[0 0],...
            'TargetStream','popupmenu','A|B',...
            'CatchStream','popupmenu','A|B|none',...
            'MinTimeBetweenTargets','edit',1.2,...
            'SplitChannels','popupmenu','Yes|No',...
            'DurationMin','edit',20,...
            'PreCueSilence','edit',3,...
            'MaxTokenLength','edit',[3 3],...
            'NtrialsPerRep','edit',[10 10 10],...
            'MatchTrialsbyCondition','popupmenu','Yes|No',...
            });
        
        o.SamplingRate=50000;
        o.PreStimSilence=0.05;
        o.StreamA='Speech';
        o.StreamASubset=1;
        o.StreamARepeatInds=[10 20 30 40];
        o.StreamARepeats=10;
        o.StreamATarInds=8;
        o.StreamASNRs=10;
        o.StreamATarSlotFreq=[0 0 0 0 0.3 0.3 .2 0.1 0.1];
        o.StreamB='Speech';
        o.StreamBSubset=1;
        o.StreamBRepeatInds=[60 70 80 90];
        o.StreamBRepeats=10;
        o.StreamBTarInds=50;
        o.StreamBSNRs=0;
        o.StreamBTarSlotFreq=[0 0 0 0 0.3 0.3 .2 0.1 0.1];
        o.RelAttenuatedB=[0 0];
        o.Seeds=[1 2 0]; %Seeds for: [A stream tokens, B stream tokens, Tbrial order]
        o.SingleTokenShift=[0 0]; %Circulaly shift the set of single tokens by this amount
        o.TargetStream='A';
        o.CatchStream='B';
        o.MinTimeBetweenTargets=1.2;
        o.SplitChannels='Yes';
        o.DurationMin=20;
        o.MaxTokenLength=[3 3];%sec Only use tokens of this length or less
        o.NtrialsPerRep=[10 10 0]; %AB, A, B
        o.MatchTrialsbyCondition='Yes';
        o.PreCueSilence=3;
        %For marmo:
        %1-30 chirp
        %31-60 phee2
        %61-90 phee3
        %91-120 Seep
        %121-132 Trill
        %133-159 Tsik
        %160-189 Tsik-Ek
        %190-219 Twitter
        
        %Hard-coded params
        o.StreamATarIdxFreq=1;
        o.StreamBTarIdxFreq=1;
        o.NumOfEvPerStim=Inf;
        o.AddsRefTarTags=true;
        o.PossibleTarTimes='Various';
        
        %Internally-changed params
        o.Duration=[]; %Not used
        o.ShuffleBlockA=[];
        o.ShuffleBlockB=[];
        o.RepASet=[];
        o.RepBSet=[];
        o.AIndexes={};
        o.AOffNs={};
        o.AOnNs=[];
        o.BIndexes={};
        o.BOffNs={};
        o.BOnNs=[];
        o.ATrialNSamples=[];
        o.BTrialNSamples=[];
        o.ATokens={};%Sounds used for the current rep
        o.BTokens={};
        o.ANames={};
        o.BNames={};
        o.Anf=[];
        o.Bnf=[];
        o.ATargetSample=[];
        o.BTargetSample=[];
        o.AMinimumTargetTime=[];
        o.BMinimumTargetTime=[];
        o.MinimumTargetTime=[];
        o.TrialSet=[];
        o.stim_order=[];
        o.MinimumTargetTime=[];
        o.UniqueTargets={};
        o.DIvars=[];
        o.TargetDistSet=[];
        
        o.DoNotSaveFields={'ATokens','BTokens'};
        o = class(o,'NaturalStreams',s);
        o = ObjUpdate (o);
    case 1
        % if single argument of class SoundObject, return it
        if isa(varargin{1},'NaturalStreams')
            o = varargin{1};
        else
            error('Wrong argument type');
        end
    otherwise
        error('Wrong number of input arguments');
end