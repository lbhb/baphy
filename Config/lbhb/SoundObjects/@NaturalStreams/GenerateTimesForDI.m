function [stimtype,stimtime,resptime,tcounter,trialnum,reftype]=GenerateTimesForDI(o,perf,w,trialref_type,trialtargetid_all,this_trial_num,trial_nums)

persistent DIvars
if this_trial_num==1
    DIvars=[];
end

if isempty(DIvars)
    vars={'stimtype','stimtime','resptime','tcounter','trialnum','reftype'};
    for i=1:length(vars)
        DIvars.(vars{i})=[];
    end
end

RelLickCutoff = w.TrainingPumpWin(1)-w.TarEarlyWin(1);
EarlyWin = w.TarEarlyWin(1,2)-w.TarTimes;

valid_refs='by_time';
%valid_refs='by_slot';
LickTimes = perf.LickTimes;
First_Lick_after_target = LickTimes(find(LickTimes > w.TarResponseWin(1),1));
if 0
    Lick_cutoff = First_Lick_after_target+10*eps;
else
    Lick_cutoff = min([w.TrainingPumpWinActual(1), First_Lick_after_target+10*eps]);
end

% LickTimes(LickTimes>=Lick_cutoff)=[];

% Dist1inds=o.TargetDistSet(trialtargetid_all{this_trial_num})==1;
% if any(Dist1inds)
%     tar_time=perf.TarTimes(Dist1inds);
%     min_tar_slot = find(o.StreamATarSlotFreq>0,1);
%     ref_times = w.RefResponseWin(w.RefStream==1,1);
%     switch valid_refs
%         case 'by_slot'
%             ref_times(1:min_tar_slot-1)=[];
%         case 'by_time'
%             ref_times(ref_times<o.AMinimumTargetTime)=[];
%     end
%     Nref=length(ref_times);
%     for i=1:length(LickTimes)
%         DIvars.stimtime=cat(1,DIvars.stimtime,ref_times,tar_time);
%         DIvars.resptime=cat(1,DIvars.resptime,ones(Nref+1,1).*LickTimes(i));
%         DIvars.stimtype=cat(1,DIvars.stimtype,zeros(Nref,1),1);
%         DIvars.reftype=cat(1,DIvars.reftype,trialref_type(this_trial_num)*ones(Nref+1,1));
%         DIvars.tcounter=cat(1,DIvars.tcounter,ones(Nref+1,1).*trialtargetid_all{this_trial_num}(Dist1inds));
%         DIvars.trialnum=cat(1,DIvars.trialnum,ones(Nref+1,1).*this_trial_num);
%     end
% end
% 
% Dist2inds=o.TargetDistSet(trialtargetid_all{this_trial_num})==2;
% if any(Dist2inds)
%     tar_time=perf.TarTimes(Dist2inds);
%     min_tar_slot = find(o.StreamBTarSlotFreq>0,1);
%     ref_times = w.RefResponseWin(w.RefStream==2,1);
%     switch valid_refs
%         case 'by_slot'
%             ref_times(1:min_tar_slot-1)=[];
%         case 'by_time'
%             ref_times(ref_times<o.BMinimumTargetTime)=[];
%     end
%     Nref=length(ref_times);
%     for i=1:length(LickTimes)
%     DIvars.stimtime=cat(1,DIvars.stimtime,ref_times,tar_time);
%     DIvars.resptime=cat(1,DIvars.resptime,ones(Nref+1,1).*LickTimes(i));
%     DIvars.stimtype=cat(1,DIvars.stimtype,zeros(Nref,1),1);
%     DIvars.reftype=cat(1,DIvars.reftype,trialref_type(this_trial_num)*ones(Nref+1,1));
%     DIvars.tcounter=cat(1,DIvars.tcounter,ones(Nref+1,1).*trialtargetid_all{this_trial_num}(Dist2inds));
%     DIvars.trialnum=cat(1,DIvars.trialnum,ones(Nref+1,1).*this_trial_num);
%     end
% end

Ndists = 3;
for j=1:Ndists
    Distinds=o.TargetDistSet(trialtargetid_all{this_trial_num})==j;
    if any(Distinds)
        tar_time=perf.TarTimes(Distinds);
        
        if j==1
            TarStr = 'A';
        elseif j==2
            TarStr = 'B';
        elseif j==3
            TarStr = o.TargetStream;
        end
        switch TarStr
            case 'A'
                rs=1;
            case 'B'
                rs=2;
            otherwise
                error('fix')
        end
        min_tar_slot = find(o.(['Stream',TarStr,'TarSlotFreq'])>0,1);
        ref_times = w.RefEarlyWin(w.RefStream==rs,1);
        switch valid_refs
            case 'by_slot'
                ref_times(1:min_tar_slot-1)=[];
            case 'by_time'
                ref_times(ref_times<o.([TarStr,'MinimumTargetTime']))=[];
        end
        ref_times(ref_times>=Lick_cutoff)=[];
        
        %Add reference stims and licks if they occured before the training pump could have if it was a target (otherwise Infs)
        Nref=length(ref_times);
        for i=1:length(ref_times)
            LickTime = LickTimes(find(LickTimes>ref_times(i)+EarlyWin,1));
            if isempty(LickTime), LickTime = Inf; end
            if LickTime-ref_times(i) > RelLickCutoff
                 LickTime = Inf; 
            end
            DIvars.stimtime=cat(1,DIvars.stimtime,ref_times(i));
            DIvars.resptime=cat(1,DIvars.resptime,LickTime);
            DIvars.stimtype=cat(1,DIvars.stimtype,0);
        end
        
        %Add target stim and lick if it occured before the training pump could have (otherwise Inf)
        if tar_time < Lick_cutoff 
            LickTime = LickTimes(find(LickTimes>tar_time+EarlyWin,1));
            if isempty(LickTime), LickTime = Inf; end
            if LickTime-tar_time > RelLickCutoff
                 LickTime = Inf; 
            end
            DIvars.stimtime=cat(1,DIvars.stimtime,tar_time);
            DIvars.resptime=cat(1,DIvars.resptime,LickTime);
            DIvars.stimtype=cat(1,DIvars.stimtype,1);
            Ntar=1;
        else
            Ntar=0;
        end
        % Add variables common to refs and tars
        DIvars.reftype=cat(1,DIvars.reftype,trialref_type(this_trial_num)*ones(Nref+Ntar,1));
        DIvars.tcounter=cat(1,DIvars.tcounter,ones(Nref+Ntar,1).*trialtargetid_all{this_trial_num}(Distinds));
        DIvars.trialnum=cat(1,DIvars.trialnum,ones(Nref+Ntar,1).*this_trial_num);
        
        %     for i=1:length(LickTimes)
        %         DIvars.stimtime=cat(1,DIvars.stimtime,ref_times(:),tar_time);  %the (:) forces ref_times to by Nx1.
        %         DIvars.resptime=cat(1,DIvars.resptime,ones(Nref+1,1).*LickTimes(i));
        %         DIvars.stimtype=cat(1,DIvars.stimtype,zeros(Nref,1),1);
        %         DIvars.reftype=cat(1,DIvars.reftype,trialref_type(this_trial_num)*ones(Nref+1,1));
        %         DIvars.tcounter=cat(1,DIvars.tcounter,ones(Nref+1,1).*trialtargetid_all{this_trial_num}(Distinds));
        %         DIvars.trialnum=cat(1,DIvars.trialnum,ones(Nref+1,1).*this_trial_num);
        %     end
    end
end

use = ismember(DIvars.trialnum,trial_nums);
stimtime=DIvars.stimtime(use);
resptime=DIvars.resptime(use);
stimtype=DIvars.stimtype(use);
reftype=DIvars.reftype(use);
tcounter=DIvars.tcounter(use);
trialnum=DIvars.trialnum(use);
