function o = FinalObjUpdate(o,TrialObject,CompletedTrials)

o.StreamATarSlotFreq=o.StreamATarSlotFreq./sum(o.StreamATarSlotFreq);
TSF_new=round(o.StreamATarSlotFreq*o.NtrialsPerRep(1))/o.NtrialsPerRep(1);
if any(TSF_new~=o.StreamATarSlotFreq)
    fprintf(['**Rounding NaturalStreams.StreamATarSlotFreq from [%s] to [%s] ',...
        'because this will ensure integer numbers of each reference length.\n'],...
        regexprep(num2str(o.StreamATarSlotFreq),' +',' '),...
        regexprep(num2str(TSF_new),' +',' '));
    o.StreamATarSlotFreq=TSF_new;
end

o.StreamATarSlotFreq=o.StreamATarSlotFreq./sum(o.StreamATarSlotFreq);
TSF_new=round(o.StreamATarSlotFreq*o.NtrialsPerRep(1))/o.NtrialsPerRep(1);
if any(TSF_new~=o.StreamATarSlotFreq)
    fprintf(['**Rounding NaturalStreams.StreamATarSlotFreq from [%s] to [%s] ',...
        'because this will ensure integer numbers of each reference length.\n'],...
        regexprep(num2str(o.StreamATarSlotFreq),' +',' '),...
        regexprep(num2str(TSF_new),' +',' '));
    o.StreamATarSlotFreq=TSF_new;
end

%% Load waveforms
StreamA=strtrim(o.StreamA);
StreamB=strtrim(o.StreamB);

object_spec = what(class(o));
wavpath = [object_spec(1).path filesep 'waveform' filesep];

WavAFileName=[wavpath StreamA '.subset',num2str(o.StreamASubset),'.mat'];
WavAVarName=[StreamA,num2str(o.StreamASubset)];
A=load(WavAFileName);

WavBFileName=[wavpath StreamB '.subset',num2str(o.StreamBSubset),'.mat'];
WavBVarName=[StreamB,num2str(o.StreamBSubset)];
B=load(WavBFileName);


%% Generate Stream A trials
saveseed=rng;
rng('default');
if o.Seeds(1)==0
    rng('shuffle'); %if seeds is 0, use a ramdom seed
else
    rng(o.Seeds(1));
end
o=fillset(o,'A',A,o.NtrialsPerRep(1),o.StreamATarSlotFreq,o.SingleTokenShift(1),o.MaxTokenLength(1));

%% Generate Stream B trials
if o.Seeds(2)==0
    rng('shuffle');
else
    rng(o.Seeds(2));
end
o.MaxTokenLength(2)=4;
o=fillset(o,'B',B,o.NtrialsPerRep(1),o.StreamBTarSlotFreq,o.SingleTokenShift(2),o.MaxTokenLength(2));

%% Swap trial pairings until all target/catches are more than MinTimeBetweenTargets apart
MinSamplesBetweenTargets = o.MinTimeBetweenTargets*o.SamplingRate;
si=1:o.NtrialsPerRep(1);
overlapping_target_trials = find(abs(o.ATargetSample - o.BTargetSample(si)) < MinSamplesBetweenTargets);
pass=0;
while ~isempty(overlapping_target_trials)
    pass=pass+1;
    if pass==50
        error('Couldn''t make targets not overlap. Tried 50 times and it still didn''t work. Try changing something.')
    end
    possible_swaps = find(abs(o.ATargetSample - o.BTargetSample(si(overlapping_target_trials(1)))) >= MinSamplesBetweenTargets & ...
        abs(o.ATargetSample(overlapping_target_trials(1)) - o.BTargetSample(si)) >= MinSamplesBetweenTargets);
    if isempty(possible_swaps)
        error('Couldn''t make targets not overlap. No possible swaps')
    end
    swapi=possible_swaps(randi(length(possible_swaps)));
    si([overlapping_target_trials(1) swapi]) = si([swapi overlapping_target_trials(1)]);
    overlapping_target_trials = find(abs(o.ATargetSample - o.BTargetSample(si)) < MinSamplesBetweenTargets);
end
if ~isequal(si,1:o.NtrialsPerRep(1))
    swap_vars={'BIndexes','BOffNs','BOnNs','BTrialNSamples','BTargetSample'};
    for i=1:length(swap_vars)
        o.(swap_vars{i}) = o.(swap_vars{i})(si);
    end
end

%% Add more references to make both streams the same length on each trial
thresh = round(0.02 * o.SamplingRate);
ramp=UTcosramp(0.02,0,0.02,o.SamplingRate)';
for i=1:o.NtrialsPerRep(1)
   if sum(o.AOnNs{i})+sum(o.AOffNs{i}(1:end-1)) - (sum(o.BOnNs{i})+sum(o.BOffNs{i}(1:end-1))) > thresh
    %A longer than B
       needed_samples = sum(o.AOnNs{i})+sum(o.AOffNs{i}(1:end-1)) - (sum(o.BOnNs{i})+sum(o.BOffNs{i}(1:end))) + thresh;
       if needed_samples<1
           continue
       end
       candidates = round(B.Nsamples/B.fs*o.SamplingRate) > needed_samples;
       candidates(~cellfun(@isempty,o.BTokens))=0;
       candidates=find(candidates);
       token = candidates(randi(length(candidates)));
       if length(o.BTokens)>=token && ~isempty(o.BTokens{token})
           error('Fix me')
       end
       o.BTokens{token} = resample(B.smtx{token},o.SamplingRate,B.fs);
       o.BTokens{token}(needed_samples+1:end)=[];
       o.BTokens{token}(end-length(ramp)+1:end) = o.BTokens{token}(end-length(ramp)+1:end) .*ramp;
       o.BOnNs{i}(end+1)=length(o.BTokens{token});
       o.BOffNs{i}(end+1)=round(o.PreStimSilence*o.SamplingRate);
       o.BIndexes{i}(end+1)=token;
       o.BTrialNSamples(i)=o.BTrialNSamples(i)+o.BOnNs{i}(end)+o.BOffNs{i}(end);
       o.BNames{token} = ['sB_' B.Names{token} 'short'];
       o.Bnf(token)=B.seg_nf(token);       
   elseif sum(o.AOnNs{i})+sum(o.AOffNs{i}(1:end-1)) - (sum(o.BOnNs{i})+sum(o.BOffNs{i}(1:end-1))) < -thresh
       %B longer than A
       needed_samples = (sum(o.BOnNs{i})+sum(o.BOffNs{i}(1:end-1))) - (sum(o.AOnNs{i})+sum(o.AOffNs{i}(1:end)))  + thresh;
       if needed_samples<1
           continue
       end
       candidates = round(A.Nsamples/A.fs*o.SamplingRate) > needed_samples;
       candidates(~cellfun(@isempty,o.ATokens))=0;
       candidates=find(candidates);
       token = candidates(randi(length(candidates)));
       if length(o.ATokens)>=token && ~isempty(o.ATokens{token})
           error('Fix me')
       end
       o.ATokens{token} = resample(A.smtx{token},o.SamplingRate,A.fs);
       o.ATokens{token}(needed_samples+1:end)=[];
       o.ATokens{token}(end-length(ramp)+1:end) = o.ATokens{token}(end-length(ramp)+1:end) .*ramp;
       o.AOnNs{i}(end+1)=length(o.ATokens{token});
       o.AOffNs{i}(end+1)=round(o.PreStimSilence*o.SamplingRate);
       o.AIndexes{i}(end+1)=token;
       o.ATrialNSamples(i)=o.ATrialNSamples(i)+o.AOnNs{i}(end)+o.AOffNs{i}(end);
       o.ANames{token} = ['sA_' A.Names{token} 'short'];
       o.Anf(token)=A.seg_nf(token);
   end
       
end

%% Generate Unique Targets
UniqueTargets={};
TargetDistSet=[];
next_tarset_id=1;
Atargets = o.ANames(o.StreamATarInds);
Btargets = o.BNames(o.StreamBTarInds);
if o.NtrialsPerRep(1)>0
    UniqueTargets=[UniqueTargets cellfun(@(x)[x '_sX'],Atargets,'Uni',0) cellfun(@(x)[x '_sX'],Btargets,'Uni',0)];
    TargetDistSet=[TargetDistSet next_tarset_id*ones(size(Atargets))];
    TargetDistSet=[TargetDistSet (next_tarset_id+1)*ones(size(Btargets))];
    next_tarset_id=next_tarset_id+2;
end
if o.NtrialsPerRep(2)>0
    UniqueTargets=[UniqueTargets cellfun(@(x)[x '_sA'],Atargets,'Uni',0)];
    TargetDistSet=[TargetDistSet next_tarset_id*ones(size(Atargets))];
    next_tarset_id=next_tarset_id+1;
end
if o.NtrialsPerRep(3)>0
    UniqueTargets=[UniqueTargets cellfun(@(x)[x '_sB'],Btargets,'Uni',0)];
    TargetDistSet=[TargetDistSet next_tarset_id*ones(size(Atargets))];
    next_tarset_id=next_tarset_id+1;
end
o.UniqueTargets = UniqueTargets;
o.TargetDistSet = TargetDistSet;

%% Decide how streams will combine
if o.Seeds(3)==0
    rng('shuffle');
else
    rng(o.Seeds(3));
end
TrialSet=[shuffle(cellfun(@(x)['X',num2str(x)],num2cell(1:o.NtrialsPerRep(1)),'Uni',0));
          shuffle(cellfun(@(x)['A',num2str(x)],num2cell(1:o.NtrialsPerRep(2)),'Uni',0));
          shuffle(cellfun(@(x)['B',num2str(x)],num2cell(1:o.NtrialsPerRep(3)),'Uni',0))];
o.TrialSet=shuffle_blocked(TrialSet(:),size(TrialSet,1)); %Shufle in blocks of the number of conditions
if o.TrialSet{1}(1)=='X' % If the first trial is a combined trial, switch it so it's a single-voice trial
    o.TrialSet(1:2)=o.TrialSet(2:-1:1);
end
    
%Add cues

if get(TrialObject,'CueTrialCount')>0 && CompletedTrials==0
    CuePostStimSilence=0.1;
   cue=o.TargetStream;
   cueIndex=length(o.([cue,'Indexes']))+1;
   cueTarInd=o.(['Stream',cue,'TarInds'])(1);
   o.([cue,'Indexes']){cueIndex} = [NaN cueTarInd];
   o.([cue,'OnNs']){cueIndex} = [0 length(o.([cue,'Tokens']){cueTarInd})];
   o.([cue,'OffNs']){cueIndex} = [o.PreCueSilence*o.SamplingRate CuePostStimSilence*o.SamplingRate];   
   o.TrialSet= [repmat({[cue,num2str(cueIndex)]},get(TrialObject,'CueTrialCount'),1); o.TrialSet];
end

o=set(o,'MaxIndex',length(o.TrialSet));
o=set(o,'Names',o.TrialSet);
o=set(o,'stim_order',(1:length(o.TrialSet))'); % Tells ReferenceTarget to play stimuli in order (NaturalStreams randomizes as desired)
o=set(o,'Duration',max([o.ATrialNSamples;o.BTrialNSamples])/o.SamplingRate);
o.MinimumTargetTime=min([o.AMinimumTargetTime,o.BMinimumTargetTime]);
rng(saveseed)


% Names=cell(1,MaxIndex);
% for ii=1:MaxIndex,
%     Names{ii}=['T'];
%     for bb=1:bandcount,
%         if idxset(ii,bb)
%             Names{ii}=[Names{ii} '+' Env.Names{idxset(ii,bb)}];
%         else
%             Names{ii}=[Names{ii} '+null'];
%         end
%     end
% end
% 
% o = set(o,'Names',Names);





function o=fillset(o,s,svar,Ntrials,TarSlotFreq,SingleTokenShift,MaxTokenLength)
if ~isfield(svar,'Post_silences')
    svar.Post_silences = (rand(size(svar.smtx))*.3 +.1)*svar.fs;
end
token_lengths = svar.Post_silences+svar.Nsamples;
use = token_lengths < MaxTokenLength*svar.fs;
DurationMinSamples = o.DurationMin*svar.fs;

if svar.notes(1:8) == 'Marmoset'
    use = use & ~contains(svar.Names,'TWITTER');
end
median_token_length = median(token_lengths(use));
pctile_15_token_length=prctile(token_lengths(use),15);
pctile_80_token_length=prctile(token_lengths(use),80);
max_repeated_token_slot =  ceil(DurationMinSamples / pctile_80_token_length);
max_repeated_token_slot = find(TarSlotFreq,1,'last')-1;
repeated_token_slot_range = 1; %number of slots between tokens in repeat set  will vary by =/- this amount
min_token_length = min(token_lengths);
%max_tokens_per_trial =  ceil(DurationMinSamples / min_token_length);
max_tokens_per_trial =  ceil(DurationMinSamples / pctile_15_token_length)+1;

%create target slot set
good_shuffle_blocks=find(all(mod(repmat(TarSlotFreq',1,Ntrials).*repmat(1:Ntrials,length(TarSlotFreq),1),1)<10*eps));
if(isempty(good_shuffle_blocks))
    o.shuffle_block_RefLen=Ntrials;
    warning(['No shuffle block length was found that could satisfy the ',...
        'ReferenceCountFreq given, [',num2str(ReferenceCountFreq),']. Shuffling all ',...
        '%d reference count indicies together'],Ntrials)
else
    o.(['ShuffleBlock',s])=good_shuffle_blocks(1);
end
TarSlot_=[];
for ii=1:length(TarSlotFreq)
    TarSlot_=cat(2,TarSlot_,ones(1,round(o.(['ShuffleBlock',s]).*TarSlotFreq(ii))).*ii);
end
TarSlot_=shuffle_blocked(TarSlot_,o.(['ShuffleBlock',s]));


%Start with empty cells
o.([s,'Indexes'])=repmat({NaN(max_tokens_per_trial,1)},Ntrials,1);
o.([s,'OnNs'])=repmat({NaN(max_tokens_per_trial,1)},Ntrials,1);
o.([s,'OffNs'])=repmat({NaN(max_tokens_per_trial,1)},Ntrials,1);

% Add Targets according to TarSlot_
if length(o.(['Stream',s,'TarInds']))>1
    error('Figure out balancing for multiple targets if needed')
else
    TarInd=o.(['Stream',s,'TarInds']);
end
for trial=1:Ntrials
    o.([s,'Indexes']){trial}(TarSlot_(trial))=TarInd;
    o.([s,'OnNs']){trial}(TarSlot_(trial))=svar.Nsamples(TarInd);
    o.([s,'OffNs']){trial}(TarSlot_(trial))=svar.Post_silences(TarInd);
end

% Add repeated references
RepeatInds = repmat(o.(['Stream',s,'RepeatInds']),1,o.(['Stream',s,'Repeats']));
RepeatInds = shuffle_blocked(RepeatInds,length(o.(['Stream',s,'RepeatInds'])));
mean_slotdiff_between_repeat_tokens = max_repeated_token_slot*Ntrials/length(RepeatInds);
slotdiff_between_repeat_token_range = round(mean_slotdiff_between_repeat_tokens) + repeated_token_slot_range*[-1 1];
slotdiff_set=slotdiff_between_repeat_token_range(1):slotdiff_between_repeat_token_range(2);
slotdiff_between_repeat_tokens = repmat(slotdiff_set,1,ceil(length(RepeatInds)/length(slotdiff_set)));
slotdiff_between_repeat_tokens(length(RepeatInds)+1:end)=[];
if mean(slotdiff_between_repeat_tokens)>mean_slotdiff_between_repeat_tokens
    use=1:length(slotdiff_between_repeat_tokens);
    while mean(slotdiff_between_repeat_tokens)>mean_slotdiff_between_repeat_tokens
        %mv=max(slotdiff_between_repeat_tokens);
        %mi=find(slotdiff_between_repeat_tokens==mv);
        %chi=mi(randi(length(mi)));
        chi=use(randi(length(use)));
        use(randi(length(use)))=[];
        slotdiff_between_repeat_tokens(chi)=slotdiff_between_repeat_tokens(chi)-1;
    end
end

slotdiff_between_repeat_tokens = shuffle_blocked(slotdiff_between_repeat_tokens,length(slotdiff_set)*2);
%slotdiff_between_repeat_tokens = repmat(mean_slotdiff_between_repeat_tokens,1,length(RepeatInds)-1);
slots=[1,1+cumsum(slotdiff_between_repeat_tokens)];
slots(length(RepeatInds)+1:end)=[];

slot_trial_index= ceil(slots / (max_repeated_token_slot));
slot_index = rem(slots,max_repeated_token_slot);
slot_index(slot_index==0)=max_repeated_token_slot;
too_high=slot_trial_index>Ntrials;
if sum(too_high)==1
    slot_trial_index(too_high)=Ntrials;
    slot_index(too_high)=max_repeated_token_slot;
elseif sum(too_high)>1
    error('fix me')
end

for i=1:length(slot_index)
    occupied_slots=find(~isnan(o.([s,'Indexes']){slot_trial_index(i)}));
    while any(occupied_slots==slot_index(i))
        if rand(1) > .7 && slot_index(i)>1
            slot_index(i)=slot_index(i)-1;
        else
            slot_index(i)=slot_index(i)+1;
        end
        occupied_slots=find(~isnan(o.([s,'Indexes']){slot_trial_index(i)}));
    end
    o.([s,'Indexes']){slot_trial_index(i)}(slot_index(i))=RepeatInds(i);
    o.([s,'OnNs']){slot_trial_index(i)}(slot_index(i))=svar.Nsamples(RepeatInds(i));
    o.([s,'OffNs']){slot_trial_index(i)}(slot_index(i))=svar.Post_silences(RepeatInds(i));
end

Nremaining = sum(sum(isnan([o.([s,'Indexes']){:}])));
PossibleTokens=1:svar.MaxIndex;
PossibleTokens=setdiff(PossibleTokens(use),unique([o.(['Stream',s,'TarInds']),o.(['Stream',s,'RepeatInds'])]));
PossibleTokens=circshift(PossibleTokens,round(SingleTokenShift*length(PossibleTokens)));
if length(PossibleTokens)< Nremaining
    warning('Duplicates in NonRepeatInd set of Stream %s',s)
    NonRepeatInds=[];
    while length(NonRepeatInds)< Nremaining
        NonRepeatInds=[NonRepeatInds shuffle(PossibleTokens)];
    end
    NonRepeatInds=NonRepeatInds(1:Nremaining);
else
    NonRepeatInds=shuffle(PossibleTokens(1:Nremaining));
end

i=0;
for trial=1:Ntrials
    while nansum([o.([s,'OnNs']){trial};o.([s,'OffNs']){trial}]) < DurationMinSamples ||...
          any(isnan(o.([s,'Indexes']){trial}(1:TarSlot_(trial))))
        i=i+1;
        nani=find(isnan(o.([s,'Indexes']){trial}),1);
        if isempty(nani)
            error('No trials left to fill')
        end
        o.([s,'OnNs']){trial}(nani)=svar.Nsamples(NonRepeatInds(i));
        o.([s,'OffNs']){trial}(nani)=svar.Post_silences(NonRepeatInds(i));
        o.([s,'Indexes']){trial}(nani)=NonRepeatInds(i);
    end
    nani=find(isnan(o.([s,'Indexes']){trial}),1);
    if any(o.([s,'Indexes']){trial}(nani:end) == TarInd)
        error('Moving Target!')
    end
    rmi=find(isnan(o.([s,'Indexes']){trial}));
    o.([s,'Indexes']){trial}(rmi)=[];
    o.([s,'OnNs']){trial}(rmi)=[];
    o.([s,'OffNs']){trial}(rmi)=[];
end

needed_tokens = unique(cat(1,o.([s,'Indexes']){:}));
o.([s,'Tokens'])=cell(max(needed_tokens),1);
for i=1:length(needed_tokens)
    o.([s,'Tokens']){needed_tokens(i)}=resample(svar.smtx{needed_tokens(i)},o.SamplingRate,svar.fs);
end
SR_scale = o.SamplingRate/svar.fs;
for trial = 1:Ntrials
    o.([s,'OnNs']){trial}=ceil(o.([s,'OnNs']){trial}*SR_scale); %This is what comes out of resample
    o.([s,'OffNs']){trial}=ceil(o.([s,'OffNs']){trial}*SR_scale);
end
o.([s,'TrialNSamples']) = cellfun(@(x,y)sum([x;y]),o.([s,'OnNs']),o.([s,'OffNs']));
o.([s,'nf'])(needed_tokens)=svar.seg_nf(needed_tokens);

for i=1:length(needed_tokens)
    o.([s,'Names']){needed_tokens(i)} = ['s' s '_' svar.Names{needed_tokens(i)}];
end
o.([s,'MinimumTargetTime'])= min(TarSlot_)*min_token_length/svar.fs;
for trial=1:Ntrials
    ti=find(o.([s,'Indexes']){trial}==TarInd);
    o.([s,'TargetSample'])(trial) = sum(o.([s,'OnNs']){trial}(1:ti-1)) + sum(o.([s,'OffNs']){trial}(1:ti-1));
end

% function o=fillset(o,s,svar,RepSet)
% 
% DurationMinSamples = o.DurationMin*svar.fs;
% o.([s,'Indexes'])=cell(0);
% o.([s,'OnNs'])=[];
% o.([s,'OffNs'])=cell(0);
% trial=0;
% while ~isempty(RepSet)
%     trial=trial+1;
%     o.([s,'OnNs'])(trial)=0;
%     o.([s,'Indexes']){trial}=[];
%     o.([s,'OffNs']){trial}=[];
%     while o.([s,'OnNs'])(trial) < DurationMinSamples
%         o.([s,'OnNs'])(trial)=o.([s,'OnNs'])(trial)+svar.Post_silences(RepSet(1))+svar.Nsamples(RepSet(1));
%         o.([s,'OffNs']){trial}=[o.([s,'OffNs']){trial} svar.Post_silences(RepSet(1))];
%         o.([s,'Indexes']){trial}=[o.([s,'Indexes']){trial} RepSet(1)];
%         RepSet(1)=[];
%         if isempty(RepSet)
%             if o.([s,'OnNs'])(trial) < DurationMinSamples
%                 warning('Not enough tokens to fill a trial. Last trial is %.3f s, should be at least %.3f s.',o.([s,'OnNs'])(trial)/svar.fs,DurationMinSamples/svar.fs)
%             end
%             break
%         end
%     end
% end