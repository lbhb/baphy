function plot_stimevents_on_BehaviorDisplay(RefObject,StimEvents)

for cnt1 = 1:length(StimEvents)
    [Type, StimName, StimRefOrTar] = ParseStimEvent (StimEvents(cnt1));
    if strcmpi(Type,'Stim')
        if strcmpi(StimRefOrTar,'Reference')
            if contains(StimName,'sA_')
                c=[.1 .5 .1];
            elseif contains(StimName,'sB_')
                c=[.1 .5 .1];
            end
        else
            if contains(StimName,'sA_')
                c=[1 .5 .5]; 
            elseif contains(StimName,'sB_')
                c=[1 .5 .5];
            end
        end
        if contains(StimName,'sA_')
            yp = [0 .3];
            ypt=0.15;
            fs=8;
        elseif contains(StimName,'sB_')
            yp = [.33 .63];
            if mod(cnt1,2)
                ypt=0.48-.06;
            else
                ypt=0.48+.06;
            end
            fs=7;
        end
        
        line([StimEvents(cnt1).StartTime StimEvents(cnt1).StartTime],...
            yp([1 2]),'color',c,'LineStyle','-','LineWidth',1.5);
        line([StimEvents(cnt1).StopTime StimEvents(cnt1).StopTime],...
            yp([1 2]),'color',c,'LineStyle','-','LineWidth',1.5);
        line([StimEvents(cnt1).StartTime StimEvents(cnt1).StopTime],...
            yp([2 2]),'color',c,'LineStyle','-','LineWidth',1.5);
        u=strfind(StimName,'_');
        if length(u)==2
            str=StimName(u(1)+1:u(2)-1);
        else
            str=[StimName(u(1)+1:u(2)-1),':',StimName(u(2)+1:u(3)-1)];
        end
        %str=StimRefOrTar(1);
        text((StimEvents(cnt1).StartTime+StimEvents(cnt1).StopTime)/2,...
            ypt,str,'color',c,...
            'FontWeight','bold','HorizontalAlignment','center','VerticalAlignment','Middle','FontSize',fs);
        if yp(1)>0
            line([StimEvents(cnt1).StartTime StimEvents(cnt1).StopTime],...
            yp([1 1]),'color',c,'LineStyle','-','LineWidth',1.5);
        end
    end
end

end