function trial_str=get_target_suffixes(o,varargin)
%(o,indexes,o_struct)
indexes=getparmC(varargin,1,[]);
o_struct=getparmC(varargin,2,[]);
if ~isempty(o_struct)
    TrialSet=o_struct.TrialSet;
else
    TrialSet=o.TrialSet;
end
if isempty(indexes)
    do_unique=true;
    indexes=1:length(TrialSet);
else
    do_unique=false;
end
for k=1:length(indexes)
    trial_str{k}=['_s' TrialSet{indexes(k)}(1)];
end
if do_unique
    trial_str=unique(trial_str);
end
end