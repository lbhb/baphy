function generatesnippets(o)
if 0
%%
rng default
SamplingRateFinalEnv=2000;
SamplingRateFinalWav=22050;
%_H_LPfilt50HzN6_filtfilt2x
%pth='C:\Users\lbhb\Downloads\';
pth='/auto/data/lbhb/Stimulus_Library/speech_samples/Librivox/';
pth=strrep(pth,'/auto/data/','H:\');
fns={'confession_01_tolstoy_64kb.wav','confession_02_tolstoy_64kb.wav','confession_03_tolstoy_64kb.wav'};
fs=22050;
lpFilt = designfilt('lowpassiir', 'FilterOrder', 6, ...
    'PassbandFrequency', 50, 'PassbandRipple', 0.2,...
    'SampleRate', fs);
hpFilt = designfilt('highpassiir', 'FilterOrder', 16, ...
    'PassbandFrequency', 70, 'PassbandRipple', 0.2,...
    'SampleRate', fs);
if 0
    warning('MIGHT WANT TO TURN OFF HIGHPASS FILTER IF USING ENVELOPES')
    %Used in SpVowels to generate long envelopes
    %Part 1 params (ramp silences and remove long silences):
    silence_thresh=.01; % envelopes below this value considered silent
    silence_time_thresh=.1; % in sec. Silences longer than this are considered silent
    max_silence=.6;
    shortened_silence_range=[.3 max_silence]; %Silences will be in this range
    
    %Part 2 params (normalization):
    normalization_range=[3 9]; % in sec. Normalize in chunks in this range.
    normalization_range_target=4; %in sec. Target length for normalization. If multile lengths are possible, choose the length closest to this.
    
    %Part 3 params (create snippets)
    st_buf=.02;
    dur_range=[25 35]-st_buf; %Make snippets in this time range. Target is the mean
    snippet_start='sil_nds';
    snippet_end='sil_mids';
    normalization_range_sample_start=[2 8]; %Normlaize start of snippets in this range.
    normalization_range_target_sample_start=3; %
else
    %Cutting up Speech
    %Part 1 params (ramp silences and remove long silences):
    silence_thresh=.01; % envelopes below this value considered silent
    silence_time_thresh=.1; % in sec. Silences longer than this are considered silent
    max_silence=.6;
    shortened_silence_range=[.3 max_silence]; %Silences will be in this range
    min_sound_length=.5;
    
    %Part 2 params (normalization):
    normalization_range=[3 9]; % in sec. Normalize in chunks in this range.
    normalization_range_target=4; %in sec. Target length for normalization. If multile lengths are possible, choose the length closest to this.
    normalization_range(1)=inf; %removes normalization
    
    %Part 3 params (create snippets)
    st_buf=0.02;
    dur_range=[1 10]-st_buf; %Make snippets in this time range. Target is the mean
    target_dur = 1; %Cut into shortest possible snippets
    snippet_start='sil_nds';
    snippet_end='sil_sts';
    normalization_range_sample_start=[2 8]; %Normlaize start of snippets in this range.
    normalization_range_target_sample_start=3; %
    normalization_range_sample_start(1)=inf;
end

%Convert to samples
st_buf_samples=st_buf*fs;
dur_range_samples=dur_range*fs;
target_dur_samples=target_dur*fs;
normalization_range_samples=normalization_range*fs;
normalization_range_sample_start_samples=normalization_range_sample_start*fs;
normalization_range_target_samples=normalization_range_target*fs;
normalization_range_target_sample_start_samples=normalization_range_target_sample_start*fs;


total=round(.02*fs)/fs;rise=total;fall=0;
sil_ramp=UTcosramp(total,rise,fall,fs)'; NSR=length(sil_ramp);

seg_ind=1;
nf_seg_ind=1;
for idx=1:length(fns)
    fprintf('\nIdx %d: ',idx)
    [sp,fs_]=audioread([pth,fns{idx}]);
    if fs~=fs_
        sp=resample(sp,fs,fs_);
    end
    %fl_t=1;
    %fl=round(fl_t*fs);
    %env=envelope(sp,fl);
    sp2=[zeros(fs,1);sp;zeros(fs,1)];
    sp2=filtfilt(hpFilt,sp2); %Apply highpass filter to waveform before extracting envelope to prevent low-frequency artifacts from messing up silence identification
    env_h=abs(hilbert(sp2));
    env_hf=filtfilt(lpFilt,env_h);
    env_hf=abs(env_hf);
    env_hf([1:fs,end-fs+1:end])=[];
    %      env_hf=filtfilt(lpFilt,env_hf);
    Env.file_norm_factor(idx)=max(env_hf);
    env_hf=env_hf./Env.file_norm_factor(idx);
    
    Wav.file_norm_factor(idx)=max(abs(sp));
    sp=sp./Wav.file_norm_factor(idx);
    
    
    %% apply ramp to make silences truly silent, remove long silences
    silent_inds_=env_hf < silence_thresh;
    %apply a smoothing filter of length silence_time_thresh
    silent_inds1 = smooth(silent_inds_, fs*silence_time_thresh);
    silent_inds = silent_inds1 > 1-10*eps; %threshold to throw out too short silences
    silent_inds = smooth(silent_inds, fs*silence_time_thresh) > 10*eps; %re-expand mask to original silence lengths
    df=diff(silent_inds);
    sil_st=[1; find(df==1)];
    sil_nd=[find(df==-1); length(env_hf)];
    if length(sil_st) ~= length(sil_nd), error('Unequal silence interval markers'), end
    
    %Remove silence markers that make sounds shorter than min_sound_length
    sound_lengths=(sil_st(2:end)-sil_nd(1:end-1))/fs;
    short_sounds=find(sound_lengths < min_sound_length);
    sil_st(short_sounds+1)=[];
    sil_nd(short_sounds)=[];
    
    sil_mids=round(sil_st+(sil_nd-sil_st)/2);
    sil_durs=(sil_nd-sil_st)/fs;
    rmi=false(size(env_hf));
    for i=1:length(sil_durs)
        if (sil_nd(i)-sil_st(i))>2*NSR
            %apply ramp to make silences truly silent
            env_hf(sil_st(i)+(0:NSR-1))=env_hf(sil_st(i)+(0:NSR-1)).*flipud(sil_ramp);
            env_hf(sil_nd(i)+(-NSR+1:0))=env_hf(sil_nd(i)+(-NSR+1:0)).*sil_ramp;
            sp(sil_st(i)+(0:NSR-1))=sp(sil_st(i)+(0:NSR-1)).*flipud(sil_ramp);
            sp(sil_nd(i)+(-NSR+1:0))=sp(sil_nd(i)+(-NSR+1:0)).*sil_ramp;
            if sil_st(i)==1
                zero_inds=sil_st(i):(sil_nd(i)-NSR);
            else
                zero_inds=(sil_st(i)+NSR):(sil_nd(i)-NSR);
            end
            env_hf(zero_inds)=0;
            sp(zero_inds)=0;
            
        else
            aa=2;
            %figure;plot(sil_st(i-2)-1e2:sil_nd(i+2),env_hf(sil_st(i-2)-1e2:sil_nd(i+2)))
        end
        if sil_durs(i)>max_silence
            new_sil_dur=rand*diff(shortened_silence_range)+shortened_silence_range(1);
            half_rm_samps=ceil((sil_durs(i)-new_sil_dur)*fs/2);
            rm_st_ind=sil_mids(i)-half_rm_samps;
            rmi(rm_st_ind+[0:half_rm_samps*2])=true;
        end
    end
    original_inds=1:length(env_hf);
    original_inds(rmi)=[];
    env_hf(rmi)=[];
    sp(rmi)=[];
    
    
    
    %% Re-compute silent indicies after removal step above
    silent_inds_=env_hf < silence_thresh;
    %apply a .01-second smoothing filter
    silent_inds1 = smooth(silent_inds_, fs*silence_time_thresh);
    silent_inds = silent_inds1 > 1-10*eps;
    silent_inds = smooth(silent_inds, fs*silence_time_thresh) > 10*eps;
    df=diff(silent_inds);
    sil_st=[1; find(df==1)];
    sil_nd=[find(df==-1); length(env_hf)];
    silence_lengths=sil_nd-sil_st;
    if length(sil_st) ~= length(sil_nd), error('Unequal silence interval markers'), end
    %Remove silence markers that make sounds shorter than min_sound_length
    sound_lengths=(sil_st(2:end)-sil_nd(1:end-1))/fs;
    short_sounds=find(sound_lengths < min_sound_length);
    sil_st(short_sounds+1)=[];
    sil_nd(short_sounds)=[];
    
    sil_mids=round(sil_st+(sil_nd-sil_st)/2);
    sil_durs=(sil_nd-sil_st)/fs;
    
    %% Part 2: Normalize each section
    sil_ind=1;
    Env.segment_nf_ind_st(idx)=nf_seg_ind;
    while 1
        possible_lengths=sil_mids(sil_ind+1:end)-sil_nd(sil_ind);
        good_lengths=find(possible_lengths>=normalization_range_samples(1) & possible_lengths<normalization_range_samples(2));
        if sum(good_lengths)>0
            [~,bli]=min(abs(possible_lengths(good_lengths)-normalization_range_target_samples));
            best_length=good_lengths(bli);
        elseif all(possible_lengths<normalization_range_samples(1))
            break
        else
            error('No good lengths, possible lengths are: %.03f %.03f %.03f %.03f %.03f %.03f ',possible_lengths(1:6)/fs)
            ind=sil_st(sil_ind)+(0:normalization_range_samples(2)*2);
            figure;plot(ind/fs,env_hf(ind));
        end
        inds=sil_nd(sil_ind):sil_mids(sil_ind+best_length);
        Env.segment_norm_factor(nf_seg_ind)=max(env_hf(inds));
        env_hf(inds)=env_hf(inds)./Env.segment_norm_factor(nf_seg_ind);
        nf_seg_ind=nf_seg_ind+1;
        sil_ind=sil_ind+best_length;
    end
    
    %% Part 3: Break into sections
    sil_ind=1;
    skipped_segment_=false;
    while 1
        if (sil_nd(sil_ind)-sil_st(sil_ind))/fs<st_buf
            sil_ind=sil_ind+1;
            continue;
        end
        switch snippet_start
            case 'sil_nds'
                st_ind=sil_nd(sil_ind)-st_buf_samples;
            otherwise
                error('')
        end
        switch snippet_end
            case 'sil_mids'
                possible_lengths=sil_mids(sil_ind+1:end)-st_ind;
            case 'sil_sts'
                possible_lengths=sil_st(sil_ind+1:end)-st_ind+st_buf_samples;
            otherwise
                error('')
        end
        good_lengths=find(possible_lengths>=dur_range_samples(1) & possible_lengths<dur_range_samples(2));
        if sum(good_lengths)>0
            [~,bli]=min(abs(possible_lengths(good_lengths)-target_dur_samples));
            best_length=good_lengths(bli);
        elseif all(possible_lengths<dur_range_samples(1))
            %All too short
            break
        else
            warning('No good lengths (too long). Skipping segment %d.',sil_ind)
            skipped_segment_=true;
            sil_ind=sil_ind+best_length;
            fprintf('%d ',seg_ind)
            continue
        end
        inds=st_ind;
        switch snippet_end
            case 'sil_mids'
                inds=st_ind:sil_mids(sil_ind+best_length);
            case 'sil_sts'
                inds=st_ind:sil_st(sil_ind+best_length)+st_buf_samples;
            otherwise
                error('')
        end
        this_env=env_hf(inds);
        this_wav=sp(inds);
        %this_env=this_env./max(this_env); already normed above
        emtx_{seg_ind}=resample(this_env,SamplingRateFinalEnv,fs);
        Env.downsampled_seg_nf(seg_ind)=max(emtx_{seg_ind});
        emtx_{seg_ind}=emtx_{seg_ind}./Env.downsampled_seg_nf(seg_ind);
        if any(emtx_{seg_ind}<-1e-4), error('Envelopes less than 0.'), end
        emtx_{seg_ind}(emtx_{seg_ind}<0)=0;
        
        smtx_{seg_ind}=resample(this_wav,SamplingRateFinalWav,fs);
        Wav.seg_nf(seg_ind)=max(abs(smtx_{seg_ind}));
        %smtx_{seg_ind}=smtx_{seg_ind}./Wavseg_nf(seg_ind);
        
        sil_sts_orig_fs{seg_ind}=[1; sil_st(sil_st>=inds(1) & sil_st<=inds(end))-inds(1)+1];
        sil_nds_orig_fs{seg_ind}=[sil_nd(sil_nd>=inds(1) & sil_nd<=inds(end))-inds(1)+1; length(this_env)];
        sil_sts{seg_ind}=[1; round((sil_st(sil_st>=inds(1) & sil_st<=inds(end))-inds(1)+1)/fs*SamplingRateFinalEnv)];
        sil_nds{seg_ind}=[round((sil_nd(sil_nd>=inds(1) & sil_nd<=inds(end))-inds(1)+1)/fs*SamplingRateFinalEnv); length(emtx_{seg_ind})];
        sil_mids_orig_fs=round(sil_sts_orig_fs{seg_ind}+(sil_nds_orig_fs{seg_ind}-sil_sts_orig_fs{seg_ind})/2);
        sil_mids_=round(sil_sts{seg_ind}+(sil_nds{seg_ind}-sil_sts{seg_ind})/2);
        
        Names{seg_ind}=sprintf('TS%d_%d',idx,round(inds(1)/fs));
        Names{seg_ind}=sprintf('TS%d_%d',idx,round(original_inds(inds(1))/fs));
        Original_Inds(seg_ind,:)=original_inds(inds([1 end]));
        File_Index(seg_ind)=idx;
        switch snippet_end
            case 'sil_mids'
                error('Figure out in needed')
            case 'sil_sts'
                Post_silences(seg_ind)=sil_nd(sil_ind+best_length)-sil_st(sil_ind+best_length)-1-2*st_buf_samples;
                Post_silences_preRS(seg_ind)=original_inds(sil_nd(sil_ind+best_length))-original_inds(sil_st(sil_ind+best_length))-1-2*st_buf_samples;
        end
        Skipped_Segment(seg_ind)=skipped_segment_;
        
        %% normalize first part of this segment to ensure each stream starts with a high amplitude
        sil_ind2=1;
        possible_lengths2=sil_mids_orig_fs(sil_ind2+1:end)-sil_nds_orig_fs{seg_ind}(sil_ind2);
        good_lengths2=find(possible_lengths2>=normalization_range_sample_start_samples(1) & possible_lengths2<normalization_range_sample_start_samples(2));
        if sum(good_lengths2)>0
            [~,bli]=min(abs(possible_lengths2(good_lengths2)-normalization_range_target_sample_start_samples));
            best_length2=good_lengths2(bli);
            do_norm=true;
        elseif all(possible_lengths2<normalization_range_sample_start_samples(1))
            %warning('Not normalizing start of segment %d, too short',seg_ind)
            do_norm=false;
            %break
        else
            error('No good lengths, possible lengths are: %.03f %.03f %.03f %.03f %.03f %.03f ',possible_lengths2(1:6)/fs)
        end
        if do_norm
            inds2=sil_nds{seg_ind}(sil_ind2):sil_mids_(sil_ind2+best_length2);
            Env.segment_norm_factor2(seg_ind)=max(emtx_{seg_ind}(inds2));
            emtx_{seg_ind}(inds2)=emtx_{seg_ind}(inds2)./Env.segment_norm_factor2(seg_ind);
            Wav.segment_norm_factor2(seg_ind)=max(abs(smtx_{seg_ind}(inds2)));
            smtx_{seg_ind}(inds2)=smtx_{seg_ind}(inds2)./Wav.segment_norm_factor2(seg_ind);
        end
        %figure;plot((1:length(emtx_{seg_ind}))/SamplingRateFinalEnv,emtx_{seg_ind},'-')
        
        
        if 0
            t1=(0:(length(this_env)-1))/fs;
            figure;plot(t1,this_env);hold on;
            for i=1:length(sil_sts{seg_ind})
                line(sil_sts_orig_fs{seg_ind}([i i])/fs,[0 1],'LineStyle','--','Color','r');
                line(sil_nds_orig_fs{seg_ind}([i i])/fs,[0 1],'LineStyle','--','Color','g');
            end
            ax=gca;
            t2=(0:(length(emtx_{seg_ind})-1))/SamplingRateFinalEnv;
            figure;plot(t2,emtx_{seg_ind});hold on;
            for i=1:length(sil_sts{seg_ind})
                line(sil_sts{seg_ind}([i i])/SamplingRateFinalEnv,[0 1],'LineStyle','--','Color','r');
                line(sil_nds{seg_ind}([i i])/SamplingRateFinalEnv,[0 1],'LineStyle','--','Color','g');
            end
            ax(2)=gca;
            linkaxes(ax);
        end
        if length(sil_sts{seg_ind}) ~= length(sil_nds{seg_ind})
            error('Silent inds mismatch')
        end
        seg_ind=seg_ind+1;
        sil_ind=sil_ind+best_length;
        skipped_segment_=false;
        fprintf('%d ',seg_ind)
    end
end
if length(unique(Names))<length(Names)
    error('Non-unique names')
end

%% Prepare Wav
Wav.smtx=smtx_;
Wav.Nsamples=cellfun(@length,smtx_);
Wav.fs=SamplingRateFinalWav;
Wav.Duration=min(Wav.Nsamples)/Wav.fs;%mean(dur_range+st_buf);
Wav.sil_sts=sil_sts;
Wav.sil_nds=sil_nds;
Wav.sil_durs=cellfun(@(x,y)(y-x)/SamplingRateFinalWav,sil_sts,sil_nds,'Uni',0);
Wav.Names=Names;
Wav.Original_Inds=Original_Inds;
Wav.File_Index=File_Index;
Wav.Post_silences=Post_silences;
Wav.Post_silences_preRS=Post_silences_preRS;
Wav.Skipped_Segment=Skipped_Segment;
Wav.MaxIndex=length(smtx_);
Wav.notes=sprintf(['Three chapters from librovox at 22.05 kHz (https://librivox.org/a-confession-version-2-by-leo-tolstoy/)'...
    ,'split into sections %d-%d sec long. Each section starts with %d ms'...
    ' of silence. Rise times are variable.',dur_range(1),dur_range(2),st_buf]);
Wav.fns=fns;
vars={'st_buf','dur_range','normalization_range','normalization_range_sample_start',...
    'normalization_range_target','normalization_range_target_sample_start','silence_thresh',...
    'silence_time_thresh','max_silence','shortened_silence_range','snippet_start','snippet_end','fs'};
for i=1:length(vars)
    Wav.params.(vars{i})=eval(vars{i});
end

dd=fileparts(which('NaturalStreams'));
outfile=[filesep 'waveform' filesep 'Speech.subset1.mat'];
%save([dd outfile],'-Struct','Wav');


%% Prepare Env
if 0
    figure;imagesc(emtx);
    xlim([.5 6000]);ylim([.5 30.5]);set(gca,'CLim',[0 1])
    d=load('C:\code\baphy-current\SoundObjects\@SpTones\envelope\Speech.subset1_H_LPfilt50HzN6_filtfilt2x.mat');
    figure;imagesc(d.Env.emtx');
    xlim([.5 6000]);ylim([.5 30.5]);set(gca,'CLim',[0 1])
end

ml=max(cellfun(@length,emtx_));
emtx=nan(ml,length(emtx_));
for i=1:length(emtx_)
    emtx(1:length(emtx_{i}),i)=emtx_{i};
end

clear Env;
Env.emtx=emtx;
Env.Nsamples=cellfun(@length,emtx_);
Env.fs=SamplingRateFinalEnv;
Env.Duration=min(Env.Nsamples)/Env.fs;%mean(dur_range+st_buf);
Env.sil_sts=sil_sts;
Env.sil_nds=sil_nds;
Env.sil_durs=cellfun(@(x,y)(y-x)/SamplingRateFinalEnv,sil_sts,sil_nds,'Uni',0);
Env.Names=Names;

%    Env.Phonemes=get(s,'Phonemes');
Env.MaxIndex=length(emtx_);
Env.notes=['Three chapters from librovox at 22.05 kHz (https://librivox.org/a-confession-version-2-by-leo-tolstoy/)'...
    ,' abs(hilbert) then'...
    ' LP filtered cheby1 N=6 50 Hz, 0.2 ripple. Then resampled to 2 kHz,'...
    ' and split into sections 25-35 sec long. Each section starts with 20 ms'...
    ' of silence. Rise times are variable.'];
Env.fns=fns;
Env.lpFilt=lpFilt;
vars={'st_buf','dur_range','normalization_range','normalization_range_sample_start',...
    'normalization_range_target','normalization_range_target_sample_start','silence_thresh',...
    'silence_time_thresh','max_silence','shortened_silence_range','snippet_start','snippet_end','fs'};
for i=1:length(vars)
    Env.params.(vars{i})=eval(vars{i});
end
        

if 1 %shorten to save resampling time during experiments
    dur_range2=[12 18];
    dur_range2_samples=dur_range2*SamplingRateFinalEnv;
    Env.emtx_all=Env.emtx;
    Env.Nsamples_all=Env.Nsamples;
    Env.Duration_all=Env.Duration;
    Env.emtx=nan(dur_range2_samples(end),length(emtx_));
    for seg_ind=1:length(emtx_)
        sil_ind2=1;
        sil_mids2=round(sil_sts{seg_ind}+(sil_nds{seg_ind}-sil_sts{seg_ind})/2);
        possible_lengths2=sil_mids2(sil_ind2+1:end);
        good_lengths2=find(possible_lengths2>=dur_range2_samples(1) & possible_lengths2<dur_range2_samples(2));
        if sum(good_lengths2)>0
            %possible_lengths2(good_lengths2)/SamplingRateFinalEnv
            [~,bli]=min(abs(possible_lengths2(good_lengths2)-mean(dur_range2_samples)));
            best_length2=good_lengths2(bli);
        elseif all(possible_lengths2<dur_range2_samples(1))
            break
        else
            error('No good lengths, possible lengths are: %.03f %.03f %.03f %.03f %.03f %.03f ',possible_lengths2(1:6)/SamplingRateFinalEnv)
        end
        inds2=1:sil_mids2(sil_ind2+best_length2);
        Env.Nsamples(seg_ind)=length(inds2); %length(inds2)/SamplingRateFinalEnv
        Env.emtx(1:length(inds2),seg_ind)=Env.emtx_all(1:length(inds2),seg_ind);
    end
    Env.emtx(max(Env.Nsamples)+1:end,:)=[];
    Env.Duration=min(Env.Nsamples)/Env.fs;
end

dd=fileparts(which('Sptones'));
outfile=['/envelope/Speech.subset6.mat'];
%save([dd outfile],'-Struct','Env');

if 0
    sil_durs=cell2mat(cellfun(@(x)x',Env.sil_durs,'Uni',0));
    figure;h7=histogram(sil_durs,50);
    figure;imagesc(Env.emtx')
    xlim([.5 6000])
end



%Check splicing back together
if 0
    S=[];
    order=1:length(smtx_);
    order=shuffle(order);
    for i=order
        S=[S;smtx_{i}.*Wav.downsampled_seg_nf(i);zeros(Post_silences_preRS(i),1)];
    end
    [spo,fs_]=audioread([pth,fns{idx}]);
    spo=spo./max(abs(spo));
    O=spo(Original_Inds(1,1):Original_Inds(end,2));
    figure;plott(fs,O);
    hold on;plott(fs,S,'--r')
    plott(fs,O-S(1:length(O)),'-g')
end

end

%% Marmo
if 1
    silence_threshs=[.07 .04 .04 .07 .12 .03 .01 .07];
    silence_time_thresh=.02;
    max_silence=.01; shortened_silence_range=[NaN max_silence];%not used since remove_inner_silences=0
    remove_inner_silences=0;
    sil_ramp_time=.01;
    trim_time=.02;
    crossfade_time_range=[.05 .4];
    crossfade_time_within=.2;
    fs=44100;
    trimN=round(trim_time*fs);
    tadd=cell(8,1);
    tadd{2}=[.01 .03];
    manual_sil_inds{5}{2}=[0.55 .73];
    manual_sil_inds{7}{2}=[0.63 0.92];
    manual_sil_inds{7}{5}=[1.4 1.86];
    manual_sil_inds{7}{6}=[.88 1.44];
    manual_sil_inds{7}{21}=[1.46 1.88];
    manual_sil_inds{8}{30}=[.82 1.88];
        
    fs=44100;
    SamplingRateFinalWav=fs;
    HPfilt  = designfilt('highpassiir', 'DesignMethod','cheby2', 'FilterOrder',6, ... %was 14th order, 60 dB
   'StopbandFrequency', 1500,'SampleRate', fs,'StopbandAttenuation',40);

    calls={'alarm_1','chirp','loud_shrill','phee_2','phee_3','phee_4','seep','trill','tsik','tsik_ek','twitter'};
    calls={'chirp','phee_2','phee_3','seep','trill','tsik','tsik_ek','twitter'};
    d='H:\lbhb\Stimulus_Library\MarmosetVocalizations\';
    ii=0;
    for i=1:length(calls)
        dd=dir([d calls{i}]);
        dd(1:2)=[];
        for j=1:length(dd)
            ii=ii+1;
            fprintf('%d ',ii)
            [wav,fs_]=audioread([d,calls{i} filesep dd(j).name]);
            if fs~=fs_
                wav=resample(wav,fs,fs_);
            end
        wav=filtfilt(HPfilt,wav);
        
        try
            manual_sil_ind=manual_sil_inds{i}{j};
        catch
            manual_sil_ind=[];
        end
        
        [wav2,min_envs(ii),silent_thresh_out,RmsNonSilent(ii),MaxNonSilent(ii)]=UTremove_silence(wav(trimN:end-trimN),1,fs,silence_threshs(i),silence_time_thresh,sil_ramp_time,max_silence,shortened_silence_range,crossfade_time_within,remove_inner_silences,tadd{i},manual_sil_ind);
        %fn=['G:\luke\Projects\SPS\MarmosetVocalizations\spectrograms_trimmed' filesep calls{i} filesep num2str(j) dd(j).name(5:end-4)];
        %UTsave(fn,{'png','fig'})
%         crossfade_time=rand(1)*diff(crossfade_time_range)+crossfade_time_range(1);
%         crossfade_times(ii)=crossfade_time;
%         crossfadeN=round(fs*crossfade_time);
%         crossfade_ramp=linspace(1,0,crossfadeN)';
%         wav2(1:crossfadeN) = wav2(1:crossfadeN).*(1-crossfade_ramp);
%         wav2(end-crossfadeN+1:end) = wav2(end-crossfadeN+1:end).*crossfade_ramp;
        smtx_{ii}=resample(wav2,SamplingRateFinalWav,fs);
        Rms(ii)=rms(wav2);
        Max(ii) = max(abs(smtx_{ii}));
        Wav.seg_nf(ii)=Max(ii);
        Wav.inds(ii,:)=[i,j];
        %smtx_{seg_ind}=smtx_{seg_ind}./Wavseg_nf(seg_ind);

        Names{ii}=dd(j).name(5:end-4);
        
        end
    end
    %% Prepare Wav
    for i=1:length(smtx_)
        smtx_{i}=smtx_{i}./RmsNonSilent(i);
        Max_after_scale(i)=max(smtx_{i});
        RMS_after_scale(i)=rms(smtx_{i});
    end
    for i=1:length(smtx_)
        smtx_{i}=smtx_{i}./max(Max_after_scale);
        Max_after_scale2(i)=max(smtx_{i});
        RMS_after_scale2(i)=rms(smtx_{i});
    end
    Wav.seg_nf = Max_after_scale2;
    keep=min_envs<.04;
    Wav.inds=Wav.inds(keep,:);
    Wav.min_envs=min_envs(keep);
    Wav.smtx=smtx_(keep);
    Wav.seg_nf=Wav.seg_nf(keep);
    Wav.Nsamples=cellfun(@length,Wav.smtx);
    Wav.fs=SamplingRateFinalWav;
    Wav.Duration=min(Wav.Nsamples)/Wav.fs;%mean(dur_range+st_buf);
    Wav.Names=Names(keep);
    Wav.MaxIndex=length(Wav.smtx);
    Wav.notes=sprintf('Marmoset vocs from Turesson et al 2016');
    vars={'silence_threshs',...
        'silence_time_thresh','max_silence','shortened_silence_range','manual_sil_inds','tadd','fs'};
    for i=1:length(vars)
        Wav.params.(vars{i})=eval(vars{i});
    end
    
    dd=fileparts(which('NaturalStreams'));
    outfile=[filesep 'waveform' filesep 'Marmo.subset1.mat'];
    %save([dd outfile],'-Struct','Wav');
    
    figure;plott(fs,smtx_{1});
    for i=1:length(smtx_)
        pause;
        fprintf('\n%d',i)
        plott(fs,smtx_{i});
    end
end