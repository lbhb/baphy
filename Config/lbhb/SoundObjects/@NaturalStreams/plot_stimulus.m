function plot_stimulus(RefObject,TarObject,ThisTarParams,TrialSound,TrialSamplingRate,RefTrialIndex,TrialIndex,TarIdx,events,TrialEnv)
t=(0:(size(TrialSound,1)-1))/TrialSamplingRate;
plot(t,TrialSound(:,1));
hold on;
sf=max(TrialSound(:,1))-min(TrialSound(:,2));
plot(t,TrialSound(:,2)+sf);

end