function [w,event,e]=waveform (o,index,IsRef,TarObject,ThisTarParams)
% function w=waveform(o, index,IsRef);
%
% generate waveform for SpNoise object
%

if nargin<3, IsRef=1; end
if(nargin<4)
    TarObject=[];
    ThisTarParams=[];
end
bSplitChannels=strcmpi(o.SplitChannels,'Yes');
SamplingRate = get(o,'SamplingRate');
PreStimSilence = get(o,'PreStimSilence');
PreStimN = PreStimSilence * SamplingRate;

TrialName=o.TrialSet{index};
TrialType=TrialName(1);
TrialIndex=str2double(TrialName(2:end));
trial_suffix=['_s',TrialType];

if any(strcmp(TrialType,{'A','X'}))
    sf=ones(size(o.AIndexes{TrialIndex}));
    for ti = 1:length(o.StreamATarInds)
        tar_inds=o.AIndexes{TrialIndex}==o.StreamATarInds(ti);
        sf(tar_inds)=sf(tar_inds)*10^(o.StreamASNRs(ti)/20);
    end
    tar_inds=ismember(o.AIndexes{TrialIndex},o.StreamATarInds);
    if strcmp(o.TargetStream,'A')
        %Do nothing
    elseif strcmp(o.TargetStream,'B')
        tar_inds=tar_inds*2; %2 means catch
    else
        error('There are targets in stream A but it''s not set to be the target or the catch stream')
    end
    [sA,evA] = generate_stream(o.AIndexes{TrialIndex},o.ATokens,sf,o.AOffNs{TrialIndex},o.ANames,tar_inds,trial_suffix);
    sB=zeros(size(sA));
else
    evA=[];
end

if any(strcmp(TrialType,{'B','X'}))
    sf=ones(size(o.BIndexes{TrialIndex}));
    for ti = 1:length(o.StreamBTarInds)
        tar_inds=o.BIndexes{TrialIndex}==o.StreamBTarInds(ti);
        sf(tar_inds)=sf(tar_inds)*10^(o.StreamBSNRs(ti)/20);
    end
    tar_inds=ismember(o.BIndexes{TrialIndex},o.StreamBTarInds);
    if strcmp(o.TargetStream,'A')
        tar_inds=tar_inds*2; %2 means catch
    elseif strcmp(o.TargetStream,'B')
        %Do nothing
    else
        error('There are targets in stream B but it''s not set to be the target or the catch stream')
    end
    [sB,evB] = generate_stream(o.BIndexes{TrialIndex},o.BTokens,sf,o.BOffNs{TrialIndex},o.BNames,tar_inds,trial_suffix);
    if ~any(strcmp(TrialType,{'A','X'}))
        sA=zeros(size(sB));
    end
else
    evB=[];
end

if length(sA)>length(sB)
    sB(end:end+length(sA)-length(sB))=0;
elseif length(sB)>length(sA)
    sA(end:end+length(sB)-length(sA))=0;
end

if o.RelAttenuatedB(1)~=0
    sA = sA * 10^(-o.RelAttenuatedB(1)/20);
end
if o.RelAttenuatedB(2)~=0
    sB = sB * 10^(-o.RelAttenuatedB(2)/20);
end

w=sA;
if bSplitChannels
    w(:,2)=sB;
else
    w=w+sB;
end

w=w*5; %Max in saved waveforms is 1. Multiple by 5 to slace to 80 dB peak SPL


event=[evA evB];
e=[];

function [s,ev] = generate_stream(indexes,tokens,sf,OffNs,Names,tar_inds,trial_suffix)
    s=[]; ev=[];
    
    LastTimeStamp=0;
    for i=1:length(indexes)
        OffN = OffNs(i)-PreStimN;
        if OffN <0
            error('This Token''s post silent interval was shorted than PreStimSilence')
        end
        if isnan(indexes(i))
            s=[s;zeros(PreStimN,1);zeros(OffN,1)];
            Duration = 0;
            PostStimSilence = OffN/SamplingRate;
            thisevent(1) = struct('Note',['PreStimSilence , ' trial_suffix(2:end) '_Silence' trial_suffix],...
                'StartTime',0,'StopTime',PreStimSilence,'Trial',[]);
            thisevent(2) = struct('Note',['Stim , ' trial_suffix(2:end) '_Silence'  trial_suffix],'StartTime'...
                ,PreStimSilence, 'StopTime', PreStimSilence+Duration, 'Trial',[]);
            thisevent(3) = struct('Note',['PostStimSilence , ' trial_suffix(2:end) '_Silence'  trial_suffix],...
                'StartTime',PreStimSilence+Duration, 'StopTime',PreStimSilence+Duration+PostStimSilence,'Trial',[]);
        else
            s=[s;zeros(PreStimN,1);tokens{indexes(i)}*sf(i);zeros(OffN,1)];
            Duration = length(tokens{indexes(i)})/SamplingRate;
            PostStimSilence = OffN/SamplingRate;
            thisevent(1) = struct('Note',['PreStimSilence , ' Names{indexes(i)} trial_suffix],...
                'StartTime',0,'StopTime',PreStimSilence,'Trial',[]);
            thisevent(2) = struct('Note',['Stim , ' Names{indexes(i)} trial_suffix],'StartTime'...
                ,PreStimSilence, 'StopTime', PreStimSilence+Duration, 'Trial',[]);
            thisevent(3) = struct('Note',['PostStimSilence , ' Names{indexes(i)} trial_suffix],...
                'StartTime',PreStimSilence+Duration, 'StopTime',PreStimSilence+Duration+PostStimSilence,'Trial',[]);
        end
        for evi = 1:length(thisevent)
            thisevent(evi).StartTime = thisevent(evi).StartTime + LastTimeStamp;
            thisevent(evi).StopTime  = thisevent(evi).StopTime  + LastTimeStamp;
            if tar_inds(i)==0
                 thisevent(evi).Note =  [thisevent(evi).Note ' , Reference'];
            elseif tar_inds(i)==1
                 thisevent(evi).Note =  [thisevent(evi).Note ' , Target'];
            elseif tar_inds(i)==2
                thisevent(evi).Note =  [thisevent(evi).Note ' , Catch'];
            end
        end
        LastTimeStamp = thisevent(end).StopTime;
        ev=[ev thisevent];%if targets exist, put them at the end to keep convention
    end
    aa=2;
end
end
% 
% F0s=get(o,'F0s');
% RelAttenuatedB=get(o,'RelAttenuatedB');
% SplitChannels=get(o,'SplitChannels');
% bSplitChannels=strcmpi(SplitChannels,'Yes');
% SamplingRate = get(o,'SamplingRate');
% Duration = get(o,'Duration'); % duration is second
% PreStimSilence = get(o,'PreStimSilence');
% PostStimSilence = get(o,'PostStimSilence');
% Names = get(o,'Names');
% SamplingRateEnv = get(o,'SamplingRateEnv');
% BaseSound=strtrim(get(o,'BaseSound'));
% Subsets = get(o,'Subsets');
% BaselineFrac=get(o,'BaselineFrac');
% idxset=get(o,'idxset');
% ShuffledOnsetTimes=get(o,'ShuffledOnsetTimes');
% SingleBandFrac=get(o,'SingleBandFrac');
% 
% EnvVarName=[BaseSound,num2str(Subsets)];
% %emtx = get(o,'emtx');
% emtx = SPNOISE_EMTX.(EnvVarName);
% 
% N=round(Duration*SamplingRate);
% timesamples = (1:N)' / SamplingRate;
% w=zeros(N+(PreStimSilence+PostStimSilence)*SamplingRate,bSplitChannels+1);
% 
% 
% if isempty(RelAttenuatedB) || length(RelAttenuatedB)<length(F0s),
%   RelAttenuatedB=zeros(size(F0s));
% end
% 
% bandcount=length(F0s);
% 
% if ~isempty(TarObject) && strcmp(ThisTarParams.TargetMode,'Envelope')
%     [e,event]=env(o,index,IsRef,TarObject,ThisTarParams);
% else
%     [e,event]=env(o,index,IsRef);
% end
% 
% for bb=1:bandcount,
%     if ~isempty(TarObject) && strcmp(ThisTarParams.TargetMode,'Carrier') && ThisTarParams.TargetChannel==bb
%         error('fix me')
%     else
%         if bb==1
%             formants=o.FormantsA;
%         elseif bb==2
%             formants=o.FormantsB;
%         else
%             error('')
%         end
%         tw=MakeVowel(o,round(o.SamplingRate*o.Duration),o.F0s(bb),o.SamplingRate,formants(1),formants(2),formants(3))';
%         %[tw,F{bb}]=MakeVowel(o,round(o.SamplingRate*o.Duration),o.F0s(bb),o.SamplingRate,formants(1),formants(2),formants(3)); tw=tw';
%         sf(bb)=max(abs(tw));
%         tw=tw./sf(bb);
%         %twC(:,bb)=tw;
%        %tw=double(Klatt_wapper(o,o.Duration,o.F0s(bb)*3,o.SamplingRate,formants*3));
%        %tw=tw./max(abs(tw));
%        
%     end
%     
%     %tw=tw./max(abs(tw(:)));%normalize to 1 so that there can be no clipping.
%     nf(bb)=max(abs(tw(:)));
%     
%     % Now, put it in the silence:
%     tw = [zeros(PreStimSilence*SamplingRate,1) ; 
%     tw ; zeros(PostStimSilence*SamplingRate,1)];
%   
%     % apply envelope
%     tw=tw.*e(:,bb);
%     
%     if SingleBandFrac && max(abs(tw))>0,
%        tw=tw./max(abs(tw));
%     end
%     
%     % adjust level relative to other bands
%     %done in env. Good?
%     %level_scale=10.^(-RelAttenuatedB(bb)./20);
%     level_scale=1/o.mean; %scale to so that mean envelope is 1. Thus, RMS will be 1, turned into 5 later = 80 dB?
%     tw_(:,bb)=tw.*level_scale;
% end
% 
% % if all(cellfun(@(x)isa(x,'ComplexTone'),o.CarrierObj))
% %     sum_amps=cellfun(@(x)sum(get(x,'ComponentAmplitudes')),o.CarrierObj);
% % elseif isa(o.CarrierObj{1},'ComplexTone')
% %     %hack for ComplexTone vs Noise
% %     sum_amps=cellfun(@(x)sum(get(x,'ComponentAmplitudes')),o.CarrierObj(1));
% %     sum_amps=sum_amps([1 1]);
% % end
% %  nfs=sum_amps * max(nf)/max(sum_amps);
% %rms_per=cellfun(@(x)sqrt(sum(get(x,'ComponentAmplitudes').^2)/2),o.CarrierObj);
% %nfs=rms_per * max(nf) / max(rms_per);
% for bb=1:bandcount
%     if bSplitChannels
%         w(:,bb)=tw_(:,bb);%/nfs(bb);
%     else
%         %w=w+tw.*level_scale;
%         w=w + tw_(:,bb);%/nfs(bb);
%     end
% end
% %want this?
% %w=w./max(abs(w(:)));
% % 10ms ramp at onset and offset:
% %done in env. Good?
% %ramp = hanning(round(.01 * SamplingRate*2));
% %ramp = ramp(1:floor(length(ramp)/2));
% %ramp=repmat(ramp,[1,chancount]);
% %w(1:length(ramp),:) = w(1:length(ramp),:) .* ramp;
% %w(end-length(ramp)+1:end,:) = w(end-length(ramp)+1:end,:) .* flipud(ramp);
% 
% % normalize min/max +/-5
% %w = 5 ./ max(abs(w(:))) .* w;
% w=w*5;%5 is 80 dB. Both envelope and carrier max out at 1.
% 
% % generate the event structure:
% %event = struct('Note',['PreStimSilence , ' Names{index}],...
% %    'StartTime',0,'StopTime',PreStimSilence,'Trial',[]);
% %event(2) = struct('Note',['Stim , ' Names{index}],'StartTime'...
% %    ,PreStimSilence, 'StopTime', PreStimSilence+Duration, 'Trial',[]);
% %event(3) = struct('Note',['PostStimSilence , ' Names{index}],...
% %    'StartTime',PreStimSilence+Duration, 'StopTime',PreStimSilence+Duration+PostStimSilence,'Trial',[]);
% 
% if 0
%     %%
%     [h,f] = freqz(F{1},2^13,SamplingRate);
%     [h(:,2),f(:,2)] = freqz(F{2},2^13,SamplingRate);
%     h(:,1)=h(:,1)./sf(1);
%     h(:,2)=h(:,2)./sf(2);
%     figure;pwelch(twC,[],[],[],SamplingRate);
%     H=20*log10(abs(h));
%     hold on;ph=plot(f/1000,H-max(H(:)),'-');
%     axis([0 8  -80 0])
%     ax=gca;
%     F0s=get(o,'F0s');
%     legend(ax(1),ph,{['"/a/" F0 ',num2str(F0s(1))],['"/i/" F0 ',num2str(F0s(2))]},'FontSize',16,'Location','E')
%     set(gca,'FontSize',14)
%     A=load('U:\luke\Projects\SPS\TestSounds\MarmoAudiogram.mat');
%     Maximize(gcf);
%     
%     [ax(2),hpl]=plot_right(gca,A.F,A.Th,'Color',cbcolor(3),'LineWidth',2);
%     set(ax(2),'YDir','reverse')
%     set(ax(2),'YLim',min(A.Th)+[0 diff(get(ax(1),'YLim'))])
%     set(ax,'Box','off','TickDir','out');
%     legend(ax(1),[ph;hpl],{['"/a/" F0 ',num2str(o.F0s(1)),', F3 shifted'],['"/i/" F0 ',num2str(o.F0s(2))],'Marmo Audiogram'},'FontSize',16,'Location','E')
%     set(ax(2),'FontSize',14,'XTickLabel','');
%     ylabel(ax(2),'Threshold (dB SPL)','FontSize',14)
%     
%     
%     figure;%pwelch(twC,[],[],[],SamplingRate);
%     H=20*log10(abs(h));
%     H=H-max(H(:));
%     Acorr=interp1(A.F,A.Th,f/1000);
%     Hc=H-Acorr+min(Acorr)+83.81;
%     ph=plot(f/1000,Hc,'-');
%     axis([0 8  0 80])
%     ax=gca;
%     F0s=get(o,'F0s');
%     legend(ax(1),ph,{['"/a/" F0 ',num2str(F0s(1)),', F3 shifted'],['"/i/" F0 ',num2str(F0s(2))]},'FontSize',16,'Location','E')
%     set(gca,'FontSize',14)
%     Maximize(gcf);
%     set(ax,'Box','off','TickDir','out'); 
%     xlabel('Frequency (kHz)','FontSize',14)
%     ylabel('Level re Threshold (dB)','FontSize',14)
%     grid on
%     
% end