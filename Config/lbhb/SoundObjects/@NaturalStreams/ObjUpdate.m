function o = ObjUpdate (o)

StreamA=strtrim(o.StreamA);
StreamB=strtrim(o.StreamB);

object_spec = what(class(o));
wavpath = [object_spec(1).path filesep 'waveform' filesep];

WavAFileName=[wavpath StreamA '.subset',num2str(o.StreamASubset),'.mat'];
WavAVarName=[StreamA,num2str(o.StreamASubset)];
A=load(WavAFileName,'MaxIndex');

WavBFileName=[wavpath StreamB '.subset',num2str(o.StreamBSubset),'.mat'];
WavBVarName=[StreamB,num2str(o.StreamBSubset)];
B=load(WavBFileName,'MaxIndex');


% saveseed=rng;%saveseed=rand('seed');
% rng('default');
% rng(o.Seeds(1));%rand('seed',Subsets*20);

% if o.StreamASub_Subset==0
%     o.RepASet=1:A.MaxIndex;
%     o.RepASet=setdiff(o.RepASet,o.StreamATarInds);
%     if o.StreamARepeats>0
%         o.RepASet=[o.RepASet repmat(o.StreamARepeatInds,1,o.StreamARepeats)];
%     end
% end
%    
% if o.StreamBSub_Subset==0
%     o.RepBSet=1:B.MaxIndex;
%     o.RepBSet=setdiff(o.RepBSet,o.StreamBTarInds);
%     if o.StreamBRepeats>0
%         o.RepBSet=[o.RepBSet repmat(o.StreamBRepeatInds,1,o.StreamBRepeats)];
%     end
% end

% Names=cell(1,MaxIndex);
% for ii=1:MaxIndex,
%     Names{ii}=['T'];
%     for bb=1:bandcount,
%         if idxset(ii,bb)
%             Names{ii}=[Names{ii} '+' Env.Names{idxset(ii,bb)}];
%         else
%             Names{ii}=[Names{ii} '+null'];
%         end
%     end
% end
% 
% o = set(o,'Names',Names);

%rng(saveseed)