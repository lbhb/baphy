function w=GenerateEventWindowsExtras(ReferenceHandle,w,exptparams,StimEvents)
refnum=0;
for cnt1 = 1:length(StimEvents)
    [Type, Note, StimRefOrTar]=ParseStimEvent(StimEvents(cnt1));
    if strcmpi(Type,'Stim')
        if strcmpi(StimRefOrTar,'Reference')
            refnum=refnum+1;
            if strcmp(Note(1:3),'sA_')
               w.RefStream(refnum)=1;
            elseif strcmp(Note(1:3),'sB_')
               w.RefStream(refnum)=2;
            else
                error('Unknown ref stream')
            end
        end
    end
end

w.two_target=true;

