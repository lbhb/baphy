function o = ObjUpdate (o)
%

par = get(o);

saveseed=rand('seed');
rand('seed',par.SoundIndexes(1)*20);

% get the full path to the files
object = what('NaturalPairsTest');
ObjPath = object.path;                            
soundpaths =  dir(strcat(ObjPath,'/NatPairSounds/*.wav'));

MaxIndex=length(par.SoundIndexes);

Names=cell(1,MaxIndex);
for ii=1:MaxIndex
    Names{ii}=soundpaths(par.SoundIndexes(ii)).name;
end

% o = set(o, 'SoundIndexes', par.SoundIndexes);
o = set(o,'Names',Names);
o = set(o, 'SoundPath', strcat(ObjPath,'/NatPairSounds'));
o = set(o,'MaxIndex', length(Names));


% return random seed to previous state
rand('seed',saveseed);

end
