function [w, event]=waveform (o,index,IsRef, TarObject, ThisTarParams)
%
% generate waveform for NaturalPairsTest. each Trial has a unique sound of
% the specified lenght, preceded and followed by silence
%

par = get(o);

if nargin<4, TarObject=[]; end
if nargin<3, IsRef = 1; end

% creates Natural-Sound snippets wtih the specified start and end point
num_waves = length(par.SoundIndexes);
waves(num_waves) = struct('wave', [],...
                  'stim_name', []);
% loads, resamples, slices, scales and holds in struct
for ff = 1:num_waves
    loadpath = strcat(par.SoundPath, '/', par.Names{ff});
    [wav, fs] = audioread(loadpath);
    [p,q] = rat(par.SamplingRate/fs,0.0001);
    resamp_wav = resample(wav,p,q);

    slice_start = (par.SamplingRate * par.SliceStart);
    slice_end = slice_start + (par.SamplingRate * par.Duration) ;
    sliced = resamp_wav(slice_start+1: slice_end); % add one in case starting from zero

    sliced = 5 ./ max(abs(sliced(:))) .* sliced;

    waves(ff).wave = sliced;
    waves(ff).name = par.Names{ff};
end

% get the right wave depending on index
w = waves(index).wave;

% Normalizes to 5 ~ 80 db 
% w = 5 ./ max(abs(w(:))) .* w;

% Now, put it in the silence:
w = [zeros(par.PreStimSilence * par.SamplingRate, 1);...
     w(:);...
     zeros(par.PostStimSilence * par.SamplingRate, 1)];

 % initialize the event struct with the right number of elements
event(3) = struct('Note', [], 'StartTime', [], 'StopTime', [], ...
                            'Trial', []);

% generate the event structure:
name = par.Names{index};


event(1) = struct('Note',['PreStimSilence , ' name],...
               'StartTime',0,...
               'StopTime',par.PreStimSilence,...
               'Trial',[]);
event(2) = struct('Note',['Stim , ' name],...
                  'StartTime',par.PreStimSilence,...
                  'StopTime', par.PreStimSilence + par.Duration,...
                  'Trial',[]);
event(3) = struct('Note',['PostStimSilence , ' name],...
                  'StartTime',par.PreStimSilence + par.Duration, ...
                  'StopTime',par.PreStimSilence+ par.Duration +...
                  par.PostStimSilence,...
                  'Trial',[]);
end
