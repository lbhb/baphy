function o = NaturalPairsTest (varargin)

% Nima Mesgarani, Apr 2007

switch nargin
case 0
    % if no input arguments, create a default object
     s = SoundObject ('NaturalPairsTest', 100000, 0, 0, 0, {}, 1, ...
                     {'SoundIndexes', 'edit', 1:16,...
                      'PreStimSilence', 'edit', 0.5,...
                      'PostStimSilence', 'edit', 0.5,...
                      'Duration','edit',1,...
                      'SliceStart', 'edit', 0,...
                      'RefRepCount','edit',1});   
    
                  
    o.SoundIndexes = 1:16;
    o.Duration = 1;
    o.PreStimSilence = 0.5;
    o.PostStimSilence = 0.5;
    o.SamplingRate=100000;
    o.SliceStart = 0;
    o.RefRepCount=1;
    o.sequences = [];
    o.MaxIndex = 0;
    o.SoundPath = '';

      
    %
    o = class(o,'NaturalPairsTest',s);  
    o = ObjUpdate(o);
case 1
    % if single argument of class SoundObject, return it
    if isa(varargin{1},'NaturalPairsTest')
        o = varargin{1};
    else
        error('Wrong argument type');
    end
otherwise
    error('Wrong number of input arguments');
end