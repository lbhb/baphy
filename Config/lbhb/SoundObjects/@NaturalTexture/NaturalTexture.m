function o = NaturalTexture(varargin)
% ripped off of NaturesSounds, wrapper for Textures generated from
% McDermott lab toolbox
%
switch nargin
case 0
    % if no input arguments, create a default object
    s = SoundObject('NaturalTexture', 48000, 0, 0, 0, {}, 1, ...
                    {'Duration','edit',3,'Subsets','edit',1,...
                     'RepIdx','edit',[0 1]});
    o.Subsets = 1;
    o.SNR=100;
    o.NoiseType = 'White';
    o.ReverbTime = 0;
    o.Duration = 3;
    o.idxset=[];
    o.RepIdx=[0 1];
    o.SoundPath = '';
    o = class(o,'NaturalTexture',s);
    o = ObjUpdate (o);
case 1
    % if single argument of class SoundObject, return it
    if isa(varargin{1},'NaturalTexture')
        o = varargin{1};
    else
        error('Wrong argument type');
    end
otherwise
    error('Wrong number of input arguments');
end