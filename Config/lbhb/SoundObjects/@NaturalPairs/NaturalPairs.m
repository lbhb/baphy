function o = NaturalPairs(varargin)
% Natural Pairs is a variation of the ContextProbe paradigm
%   properties:
%   SoundIndex: indexes listing the sounds to be used building the stimulus
%               as ordererd in alphabetical manner
%   PreStimSilcence: In seconds
%   PostStimSilence: In seconds
%   Duration: Duration in seconds of the individuale sound snippets used to
%             build the sound sequence
%   SequenceStructure: The order in which the sound snippets are organized
%             in the sequences 
%   SliceStart: In seconds. Point at which start slicing the original
%             sounds to generate the snippets 
%   SamplingRate
%
%
% methods: waveform, set, get
% 

switch nargin
case 0
    % if no input arguments, create a default object
    s = SoundObject ('NaturalPairs', 100000, 0, 0, 0, {}, 1, ...
                     {'SoundIndexes', 'edit', [1, 2],...
                      'Duration','edit', 1,...
                      'SequenceStructure', 'popupmenu', 'AllPermutations|Triplets',...
                      'SliceStart', 'edit', 0});
                  
    o.SoundIndexes = [1, 2];
    o.Duration = 1;
    o.PreStimSilence = 1;
    o.PostStimSilence = 1;
    o.SamplingRate=100000;
    o.SequenceStructure = 'Triplets';
    o.SliceStart = 0;
    o.sequences = [];
    o.stimuli = [];
    o.MaxIndex = 0;
    o.SoundPath = '';
      
    %
    o = class(o,'NaturalPairs',s);  
    o = ObjUpdate(o);
    
case 1
    % if single argument of class SoundObject, return it
    if isa(varargin{1},'NaturalPairs')
        o = varargin{1};
    else
        error('Wrong argument type');
    end
otherwise
    error('Wrong number of input arguments');
end