function o = ObjUpdate (o)
%

par = get(o);

saveseed=rand('seed');
rand('seed',par.SoundIndexes(1)*20);

stimuli_count = length(par.SoundIndexes);    
stimuli(stimuli_count) = struct('sound_idx', [],...
                                'sound_name', []);

% get the full path to the files
object = what('NaturalPairs');
ObjPath = object.path;                            
Nat_files =  dir(strcat(ObjPath,'/NatPairSounds/*.wav'));

counter = 1;
for ee = 1:stimuli_count
    sound_idx = par.SoundIndexes(ee);
    stimuli(counter).sound_idx = sound_idx;
    stimuli(counter).sound_name = Nat_files(sound_idx).name;
    counter = counter + 1;
end   
    

if strcmp('AllPermutations', strtrim(par.SequenceStructure))
    % as Taken from Asari and Zador: Each of the four sounds is
    % presented preceded by every other sound and silence a single time
    % Aditionally, each sound is used botha as a context and a probe
    % Thise is the most efficient way of packing sounds in time.
    % These sequences were defined using a python script made by MLE
    % and are static since calculating them anew can take many minutes.
    switch length(par.SoundIndexes)
        case 2
            sequences = [1 2 2;
                         2 1 1];
        case 3
            sequences = [1 2 2 1;
                         2 3 3 2;
                         3 1 1 3];
        case 4
            sequences = [1 3 2 4 4;
                         3 4 1 1 2;
                         4 2 3 3 1;
                         2 2 1 4 3];
        case 5
            sequences = [1 2 1 1 3 1;
                         2 3 3 2 4 2;
                         3 4 1 4 4 3;
                         4 5 1 5 5 4;
                         5 2 2 5 3 5];
        case 6
            sequences = [1 2 3 4 4 5 6;
                         2 1 5 3 3 6 4;
                         3 1 4 2 6 6 5;
                         4 3 5 5 1 6 2;
                         5 2 2 4 6 1 3;
                         6 3 2 5 4 1 1];
        case 7
            sequences = [1 2 3 4 4 5 6 7;
                         2 1 3 3 6 4 7 5;
                         4 1 5 7 7 3 2 6;
                         3 5 5 4 2 7 6 1;
                         5 3 1 7 4 6 2 2;
                         7 1 6 6 5 2 4 3;
                         6 3 7 2 5 1 1 4];
        case 8
            sequences = [1 2 3 4 4 5 6 7 8;
                         2 1 3 5 5 7 4 8 6;
                         3 3 1 5 2 8 7 6 4;
                         4 1 6 2 7 3 8 8 5;
                         5 1 4 2 6 8 3 7 7;
                         6 3 2 2 5 8 4 7 1;
                         7 5 4 3 6 6 1 8 2;
                         8 1 1 7 2 4 6 5 3];
        case 9
            sequences = [1 2 3 4 4 5 6 7 8 9;
                         2 1 1 3 5 4 8 6 9 7;
                         3 1 4 2 7 5 9 6 8 8;
                         4 1 6 2 8 5 7 9 3 3;
                         5 5 1 7 2 4 9 8 3 6;
                         6 4 7 7 3 2 9 5 8 1;
                         8 7 4 6 3 9 1 5 2 2;
                         7 1 9 2 6 6 5 3 8 4;
                         9 9 4 3 7 6 1 8 2 5];
        case 10
            sequences = [ 1  2  3  4  5  6  6  7  8  9 10;
                          2  1  3  5  4  6  9  7  7 10  8;
                          3  1  1  4  2  5  7  9  8 10  6;
                          4  1  5  2  8  8  7  6  3 10  9;
                          5  1  6  8  2  9  4  4 10  3  7;
                          6 10  1  7  2  4  8  3  3  9  5;
                          7  3  2 10  5  5  8  1  9  6  4;
                          8  4  7  1 10 10  2  6  5  9  3;
                          9  2  2  7  5 10  4  3  8  6  1;
                         10  7  4  9  9  1  8  5  3  6  2];
        otherwise
            sequences = 'SoundIndex must be a list of 1<lenght<=10 when using AllPermutations';
    end
elseif strcmp('Triplets', strtrim(par.SequenceStructure))
    % in this sequence 1,2,3 and 4,5,6 are triplets of continuosos 
    % snippets from a sound. additional attention is given to
    % 2, 3, 5 and 6 so each of them is presented once after each of
    % the following conditions:
    % Silence (silences is not specified by a number but by the
    %          begginning of line)
    % Differente Sound (generating a sharp transision in sound 
    %          statistics)
    % Preceding sound (generating a continuous transision)
    % Following Sound or Previous non contiguous Sound (generating 
    %         a sharp transition, withoute a strong change in the
    %         sound statistics)
    switch length(par.SoundIndexes)
        case 2
            sequences = [5 6 2 3 5;
                         6 5 3 2 6;
                         2 4 5 4 6;
                         3 1 2 1 3];
        otherwise
            sequences = 'SoundIndex must be a list of lenght==2 when using Triplets';
    end
else
    error('invalid value for SequenceStructure')
end


% generate names for the trials i.e. sequences
Names{size(sequences,1)} = '';
for ii=1:size(sequences,1)
    Names{ii} = sprintf('sequence%03d: %s', ii, num2str(sequences(ii, :)));
end


%o = set(o, 'mean', env_mean);
o = set(o, 'Names',Names);
o = set(o, 'sequences', sequences);
o = set(o, 'stimuli', stimuli);
o = set(o, 'MaxIndex', size(sequences, 1));
o = set(o, 'SoundPath', strcat(ObjPath,'/NatPairSounds'));
    
% return random seed to previous state
rand('seed',saveseed);

end