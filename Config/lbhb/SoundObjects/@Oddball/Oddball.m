function o = Oddball(varargin)
% Oddball is the constructor for the object Oddball which is a child of  
%       SoundObject class
%
% Run class: Oddball
%
% MLE 2018-02-05, based on SSA object.
%
switch nargin
case 0
    % if no input arguments, create a default object
    s = SoundObject ('Oddball', 100000, 0, 0.5, 0.5, ...
        {''}, 1, {'Duration','edit',5,...
        'sound_duration','edit',0.03,...
        'interval_duration','edit',0.3,...
        'SequenceCount','edit',20,...
        'standard_deviant_rate','edit','0.8 0.2',...
        'Frequencies','edit','1000 2000',...
        'Bandwidth','edit',0,...
        'Jitter','popupmenu','On|Off',...
        'envelope', 'popupmenu', 'symetric|ramped|damped',...
        'env_half_life','edit', 1,...
        'MinInterval', 'edit',0.03});
    o.sound_duration = 0.1;
    o.interval_duration = 0.3;
    o.SequenceCount = 20;
    o.standard_deviant_rate = [0.8 0.2];
    o.Bandwidth=0;
    o.Frequencies= [1000 2000]; 
    o.Duration = 5;
    o.Sequences = [];
    o.Jitter = 'Off';
    o.envelope = 'symetric';
    o.env_half_life = 0.016;
    o.MinInterval = 0.01;
    o.Intervals = [];
    o = class(o,'Oddball',s);
    o = ObjUpdate (o);
        
otherwise
    error('Wrong number of input arguments');
end