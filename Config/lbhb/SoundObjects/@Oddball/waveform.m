function [w, event, e]=waveform(o,index,IsRef)
% function w=waveform(t);
% this function is the waveform generator for object Oddball
%
% created MLE 2018-April-26

par=get(o);

if nargin<3, IsRef = 1; end

Names=par.Names;
sound_samples=round(par.sound_duration.*par.SamplingRate);
reg_interval_samples = round(par.interval_duration * par.SamplingRate);
sounds=cell(length(par.Frequencies),1);
sound_names=cell(length(par.Frequencies),1);
rampon = linspace(0,1,round(par.SamplingRate.*0.005))';
rampoff = linspace(1,0,round(par.SamplingRate.*0.005))';

% create the temporally asimetric envelopes
% damped envelope = A * exp(c*(t/hl))
% A is the starting amplitude, hl is the halflife and c is a 
% constant -0.693147, to transform from hl to time constant in ms 
% Patterson. The Journal of the Acoustical Society of America 1994

half_life = par.env_half_life;
hc_cnt = -0.693147;
sound_strech = linspace (0, par.sound_duration, sound_samples)';
damped_envelope = 1 * exp (hc_cnt * (sound_strech / half_life));
ramped_envelope = flipud(damped_envelope);

% create the two wave forms
for ff=1:length(par.Frequencies)
    f0=par.Frequencies(ff);
    % either noise burst or pure tones
    if par.Bandwidth>0
        lf=round(2.^(log2(f0)-par.Bandwidth./2));
        hf=round(2.^(log2(f0)+par.Bandwidth./2));
        sounds{ff}=(BandpassNoise(lf,hf,par.sound_duration,par.SamplingRate));
        sounds{ff}(1:length(rampon))=sounds{ff}(1:length(rampon)).*rampon;
        sounds{ff}((end-length(rampoff)+1):end)=...
            sounds{ff}((end-length(rampoff)+1):end).*rampoff;
    else
        tt=(0:(sound_samples-1))'./par.SamplingRate;
        sounds{ff}= sin(2*pi*par.Frequencies(ff)*tt);
    end
    
    % add asimetric envelope if necesary
    if strcmp(strtrim(par.envelope), 'ramped')
        sounds{ff} = sounds{ff} .* damped_envelope;
    elseif strcmp(strtrim(par.envelope), 'damped')
        sounds{ff} = sounds{ff} .* ramped_envelope;
    elseif strcmp(strtrim(par.envelope), 'symetric')
        
    else
        disp('error with envelope type strings')
    end
    
    sound_names{ff}=num2str(f0);
end

SeqPerRate=ceil(par.SequenceCount./length(par.standard_deviant_rate));
Sequence=par.Sequences(:,index);
SeqNum=ceil(index/SeqPerRate);
current_time_sample=0;
w=zeros(round(par.Duration * par.SamplingRate),1);

% initialize the event struct with the right number of elements
soundsPerTrial=floor(par.Duration./(par.sound_duration+par.interval_duration));
struct_size = soundsPerTrial*3 + 3; % one per trial, two pre pre and post silence
event(struct_size) = struct('Note', [], 'StartTime', [], 'StopTime', [], ...
                            'Trial', []);
    
for ii=1:length(Sequence)
  
    % set the sample position in which the sound starts with
    % special cases for the initial and last sounds
    if ii == 1
        %forces sound to 1/2 stardard interval after start
        sound_start_sample = round(reg_interval_samples / 2);
        
    elseif ii == length(Sequence)
        % forces the last sound to the same position regardless of jitter
        % status, i.e. as if the sequences was regular  
        sound_start_sample = round((soundsPerTrial - 1) * ...
                             (sound_samples + reg_interval_samples) + ...
                             (reg_interval_samples/2));   
        %sound_start_sample = size(w,1) - round(reg_interval_samples/2) - sound_samples;
        
    else
        interval_samples = round(par.Intervals(ii-1) * par.SamplingRate);
        sound_start_sample = current_time_sample + interval_samples;
        
    end 
    
    % adds sound to wave w
    w((1:sound_samples) + sound_start_sample) = sounds{Sequence(ii)};

    % Generates the event name, either onset, standard or deviant
    if ii==1
        nn=sprintf('%s+ONSET',sound_names{Sequence(ii)});
    elseif Sequence(ii)==1
        nn=sprintf('%s+%.2f',sound_names{Sequence(ii)},par.standard_deviant_rate(SeqNum));
    elseif Sequence(ii)==2
        nn=sprintf('%s+%.2f',sound_names{Sequence(ii)},par.standard_deviant_rate(3-SeqNum));
    end


    % and generate the event structure:
    % current time describes the time of the sound onset
    sound_onset_time = par.PreStimSilence + (sound_start_sample / par.SamplingRate);
    sound_dur = par.sound_duration;
    interval_dur = par.interval_duration;
    
    prefixes = {'SubPreStimSilence , '; 'SubStim , '; 'SubPostStimSilence , '};
    % indexes are in group of three, and offset by 3 to make space for
    % whole trial events later on
    event((ii-1)*3+4) = struct('Note',[prefixes{1} nn],...
        'StartTime',sound_onset_time - interval_dur / 2, ...
        'StopTime',sound_onset_time,...
        'Trial', []);
    event((ii-1)*3+5) = struct('Note',[prefixes{2} nn],...
        'StartTime',sound_onset_time,...
        'StopTime', sound_onset_time + sound_dur,...
        'Trial', []);
    event((ii-1)*3+6) = struct('Note',[prefixes{3} nn],...
        'StartTime',sound_onset_time + sound_dur,...
        'StopTime',sound_onset_time + sound_dur + interval_dur / 2,...
        'Trial',[]); 
    
    % adds offset to current_time_sample
    current_time_sample = sound_start_sample + sound_samples;
end

% generates envelope
[e, ~] = env(o, index,IsRef);

% normalize min/max +/-5
w = 5 ./ max(abs(w(:))) .* w;

% Now, put it in the silence:
w = [zeros(par.PreStimSilence*par.SamplingRate,1) ; w(:) ;zeros(par.PostStimSilence*par.SamplingRate,1)];

prefixes = {'PreStimSilence , '; 'Stim , '; 'PostStimSilence , '};
% add initial PreStimSilence
event(1) = struct('Note',[prefixes{1} Names{index}],...
   'StartTime', 0,...
   'StopTime',par.PreStimSilence,...
   'Trial', []);
% add whole sequences as single reference event
event(2) = struct('Note',[prefixes{2} Names{index}],...
   'StartTime', par.PreStimSilence,...
   'StopTime', par.PreStimSilence + par.Duration,...
   'Trial', []);
% add final PostStimSilence
event(3) = struct('Note',[prefixes{3} Names{index}],...
   'StartTime',par.PreStimSilence + par.Duration,...
   'StopTime',par.PreStimSilence + par.Duration + par.PostStimSilence,...
   'Trial', []);

end