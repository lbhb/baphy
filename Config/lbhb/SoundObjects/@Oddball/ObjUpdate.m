
function o = ObjUpdate (o)
% This function is used to recalculate the properties of the object that
% depends on other properties. In these cases, user firs changes the
% properties using set, and then calls ObjUpdate. The default is empty, you
% need to write your own.

% MLE 2018-02-05

par=get(o);

SequenceCount=par.SequenceCount;
% this should be deprecated, always using only two stimulus
SeqPerRate=ceil(SequenceCount./length(par.standard_deviant_rate));
Names=cell(1,SequenceCount);
sounds_per_trial=floor(par.Duration./(par.sound_duration+par.interval_duration));
s=rand('state');
rand('state',sounds_per_trial*SequenceCount);

if strcmp(strtrim(par.Jitter), 'On')
    % draws random values from an uniform distribution,between the begining and
    % end of the random sound period. this values are the time onsets of tones,
    % this is done in a pseudorandom manner, constraining the results to trial
    % duration conditions. the distribution of the sound onsets follows then a
    % poisson.
    rand_sound_period = par.Duration - par.sound_duration*2 - par.interval_duration;
    expected_duration = rand_sound_period;

    % randomises intervals until they fit the trial duration constrain
    while expected_duration >= rand_sound_period

        rand_start_times = sort(random('Uniform', 0, rand_sound_period, [sounds_per_trial-2,1]))';
        rand_intervals = diff([0, rand_start_times]);
        expected_duration = sum(rand_intervals) + length(rand_intervals) * ...
            par.sound_duration + par.MinInterval;
    end

    % adjust length of short intervals to Mininteval, substracts the difference
    % fromm longest random interval to preserve duration of the trial. 
    while min(rand_intervals) < par.MinInterval
        [m, mm] = min(rand_intervals);
        [M, MM] = max(rand_intervals);
        rand_intervals(mm) = par.MinInterval;
        rand_intervals(MM) = M - (par.MinInterval - m);
    end
    Intervals = rand_intervals';
elseif strcmp(strtrim(par.Jitter), 'Off')
    % sets as list of regular intervals
    Intervals = zeros(sounds_per_trial - 2, 1) + par.interval_duration;
else
    disp('something wrong with par.Jitter string')
end    

% defines first block of sequences with sound 1 as standard. pseudorandomly
% selects the identity of the sounds, by shufling sequences with ordered
% sounds at the adecuate rate. forces the onset to always be standard
deviants_per_trial = ceil(sounds_per_trial * par.standard_deviant_rate(2));
half_count = ceil(SequenceCount/2);
first_half = zeros(sounds_per_trial - 1, half_count);
first_half(1:deviants_per_trial, :) = 1;

% interatively shuffles every column of the first half of sequences 
for cc=1:size(first_half,2)
    rand_idx = randperm(size(first_half,1));
    first_half(rand_idx,cc) = first_half(:,cc);
end
% add first sound as standard
first_half = [zeros(1,size(first_half,2))', first_half']';

second_half = not(first_half);
% sets numbers to 1 and 2, as required by waveform
Sequences = [first_half, second_half] + 1;
% forces the number of Sequences to the defined sequence count
Sequences = Sequences(:, 1:SequenceCount);

% sets names of the sequences
for nn = 1:SequenceCount
    SeqNum=ceil(nn/SeqPerRate);
    Names{nn}=sprintf('Seq%03d+F1%.2f',nn,par.standard_deviant_rate(SeqNum));
end

rand('state',s);

o = set(o,'MaxIndex',SequenceCount);
o = set(o,'SamplingRate',100000);
o = set(o,'Names',Names);
o = set(o,'Sequences',Sequences);
o = set(o,'Intervals',Intervals);
