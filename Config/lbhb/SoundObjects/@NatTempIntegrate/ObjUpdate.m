function o = ObjUpdate(o)
%

par = get(o);

saveseed=rand('seed');
soundset = par.Subsets;
object_spec = what('NatTempIntegrate');

if soundset==1  %Natrual sounds full library
   soundpath = [object_spec(1).path filesep 'sounds'];
else
   error('soundset>1 not supported');
end

temp = dir([soundpath filesep '*.wav']);
UNames = cell(1,length(temp));
[UNames{:}] = deal(temp.name);

MaxIndex=length(UNames);
idxset=(1:MaxIndex)';

Names=cell(1,MaxIndex);
for ii=1:MaxIndex
    Names{ii}=UNames{idxset(ii)};
end

orders = load([soundpath filesep 'stim-orders20' filesep 'r1.mat']);
stim_order_string=orders.stim_order;
stim_order = zeros(size(stim_order_string));
for j = 1:size(stim_order,2)
   for i = 1:size(stim_order,1)
      stim_order(i,j) = find(strcmp(Names,[stim_order_string{i,j},'.wav']));
   end
end

o = set(o,'stim_order',stim_order);
o = set(o,'Names',Names);
o = set(o,'idxset',idxset);
o = set(o,'SoundPath',soundpath);
o = set(o,'MaxIndex', length(Names));

% return random seed to previous state
rand('seed',saveseed);

end