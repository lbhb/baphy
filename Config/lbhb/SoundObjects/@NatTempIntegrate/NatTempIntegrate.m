function o = NatTempIntegrate(varargin)
% Natural Temporal Integration is SNH's set of natural sound segments
% stitched together to test temporal integration properties in AC
%   SoundIndex: indexes listing the sounds to be used building the stimulus
%               as ordererd in alphabetical manner
%   PreStimSilcence: In seconds
%   PostStimSilence: In seconds
%   Duration: Duration in seconds of the individuale sound snippets used to
%             build the sound sequence
%   SequenceStructure: The order in which the sound snippets are organized
%             in the sequences 
%   SliceStart: In seconds. Point at which start slicing the original
%             sounds to generate the snippets 
%   SamplingRate
%
%There are twelve 10-second stimuli, which should get repeated as many times as is practical. We could consider expanding the stimulus set at some point, but would probably vote to start with this set, which we've already successfully tested. The attached zip files have MAT files with a suggested ordering of stimuli (though I don't think this is super critical). Each "run" file has a 12 x 4 cell matrix, where each column corresponds to a random ordering of the 12 stimuli. You can just iterate through the four columns and when you're done, move on to the next run file.
%
% methods: ObjectUpdate, waveform, set, get
% 

switch nargin
case 0
    % if no input arguments, create a default object
    s = SoundObject('NatTempIntegrate', 100000, 0, 0, 0, {}, 1, ...
       {'Subsets', 'edit', 1,...
       'PreStimSilence', 'edit', 1,...
       'PostStimSilence', 'edit', 1,...
       'Duration','edit', 10,...
       });
        
    o.Duration = 10;
    o.PreStimSilence = 1;
    o.PostStimSilence = 1;
    o.SamplingRate=100000;
    
    o.Subsets = 1;
    o.stim_order = {};
    o.MaxIndex = 0;
    o.SoundPath = '';
    o.Names = {};
    o.idxset = [];
    
    % deprecated
    o.SoundIndexes = [1, 2];
    o.SequenceStructure = 'Triplets';
    o.SliceStart = 0;
    o.sequences = [];
    o.stimuli = [];
    %
    o = class(o,'NatTempIntegrate',s);  
    o = ObjUpdate(o);
    
case 1
    % if single argument of class SoundObject, return it
    if isa(varargin{1},'NatTempIntegrate')
        o = varargin{1};
    else
        error('Wrong argument type');
    end
otherwise
    error('Wrong number of input arguments');
end