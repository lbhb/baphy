function [w, event]=waveform (o,index,IsRef, TarObject, ThisTarParams)
%
% generate waveform for ContectProbe object
%

par = get(o);

if nargin<4, TarObject=[]; end
if nargin<3, IsRef = 1; end

% check soud number and sequence numbers are compatible
if strcmp('AllPermutations', strtrim(par.SequenceStructure)) &&...
          length(par.SoundIndexes) ~= 4
    error('SoundIndexes must be a list of 4 when using "AllPermutations"');
elseif strcmp('Triplets', strtrim(par.SequenceStructure)) &&...
        length(par.SoundIndexes) ~= 2
    error('SoundIndexes must be a list of 2 when using "Triplets"');
end



% creates the snippets of natural sounds to later organize in sequences

% 1 second snippets from 4 different souds
if strcmp('AllPermutations', strtrim(par.SequenceStructure))
    nat_wave(4) = struct('wave', [],...
                         'stim_name', []);
    % loads, resamples, slices, scales and holds in struct
    for ff = 1:4
        loadpath = strcat(par.SoundPath, '/', par.stimuli(ff).sound_name);
        [wav, fs] = audioread(loadpath);
        [p,q] = rat(par.SamplingRate/fs,0.0001);
        resamp_wav = resample(wav,p,q);

        slice_start = (par.SamplingRate * par.SliceStart);
        slice_end = slice_start + (par.SamplingRate * par.Duration) ;
        sliced = resamp_wav(slice_start+1: slice_end); % add one in case starting from zero
        
        sliced = 5 ./ max(abs(sliced(:))) .* sliced;

        nat_wave(ff).wave = sliced;
        nat_wave(ff).name = par.stimuli(ff).sound_name;
    end

% 3 sequential 1 second snippets, from 2 different sounds. 6 snippets
% total
elseif strcmp('Triplets', strtrim(par.SequenceStructure))
    nat_wave(6) =  struct('wave', [],...
                         'stim_name', []);
    stim_counter = 1;
    for ff = 1:2
        loadpath = strcat(par.SoundPath, '/', par.stimuli(ff).sound_name);
        [wav, fs] = audioread(loadpath);
        [p,q] = rat(par.SamplingRate/fs,0.0001);
        resamp_wav = resample(wav,p,q);
        % makes te contiguous snippets
        slice_counter = 0;
        for ss = 1:3
            slice_start = (par.SamplingRate * (par.SliceStart + slice_counter));
            slice_end = slice_start + (par.SamplingRate * par.Duration) ;
            sliced = resamp_wav(slice_start+1: slice_end); % add one in case starting from zero

            sliced = 5 ./ max(abs(sliced(:))) .* sliced;
           
            nat_wave(stim_counter).wave = sliced;
            slice_name = strcat(par.stimuli(ff).sound_name, '-slice_', num2str(slice_counter));
            nat_wave(stim_counter).name = slice_name;
            stim_counter = stim_counter + 1;
            slice_counter = slice_counter + 1;         
        end
    end
end

stimuli = nat_wave;

% save random seeds for reproducibility
saveseed=rand('seed');
savenseed=randn('seed');
rand('seed',index*20);
randn('seed',index*20);

% Duration of single stim times number of stims pertrial
sound_samples = round(par.Duration * par.SamplingRate);
N= sound_samples * size(par.sequences,2);
w=zeros(N,1);


sequence = par.sequences(index,:);

% initialize the event struct with the right number of elements
stim_num = length(sequence);  
struct_size = stim_num * 3 + 3; % plus one per sequences, two pre pre and post silence
event(struct_size) = struct('Note', [], 'StartTime', [], 'StopTime', [], ...
                            'Trial', []);
current_time_sample = 0;
for ii = 1:stim_num

    % get the corre 
    current_stim_idx = sequence(ii);
    tw = nat_wave(current_stim_idx).wave;
    stim.stim_name = nat_wave(current_stim_idx).name;
    
    w((1:sound_samples) + current_time_sample) = tw;  

    % set SubRef names and add to event structure
    if current_stim_idx == 1
        context = 'silence';
    else
        context = stimuli(current_stim_idx-1).stim_name;
    end
    probe = stim.stim_name;

    note = [' probe:', probe, '_context:', context]; 
    %note = probe;
    
    prefixes = {'SubPreStimSilence , '; 'SubStim , '; 'SubPostStimSilence , '};
    %prefixes = {'PreStimSilence , '; 'Stim , '; 'PostStimSilence , '}
     
    start_time = (current_time_sample/par.SamplingRate) + par.PreStimSilence;
    stop_time = start_time + par.Duration;   
    
    event((ii-1)*3 + 4) = struct('Note', [prefixes{1} note],...
                         'StartTime', start_time, 'StopTime', start_time,...
                         'Trial', []);
    event((ii-1)*3 + 5) = struct('Note', [prefixes{2} note],...
                          'StartTime', start_time, 'StopTime', stop_time,...
                         'Trial', []);  
    event((ii-1)*3 + 6) = struct('Note', [prefixes{3} note],...
                         'StartTime', stop_time, 'StopTime', stop_time,...
                         'Trial', []);  
                   
    current_time_sample = current_time_sample + sound_samples;
end

% Normalizes to 5 ~ 80 db 
w = 5 ./ max(abs(w(:))) .* w;

% Now, put it in the silence:
w = [zeros(par.PreStimSilence * par.SamplingRate, 1);...
     w(:);...
     zeros(par.PostStimSilence * par.SamplingRate, 1)];

% generate the event structure:
event(1) = struct('Note',['PreStimSilence , ' par.Names{index}],...
               'StartTime',0,...
               'StopTime',par.PreStimSilence,...
               'Trial',[]);
event(2) = struct('Note',['Stim , ' par.Names{index}],...
                  'StartTime',par.PreStimSilence,...
                  'StopTime', par.PreStimSilence + par.Duration * stim_num,...
                  'Trial',[]);
event(3) = struct('Note',['PostStimSilence , ' par.Names{index}],...
                  'StartTime',par.PreStimSilence + par.Duration * stim_num, ...
                  'StopTime',par.PreStimSilence+ par.Duration * stim_num +...
                  par.PostStimSilence,...
                  'Trial',[]);

% return random seed to previous state
rand('seed',saveseed);
randn('seed',savenseed);
end
