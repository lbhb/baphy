
function o = ObjUpdate (o)
% This function is used to recalculate the properties of the object that
% depends on other properties. In these cases, user firs changes the
% properties using set, and then calls ObjUpdate. The default is empty, you
% need to write your own.

% SVD 2015-03-05

par=get(o);

TotalCount=par.SequenceCount;
SeqPerRate=ceil(TotalCount./length(par.F1Rates));
Names=cell(1,TotalCount);
PipsPerTrial=floor(par.Duration./(par.PipDuration+par.PipInterval));
Sequences=zeros(PipsPerTrial,TotalCount);
s=rand('state');
rand('state',PipsPerTrial*TotalCount);

randist=par.RandDistribution;
if strcmp(strtrim(randist),'Exponential')
    % creates list of jittered intervals 
    % under exponential distrbution of mu=Pipinterval
    Intervals=random('exponential',par.PipInterval,[PipsPerTrial+1,1]);
elseif strcmp(strtrim(randist),'Normal')    
    % creates list of jittered intervals 
    % under Normal distrbution of Mean=Pipinterval, stdev = par.StDev
    Intervals=random('normal',par.PipInterval,par.StDev,[PipsPerTrial+1,1]);
else disp('Error: choose either Exponential or Normal distribution');
end

PipIdRandomizer = par.PipIdSelection;

%for backwards reproducibility, default: 
%uses random pip identity generator if jitter off
if strcmp(strtrim(PipIdRandomizer), 'Random') 
    for ii=1:TotalCount,
        SeqNum=ceil(ii/SeqPerRate);
        Names{ii}=sprintf('Seq%03d+F1%.2f',ii,par.F1Rates(SeqNum));

        F1Rate=par.F1Rates(SeqNum);
        Sequences(:,ii)=double(rand(PipsPerTrial,1)>F1Rate)+1;
    end
    
% better pip identity generator, semirandom.
% defines set number of starndard and deviants, then shuffles position     
elseif strcmp(strtrim(PipIdRandomizer), 'Pseudorandom') 
    % Defines ordered list of ones and twos (freq1 and freq2), with a ratio
    % acording to selected F1Rates, 
    DeviantsPerTrial = ceil (PipsPerTrial * par.F1Rates(2)); 
    PipOrderedList = zeros (PipsPerTrial, 1);
    PipOrderedList((1:DeviantsPerTrial),1) = ones (DeviantsPerTrial,1) ;
    PipOrderedList = PipOrderedList + 1; 
    
    %Uses random sampling without replacement to shuffle the ordered list
    %for each trial. 
    for ii = 1:(TotalCount),
        SeqNum=ceil(ii/SeqPerRate);
        Names{ii}=sprintf('Seq%03d+F1%.2f',ii,par.F1Rates(SeqNum));
        
        Sequences(:,ii) = randsample(PipOrderedList,PipsPerTrial);
        
    end
    seqHalf = Sequences(:,(1:(ceil(TotalCount/2)))); %slices first half of sequences 
    invertHalf = (1./seqHalf)*2;   %inverts slice, so 1 becomes 2 and viceversa
    if mod(TotalCount,2) %true if number of trials is odd
        Sequences(:, (ceil(TotalCount/2))+1:end)= invertHalf(:,1:(end-1)); %replaces second half with inversed first half
    else 
        Sequences(:, (TotalCount/2)+1:end)= invertHalf; %replaces second half with inversed first half
    end     

end

rand('state',s);

o = set(o,'SamplingRate',100000);
o = set(o,'MaxIndex',TotalCount);
o = set(o,'Names',Names);
o = set(o,'Sequences',Sequences);
o = set(o,'Intervals',Intervals);
