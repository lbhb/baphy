function o = ObjUpdate (o);
%

soundset=get(o,'Subsets');
RepIdx=get(o,'RepIdx');

object_spec = what('NaturalSounds');
if soundset>=10
    soundset=round(soundset/10); end
if soundset==1  %Natrual sounds full library
    soundpath = [object_spec(1).path filesep 'sounds'];
elseif soundset==2  %Natural sounds, selected 8 sounds: 3 ferret vocs, 5 other)
    soundpath = [object_spec(1).path filesep 'sounds_set2'];
elseif soundset==3  %SNH set 1
    soundpath = [object_spec(1).path filesep 'sounds_set3'];
elseif soundset==4  %SNH set 2
    soundpath = [object_spec(1).path filesep 'sounds_set4'];
elseif soundset==5  %SNH set 3
    soundpath = [object_spec(1).path filesep 'sounds_set5'];
elseif soundset==6  % Alex Kell noise test
    soundpath = [object_spec(1).path filesep 'sounds_set6'];
elseif soundset==7  % validation set from sets 3/4
    soundpath = [object_spec(1).path filesep 'sounds_set7'];
elseif soundset==8  % eclectic synthetic
    soundpath = [object_spec(1).path filesep 'sounds_set8'];
else
    disp('Wrong subset!!'); 
    return; 
end

temp = dir([soundpath filesep '*.wav']);
UNames = cell(1,length(temp));
[UNames{:}] = deal(temp.name);

MaxIndex=length(UNames);
idxset=(1:MaxIndex)';
if length(RepIdx)>=2 && RepIdx(1)>0 && RepIdx(2)>1,
    RepSet=1:RepIdx(1);
    RepCount=RepIdx(2);
    idxset=cat(1,repmat(idxset(RepSet,:),[RepCount 1]),...
               idxset((RepIdx(1)+1):end,:));
    MaxIndex=size(idxset,1);
end

Names=cell(1,MaxIndex);
for ii=1:MaxIndex,
    Names{ii}=UNames{idxset(ii)};
end

o = set(o,'Names',Names);
o = set(o,'idxset',idxset);
o = set(o,'SoundPath',soundpath);
o = set(o,'MaxIndex', length(Names));
