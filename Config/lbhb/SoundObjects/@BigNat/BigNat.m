function o = BigNat(varargin)

% very large natural sound library
% SVD 2022-01-20


switch nargin
case 0
    % if no input arguments, create a default object
    s = SoundObject('BigNat', 48000, 0, 1, 1, {}, 1, ...
                    {'Duration','edit',3, ...
                     'SoundPath','edit','H:\sounds\BigNat\v1\', ...
                     'FitRange','edit',[1 100], ...
                     'FitRepeats','edit',1, ...
                     'TestRange','edit',[1 4], ...
                     'TestRepeats','edit',8, ...
                     'FitBinaural','popupmenu','None|OneOffset|TwoOffset|Diotic', ...
                     'TestBinaural','popupmenu','None|OneOffset|TwoOffset|Diotic', ...
                     'BinauralIndexOffset','edit',51,...
                     'FixedAmpScale','edit',5,...
                     'AttenSet','edit',0,...
                     'RefRepCount','edit',1
                     });
    %o.SoundPath = '/auto/data/sounds/BigNat/v1/';
    o.SoundPath = 'H:\sounds\BigNat\v2\';
    o.FitRange= [1 100];
    o.FitRepeats = 1;
    o.FitIndices = [];
    o.TestRange= [1 10];
    o.TestRepeats = 8;
    o.TestIndices = [];
    o.FitBinaural = 'None';
    o.TestBinaural = 'None';
    o.BinauralIndexOffset = 100;
    o.Duration = 20;
    o.idxset = 1:110;
    o.FixedAmpScale = 5;
    o.AttenSet=0;
    o.RefRepCount = 1;
    o.NumOfEvPerStim = 3;
    o.OverrideAutoScale = 1;

    o = class(o,'BigNat',s);
    o = ObjUpdate(o);
case 1
    % if single argument of class BigNat, return it
    if isa(varargin{1},'BigNat')
        o = varargin{1};
    else
        error('Wrong argument type');
    end
otherwise
    error('Wrong number of input arguments');
end