function plot_stimulus(RefObject,TarObject,ThisTarParams,TrialSound,TrialSamplingRate,RefTrialIndex,TrialIndex,TarIdx,events,TrialEnv)

n = strsep(events(2).Note, ',');
n = strtrim(n{2});

subplot(2,1,1);
w0=resample(TrialSound(:,1),10000,TrialSamplingRate);
t=(1:length(w0))/10000;
plot(t,w0);
ylim([-2, 2]);

title(n, 'Interpreter', 'none');
subplot(2,1,2);
if size(TrialSound,2)>1
    w0=resample(TrialSound(:,2),10000,TrialSamplingRate);
    plot(t,w0);
else
    plot(t,zeros(length(t),1));
end
ylim([-2, 2]);

end