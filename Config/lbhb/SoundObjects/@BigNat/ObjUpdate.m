function o = ObjUpdate (o)

%

par = get(o);

fitfiles = dir([par.SoundPath filesep 'cat*.wav']);
if isempty(fitfiles)
   fitfiles = dir([par.SoundPath filesep 'seq*.wav']);
   if par.FixedAmpScale==5
      o = set(o, 'FixedAmpScale', 50);
   end
%else
%   o = set(o, 'FixedAmpScale', 5);
end

fitnames = cell(1,length(fitfiles));
[fitnames{:}] = deal(fitfiles.name);

testfiles = dir([par.SoundPath filesep '00*.wav']);
testnames = cell(1,length(testfiles));
[testnames{:}] = deal(testfiles.name);

if isempty(fitnames)
    warning(['Fitnames empty, not updating']);
    return
end
if isempty(testnames)
    warning(['Testnames empty, not updating']);
    return
end
if par.FitRange(2)>length(fitnames)
    warning(['Truncating FitRange max']);
    par.FitRange(2)=length(fitnames);
end
if par.TestRange(2)>length(testnames)
    warning(['Truncating TestRange max']);
    par.TestRange(2)=length(testnames);
end

if length(par.FitRange)==2
   FitRange=(par.FitRange(1):par.FitRange(2))';
else
   FitRange = par.FitRange(:);
end

if length(par.TestRange)==2
   TestRange=(par.TestRange(1):par.TestRange(2))';
else
   TestRange = par.TestRange(:);
end

if diff(par.TestRange)>2
    offset=2;
else
    offset=1;
end
[FitRange, FitNames] = lGenerateNames(strtrim(par.FitBinaural), FitRange, fitnames, par.BinauralIndexOffset, par.AttenSet);
[TestRange, TestNames] = lGenerateNames(strtrim(par.TestBinaural), TestRange, testnames, offset, par.AttenSet);

% add repeats after generating names since lGenerateNames deletes duplicates
FitRange = repmat(FitRange,[par.FitRepeats 1]);
TestRange = repmat(TestRange,[par.TestRepeats 1]);
FitNames = repmat(FitNames, [1 par.FitRepeats]);
TestNames = repmat(TestNames, [1 par.TestRepeats]);

Names = [FitNames(:)', TestNames(:)'];
MaxIndex=length(Names);
idxset=(1:MaxIndex)';


o = set(o,'Names',Names);
o = set(o,'idxset',idxset);
o = set(o,'MaxIndex', length(Names));

end

function [outrange, outfiles] = lGenerateNames(Binaural, range, files, offset, AttenSet)

if strcmpi(Binaural,'OneOffsetFlipped')
   NewRange=zeros(size(range,1)*6,2);
   for ii = 1:length(range)
      ii2 = mod(ii+offset-1,length(range))+1;
      NewRange(ii*6-5,:)=[range(ii) range(ii2)];
      NewRange(ii*6-4,:)=[range(ii2) range(ii)];
      NewRange(ii*6-3,:)=[range(ii) 0];
      NewRange(ii*6-2,:)=[0 range(ii)];
      NewRange(ii*6-1,:)=[range(ii2) 0];
      NewRange(ii*6-0,:)=[0 range(ii2)];
   end
   outrange=NewRange;
elseif strcmpi(Binaural,'OneOffset')
   NewRange=zeros(size(range,1)*3,2);
   for ii = 1:length(range)
      ii2 = mod(ii+offset-1,length(range))+1;
      NewRange(ii*3-2,:)=[range(ii) range(ii2)];
      NewRange(ii*3-1,:)=[range(ii) 0];
      NewRange(ii*3-0,:)=[0 range(ii)];
      % ii2 will be played out of the other speakers on subsequent trials
   end
   outrange=NewRange;
elseif strcmpi(Binaural,'Diotic')
   NewRange=zeros(size(range,1)*3,2);
   for ii = 1:length(range)
      ii2 = ii;
      NewRange(ii*3-2,:)=[range(ii) range(ii2)];
      NewRange(ii*3-1,:)=[range(ii) 0];
      NewRange(ii*3-0,:)=[0 range(ii)];
      % ii2 will be played out of the other speakers on subsequent trials
   end
   outrange=NewRange;
elseif strcmpi(Binaural,'TwoOffset')
   NewRange=zeros(size(range,1)*4,2);
   for ii = 1:length(range)
      ii2 = mod(ii+offset-1,length(range))+1;
      ii3 = mod(ii+offset,length(range))+1;
      NewRange(ii*4-3,:)=[range(ii) 0];
      NewRange(ii*4-2,:)=[0 range(ii)];
      NewRange(ii*4-1,:)=[range(ii) range(ii2)];
      NewRange(ii*4-0,:)=[range(ii) range(ii3)];
   end
   outrange=NewRange;
else
   outrange=range;
end

if isempty(AttenSet)
    AttenSet=0;
end
if size(outrange,1)>0
    k=size(outrange,2);
    n = numel(AttenSet);                          %//  number of values
    if k==1
        AttenRange=AttenSet(:);
    elseif k==2
        AttenRange = [AttenSet(:) AttenSet(:)];
        %AttenRange=zeros(n*n,k);
        %c=0;
        %for i=1:n
        %    for j=1:n
        %        c=c+1;
        %        AttenRange(c,:)=[AttenSet(i) AttenSet(j)];
        %    end
        %end
    else
        error('k>2 not supported for variable attenuation');
    end
    
else
    AttenRange=[];
end


outfiles=cell(1,length(outrange)*size(AttenRange,1));
f=0;
for atten=1:size(AttenRange,1)
    att = cell(size(AttenRange,2));
    for i=1:size(AttenRange,2)
        if (size(AttenRange,1)==1) && AttenRange(1,1)==0
            att{i}='';
        elseif size(AttenRange,2)==1
            att{i}=sprintf(':0:%d',AttenRange(atten,i));
        else
            att{i}=sprintf(':%d',AttenRange(atten,i));
        end
    end
    
    for ii = 1:size(outrange,1)
        f=f+1;
       if size(outrange,2)==2 && outrange(ii,1)==0
          outfiles{f}=sprintf('NULL:1%s+%s:2%s', ':0', files{outrange(ii,2)}, att{2});
       elseif size(outrange,2)==2 && outrange(ii,2)==0
          outfiles{f}=sprintf('%s:1%s+NULL:2%s', files{outrange(ii,1)}, att{1}, ':0');
       elseif size(outrange,2)==2
          outfiles{f}=sprintf('%s:1%s+%s:2%s', files{outrange(ii,1)}, att{1}, files{outrange(ii,2)}, att{2});
       else
          outfiles{f}=[files{outrange(ii,1)}, att{1}];
       end
    end
end

outfiles=unique(outfiles);

end



