function [w,event]=waveform(o, index, IsRef)
% function w=waveform(o, index);
% adapted from generic SoundObject to deal with binaural stimulation

% SVD, FEb 2022

maxIndex = get(o,'MaxIndex');
if index > maxIndex
    error(sprintf('Maximum possible index is %d',maxIndex));
end
%
event=[];
SamplingRate = ifstr2num(get(o,'SamplingRate'));
PreStimSilence = ifstr2num(get(o,'PreStimSilence'));
PostStimSilence = ifstr2num(get(o,'PostStimSilence'));
% If more than two values are specified, choose a random number between the
% two:
if length(PreStimSilence)>1
    PreStimSilence = PreStimSilence(1) + diff(PreStimSilence) * rand(1);
end
if length(PostStimSilence)>1
    PostStimSilence = PostStimSilence(1) + diff(PostStimSilence) * rand(1);
end
Duration = ifstr2num(get(o,'Duration'));
if Duration>0
    totalSamples = floor(Duration * SamplingRate);
else
    totalSamples = 0;
end

object_spec = what(class(o));

allfields=get(o);
soundpath=get(o,'SoundPath');
files = get(o,'Names');

these_files = split(files{index}, '+');
c=0;
for ii=1:length(these_files)
   this_file=split(these_files{ii}, ':');
   if length(this_file)>1
      channel=str2num(this_file{2});
   else
      channel=1;
   end
   if length(this_file)>2
       attendb = str2num(this_file{3});
       scaleby = 10.^(-attendb./20);

   else
       attendb = 0;
       scaleby = 1;
   end
   
   this_file=this_file{1};
   
   if ~strcmpi(this_file,'NULL')
      [w_,fs] = wavread([soundpath filesep this_file]);

      if fs~=SamplingRate
         w_ = resample(w_, SamplingRate, fs);
      end
      
      if totalSamples
          w_ = w_(1:min(size(w_,1),totalSamples),:);
          if size(w_,1)<totalSamples
             % pad with zeros
             w_=cat(1,w_,zeros(totalSamples-length(w_),size(w_,2)));
          end
      end
      
      if isfield(get(o),'FixedAmpScale') && (get(o,'FixedAmpScale')>0)
         w_ = w_ * get(o,'FixedAmpScale');
      elseif max(abs(w_))>0
         w_ = 5*w_ / max(abs(w_));
      end
      
      % level compress to remove loud clicks:
      w_ = remove_clicks(w_, 15);
      
      if scaleby~=1
          w_ = w_ * scaleby;
      end
      fprintf('index=%d: max(abs(w_))=%.3f  std(w_)=%.3f attemndb=%.0f\n',index,max(abs(w_)),std(w_),attendb);
      
      if (c==0) && (channel==1)
         w=w_(:);
      elseif (c==0)
         w=[zeros(size(w_(:))) w_(:)];
      elseif size(w,2)<channel
         w(:,channel) = w_;
      else
         w(:,channel) = w(:,channel)+w_;
      end
      c=c+1;
   end
end
chancount=size(w,2);

% 10ms ramp at onset:
ramp = hanning(.01 * SamplingRate*2);
ramp = ramp(1:floor(length(ramp)/2));
for ii = 1:chancount
   w(1:length(ramp),ii) = w(1:length(ramp),ii) .* ramp;
   w(end-length(ramp)+1:end,ii) = w(end-length(ramp)+1:end,ii) .* flipud(ramp);
end

% If the object has Duration parameter, cut the sound to match it, if
% possible:
if isfield(get(o),'Duration')
    Duration = ifstr2num(get(o,'Duration'));
    if Duration>0
        totalSamples = floor(Duration * SamplingRate);
        w = w(1:min(size(w,1),totalSamples),:);
        
        if size(w,1)<totalSamples
           % measure Duration of actual waveform
           Duration = size(w,1) / SamplingRate;
           % pad with zeros
           %w=cat(1,w,zeros(totalSamples-length(w),size(w,2)));
        end
    else
        Duration = size(w,1) / SamplingRate;
    end
else
    Duration = size(w,1) / SamplingRate;
end

% Now, put it in the silence:
w = [zeros(ceil(PreStimSilence*SamplingRate),chancount);
     w ;
     zeros(ceil(PostStimSilence*SamplingRate),chancount)];
%
if isfield(get(o),'SNR')
    NoiseType = strrep(NoiseType,' ','');
    if get(o,'SNR') <100 && ~strcmpi(NoiseType,'none') && ~strcmpi(NoiseType,'SpectSmooth')
        if strcmpi(NoiseType,'white')
            % even for white, always load if from file. (Jan-9-2008, Nima)
            randn('seed',index);
            % for speech, we should add white noise to the 16K waveform, to
            % make it the same as others:
            if strcmpi(get(o,'descriptor'),'speech') && get(o,'SamplingRate')~=16000
                w = resample(w,16000,get(o,'SamplingRate'));
            end
            w = awgn(w, get(o,'SNR'), 'measured');
            if strcmpi(get(o,'descriptor'),'speech') && get(o,'SamplingRate')~=16000
                w = resample(w,get(o,'SamplingRate'),16000);
            end
        else
            [n,fs] = wavread([soundpath filesep lower(NoiseType) '.wav']);
            % to add randomness to different samples, shift the noise by
            % the index. So for the same index, the noise is always the
            % same. but for different indices, its different. This assumes
            % that the noise length is x times of the duration and index.
            n(1:floor(index*fs/5.5))=[]; % for each sample, shift the noise by 250ms
            if fs ~= SamplingRate
                n = resample(n,SamplingRate,fs);
            end
            n(length(w)+1:end)=[]; % assume the noise is always longer than the signal!
            sigPower = sum(abs(w(:)).^2)/length(w(:));
            sigPower = 10*log10(sigPower);
            noisePower = sum(abs(n(:)).^2)/length(n(:));
            noisePower = 10*log10(noisePower);
            curSNR = sigPower-noisePower;
            reqSNR = get(o,'SNR');
            noiseGain = sqrt(10^((reqSNR-curSNR)/10));
            w = w + n/noiseGain;
        end
    end
end
if isfield(get(o),'ReverbTime')
    len = length(w);
    randn('seed',index);
    w = addreverb (w, SamplingRate, get(o,'ReverbTime'));
    w = w(1:len);
end
% and generate the event structure:
event = struct('Note',['PreStimSilence , ' files{index}],...
   'StartTime',0,'StopTime',PreStimSilence,'Trial',[]);
event(2) = struct('Note',['Stim , ' files{index}], ...
   'StartTime', PreStimSilence, ...
   'StopTime', PreStimSilence+Duration, 'Trial',[]);
event(3) = struct('Note',['PostStimSilence , ' files{index}],...
   'StartTime',PreStimSilence+Duration, ...
   'StopTime',PreStimSilence+Duration+PostStimSilence,'Trial',[]);
