function plot_stimulus(RefObject,TarObject,ThisTarParams,TrialSound,TrialSamplingRate,RefTrialIndex,TrialIndex,TarIdx,events,TrialEnv)

subplot(2,1,1);
plot(TrialSound(:,1));
if size(TrialSound,2)>1
    subplot(2,1,2);
    plot(TrialSound(:,2));
end    

end