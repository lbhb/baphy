function o = FusionNoise(varargin)
% FusionNoise is the constructor for the object FusionNoise which is a child of  
%       SoundObject class
%
% Run class: FNS
%
% To get the waveform and events:
%   [w, events] = waveform(t);  
%
% methods: set, get, waveform, ObjUpdate

% SVD created 2022-03-16, based on NoiseBurst object.

switch nargin
case 0
    % if no input arguments, create a default object
    s = SoundObject ('FusionNoise', 100000, 0, 0, 0, ...
        {''}, 1, {'Duration','edit',3,...
        'Centers1','edit',1000,...
        'BW1','edit',0.5,...
        'PulseDuration1','edit',0.5,...
        'Gap1','edit',0.5,...
        'Channel1','edit',0,...
        'Centers2','edit',1000,...
        'BW2','edit',0.5,...
        'PulseDuration2','edit',3,...
        'Gap2','edit',0,...
        'Channel2','edit',1,...
        'RiseTime','edit',0.01,...
        'FlipChannels','popupmenu', 'No|Yes',...
        'Frozen', 'popupmenu', 'No|Yes',...
        'RefRepCount','edit',1});
    o.Centers1 = 1000;
    o.BW1 = 0.5;
    o.PulseDuration1 = 0.5;
    o.Gap1 = 0.5;
    o.Channel1 = 0;
    o.Centers2 = 1000;
    o.BW2 = 0.5;
    o.PulseDuration2 = 3;
    o.Gap2 = 0;
    o.Channel2 = 1;
    o.NoiseBand = 1;
    o.RiseTime = 0.01;
    o.RefRepCount=1;
    o.PreStimSilence = 0.5;
    o.PostStimSilence = 0.5;
    o.Duration = 3;
    o.FlipChannels = 'No';
    o.Frozen = 'No';
    o.ParameterSet = [];
    o = class(o,'FusionNoise',s);
    o = ObjUpdate (o);
case 1
    % if single argument of class SoundObject, return it
    if isa(varargin{1},'SoundObject')
        o = varargin{1};
    else
        error('Wrong argument type');
    end
        
otherwise
    error('Wrong number of input arguments');
end