function o = ObjUpdate (o);
% This function is used to recalculate the properties of the object that
% depends on other properties. In these cases, user firs changes the
% properties using set, and then calls ObjUpdate. The default is empty, you
% need to write your own.

% SVD 2007-03-30
global FORCESAMPLINGRATE;

par = get(o);
Centers1 = par.Centers1;
Centers2 = par.Centers2;
BW1 = par.BW1;
BW2 = par.BW2;

if length(BW1)<length(Centers1)
   BW1=repmat(BW1(1),1,length(Centers1));
end
if length(BW2)<length(Centers2)
   BW2=repmat(BW2(1),1,length(Centers2));
end

ParameterSet = [];
Names = {};
MaxIndex=0;
for i=1:length(Centers1)
   for j=1:length(Centers2)
      MaxIndex=MaxIndex+1;
      row = [Centers1(i) BW1(i) par.PulseDuration1 par.Channel1 ...
             Centers2(j) BW2(j) par.PulseDuration2 par.Channel2];
      ParameterSet(MaxIndex,:) = row;
      Names{MaxIndex} = sprintf('%.0f-%.2f-%.3f-%d_%.0f-%.2f-%.3f-%d',row);
      if strcmpi(par.FlipChannels,'Yes')
         MaxIndex=MaxIndex+1;
         row = [Centers2(j) BW2(j) par.PulseDuration2 par.Channel2 ...
                Centers1(i) BW1(i) par.PulseDuration1 par.Channel1];
         ParameterSet(MaxIndex,:) = row;
         Names{MaxIndex} = sprintf('%.0f-%.2f-%.3f-%d_%.0f-%.2f-%.3f-%d',row);
      end
   end
end

o = set(o,'ParameterSet',ParameterSet);
o = set(o,'MaxIndex',MaxIndex);
o = set(o,'Names',Names);
