function [w, event]=waveform(o,index,IsRef)
% function w=waveform(t);
% this function is the waveform generator for object NoiseBurst
%
% created SVD 2007-03-30

event = [];

SamplingRate = get(o,'SamplingRate');
Duration = get(o,'Duration'); % duration in seconds
PreStimSilence = get(o,'PreStimSilence');
PostStimSilence = get(o,'PostStimSilence');
NoiseBand= get(o,'NoiseBand');
ParameterSet = get(o,'ParameterSet');
Names = get(o,'Names');
Frozen = strcmpi(get(o,'Frozen'),'Yes');
RiseTime = get(o,'RiseTime');

% parameters for this index value
parms = ParameterSet(index,:);
Centers=[parms(1) parms(5)];
BW=[parms(2) parms(6)];
Channels=[get(o,'Channel1') get(o,'Channel2')];
PulseDuration=[get(o,'PulseDuration1') get(o,'PulseDuration2')];
Gaps=[get(o,'Gap1') get(o,'Gap2')];


channel_count=max(Channels)+1;
timesamples = (1 : round(Duration*SamplingRate))' / SamplingRate;
w=zeros(length(timesamples),channel_count);

% record state of random number generator
s=rng;

if Frozen
   % frozen noise--always the same composition of phases for same FNS
   % parameters
   rng(index.*100, 'twister');
else
   % To randomize the phase
   rng(sum(100*clock), 'twister');
end

% 10ms ramp at onset and offset:
ramp = hanning(round(RiseTime * SamplingRate*2));
ramp = ramp(1:floor(length(ramp)/2));

for ii=1:length(PulseDuration)
   if NoiseBand % bandpass white noise option
      % use standard BP noise function.
      if Centers(ii)>0
         lf = round(Centers(ii)*(1-BW(ii)/2));
         hf = round(Centers(ii)*(1+BW(ii)/2));
         fprintf('lf %.3f to hf %.3f dur %.2f fs %0f\n',lf,hf,PulseDuration(ii),SamplingRate);
         w0=BandpassNoise(lf,hf,PulseDuration(ii),SamplingRate);
      else
         w0=zeros(PulseDuration(ii)*SamplingRate,1);
      end
   else
      error('NoiseBand must be 1. Alternatives not supported');
   end
   
   % 10ms ramp at onset and offset:
   ramp = hanning(round(RiseTime * SamplingRate*2));
   ramp = ramp(1:floor(length(ramp)/2));
   w0(1:length(ramp)) = w0(1:length(ramp)) .* ramp;
   w0(end-length(ramp)+1:end) = w0(end-length(ramp)+1:end) .* flipud(ramp);
   
   
   % normalize min/max +/-5
   if max(abs(w0(:)))>0
      w0 = 5 ./ max(abs(w0(:))) .* w0;
   end
   
   w0(1:length(ramp)) = w0(1:length(ramp)) .* ramp;
   w0(end-length(ramp)+1:end) = w0(end-length(ramp)+1:end) .* flipud(ramp);

   t1 = 0;
   while t1<Duration
      w(t1*SamplingRate+(1:length(w0)),Channels(ii)+1)= ...
         w(t1*SamplingRate+(1:length(w0)),Channels(ii)+1) + w0;
      t1=t1+PulseDuration(ii)+Gaps(ii);
   end
end

% return random number generator to previous state
rng(s);

% Now, put it in the silence:
w = [zeros(PreStimSilence*SamplingRate,channel_count) ; 
   w ;
   zeros(PostStimSilence*SamplingRate,channel_count)];

% and generate the event structure:
event = struct('Note',['PreStimSilence , ' Names{index}],...
    'StartTime',0,'StopTime',PreStimSilence,'Trial',[]);
event(2) = struct('Note',['Stim , ' Names{index}],'StartTime'...
    ,PreStimSilence, 'StopTime', PreStimSilence+Duration,'Trial',[]);
event(3) = struct('Note',['PostStimSilence , ' Names{index}],...
    'StartTime',PreStimSilence+Duration, 'StopTime',PreStimSilence+Duration+PostStimSilence,'Trial',[]);
