function o = ObjUpdate (o)
%

par = get(o);

% get the full path to the files
object = what('OverlappingPairs');
ObjPath = object.path;                            
bg_files = dir(strcat(ObjPath,'/', par.BG_Folder, '/*.wav'));
fg_files = dir(strcat(ObjPath,'/', par.FG_Folder, '/*.wav'));

if any(par.Background > length(bg_files))
    warning('A background index is out of range')
    return
end

if any(par.Foreground > length(fg_files))
   warning('A foreground index is out of range')
   return
end

if strcmp('No', strtrim(par.Combos))
    maxidx = length(unique(par.Foreground)) + length(unique(par.Background));
    types = {['Background'],['Foreground']};
    
    Sounds(maxidx) = struct('sound_name', [],...
                            'type', []);
    
    counter = 1;
    for ee = 1:length(unique(par.Background))
        sound_idx = par.Background(ee);
        Sounds(counter).sound_name = bg_files(sound_idx).name;
        Sounds(counter).type = types(1);
        counter = counter + 1;
    end   

    for ff = 1:length(unique(par.Foreground))
        sound_idx = par.Foreground(ff);
        Sounds(counter).sound_idx = sound_idx;
        Sounds(counter).sound_name = fg_files(sound_idx).name;
        Sounds(counter).type = types(2);
        counter = counter + 1;
    end   
        
    o = set(o, 'Sounds', Sounds); 
    
    Names{maxidx,1} = '';
    for aa = 1:maxidx
        if strcmp('Background', strtrim(Sounds(aa).type))
            names = strsplit(Sounds(aa).sound_name, '.');
            full_name = strcat(names{1},'-','0','-',num2str(par.Duration),'_','null');
        elseif strcmp('Foreground', strtrim(Sounds(aa).type))
            names = strsplit(Sounds(aa).sound_name, '.');
            full_name = strcat('null','_',names{1},'-','0','-',num2str(par.Duration));
        end
        Names{aa} = full_name;
    end
        
elseif strcmp('Yes', strtrim(par.Combos))
    error('Did not finish this yet, do manual.')

elseif strcmp('Manual', strtrim(par.Combos))    
    
    if length(par.Background) ~= length(par.Foreground)
       warning('The same number of background and foreground indexes not given. Using min()')
       pairs = min(length(par.Background), length(par.Foreground));
    else
       pairs = length(par.Background);
    end
    
    if strcmp('No', strtrim(par.Synthetic)) && strcmp('No', strtrim(par.Binaural)) && ~strcmp('Some', strtrim(par.Synthetic))
        if strcmp('Yes', strtrim(par.SoundRepeats))
            error('SoundRepeats cannot be yes if we are doing the basic run, there is no point and I did not want to code it.')
        end
        if strcmp('Some', strtrim(par.NormalizeRMS))
            error('NormalizeRMS=Some is a very specific thing for Synthetic, cannot be Some if we are doing the basic run, there is no point and I did not want to code it.')
        end
        
        % For basic OLP, you just need the unique FGs/BGs plus the number
        % of pairs
        [C, bg_repeat_pair, ic] = unique(par.Background, 'stable');
        [D, fg_repeat_pair, ie] = unique(par.Foreground, 'stable');
        bg_repeat_pairidx = setdiff(1:pairs, bg_repeat_pair);
        fg_repeat_pairidx = setdiff(1:pairs, fg_repeat_pair);
        bg_alone = length(C); % Not *2 because there is no half / ipsilateral condition
        fg_alone = length(D); 
        combos = pairs;
        
    else
        if strcmp('No', strtrim(par.Synthetic))
            if strcmp('No', strtrim(par.SoundRepeats)) 
                [C, bg_repeat_pair, ic] = unique(par.Background, 'stable');
                [D, fg_repeat_pair, ie] = unique(par.Foreground, 'stable');
                bg_repeat_pairidx = setdiff(1:pairs, bg_repeat_pair);
                fg_repeat_pairidx = setdiff(1:pairs, fg_repeat_pair);
                bg_alone = length(C) * 2;
                fg_alone = length(D) * 2;
                combos = length(par.Background) * 4;     %bg and gf length must be equal
            elseif strcmp('Yes', strtrim(par.SoundRepeats))
                bg_repeat_pairidx = setdiff(1:pairs, 1:pairs);
                fg_repeat_pairidx = setdiff(1:pairs, 1:pairs);        
                bg_alone = length(par.Background) * 2;
                fg_alone = length(par.Foreground) * 2;
                combos = length(par.Background) * 4;     %bg and gf length must be equal
            end    
        elseif strcmp('C-T-S-ST', strtrim(par.Synthetic)) && strcmp('No', strtrim(par.SoundRepeats)) && ~strcmp('Some', strtrim(par.NormalizeRMS))
            [C, bg_repeat_pair, ic] = unique(par.Background, 'stable');
            [D, fg_repeat_pair, ie] = unique(par.Foreground, 'stable');
            bg_repeat_pairidx = setdiff(1:pairs, bg_repeat_pair);
            fg_repeat_pairidx = setdiff(1:pairs, fg_repeat_pair);
            bg_alone = length(C) * 5;
            fg_alone = length(D) * 5;
            combos = length(par.Background) * 5;     %bg and gf length must be equal
        elseif strcmp('C-T-S-ST-STM', strtrim(par.Synthetic)) && strcmp('No', strtrim(par.SoundRepeats)) && ~strcmp('Some', strtrim(par.NormalizeRMS))
            [C, bg_repeat_pair, ic] = unique(par.Background, 'stable');
            [D, fg_repeat_pair, ie] = unique(par.Foreground, 'stable');
            bg_repeat_pairidx = setdiff(1:pairs, bg_repeat_pair);
            fg_repeat_pairidx = setdiff(1:pairs, fg_repeat_pair);
            bg_alone = length(C) * 6;
            fg_alone = length(D) * 6;
            combos = length(par.Background) * 6;     %bg and gf length must be equal
        elseif strcmp('C-T-S-ST-STM', strtrim(par.Synthetic)) && strcmp('No', strtrim(par.SoundRepeats)) && strcmp('Some', strtrim(par.NormalizeRMS))
            [C, bg_repeat_pair, ic] = unique(par.Background, 'stable');
            [D, fg_repeat_pair, ie] = unique(par.Foreground, 'stable');
            bg_repeat_pairidx = setdiff(1:pairs, bg_repeat_pair);
            fg_repeat_pairidx = setdiff(1:pairs, fg_repeat_pair);
            bg_alone = length(C) * 7;
            fg_alone = length(D) * 7;
            combos = length(par.Background) * 7;     %bg and gf length must be equal
        elseif strcmp('Yes', strtrim(par.Synthetic)) && strcmp('Yes', strtrim(par.SoundRepeats))
            error('SoundRepeats cannot be yes if we are doing Synthetic, there is no point and I did not want to code it.')
        end
    end
        
    maxidx = bg_alone + fg_alone + combos;
    
    SoundPairs(pairs) = struct('bg_sound_name', [],...
                               'fg_sound_name', []);
    ComboIndex(maxidx) = struct('pair', [],...
                                'type', []);
    Names{maxidx,1} = '';  
    Sounds(maxidx) = struct('bg', [],...
                           'fg', []);
    %types = {['fBG+xFG'],['hBG+xFG'],['xBG+fFG'],['xBG+hFG'],['fBG+fFG'],['fBG+hFG'],['hBG+fFG'],['hBG+hFG']};
    %types2 = {['BGc1+xFG'],['BGc2+xFG'],['xBG+FGc1'],['xBG+FGc2'],['BGc1+FGc1'],['BGc2+FGc2'],['BGc1+FGc2'],['BGc2+FGc1']};
    %types3 =
    %{['BGn+xFG],[xBG+FGn],[BGn+FGn],['BGt+xFG],[xBG+FGt],[BGt+FGt],['BGs+xFG],[xBG+FGs],[BGs+FGs],['BGst+xFG],[xBG+FGst],[BGst+FGst]};
        %n = normal, s=spectral mod, t=temporal mod, st=spectrotemporal mod
    if strcmp('No', strtrim(par.Binaural)) && strcmp('No', strtrim(par.Synthetic)) && strcmp('Yes', strtrim(par.Dynamic))
        types = [1, 2, 3, 4, 5, 6, 7, 8];
    elseif strcmp('Yes', strtrim(par.Binaural)) && strcmp('No', strtrim(par.Synthetic))
        types = [9, 10, 11, 12, 13, 14, 15, 16];
    elseif strcmp('No', strtrim(par.Binaural)) && strcmp('C-T-S-ST', strtrim(par.Synthetic)) && ~strcmp('Some', strtrim(par.NormalizeRMS))
        types = [17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
    elseif strcmp('No', strtrim(par.Binaural)) && strcmp('C-T-S-ST-STM', strtrim(par.Synthetic)) && ~strcmp('Some', strtrim(par.NormalizeRMS))
        types = [17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34];
    elseif strcmp('C-T-S-ST-STM', strtrim(par.Synthetic)) && strcmp('No', strtrim(par.SoundRepeats)) && strcmp('Some', strtrim(par.NormalizeRMS))
        types = [17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37];
    % The basic... coming last.
    elseif strcmp('No', strtrim(par.Synthetic)) && strcmp('No', strtrim(par.Binaural)) && ~strcmp('Some', strtrim(par.Synthetic))
        types = [1, 3, 5];
    end
    
    idx = 1;
    for ee = 1:pairs
        bg = par.Background(ee);
        fg = par.Foreground(ee);
%        SoundPairs(ee).bg_sound_idx = bg;
        SoundPairs(ee).bg_sound_name = bg_files(bg).name;
%        SoundPairs(ee).fg_sound_idx = fg;
        SoundPairs(ee).fg_sound_name = fg_files(fg).name;
        bg_name = strsplit(SoundPairs(ee).bg_sound_name, '.');
        fg_name = strsplit(SoundPairs(ee).fg_sound_name, '.');
        
        % Compile strings
        if strcmp('No', strtrim(par.Synthetic))
            if strcmp('Yes', strtrim(par.NormalizeRMS))
                synth_suffix = 'N';
            else
                synth_suffix = 'A';
            end
        end
                      
        SNR_extra = sprintf('-1-%s-%ddB', synth_suffix, par.SNR);
        
        for tt = 1:length(types)
            % generate specialized name to match parameters specifying type
            % of pairing
            if types(tt) == 1
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),SNR_extra,'_','null');
            elseif types(tt) == 2
                full_name = strcat(bg_name{1},'-',num2str(par.SilenceOnset),'-',num2str(par.Duration),SNR_extra,'_','null');
            elseif types(tt) == 3
                full_name = strcat('null','_',fg_name{1},'-','0','-',num2str(par.Duration),SNR_extra);
            elseif types(tt) == 4
                full_name = strcat('null','_',fg_name{1},'-',num2str(par.SilenceOnset),'-',num2str(par.Duration),SNR_extra);
            elseif types(tt) == 5
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),SNR_extra,'_',...
                                        fg_name{1},'-','0','-',num2str(par.Duration),SNR_extra);
            elseif types(tt) == 6
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),SNR_extra,'_',...
                                        fg_name{1},'-',num2str(par.SilenceOnset),'-',num2str(par.Duration),SNR_extra);                               
            elseif types(tt) == 7
                full_name = strcat(bg_name{1},'-',num2str(par.SilenceOnset),'-',num2str(par.Duration),SNR_extra,'_',...
                                        fg_name{1},'-','0','-',num2str(par.Duration),SNR_extra);   
            elseif types(tt) == 8
                full_name = strcat(bg_name{1},'-',num2str(par.SilenceOnset),'-',num2str(par.Duration),SNR_extra,'_',...
                                        fg_name{1},'-',num2str(par.SilenceOnset),'-',num2str(par.Duration),SNR_extra);                                   
            elseif types(tt) == 9
                % start binaural
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','1','_','null');
            elseif types(tt) == 10
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','2','_','null');
            elseif types(tt) == 11
                full_name = strcat('null','_',fg_name{1},'-','0','-',num2str(par.Duration),'-','1');
            elseif types(tt) == 12
                full_name = strcat('null','_',fg_name{1},'-','0','-',num2str(par.Duration),'-','2');
            elseif types(tt) == 13
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','1','_',...
                                        fg_name{1},'-','0','-',num2str(par.Duration),'-','1');
            elseif types(tt) == 14
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','2','_',...
                                        fg_name{1},'-','0','-',num2str(par.Duration),'-','2');
            elseif types(tt) == 15
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','1','_',...
                                        fg_name{1},'-','0','-',num2str(par.Duration),'-','2');
            elseif types(tt) == 16
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','2','_',...
                                        fg_name{1},'-','0','-',num2str(par.Duration),'-','1');
            
            elseif types(tt) == 17
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','N','_','null');
            elseif types(tt) == 18
                full_name = strcat('null','_',fg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','N');
            elseif types(tt) == 19
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','N','_',...
                                        fg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','N');
            elseif types(tt) == 20
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','C','_','null');
            elseif types(tt) == 21
                full_name = strcat('null','_',fg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','C');
            elseif types(tt) == 22
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','C','_',...
                                        fg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','C');
            elseif types(tt) == 23
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','T','_','null');
            elseif types(tt) == 24
                full_name = strcat('null','_',fg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','T');
            elseif types(tt) == 25
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','T','_',...
                                        fg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','T');
            elseif types(tt) == 26
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','S','_','null');
            elseif types(tt) == 27
                full_name = strcat('null','_',fg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','S');
            elseif types(tt) == 28
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','S','_',...
                                        fg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','S');
            elseif types(tt) == 29
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','U','_','null');
            elseif types(tt) == 30
                full_name = strcat('null','_',fg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','U');
            elseif types(tt) == 31
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','U','_',...
                                        fg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','U');
            elseif types(tt) == 32
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','M','_','null');
            elseif types(tt) == 33
                full_name = strcat('null','_',fg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','M');
            elseif types(tt) == 34
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','M','_',...
                                        fg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','M');
            % A is the original, unRMS balanced sound ONLY in this specific
            % case where RMS is 'Some' (6/2). All other times use Yes or No
            % to have consistently RMS balanced or not
            elseif types(tt) == 35
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','A','_','null');
            elseif types(tt) == 36
                full_name = strcat('null','_',fg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','A');
            elseif types(tt) == 37
                full_name = strcat(bg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','A','_',...
                                        fg_name{1},'-','0','-',num2str(par.Duration),'-','1','-','A');
            end
            
            
            if strcmp('No', strtrim(par.Synthetic)) && strcmp('No', strtrim(par.Binaural)) && strcmp('No', strtrim(par.Dynamic))
                if ismember(tt, [1])
                    if ~ismember(ee,bg_repeat_pairidx)
                        ComboIndex(idx).pair = ee;
                        ComboIndex(idx).type = types(tt);
                        Names{idx} = full_name;
                        idx = idx + 1;
                    end
                elseif ismember(tt, [2])
                    if ~ismember(ee,fg_repeat_pairidx)
                        ComboIndex(idx).pair = ee;
                        ComboIndex(idx).type = types(tt);
                        Names{idx} = full_name;
                        idx = idx + 1;
                    end
                else
                    ComboIndex(idx).pair = ee;
                    ComboIndex(idx).type = types(tt);
                    Names{idx} = full_name;
                    idx = idx + 1;
                end
            
            else
                if strcmp('No', strtrim(par.Synthetic))
                if ismember(tt, [1,2,5,6,7,8])
                    Sounds(ee).bg = SoundPairs(ee).bg_sound_name;
                end
                if ismember(tt, [3,4,5,6,7,8])
                    Sounds(ee).fg = SoundPairs(ee).fg_sound_name;
                end

                if ismember(tt, [1,2])
                    if ~ismember(ee,bg_repeat_pairidx)
                        ComboIndex(idx).pair = ee;
                        ComboIndex(idx).type = types(tt);
                        Names{idx} = full_name;
                        idx = idx + 1;
                    end
                elseif ismember(tt, [3,4])
                    if ~ismember(ee,fg_repeat_pairidx)
                        ComboIndex(idx).pair = ee;
                        ComboIndex(idx).type = types(tt);
                        Names{idx} = full_name;
                        idx = idx + 1;
                    end
                else
                    ComboIndex(idx).pair = ee;
                    ComboIndex(idx).type = types(tt);
                    Names{idx} = full_name;
                    idx = idx + 1;
                end
            
                elseif strcmp('C-T-S-ST', strtrim(par.Synthetic)) && ~strcmp('Some', strtrim(par.NormalizeRMS))
                    if ismember(types(tt), [17,19,20,22,23,25,26,28,29,31])
                        Sounds(ee).bg = SoundPairs(ee).bg_sound_name;
                    end
                    if ismember(types(tt), [18,19,21,22,24,25,27,28,32,33])
                        Sounds(ee).fg = SoundPairs(ee).fg_sound_name;
                    end

                    if ismember(types(tt), [17,20,23,26,29])
                        if ~ismember(ee,bg_repeat_pairidx)
                            ComboIndex(idx).pair = ee;
                            ComboIndex(idx).type = types(tt);
                            Names{idx} = full_name;
                            idx = idx + 1;
                        end
                    elseif ismember(types(tt), [18,21,24,27,30])
                        if ~ismember(ee,fg_repeat_pairidx)
                            ComboIndex(idx).pair = ee;
                            ComboIndex(idx).type = types(tt);
                            Names{idx} = full_name;
                            idx = idx + 1;
                        end
                    else
                        ComboIndex(idx).pair = ee;
                        ComboIndex(idx).type = types(tt);
                        Names{idx} = full_name;
                        idx = idx + 1;
                    end     

                elseif strcmp('C-T-S-ST-STM', strtrim(par.Synthetic)) && ~strcmp('Some', strtrim(par.NormalizeRMS))
                    if ismember(types(tt), [17,19,20,22,23,25,26,28,29,31,32,34])
                        Sounds(ee).bg = SoundPairs(ee).bg_sound_name;
                    end
                    if ismember(types(tt), [18,19,21,22,24,25,27,28,30,31,33,34]) 
                        Sounds(ee).fg = SoundPairs(ee).fg_sound_name;
                    end

                    if ismember(types(tt), [17,20,23,26,29,32])
                        if ~ismember(ee,bg_repeat_pairidx)
                            ComboIndex(idx).pair = ee;
                            ComboIndex(idx).type = types(tt);
                            Names{idx} = full_name;
                            idx = idx + 1;
                        end
                    elseif ismember(types(tt), [18,21,24,27,30,33])
                        if ~ismember(ee,fg_repeat_pairidx)
                            ComboIndex(idx).pair = ee;
                            ComboIndex(idx).type = types(tt);
                            Names{idx} = full_name;
                            idx = idx + 1;
                        end
                    else
                        ComboIndex(idx).pair = ee;
                        ComboIndex(idx).type = types(tt);
                        Names{idx} = full_name;
                        idx = idx + 1;
                    end
                elseif strcmp('C-T-S-ST-STM', strtrim(par.Synthetic)) && strcmp('Some', strtrim(par.NormalizeRMS))
                    if ismember(types(tt), [17,19,20,22,23,25,26,28,29,31,32,34,35,37])
                        Sounds(ee).bg = SoundPairs(ee).bg_sound_name;
                    end
                    if ismember(types(tt), [18,19,21,22,24,25,27,28,30,31,33,34,36,37]) 
                        Sounds(ee).fg = SoundPairs(ee).fg_sound_name;
                    end

                    if ismember(types(tt), [17,20,23,26,29,32,35])
                        if ~ismember(ee,bg_repeat_pairidx)
                            ComboIndex(idx).pair = ee;
                            ComboIndex(idx).type = types(tt);
                            Names{idx} = full_name;
                            idx = idx + 1;
                        end
                    elseif ismember(types(tt), [18,21,24,27,30,33,36])
                        if ~ismember(ee,fg_repeat_pairidx)
                            ComboIndex(idx).pair = ee;
                            ComboIndex(idx).type = types(tt);
                            Names{idx} = full_name;
                            idx = idx + 1;
                        end
                    else
                        ComboIndex(idx).pair = ee;
                        ComboIndex(idx).type = types(tt);
                        Names{idx} = full_name;
                        idx = idx + 1;
                    end
                end
            end
        end
    end
    
    o = set(o, 'SoundPairs', SoundPairs);
    o = set(o, 'ComboIndex', ComboIndex);
    o = set(o, 'Sounds', Sounds);
    
else
    error('Invalid value for Combos')
end

o = set(o, 'MaxIndex', maxidx);
o = set(o, 'bg_SoundPath', strcat(ObjPath,'/', par.BG_Folder));
o = set(o, 'fg_SoundPath', strcat(ObjPath,'/', par.FG_Folder));
o = set(o, 'Names', Names);
    
end
