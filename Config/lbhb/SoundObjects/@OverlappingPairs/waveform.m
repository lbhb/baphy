function [w, event]=waveform (o,index,IsRef, TarObject, ThisTarParams)
%
% generate waveform for ContectProbe object
%

par = get(o);

if nargin<4, TarObject=[]; end
if nargin<3, IsRef = 1; end

%Greg add
if strcmp('No', strtrim(par.Combos))
    Waves(1) = struct('wave', [],...
                       'name', []);
    
    if strcmp('Background', strtrim(par.Sounds(index).type))
        loadpath = strcat(par.bg_SoundPath, '/', par.Sounds(index).sound_name); 
%         names = strsplit(par.Sounds(index).sound_name, '.');
%         full_name = strcat(names(1),'-','0','-',string(par.Duration),'_','null');
    elseif strcmp('Foreground', strtrim(par.Sounds(index).type))
        loadpath = strcat(par.fg_SoundPath, '/', par.Sounds(index).sound_name);
%         names = strsplit(par.Sounds(index).sound_name, '.');
%         full_name = strcat('null','_',names(1),'-','0','-',string(par.Duration));
    end
    full_name = par.Names(index);
    [wav, fs] = audioread(loadpath);
    [p,q] = rat(par.SamplingRate/fs,0.0001);
    resamp_wav = resample(wav,p,q);
    
    resamp_wav = 5 * resamp_wav / max(abs(resamp_wav(:)));

    Waves(1).wave = resamp_wav;
    Waves(1).name = full_name;

elseif strcmp('Yes', strtrim(par.Combos))
    error('This still is not working yet')

elseif strcmp('Manual', strtrim(par.Combos))
    % Key for the combos:
    % 1: Full Background
    % 2: Half Background
    % 3: Full Foreground
    % 4: Half Foreground
    % 5: Full Background and Full Foreground
    % 6: Full Background and Half Foreground
    % 7: Half Background and Full Foreground
    % 8: Half Background and Half Foreground
    Waves(1) = struct('wave', [],...
                      'name', []);
    
    pair = par.ComboIndex(index).pair;
    
    background = par.SoundPairs(pair).bg_sound_name;
    bg_name = strsplit(par.SoundPairs(pair).bg_sound_name, '.');
    foreground = par.SoundPairs(pair).fg_sound_name;
    fg_name = strsplit(par.SoundPairs(pair).fg_sound_name, '.');
    if ~strcmp('No', strtrim(par.Synthetic))    
        if ismember(par.ComboIndex(index).type, [17,18,19])
            mod = 'N';
            bg_loadpath = strcat(par.bg_SoundPath, '/', background);
            fg_loadpath = strcat(par.fg_SoundPath, '/', foreground);
        elseif ismember(par.ComboIndex(index).type, [20,21,22])
            mod = 'C';
            bg_loadpath = strcat(par.bg_SoundPath, '/', 'Cochlear', '/', bg_name{1}, '_', mod, '.wav');
            fg_loadpath = strcat(par.fg_SoundPath, '/', 'Cochlear', '/', fg_name{1}, '_', mod, '.wav');
        elseif ismember(par.ComboIndex(index).type, [23,24,25])
            mod = 'T';
            bg_loadpath = strcat(par.bg_SoundPath, '/', 'Temporal', '/', bg_name{1}, '_', mod, '.wav');
            fg_loadpath = strcat(par.fg_SoundPath, '/', 'Temporal', '/', fg_name{1}, '_', mod, '.wav');
        elseif ismember(par.ComboIndex(index).type, [26,27,28])
            mod = 'S';
            bg_loadpath = strcat(par.bg_SoundPath, '/', 'Spectral', '/', bg_name{1}, '_', mod, '.wav');
            fg_loadpath = strcat(par.fg_SoundPath, '/', 'Spectral', '/', fg_name{1}, '_', mod, '.wav');
        elseif ismember(par.ComboIndex(index).type, [29,30,31])
            mod = 'U';
            bg_loadpath = strcat(par.bg_SoundPath, '/', 'Spectrotemporal', '/', bg_name{1}, '_', mod, '.wav');
            fg_loadpath = strcat(par.fg_SoundPath, '/', 'Spectrotemporal', '/', fg_name{1}, '_', mod, '.wav');
        elseif ismember(par.ComboIndex(index).type, [32,33,34])
            mod = 'M';
            bg_loadpath = strcat(par.bg_SoundPath, '/', 'SpectrotemporalMod', '/', bg_name{1}, '_', mod, '.wav');
            fg_loadpath = strcat(par.fg_SoundPath, '/', 'SpectrotemporalMod', '/', fg_name{1}, '_', mod, '.wav');
        elseif ismember(par.ComboIndex(index).type, [35,36,37])
            mod = 'A';
            bg_loadpath = strcat(par.bg_SoundPath, '/', background);
            fg_loadpath = strcat(par.fg_SoundPath, '/', foreground);
        end
    else
        bg_loadpath = strcat(par.bg_SoundPath, '/', background);
        fg_loadpath = strcat(par.fg_SoundPath, '/', foreground);
    end
    
    [bg_wav, bg_fs] = audioread(bg_loadpath);
    [bg_p,bg_q] = rat(par.SamplingRate/bg_fs,0.0001);
    bg_resamp_wav = resample(bg_wav,bg_p,bg_q);

    [fg_wav, fg_fs] = audioread(fg_loadpath);
    [fg_p,fg_q] = rat(par.SamplingRate/fg_fs,0.0001);
    fg_resamp_wav = resample(fg_wav,fg_p,fg_q);
    
    intended_samples=round(par.Duration*par.SamplingRate);
    if length(fg_resamp_wav)>intended_samples
       fg_resamp_wav=fg_resamp_wav(1:intended_samples);
       bg_resamp_wav=bg_resamp_wav(1:intended_samples);
    end
    
    %%%
    if strcmp('Yes', strtrim(par.NormalizeRMS))
        % normalize to std==1, compress outlier clicks
        bg_resamp_wav=remove_clicks(bg_resamp_wav/std(bg_resamp_wav),15);
        fg_resamp_wav=remove_clicks(fg_resamp_wav/std(fg_resamp_wav),15);
    elseif strcmp('No', strtrim(par.NormalizeRMS))
        % normalize max==5
        bg_resamp_wav = 5 * bg_resamp_wav / max(abs(bg_resamp_wav(:)));
        fg_resamp_wav = 5 * fg_resamp_wav / max(abs(fg_resamp_wav(:)));
    elseif strcmp('Some', strtrim(par.NormalizeRMS))
        %In this specific kind of hard coded case I'm testing the
        %difference of rms norm vs not, so this just specifies A as the
        %normal, unRMS condition
        if ismember(par.ComboIndex(index).type, [35,36,37])
            bg_resamp_wav = 5 * bg_resamp_wav / max(abs(bg_resamp_wav(:)));
            fg_resamp_wav = 5 * fg_resamp_wav / max(abs(fg_resamp_wav(:)));  
        else
            bg_resamp_wav=remove_clicks(bg_resamp_wav/std(bg_resamp_wav),15);
            fg_resamp_wav=remove_clicks(fg_resamp_wav/std(fg_resamp_wav),15);  
        end
    end
    
    if strcmp('Yes', strtrim(par.Ramp))
        on_ramp = linspace(0, 1, 0.005 * par.SamplingRate)';
        off_ramp = linspace(1, 0, 0.005 * par.SamplingRate)';

        ramp = ones((par.Duration * par.SamplingRate), 1);
        ramp(1:length(on_ramp)) = ramp(1:length(on_ramp)) .* on_ramp;
        ramp(end-length(off_ramp)+1:end) = ramp(end-length(off_ramp)+1:end) .* off_ramp;  
        
        if ismember(par.ComboIndex(index).type, [2, 7])
            bg_ramp = ones((par.Duration * par.SamplingRate), 1);
            bg_ramp(1:(par.SilenceOnset * par.SamplingRate)) = 0;
            bg_resamp_wav = bg_resamp_wav .* bg_ramp;
            
            fg_resamp_wav = fg_resamp_wav .* ramp;
        
        elseif ismember(par.ComboIndex(index).type, [4, 6])
            bg_resamp_wav = bg_resamp_wav .* ramp;
        
            fg_ramp = ones((par.Duration * par.SamplingRate), 1);
            fg_ramp(1:(par.SilenceOnset * par.SamplingRate)) = 0;
            fg_resamp_wav = fg_resamp_wav .* fg_ramp;
            
        elseif par.ComboIndex(index).type == 8
            bg_ramp = ones((par.Duration * par.SamplingRate), 1);
            bg_ramp(1:(par.SilenceOnset * par.SamplingRate)) = 0;
            bg_resamp_wav = bg_resamp_wav .* bg_ramp;

            fg_ramp = ones((par.Duration * par.SamplingRate), 1);
            fg_ramp(1:(par.SilenceOnset * par.SamplingRate)) = 0;
            fg_resamp_wav = fg_resamp_wav .* fg_ramp;
        
        else
            bg_resamp_wav = bg_resamp_wav .* ramp;
            fg_resamp_wav = fg_resamp_wav .* ramp;   
        end
    end        
         
    null_wav = zeros(size(bg_resamp_wav));
    
    if strcmp('No', strtrim(par.Synthetic))
        if strcmp('Yes', strtrim(par.NormalizeRMS))
            synth_suffix = 'N';
        else
            synth_suffix = 'A';
        end
    end
    
    if par.SNR ~= 0
        scaleby = 10.^(-par.SNR./20);
        bg_resamp_wav = bg_resamp_wav .* scaleby;
        SNR_extra = sprintf('-1-%s-%ddB', synth_suffix, par.SNR);
        if par.ComboIndex(index).type>8
            error('SNR <> 0 not supported for this combination type');
        end
    else
        SNR_extra = sprintf('-1-%s-%ddB', synth_suffix, par.SNR);
    end

    if par.ComboIndex(index).type == 1
        Waves(1).wave = bg_resamp_wav;
        Waves(1).name = strcat(bg_name(1),'-','0','-',string(par.Duration),SNR_extra,'_','null');
    elseif par.ComboIndex(index).type == 2
        bg_resamp_wav([1:(length(bg_resamp_wav) * par.SilenceOnset)]) = 0;
        Waves(1).wave = bg_resamp_wav;
        Waves(1).name = strcat(bg_name(1),'-',string(par.SilenceOnset),'-',string(par.Duration),SNR_extra,'_','null');
    elseif par.ComboIndex(index).type == 3
        Waves(1).wave = fg_resamp_wav;
        Waves(1).name = strcat('null','_',fg_name(1),'-','0','-',string(par.Duration),SNR_extra);
    elseif par.ComboIndex(index).type == 4
        fg_resamp_wav([1:(length(fg_resamp_wav) * par.SilenceOnset)]) = 0;
        Waves(1).wave = fg_resamp_wav;
        Waves(1).name = strcat('null','_',fg_name(1),'-',string(par.SilenceOnset),'-',string(par.Duration),SNR_extra);
    elseif par.ComboIndex(index).type == 5
        Waves(1).wave = bg_resamp_wav + fg_resamp_wav;
        Waves(1).name = strcat(bg_name(1),'-','0','-',string(par.Duration),SNR_extra,'_',...
                                fg_name(1),'-','0','-',string(par.Duration),SNR_extra);
    elseif par.ComboIndex(index).type == 6
        fg_resamp_wav([1:(length(fg_resamp_wav) * par.SilenceOnset)]) = 0;
        Waves(1).wave = bg_resamp_wav + fg_resamp_wav;
        Waves(1).name = strcat(bg_name(1),'-','0','-',string(par.Duration),SNR_extra,'_',...
                                fg_name(1),'-',string(par.SilenceOnset),'-',string(par.Duration),SNR_extra);                               
    elseif par.ComboIndex(index).type == 7
        bg_resamp_wav([1:(length(bg_resamp_wav) * par.SilenceOnset)]) = 0;
        Waves(1).wave = bg_resamp_wav + fg_resamp_wav;
        Waves(1).name = strcat(bg_name(1),'-',string(par.SilenceOnset),'-',string(par.Duration),SNR_extra,'_',...
                                fg_name(1),'-','0','-',string(par.Duration),SNR_extra);   
    elseif par.ComboIndex(index).type == 8
        bg_resamp_wav([1:(length(bg_resamp_wav) * par.SilenceOnset)]) = 0;
        fg_resamp_wav([1:(length(fg_resamp_wav) * par.SilenceOnset)]) = 0;
        Waves(1).wave = bg_resamp_wav + fg_resamp_wav;
        Waves(1).name = strcat(bg_name(1),'-',string(par.SilenceOnset),'-',string(par.Duration),SNR_extra,'_',...
                                fg_name(1),'-',string(par.SilenceOnset),'-',string(par.Duration),SNR_extra);     
    elseif par.ComboIndex(index).type == 9
        Waves(1).wave = cat(2, bg_resamp_wav, null_wav);
        Waves(1).name = strcat(bg_name{1},'-','0','-',string(par.Duration),'-','1','_','null');
    elseif par.ComboIndex(index).type == 10
        Waves(1).wave = cat(2, null_wav, bg_resamp_wav);
        Waves(1).name = strcat(bg_name{1},'-','0','-',string(par.Duration),'-','2','_','null');
    elseif par.ComboIndex(index).type == 11
        Waves(1).wave = cat(2, fg_resamp_wav, null_wav);
        Waves(1).name = strcat('null','_',fg_name{1},'-','0','-',string(par.Duration),'-','1');
    elseif par.ComboIndex(index).type == 12
        Waves(1).wave = cat(2, null_wav, fg_resamp_wav);
        Waves(1).name = strcat('null','_',fg_name{1},'-','0','-',string(par.Duration),'-','2');
    elseif par.ComboIndex(index).type == 13
        Waves(1).wave = cat(2, bg_resamp_wav+fg_resamp_wav, null_wav);
        Waves(1).name = strcat(bg_name{1},'-','0','-',string(par.Duration),'-','1','_',...
                                fg_name{1},'-','0','-',string(par.Duration),'-','1');
    elseif par.ComboIndex(index).type == 14
        Waves(1).wave = cat(2, null_wav, bg_resamp_wav+fg_resamp_wav);
        Waves(1).name = strcat(bg_name{1},'-','0','-',string(par.Duration),'-','2','_',...
                                fg_name{1},'-','0','-',string(par.Duration),'-','2');
    elseif par.ComboIndex(index).type == 15
        Waves(1).wave = cat(2, bg_resamp_wav, fg_resamp_wav);
        Waves(1).name = strcat(bg_name{1},'-','0','-',string(par.Duration),'-','1','_',...
                                fg_name{1},'-','0','-',string(par.Duration),'-','2');
    elseif par.ComboIndex(index).type == 16
        Waves(1).wave = cat(2, fg_resamp_wav , bg_resamp_wav);
        Waves(1).name = strcat(bg_name{1},'-','0','-',string(par.Duration),'-','2','_',...
                                fg_name{1},'-','0','-',string(par.Duration),'-','1');
                            
    elseif ismember(par.ComboIndex(index).type, [17, 20, 23, 26, 29, 32, 35])
        Waves(1).wave = bg_resamp_wav;
        Waves(1).name = strcat(bg_name(1),'-','0','-',num2str(par.Duration),'-','1','-',mod,'_','null');
    elseif ismember(par.ComboIndex(index).type, [18, 21, 24, 27, 30, 33, 36])
        Waves(1).wave = fg_resamp_wav;
        Waves(1).name = strcat('null','_',fg_name(1),'-','0','-',num2str(par.Duration),'-','1','-',mod);
    elseif ismember(par.ComboIndex(index).type, [19, 22, 25, 28, 31, 34, 37])
        Waves(1).wave = bg_resamp_wav + fg_resamp_wav;
        Waves(1).name = strcat(bg_name(1),'-','0','-',num2str(par.Duration),'-','1','-',mod,'_',...
                                        fg_name(1),'-','0','-',num2str(par.Duration),'-','1','-',mod);
    end
    if ismember(par.ComboIndex(index).type, [1, 2, 3, 4])
        Waves(1).elements = 1;
    elseif ismember(par.ComboIndex(index).type, [5, 6, 7, 8])
        Waves(1).elements = 2;
    elseif ismember(par.ComboIndex(index).type, [9, 10, 11, 12])
        Waves(1).elements = 1;
    elseif ismember(par.ComboIndex(index).type, [14, 15, 15, 16])
        Waves(1).elements = 2;
    
    elseif ismember(par.ComboIndex(index).type, [17, 18, 20, 21, 23, 24, 26, 27, 29, 30, 32, 33, 35, 36])
        Waves(1).elements = 1;
    elseif ismember(par.ComboIndex(index).type, [19, 22, 25, 28, 31, 34, 37])
        Waves(1).elements = 2;
    end
end



% save random seeds for reproducibility
saveseed=rand('seed');
savenseed=randn('seed');
rand('seed',index*20);
randn('seed',index*20);

% initialize the event struct with the right number of elements  
event(3) = struct('Note', [], 'StartTime', [], 'StopTime', [], ...
                            'Trial', []);

w = Waves(1).wave;
% Normalizes to 5 ~ 80 db 
% Sounds are normalized individually above now
%w = 5 ./ max(abs(w(:))) .* w;

% Now, put it in the silence:
w = [zeros(par.PreStimSilence * par.SamplingRate, size(w,2));...
     w;...
     zeros(par.PostStimSilence * par.SamplingRate, size(w,2))];

% generate the event structure:
event(1) = struct('Note',['PreStimSilence , ' Waves.name{1}],...
               'StartTime',0,...
               'StopTime',par.PreStimSilence,...
               'Trial',[]);
event(2) = struct('Note',['Stim , ' Waves.name{1}],...
                  'StartTime',par.PreStimSilence,...
                  'StopTime', par.PreStimSilence + par.Duration,...
                  'Trial',[]);
event(3) = struct('Note',['PostStimSilence , ' Waves.name{1}],...
                  'StartTime',par.PreStimSilence + par.Duration, ...
                  'StopTime',par.PreStimSilence+ par.Duration + par.PostStimSilence,...
                  'Trial',[]);

% return random seed to previous state
rand('seed',saveseed);
randn('seed',savenseed);
end
