function o = OverlappingPairs(varargin)
% Overlapping Pairs is a variation of the ContextProbe paradigm and I
% directly changed things in Mateo's NaturalPairs
%   properties:
%   Background: Background texture that will be used (dropdown)
%   Foreground: Foreground marmoset vocalization that will be used
%   Combos: Select whether to play the chosen sounds alone or in
%           combinations
%   PreStimSilcence: In seconds
%   PostStimSilence: In seconds
%   SilenceOnset: In seconds, amount of silence before a sound plays
%   Duration: Duration in seconds of the individuale sound snippets used to
%             build the sound sequence
%   SequenceStructure: The order in which the sound snippets are organized
%             in the sequences 
%   SliceStart: In seconds. Point at which start slicing the original
%             sounds to generate the snippets 
%   SamplingRate
%
%
% methods: waveform, set, get
% 

switch nargin
case 0
    % if no input arguments, create a default object
    s = SoundObject ('OverlappingPairs', 100000, 0, 0, 0, {}, 1, ...
                     {'BG_Folder', 'popupmenu', 'Background1|Background2|Background4',...
                      'FG_Folder', 'popupmenu', 'Foreground1|Foreground2|Foreground3|Foreground4',...
                      'Combos', 'popupmenu', 'No|Manual|Yes',...
                      'Background', 'edit', [1,2],...
                      'Foreground', 'edit', [1,2],...
                      'PreStimSilence', 'edit', 0.5,...
                      'PostStimSilence', 'edit', 0.5,...
                      'SilenceOnset', 'edit', 0.5,...
                      'Duration','edit', 1,...
                      'RefRepCount','edit', 1,...
                      'SoundRepeats', 'popupmenu', 'Yes|No',...
                      'Binaural', 'popupmenu', 'No|Yes',...
                      'Synthetic', 'popupmenu', 'No|C-T-S-ST|C-T-S-ST-STM',...
                      'Dynamic', 'popupmenu', 'No|Yes',...
                      'NormalizeRMS', 'popupmenu', 'No|Yes|Some'...
                      'Ramp', 'popupmenu', 'No|Yes', ...
                      'SNR', 'edit', 0, ...
                      });
                  
    o.Background = [3,2,1,1,5,1];
    o.Foreground = [4,5,4,5,6,7];
    o.Combos = 'Manual';
    o.Duration = 1;
    o.PreStimSilence = 0.5;
    o.PostStimSilence = 0.5;
    o.SilenceOnset = 0.5;
    o.SamplingRate=100000;
    o.SliceStart = 0.5;
    o.MaxIndex = 0;
    o.RefRepCount = 1;
    o.SoundPairs = [];
    o.ComboIndex = [];
    o.Sounds = [];
    o.bg_SoundPath = '';
    o.fg_SoundPath = '';
    o.SoundRepeats = 'No';
    o.Binaural = 'No';
    o.Synthetic = 'No';
    o.Dynamic = 'No';
    o.NormalizeRMS = 'No';
    o.Ramp = 'No';
    o.BG_Folder = 'Background2';
    o.FG_Folder = 'Foreground3';
    o.OverrideAutoScale = 1;
    o.SNR = 0;
    
    %
    o = class(o,'OverlappingPairs',s);  
    o = ObjUpdate(o);
    
case 1
    % if single argument of class SoundObject, return it
    if isa(varargin{1},'OverlappingPairs')
        o = varargin{1};
    else
        error('Wrong argument type');
    end
otherwise
    error('Wrong number of input arguments');
end