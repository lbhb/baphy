function Duration = LogDuration (o,HW, StimEvents, globalparams, exptparams, TrialIndex)
% Specifies how long we want to collect data

% the duration is when sound ends plus response time plus
% PosTargetLickWindow:

% find max StopTime
EndTimes = cat(1,StimEvents.StopTime);
Duration = max(EndTimes);

