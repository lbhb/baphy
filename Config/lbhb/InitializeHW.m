function [HW, globalparams] = InitializeHW (globalparams)
% function HW = InitializeHW (globalparams);
%
% InitializeHW initializes the hardware based on which hardware setup is
% used and the parameters of the experiments specified in globalparams.
% Specific values are now lab-dependent.  The range of possible HWSetup
% values is specified in BaphyMainGuiItems (also lab-dependent)
%
% Nima, original November 2005
% SVD, lab specific setups 2012-05
%
global SAVEPUPIL SAVEPHOTOMETRY SAVELICK
global FORCESAMPLINGRATE
global HW

if ~exist('globalparams','var')
    globalparams=struct();
end
if ~isfield(globalparams,'HWSetup')
    globalparams.HWSetup=0;
end
if ~isfield(globalparams,'Physiology')
    globalparams.Physiology='No';
end

% create a default HW structure
HW=HWDefaultNidaq(globalparams);

doingphysiology = ~strcmp(globalparams.Physiology,'No');

% Based on the hardware setup, start the initialization:
switch globalparams.HWSetup
  
  case 0 % TEST MODE
    % create an audioplayer object which lets us control
    % start, stop, smapling rate , etc.
    HW.AO = audioplayer(rand(4000,1), HW.params.fsAO);
    %HW=IOMicTTLSetup(HW);
    %start(HW.AI);
    HW.DIO.Line.LineName = {'Touch','TouchL','TouchR'};
    
  case {1},  
    %% (SB-1) SMALL BOOTH 1
                   % if doingphysiology then testing OpenEphys

    DAQID = 'Dev1'; % NI BOARD ID WHICH CONTROLS STIMULUS & BEHAVIOR
    niResetDevice(DAQID);
    
    % DIGITAL IO
    
    % Copied from LB-1 (yes, the comment below says copied from SB-1, but
    % that was an older version of this setup).
    HW=niCreateDO(HW,DAQID,'port0/line0:2','TrigAI,TrigAO,TrigAIInv','InitState',[0 0 1]);
    % port0/line2 reserved for inverse TrigAI
    HW=niCreateDO(HW,DAQID,'port0/line3','Pump','InitState',0);
    HW=niCreateDI(HW,DAQID,'port0/line4','Touch');
    HW=niCreateDO(HW,DAQID,'port0/line5','Light','InitState',0);
    HW=niCreateDO(HW,DAQID,'port0/line6','Light2','InitState',1);
    HW=niCreateDO(HW,DAQID,'port0/line7','Light3','InitState',0);
    %HW=niCreateDO(HW,DAQID,'port0/line4','Shock','InitState',0);
    
    %HW=niCreateDI(HW,DAQID,'port0/line5','Touch');
    
    % ANALOG INPUT
    HW=niCreateAI(HW,DAQID,'ai0:1','Touch,Microphone','/Dev1/PFI0');
    
    % ANALOG OUTPUT
    %HW=niCreateAO(HW,DAQID,'ao0:1','SoundOut1,SoundOut2','/Dev1/PFI1');
    HW=niCreateAO(HW,DAQID,'ao0:1','SoundOut2,SoundOut1','/Dev1/PFI1');
    
    % SETUP SPEAKER CALIBRATION
    %HW.Calibration.Speaker = 'FreeFieldCMB1';
    %HW.Calibration.Microphone = 'BK4944A';
    %HW.Calibration = IOLoadCalibration(HW.Calibration);
   
    % no filter, so use higher AO sampling rate in some sound objects:
    FORCESAMPLINGRATE=[];
    
    HW.params.SoftwareEqz(:)=0;%calibated 11/8/21016 DS & LAS, see "H:\daq\Calibrations\SB1\2016-11-08 16-04.jpg"  
    %SoftawreEqz for left speaker should be -4.7 (stimuli intended to be
    %played at 80 dB SPL will be played at 75.3 dB SPL without
    %equalization)
    
    % testing openephys
    if doingphysiology
        msgbox('open ephys');
        % open ephy connection code here
        %[HW,globalparams] = IOConnectWithManta(HW,globalparams); 
    end
    
    
  case {2,3,8,9,10,11,12,13,14,15,20,21}
      %% (LB-1) LARGE SOUND BOOTH 1  (2,3,8,9,10,11)
      % (LB-3) LARGE SOUND BOOTH 3  (12,13,14,15)
      
      % LB-1
      % setup 2,8, = audio channel 1 (AO0) on Right
      % setup 3,9,10 = audio channel 2 (AO0) on Left
        
      % LB-3:
      % East-facing configuration
      % setup 12 = audio channel 1 (AO0) on Left of experimentor facing
      % into booth
      % setup 14 = audio channel 1 (AO0) on Left + Stim on AO2
      % setup 13 = audio channel 2 (AO0) on Right
      % setup 15 = audio channel 2 (AO0) on Right + Stim on AO2
      
    DAQID = 'Dev1'; % NI BOARD ID WHICH CONTROLS STIMULUS & BEHAVIOR
    niResetDevice(DAQID);

    % DIGITAL IO
    % "TrigAI" and "TrigAO" are special identifiers for DIO lines used to
    % trigger analog in and out, respectively.
    HW=niCreateDO(HW,DAQID,'port0/line0:2','TrigAI,TrigAO,TrigAIInv','InitState',[0 0 1]);
    
    % port0/line2 reserved for inverse TrigAI
    HW=niCreateDO(HW,DAQID,'port0/line3','Pump','InitState',0);
    HW=niCreateDI(HW,DAQID,'port0/line4','Touch');
    HW=niCreateDO(HW,DAQID,'port0/line5','Light','InitState',0);
    
    % old cue lights (deprecated?):
    HW=niCreateDO(HW,DAQID,'port0/line6','Light2','InitState',1);
    HW=niCreateDO(HW,DAQID,'port0/line7','Light3','InitState',0);
    
    %% ANALOG INPUT
    HW=niCreateAI(HW,DAQID,'ai0:1','Touch,Microphone','/Dev1/PFI0');
    
    %% ANALOG OUTPUT
    % multiple configurations for the same booth
    % temp removed Light support for LB1 open ephys (10 & 11)
    switch globalparams.HWSetup
        case {2,12,11}
            HW=niCreateAO(HW,DAQID,'ao0:1','SoundOut1,SoundOut2','/Dev1/PFI1');
        case {3,13,10}
            HW=niCreateAO(HW,DAQID,'ao0:1','SoundOut2,SoundOut1','/Dev1/PFI1');
        case {8,14,21}
            HW=niCreateAO(HW,DAQID,'ao0:2','SoundOut1,SoundOut2,LightOut1',...
                '/Dev1/PFI1');
        case{9,15,20}
            HW=niCreateAO(HW,DAQID,'ao0:2','SoundOut2,SoundOut1,LightOut1',...
                '/Dev1/PFI1');
        otherwise
            error('HW setup not defined');
    end
    
    % no filter, so use higher AO sampling rate in some sound objects:
    FORCESAMPLINGRATE=[];
    
    switch globalparams.HWSetup
        case {2,3,8,9,10,11,20,21}
            HW.params.SoftwareEqz(:)=2;
        case {12,14}
            % east-facing, L speaker is contralateral (primary)
            HW.params.SoftwareEqz(:)=0;
        case {13,15}
            % west-facing, R speaker is contralateral (primary)
            HW.params.SoftwareEqz(:)=0;
        otherwise
            HW.params.SoftwareEqz(:)=0;
    end
    
    %% COMMUNICATE WITH MANTA
    if doingphysiology  
        switch globalparams.HWSetup
            case  {12,13,14,15}
                %LB3 (tateril)
                %HW.OEP = struct('url','tcp://10.147.70.43:5556');
                % Now using coyote to collect open ephys. Need new IP
                %HW.OEP = struct('url', 'tcp://10.147.70.38:5556'); %Use for OEP running on coyote (NOT USED ANYMORE?)
                HW.OEP = struct('url', 'tcp://10.147.70.28:5556'); %Use for OEP running on Tateril (localhost)
                %HW.OEP = struct('url', 'tcp://10.147.70.9:5556'); %Use for OEP running on badger (patched in to LB2 for ridiculous single-neuropixels reasons)
                [HW,globalparams] = IOConnectWithOpenEphys(HW,globalparams);
            case {10,11,20,21}
                %LB-1
                %HW.OEP = struct('url','tcp://10.147.71.8:5556');
                %HW.OEP = struct('url','tcp://10.147.70.64:5556');
                % mole
                %HW.OEP = struct('url','tcp://10.147.70.16:5556');
                % muskrat
                HW.OEP = struct('url','tcp://10.147.70.68:5556');
                [HW,globalparams] = IOConnectWithOpenEphys(HW,globalparams);
            otherwise
                [HW,globalparams] = IOConnectWithManta(HW,globalparams); 
        end
    end
    
    %% COMMUNICATE WITH Pupil
    if SAVEPUPIL
        [HW,globalparams] = IOConnectPupil(HW,globalparams);
    end
    %% COMMUNICATE WITH photometry
    if SAVEPHOTOMETRY 
        [HW,globalparams] = IOConnectPupil(HW,globalparams,'Photometry');
    end
    %% COMMUNICATE WITH Lick
    if SAVELICK
        [HW,globalparams] = IOConnectPupil(HW,globalparams,'Lick');
    end
    
  case {4,5,16,17,18,19},  % (LB-2) LARGE SOUND BOOTH 2
      % setup 4 = audio channel 1 (AO0) on Right
      % setup 5 = audio channel 2 (AO0) on Left
    %HW.params.fsAI=25000; % max when using Differential AI config
    %HW.params.fsAI=50000; % max when using Referenced Single-Ended AI config
    DAQID = 'Dev1'; % NI BOARD ID WHICH CONTROLS STIMULUS & BEHAVIOR
    niResetDevice(DAQID);

    %% DIGITAL IO
    % "TrigAI" and "TrigAO" are special identifiers for DIO lines used to
    % trigger analog in and out, respectively.
    HW=niCreateDO(HW,DAQID,'port0/line0:2','TrigAI,TrigAO,TrigAIInv','InitState',[0 0 1]);
    % port0/line2 reserved for inverse TrigAI
    HW=niCreateDO(HW,DAQID,'port0/line3','Pump','InitState',0);
    HW=niCreateDI(HW,DAQID,'port0/line4','Touch');
    %HW=niCreateDO(HW,DAQID,'port0/line5','Light','InitState',0); % spout light. Currently not plugged in.
    HW=niCreateDO(HW,DAQID,'port0/line6','Light2','InitState',0,'OnState',0); % room light
    HW=niCreateDI(HW,DAQID,'port0/line7','LeverPress'); % Marmoset lever.
    HW=niCreateDO(HW,DAQID,'port0/line5','Buzzer','InitState',0);
    
    %HW=niCreateDO(HW,DAQID,'port0/line7','Light3','InitState',0);
    %HW=niCreateDO(HW,DAQID,'port0/line4','Shock','InitState',0);
    
    %% ANALOG INPUT
%     if 1
%         HW=niCreateAI(HW,DAQID,'ai0:2','Touch,Microphone,LeverPress','/Dev1/PFI0');
%     else
%         HW=niCreateAI(HW,DAQID,'ai0:2','Touch,Microphone,LeverPress','/Dev1/PFI0',{'RSE','Diff','RSE'});
%     end
    %HW.params.LickSign  = [1 1];
    %HW=niCreateAI(HW,DAQID,'ai0:1','Touch,Microphone','/Dev1/PFI0');
    HW.params.LickSign  = [1];
    HW=niCreateAI(HW,DAQID,'ai0','Touch','/Dev1/PFI0');
    %HW.params.LickSign  = [-1];%invert lever presses but not licks
    %HW=niCreateAI(HW,DAQID,'ai2','LeverPress','/Dev1/PFI0');
    
    
    %% ANALOG OUTPUT
    if any(globalparams.HWSetup==[4 16 18])
        HW=niCreateAO(HW,DAQID,'ao0:1','SoundOut1,SoundOut2','/Dev1/PFI1');
         HW.params.SoftwareEqz=[4.7 7.5]; %atten on [Right Left]
         HW.params.SoftwareEqz=[1.2 2]; %Both at notch #9 5/21/19, LAS
         HW.params.SoftwareEqz=[0 0]; %Old amplifier broke. New amplifier is from LB3, has continuous knobs. Calibration somewhat unstable 9/29/20, LAS
    else
        HW=niCreateAO(HW,DAQID,'ao0:1','SoundOut2,SoundOut1','/Dev1/PFI1');
        HW.params.SoftwareEqz=[7.5 4.7]; %atten on [Left Right]
        HW.params.SoftwareEqz=[2 1.2]; %Both at notch #9 5/21/19, LAS
        HW.params.SoftwareEqz=[0 0]; %Old amplifier broke. New amplifier is from LB3, has continuous knobs. Calibration somewhat unstable 9/29/20, LAS
    end
    
    % no filter, so use higher AO sampling rate in some sound objects:
    FORCESAMPLINGRATE=[];
    
    
    %% COMMUNICATE WITH MANTA
    if doingphysiology  
        if any(globalparams.HWSetup==16:19)
            cd('C:\code\baphy\Hardware\OpenEphys\')
            zeroMQrr
            cd('C:\Users\lbhb')
        end
        switch globalparams.HWSetup
            case {16,17}
                HW.OEP = struct('url','tcp://10.147.70.58:5556');
                [HW,globalparams] = IOConnectWithOpenEphys(HW,globalparams);
            case {18,19}
                HW.OEP = struct('url','tcp://10.147.70.32:5556');
                [HW,globalparams] = IOConnectWithOpenEphys(HW,globalparams);
            otherwise
                [HW,globalparams] = IOConnectWithManta(HW,globalparams); 
        end
    end
    
    %% COMMUNICATE WITH Pupil
    if SAVEPUPIL
        [HW,globalparams] = IOConnectPupil(HW,globalparams);
    end
    %% COMMUNICATE WITH Lick
    if SAVELICK
        [HW,globalparams] = IOConnectPupil(HW,globalparams,'Lick');
    end

     case {6,7},  % (DR-1) Dark Room 1
      % setup 6 = audio channel 1 (AO0) on Right
      % setup 7 = audio channel 2 (AO0) on Left

    DAQID = 'Dev1'; % NI BOARD ID WHICH CONTROLS STIMULUS & BEHAVIOR
    niResetDevice(DAQID);

    %% DIGITAL IO
    % "TrigAI" and "TrigAO" are special identifiers for DIO lines used to
    % trigger analog in and out, respectively.
    HW=niCreateDO(HW,DAQID,'port0/line0:2','TrigAI,TrigAO,TrigAIInv','InitState',[0 0 1]);
    % port0/line2 reserved for inverse TrigAI
    HW=niCreateDO(HW,DAQID,'port0/line3','Pump','InitState',0);
    HW=niCreateDI(HW,DAQID,'port0/line4','Touch');
    HW=niCreateDO(HW,DAQID,'port0/line5','Light','InitState',0);
    HW=niCreateDO(HW,DAQID,'port0/line6','Light2','InitState',0);
    HW=niCreateDO(HW,DAQID,'port0/line7','Light3','InitState',0);
    HW=niCreateDO(HW,DAQID,'port0/line4','Shock','InitState',0);
    
    %% ANALOG INPUT
    HW=niCreateAI(HW,DAQID,'ai0:1','Touch,Microphone','/Dev1/PFI0');
    
    %% ANALOG OUTPUT
    if globalparams.HWSetup==6
        HW=niCreateAO(HW,DAQID,'ao0:1','SoundOut1,SoundOut2','/Dev1/PFI1');
    else
        HW=niCreateAO(HW,DAQID,'ao0:1','SoundOut2,SoundOut1','/Dev1/PFI1');
    end
    
    % no filter, so use higher AO sampling rate in some sound objects:
    FORCESAMPLINGRATE=[];
    
    HW.params.SoftwareEqz(:)=1.5;
    
    %% COMMUNICATE WITH MANTA
    if doingphysiology  [HW,globalparams] = IOConnectWithManta(HW,globalparams); end
    
end % END SWITCH

if isfield(HW,'MANTA')
  HW.params.DAQSystem = 'MANTA'; 
elseif isfield(HW,'OEP')
  HW.params.DAQSystem = 'Open-Ephys'; 
else
  HW.params.DAQSystem = 'AO';
end
globalparams.HWparams = HW.params;

function CBF_Trigger(obj,event)
[TV,TS] = datenum2time(now); fprintf([' >> Trigger received (',TS{1},')\n']); 

