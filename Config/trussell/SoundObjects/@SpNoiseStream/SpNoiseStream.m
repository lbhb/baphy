function o = SpNoiseStream(varargin)
% SpNoiseStream is based on the SpNoise object
%
% properties:
%   PreStimSilcence
%   PostStimSilence
%   SamplingRate
%   Loudness
%   Subsets: can be 1, 2, 3 or 4. each contains 30 different sentences from
%       Timit database. each subset is 30 sentences spoken by 30 different
%       speakers (15 male 15 female). sentences in subset 1 and 4 are three
%       seconds long, subset 2 and 3 are four seconds. Subset 4 is all the
%       same sentence: "She had your dark suit in greasy wash water all
%       year"
%   Phonemes: contains the phoneme events for the specified names.
%   Words: contains the word events for the specified names.
%   Sentences: contains the sentece events for the specified names.
% 
% methods: waveform, LabelAxis, set, get
% 

% SVD 2016-01-20

switch nargin
case 0
    
    % SpNoise subobject, fixed parameters:
    %                  'IterateStepMS','edit',0,...
    %                  'IterationCount','edit',0,...
    %                  'TonesPerOctave','edit',0,...
    %                  'UseBPNoise','edit',1,...
    %                  'ShuffleOnset','edit',0,...
    %                  'RelAttenuatedB','edit',0,...
    
    s = SoundObject ('SpNoiseStream', 100000, 0, 0, 0, {}, 1, ...
                     {'LowFreq','edit',[1000 4000],...
                      'HighFreq','edit',[2000 8000],...
                      'RelAttenuatedB','edit',[0 10],...
                      'SplitChannels','popupmenu','No|Yes',...
                      'BaseSound','popupmenu','Speech|FerretVocal',...
                      'Subsets','edit',1,...
                      'BaselineFrac','edit',0,...
                      'RepIdx','edit',[0 1],...
                      'Duration','edit',3});
    o.LowFreq = [1000 4000];
    o.HighFreq = [2000 8000];
    o.RelAttenuatedB=[0 10];
    o.SplitChannels='No';
    o.BaseSound = 'Speech';
    o.Subsets = 1;
    o.BaselineFrac=0;  % minimum sound level (as a fraction of peak)
    o.RepIdx=[0 1];
    o.Duration = 3;
    o.PreStimSilence = 0.5;
    o.PostStimSilence = 0.5;
    o.SamplesPerGroup=10;
    
    o.SpId=[];
    o.dbScale=[];
    o.MaxIndex=0;
    o.StreamCount=2;
    
    % fix at defaults for SpNoise
    o.IterateStepMS = 0;% if zero, don't iterate. no effect if TonesPerOctave>0
    o.IterationCount = 0;% if zero, don't iterate. no effect if TonesPerOctave>0
    o.TonesPerOctave = 0;% if zero, use BP noise
    o.TonesPerBurst=round(log2(o.HighFreq./o.LowFreq).*o.TonesPerOctave);
    o.UseBPNoise=1;
    o.SNR = 1000;
    o.ShuffleOnset=2; % if 2, randomly rotate stimulus waveforms in time
    o.SetSizeMult=1;  % for multi-channel stim, how many times larger should MaxIndex be than the original set
    o.CoherentFrac=0.1;  % for multi-channel stim, what fraction should be the same in all channels
    o.SingleBandFrac=0;  % for multi-channel stim, what fraction should be the same in all channels
    %o.Phonemes = {struct('Note','','StartTime',0,'StopTime',0)};
    %o.Words= {struct('Note','','StartTime',0,'StopTime',0)};    
    %o.Sentences = {''};
    o.SamplingRate=100000;
    %
    o = class(o,'SpNoiseStream',s);
    o = ObjUpdate(o);
    
case 1
    % if single argument of class SoundObject, return it
    if isa(varargin{1},'SpNoise')
        o = varargin{1};
    else
        error('Wrong argument type');
    end
    
case 7
    error('SpeechPhoneme format not supported for this object');
    
otherwise
    error('Wrong number of input arguments');
end