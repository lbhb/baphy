

directory = '/KiloSort/Prince/PRN002/raw/PRN002a01/PRN002a01_p_FTC2022-10-10_09_31_48';
session = Session(directory);


session.recordNodes{1}.recordings{1}.continuous.keys
session.recordNodes{2}.recordings{1}.continuous.keys

spike_rec = 1
lfp_rec = 2

k = session.recordNodes{lfp_rec}.recordings{1}.continuous.keys;
fs_lfp = session.recordNodes{lfp_rec}.recordings{1}.continuous(k{1}).metadata.sampleRate;
startTimestamp_lfp = session.recordNodes{lfp_rec}.recordings{1}.continuous(k{1}).metadata.startTimestamp;

ev =  session.recordNodes{spike_rec}.recordings{1}.ttlEvents
event_keys = ev.keys()
k = event_keys{find(contains(event_keys,'AP') & contains(event_keys,'Neuropix'),1)}
trial_starts = [ev(k).sample_number(find(ev(k).state==1))]
trial_stops = [ev(k).sample_number(find(ev(k).state==0))]

trial_starts_ts = [ev(k).timestamp(find(ev(k).state==1))]
trial_stops_ts = [ev(k).timestamp(find(ev(k).state==0))]

cont = session.recordNodes{spike_rec}.recordings{1}.continuous;
k = cont.keys;
k=k{1};
fs = cont(k).metadata.sampleRate;
startTimestamp = cont(k).metadata.startTimestamp;


for ii=1:length(trial_starts_ts)
   i1=min(find(cont(k).timestamps>=trial_starts_ts(ii)));
   i2=min(find(cont(k).timestamps>=trial_stops_ts(ii)));
   expected_frames = trial_stops(ii)-trial_starts(ii);
   fprintf('Trial %2d: %7d-%7d, actual=%6d expected=%6d diff=%d\n', ii, i1, i2, i2-i1, expected_frames, (i2-i1)-expected_frames);
end

