function [HW,globalparams] = IOConnectWithOpenEphys(HW,globalparams)

global Verbose; if isempty(Verbose) Verbose = 0; end

%zeroMQrr('CloseThread', HW.OEP.url);

zeroMQrr('CloseAll');


% TEST CONNECTION
isacquiring=check_OEP_acquiring(HW,true);
time_between_checks=.25;
checks_per_report=4*3;%3 sec
timeout=30;%seconds
timeout_in_checks=timeout/time_between_checks;
switch isacquiring
    case {'1','StoppedRecording'}
    case {'0','failed waiting for a reply'}
        i=0;
        while ~strcmp(isacquiring,'1') && i< timeout_in_checks
            isacquiring=check_OEP_acquiring(HW,false);
            pause(time_between_checks);
            if(mod(i,checks_per_report)==0)
                switch isacquiring
                    case '0'
                        fprintf('\nNot acquiring, press the play button on Open Ephys')
                    case 'failed waiting for a reply'
                        fprintf(['\n No connection to OpenEphys on ',HW.OEP.url,'. \nTry pressing ''Restart Connection'' on the Network Event Module, check for correct IP address'])
                end
            end
            i=i+1;
        end
    otherwise
        %Error
        error('check_OEP_acquiring returned invalid status');
        return
end
if ~(strcmp(isacquiring,'1')||strcmp(isacquiring,'StoppedRecording')) 
    %timed out
    return
end
isrecording=zeroMQrr_get_response(HW.OEP.url, 'IsRecording');
while strcmp(isrecording,'1')
        fprintf('\nOpen-Ephys is in recording mode. Turning off and pausing for .5 sec to start new recording.')
        zeroMQrr('Send',HW.OEP.url,'StopRecord')
        pause(.5);
        isrecording=zeroMQrr_get_response(HW.OEP.url, 'IsRecording');
end
if(~strcmp(isrecording,'0'))
    warning(['\nisrecording did not return 0 or 1, it returned:',isrecording])
end
% CONNECT TO OEP
% if ~HW.OEP.Connect
%   fprintf('Waiting for connect from Open Ephys ... '); Connected = 0;
%   HW.OEP.handle = zeroMQwrapper('StartConnectThread',HW.OEP.url);
%   [isacquiring, details]=zeroMQwrapper('Send',HW.OEP.handle, 'IsAcquiring', 1);
%   BaphyMANTAConn = tcpip('0.0.0.0',HW.MANTA.Port,'TimeOut',10,'OutputBufferSize',2^18,'InputBufferSize',2^18,'NetworkRole','server');
%   fopen(BaphyMANTAConn);
%   flushinput(BaphyMANTAConn); flushoutput(BaphyMANTAConn);
%   set(BaphyMANTAConn,'Terminator',HW.MANTA.MSGterm);
%   fprintf([' TCP IP connection established.\n']);
% end

% SEND INITIALIZATION
SaveFile_baphy_computer = [M_setRawFileName(globalparams.mfilename),'_'];
SaveFile_OEP_computer=SaveFile_baphy_computer;
%SaveFile_OEP_computer=strrep(SaveFile_OEP_computer,'L:\','D:\Data\');
SaveFile_OEP_computer=strrep(SaveFile_OEP_computer, 'L:\','/data/'); % for baphy on Weasel, OEP on mole
SaveFile_OEP_computer=strrep(SaveFile_OEP_computer, 'K:\', '/Data/'); % for baphy on Tateril, OEP on coyote
SaveFile_OEP_computer=strrep(SaveFile_OEP_computer,'H:\daq\','/home/luke/Data/');SaveFile_OEP_computer=strrep(SaveFile_OEP_computer,'\','/');
SaveFile_OEP_computer=strrep(SaveFile_OEP_computer,'A:/','/home/luke/Data/');SaveFile_OEP_computer=strrep(SaveFile_OEP_computer,'\','/');
SaveFile_OEP_computer=strrep(SaveFile_OEP_computer,'B:/','/media/HD/Data/');SaveFile_OEP_computer=strrep(SaveFile_OEP_computer,'\','/');
fprintf('\nOpen-Ephys saving to: %s\n',SaveFile_OEP_computer);
createNewDir=true;
[RecDir_OEP_computer,fn,ext]=fileparts(SaveFile_OEP_computer);
[RecDir_baphy_computer,fn,ext]=fileparts(SaveFile_baphy_computer);
PrependText=fn;
AppendText=[];
message='';
%     zeroMQrr('Send',url, 'StartAcquisition', 1);
%options:
if createNewDir
    message=[message ' CreateNewDir=1'];
end
if ~isempty(RecDir_OEP_computer)
    message=[message ' RecDir=' RecDir_OEP_computer];
end
if ~isempty(PrependText)
    message=[message ' PrependText=' PrependText];
end
if ~isempty(AppendText)
    message=[message ' AppendText=' AppendText];
end
%     message=sprintf('StartAcquisition CreateNewDir=%i RecDir=%s PrependText=%s AppendText=%s');
disp(message)

isrec=false;
while(~isrec)
    zeroMQrr('Send',HW.OEP.url, ['StartRecord ',message]);
    start_time = datestr(now,'_YYYY-mm-dd_hh-MM-SS');
pause(.25);
isrecording=zeroMQrr_get_response(HW.OEP.url, 'IsRecording');
switch isrecording
    case 'StartedRecording'
        isrec=true; %newer OEP versions
    case '1'
        isrec=true;
    case {'0','failed waiting for a reply'}
        pause(.5);
        isrecording=zeroMQrr_get_response(HW.OEP.url, 'IsRecording');
        isrec=false;
        fprintf('/nFailed to start recording. Pausing and trying again.')
        pause(.25);
    otherwise
        error(['Open Ephys did not start recording, despite being in acquiring mode. isrecording = ',isrecording])
end
end
HW.OEP.current_recording_path=zeroMQrr_get_response(HW.OEP.url, 'getRecordingPath RecordNode=103');
if isempty(HW.OEP.current_recording_path)
    pth = [RecDir_baphy_computer '/' PrependText start_time];
    if exist(pth,'dir')
        warning('Open Ephys did not return a path, guessing  %s. This path exists so we assume that''s right',pth)
        HW.OEP.current_recording_path = pth;
    else
        try_to_find_path = 1;
        try_count = 0;
        %Try to find the path a few times. Pause 1 sec between each try.
        %OEP on Mole is slow to start, so baphy gets here before OEP is recording.
        while try_to_find_path
            a=dir(RecDir_baphy_computer);
            fi = find(arrayfun(@(x)length(x.name)>length(PrependText) && contains(x.name,PrependText),a));
            if length(fi) ~=1
                try_count = try_count + 1;
                if try_count < 5
                    warning('Open Ephys did not return a path, guessed  %s or any path starting with %s*, but couldn''t find anything, pausing 1 sec and trying again.',pth,[RecDir_baphy_computer '/' PrependText])
                    pause(1)
                else
                    error('Open Ephys did not return a path, guessed  %s or any path starting with %s*, but couldn''t find anything. Tried 5 times, giving up.',pth,[RecDir_baphy_computer '/' PrependText])
                end
            else
                HW.OEP.current_recording_path = [RecDir_baphy_computer '/' a(fi).name];
                warning('Open Ephys did not return a path, searched for any path starting with %s* and found %s, using it.',[RecDir_baphy_computer '/' PrependText], HW.OEP.current_recording_path)
                try_to_find_path=false;
            end
        end
    end
else
    HW.OEP.current_recording_path
end
globalparams.rawfilename=HW.OEP.current_recording_path;
a=2;
disp(globalparams.rawfilename)


% % TRANSFER VARIABLES
% Vars = {'MG.DAQ.ArraysByBoard','MG.DAQ.SystemsByBoard',...
%     'MG.DAQ.ElectrodesByChannel'};
% for i=1:length(Vars)
%     MSG = ['GETVAR',HW.MANTA.COMterm,Vars{i},HW.MANTA.MSGterm];
%     RESP = IOSendMessageManta(HW,MSG);
%     if RESP(end)=='!'; RESP = RESP(1:end-1); end
%     globalparams.(Vars{i}(find(Vars{i}=='.',1,'last')+1:end)) = eval(RESP);
% end

end

function isacquiring=check_OEP_acquiring(HW,verbose)
if(verbose)
    fprintf(['Checking Connection...']);
end
try
    [isacquiring, details]=zeroMQrr('Send',HW.OEP.url, 'IsAcquiring', 1);
catch err
    isacquiring = 'Error';
end
switch isacquiring
    case {'0','1'};
        if(verbose)
            fprintf('OK\n');
        end
        HW.OEP.Connect = 1;
    case 'failed waiting for a reply';
        if(verbose)
            fprintf(['No connection to OpenEphys on ',HW.OEP.url,'\n']);
        end
        HW.OEP.Connect = 0;
    case 'Error';
        if(verbose)
            fprintf(['Error connecting to OpenEphys: ',err.message]);
        end
        HW.OEP.Connect = 0;
    otherwise;
        if(verbose)
            fprintf(['Unknown Error connecting to OpenEphys, isaquiring returned: ',isacquiring,'\n']);
        end
        HW.OEP.Connect = 0;
end
end

function response=zeroMQrr_get_response(url,message)
responded=false;
while(~responded)
    response=zeroMQrr('Send',url, message,1);
    if strcmp(response,'failed waiting for a reply')
        fprintf('/nDidn''t get a response to 0mq message %s. Pausing and trying again.',message)
        pause(.25);
    else
        responded=true;
    end
end
end
