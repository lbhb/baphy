function imp=OEP_impedance_xml_to_array(fn)

a=xml2struct(fn);
for i=1:length(a.CHANNEL_IMPEDANCES.CHANNEL)
    imp.attributes(i)=a.CHANNEL_IMPEDANCES.CHANNEL{i}.Attributes;
    imp.magnitude(i)=str2double(imp.attributes(i).magnitude);
    imp.phase(i)=str2double(imp.attributes(i).phase);
end