function run_info= OEPgetinfo(OEPdir)
if ~exist(OEPdir,'dir')
    error('Open ephys data directory not found in %s',OEPdir)
end

%% Check for another OEP dir in the same place as this one (happen when multiple record nodes started with a slightly different timestamp)
basepath = fileparts(OEPdir);
dd=dir(basepath);
dd(1:2)=[];
if length(dd)>1
    for i=1:length(dd)
        inds = strfind(dd(i).name,'_');
        start_times(i) = datenum(dd(i).name(inds(3)+1:end),'YYYY-mm-dd_hh-MM-SS');
    end
    [~,first_ind] = min(start_times);
    warning(['Multiple OEP directories found in %s. Moving them all to ',...
        'the earliest one, %s'], basepath, dd(first_ind).name)
    move_inds =  setdiff(1:length(dd),first_ind);
    for i=1:length(move_inds)
        contents = dir([basepath filesep dd(move_inds(i)).name]);
        contents(1:2)=[];
        for j=1:length(contents)
            source = [basepath filesep dd(move_inds(i)).name filesep contents(j).name];
            dest = [basepath filesep dd(first_ind(i)).name filesep contents(j).name];
            [status, msg] = movefile(source, dest);
            if ~status
                error('Error moving %s to %s. Error message: %s',source, dest, msg)
            end
        end
        [status, msg] = rmdir([basepath filesep dd(move_inds(i)).name]);
        if ~status
            error('Error deleting %s after moving contents to %s. Error message: %s',...
                [basepath filesep dd(move_inds(i)).name],[basepath filesep dd(first_ind(i)).name], msg)
        end
    end
end
%%
get_info=true;
set_json_file=true;
if ~exist([OEPdir filesep 'settings.xml'],'file')
    dd=dir(OEPdir);
    dd(1:2)=[];
    if length(dd) == 1
        OEPdir = [dd.folder filesep dd.name];
    elseif length(dd) == 2 || length(dd) == 3  %Reocrdings with Raw Data and spike events, optionally with LFP
        get_info=false;
        set_json_file=false;
        for i=1:length(dd)
            OEPdirs{i} = [dd(i).folder filesep dd(i).name];
            has_events(i)=exist([OEPdirs{i},filesep,'all_channels.events'],'file');
        end
        if all(has_events)
            datatype = 'OEP';
        elseif ~any(has_events)
            datatype = 'binary';
        else
            error('Some record nodes are OEP, some are binary. Figure out how to deal with this if you need it.')
        end
        for i=1:length(dd)
            all_json_files{i} = [OEPdirs{i} filesep 'experiment1' filesep 'recording1' filesep 'structure.oebin'];
            if ~exist(all_json_files{i},'file')
                error('Determined open ephys data is in flat binary format, but can''t find json file in %s.',all_json_files)
            end
            all_events_folders{i} = list_open_ephys_binary(all_json_files{i},'events');
            all_continuous_data_folders{i} = list_open_ephys_binary(all_json_files{i},'continuous');
            spike_data_folders{i} = list_open_ephys_binary(all_json_files{i},'spikes');
        end
        if sum(cellfun(@length,all_continuous_data_folders))==2
            continuous_inds = find(~cellfun(@isempty,all_continuous_data_folders));
            clear json
            for i=1:length(continuous_inds)
                json(i)=jsondecode(fileread(all_json_files{continuous_inds(i)}));
                %json.continuous(1).sample_rate
            end
            SRs=arrayfun(@(x)x.continuous(1).sample_rate,json);
            LFP_ind = continuous_inds(SRs < 5000);
            continuous_ind = continuous_inds(SRs > 10000);
            if length(LFP_ind)~=1 && length(continuous_ind)~=1
                error('Two nodes are recording continuous data and I can''t figure out which is for LFP, which is for spikes, based on the sampling rate.')
            end
        else
            continuous_ind = find(~cellfun(@isempty,all_continuous_data_folders));            
        end
        if sum(cellfun(@length,spike_data_folders))==2
            error('Two nodes are recording spikes, fiugre out how to handle this.')            
        end
        spike_mask = ~cellfun(@isempty,spike_data_folders);
        continuous_node_id = str2double(OEPdirs{continuous_ind}(end-2:end));
        run_info=get_session_info(OEPdirs{1},continuous_node_id); %Read info from settings.xml
        json_file=all_json_files{continuous_ind}; %set json file to a string for the continuous json file, to be used below to find sampling rate, etc
        %run_info.json_file = json_file; (set below)
        run_info.json_file_spikes = all_json_files{spike_mask};
        if sum(cellfun(@length,all_continuous_data_folders))==2
            run_info.json_file_LFP = all_json_files{LFP_ind};
        else
            run_info.json_file_LFP=[];
        end
        run_info.datatype = datatype;
        events_folders = all_events_folders{continuous_ind};
        continuous_data_folders = all_continuous_data_folders{continuous_ind};
        %run_info(2)=get_session_info(OEPdirs{2}); %Read info from settings.xml
    else
        error('More than 3 folders/file in Open Ephys data directory, %s. More than 3 record nodes? Write more code to choose the right one.')
    end
    
end

if get_info
    run_info=get_session_info(OEPdir); %Read info from settings.xml
    run_info.json_file_LFP=[];
    run_info.json_file_spikes=[];
    if exist([OEPdir,filesep,'all_channels.events'],'file')
        run_info.datatype = 'OEP';
    else
        run_info.datatype = 'binary';
    end
    run_info.OEPdir=OEPdir;
end

% Find channel map in Filters/Channel Map Node. 
%   If the source is Neuropix-PXI, assume this is a channel map used to reduce the number of
%   channels that are spike-sorted online, and store it in run_info.sorted_spikes_channel_map
%   If the source is Rhythm FPGA, assume this is a channel map used to map a UCLA electrode
%   into channels that are ordered by the geometry, and store it in run_info.raw_data_channel_map
%   sorted_spikes_channel_map is used in loadspikeraster.m
%   raw_data_channel_map is used in evpread.m

channel_map_ind = find(strcmp('Filters/Channel Map',{run_info.processors.processorName}));
run_info.sorted_spikes_channel_map=[];
run_info.raw_data_channel_map=[];
if length(channel_map_ind)==1
    if any(strcmp('Sources/Neuropix-PXI',{run_info.processors.processorName}))
        run_info.sorted_spikes_channel_map =  [run_info.processors(channel_map_ind).channels.map];
    elseif any(strcmp('Sources/Rhythm FPGA',{run_info.processors.processorName}))
        run_info.raw_data_channel_map =  [run_info.processors(channel_map_ind).channels.map];
    end
elseif length(channel_map_ind)>1
    error('Two channel maps! Not sure what to do.')
end

switch run_info.datatype
    case 'OEP'
        [data, info,timestamps] = load_open_ephys_data_faster([OEPdir,filesep,'all_channels.events']);
        %eventType: 3 TTL pulses (trials).  5: Network Events (named in messages.events)
        run_info.spikefs=info.header.sampleRate;
        run_info.auxfs=info.header.sampleRate;
        run_info.spikechancount=run_info.num_spike_channels;
        run_info.trialcount=sum(info.eventId(info.eventType==3));
        %event type 3 are tiggers from baphy
        %event type 5 are network events where baphy tells OEP a trial is starting via TCP-IP
        run_info.auxchancount=run_info.num_aux_channels;
        run_info.lfpchancount=run_info.num_spike_channels;
        run_info.lfpfs=info.header.sampleRate;
    case 'binary'
        run_info.auxchancount=run_info.num_aux_channels;
        run_info.lfpchancount=run_info.num_spike_channels;
        run_info.spikechancount=run_info.num_spike_channels;
        if set_json_file
            json_file = [OEPdir filesep 'experiment1' filesep 'recording1' filesep 'structure.oebin'];
            if ~exist(json_file,'file')
                error('Determined open ephys data is in flat binary format, but can''t find json file in %s.',json_file)
            end
            run_info.json_file = json_file;
            events_folders = list_open_ephys_binary(json_file,'events');
            continuous_data_folders = list_open_ephys_binary(json_file,'continuous');
        end
        
        %run_info.event_ind_TTL = find(contains(events_folders,'Neuropix-PXI') & contains(events_folders,'TTL'),1); %Find the first neuropixels data stream, use it to get data from
        run_info.event_ind_TTL = find(contains(events_folders,'TTL'),1); %Find the first neuropixels data stream, use it to get data from
        % SWITCH IF YOU WANT LFP
        sp = split(events_folders(run_info.event_ind_TTL),'/');
        run_info.data_name = sp{1};
        run_info.data_ind = find(strcmp(continuous_data_folders,[run_info.data_name '/']));
        run_info.event_ind_TCP = find(contains(events_folders,'Network_Events') & contains(events_folders,'TEXT_group'));
        if length(run_info.event_ind_TCP)~=1
            error('Couldn''t find Network Events data. Foldername should be something like: Network_Events-109.0/TEXT_group_1/')
        end
        if length(run_info.event_ind_TCP)~=1
            error('Couldn''t find TTL Events data. Foldername should be something like: Neuropix-PXI-100.0/TTL_1/')
        end
        if length(run_info.data_ind)~=1
            error('Couldn''t find continuous data corresponding to TTL events Foldername should be something like: Neuropix-PXI-100.0/')
        end
        %event_data_TCP = load_open_ephys_binary(json_file,'events',run_info.event_ind_TCP);
        json=jsondecode(fileread(json_file));
        run_info.spikefs=json.continuous(1).sample_rate;
        run_info.auxfs=json.continuous(1).sample_rate;
        run_info.lfpfs=json.continuous(1).sample_rate;
        
        event_data_TTL = load_open_ephys_binary(json_file,'events',run_info.event_ind_TTL);
        run_info.trialcount=sum(event_data_TTL.Data==1); %1 are trigger onsets, -1 are offsets
        run_info.json_file = json_file;
end

if run_info.trialcount == 0
    error(['Trial (trigger pulse) count is zero. This likely means OEP is not',...
        ' getting the trigger pulses for some reason. Check the connection.'])
end

run_info.electrode='unknown';
if isfield(run_info,'channel_map_display_name')
    if length(run_info.channel_map_display_name)>12 && strcmpi(run_info.channel_map_display_name(1:12),'channel map ')
        run_info.electrode=run_info.channel_map_display_name(13:end);
    elseif length(run_info.channel_map_display_name)>3 && strcmpi(run_info.channel_map_display_name(1:3),'64D')
        run_info.electrode=[run_info.channel_map_display_name(1:3),lower(run_info.channel_map_display_name(4:end))];
    end
end
if strcmp(run_info.electrode,'unknown')
   % error('unknown electrode')
end
        % use this to mess with the channel map if needed
% chanmap=65:128;
% for i=1:length(chanmap)
%     run_info.processors(2).channels(i).map=chanmap(i);
% end

end