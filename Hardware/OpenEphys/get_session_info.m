function info = get_session_info(directory, continuous_node_id, VERBOSE)

if nargin <2
    continuous_node_id=[];
end
if nargin <3
    VERBOSE=0;
end

% STEP 1: GET FULL PATH FOR DIRECTORY

directory = get_full_path(directory);

%%

% STEP 2: PARSE THE 'SETTINGS.XML' FILE
raw_data_nodes={'Sources/Neuropix-PXI','Sources/Rhythm FPGA','Filters/Channel Map','Filters/Record Node',...
    'Neuropix-PXI','Record Node','Channel Map'}; %nodes over which to look for raw data
directory_contents = dir(directory);

DOMnode = xmlread([directory filesep 'settings.xml']);
xRoot =DOMnode.getDocumentElement;

processors = cell(0);
electrodes = cell(0);
IsNeuroPix = 0;

% LOOP THROUGH TOP-LEVEL NODES
processorIndex = 0;
for i = 1:xRoot.getChildNodes.getLength-1
    if strcmp(xRoot.item(i).getNodeName, 'INFO')
        xInfo = xRoot.item(i);
        for j = 1:xInfo.getChildNodes.getLength-1
            if ~strcmp(xInfo.item(j).getNodeName,'#text')
                name = char(xInfo.item(j).getNodeName);
                try
                    value = char(xInfo.item(j).getFirstChild.getData);
                    except
                    value = 'null'
                end
                info.info.(name)=value;
                if strcmp(name,'VERSION')
                    info.info.VERSION_=info.info.VERSION;
                    dots=strfind(info.info.VERSION_,'.');
                    info.info.VERSION_(dots(2:end))=[];
                    info.info.VERSION_=str2double(info.info.VERSION_);
                end
            end
        end
        
    end
    if strcmp(xRoot.item(i).getNodeName, 'SIGNALCHAIN')
        
        xSignalChain = xRoot.item(i);
        
        % LOOP THROUGH PROCESSOR NODES
        for j = 1:xSignalChain.getChildNodes.getLength-1
            
            if strcmp(xSignalChain.item(j).getNodeName, 'PROCESSOR')
                
                processorIndex = processorIndex + 1;
                
                xProcessor = xSignalChain.item(j);
                
                nodeId = ifstr2num(xProcessor.getAttribute('nodeId'));
                if isempty(nodeId)
                    % backward compatible?
                    nodeId = str2num(char(xProcessor.getAttributes.item(0).getValue));
                end
                processorName = char(xProcessor.getAttribute('name'));
                
                processors(processorIndex).nodeId = nodeId;
                processors(processorIndex).processorName = processorName;
                processors(processorIndex).chainIdx = i;
                if processorIndex==1
                    processors(processorIndex).channels = []; % empty cell for channels
                else
                    processors(processorIndex).channels = processors(processorIndex-1).channels;
                end
                if strcmp(processorName, 'Filters/Spike Detector')
                    
                    electrodeIndex = 0;
                    
                    % LOOP THROUGH ELECTRODE NODES IN SPIKE DETECTOR
                    for k = 1:xProcessor.getChildNodes.getLength-1
                        
                        if strcmp(xProcessor.item(k).getNodeName, 'ELECTRODE')
                            
                            electrodeIndex = electrodeIndex + 1;
                            
                            xElectrode = xProcessor.item(k);
                            
                            electrodeName = char(xElectrode.getAttributes.item(0).getValue);
                            channels = [ ];
                            
                            % LOOP THROUGH CHANNEL NODES FOR ELECTRODE
                            for m = 1:xElectrode.getChildNodes.getLength-1
                                
                                if strcmp(xElectrode.item(m).getNodeName, 'SUBCHANNEL')
                                    
                                    xChannel = xElectrode.item(m);
                                    
                                    ch = str2num(char(xChannel.getAttributes.item(0).getValue));
                                    
                                    channels = [channels ch];
                                    
                                end
                                
                            end
                            
                            electrodes{electrodeIndex,1} = electrodeName;
                            electrodes{electrodeIndex,2} = channels;
                            
                        end
                    end
                elseif any(strcmp(processorName,raw_data_nodes))
                    
                    
                    % LOOP THROUGH ELECTRODE NODES IN SPIKE DETECTOR
                    for k = 1:xProcessor.getChildNodes.getLength-1
                        if strcmp(xProcessor.item(k).getNodeName, 'CHANNEL_INFO')
                            channelIndex = 0;
                            chi=xProcessor.item(k);
                            for m = 1:chi.getChildNodes.getLength-1
                                if strcmp(chi.item(m).getNodeName, 'CHANNEL')
                                    channelIndex = channelIndex + 1;
                                    
                                    xChannel = chi.item(m);
                                    
                                    processors(processorIndex).channels(channelIndex).name = char(xChannel.getAttribute('name'));
                                    processors(processorIndex).channels(channelIndex).gain = str2double(char(xChannel.getAttribute('gain')));
                                    processors(processorIndex).channels(channelIndex).number = str2double(char(xChannel.getAttribute('number')));
                                end
                            end
                        elseif strcmp(xProcessor.item(k).getNodeName, 'CHANNEL')
                            chi=xProcessor.item(k);
                            if(isempty(processors(processorIndex).channels))
                                ch_ind=1;
                                processors(processorIndex).channels(ch_ind).number=str2double(char(chi.getAttribute('number')));
                                processors(processorIndex).channels(ch_ind).name=char(chi.getAttribute('name'));
                            else
                                ch_ind=[processors(processorIndex).channels.number]==str2double(char(chi.getAttribute('number')));
                                if(~any(ch_ind))
                                    ch_ind=length(processors(processorIndex).channels)+1;
                                    processors(processorIndex).channels(ch_ind).number=str2double(char(chi.getAttribute('number')));
                                    processors(processorIndex).channels(ch_ind).name=char(chi.getAttribute('name'));
                                end
                            end
                            processors(processorIndex).channels(ch_ind).record = logical(str2double(char(chi.item(1).getAttribute('record'))));
                            processors(processorIndex).channels(ch_ind).audio = logical(str2double(char(chi.item(1).getAttribute('audio'))));
                            processors(processorIndex).channels(ch_ind).param = logical(str2double(char(chi.item(1).getAttribute('param'))));
                        end
                        if(strcmp(processorName,'Filters/Channel Map') | strcmp(processorName,'Channel Map'))
                            %disp(xProcessor.item(k).getNodeName);
                            if strcmp(xProcessor.item(k).getNodeName, 'CUSTOM_PARAMETERS')
                                info.channel_map_display_name=char(xProcessor.item(k).getAttribute('displayName'));
                                channel_list=xProcessor.item(k).item(1);
                                c_ = 0;
                                for m = 1:channel_list.getChildNodes.getLength-1
                                    if strcmp(channel_list.item(m).getNodeName, 'CH')
                                        ch = str2double(channel_list.item(m).getAttribute('index'));
                                        enabled = str2num(channel_list.item(m).getAttribute('enabled'));
                                        if enabled
                                            if VERBOSE, fprintf("ch %d en %d\n",ch,enabled); end
                                            ch_ind=find([processors(processorIndex).channels.number]==ch);
                                            if c_==0
                                                c_ = c_ + 1;
                                                new_channels = processors(processorIndex).channels(ch_ind);
                                                new_channels.map = ch_ind;
                                            elseif c_>0 && ~ismember(ch_ind,[new_channels.map])
                                                c_ = c_ + 1;
                                                m_ = processors(processorIndex).channels(ch_ind);
                                                m_.map = ch_ind;
                                                new_channels(c_) = m_;
                                            end
                                        end
                                    end
                                end
                                processors(processorIndex).channels = new_channels;

                            elseif strcmp(xProcessor.item(k).getNodeName, 'EDITOR')
                                map=xProcessor.item(k);
                                info.channel_map_display_name=char(xProcessor.item(k).getAttribute('displayName'));
                                for m = 1:map.getChildNodes.getLength-1
                                    %disp(xProcessor.item(k).item(m).getNodeName);
                                    if strcmp(map.item(m).getNodeName,'CHANNEL')
                                        if (~strcmp(char(map.item(m).getAttribute('Reference')),'-1'))
                                            if str2double(char(map.item(m).getAttribute('Enabled')))
                                                ch_ind=[processors(processorIndex).channels.number]==str2double(char(map.item(m).getAttribute('Number')));
                                                processors(processorIndex).channels(ch_ind).map = str2double(char(map.item(m).getAttribute('Mapping')));
                                            end
                                        end
                                    end
                                end
                                
                            end
                        end
                        if(strcmp(processorName,'Sources/Neuropix-PXI') | strcmp(processorName,'Neuropix-PXI'))
                            IsNeuroPix = 1;
                            if strcmp(xProcessor.item(k).getNodeName, 'EDITOR')
                                %map=xProcessor.item(k);
                                %info.channel_map_display_name=char(xProcessor.item(k).getAttribute('displayName'));
                                for m = 1:xProcessor.item(k).getChildNodes.getLength-1
                                    if strcmp(xProcessor.item(k).item(m).getNodeName,'NP_PROBE')
                                        NP_PROBE_Attributes=xProcessor.item(k).item(m).getAttributes();
                                        L = NP_PROBE_Attributes.getLength();
                                        for n=1:L
                                            name = char(NP_PROBE_Attributes.item(n-1).getName);
                                            val = char(NP_PROBE_Attributes.item(n-1).getValue);
                                            val_num = str2num(val);
                                            if ~isempty(val_num)
                                                val=val_num;
                                            end
                                            processors(processorIndex).NP_PROBE_info.(name) = val;
                                        end
                                        for n = 1:xProcessor.item(k).item(m).getChildNodes.getLength-1
                                            if strcmp(xProcessor.item(k).item(m).item(n).getNodeName,'CHANNELS')
                                                CHANNELS_Attributes=xProcessor.item(k).item(m).item(n).getAttributes();
                                            elseif strcmp(xProcessor.item(k).item(m).item(n).getNodeName,'ELECTRODE_XPOS')
                                                X_Attributes=xProcessor.item(k).item(m).item(n).getAttributes();
                                            
                                            elseif strcmp(xProcessor.item(k).item(m).item(n).getNodeName,'ELECTRODE_YPOS')
                                                Y_Attributes=xProcessor.item(k).item(m).item(n).getAttributes();
                                            end
                                        end
                                        L = CHANNELS_Attributes.getLength();
                                        for ii=1:CHANNELS_Attributes.getLength()
                                            name = char(CHANNELS_Attributes.item(ii-1).getName);
                                            bank = str2num(char(CHANNELS_Attributes.item(ii-1).getValue));
                                            xname = char(X_Attributes.item(ii-1).getName);
                                            xval = char(X_Attributes.item(ii-1).getValue);
                                            yname = char(Y_Attributes.item(ii-1).getName);
                                            yval = char(Y_Attributes.item(ii-1).getValue);
                                            n = ifstr2num(name(3:end));
                                            if VERBOSE, fprintf("%s %s %s: %d (%s,%s)\n", name, xname, yname, n, xval, yval); end
                                            processors(processorIndex).channels(n+1).name=name;
                                            processors(processorIndex).channels(n+1).bank=bank;
                                            processors(processorIndex).channels(n+1).number=n;
                                            processors(processorIndex).channels(n+1).record=1;
                                            processors(processorIndex).channels(n+1).param=1;
                                            processors(processorIndex).channels(n+1).audio=1;
                                            %processors(processorIndex).channels(n+1).map=[];
                                            processors(processorIndex).channels(n+1).xpos=str2num(xval);
                                            processors(processorIndex).channels(n+1).ypos=str2num(yval);
                                        end

                                    end
                                end
                            end
                        end
                    end
                end
                
            end
        end
    end
end
for i=1:length(processors)
    if(isempty(processors(i).channels))
        processors(i).num_rec_chans=0;
    else
        processors(i).channels(7).name = upper(processors(i).channels(7).name); % No idea why this is necessary, CRH
        processors(i).num_rec_chans=sum([processors(i).channels.record]);
    end
end
record_nodes = find(strcmp({processors.processorName},'Filters/Record Node') | ...
    strcmp({processors.processorName},'Record Node'));
if isempty(record_nodes)
    record_nodes = find(strcmp({processors.processorName},'Sources/Rhythm FPGA'));
end
if ~isempty(record_nodes)
    info.raw_data.source_inds = record_nodes;
    info.info.SeparateRecordNode=1;
else
    info.raw_data.source_inds=find([processors.num_rec_chans]>0);
    info.info.SeparateRecordNode=0;
end
if(length(info.raw_data.source_inds)==1)
    info.raw_data.source_ind=info.raw_data.source_inds;
    sel=1;
elseif ~isempty(continuous_node_id)
    sel = find([processors(info.raw_data.source_inds).nodeId]==continuous_node_id);
else
    persistent last_directory uselast lastsel
    if(strcmp(last_directory,directory) && uselast)
        sel=lastsel;
    else
        [sel,ok] = listdlg('PromptString','Select raw data source:','ListString',{processors(info.raw_data.source_inds).processorName},'SelectionMode','single','ListSize',[160 20*length(info.raw_data.source_inds)]);
        if(ok)
            last_directory=directory;
            lastsel=sel;
            ButtonName = questdlg('Use this source for all raw data from this run?','', 'Yes');
            switch ButtonName
                case 'Yes'
                    uselast=true;
                otherwise
                    uselast=false;
            end
            
        else
            error('Cancelled. In the future make this fail gracefully.')
        end
    end
end
info.raw_data.source_ind=info.raw_data.source_inds(sel);
info.raw_data.source_name=processors(info.raw_data.source_ind).processorName;
info.raw_data.source_Id=processors(info.raw_data.source_ind).nodeId;
%%
%info.channelNames=[processors(info.raw_data.source_ind).channels.name];
%chan_name_ind=1;
%chan_name_ind=info.raw_data.source_ind;
chan_name_ind=find(arrayfun(@(x)strcmp(x.processorName,'Sources/Rhythm FPGA'),processors));
if isempty(chan_name_ind)
    chan_name_ind=find(arrayfun(@(x)strcmp(x.processorName,'Neuropix-PXI'),processors));
    if isempty(chan_name_ind)
        chan_name_ind=find(arrayfun(@(x)strcmp(x.processorName,'Sources/Neuropix-PXI'),processors));
    end
    info.info.SourceIsNeuropixels=1;
    chan_prefix='CH';
else
    info.info.SourceIsNeuropixels=0;
    chan_prefix='CH';
end
if isempty(chan_name_ind)
    error(['Didn''t find Rhythm FPGA or Neuropix-PXI in signal chain according to ',directory filesep 'settings.xml'])
end
if info.info.SeparateRecordNode
    %info.spike_channels=find(arrayfun(@(x)contains(upper(x.name),chan_prefix),processors(chan_name_ind).channels([processors(info.raw_data.source_ind).channels.param])));
    info.spike_channels=find(arrayfun(@(x)contains(upper(x.name),chan_prefix),processors(chan_name_ind).channels([processors(chan_name_ind).channels.param])));
else
    info.spike_channels=find(arrayfun(@(x)contains(upper(x.name),chan_prefix),processors(chan_name_ind).channels([processors(info.raw_data.source_ind).channels.record])));
end
info.spike_channels_are_adcs=false;
if isempty(info.spike_channels) % || 1 %Quick fix for when you accidentally record spike and ADC channels at once
    adc_channels=find(arrayfun(@(x)~isempty(strfind(x.name,'ADC')),processors(chan_name_ind).channels([processors(info.raw_data.source_ind).channels.record])));
    if ~isempty(adc_channels)
        warning('Spike channels were empty, but there were some ADC channels were found. Assuming ADC channels have spike data on them (LB3 hack)')
        info.spike_channels=adc_channels;
        info.spike_channels_are_adcs=true;
    end
end
for i=1:length(info.spike_channels)
    if(isfield(processors(info.raw_data.source_ind).channels(info.spike_channels(i)),'map'))
        info.spike_channels_mapped(i)=info.spike_channels(i);
        info.spike_channels(i)=processors(info.raw_data.source_ind).channels(info.spike_channels(i)).map;
    end
end
info.num_aux_channels=sum(arrayfun(@(x)~isempty(strfind(x.name,'AUX')),processors(chan_name_ind).channels([processors(info.raw_data.source_ind).channels.record])));
info.num_spike_channels=length(info.spike_channels);

channel_map_ind = find(contains({processors.processorName}, 'Channel Map'));
info.sorted_spikes_channel_map=[];
info.raw_data_channel_map=[];
if length(channel_map_ind)==1
    if any(contains({processors.processorName},'Neuropix-PXI'))
        info.sorted_spikes_channel_map =  [processors(channel_map_ind).channels.map];
    elseif any(contains({processors.processorName},'Rhythm FPGA'))
        info.raw_data_channel_map =  [processors(channel_map_ind).channels.map];
    end
elseif length(channel_map_ind)>1
    error('Two channel maps! Not sure what to do.')
end



% STEP 3: FIND THE AVAILABLE FILES
if(0)
for filenum = 1:length(directory_contents)
    
   if numel(strfind(directory_contents(filenum).name, '.continuous')) > 0
      
       fname = directory_contents(filenum).name;
       
      % disp(fname)
       
       underscore = strfind(fname, '_');
       chstr = strfind(fname, 'CH');
       dot = strfind(fname, '.');
       
       nodeId = str2num(fname(1:underscore-1));
       chNum = str2num(fname(chstr+2:dot-1));
       
      % disp(nodeId)
       
       for n = 1:size(processors,1)
          
           if nodeId == processors{n,1}
              
               processors{n,3} = [processors{n,3} chNum];
               
           end
           
       end
       
   elseif numel(strfind(directory_contents(filenum).name, '.spikes')) > 0
       
       % don't need to do anything here
       
   elseif numel(strfind(directory_contents(filenum).name, '.events')) > 0
       
       info.events_file = directory_contents(filenum).name;
       
   end

end
end

info.electrodes = electrodes;
info.processors = processors;

