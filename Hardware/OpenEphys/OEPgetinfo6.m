function run_info = OEPgetinfo6(OEPdir)
% 
% Revised OE info load using open-ephys-matlab-tools
% source: https://github.com/open-ephys/open-ephys-matlab-tools
%
% SVD 2022-10-12
%
if ~exist(OEPdir,'dir')
    error('Open ephys data directory not found in %s',OEPdir)
end
session = Session(OEPdir);
if isempty(session.recordNodes)
    % try going down one step
    dd=dir(OEPdir);
    if length(dd)>2
        dd(1:2)=[];
        for i=1:length(dd)
            testOEPdir = [dd(i).folder filesep dd(i).name];
            if ~exist([OEPdir filesep 'settings.xml'],'file')
                OEPdir=testOEPdir;
            end
        end
    end
    session = Session(OEPdir);

end


recnode_count = length(session.recordNodes);
rec = struct();
spike_rec=0;
lfp_rec=0;
oespike_rec=0;

for r=1:recnode_count
    recording = session.recordNodes{r}.recordings{1};
    k = recording.continuous.keys;
    rec(r).path = recording.directory;
    rec(r).format = recording.format;
    if ~isempty(recording.info.continuous)
        rec(r).fs = recording.continuous(k{1}).metadata.sampleRate;
        rec(r).startTimestamp = recording.continuous(k{1}).metadata.startTimestamp;
        rec(r).stream_name = recording.info.continuous(1).stream_name;
        rec(r).proc_id = recording.info.continuous(1).recorded_processor_id;
        rec(r).num_channels = recording.info.continuous(1).num_channels;
        rec(r).channels = recording.info.continuous(1).channels;
    else
        rec(r).stream_name = "spikes";
        rec(r).fs = 30000;
        keys = recording.spikes.keys;
        for kk = 1:length(keys)
            einfo=split(keys{kk},'Electrode ');
            electrode = str2num(einfo{2});
            rec(r).spikes(electrode) = recording.spikes(keys{kk});
        end
        rec(r).num_channels=length(keys);
        rec(r).channels=1:rec(r).num_channels;
    end
    
    if contains(rec(r).stream_name, 'AP')
        spike_rec=r;
    elseif contains(rec(r).stream_name, 'LFP')
        lfp_rec=r;
    elseif contains(rec(r).stream_name, 'spikes')
        oespike_rec = r;
    end
    ev = recording.ttlEvents;
    event_keys = ev.keys();
    k = event_keys{find(contains(event_keys,'AP') & contains(event_keys,'Neuropix'),1)};
    rec(r).trial_onsets = [ev(k).sample_number(find(ev(k).state==1))];
    rec(r).trial_offsets = [ev(k).sample_number(find(ev(k).state==0))];
    rec(r).trial_onsets_ts = [ev(k).timestamp(find(ev(k).state==1))];
    rec(r).trial_offsets_ts = [ev(k).timestamp(find(ev(k).state==0))];

end

if ~exist([OEPdir filesep 'settings.xml'],'file')
    dd=dir(OEPdir);
    if length(dd)>2
        dd(1:2)=[];
        for i=1:length(dd)
            OEPdirs{i} = [dd(i).folder filesep dd(i).name];
            has_events(i)=exist([OEPdirs{i},filesep,'all_channels.events'],'file');
        end
        continuous_ind = spike_rec;
        continuous_node_id = str2double(OEPdirs{continuous_ind}(end-2:end));
        old_run_info=get_session_info(OEPdirs{spike_rec},continuous_node_id); %Read info from settings.xml
    end
else     
    old_run_info=get_session_info(OEPdir); %Read info from settings.xml
end

run_info=old_run_info;

run_info.datatype=rec(spike_rec).format;
run_info.OEPdir=OEPdir;
run_info.recordings = rec;
run_info.session = session;
run_info.info = session.recordNodes{spike_rec}.recordings{1}.info;
v_ = split(run_info.info.GUIVersion,'.');
run_info.info.VERSION_ = str2double(join(v_(1:2),'.'));

run_info.spike_rec = spike_rec;
run_info.lfp_rec = lfp_rec;
run_info.oespike_rec = oespike_rec;
run_info.aux_rec = 0;

run_info.json_file = fullfile(rec(spike_rec).path,'structure.oebin');
run_info.json_file_LFP = fullfile(rec(lfp_rec).path,'structure.oebin');
if oespike_rec>0
    run_info.json_file_spikes = fullfile(rec(oespike_rec).path,'structure.oebin');
else
    run_info.json_file_spikes = [];
end

continuous_data_folders = list_open_ephys_binary(run_info.json_file,'continuous');
events_folders = list_open_ephys_binary(run_info.json_file,'events');
run_info.event_ind_TTL = find(contains(events_folders,'TTL'),1); %Find the first neuropixels data stream, use it to get data from
sp = split(events_folders(run_info.event_ind_TTL),'/');
run_info.data_name = sp{1};
run_info.data_ind = find(strcmp(continuous_data_folders,[run_info.data_name '/']));
run_info.event_ind_TCP = find(contains(events_folders,'Network_Events') & contains(events_folders,'TEXT_group'));

run_info.spikefs = rec(spike_rec).fs;
run_info.spikechancount = rec(spike_rec).num_channels;
run_info.spike_channels=zeros(1,run_info.spikechancount);
for c=1:run_info.spikechancount
    run_info.spike_channels(c) = ifstr2num(rec(spike_rec).channels(c).channel_name(3:end));
end
run_info.lfpfs = rec(lfp_rec).fs;
run_info.lfpchancount = rec(lfp_rec).num_channels;

% TODO support for aux channels
run_info.auxfs = rec(spike_rec).fs;
run_info.auxchancount = 0;

run_info.trialcount=length(rec(spike_rec).trial_onsets);

if run_info.trialcount == 0
    error(['Trial (trigger pulse) count is zero. This likely means OEP is not',...
        ' getting the trigger pulses for some reason. Check the connection.'])
end

run_info.electrode='unknown';
if isfield(run_info,'channel_map_display_name')
    if length(run_info.channel_map_display_name)>12 && strcmpi(run_info.channel_map_display_name(1:12),'channel map ')
        run_info.electrode=run_info.channel_map_display_name(13:end);
    elseif length(run_info.channel_map_display_name)>3 && strcmpi(run_info.channel_map_display_name(1:3),'64D')
        run_info.electrode=[run_info.channel_map_display_name(1:3),lower(run_info.channel_map_display_name(4:end))];
    end
end

end
