function exptparams=replicate_behavior_analysis(parmfile,savetodb)
% function exptparams=replicate_behavior_analysis(parmfile,savetodb)
%
%parmfile='/auto/data/daq/Portabello/training2012/Portabello_2012_11_20_TSP_5.m';
%parmfile='/auto/data/daq/Shaggyink/training2019/Shaggyink_2019_04_07_PTD_1.m';
%exptparams=replicate_behavior_analysis(parmfile,savetodb)

%baphy_set_path
if ~exist('savetodb','var'),
    savetodb=0;
end

[pathname,basename]=fileparts(parmfile);

LoadMFile(parmfile);


% if its a RefTar module, create the objects:
if strcmpi(globalparams.Module,'Reference Target')
    exptparams = convert_trial_behavior_structs_to_objects(exptparams);
end
if isfield(exptparams,'Performance')
    exptparams = rmfield(exptparams,'Performance');
end
exptparams.OfflineAnalysis = 1;

HW.params = globalparams.HWparams;
evpfile=[];
if exist([pathname filesep 'tmp'] , 'dir')
    evpfile = [pathname filesep 'tmp' filesep basename '.evp'];
    if ~exist(evpfile,'file'), evpfile=[];end
end
if isempty(evpfile)
    evpfile = [pathname filesep basename '.evp'];
    if ~exist(evpfile,'file')
        evpgetinfo(evpfile); %Run to extract evp file
        if ~exist(evpfile,'file'), error('evp file not found');end
    end
end
% now, for each trial, create the stimulus event and read the evp data.
% Then, call the BehaviorDisplay method of the behavior object with the
% appropriate data (lick and stim events)
% first, the mfile:
[spikecount, auxcount, TotalTrial, spikefs, auxfs] = evpgetinfo(evpfile);
include = ''; 
ThisTrial = 0;
exptparams.ResultsFigure=figure('position',[1 1 900 900]);
if isfield(globalparams,'rawfilecount')
    if TotalTrial>globalparams.rawfilecount
        TotalTrial=globalparams.rawfilecount;
    end
    globalparams=rmfield(globalparams,'rawfilecount');
end

%% Figure out trial start times and if we're going to use them
have_trial_start_data=false;
st_inds=find(arrayfun(@(x)~isempty(strfind(x.Note,'TRIALSTART')),exptevents));
if ~isempty(strfind(exptevents(st_inds(1)).Note,','))
    have_trial_start_data=true;
    TrialStartTimes = nan(1,length(st_inds));
    for i=1:length(st_inds)
        temp=strsep(exptevents(st_inds(i)).Note,',');
        TrialStartTimes(i)=datenum(temp{2},'yyyy-mm-dd HH:MM:SS.FFF');
    end
elseif isfield(exptevents(st_inds(1)),'ClockStartTime') && ~isempty(exptevents(st_inds(1)).ClockStartTime)
    have_trial_start_data=true;
    TrialStartTimes=[exptevents(st_inds).ClockStartTime];
end
if have_trial_start_data
    TrialStartTimes=TrialStartTimes-TrialStartTimes(1);
    TrialStartTimes=arrayfun(@(x)sum(datevec(x).*[0 0 0 60 1 1/60]),TrialStartTimes); %convert to min
end
if strcmp(getfield(get(exptparams.BehaveObject,'DisplayParams'),'plot_performance_over'),'trial_start_time')
    if have_trial_start_data
        x_as_time=true;
    else
        x_as_time=false;
        %warning('This mfile doesn''t have trial start time data saved, so behvior can''t be plotted by time as requested. Plotting by index.')
    end
else
    x_as_time=false;
end


%% Loop over trials
for cnt1 = 1:TotalTrial
    % Stim events are between TrialStart and Last PostStimSilence events:
    [~,~,~,~,StimStart] = evtimes(exptevents,'TrialStart*',cnt1);
    [~,~,~,~,StimEnds] = evtimes(exptevents,'PostStimSilence*',cnt1);
    [~,~,~,~,TrialEnd] = evtimes(exptevents,'TRIALSTOP',cnt1);

    % includes Pre- And PostStimSilence events
    StimEvents = exptevents(StimStart+1:StimEnds(end));
    exptparams.temporary.BehaviorEvents = exptevents(StimEnds(end)+1:TrialEnd);
    pas=0; 
    if ~isempty(strfind(StimEvents(end).Note,'Target')) && isempty(strfind(StimEvents(end).Note,include)) ...
            && ~isempty(include)
        pas=1;
    end
    if ~pas
        ThisTrial=ThisTrial+1;
        [rS,STrialIdx,Lick,ATrialIdx]=evpread(evpfile, [], 1,cnt1);
        % why are they sometimes empty??
        if isempty(Lick), warning(['empty ' num2str(cnt1)]);
            Lick = zeros(ceil(1+exptevents(StimEnds(end)).StopTime*auxfs),1);
        end
        exptparams = PerformanceAnalysis(exptparams.BehaveObject, HW, StimEvents, ...
            globalparams, exptparams, ThisTrial, Lick);
        % disabled display per Block
        try
            num_non_cue=sum(~[exptparams.TrialParams(1:cnt1).CueSeg]);
            last_is_cue=exptparams.TrialParams(cnt1).CueSeg;
        catch
            num_non_cue=cnt1;
            last_is_cue=0;
        end
        if (num_non_cue>0 && ~mod(num_non_cue,exptparams.TrialBlock) && ~last_is_cue) || cnt1==TotalTrial || cnt1==1
            if x_as_time
                starttimes=TrialStartTimes(1:cnt1);
            else
                starttimes=[];
            end
            exptparams = BehaviorDisplay(exptparams.BehaveObject, HW, ...
                                         StimEvents, globalparams, ...
                                         exptparams, ThisTrial, Lick, [],...
                                         starttimes);
            set(gcf,'Name',basename);
        elseif x_as_time
            RecentIndex = max(1, ThisTrial-exptparams.TrialBlock+1):ThisTrial;
            exptparams.Performance(end-1).RecentStartTimeMean=mean(TrialStartTimes(RecentIndex));
        end
    end
end
exptparams.TotalTrials = ThisTrial;
exptparams = BehaviorDisplay(exptparams.BehaveObject, HW, StimEvents, globalparams, ...
    exptparams, ThisTrial, [], []);
drawnow;

if savetodb,
    disp('saving perf data to database');
    [Parameters, Performance] = PrepareDatabaseData ( globalparams, exptparams);
    dbWriteData(globalparams.rawid, Parameters, 0, 0);  % this is parameter and dont keep previous data
    dbWriteData(globalparams.rawid, Performance, 1, 0); % this is performance and dont keep previous data
    if isfield(Performance,'HitRate') && isfield(Performance,'Trials')
        sql=['UPDATE gDataRaw SET corrtrials=',num2str(round(Performance.HitRate*Performance.Trials)),',',...
            ' trials=',num2str(Performance.Trials),' WHERE id=',num2str(globalparams.rawid)];
        mysql(sql);
    elseif isfield(Performance,'Hit') && isfield(Performance,'FalseAlarm')
        sql=['UPDATE gDataRaw SET corrtrials=',num2str(Performance.Hit(1)),',',...
            ' trials=',num2str(Performance.FalseAlarm(2)),' WHERE id=',num2str(globalparams.rawid)];
        mysql(sql);
    end
    
    SaveBehaviorFigure(globalparams,exptparams);
    
    disp('DONE!');
else
    disp('skipping save to db');
end


if 0,
    dbopen;
    sql=['SELECT * FROM gDataRaw where id>=95696 and behavior="active" and not(bad)',...
         ' and parmfile like "por%TSP%" ORDER BY id'];
    rawdata=mysql(sql);
    for ii=1:length(rawdata),
        close all
        parmfile=[rawdata(ii).resppath rawdata(ii).parmfile];
        fprintf('processing %d : %s\n',ii,parmfile);
        replicate_behavior_analysis(parmfile,1);
    end
end

if 0
   [success, msg, mid] = copyfile(parmfile,strrep(parmfile,'.m','_bck.m'));
   if ~success
       error(msg);
   end
   b=LoadMFile(parmfile);
   %b.globalparams.mfilename=strrep(b.globalparams.mfilename,'.m','_DI_update.m');
   b.exptparams.Performance = exptparams.Performance;
   update_baphy_commit_hash=0;
   force_overwrite=1;
   WriteMFile(b.globalparams,b.exptparams,b.exptevents,force_overwrite,update_baphy_commit_hash); 
    
end