function dblink_phy(spkfile,s,channum,sortidxs,extras,gSingleRawFields,goodtrials)
%
% after saving spike file from phy, record information in celldb
%   for all sorted units.
%   pulls relevant meta data out of extras, phi (via gSingleRawFields), and goodtrials
%   and then calls dblink_sorted to actuall save to the database
%
% inputs:
%   rawid of sorted file
%   spkfile - path+name of spk.mat file
%   s - sortinfo (meska format cell array)
%   channum - electrode channel associated with each entry in s
%   extras - from spk.mat file (meska format)
%   gSingleRawFields - stats from phy
%   goodtrials - string defining vector listing which trials are good (meska format--empty==all trials)
%
% deals with multi-runclass expts
%
% created SVD 2018-06-20  (simplified from matchcell2file)
%
dbopen;
global chanstr

[~,fname]=fileparts(spkfile) ;
stimfile=strrep(fname,'.spk','');

sql=['SELECT * FROM gDataRaw WHERE parmfile like "%',stimfile,'.m"'];
rawdata=mysql(sql);

if isempty(rawdata)
    sql=['SELECT * FROM gDataRaw WHERE parmfile like "%',stimfile,'%"'];
    rawdata=mysql(sql);
end

[respfile,resppath]=basename(spkfile);

if isempty(rawdata)
   error('rawid=%d not found',rawid);
end
rawid=rawdata.id;

% cool, found a matching raw file.
% get info for relevant raw data files
stimpath=rawdata.resppath;
stimfile=rawdata.parmfile;
siteid=rawdata.cellid;

sql=['SELECT * FROM gData WHERE rawid=',num2str(rawid),...
     ' AND name="Ref_Duration"'];
datadata=mysql(sql);
if ~isempty(datadata) && ~isempty(datadata(1).value)
    stimspeedid=datadata(1).value;
else
    stimspeedid=0;
end

% figure out stim SNR, if specified
sql=['SELECT * FROM gData WHERE rawid=',num2str(rawid),...
    ' AND name="Ref_SNR"'];
snrdata=mysql(sql);
if ~isempty(snrdata) && ~isempty(snrdata.value)
    stimsnr=snrdata.value;
else
    stimsnr=1000;
end

if ~isfield(extras,'torcList')
    delay = 0;
else
    delay = get(extras.torcList.tag,'Onset');
end

% get info abou the site
sql=['SELECT * FROM gCellMaster WHERE siteid="',siteid,'"'];
site=mysql(sql);

if isempty(site)
   error('%s: site %s doesn''t exist in celldb\n',...
         mfilename,siteid);
end

% add conversion to char array, CRH 7-20-2018. Not sure why necessary...
areas=strsep(char(site.area),',',1);

if ~isempty(s{1}(1).sorter)
    addedby=s{1}(1).sorter;
else
    addedby='david';
end

spikedata=struct();
cellcount=0;
for ci=1:length(s)
   for ui=1:length(s{ci})
      if ~isempty(s{ci}(ui).Template)
         cellcount=cellcount+1;
         spikedata(cellcount).channum = channum(ci);
         spikedata(cellcount).unit = ui;
         spikedata(cellcount).sortidx = sortidxs(ci);
         
         spikedata(cellcount).spikecount=size(s{ci}(ui).unitSpikes,2);
         spikedata(cellcount).cellid=sprintf('%s-%s%d',siteid,chanstr{channum(ci)},ui);
         if channum(ci) <= length(areas)
            spikedata(cellcount).area=areas{channum(ci)};
         else
            spikedata(cellcount).area='';
         end

         % generic info for all units
         spikedata(cellcount).resplen=extras.npoint;
         spikedata(cellcount).repcount=extras.sweeps;
         spikedata(cellcount).stimfilecrf=delay;
         spikedata(cellcount).respfmtcode=0;
         spikedata(cellcount).respfilefmt='kilosort';
         spikedata(cellcount).stimfilefmt='par';
         spikedata(cellcount).stimfmtcode=1;
         spikedata(cellcount).stimspeedid=stimspeedid;
         spikedata(cellcount).stimsnr=stimsnr;
         spikedata(cellcount).addedby=addedby;
         spikedata(cellcount).stimpath=stimpath;
         spikedata(cellcount).stimfile=stimfile;
         spikedata(cellcount).path=resppath;
         spikedata(cellcount).respfile=respfile;
         spikedata(cellcount).goodtrials=goodtrials{1};
         %      ' info="',mfilename,'",',...

         % metadata for gSingleRawData
         SRFs=fieldnames(gSingleRawFields);
         for k=1:length(SRFs)
            if ~strcmp(SRFs{k}, 'unit_start')
               if iscell(gSingleRawFields.(SRFs{k}){ci})
                   spikedata(cellcount).gSingleRawFields.(SRFs{k})=...
                  gSingleRawFields.(SRFs{k}){ci}{ui};
               else
                   spikedata(cellcount).gSingleRawFields.(SRFs{k})=...
                  gSingleRawFields.(SRFs{k}){ci}(ui);
               end
            end
         end
      end
   end
end

dblink_sorted(rawid,spikedata,gSingleRawFields);

