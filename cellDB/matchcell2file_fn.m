function matchcell2file_fn(destin,source,s,chanNum,extras,gSingleRawFields,goodtrials)
%
% after saving spike file, record information in celldb
%   figure out cell identities and establish appropriate links
%   between gDataRaw --> gSingleCell via gSingleRaw and sCellFile
%
% created SVD 2005-10-01
%
global chanstr
for i=1:length(destin)
    [~,fname{i}]=fileparts(destin{i}) ;
    fname{i}=strrep(fname{i},'.spk','');
end

if(length(fname)>1)
    onefile=false;
else
    onefile=true;
end

dbopen;

sql=['SELECT * FROM gDataRaw WHERE parmfile like "%',basename(fname{1}),'.m"'];
rawdata1=mysql(sql);

if isempty(rawdata1),
    sql=['SELECT * FROM gDataRaw WHERE parmfile like "%',basename(fname{1}),'%"'];
    rawdata1=mysql(sql);
end

if onefile,
    rawcount=1;
    rawdata2=[];
else
    rawcount=2;
    sql=['SELECT * FROM gDataRaw WHERE parmfile like "%',basename(fname{2}),'%"'];
    rawdata2=mysql(sql);
end

if length(rawdata1)==1,
    % cool, found a matching raw file.
    
    % check for multi-runclass
    if ~isempty(findstr(rawdata1.runclass,'_')),
        runclasses=strrep(rawdata1.runclass,'_','","');
        rcdata=mysql(['SELECT * FROM gRunClass where gRunClass.name in ("',runclasses,'")']);
        runclassids=unique(cat(2,rcdata.id));
    else
        runclassids=rawdata1.runclassid;
    end
    if length(runclassids)>1,
        fprintf('Mulitple runclassids: ');
        fprintf('%d ',runclassids);
        fprintf('\n');
    end
    
    % delete any existing entries from sCellFile so we can have a clean
    % start
    sql=['SELECT id,cellid FROM sCellFile WHERE rawid=',num2str(rawdata1.id)];
    cfd=mysql(sql);
    if ~isempty(cfd)
       yn=input(sprintf('Delete %d existing sCellFile/gSingleRaw entries and regenerate ([y]/n)? ',length(cfd)),'s');
       if isempty(yn) || yn(1)~='n'
          sql=['DELETE FROM sCellFile WHERE rawid=',num2str(rawdata1.id)];
          mysql(sql);
          sql=['DELETE FROM gSingleRaw WHERE rawid=',num2str(rawdata1.id)];
          mysql(sql);
          
       end
    end
    
    for ci=1:length(chanNum) % next, figure out how many cells were identified
        spikecount=zeros(size(s{ci}));
        cellids={};
        for ii=1:size(spikecount,1),
            spikecount(ii)=size(s{ci}(ii).unitSpikes,2);
            % now always assume there will be >1 channels to avoid past
            % cellid naming problems.
            %if extras.numChannels>1,
            cellids{ii}=sprintf('%s-%s%d',rawdata1.cellid,chanstr{chanNum(ci)},ii);
            %else
            %    cellids{ii}=sprintf('%s-%d',siteid,ii);
            %end
        end
        unitcount=max(find(sum(spikecount,2)>0));
        
        % disabled delete cell feature.  don't want to get rid of cells
        % in case later file has more/less
        if 0
            % are there already more cells in db than measured units?
            sql=['SELECT id,cellid FROM gSingleCell WHERE masterid=',...
                num2str(rawdata1.masterid),...
                ' AND channum=',chanNum,' ORDER BY id'];
            singledata=mysql(sql);
            if length(singledata) > unitcount,
                disp('too many pre-existing units in db!');
                yn=input('delete extras ([y]/n)? ','s');
                if isempty(yn) | yn(1)~='n',
                    for ii=unitcount+1:length(singledata),
                        sql=['DELETE FROM sCellFile WHERE singleid=', ...
                            num2str(singledata(ii).id)];
                        mysql(sql);
                        sql=['DELETE FROM gSingleRaw WHERE singleid=', ...
                            num2str(singledata(ii).id)];
                        mysql(sql);
                        sql=['DELETE FROM gSingleCell WHERE id=',num2str(singledata(ii).id)];
                        mysql(sql);
                    end
                end
            end
        end
        
        % get info for relevant raw data files
        siteid=rawdata1.cellid;
        [rawdata,site,celldata,rcunit,rcchan]=dbgetsite(siteid);
        rawids=cat(1,rawdata.id);
        singleids=cat(1,celldata.id);
        
        % figure out rawid (==row in rcunit, rcchan)
        if ~isempty(rawdata2),
            r1=[find(rawdata1.id==rawids);
                find(rawdata2.id==rawids)];
        else
            r1=find(rawdata1.id==rawids);
        end
        
        % find existing units with channel matched to current unit
        if extras.numChannels>1
            cc=nanmin(rcchan);
            cc2=nanmax(rcchan);
            if any(cc~=cc2)
                warning('channel mismatch in same unit for different files!')
                %keyboard
            end
            if any(nanmin(rcunit)~=nanmax(rcunit))
                warning('unit mismatch in same unit for different files!')
                %keyboard
            end
            cc=find(cc==chanNum(ci));
        else
            cc=1:size(rcchan,2);
        end
        rawdata=rawdata(r1);
        rawids=rawids(r1);
        rcunit=rcunit(r1,:);
        rcchan=rcchan(r1,:);
        
        % kludge to deal with units that don't have spikes?
        if length(cc)>length(spikecount),
            spikecount((length(spikecount)+1):length(cc),:)=0;
        end
        
        % two possibilities: cells either exist or don't exist yet in db
        while length(cc)<unitcount,
            rcunit=[rcunit zeros(length(r1),1)];
            rcchan=[rcchan zeros(length(r1),1)];
            singleids=[singleids;-1];
            cc=[cc length(singleids)];
            fprintf('adding cell %s\n',cellids{length(cc)});
        end
        if length(cc)>unitcount,
           cc=cc(1:unitcount);
        end
        
        rcchan(:,cc)=chanNum(ci);
        
        for ii=1:length(cc),
            rcunit(:,cc(ii))=ii;
        end
        
        % save mapping between channels and cells
        dbsetsite(siteid,rcunit,rcchan,singleids,rawids);
        
        sql=['SELECT id,cellid,area,channum,unit',...
            ' FROM gSingleCell WHERE masterid=',...
            num2str(rawdata1.masterid),' ORDER BY cellid'];
        singledata=mysql(sql);
        singleids=cat(1,singledata.id);
        singleunits=cat(1,singledata.unit);
        singlechannums=cat(1,singledata.channum);
        singleareas=cell(length(singledata),1);
        [singleareas{:}]=deal(singledata.area);
        
        % save sorted cell info in sCellFile
        % respfilefmt="meska"; respfmtcode=2; addedby=sorter;
        % info=mfilename; cellid=cellids{}
        % spikes?
        % from gDataRaw etc: runclassid, masterid, singleid, singlerawid
        respfile={};resppath={};
        for i=1:length(destin)
            [respfile{i},resppath{i}] = basename([destin{i}]);
        end
        stimfile={};stimpath={};
        for i=1:length(destin)
            [stimfile{1},stimpath{1}] = basename(source{i});
        end
        if ~isempty(s{1}(1).sorter),
            addedby=s{1}(1).sorter;
        else
            addedby='david';
        end
        if rawcount==2,
            extras(2)=extras2;
        end
        
        for rr=1:rawcount
            
            sql=['SELECT * FROM gData WHERE rawid=',num2str(rawids(rr)),...
                ' AND name="Ref_Duration"'];
            datadata=mysql(sql);
            if ~isempty(datadata) && ~isempty(datadata(1).value),
                stimspeedid=datadata(1).value;
            else
                stimspeedid=0;
            end
            
            % figure out stim SNR, if specified
            sql=['SELECT * FROM gData WHERE rawid=',num2str(rawids(rr)),...
                ' AND name="Ref_SNR"'];
            snrdata=mysql(sql);
            if ~isempty(snrdata) && ~isempty(snrdata.value),
                stimsnr=snrdata.value;
            else
                stimsnr=1000;
            end
            
            if ~isfield(extras(rr),'torcList')
                delay = 0;
            else
                delay = get(extras(rr).torcList.tag,'Onset');
            end
            for ii=cc
                newsingleid=singleids(singlechannums==rcchan(1,ii) &...
                    singleunits==rcunit(1,ii));
                newarea=singleareas{singleids==newsingleid};
                % figure out singlerawid for this cell/raw combo
                sql=['SELECT * FROM gSingleRaw WHERE rawid=',num2str(rawids(rr)),...
                    ' AND singleid=',num2str(newsingleid)];
                singlerawdata=mysql(sql);
                if ~isempty(singlerawdata),
                    singlerawid=singlerawdata.id;
                else
                    singlerawid=-1;
                end
                
                if spikecount(cc==ii,rr) ==0,
                    sql=['DELETE FROM sCellFile',...
                        ' WHERE singleid=',num2str(newsingleid),...
                        ' AND rawid=',num2str(rawids(rr)),...
                        ' AND respfmtcode=0'];
                    mysql(sql);
                else
                    % record iso pct, unit type, etc.
                    SRFs=fieldnames(gSingleRawFields);
                    sql='UPDATE gSingleRaw set ';
                        for k=1:length(SRFs)
                            if(~isnan(gSingleRawFields.(SRFs{k}){ci}(rcunit(rr,ii))))%don't update fields whose values are NaN
                                sql=[sql,SRFs{k},'=',num2str(gSingleRawFields.(SRFs{k}){ci}(rcunit(rr,ii))),', '];
                            end
                        end
                        if(length(sql)>22)
                            %if any fields were not NaN, update fields
                            sql=[sql(1:end-2),' WHERE id=',num2str(singlerawid)];    
                            mysql(sql);
                        end
                    
                    
                    rcc=0;
                    for thisrc=runclassids,
                        rcc=rcc+1;
                        if length(runclassids)>1,
                            respvarname=sprintf('Reference%d',rcc);
                        else
                            respvarname='Reference';
                        end
                        
                        % check to see if sorted entry already exists for this cell/rawid
                        sql=['SELECT * FROM sCellFile',...
                            ' WHERE singleid=',num2str(newsingleid),...
                            ' AND rawid=',num2str(rawids(rr)),...
                            ' AND runclassid=',num2str(thisrc),...
                            ' AND respfmtcode=0'];
                        cellfiledata=mysql(sql);
                        
                        if isempty(cellfiledata),
                            sqlinsert('sCellFile',...
                                'cellid',cellids{rcunit(rr,ii)},...
                                'masterid',rawdata(rr).masterid,...
                                'rawid',rawids(rr),...
                                'runclassid',thisrc,...
                                'path',resppath{rr},...
                                'resplen',extras(rr).npoint,...
                                'repcount',extras(rr).sweeps,...
                                'respfile',respfile{rr},...
                                'respvarname',respvarname,...
                                'respfiletype',rcunit(rr,ii),...
                                'respfmtcode',0,...
                                'respfilefmt','meska',...
                                'stimfile',stimfile{rr},...
                                'stimfilecrf',delay,...
                                'stimfilefmt','par',...
                                'stimfmtcode',1,...
                                'stimspeedid',stimspeedid,...
                                'stimsnr',stimsnr,...
                                'addedby',addedby,...
                                'info',mfilename,...
                                'stimpath',stimpath{rr},...
                                'spikes',spikecount(cc==ii,rr),...%'spikes',length(spksav1{rcunit(rr,ii)}),...
                                'singleid',newsingleid,...
                                'area',newarea,...
                                'channum',rcchan(rr,ii),...
                                'unit',rcunit(rr,ii),...
                                'singlerawid',singlerawid,...
                                'goodtrials',char(goodtrials{rcunit(rr,ii)}));
                        else
                            sql=['UPDATE sCellFile SET',...
                                ' masterid=',num2str(rawdata(rr).masterid),',',...
                                ' runclassid=',num2str(thisrc),',',...
                                ' path="',resppath{rr},'",',...
                                ' resplen=',num2str(extras(rr).npoint),',',...
                                ' repcount=',num2str(extras(rr).sweeps),',',...
                                ' respfile="',respfile{rr},'",',...
                                ' respvarname="',respvarname,'",',...
                                ' respfiletype=',num2str(rcunit(rr,ii)),',',...
                                ' respfmtcode=0,',...
                                ' respfilefmt="meska",',...
                                ' stimfile="',stimfile{rr},'",',...
                                ' stimfilecrf=',num2str(delay),',',...
                                ' stimfilefmt="par",',...
                                ' stimfmtcode=1,',...
                                ' stimspeedid=',num2str(stimspeedid),',',...
                                ' stimsnr=',num2str(stimsnr),',',...
                                ' addedby="',addedby,'",',...
                                ' info="',mfilename,'",',...
                                ' stimpath="',stimpath{rr},'",',...
                                ' spikes=',num2str(spikecount(cc==ii,rr)),',',...%' spikes=',num2str(length(spksav1{rcunit(rr,ii)})),',',...
                                ' singleid=',num2str(newsingleid),',',...
                                ' area="',newarea,'",',...
                                ' singlerawid=',num2str(singlerawid),',',...
                                ' channum=',num2str(rcchan(rr,ii)),',',...
                                ' unit=',num2str(rcunit(rr,ii)),',',...
                                ' goodtrials="',goodtrials{rcunit(rr,ii)},'"',...
                                ' WHERE id=',num2str(cellfiledata.id)];
                            mysql(sql);
                        end
                    end
                end
            end
            
            sql=['UPDATE gDataRaw SET matlabfile="',resppath{rr},respfile{rr},'"',...
                ' WHERE id=',num2str(rawids(rr))];
            mysql(sql);
        end
    end
elseif length(rawdata1)>1,
    disp('uh oh. multiple matches in celldb');
    keyboard
    
else
    warning('oh no! no matches found in celldb');
end


