% function dbWriteMetaData(cellid,t,append)
%
% cellid - name of cell (entry in gSingleCell)
% t - structure with each field either a scalar, matrix or string
% append - if 1, append contents of t to existing structure
%          (but overwrite any matching field names, default 0)
%
% created CRH 2018-06-15
%

function dbWriteMetaData(cellid,t,append)
dbopen;
if ~exist('append','var')
   append=0;
end

fn=fieldnames(t);

if append
    
   tnew=dbReadMetaData(cellid);

   for ii=1:length(fn)
      tnew.(fn{ii})=t.(fn{ii});
   end
   t=tnew;
end

meta_data_json = strrep(jsonencode(t), '''', '\''');
sql = ['UPDATE gSingleCell set meta_data=''', meta_data_json,''' WHERE cellid="',cellid,'"'];

mysql(sql);

fprintf('Saved %d meta_data parameters for cellid %s\n',length(fn),cellid);

