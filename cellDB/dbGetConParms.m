% function r=dbGetConParms()
% 
% returns a struct with all the global celldb connection settings.
%
% created SVD 2018-01-16 for migration over the separate Narf/NEMS database
%
function r=dbGetConParms()

global DBISOPEN DBUSER DB_ENABLED
global DB_USER DB_SERVER DB_PASSWORD DB_NAME

r=struct('DBISOPEN',DBISOPEN, ...
   'DBUSER',DBUSER,...
   'DB_ENABLED',DB_ENABLED,...
   'DB_USER',DB_USER,... 
   'DB_SERVER',DB_SERVER,... 
   'DB_PASSWORD',DB_PASSWORD,... 
   'DB_NAME',DB_NAME);




