function dblink_sorted(rawid,spikedata,append_units)
%
% after saving spike file, record information in celldbx
%   for all sorted units.
%
%   figure out cell identities and establish appropriate links
%   between gDataRaw --> gSingleCell via gSingleRaw and sCellFile
%
% inputs:
%   rawid of sorted file
%   spkfile - path+name of spk.mat file
%   s - sortinfo (meska format cell array)
%   channum - electrode channel associated with each entry in s
%   extras - from spk.mat file (meska format)
%   gSingleRawFields - stats from phy
%   goodtrials - string defining vector listing which trials are good (meska format--empty==all trials)
%
% deals with multi-runclass expts, saves sorting metadata drawn from extras, phi (via gSingleRawFields), and goodtrials.
%
% created SVD 2018-06-20  (simplified from matchcell2file)
%
% to do: break out spikedata definition and other extras into a wrapper so that this can be called without wonky parameters

dbopen;
global chanstr

sql=['SELECT * FROM gDataRaw WHERE id=',num2str(rawid)];
rawdata=mysql(sql);

if isempty(rawdata)
   error('rawid=%d not found',rawid);
end
if ~exist('append_units','var')
   append_units=0;
end

% cool, found a matching raw file.
% get info for relevant raw data files
siteid=rawdata.cellid;

% check for multi-runclass
if ~isempty(findstr(rawdata.runclass,'_'))
    runclasses=strrep(rawdata.runclass,'_','","');
    rcdata=mysql(['SELECT * FROM gRunClass where gRunClass.name in ("',runclasses,'")']);
    runclassids=unique(cat(2,rcdata.id));
else
    runclassids=rawdata.runclassid;
end
if length(runclassids)>1
    fprintf('Mulitple runclassids: ');
    fprintf('%d ',runclassids);
    fprintf('\n');
end

% get info abou the site
sql=['SELECT * FROM gCellMaster WHERE siteid="',siteid,'"'];
site=mysql(sql);

if isempty(site)
   error('%s: site %s doesn''t exist in celldb',mfilename,siteid);
end

% delete any existing entries from sCellFile so we can have a clean start
cellcount=length(spikedata);

% this should have already been called for all current rawids by 
% UTkilosort_load_completed_job
%dblink_purge_rawids(rawid);

% sql=['SELECT id,cellid FROM sCellFile WHERE rawid=',num2str(rawid)];
% cfd=mysql(sql);
% if ~isempty(cfd)
%    yn=input(sprintf('Delete %d existing sCellFile/gSingleRaw entries and regenerate ([y]/n)? ',length(cfd)),'s');
%    if isempty(yn) || yn(1)~='n'
%       sql=['DELETE FROM sCellFile WHERE rawid=',num2str(rawid)];
%       mysql(sql);
%       sql=['DELETE FROM gSingleRaw WHERE rawid=',num2str(rawid)];
%       mysql(sql);
%       sql=['DELETE gSingleCell FROM gSingleCell LEFT JOIN gSingleRaw ON gSingleCell.id=gSingleRaw.singleid WHERE gSingleCell.cellid like "',siteid,'%" AND gSingleRaw.singleid is null'];
%       mysql(sql);
%    end
% end

for ii=1:cellcount

   % check if single cell exists
   sql=['SELECT * FROM gSingleCell WHERE cellid="',spikedata(ii).cellid,'"'];
   sdata=mysql(sql);

   % add it (or update?) if necessary
   if isempty(sdata)
      % create new cell
      [aff,singleid]=sqlinsert('gSingleCell',...
                             'siteid',siteid,...
                             'cellid',spikedata(ii).cellid,...
                             'masterid',site.id,...
                             'penid',site.penid,...
                             'unit',spikedata(ii).unit,...
                             'channel',chanstr{spikedata(ii).channum},...
                             'channum',spikedata(ii).channum,...
                             'area',spikedata(ii).area,...
                             'addedby',spikedata(ii).addedby,...
                             'info','dblink_sorted.m');
      if aff
         fprintf('created cell %s (%d)\n',spikedata(ii).cellid,singleid);
      end
   else
      % do nothing
      singleid=sdata.id;
      fprintf('cell %s (%d) already exists\n',spikedata(ii).cellid,singleid);
   end

   % check if gSingleRaw exists
   sql=['SELECT * FROM gSingleRaw',...
        ' WHERE singleid=',num2str(singleid),...
        ' AND rawid=',num2str(rawid)];
   srdata=mysql(sql);

   % add or update gSingleRaw as needed
   if isempty(srdata)
       % doesn't exist, add it
       [aff,singlerawid]=sqlinsert('gSingleRaw',...
                                   'cellid',spikedata(ii).cellid,...
                                   'singleid',singleid,...
                                   'masterid',site.id,...
                                   'penid',site.penid,...
                                   'rawid',rawid,...
                                   'unit',spikedata(ii).unit,...
                                   'channum',spikedata(ii).channum,...
                                   'addedby',spikedata(ii).addedby,...
                                   'info',mfilename);
      if aff
         fprintf('created cell %s link to rawid %d\n',spikedata(ii).cellid,rawid);
      end
   elseif length(srdata)==1
      singlerawid=srdata.id;
   else
      error('two gSingleRaw entries');
   end

   % record iso pct, unit type, etc. in gSingleRaw
   SRFs=fieldnames(spikedata(ii).gSingleRawFields);
   sql='UPDATE gSingleRaw set ';
   for k=1:length(SRFs)
       if ~isnan(spikedata(ii).gSingleRawFields.(SRFs{k}))
          %don't update fields whose values are NaN
          if ischar(spikedata(ii).gSingleRawFields.(SRFs{k}))
              sql=[sql,SRFs{k},'=''',spikedata(ii).gSingleRawFields.(SRFs{k}),''', '];
          else
              sql=[sql,SRFs{k},'=',num2str(spikedata(ii).gSingleRawFields.(SRFs{k})),', '];
          end
       end
   end
   %if(length(sql)>22)
   %    %if any fields were not NaN, update fields
   %    sql=[sql(1:end-2),' WHERE id=',num2str(singlerawid)];
   %    mysql(sql);
   %end
   sql=[sql,...
        ' cellid="',spikedata(ii).cellid,'",',...
        ' unit=',num2str(spikedata(ii).unit),',',...
        ' channum=',num2str(spikedata(ii).channum),...
        ' WHERE id=',num2str(singlerawid)];
   mysql(sql);


   % add one sCellFile entry per runclass id (dealing with multi-runclass expts)
   rcc=0;
   for thisrc=runclassids
       rcc=rcc+1;
       if length(runclassids)>1
           respvarname=sprintf('Reference%d',rcc);
       else
           respvarname='Reference';
       end

       % check to see if sorted entry already exists for this cell/rawid
       sql=['SELECT * FROM sCellFile',...
           ' WHERE singleid=',num2str(singleid),...
           ' AND rawid=',num2str(rawid),...
           ' AND runclassid=',num2str(thisrc),...
           ' AND respfmtcode=',num2str(spikedata(ii).respfmtcode)];
       cellfiledata=mysql(sql);

       % add or update sCellFile as needed
       if isempty(cellfiledata)
           [aff,cellfileid]=sqlinsert('sCellFile',...
              'cellid',spikedata(ii).cellid,...
              'singleid',singleid,...
              'runclassid',thisrc,...
              'respfmtcode',spikedata(ii).respfmtcode,...
              'masterid',site.id,...
              'rawid',rawid);
           if aff
              fprintf('created sCellFile entry %s link to rawid %d\n',...
                 spikedata(ii).cellid,rawid);
           end
       else
           cellfileid=cellfiledata.id;
       end
       
       sql=['UPDATE sCellFile SET',...
          ' masterid=',num2str(site.id),',',...
          ' runclassid=',num2str(thisrc),',',...
          ' path="',spikedata(ii).path,'",',...
          ' resplen=',num2str(spikedata(ii).resplen),',',...
          ' repcount=',num2str(spikedata(ii).repcount),',',...
          ' respfile="',spikedata(ii).respfile,'",',...
          ' respvarname="',respvarname,'",',...
          ' respfiletype=',num2str(spikedata(ii).unit),',',...
          ' respfmtcode=',num2str(spikedata(ii).respfmtcode),',',...
          ' respfilefmt="',spikedata(ii).respfilefmt,'",',...
          ' stimfile="',spikedata(ii).stimfile,'",',...
          ' stimfilecrf=',num2str(spikedata(ii).stimfilecrf),',',...
          ' stimfilefmt="',spikedata(ii).stimfilefmt,'",',...
          ' stimfmtcode=',num2str(spikedata(ii).stimfmtcode),',',...
          ' stimspeedid=',num2str(spikedata(ii).stimspeedid),',',...
          ' stimsnr=',num2str(spikedata(ii).stimsnr),',',...
          ' addedby="',spikedata(ii).addedby,'",',...
          ' info="',mfilename,'",',...
          ' stimpath="',spikedata(ii).stimpath,'",',...
          ' spikes=',num2str(spikedata(ii).spikecount),',',...
          ' singleid=',num2str(singleid),',',...
          ' area="',spikedata(ii).area,'",',...
          ' singlerawid=',num2str(singlerawid),',',...
          ' channum=',num2str(spikedata(ii).channum),',',...
          ' unit=',num2str(spikedata(ii).unit),',',...
          ' sortidx=',num2str(spikedata(ii).sortidx),',',...
          ' goodtrials="',spikedata(ii).goodtrials,'"',...
          ' WHERE id=',num2str(cellfileid)];
       mysql(sql);
   end
end

sql=['UPDATE gDataRaw SET matlabfile="',spikedata(ii).path,spikedata(ii).respfile,'"',...
     ' WHERE id=',num2str(rawid)];
mysql(sql);
