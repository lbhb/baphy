% function [rawdata,site,celldata,rcunit,rcchan]=dbcheck_site_integrity(siteid)
%
% siteid is string name of site. 
% 
% returns: pass = 1 if everything seems to be ok, 0 if not
%
% created SVD 2005-09-01 -- ripped off dbgetscellfile
%
function pass=dbcheck_site_integrity(siteid)

%global chanstr

dbopen;

pass=1;

sql=['SELECT * FROM gCellMaster WHERE siteid="',siteid,'"'];
site=mysql(sql);

if isempty(site)
   fprintf('%s: site %s doesn''t exist in celldb\n',...
           mfilename,siteid);
   pass=0;
   return
end

sql=['SELECT * FROM gPenetration WHERE id=',num2str(site.penid)];
pendata=mysql(sql);

if pendata.numchans<=8
   use_chanstr={'a','b','c','d','e','f','g','h'};
elseif pendata.numchans<100
   use_chanstr=cell(1,pendata.numchans);
   for ii=1:length(use_chanstr)
      use_chanstr{ii}=sprintf('%02d-',ii);
   end
else
   use_chanstr=cell(1,pendata.numchans);
   for ii=1:length(use_chanstr)
      use_chanstr{ii}=sprintf('%03d-',ii);
   end
end

masterid=num2str(site.id);

sql=['SELECT * FROM gDataRaw WHERE masterid=',num2str(masterid),...
   ' AND not(bad) ORDER BY parmfile,respfile,id'];
rawdata=mysql(sql);
%rawids=cat(1,rawdata.id);

sql=['SELECT * FROM gSingleCell WHERE masterid=',num2str(masterid),...
   ' ORDER BY cellid'];
celldata=mysql(sql);
singleids=cat(1,celldata.id);
cellids={celldata.cellid};

for cc=1:length(cellids)
   if ~ismember(celldata(cc).channel,use_chanstr)
      fprintf('numeric/alpha channel mismatch for %s, singleid=%d, channel=%s, unit=%d\n',...
         cellids{cc},singleids(cc),celldata(cc).channel,celldata(cc).unit);
      pass=0;
   end
   test_cellid = [siteid,'-',use_chanstr{celldata(cc).channum},num2str(celldata(cc).unit)];
   if ~strcmp(test_cellid,cellids{cc})
      fprintf('cellid/chan/unit mismatch for %s, singleid=%d, channel=%s, unit=%d. expected cellid %s\n',...
         cellids{cc},singleids(cc),celldata(cc).channel,celldata(cc).unit,test_cellid);
      pass=0;
   end
end

for cc=1:length(cellids)
   cellid=cellids{cc};
   sql=['SELECT sCellFile.*,',...
      ' gSingleCell.id as sid,gSingleCell.cellid as scellid,',...
      ' gSingleRaw.id as rid,gSingleRaw.cellid as rcellid',...
      ' FROM sCellFile LEFT JOIN gSingleCell on sCellFile.singleid=gSingleCell.id',...
      ' LEFT JOIN gSingleRaw on sCellFile.singlerawid=gSingleRaw.id',...
      ' WHERE sCellFile.cellid="',cellid,'"'];
   cfd=mysql(sql);
   
   for ii=1:length(cfd)
      if ~strcmp(cfd(ii).cellid,cfd(ii).scellid)
         fprintf('cellid mismatch between sCellFile(id=%d) and gSingleRaw(id=%d) - %s/%s\n',...
            cfd(ii).id,cfd(ii).sid,cfd(ii).cellid,cfd(ii).scellid);
         pass=0;
      end
      if ~strcmp(cfd(ii).cellid,cfd(ii).rcellid)
         fprintf('cellid mismatch between sCellFile(id=%d) and gSingleRaw(id=%d) - %s/%s\n',...
            cfd(ii).id,cfd(ii).rid,cfd(ii).cellid,cfd(ii).rcellid);
         pass=0;
      end
   end
end

sql=['SELECT * FROM gSingleRaw LEFT JOIN sCellFile ON gSingleRaw.id=sCellFile.singlerawid',...
   ' WHERE isnull(sCellFile.cellid) AND (gSingleRaw.channum>1 OR gSingleRaw.unit>1) AND gSingleRaw.masterid=',num2str(masterid)];
rfdata=mysql(sql);
for ii=1:length(rfdata)
   fprintf('orphaned gSingleRaw entry cellid=%s, rawid=%d, singlerawid=%d\n',...
      rfdata(ii).cellid,rfdata(ii).rawid,rfdata(ii).id);
   pass=0;
end

