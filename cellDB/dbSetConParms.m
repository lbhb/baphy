% function dbSetConParms(r)
% 
% sets db connection with struct containing any or all of the parameters:
%     DBISOPEN DBUSER DB_ENABLED
%     DB_USER DB_SERVER DB_PASSWORD DB_NAME
%
% created SVD 2018-01-16 for migration over the separate Narf/NEMS database
%
function r=dbSetConParms(r)

global DBISOPEN DBUSER DB_ENABLED
global DB_USER DB_SERVER DB_PASSWORD DB_NAME

if isfield(r,'DBISOPEN')
   DBISOPEN=r.DBISOPEN;
end
if isfield(r,'DBUSER')
   DBUSER=r.DBUSER;
end
if isfield(r,'DB_ENABLED')
   DB_ENABLED=r.DB_ENABLED;
end
if isfield(r,'DB_USER')
   DB_USER=r.DB_USER;
end
if isfield(r,'DB_SERVER')
   DB_SERVER=r.DB_SERVER;
end
if isfield(r,'DB_PASSWORD')
   DB_PASSWORD=r.DB_PASSWORD;
end
if isfield(r,'DB_NAME')
   DB_NAME=r.DB_NAME;
end

dbopen(1);




