% function t=dbReadMetaData(cellid);
%
% cellid - name of cell (entry in gSingleCell)
%
% t - structure with each field either a scalar, matrix or string
%
% created CRH 2018-06-15
%
function t=dbReadMetaData(cellid);

sql=['SELECT * FROM gSingleCell WHERE cellid="',cellid,'"'];
singledata=mysql(sql);
if isempty(singledata),
   warning(['no match for cellid ',cellid]);
else
   t.meta_data = jsondecode(char(singledata.meta_data));
end