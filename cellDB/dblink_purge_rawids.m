function status=dblink_purge_rawids(rawids)

if length(rawids) == 1
   rawidlist=mat2str(rawids);
else
   rawidlist=mat2string(rawids);
   rawidlist=strrep(rawidlist,' ', ',');
   rawidlist=rawidlist(2:(end-1));
end

sql=['SELECT id,cellid FROM sCellFile WHERE rawid in (', rawidlist, ')'];
cfd=mysql(sql);
status=1;
if ~isempty(cfd)
   siteid = strsep(cfd(1).cellid,'-');
   siteid = siteid{1};
   % yn=input(sprintf('Delete %d existing sCellFile/gSingleRaw entries and regenerate ([y]/n)? ',length(cfd)),'s');
   %if isempty(yn) || yn(1)~='n'
      uiwait(warndlg(['About to purge entries in sCellFile, gSingleRaw, gSingleCell, NarfResults, NarfBatches, NarfData, sRunData. ', ...
          'Make sure to purge cached recordings in /auto/data/nems_db/recordings/*/', siteid, '* !'], ...
          'Reminder!','modal'))
      dbopen;
      sql=['DELETE FROM sCellFile WHERE rawid in (', rawidlist, ')'];
      mysql(sql);
      sql=['DELETE FROM gSingleRaw WHERE rawid in (', rawidlist, ')'];
      mysql(sql);
      
      % purge Narfxx tables
      sql=['SELECT gSingleCell.* FROM gSingleCell LEFT JOIN gSingleRaw ON gSingleCell.id=gSingleRaw.singleid WHERE gSingleCell.cellid like "',siteid,'%" AND gSingleRaw.singleid is null'];
      celldata=mysql(sql);
      cells_to_purge={celldata.cellid};
      cellstr=['("' , strrep(strjoin(cells_to_purge),' ','","') , '")'];
      sql=['DELETE FROM NarfResults WHERE cellid in ' cellstr];
      mysql(sql);
      sql=['DELETE FROM NarfBatches WHERE cellid in ' cellstr];
      mysql(sql);
      sql=['DELETE FROM NarfData WHERE cellid in ' cellstr];
      mysql(sql);
      sql=['DELETE FROM sRunData WHERE cellid in ' cellstr];
      mysql(sql);
      sql=['DELETE FROM gSingleCell WHERE cellid in ' cellstr];
      mysql(sql);
   %else
    %   status=0;
   %end
end
