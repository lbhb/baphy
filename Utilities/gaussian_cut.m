function y=gaussian_cut(x,params)
a=params(1);
x0=params(2);
sigma=params(3);
xcut=params(4);
y=a*exp(-(x-x0).^2/(2*sigma^2));
y(x<xcut)=0;