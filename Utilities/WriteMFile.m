function WriteMFile(globalparams,exptparams,exptevents,force_overwrite,update_baphy_commit_hash)
% function WriteMFile(globalparams,exptparams,exptevents,force_overwrite[=0],update_baphy_commit_hash[=0])
%
% inputs:
% XXXXparams==[] means don't write that variable. %
% force_overwrite=0 (append) by default
%
% Nima, Stephen , January 2006
%
 if nargin<5
     update_baphy_commit_hash=1;
 end
% FNL 2017-02-28: strip out variables that screw up save
if isfield (exptparams,'FigureHandle'), exptparams = rmfield(exptparams,'FigureHandle');end
if isfield (exptparams,'TempDisp'),     exptparams = rmfield(exptparams,'TempDisp');end
if isfield (exptparams,'wavefig'),      exptparams = rmfield(exptparams,'wavefig');end
if isfield (exptparams,'temporary'),      exptparams = rmfield(exptparams,'temporary');end
if isfield (exptparams,'feedback_text'), exptparams = rmfield(exptparams,'feedback_text');end
if isfield(exptparams,'TrialObject')
    if isfield(exptparams.TrialObject,'TrialEnvelope')
         exptparams.TrialObject.TrialEnvelope=[];
    end
    try 
        has_tev=~isempty(get(exptparams.TrialObject,'TrialEnvelope'));
        if has_tev
             exptparams.TrialObject=set(exptparams.TrialObject,'TrialEnvelope',[]);
        end
    catch err
        if contains(err.message,'There is no ''TrialEnvelope'' property in specified class')
        else
            warning(['Error in removing TrialEnvelope from exptparams.TrialObject for saving:  ',err.message]);
        end
    end
end

if update_baphy_commit_hash
    % CRH 2021-04-30, save latest commit hash in globalparams
    [~,r]=system(sprintf('git -C %s rev-parse HEAD', fileparts(which('baphy'))));
    globalparams.baphy_commit_hash=strtrim(r);
    % if there are changed files (uncommitted) track these here too
    [~,files_changed]=system(sprintf('git -C %s status --porcelain', fileparts(which('baphy'))));
    %files_changed = cellstr(files_changed);
    files_changed = strsplit(files_changed,newline); %strrep(files_changed{1},char(10),[''',...',char(10),'''']);
    if isempty(files_changed{end}), files_changed(end)=[]; end
    globalparams.uncommitted_changes=files_changed;
end

% write it to temp folder on local machine, then move it to the permanent
% folder:
fname = globalparams.tempMfile;
if ~exist(fname,'file') | (exist('force_overwrite','var') & force_overwrite),
    fid = fopen(fname,'w');
else
    fid = fopen(fname,'a');
end
fprintf(fid,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf(fid,'%% STARTING WRITE/APPEND\n');
fprintf(fid,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf(fid,'MFileDone=0;\n');
fclose(fid);

if ~isempty(globalparams),
    AddToMFile(fname,'globalparams',globalparams);
end
if ~isempty(exptparams),
    exptparams_save=exptparams;
    if ~verLessThan('matlab','8.4')
        if isfield(exptparams_save,'ResultsFigure')
            try
                exptparams_save.ResultsFigure=exptparams_save.ResultsFigure.Number; %LAS fix for MATLAB 2017 (ResultsFigure is saved as a huge structure, don't want to save it!
            catch
                exptparams_save.ResultsFigure=[];
            end
        end
    end
    AddToMFile(fname,'exptparams',exptparams_save);
end
if ~isempty(exptevents),
    AddToMFile(fname,'exptevents',exptevents);
end

fid = fopen(fname,'a');
fprintf(fid,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf(fid,'%% DONE WRITE/APPEND\n');
fprintf(fid,'%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n');
fprintf(fid,'MFileDone=1;\n');
fclose(fid);
% now move it:
if ~strcmp(globalparams.tempMfile, globalparams.mfilename),
  try
    movefile(globalparams.tempMfile, globalparams.mfilename);
  catch   % added by Yves because fid was not closed correctly before this point; 2013/10
    fclose('all')
    movefile(globalparams.tempMfile, globalparams.mfilename);
  end   
end

% Check if .mat file exists, if so, delete it
[Path,Name,Ext] = fileparts(globalparams.mfilename);
MatFile = [Path,filesep,'tmp',filesep,Name,'.mat'];;
if exist(MatFile,'file')  delete(MatFile); end