function baphy_remote_figsave(FIG,UH,globalparams,analysis_name,is_psth)
%baphy_remote_figsave(FIG,UH,globalparams,analysis_name)
%baphy_remote_figsave(FIG,UH,globalparams,analysis_name,is_psth)
% FIG: handle of figure to save
% UH: array of handles to make invisible during saving (may be empty)
% globalparams: struct from baphy m-file
% analysis_name: name for the saved figure

global BEHAVIOR_CHART_PATH USECOMMONREFERENCE

AnalysisPath=strrep(BEHAVIOR_CHART_PATH,'behaviorcharts','analysis');

if ~exist('is_psth','var') is_psth = 0; end

yearstr=globalparams.date(1:4);
if exist(AnalysisPath,'dir'),
    if ~exist([AnalysisPath lower(globalparams.Ferret)]),
        mkdir(AnalysisPath,lower(globalparams.Ferret));
    end
    jpegpath=[AnalysisPath lower(globalparams.Ferret) filesep yearstr];
elseif exist('M:\web\analysis','dir'),
    if ~exist(['M:\web\analysis' filesep lower(globalparams.Ferret)]),
        mkdir('M:\web\analysis',lower(globalparams.Ferret));
    end
    jpegpath=['M:\web\analysis' filesep lower(globalparams.Ferret) filesep yearstr];
elseif exist('H:\web\analysis','dir'),
    if ~exist(['H:\web\analysis' filesep lower(globalparams.Ferret)]),
        mkdir('H:\web\analysis',lower(globalparams.Ferret));
    end
    jpegpath=['H:\web\analysis' filesep lower(globalparams.Ferret) filesep yearstr];
elseif exist('H:\web\celldb\analysis','dir'),
    if ~exist(['H:\web\celldb\analysis' filesep lower(globalparams.Ferret)]),
        mkdir('H:\web\celldb\analysis',lower(globalparams.Ferret));
    end
    jpegpath=['H:\web\celldb\analysis' filesep lower(globalparams.Ferret) filesep yearstr];
elseif exist('/auto/data/web/analysis','dir'),
    if ~exist(['/auto/data/web/analysis' filesep lower(globalparams.Ferret)]),
        mkdir('/auto/data/web/analysis',lower(globalparams.Ferret));
    end
   jpegpath=['/auto/data/web/analysis' filesep lower(globalparams.Ferret) filesep yearstr];
else
   jpegpath='';
end
if USECOMMONREFERENCE
    com_str='_ComRef';
else
    com_str='';
end
if ~isempty(jpegpath),
  if is_psth && strcmp(analysis_name,'raster') analysis_name = 'psth'; end
  jpegfile=[basename(globalparams.mfilename(1:end-1)),analysis_name,com_str,'.jpg'];
  if ~exist(jpegpath),  mkdir(jpegpath);   end
  fprintf('printing to %s\n',jpegfile);

  if(~isempty(UH))
      set(UH,'Visible','off'); drawnow
  end
  try
      if(0)
          print('-djpeg',['-f',num2str(FIG)],[jpegpath filesep jpegfile]);
      else
          add={};
          ou=get(FIG,'Units');
          set(FIG,'Units','pixels');
          p=get(FIG,'Position');
          if p(3)<1000
             add={'-m2'};% increase resolution by a factor of 2
          end
          set(FIG,'Units',ou);
          fname=[jpegpath filesep jpegfile];
          export_fig(fname,'-jpg',add{:},'-nocrop',FIG);
          [w,s]=unix(['chmod a+rw ',fname]);

      end
  catch err
      if strcmp(err.identifier,'MATLAB:print:CannotCreateOutputFile')
          warning(['Did not save jpeg. Could not create ',jpegpath filesep jpegfile])
      elseif strcmp(err.identifier,'MATLAB:render:badData')
          warning('export_fig did not work, using print')
          print('-djpeg',['-f',num2str(FIG)],[jpegpath filesep jpegfile]);
      else
          rethrow(err)
      end
  end
  if(~isempty(UH))
      set(UH,'Visible','on');
  end
end
