%function [r,tags,trialset,exptevents,sortextras]=...
%     loadsiteraster(spkfile,options)
%
% created SVD
%
function [r,tags,trialset,exptevents,sortextras,cellids]=...
    loadsiteraster(spkfile,startbin,stopbin,options)


persistent siteid meanresp

if nargin==2,
   options=startbin;
   startbin=[];
   stopbin=[];
end

channel=getparm(options,'channel',[]);
unit=getparm(options,'unit',[]);
rasterfs=getparm(options,'rasterfs',1000);
includeprestim=getparm(options,'includeprestim',0);
shuffletrials=getparm(options,'shuffletrials',0);
meansub=getparm(options,'meansub',0);
if ~isfield(options,'tag_masks'),
   tag_masks={};
elseif isnumeric(options.tag_masks),
   psthonly=options.tag_masks;
   tag_masks={};
else
   tag_masks=options.tag_masks;
end
psthonly=getparm(options,'psthonly',-1);
sorter=getparm(options,'sorter','');
includeincorrect=getparm(options,'includeincorrect',0);
mua=getparm(options,'mua',0);
lfpclean=getparm(options,'lfpclean',0);
lfp=getparm(options,'lfp',0);

if isempty(channel),
   sql=['SELECT distinct cellid, channum, unit FROM sCellFile WHERE respfile="',basename(spkfile),'"',...
      ' ORDER BY cellid'];
   cfd=mysql(sql);
   channel=cat(1,cfd.channum);
   unit=cat(1,cfd.unit);
   fprintf('identified %d units for %s\n',length(channel),basename(spkfile));
   cellids={cfd.cellid};
end


r=[];
cellcount=length(channel);

for ii=1:cellcount,
   if lfp==0,
      params=options;
      params.channel=channel(ii);
      params.unit=unit(ii);
%       params.rasterfs=rasterfs;
%       params.psthonly=psthonly;
%       params.tag_masks=tag_masks;
%       params.includeprestim=includeprestim;
%       if isfield(options,'trialrange'),
%          params.trialrange=options.trialrange;
%       end
      if nargout>4
          [tr,tags,trialset,exptevents,sortextras]= ...     
              loadspikeraster(spkfile,params);   % edited CRH 1/7/18 - case based on nargout user so that this uses cache
      else
          [tr,tags,trialset,exptevents]= ...     
              loadspikeraster(spkfile,params);   % edited CRH 1/7/18 - case based on nargout user so that this uses cache
      end
   else
      parmfile=strrep(spkfile,'sorted/','');
      parmfile=strrep(parmfile,'.spk.mat','');
      %[r,tags,trialset,exptevents]=loadgammaraster(mfile,channel,...
      %          rasterfs,includeprestim,tag_masks,psthonly,lof,hif,envelope,trialrange)
      
      [tr,tags,trialset,exptevents]=loadgammaraster(parmfile,...
         channel(ii),rasterfs,includeprestim,tag_masks,psthonly,80,200,1);
   end
   if size(tr,3)==1,
      r=merge_rasters(r,tr,3);
   else
       if ii==1
           r = tr;
       else
            r=merge_rasters(r,tr,4);
       end
   end
end

if meansub,
   disp('subtracting mean response from each channel');
   
   thissiteid=basename(spkfile);
   thissiteid=strsep(thissiteid,'_');
   thissiteid=thissiteid{1}(1:(end-2));
   
   if ~strcmp(thissiteid,siteid) || size(r,3)~=length(meanresp),
      meanresp=squeeze(nanmean(nanmean(r,1),2));
      siteid=thissiteid;
   end
   for ii=1:size(r,3),
      r(:,:,ii)=r(:,:,ii)-meanresp(ii);
   end
end

if shuffletrials,
   disp('shuffling response trials');
   for ii=1:size(r,1),
      for jj=1:size(r,3),
         r(ii,:,jj)=shuffle(r(ii,:,jj));
      end
   end
end

if size(r,4)>1,
   return
end

if psthonly>0,
    r=permute(r,[3 1 2]);
    r=r(:,:);

    if exist('startbin','var'),
        if isempty(startbin) || startbin==0,
            startbin=1;
        end
        if ~exist('stopbin','var') || isempty(stopbin) || stopbin==0,
            stopbin=size(r,2).*size(r,3);
        elseif stopbin>size(r,2).*size(r,3),
            stopbin=size(r,2).*size(r,3);
        end
        r=r(:,startbin:stopbin);
    end
end
