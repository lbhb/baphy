function r_f = remove_opto_artifacts(r, trial_start_indices, spikefs, ...
   exptparams, exptevents, remove_laser_artifact_sec, interp_laser_sec, verbose)
%function r_f = remove_opto_artifacts(r, trial_start_indices, spikefs, ...
%   exptparams, exptevents, remove_laser_artifact_sec, interp_laser_sec)
%
% 
% remove artifacts around laser on and off at two time scales. both seem
% important, but it hasn't been tested exhaustively. a bit slow to run if
% the data doesn't include any laser stim.
%  remove_laser_artifact_sec - compute average across this long window
%  following on/off event within one channel. subtract mean from each event
%  in the raw trace
%  inter_laser_sec - if >0, also linear interpolate over this much shorter
%  period after the mean subtraction. This gets rid of really nasty brief
%  stuff.
%
% at some point consider scaling average that is subtracted on each trial,
% scaled to the size of the artifict -- if artifact size varies
% substantiall.
%
if ~exist('remove_laser_artifact_sec', 'var')
   remove_laser_artifact_sec = 0.1;
end
if ~exist('interp_laser_sec', 'var')
   interp_laser_sec = 0;
end
if ~exist('verbose', 'var')
    verbose=0;
end

if trial_start_indices(1)==1
    trial_start_indices(1)=2;
end
% figure out laser on/off times.
[t,trial,Note,toff,EventIndex] = evtimes(exptevents,'LightStim');
if isempty(t)
   par = exptparams.TrialObject;
   fs = par.SamplingRate;
   % need to regenerate
   TrialCount=exptevents(end).Trial;
   for trialidx = 1:TrialCount
      [~,~,~,~,EventIndex] = evtimes(exptevents,'*Reference+Light',trialidx);
      newevents = exptevents(EventIndex);
      if ~isempty(newevents)
         maxevent=newevents(end);
         maxtrialbins=round(maxevent.StopTime.*fs);
         [LightBand, newevents]=generateLightTrial(par,newevents,maxtrialbins);
      end
      
      if trialidx==1
         events=newevents;
      else
         events=cat(2,events,newevents);
      end
   end
   exptevents=events;
   [t,trial,Note,toff,EventIndex] = evtimes(exptevents,'LightStim');
end

% r=r-mean(r);
r_f=r;

if isempty(t)
   % no stim events to remove!
   return
end

% figure out when laser events occurred
minstimwindow=min(toff-t);
if minstimwindow>remove_laser_artifact_sec/2
   % long stim periods. account for onset and offset
   onsetbins=round(minstimwindow*spikefs);
   offsetbins=round(remove_laser_artifact_sec*spikefs);
else
   % short, just chop single window
   if trial(1)==trial(2)
      % short series. make sure to only chop up to next pulse
      minstimwindow=t(2)-t(1);
      onsetbins=round(minstimwindow*spikefs);
   else
      onsetbins=round(remove_laser_artifact_sec*spikefs);
   end
   
   offsetbins=0;
end


onsetmean=zeros(onsetbins,1);
offsetmean=zeros(offsetbins,1);

% average over laser onsets
onsets = zeros(onsetbins,length(t));
for evidx = 1:length(t)
   % fudge backwards and extra bin
   start = round(trial_start_indices(trial(evidx))+t(evidx)*spikefs-1);
   if start>0
       onsets(:,evidx) = r(start:(start+onsetbins-1));
       onsetmean = onsetmean+r(start:(start+onsetbins-1));
   else
       onsets(:,evidx) = r(1:onsetbins);
       onsetmean = onsetmean+r(1:onsetbins);
   end
   
   if offsetbins>0
       % now average over laser offsets, accounting for possible overlap with
       % onset removal
      stop = round(trial_start_indices(trial(evidx))+toff(evidx)*spikefs-1);
      %fprintf("%d %d %d\n",evidx,trial_start_indices(evidx),stop);
      offsetmean = offsetmean + r(stop:(stop+offsetbins-1));
   end
end
onsetmean=onsetmean/length(t);
offsetmean=offsetmean/length(t);

%[u,s,v]=svd(onsets,'econ');
%onsetmean=u(:,1);

% then subtract mean from each onset
for evidx = 1:length(t)
   start = round(trial_start_indices(trial(evidx))+t(evidx)*spikefs-1);
   %r_f(start:(start+onsetbins-1)) = r_f(start:(start+onsetbins-1)) - onsetmean*v(evidx,1)*s(1,1);
   r_f(start:(start+onsetbins-1)) = r_f(start:(start+onsetbins-1)) - onsetmean;
end

if offsetbins>0
   % then subtract mean from each offset
   for evidx = 1:length(t)
      stop = round(trial_start_indices(trial(evidx))+toff(evidx)*spikefs-1);
      r_f(stop:(stop+offsetbins-1)) = r_f(stop:(stop+offsetbins-1)) - offsetmean;
   end
end

if interp_laser_sec > 0
   % optional: linear interpolation over area with worst artifact.
   interp_laser_bins = round(interp_laser_sec*spikefs);

   % interp onsets first
   for evidx = 1:length(t)
      start = round(trial_start_indices(trial(evidx))+t(evidx)*spikefs);
      mask0=start-4; mask1=start+interp_laser_bins;
      r_f(mask0:mask1)=linspace(r_f(mask0),r_f(mask1),mask1-mask0+1);
   end
   
   % then offsets, though unlikely to overlap unless the pulse is very brief!
   for evidx = 1:length(t)
      stop = round(trial_start_indices(trial(evidx))+toff(evidx)*spikefs);
      mask0=stop-4; mask1=stop+interp_laser_bins;
      r_f(mask0:mask1)=linspace(r_f(mask0),r_f(mask1),mask1-mask0+1);
   end
end

if verbose

    avg_window = -50:400;

    a = zeros(length(avg_window),length(trial));
    a_f = zeros(length(avg_window),length(trial));
    b = zeros(length(avg_window),length(trial));
    b_f = zeros(length(avg_window),length(trial));
    for ii = 1:length(trial)
        start = double(round(trial_start_indices(trial(ii))+t(ii)*spikefs-1));
        a(:,ii) = r(start+avg_window);
        a_f(:,ii) = r_f(start+avg_window);
        stop = double(round(trial_start_indices(trial(ii))+toff(ii)*spikefs-1));
        b(:,ii) = r(stop+avg_window);
        b_f(:,ii) = r_f(stop+avg_window);
    end

    fs=spikefs;

    figure;
    subplot(2,2,1);
    plot(avg_window./fs.*1000, a);
    title('artifact events');
    xlabel('ms');
    axis tight

    subplot(2,2,3);
    plot(avg_window./fs.*1000, a_f);
    title('events with mean artifact subtracted');
    xlabel('ms');
    axis tight
    
    
    subplot(2,2,2);
    plot(avg_window./fs.*1000, b);
    title('offset artifact events');
    xlabel('ms');
    axis tight

    subplot(2,2,4);
    plot(avg_window./fs.*1000, b_f);
    title('offsets with mean artifact subtracted');
    xlabel('ms');
    axis tight
    
    drawnow
end


