function IdxSet=shuffle_blocked(Idx,shuffle_block,varargin)
% IdxSet=shuffle_blocked(Idx,shuffle_block,end_shuf_cond)
if nargin==2
    end_shuf_cond=0;
else
    end_shuf_cond=varargin{1};
end
%shuffles first dimension of Idx
do_transform = isrow(Idx);
if do_transform
    Idx=Idx';
end
if shuffle_block > size(Idx,1)
    warning(['Shuffle block was set to %d, which is more than the length ',...
        'of the target index set (%d). Setting to %d.'],shuffle_block,size(Idx,1),size(Idx,1))
    shuffle_block = size(Idx,1);
end
if(shuffle_block~=size(Idx,1))
    separate_shuffles=size(Idx,1)/shuffle_block;
    if(mod(separate_shuffles,1)~=0)
        if end_shuf_cond==0
            warning(['The length of the index (%d) is not evenly divisible ',...
                'by the computed shuffle block (%d). Shuffling the last block ',...
                'and remainder together.'],size(Idx,1),shuffle_block)
        end
        if end_shuf_cond<2
            separate_shuffles=floor(separate_shuffles)-1;
        else
            separate_shuffles=floor(separate_shuffles);
        end
        do_end_shuffle=true;
    else
        do_end_shuffle=false;
    end
    if iscell(Idx)
        IdxSet=cell(size(Idx));
    else
        IdxSet=nan(size(Idx));
    end
    for i=1:separate_shuffles
        shufInd=randperm(shuffle_block);
        thisIdx=Idx((i-1)*shuffle_block+(1:shuffle_block),:);
        IdxSet((i-1)*shuffle_block+(1:shuffle_block),:)=thisIdx(shufInd,:);
        %IdxSet((i-1)*shuffle_block+(1:shuffle_block))=shuffle(Idx((i-1)*shuffle_block+(1:shuffle_block)));
    end
    if(do_end_shuffle)
        i=i+1;
        thisIdx=Idx((i-1)*shuffle_block+1:end,:);
        shufInd=randperm(size(thisIdx,1));
        IdxSet((i-1)*shuffle_block+1:end,:)=thisIdx(shufInd,:);
        %IdxSet((i-1)*shuffle_block+1:end)=shuffle(Idx((i-1)*shuffle_block+1:end));
    end
else
    shufInd=randperm(size(Idx,1));
    IdxSet=Idx(shufInd,:);
end
if do_transform
    IdxSet=IdxSet';
end
end