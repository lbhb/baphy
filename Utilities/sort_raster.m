function [r,tags,trialset]=sort_raster(r,tags,trialset,tag_masks,StimTagNames,filename,using_rc_subset,do_resort)
%[r,tags,trialset]=sort_raster(r,tags,trialset,tag_masks,StimTagNames,filename,using_rc_subset,do_resort)

% sorts by StimTagNames if exclusion criterea are met (see below)
% sorts by frequency if FTC
% returns only Reference1 or 2 if using_rc_subset

extraidx=length(StimTagNames);
if(isempty(tag_masks) || length(tag_masks{1})<8 || ...
        ~strcmp(tag_masks{1}(1:8),'SPECIAL-'))
     % SVD 2017-03-10 : break up by commas, then pull out last
     % value (Reference/Target/Reference1/etc) from cell array
    refstrs=cellfun(@(x)strsep(x,','),tags,'Uni',0);
    refstrs=cellfun(@(x)strtrim(x{end}),refstrs,'Uni',0);
    u_refstrs=unique(refstrs);
    if length(u_refstrs)>1,
       refdash=cellfun(@(x)findstr(x,'REF-'),tags,'Uni',0);
       rd=cat(1,refdash{:});
       if ~sum(rd==0),
          u_refstrs=1;
       end
    end
else
    u_refstrs=1;
end


%% main sorting block
%sort by StimTagNames as long as inclusion criterea are met
if (isempty(tag_masks) || length(tag_masks{1})<8 || ...
    ~strcmp(tag_masks{1}(1:8),'SPECIAL-')) && ...
       (isempty(tag_masks) || length(tag_masks{1})<6 || ...
        ~strcmpi(tag_masks{1}((end-5):end),'TARGET')) && ...
       (isempty(tag_masks) || length(tag_masks{1})<4 || ...
        ~strcmpi(tag_masks{1}((end-3):end),'TARG')) && ...
       do_resort && ...
       isempty(strfind(filename,'_FTC')) && ...
       isempty(strfind(filename,'_CCH')) && ...
       isempty(strfind(filename,'_AMN')) && ...
       isempty(strfind(filename,'_SSA')) && ...
       isempty(strfind(filename,'_MTS')) && ...
       isempty(strfind(filename,'_ALM')) && ...
       isempty(strfind(filename,'_AMN')) && ...
       isempty(strfind(filename,'_PHD')) && ...
       isempty(strfind(filename,'_FLT')) && ...
       isempty(strfind(filename,'_VWL')) && ...
       (isempty(strfind(filename,'PPS_VOC')) || length(u_refstrs)==1),
   maptoidx=zeros(1,length(tags));
   if length(u_refstrs)>1
       error('Check to see that this is doing the right thing for interleaved stimuli')
   end
   for ii=1:length(tags),
      tt=strsep(tags{ii},',',1);
      if length(tt)==4 && strcmp(tt{4},' Reference'),
         % special case, put comma in string descriptor! arg!
         tt{2}=[tt{2} ',' tt{3}];
         tt={tt{1},tt{2},tt{4}};
      end
      if isnumeric(tt{2}),
          tt=num2str(tt{2});
      else
          tt=strtrim(tt{2});
      end
      matchidx=find(strcmp(tt,StimTagNames));
      if isempty(matchidx),
          matchidx=strmatch(tt,StimTagNames);
      end
      if isempty(matchidx) && ~isempty(strfind(filename,'_SPT')),
         tt2=strrep(tt,'SPORC','TORC');
         matchidx=strmatch(tt2,StimTagNames);
      end
      
      if ~isempty(matchidx),
         maptoidx(ii)=matchidx(1);
      else
         extraidx=extraidx+1;
         maptoidx(ii)=extraidx;
         % disp('loadspikeraster.m: stimtagnames/tags mismatch! tacking on end');
      end
   end
   
   if 1,
       newr=nan*ones(size(r,1),size(r,2),extraidx,size(r,4));
       newtags=cell(1,extraidx);
       newtrialset=zeros(size(trialset,1),extraidx);
       for ttt=1:length(maptoidx),
           newr(:,:,maptoidx(ttt),:)=r(:,:,ttt,:);
           newtags{maptoidx(ttt)}=tags{ttt};
           newtrialset(:,maptoidx(ttt))=trialset(:,ttt);
       end
       r=newr;
       tags=newtags;
       trialset=newtrialset;
   else
       %old method... stimulus idx not lined up properly if full rep not completed.
       [ttt,mapidx]=sort(maptoidx);
       r=r(:,:,mapidx,:);
       tags={tags{mapidx}};
       trialset=trialset(:,mapidx);
   end
   
   %SVD : force skip of step that removes the nan trials (for expts where
   %full rep not completed)
   using_rc_subset=0;
   
end

%% If FTC, sort data by frequency
if any(cellfun(@(x)~isempty(strfind(filename,x)),{'_FTC','_FLT'})) && ...
    length(tags)>1 && ...
    (isempty(tag_masks) || length(tag_masks{1})<8 || ...
    ~strcmp(tag_masks{1}(1:8),'SPECIAL-')),
    isflt=~isempty(strfind(filename,'_FLT'));
    for cnt1=1:length(tags),
        temptags = strrep(strsep(tags{cnt1},',',1),' ','');
        if(isflt)
            temptags2 = strrep(strsep(temptags{2},':',1),' ','');
            unsortedtags(cnt1) = str2num(temptags2{1});
        else
            unsortedtags(cnt1) = str2num(temptags{2});
        end
    end
    
    [sortedtags, index] = sort(unsortedtags); % sort the numeric tags
    
    tags=tags(index);
    r=r(:,:,index,:);
    trialset=trialset(:,index);
end

%% For interleaved stim data, pull out only Reference1 or Reference2
%if ~isempty(tag_masks) && isempty(findstr(tag_masks{1},'SPECIAL'))
if using_rc_subset,
   goodmasks=zeros(length(tags),1);
   for cnt1=1:length(tags),
      if ~isempty(findstr(tags{cnt1},tag_masks{1})),
         goodmasks(cnt1)=1;
      end
   end
   r=r(:,:,find(goodmasks),:);
   tags=tags(find(goodmasks));
   trialset=trialset(:,find(goodmasks));
end
