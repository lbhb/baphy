function [status,failed_var]=UTcheck_matching_opts(reference,test,exact,ignore_fields)
if(nargin<4)
    ignore_fields={};
end
failed_var='';
status=false;
newvars={'merge_threshold_missing_percentage','SU_missing_spike_percentage','keep_cutoff'};% new variables added, ignore them when making comparisons of new files...
ignore_fields=[ignore_fields,newvars];
if(exact)
    status=isequalwithequalnans(test,reference);
else
    if(isobject(test))
        test=struct(test);
        reference=struct(reference);
    end
    if(isstruct(test))
        fns=fieldnames(test);
        mi=strcmp(fns,'match_conditions');
        if(any(mi))
            do_match_conditions=true;
            fns(mi)=[];
        else
            do_match_conditions=false;
        end
        ri=cellfun(@(x)any(strcmp(ignore_fields,x)),fns);
        fns(ri)=[];
        if(isempty(fns))
            status=true;
        end
        for i=1:length(fns)
            if(isfield(reference,fns{i}))
                if(length(reference)>1)
                    for j=1:length(reference)
                        [status,failed_var]=UTcheck_matching_opts(reference(j).(fns{i}),test(j).(fns{i}),exact);
                        if(~status)
                            nvi=strcmp(failed_var,ignore_fields);
                            if(any(nvi))
                                test.(fns{i})=rmfield(test.(fns{i}),ignore_fields(nvi));
                                %error('Make sure this is what you want to do (ignore this mismatch)...')
                                [status,failed_var]=UTcheck_matching_opts(reference.(fns{i}),test.(fns{i}),exact);
                            end
                        end
                    end
                else
                    
                    [status,failed_var]=UTcheck_matching_opts(reference.(fns{i}),test.(fns{i}),exact);
                    nvi=cellfun(@(x)any(strcmp(failed_var,x))||any(strcmp(fns{i},x)),ignore_fields);
                    while(any(nvi))
                        test.(fns{i})=rmfield(test.(fns{i}),ignore_fields(nvi));
                        %error('Make sure this is what you want to do (ignore this mismatch)...')
                        [status,failed_var]=UTcheck_matching_opts(reference.(fns{i}),test.(fns{i}),exact);
                        nvi=cellfun(@(x)any(strcmp(failed_var,x)),ignore_fields);
                    end
                end
                
                status=all(status);
                if(~status)
                    if(~isempty(failed_var))
                        failed_var=[fns{i},'.',failed_var];
                    else
                        failed_var=fns{i};
                    end
                    break;
                end
            elseif(any(strcmp(ignore_fields,fns{i})))
                %ignore
            else
                failed_var=fns{i};
                status=false;
                break
            end
        end
        if(do_match_conditions)
            
            [pass,failed_var]=UTcheck_complex(reference,test.match_conditions,failed_var);
            if(~pass)
                status=false;
            end
        end
    else
        try
            if(iscell(test))
                if(isempty(test)&&isempty(reference))
                    status=true;
                else
                for i=1:length(test)
                    [status(i),failed_var{i}]=UTcheck_matching_opts(reference{i},test{i},exact);
                end
                end
            else
                status=isequalwithequalnans(reference,test);
            end
        catch
            status=isequalwithequalnans(reference,test);
        end
    end
end
