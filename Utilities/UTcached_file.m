function [status,data,failed_var] = UTcached_file(mode,pth,options,varargin)

file_exists=exist(pth,'file');
failed_var=[];
if(file_exists)
    d=load(pth,'options','count');
    
    exact=0;ignore_fields=[];
    for k=1:length(d.options)
        [opteq(k),failed_var{k}]=UTcheck_matching_opts(d.options{k},options,exact,ignore_fields);
    end
    ai=find(opteq);
    if(isempty(ai))
        ai=d.count+1;
        d.count=ai;
        matched=false;
    elseif(length(ai)>1)
        error('multiple matching stored structures')
    else
        matched=true;
    end
else
    matched=false;
    d.count=1;
    addstr={'-append'};
    ai=1;
end
switch mode
    case 'load'
        if(matched)
            d=load(pth,['data',num2str(ai)]);
            status=true;
            data=d.(['data',num2str(ai)]);
        else
            status=false;
            data=struct;
        end
    case 'save'
        for i=1:length(varargin)
            d.(['data',num2str(ai)]).(inputname(3+i))=varargin{i};
        end
        d.options{ai}=options;
        if(file_exists)
            save(pth,'-Struct','d','-append')
        else
            UTmkdir(pth);
            save(pth,'-Struct','d')
        end
        status=true;
        data=[];
end

end