
tip_depth=2514;
[s,UCLA_to_OEP]=probe_64D();
depths=s.z(UCLA_to_OEP(Electrodes))+s.tipelectrode;
            x=s.x(UCLA_to_OEP(Electrodes));x=round(x/10)*10;
            [unx,~,uni]=unique(x);
            fs=18;fs_a=14;
smooth_passes=1;
fh=figure;
bad_channels=(Electrodes==31)';
set(fh,'Position',[ 5          55         948        1062]);
ax=subplot1(1,length(unx),'Gap',[0.01 0],'Max',[.88 .95]);
stimi=options_o.StimInds{1};
L=size(R,1);
%options_o.PreStimSilence=.1;
time=((1:L)./options_o.rasterfs-options_o.PreStimSilence)*1000;
for xi=1:length(unx)
    E_mask=x==unx(xi) & ~bad_channels;
    imagesc(time,tip_depth-depths(E_mask),fastsmooth(squeeze(mean(mean(R(:,:,stimi,E_mask),2),3))*options_o.rasterfs,3,smooth_passes,1)','Parent',ax(xi))
end
set(ax,'Xlim',[-10 100]);
linkaxes(ax);
set(ax,'YDir','reverse')
%set(ax,'YLim',tip_depth+[min(-depth) max(-depth)]+[-25 +25])
set(ax,'YLim',tip_depth+[min(-depths) max(-depths)])
colormap('hot');
colormap(pmkmp(256,'Linlhot'));
cl=get(ax,'CLim');
%set(ax,'CLim',[0 max([cl{:}])])
xlabel(ax(1),'Time (ms)','FontSize',fs)
ylabel(ax(1),'Depth (\mum)','FontSize',fs)
set(ax,'TickDir','out','Box','off')
set(ax,'FontSize',fs_a)
axp=get(ax(end),'Position');
cb=colorbar('peer',ax(end),'FontSize',fs_a);
set(ax(end),'Position',axp);
ylabel(cb,'Rate (spikes/sec)','FontSize',fs)
if(length(tags)>1)
    freq_str=', ';
    for i=1:length(options_o.StimInds{1})
        ss=strsep(tags{options_o.StimInds{1}(i)},',');
        freq_str=[freq_str,num2str(ss{2}),','];
    end
    freq_str(end)=[];
else
    freq_str='';
end
str=[strrep(basename(globalparams.mfilename(1:end-1)),'_','\_'),'MU PSTH',freq_str];
title(str,'FontSize',fs)
set(gcf,'Color','w')
if(options_o.save_figs)
    baphy_remote_figsave(fh,[],globalparams,'MU PSTH')
end

if(0)
    [s,UCLA_to_OEP]=probe_64D();
depth=s.z(UCLA_to_OEP(Electrodes));
options_o.PreStimSilence=.5;
time=((1:L)./options_o.rasterfs-options_o.PreStimSilence)*1000;
figure;
smooth_passes=1;
e_inds=3:3:64;
e_inds=1:21;
imagesc(time,tip_depth-depth(e_inds),fastsmooth(squeeze(mean(R(:,:,:,e_inds),2)),3,smooth_passes,1)')
ax=gca;
set(ax,'Xlim',[-10 100]);
linkaxes(ax);
set(ax,'YDir','reverse')
%set(ax,'YLim',[min(-depth) max(-depth)]+[-25 +25])
%colormap('hot');
colormap(pmkmp(256,'LinearL'))
%cl=get(ax,'CLim');
%set(ax,'CLim',[0 max([cl{:}])])
xlabel(ax(1),'Time (ms)')
ylabel(ax(1),'Depth (\mum)')
set(ax,'TickDir','out','Box','off')

end