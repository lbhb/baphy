% function gzevpfile=evpcompress(evpfile,globalparams);
%
% gzip an evp file but save header information in temp directory
% first to speed up header checks.
%
% created SSVD 2009-01-19
%
function [gzevpfile,runinfo]=evpcompress(evpfile,globalparams)

global BAPHY_LAB

% adding evp extension if not there
if ~exist(evpfile,'file') && ~strcmpi(evpfile(end-3:end),'.evp'),
   evpfile=[evpfile '.evp'];
end

gzevpfile=[evpfile,'.gz'];
if exist(gzevpfile,'file')
   disp('zipped file already exists. skipping');
   runinfo=rawgetinfo(evpfile,globalparams);
   return
end

% save header
[bb,pp]=basename(evpfile);
if ~exist([pp 'tmp'],'dir'),
   mkdir(pp,'tmp');
end
hfilename=[pp 'tmp' filesep bb '.head'];

[pp,bb,ee]=fileparts(evpfile);
bbpref=strsep(bb,'_');
bbpref=bbpref{1};
checkrawevp=[pp filesep 'raw' filesep bbpref filesep bb '.001.1' ee];
if exist(checkrawevp,'file'),
  evpfile=checkrawevp;
end

if ~exist(evpfile,'file')
   warning('evp file not found.');
   return
end

runinfo=rawgetinfo(evpfile,globalparams);
EVPVersion=runinfo.evpv;

% save OLD version header even for evp version 5!
if EVPVersion>=5,
   disp('saving header data in pre evp version 5 format');
end
header3=[EVPVersion runinfo.spikechancount runinfo.auxchancount runinfo.spikefs runinfo.auxfs runinfo.trialcount runinfo.lfpchancount runinfo.lfpfs 0 0];

%[fid,sError]=fopen(evpfile,'r','l');
%header3=fread(fid,10,'uint32');
%fclose(fid);

[fid,sError]=fopen(hfilename,'w','l');
%fwrite(fid,header3,'uint32');
fwrite(fid,header3(1:7),'uint32');
fwrite(fid,header3(8:10),'single');
fclose(fid);



sigthreshold=4;
if 0,
    disp('skipping evpcompress');
elseif runinfo.spikechancount>0,
  fprintf('pre-caching sigma threshold events for %d channels.\n',runinfo.spikechancount);
  for channel=1:min([runinfo.spikechancount,globalparams.NumberOfElectrodes])
    if runinfo.spikechancount<=8,
      cachefile=cacheevpspikes(evpfile,channel,unique([sigthreshold -4 4]),2,0,.3,runinfo);
    elseif strcmpi(BAPHY_LAB,'lbhb'),
      % option to cache with threshold of 5.5 for array recordings.
      %cachefile=cacheevpspikes(evpfile,channel,unique([sigthreshold 5.5]),0,0,.3,runinfo);
      %cachefile=cacheevpspikes(evpfile,channel,unique([sigthreshold]),0,0,.3,runinfo);
    else
      % also cache with threshold of 3.7 for array recordings.
      cachefile=cacheevpspikes(evpfile,channel,unique([sigthreshold 4 3.7]),0,0,.3,runinfo);
    end
  end
else
  % maybe this is evp version 5? check for evps in raw directory:
  [pp,bb,ee]=fileparts(evpfile);
  rawevp=[pp filesep 'raw' filesep bb '.001.*.evp'];
  dd=dir(rawevp);
  ok=zeros(length(dd),1);
  for ii=1:length(dd),
    if isempty(findstr(dd(ii).name,'.mean.')),
      ok(ii)=1;
    end
  end
  dd=dd(find(ok));
  runinfo.spikechancount=length(dd);
  fprintf('pre-caching sigma threshold events for %d channels (in raw).\n',runinfo.spikechancount);
  for channel=1:runinfo.spikechancount,
    revp=[pp filesep 'raw' filesep dd(1).name];
    cachefile=cacheevpspikes(revp,channel,sigthreshold,2,0,.3,runinfo);
  end
  
end

gzip(evpfile);
