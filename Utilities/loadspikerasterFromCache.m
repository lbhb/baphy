function [r] = loadspikerasterFromCache(expParms, options)

    % Load a spike raster from cache in experiment directory
    % If file doesn't exist in cache, then this will create it by calling
    % cacheSpikeRaster.m. If file does exist in cache, this will load
    % faster than loadspikeraster
    %
    %
    % inputs:
    %  expParms - struct containing unique experiment identifiers
    %   animal - string (animal name in db)
    %   exp_site - string (ex: TAR010c)
    %   runclass - string (ex: PTD) note - if more than one file meets
    %   criteria, load all files and cache separately
    %   runs - int, which runs of the given runclass to load data for (ex: runs=5 would load data
    %   for TAR017b05 but not for TAR017b04)
    %   behaviorClass - string (ex: passive), DEFAULT: 'all' (will load
    %   whichever is appropriate based on runs variable
    %
    %  options - structure can contain the following fields:
    %   channel - electrode channel (default 1)
    %   unit - unit number (default is 1)
    %   rasterfs in Hz (default 1000)
    %   includeprestim - raster includes silent period before stimulus onset
    %   tag_masks - cell array of strings to filter tags, eg,
    %             {'torc','reference'} or {'target'}.  AND logic.  default ={}
    %   psthonly - shape of r (default -1, see below)
    %   sorter - preferentially load spikes sorted by sorter.  if not
    %            sorted by sorter, just take primary sorting
    %   lfpclean - [0] if 1, remove MSE prediction of spikes (single
    %              trial) predicted by LFP
    %   includeincorrect - if 1, load all trials, not just correct (default 0)
    %   runclass - if set, load only data for runclass when multiple runclasses
    %              in file
    %
    % created 1-5-18, CRH


    if ~isa(expParms, 'struct')
        error('not right data type')
    end
    
    if ~isfield(expParms, 'animal')
       error('expParms.animal must specify animal name. Example: Tartufo') 
    else
        animal = expParms.animal;
    end
    
    if ~isfield(expParms, 'exp_site')
        site = expParms.exp_site;
        error('expParms.animal must experiment and site. Example: TAR010c')
    else
        site = expParms.exp_site;
    end
    
    if ~isfield(expParms, 'runclass')
        run = 'none';
        warning('Make sure tag-masks applied to all runclasses and that all files were sorted together')
    else
        run = expParms.runclass;
    end
    
    if ~isfield(expParms, 'runs')
        error('specify one run - may want to fix this in the future...');
    else
        runs = num2str(expParms.runs);
        if length(runs)==1
            runs = strcat('0',runs);
        end
    end
    
    
    exp = site(1:(end-1));
    if ~isfield(expParms, 'behaviorClass')
        % check behavior class
        data_dir = strcat('/auto/data/daq/',animal,filesep,exp,filesep,'sorted');
        fn = strcat(data_dir,filesep,site,runs,'_p_',run,'.spk.mat');
        if exist(fn)==2
            behaviorClass='p';
        elseif exist(strcat(data_dir,filesep,site,runs,'_a_',run,'.spk.mat'))==2
            behaviorClass='a';
        else
            error('Error in expParms. Cant find experiments matching user input')
        end
    elseif strcmp(expParms.behaviorClass,'passive')
        behaviorClass='p';
    elseif strcmp(expParms.behaviorClass,'active')
        behaviorClass='a';
    end
   
    
    % define the root directory for this experiement
    root_dir = strcat('/auto/data/daq/',animal,filesep,exp,filesep,'sorted',filesep,'cache',filesep);
    fn = strcat(site,runs,'_',behaviorClass,'_',run,'_ch',num2str(options.channel),'_', ...
        num2str(options.unit),'_fs',num2str(options.rasterfs),'_',options.tag_masks); 
    cached_fn = char(strcat(root_dir,fn));
    
    try 
        r = load(cached_fn);
    catch
        cacheSpikeRaster(expParms,options);
        r = load(cached_fn);
    end    

end