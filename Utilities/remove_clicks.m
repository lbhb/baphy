function w_clean = remove_clicks(w, max_threshold, verbose)

if ~exist('max_threshold', 'var')
   max_threshold=15;
end
if ~exist('verbose', 'var')
   verbose=0;
end

w_clean = w;

% log compress everything > 67% of max
crossover=0.67 * max_threshold;
ii = (w>crossover);
w_clean(ii)=crossover+log(w_clean(ii)-crossover+1);
jj = (w<-crossover);
w_clean(jj)=-crossover-log(-w_clean(jj)-crossover+1);

if verbose
   fprintf('bins compressed down: %d up: %d max %.2f-->%.2f\n',...
      sum(ii),sum(jj),max(abs(w)),max(abs(w_clean)));
end
