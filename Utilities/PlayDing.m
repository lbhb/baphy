function PlayDing(hObject, eventdata,HW)
%[globalparams, quit_baphy] = BaphyMainGui;
%global globalparams
%[HW, globalparams] = InitializeHW(globalparams);
%fn='U:\luke\Projects\SPS\MarmosetVocalizations\trill\typeTRILL_d140903_t144118_207.wav';
%fn='U:\luke\Projects\SPS\MarmosetVocalizations\trill\typeTRILL_d150205_t094657_227.wav';

fn='C:\Windows\Media\ding.wav';

[y,fs]=audioread(fn);
%y(1:3.7e4)=[];
if size(y,2)==1
    y(:,2)=y(:,1);
end
intensity=75;
y=resample(y,HW.params.fsAO,fs);
attend_db=80-intensity-HW.SoftwareAttendB;
level_scale=10.^(-attend_db./20);
y=y./max(y(:)).*level_scale;
IOStopSound(HW)
IOStartSound(HW,[y;zeros(20*HW.params.fsAO,HW.AO.NumChannels)]);
pause(2);
IOStopSound(HW)