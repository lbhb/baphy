%[lfpout,ltrialidx]=evplfp(evpfile,lfpchans,trials);
%
function [lfpout,ltrialidx,lfpfs,spikefs]=evplfp(evpfile,lfpchans,trials,options)

global USECOMMONREFERENCE

if isempty(USECOMMONREFERENCE) || ~USECOMMONREFERENCE,
   commstr='.NOCOM';
else
   commstr='';
end

[pp,bb,ee]=fileparts(evpfile);
if(isfield(options,'runinfo'))
    EVPVersion=options.runinfo.evpv;
    [~,bb,~]=fileparts(options.globalparams.mfilename);
else
    Pos = find(bb=='.');
    switch length(Pos) % Stephen Style Fix for EVP detection :)
        case 2; EVPVersion = 5;
        case 0; EVPVersion = 4;
        otherwise error('Unknown EVP Version');
    end
end
switch EVPVersion
  case {5 6}; 
      ri=strfind(pp,'raw');
      if ~isempty(ri)
          pp = pp(1:ri-2);
      end
  otherwise
end

if(~isfield(options,'runinfo'))
    options.runinfo=rawgetinfo(evpfile,[],EVPVersion);
end
if any(options.runinfo.evpv==5:6),
    options.runinfo.lfpfs=options.rasterfs;
end
options.lfp_filter_freqs=getparm(options,'lfp_filter_freqs',[]);


lfpout=[];
for lidx=1:length(lfpchans),
   
   if ~isempty(pp)
      pp=[pp filesep];
   end
   cachefile=[pp 'tmp' filesep bb '.chan' ...
              num2str(lfpchans(lidx)) '.fs' num2str(options.rasterfs) ...
              commstr '.lfp.mat'];
   cachefile=strrep(cachefile,[filesep 'raw' filesep],filesep);
   
   fprintf('evplfp: cache file=%s\n',cachefile);
   
   if ~options.recache && exist(cachefile,'file'),
      data=load(cachefile);
      if options.runinfo.trialcount>=max(trials),
%                       bHumbug=[0.995386247699319  -5.972013278489225  14.929576915653460  -19.905899769726052  14.929576915653460  -5.972013278489225  0.995386247699319 ];
%         aHumbug = [ 1.000000000000000  -5.990446012819222  14.952579842917430  -19.905857198474035  14.906552701679249  -5.953623115411301  0.990793782108932  ];
%          LHumbug = length(bHumbug)-1;
%      Raw=double(data.rl)'; 
% % IVHumbug = zeros(LHumbug,size(Raw,2)); 
%         [Raw] = filter(bHumbug,aHumbug,Raw);
%         lfpout(:,lidx)=Raw;
          lfpout(:,lidx)=data.rl;
          ltrialidx=data.ltrialidx;

      end
   end
   
   if options.recache || ~exist(cachefile,'file') || options.runinfo.trialcount<max(trials),
      fprintf('not found. generating from evp...\n');
      
      [~,~,~,~,rl,ltrialidx]=evpread(evpfile,'spikeelecs',[],'lfpelecs',lfpchans(lidx),'SRlfp',options.rasterfs,'runinfo',options.runinfo,'globalparams',options.globalparams,'lfp_filter_freqs',options.lfp_filter_freqs);
      % downsample prior to saving cache file
      if isfield(options,'rasterfs') && options.rasterfs<options.runinfo.lfpfs,
         ltrialidxnew=zeros(size(ltrialidx));
         ltrialidx=cat(1,ltrialidx,length(rl)+1);
         rlnew=[];
         stepsize=options.runinfo.lfpfs./options.rasterfs;
         smfilt=ones(round(stepsize),1);
         for ll=1:length(ltrialidxnew),
            ltrialidxnew(ll)=length(rlnew)+1;
            tl=rconv2(rl(ltrialidx(ll):(ltrialidx(ll+1)-1)),smfilt);
            rlnew=cat(1,rlnew,tl(round((stepsize/2):stepsize:length(tl))));
         end
         rl=rlnew;
         ltrialidx=ltrialidxnew;
      end
      lfpout(:,lidx)=rl;
      
      if isempty(findstr(computer,'PCWIN')),
          w=unix(['mkdir -p ' pp filesep 'tmp']);
      elseif ~exist([pp filesep 'tmp'],'dir'),
          mkdir(pp,'tmp');
      end
      save(cachefile,'rl','ltrialidx');
   end
end

% keep only requested trials
if exist('trials','var') && ~isempty(trials) && length(trials)<length(ltrialidx),
   keepidx=zeros(length(lfpout),1);
   ltrialidx0=zeros(length(trials),1);
   for tt=1:length(trials),
      ltrialidx0(tt)=sum(keepidx)+1;
      
      if trials(tt)<options.runinfo.trialcount,
         keepidx(ltrialidx(trials(tt)):(ltrialidx(trials(tt)+1)-1))=1;
      else
         keepidx(ltrialidx(trials(tt)):end)=1;
      end
   end
   lfpout=lfpout(find(keepidx),:);
   ltrialidx=ltrialidx0;
end

lfpfs=options.runinfo.lfpfs;
spikefs=options.runinfo.spikefs;