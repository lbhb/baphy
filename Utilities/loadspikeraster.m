function [r,tags,trialset,exptevents,sortextras,options]=...
  loadspikeraster(spkfile,channel,unit,rasterfs,includeprestim,tag_masks,psthonly,sorter)
% function [r,tags,trialset,exptevents,sortextras]=loadspikeraster(spkfile,options)
%
% load spike raster from sorted spike (.spk.mat) file
%
% inputs:
%  spkfile - name of .spk.mat file generated using meska
%
%  options - structure can contain the following fields:
%   channel - electrode channel (default 1)
%   unit - unit number (default 1)
%   rasterfs in Hz (default 1000)
%   includeprestim - (0 or 1, default=0) raster includes silent period before stimulus onset 
%                    alterative: includeprestim=[pre post]. force pre- and
%                    post-sec duration silences
%   tag_masks - cell array of strings to filter tags, eg,
%             {'torc','reference'} or {'target'} or {'Stim*'}.  AND logic.  default ={}
%   psthonly - shape of r (default -1, see below)
%   sorter - preferentially load spikes sorted by sorter.  if not
%            sorted by sorter, just take primary sorting
%   lfpclean - [0] if 1, remove MSE prediction of spikes (single
%              trial) predicted by LFP
%   includeincorrect - if 1, load all trials, not just correct (default 0)
%   runclass - if set, load only data for runclass when multiple runclasses
%              in file
%   trialrange - vector of trial numbers to include (dovetails with
%                sCellFile.good_trials)
%
% returns:
%   r is (psthonly==-1) time X nsweeps X nrec matrix
%        (psthonly==0) time*nrec X nsweeps X matrix
%        (psthonly==1) time*nrec X 1 vector, averaged nsweeps
%   tags (if baphy_fmt, the event name assocated with each rec)
%
% TO DO : check logic for choosing most recent primary sort!  Currently
%         just taking the first entry in the sorter list!!!
%
% old syntax (before 2007-10-03). still backward compatible:
% function [r,tags,trialset,exptevents]=loadspikeraster(spkfile,channel,unit,rasterfs,includeprestim,tag_masks,psthonly,sorter)
%    (options variable should now contain all the parameters)
%
% created SVD 2005-08-12
% modified SVD 2006-02-25  -- baphy_fmt support.  arbitrary trial length,
%                             use event tags for unwrapping into raster
% modified SVD 2006-08-11  -- added tag_mask event tag filter.
% modified SVD 2007-10-03  -- changed parameter input form.
% modified CRH 2018-01-05  -- generate a cache of spike rasters 

ENABLE_CACHING=0;

trialset = [ ];
% the user has passed the mfilename, parse it here.
if iscell(spkfile)   mfile = spkfile{2}; spkfile = spkfile{1};
else                   mfile = []; end

%% PARSE PARAMETERS
if ~exist('channel','var')
    options=[];
    options.channel=1;
    stimidx=[];
else
    options=channel;
end

if isstruct(channel),
   options=channel;
   options.useOEPspikes=getparm(options,'useOEPspikes',0);
   channel=getparm(options,'channel',1);
   unit=getparm(options,'unit',1);
   rasterfs=getparm(options,'rasterfs',1000);
   includeprestim=getparm(options,'includeprestim',0);
   if ~isfield(options,'tag_masks'),
      tag_masks={};
   elseif isnumeric(options.tag_masks),
      psthonly=options.tag_masks;
      tag_masks={};
   else
      tag_masks=options.tag_masks;
   end
   psthonly=getparm(options,'psthonly',-1);
   sorter=getparm(options,'sorter','');
   includeincorrect=getparm(options,'includeincorrect',0);
   analoglicktrace=getparm(options,'analoglicktrace',0);
   mua=getparm(options,'mua',0);
   lfpclean=getparm(options,'lfpclean',0);
   stimidx=getparm(options,'stimidx',[]);
   spikeshape=getparm(options,'spikeshape',0);
   input_sortidx=getparm(options,'sortidx',[]);
   ask_to_copy_if_file_not_found=getparm(options,'ask_to_copy_if_file_not_found',1);
   runinfo=getparm(options,'runinfo',[]);

else
   options=[];
   if ~exist('channel','var')              channel=1;    end
   if ~exist('unit','var')                    unit=1;         end
   if ~exist('rasterfs','var')              rasterfs=1000;  end
   if ~exist('includeprestim','var')    includeprestim=0;    end
   if ~exist('tag_masks','var')          tag_masks={}; 
   elseif isnumeric(tag_masks)        psthonly=tag_masks; tag_masks={}; end
   if ~exist('psthonly','var')             psthonly=-1;   end
   if ~exist('sorter','var')                 sorter='';    end
   if ~exist('spikeshape','var')         spikeshape = 0; end
   if ~exist('analoglicktrace','var')         analoglicktrace = 0; end
   includeincorrect=0;  mua=0;  lfpclean=0; stimidx=[];
   input_sortidx=[];
   ask_to_copy_if_file_not_found=1;
end

fprintf('%s: loading %s chan=%d unit=%d rasterfs=%d Hz\n',...
        mfilename,(spkfile),channel,unit,rasterfs);

% ADAPT FILENAME
if options.useOEPspikes
    %% FROM OEP
    mfile = spkfile;
else
    if ~strcmp(spkfile(end-3:end),'.mat')   spkfile=[spkfile '.mat']; end
    spkfile = LF_copyForSeil(spkfile,ask_to_copy_if_file_not_found);
    if(isempty(spkfile))
        r=[];tags=[];trialset=[];exptevents=[];sortextras=[];options=[];
        return
    end
end

%% Check to see if cached file exists
% did user input path, or are they in current working directory of cache
% files?

if ENABLE_CACHING,
   if length(strsplit(spkfile,filesep))>1
      [filepath,name,~] = fileparts(spkfile);
      [~,fn,~]=fileparts(name);
      root_dir_for_cache=strcat(filepath, filesep,'cache');
      if ~(exist(root_dir_for_cache)==7)
         mkdir(root_dir_for_cache)
      end
      
      % This shouldn't have to run, just in case cache was created without
      % being world writeable
      if isunix && unix(['mkdir ', strcat(root_dir_for_cache,'/test')])>0
         if exist(strcat(root_dir_for_cache,'/test'))==7
            unix(['rm -r ', strcat(root_dir_for_cache,'/test')]);  % remove test directory if it exists
         end
         status = unix(['sudo chmod 666 -R ', root_dir_for_cache]);
         if ~status==0
            disp('Might be permissions issues...')
         end
      elseif isunix && exist(strcat(root_dir_for_cache,'/test'))==7
         unix(['rm -r ', strcat(root_dir_for_cache,'/test')]);  % remove test directory if it exists
      end
      
   else
      current_folder = pwd;
      root_dir_for_cache=strcat(current_folder,filesep,'cache');
      
      if ~(exist(root_dir_for_cache)==7)
         mkdir(root_dir_for_cache)
      end
      
      if isunix   % Make the cache folder world writeable to solve permissions issues
         status = unix(['chmod 777 -R ', root_dir_for_cache]);
         if ~status==0
            disp('Might be permissions issues - need to be sudo to run chmod?')
         end
      end
   end
   
   [~,name,~] = fileparts(spkfile);
   [~,fn,~]=fileparts(name);
   
   % name cache file
   if isfield(options,'runclass')
      run = strcat('run-',options.runclass);
   else
      run = 'run-all';
   end

   if length(includeprestim)==1 && includeprestim>0
      prestim = strcat('prestim-',num2str(includeprestim));
   elseif length(includeprestim)>1
      prestim=strcat('prestim-',strrep(strrep(strrep(mat2str(includeprestim),' ','-'),'[',''),']',''));
   else
      prestim=strcat('prestim-none');
   end
   
   if isempty(tag_masks)
      tm = 'tags-default';
   else
      tm = strrep(strcat('tags-',tag_masks{:}),'*','X');  % replace asterik with 'X'
   end
   
   if includeincorrect==0
      ic = 'correctTrials';
   else
      ic='allTrials';
   end
   
   if length(num2str(channel))==1
      ch_str=strcat('0',num2str(channel));
   else
      ch_str=num2str(channel);
   end
   
   
   cache_fn = strcat(fn, '_ch', ch_str, '-', num2str(unit), '_fs',num2str(rasterfs),'_',tm,'_',run,'_',prestim,'_',ic,'_psth',num2str(psthonly));
   
   
   % check to see if already exists, load it if it does
   abs_path_to_file = strcat(root_dir_for_cache,filesep,cache_fn,'.mat');
   RELOAD = 1;
   options.cachefile=abs_path_to_file;

   if exist(abs_path_to_file,'file')
      % load from cache
      
      % if nargout > 4, run as normal. Cache only includes r:exptevents and
      % excludes sortextras and options
      if nargout>4
         RELOAD=1;   % the last nargs out aren't cached
      else
         disp('loadspikeraster: loading from cache')
         out = load(abs_path_to_file);
         RELOAD=0;
         
         r = out.r;
         tags = out.tags;
         trialset = out.trialset;
         exptevents=out.exptevents;
      end
      
   end
   
   % if make ==1 then we need to remake the cache file because some or all of
   % it is missing
   if ~RELOAD
      return
   end
end

% otherwise run the code below to generate spike raster  

%% LOAD SPIKES
if options.useOEPspikes
    %% FROM OEP
    sortextras=[];
    MF=LoadMFile(mfile);
    exptevents=MF.exptevents;
    if(isempty(runinfo))
        runinfo=rawgetinfo(mfile,MF.globalparams);
        options.runinfo=runinfo;
    end
    if isempty(options.runinfo.json_file_spikes)
        error('No spike data found. Were spikes saved in OEP using the spike detector module?')
    end
    if isempty(options.runinfo.sorted_spikes_channel_map)
        channel_unmapped = options.channel;
    else
        % sorted_spikes_channel_map spec: map(y)=x where x is original
        % channel and y is new channel.
        matchi = find(options.runinfo.sorted_spikes_channel_map == options.channel);
        if length(matchi)==1
            fprintf('Mapping channel  %d from Spike recording node channel %d\n',options.channel,matchi)
            channel_unmapped = matchi;
        else
            error('No match for channel %d, was this channel saved in OEP (including channel mapping)? Available channels are: [%s]',options.channel,num2str(options.runinfo.sorted_spikes_channel_map))
        end
    end
    if isfield(options.runinfo,'oespike_rec') && options.runinfo.oespike_rec>0
        % OE v0.6 loaded already
        OEPspiketimes = options.runinfo.recordings(options.runinfo.oespike_rec).spikes(channel_unmapped).sample_numbers;
    else
        dat=load_open_ephys_binary(options.runinfo.json_file_spikes, 'spikes', 1);
        OEPspiketimes = dat.Timestamps(dat.ElectrodeIndexes==channel_unmapped);
    end
    switch options.runinfo.datatype
        case 'OEP'
            error('Not tested (copied from evpread line 470)')
            [EVdata, EVinfo,EVtimestamps] = load_open_ephys_data_faster([P.globalparams.rawfilename,filesep,'all_channels.events']);
            trial_onsets=EVtimestamps(EVinfo.eventId==1&EVinfo.eventType==3); % in samples
            trial_offsets=EVtimestamps(EVinfo.eventId==0&EVinfo.eventType==3); % in samples
            
            % Get TCP message events
            % baphyTcpOnsets = EVtimestamps(EVinfo.eventType==5); LAS: old way, now OEP is sending more TCP pulses so filter out only the TrialStart ones
            fid=fopen([P.globalparams.rawfilename,filesep,'messages.events']);
            g = textscan(fid,'%s','delimiter','\n');
            MSGtimestamp=zeros(size(g{1}));
            MSGmsg=cell(size(g{1}));
            is_TrialStart=false(size(g{1}));
            for ii=1:length(g{1})
                spaces=strfind(g{1}{ii},' ');
                MSGtimestamp(ii)=str2double(g{1}{ii}(1:spaces(1)-1));
                MSGmsg{ii}=g{1}{ii}(spaces(1)+1:end);
                is_TrialStart(ii)=length(MSGmsg{ii})>11 && strcmp(MSGmsg{ii}(1:11),'TrialStart ');
            end
            baphyTcpOnsets=MSGtimestamp(is_TrialStart);
        case 'binary'
            %Get trial onsets from events in continuous folder (events in spikes folder are identical)
            EVdata = load_open_ephys_binary(options.runinfo.json_file,'events',options.runinfo.event_ind_TTL);
            trial_onsets=EVdata.Timestamps(EVdata.Data==1); % in samples
            trial_offsets=EVdata.Timestamps(EVdata.Data==-1); % in samples
            % Get TCP message events
            if 0
                EVdata = load_open_ephys_binary(options.runinfo.json_file,'events',options.runinfo.event_ind_TCP);
                %TBD, readNPY can't read text so this doesn't work.
            else
                baphyTcpOnsets = NaN;
            end
        case 'Binary'  % OE 0.6
            spike_rec = options.runinfo.spike_rec;
            trial_onsets = options.runinfo.recordings(spike_rec).trial_onsets;
            trial_offsets = options.runinfo.recordings(spike_rec).trial_offsets;

            baphyTcpOnsets = NaN;
    end
    
    %assumes requested trials are continuous!
    if(length(trial_onsets)==length(trial_offsets)+1)
        %last trial lacks an offset TTL. Likely an error in data collection.
        trial_offsets(end+1)=trial_onsets(end)+mode(trial_offsets-trial_onsets(1:end-1));
        missing_last_offset=true;
    else
        missing_last_offset=false;
    end
    
    %% simple error checking: do the trial onset times logged in TCP events match the TTL pulses?
    
    if ~isequaln(baphyTcpOnsets, NaN)
        evcountdiff=length(baphyTcpOnsets) - length(trial_onsets);
        dd=trial_onsets(1:end+evcountdiff) - baphyTcpOnsets;
        if evcountdiff<0
            disp('');
            warning('more TTL pulse trial_onsets than TCP events!')
            %keyboard % MLE anotated out the keyboard, why is this here?
            dd=trial_onsets(1:end+evcountdiff) - baphyTcpOnsets;
            
            if 0
                %LAS for figuring out why these are so offset!
                shift_range=-10:10;
                for ii=1:length(shift_range)
                    indsA=(1:length(baphyTcpOnsets))+shift_range(ii);
                    indsB=1:length(baphyTcpOnsets);
                    keep = indsA<=length(trial_onsets) & indsA>0;
                    dd=trial_onsets(indsA(keep)) - baphyTcpOnsets(indsB(keep));
                    mindf(ii)=min(dd);
                    maxdf(ii)=max(dd);
                    meandf(ii)=mean(dd);
                end
                [~,ii]=min(abs(meandf));
                [~,ii]=min(abs(maxdf));
                maxdf(ii+[0 1])/options.SR
            end
        elseif evcountdiff>0
            warning('more TCP events than TTL pulse trial_onsets!')
            dd=trial_onsets - baphyTcpOnsets((evcountdiff+1):end);
        end
        
        
        if exist('dd', 'var') && max(dd) > options.SR/10
            disp('');
            warning('trigger to TCP offset > 100 ms!');
            %keyboard %CRH got rid of this keyboard. Why is this here?
        end
    end
    spikefs=options.runinfo.spikefs;
else
    %% FROM SPIKEFILE
    spikeinfop = load(spkfile);
    exptevents=spikeinfop.exptevents;
    %% SPECIAL CODE FOR MODEL CELLS
    if isfield(spikeinfop,'sortextras') && length(spikeinfop.sortextras)>=channel,
       sortextras=spikeinfop.sortextras{channel};
    else
       sortextras=[];
    end
    if psthonly==1 && isfield(spikeinfop,'sortextras') && ...
          isfield(spikeinfop.sortextras{1},'psth'),
       r=spikeinfop.sortextras{1}.psth;
       return
    end
    mf=rasterfs;          % output samples per sec
    mfOld=spikeinfop.rate;  % input samples per sec
    spikefs=spikeinfop.rate;
end



%% OLD FORMAT SPK FILE
if ~options.useOEPspikes && (~isfield(spikeinfop,'baphy_fmt') || ~spikeinfop.baphy_fmt)
    try
        spikeinfop.sortinfo{channel}{1}(unit);
    catch
        warning(sprintf('Channel %d, unit %d does not exist',channel,unit))
        [r,tags,trialset,exptevents,sortextras]=deal([]);
        return
    end
    if ~isempty(mfile)
        % if mfile is available, pass it on
        spkfile = {spkfile,mfile};
    end
    disp('running loadspikeraster_oldfmt');
    if length(includeprestim)==1,
       adjustps=includeprestim;
    else
       adjustps=0;
    end
    r=loadspikeraster_oldfmt(spkfile,channel,unit,rasterfs,[1 1]-adjustps,psthonly);
    tags={};
    if length(includeprestim)>1,
       r=cat(1,zeros(round(includeprestim(1).*rasterfs),size(r,2),size(r,3)),...
             r,...
             zeros(round(includeprestim(2).*rasterfs),size(r,2),size(r,3)));
    end
    trialset=[];
    exptevents=[];
    return
end

if strfind(lower(spkfile),'mts')              %add by PBY at July 26, 2007
    if ~isempty(tag_masks) && length(tag_masks{1})>=16 && strcmp(tag_masks{1}(1:16),'SPECIAL-COLLAPSE') 
        exptevents=baphy_mts_evt_merge(exptevents,1);  %merge TORC and TS togather
    else
        exptevents=baphy_mts_evt_merge(exptevents);    %do not merge with TORC 
    end
end

if ~options.useOEPspikes
    if isempty(spikeinfop.sortinfo{channel})
        warning([mfilename,': channel>channels!']);
        r=[];
        tags={};
        return
    end
    if isempty(sorter),
        sortidx=1;
    else
        ii=1;
        sortidx=1;
        for ii=length(spikeinfop.sortinfo{channel}):-1:1,
            if strcmpi(sorter,spikeinfop.sortinfo{channel}{ii}(1).sorter)
                sortidx=ii;
            end
        end
        fprintf('sortidx=%d, sorter=%s\n',...
            sortidx,spikeinfop.sortinfo{channel}{sortidx}(1).sorter);
    end
    if(~isempty(input_sortidx))
        fprintf('sortidx=%d was requested, setting sortidx to %d\n',input_sortidx,input_sortidx)
        sortidx=input_sortidx;
    end
    if(sortidx>1)
        sortextras=spikeinfop.sortextras_nonprimary{channel}{sortidx};
    end
    try
        spikeinfop.sortinfo{channel}{sortidx}(unit);
    catch
        warning(sprintf('Channel %d, unit %d does not exist',channel,unit))
        [r,tags,trialset,exptevents,sortextras]=deal([]);
        return
    end
    if(length(spikeinfop.sortinfo{channel}{sortidx}(unit).comments)>=18)
        if(strcmp(spikeinfop.sortinfo{channel}{sortidx}(unit).comments(1:18),'Sorted by KiloSort'))
            type_st=strfind(spikeinfop.sortinfo{channel}{sortidx}(unit).comments,'Type is:');
            if(isempty(type_st))
                type_str='';
            else
                type_str=spikeinfop.sortinfo{channel}{sortidx}(unit).comments(type_st+9:end);
            end
            fprintf(['KiloSort IDs: ',num2str(spikeinfop.sortinfo{channel}{sortidx}(unit).sortparameters.KiloSort_ids'),': ',type_str,'\n'])
            options.KSIDs=spikeinfop.sortinfo{channel}{sortidx}(unit).sortparameters.KiloSort_ids';
        end
    end
    
    units=length(spikeinfop.sortinfo{channel}{sortidx});
    
    if unit>units || isempty(spikeinfop.sortinfo{channel}{sortidx}(unit).Template),
        warning([mfilename,': unit>units!']);
        r=[ ];
        tags={};
        return
    end
    
    %% GET SPIKE TIMES
    if mua==1,
        disp('loading all threshold events');
        
        rawSpikes=[];
        tt=spikeinfop.sortextras{channel}.spiketimes;
        ts=[spikeinfop.sortextras{channel}.trialstartidx; max(tt)+1];
        
        for trialidx=1:(length(ts)-1),
            ttt=tt(find(tt>=ts(trialidx) & tt<ts(trialidx+1)))-ts(trialidx)+1;
            rawSpikes=cat(2,rawSpikes,[ones(1,length(ttt)).*trialidx;ttt']);
        end
    elseif mua==2,
        disp('loading all sorted spikes');
        rawSpikes=[];
        for uu=1:length(spikeinfop.sortinfo{channel}{sortidx}),
            rawSpikes=cat(2,rawSpikes,...
                spikeinfop.sortinfo{channel}{sortidx}(uu).unitSpikes);
        end
    else
        rawSpikes = ...
            spikeinfop.sortinfo{channel}{sortidx}(unit).unitSpikes;
    end
    
    
    %% GET SPIKE SHAPES
    if spikeshape
        SpikeShape = spikeinfop.sortinfo{channel}{sortidx}(unit).Template(:,unit);
        sortextras.SpikeShape = SpikeShape;
    end
    
    cellid=basename(spkfile);
    if strcmp(cellid(1:3),'c02') || strcmp(cellid(1:3),'c03') || ...
            strcmpi(cellid(1),'J') ||...
            (strcmpi(cellid(1),'o') && ~strcmp(cellid(1:3),'oni') && ~strcmp(cellid(1:3),'oys')),
        disp('old file: shifting responses forward 15 ms');
        rawSpikes(2,:)=rawSpikes(2,:)+mfOld.*0.015;
        rawSpikes=rawSpikes(:,find(rawSpikes(2,:)>0));
    end
end

% guess evpfile name in case we want to load licks
if options.useOEPspikes
    evpfile='';
else
    [evpb,evpp]=basename(spikeinfop.fname);
    ff=findstr(evpp,spkfile);
    if ~isempty(ff),
        evpfile=[spkfile(1:(ff-1)),spikeinfop.fname];
        evpfile=strrep(evpfile,'.m.evp','.evp');
    else
        evpfile='';
    end
end

%figure out run class
rc=strsep(basename(spkfile),'.',1);
rc=strsep(rc{1},'_');
using_rc_subset=0;
if length(unique(rc))<=3,
   runclass=rc{end};
   options.runclass=runclass;
else
    runclassset=rc(3:end);
    options.runclass=getparm(options,'runclass',runclassset{1});
    rcmatch=min(find(strcmp(runclassset,options.runclass)));
    if(isempty(rcmatch))
        runclass=options.runclass;
    else
        runclass=runclassset{rcmatch};
        if isempty(tag_masks) || ...
                (length(tag_masks)==1 && strcmpi(tag_masks{1},'Ref')),
            using_rc_subset=1;
            tag_masks{1}=sprintf('Reference%d',rcmatch);
            fprintf('Fixing tag_masks to %s for runclass=%s\n',tag_masks{1},runclass);
        end
    end
end
%tag_masks{1}='Reference1';

% figure out the tag for each different stimulus 
% 2013-08-07 -- SVD moved to separate function so that it can be
% shared with loadspikeraster.m
parmfile=strrep(spkfile,'/sorted/','/');
parmfile=strrep(parmfile,'spk.mat','m');

[eventtime,evtrials,Note,eventtimeoff,tags]= ...
    loadeventfinder(exptevents,tag_masks,includeprestim,...
    runclass,evpfile,parmfile);
 
% define output raster matrix
referencecount=length(tags);
r=nan*zeros(1,1,referencecount);

% figure out the time that each trial started and stopped
[starttimes,starttrials]=evtimes(exptevents,'TRIALSTART*');
[stoptimes,stoptrials]=evtimes(exptevents,'TRIALSTOP');
[shockstart,shocktrials,sn,shockstop]=evtimes(exptevents,'BEHAVIOR,SHOCKON');

if isfield(options,'tag_masks') && strcmpi(options.tag_masks{1},'SPECIAL-TRIAL'),
    for ii=1:length(eventtimeoff)
        if options.useOEPspikes
            trial_mask = OEPspiketimes>trial_onsets(ii);
            if ii<length(eventtimeoff)
                trial_mask = trial_mask & OEPspiketimes<trial_onsets(ii+1);
            else
                %last trial, do nothing (use all spikes at end)
            end
            last_spike_time = double(max(OEPspiketimes(trial_mask)) - trial_onsets(ii));
            truelen=max([0 last_spike_time])/spikefs;
        else
            truelen=max([0 rawSpikes(2,rawSpikes(1,:)==ii)])/spikefs;
        end
        eventtimeoff(ii)=max(eventtimeoff(ii),truelen);
        stoptimes(ii)=eventtimeoff(ii);
    end
end

trialcount=max(starttrials);
if analoglicktrace,
    % generate parameter filename;
    if(~isempty(mfile))
        LoadMFile(mfile);
    else
        LoadMFile(parmfile);
    end
    
    outcomes={exptparams.Performance(1:(end-1)).ThisTrial};
    hittrials=find(strcmp(outcomes,'Hit'))';
else
    [~,hittrials]=evtimes(exptevents,'OUTCOME,MATCH');
    if isempty(hittrials),
        [~,hittrials]=evtimes(exptevents,'BEHAVIOR,PUMPON*');
    end
end


if isfield(options,'trialfrac'),
   if length(options.trialfrac)==2,
      trialrange=round(trialcount.*options.trialfrac(1)):...
          round(trialcount.*options.trialfrac(2));
      td=diff(options.trialfrac);
   else
      trialrange=1:round(trialcount.*options.trialfrac);
      td=options.trialfrac;
   end
   fprintf('trial frac=%.2f, keeping %d/%d trials\n',...
           td,trialrange(end),trialcount);
elseif isfield(options,'trialrange') && ~includeincorrect && ~isempty(hittrials),
   trialrange=options.trialrange(ismember(options.trialrange,hittrials));
elseif isfield(options,'trialrange'),
   trialrange=options.trialrange;
elseif ~includeincorrect && ~isempty(hittrials),
   %disp('including only correct trials.');
   trialrange=hittrials(:)';
   
elseif ~isempty(hittrials),
   lasttrial=trialcount; % hittrials(round(length(hittrials).*0.85));
   if lasttrial<60,
      lasttrial=min(60,trialcount);
   end
   trialrange=1:lasttrial;
   fprintf('stopping at trial %d/%d\n',lasttrial,trialcount);
else
   trialrange=1:trialcount;
end

if lfpclean
   % fit LFP-spike filter here
   
    % guess evpfile name to load LFP
    [evpb,evpp]=basename(spikeinfop.fname);
    evpp=strrep(evpp,'\',filesep);
    
    ff=findstr(evpp,spkfile);
    if ~isempty(ff),
       evpfile=[spkfile(1:(ff-1)),strrep(spikeinfop.fname,'\',filesep)];
    else
       error('evpfile not known');
    end
    %[spikechancount,auxchancount,trialcount,spikefs,...
    % auxfs,lfpchancount,lfpfs]=evpgetinfo(evpfile);
    %[dum1,dum2,dum3,dum4,rL,ltrialidx]=...
    %    evpread(evpfile,[],[],[],channel);
    
    [rL,ltrialidx,lfpfs,spikefs]=evplfp(evpfile,channel);
    ltrialidx=cat(1,ltrialidx,length(rL)+1);
    
    lr=[];
    sr=[];
    
    %for each trial
    for ii=1:trialcount,

       lstart=ltrialidx(ii);
       lstop=ltrialidx(ii+1)-1;
       
       lthistrial=resample(rL(lstart:lstop),rasterfs.*100,lfpfs.*100);
       expectedspikebins=length(lthistrial)./rasterfs.*spikefs;
       thistrialidx=find(rawSpikes(1,:)==ii);
       if ~isempty(thistrialidx),
          sthistrial=histc(rawSpikes(2,thistrialidx),0:spikefs/rasterfs:...
                       (expectedspikebins+spikefs/rasterfs))';
       else
          sthistrial=zeros(size(0:spikefs/rasterfs:(expectedspikebins+spikefs/rasterfs)))';
       end
       
       mm=min(length(sthistrial),length(lthistrial));
       cleantrialidx(ii)=length(lr)+1;
       lr = cat(1,lr,lthistrial(1:mm));
       sr = cat(1,sr,sthistrial(1:mm));
    end
    cleantrialidx(trialcount+1)=length(lr)+1;
    
    IR_LEN=20;
    fprintf('computing cross-corr...\n');
    bootcount=16;
    bootstep=length(lr)./bootcount;
    xc=zeros(IR_LEN.*2+1,bootcount);
    ac=zeros(IR_LEN.*2+1,bootcount);
    for bb=1:bootcount,
       bidx=[1:round((bb-1).*bootstep) round(bb.*bootstep+1):length(lr)];
       
       xc(:,bb)=xcov(lr(bidx),sr(bidx),IR_LEN,'unbiased');
       ac(:,bb)=xcov(lr(bidx),lr(bidx),IR_LEN,'unbiased');
    end
    
    fft_xc = fft(xc);
    fft_ac = fft(ac);
    
    h_fft = fft_xc./fft_ac;
    mm=abs(mean(h_fft,2));
    ee=abs(std(h_fft,0,2)).*sqrt(bootcount-1);
    
    h_fft=mean(h_fft,2);
    
    sigrat=2;
    smd=mm./(ee+eps.*(ee==0)) ./ sigrat;

    smd=(1-smd.^(-2));
    smd=smd.*(smd>0);
    smd(find(isnan(smd)))=0;
    h_fft=h_fft.*smd;

    %h_fft(mm./ee<2,:)=0;
    
    h_re = fftshift(real(ifft(h_fft)));
    %h_re = h_re - mean(h_re);
    h = h_re .* hann(IR_LEN*2+1);
    
    out_spikes= conv2(lr, h,'same');
    
    if sum(abs(out_spikes))>0,
       
       gg=sr'*out_spikes./(out_spikes'*out_spikes);
       
       output_clean = sr - gg.*out_spikes;
    
       fprintf('lfp spike pred xc: %.3f\n',xcov(out_spikes,sr,0,'coeff'));
    else
       disp('no significant LFP-spike filter');
       output_clean=sr;
    end
    %keyboard
    
end


trialset=zeros(1,referencecount);
for trialidx=trialrange,
    starttime=starttimes(starttrials==trialidx);
    stoptime=stoptimes(stoptrials==trialidx);
    expectedspikebins=(stoptime-starttime)*spikefs;
    
    if options.useOEPspikes
        thistrialidx = find(OEPspiketimes>trial_onsets(trialidx) & OEPspiketimes<trial_offsets(trialidx));
        if isempty(OEPspiketimes(thistrialidx))
            spiketimes_this_trial=[];
        else
            spiketimes_this_trial = double(OEPspiketimes(thistrialidx) - trial_onsets(trialidx));
        end
    else
        if (size(rawSpikes,1)>0),
            thistrialidx=find(rawSpikes(1,:)==trialidx);
        else
            thistrialidx=[];
        end
        spiketimes_this_trial = rawSpikes(2,thistrialidx);
    end
    
    
    % bin whole trial into a raster
    if lfpclean,
       % remove activity predicted by LFP here
       raster=output_clean(cleantrialidx(trialidx):(cleantrialidx(trialidx+1)-1))';
    elseif isempty(thistrialidx),
       raster=zeros(size(0:spikefs/rasterfs:(expectedspikebins+spikefs/rasterfs)));
    else
       if 1 || rasterfs>=100,
          raster=histc(spiketimes_this_trial,0:spikefs/rasterfs:...
                       (expectedspikebins+spikefs/rasterfs));
       else
          %disabled svd 2010-04-20
          %smooth before downsample
          raster=histc(spiketimes_this_trial,0:spikefs/1000:...
                       (expectedspikebins+spikefs/1000));
          raster=resample(raster,rasterfs,1000);
       end
    end
    
    shockhappened=find(shocktrials==trialidx);
    for tt=1:length(shockhappened),
       shockidx=shockhappened(tt);
       fprintf('trial %d: nan-ing shock %.1f-%.1f sec\n',...
               trialidx,shockstart(shockidx),shockstop(shockidx)+0.15);
       raster(round(shockstart(shockidx).*rasterfs):...
              round((shockstop(shockidx)+0.15).*rasterfs))=nan;
    end
    
    % figure out the start and stop time of each event during the trial
    % and place the corresponding spikes in the raster.
    evidxthistrial=find(evtrials==trialidx);
    
    for evidx=evidxthistrial(:)',
        %bb=strsep(Note{evidx},',',1);
        %bb=strtrim(bb{2});
        bb=Note{evidx};
        % figure out which event slot this is
        refidx=find(strcmp(tags,bb));
        
        % if it doesn't match one of the expected tags then skip it (eg, a
        % target when we only want references)
        if ~isempty(refidx),
            repidx=min(find(sum(~isnan(r(:,:,refidx)))==0));
            if isempty(repidx),
                repidx=size(r,2)+1;
            end
            rlen=(eventtimeoff(evidx)-eventtime(evidx))*rasterfs;
            
            % make sure the raster matrix is big enough for the next event
            if size(r,1)<rlen | repidx>size(r,2) | refidx>size(r,3),
                r((size(r,1)+1):round(rlen),:,:)=nan;
                r(:,(size(r,2)+1):repidx,:)=nan;
            end
            
            startbin=round(eventtime(evidx)*rasterfs);
            stopbin=round(eventtimeoff(evidx)*rasterfs);
            if eventtime(evidx)<=0 && length(raster)<stopbin,
               rl=length(raster)-startbin;
               missingbins=-startbin;
               r(1:missingbins,repidx,refidx)=nan;
               r((missingbins+1):rl,repidx,refidx)=raster(1:end);
            elseif eventtime(evidx)<=0,
               missingbins=-startbin;
               r(1:missingbins,repidx,refidx)=nan;
               r((missingbins+1):(stopbin-startbin),repidx,refidx)=...
                   raster(1:stopbin);
            elseif length(raster)>=stopbin,
                % actually put the responses in the output raster matrix
                r(1:(stopbin-startbin),repidx,refidx)=raster((startbin+1):stopbin);
            else
               rl=length(raster)-startbin;
               r(1:rl,repidx,refidx)=raster((startbin+1):end);
            end
            trialset(repidx,refidx)=trialidx;
           
        end
    end
    drawnow;
end

if options.useOEPspikes
    StimTagNames=MF.exptparams.TrialObject.ReferenceHandle.Names;
    do_resort=1;
    filename=mfile;
else
    StimTagNames=spikeinfop.sortextras{channel}.StimTagNames;
    filename=spikeinfop.fname;
    do_resort=isfield(spikeinfop,'sortextras');%don't resort if sortextras isn't a field
end

[r,tags,trialset]=sort_raster(r,tags,trialset,tag_masks,StimTagNames,filename,using_rc_subset,do_resort);

if ~isempty(stimidx),
   r(:,:,setdiff(1:size(r,3),stimidx))=nan;
end
%Output detection threshold used for plots of raw data with spike times
%overlaid (raw_trace_plot.m)
if ~options.useOEPspikes
    if(length(spikeinfop.sortinfo{channel}{sortidx}(unit).comments)>=18&&strcmp(spikeinfop.sortinfo{channel}{sortidx}(unit).comments(1:18),'Sorted by KiloSort'))
        options.numChannels=sortextras.numChannels;
    else
        try
            options.detection_threshold=sortextras.sigma*sortextras.sigthreshold;
            options.sigthreshold=sortextras.sigthreshold;
            options.numChannels=sortextras.numChannels;
        catch err
            warning(err.message)
        end
    end
end
if psthonly==-1
   % do nothing?
   
elseif psthonly
   % time/record all in one long vector, averaged over reps
   r=nanmean(permute(r,[2 1 3]));
   r=r(:);
else
   % time/record X rep
   r=permute(r,[1 3 2]);
   r=reshape(r,size(r,1)*size(r,2),size(r,3));
end

if ENABLE_CACHING,
   % cache data (if nargout >4, only 1:4 will be cached)
   save(abs_path_to_file, 'r', 'tags', 'trialset','exptevents') %always save these four variables
   if isunix
      
      % Make the cache file world writeable in case it needs to be overwritten
      % 666 means read+write but not execute. mat files aren't executable
      s = unix(['chmod 666 ', abs_path_to_file]);
      
      if s
         s = unix(['sudo chmod 666 ', abs_path_to_file]);
      end
   end
end

return

% code for generating example stimulus waveform:
Ref=Torc;
Ref=set(Ref,'PreStimSilence',ifstr2num(exptparams.RefTarObject.ReferenceHandle.PreStimSilence));
Ref=set(Ref,'PosStimSilence',ifstr2num(exptparams.RefTarObject.ReferenceHandle.PosStimSilence));
Ref=set(Ref,'Duration',ifstr2num(exptparams.RefTarObject.ReferenceHandle.Duration));
Ref=set(Ref,'FrequencyRange',(exptparams.RefTarObject.ReferenceHandle.FrequencyRange));
Ref=set(Ref,'Rates',(exptparams.RefTarObject.ReferenceHandle.Rates));
Ref=ObjUpdate(Ref);
[w,stimfs]=waveform(Ref,10);
w=[zeros(stimfs*get(Ref,'PreStimSilence'),1);
    w;
    zeros(stimfs*get(Ref,'PosStimSilence'),1)];
wresamp=resample(w,spikefs,stimfs);



function spkfile = LF_copyForSeil(spkfile,ask_to_copy_if_file_not_found)

% special code for copying temp files over to seil cluster
ss_stat=onseil;
%fprintf('LF_copyForSeil: ss_stat=%d local copy exists=%d\n',...
%   [ss_stat exist(spkfile,'file')]);
if ss_stat && ~exist(spkfile,'file'),
    
    trin=spkfile;
    if ss_stat==1,
        disp('mapping file to seil');
        trout=strrep(trin,'/auto/data/','/homes/svd/data/');
    elseif ss_stat==2,
        trout=trin;
    end
    
    [bb,pp]=basename(trout);
    if strcmp(bb(1:5),'model') && ss_stat~=2,
        disp('model cell, forcing recopy of response');
        ['\rm ' trout]
        unix(['\rm ' trout]);
    end
    if ~exist(trout,'file'),
        if ask_to_copy_if_file_not_found
            fprintf('Trying to copy from umd\n');
            str = 'y'
            % str = input([trin ,' not found. Try to copy from umd?: '],'s');
            switch lower(str)
                case {'y','yes'}
                    if ss_stat==2,
                        disp('mapping file to OHSU');
                    end
                    unix(['mkdir -p ',pp]);
                    ['scp svd@bhangra.isr.umd.edu:',trin,' ',pp]
                    unix(['scp svd@bhangra.isr.umd.edu:',trin,' ',pp]);
                otherwise
                    spkfile=[];
                    return
            end
        end
    end
    spkfile=trout;
    
end


