function common_ref=read_common_ref(filename,options,meta)
global USECOMMONREFERENCE

%define default options
options.av_mode=getparm(options,'av_mode','median');
options.trials=getparm(options,'trials','ERROR');
options.channels=getparm(options,'channels','ERROR');
options.globalparams=getparm(options,'globalparams','ERROR');
options.rawfilename=getparm(options,'rawfilename','ERROR');
options.runinfo=getparm(options,'runinfo','ERROR');
use_cache=true;
options2=options;
rm_list={'mfilename','evpfilename','PupilFilename','tempevpfile','rawfilename'};
curr_fns=fieldnames(options2.globalparams);
rm_list(~ismember(rm_list,curr_fns))=[];
options2.globalparams=rmfield(options2.globalparams,rm_list);
options2=rmfield(options2,'rawfilename');
if isfield(options2.runinfo,'session')
    options2.runinfo = rmfield(options2.runinfo,'session');
end
[status,Data,failed_vars]=UTcached_file('load',filename,options2);

if(status && use_cache)
    common_ref=Data.common_ref;
    fprintf(['Read previously generated Common Reference from channels: ',num2str(options.channels) '\n'])
    return
end

fprintf(['Loading Common Reference from channels: ',num2str(options.channels) '\n'])

if(~all(diff(options.trials)==1))
    error('options.trials must be contiguous')
end
max_size_rall=1;%max size in Gb
L=meta.trial_offsets(options.trials(end))-meta.trial_onsets(options.trials(1))+1;
num_blocks=ceil(L*length(options.channels)*8/1e9/max_size_rall);
unassigned_trials=options.trials;
trial_starts=[];
trial_ends=[];
while(~isempty(unassigned_trials))
    if(isempty(trial_starts))
        trial_starts=1;
    else
        trial_starts=[trial_starts unassigned_trials(1)];
    end
    Ls=meta.trial_onsets(trial_starts(end):end)-meta.trial_onsets(trial_starts(end));
    this_num_blocks=ceil(Ls*length(options.channels)*8/1e9/max_size_rall);
    trial_ind=find(this_num_blocks>1,1);
    if(isempty(trial_ind))
        trial_ind=length(this_num_blocks);
    else
        trial_ind=trial_ind-2;
    end
    trial_ends=[trial_ends trial_starts(end)+trial_ind-1];
    unassigned_trials(ismember(unassigned_trials,trial_starts(end):trial_ends(end)))=[];
end


USECOMMONREFERENCE=false;% this should not be a global! But it is, so turn it off and back on. Change it someday?        
common_ref=nan(L,1);
for i=1:length(trial_starts)
    fprintf('Common average trial chunk %d/%d\n',i,length(trial_starts));
    
    R=evpread(options.rawfilename,'spikechans',options.channels,'trials',trial_starts(i):trial_ends(i),...
        'globalparams',options.globalparams,'filterstyle','none','runinfo',options.runinfo);
    
    if(i==length(trial_starts))
        inds=(meta.trial_onsets(trial_starts(i)):meta.trial_offsets(trial_ends(i)));
    else
        inds=(meta.trial_onsets(trial_starts(i)):meta.trial_onsets(trial_ends(i)+1)-1);
    end
    switch options.av_mode
        case 'median'
            common_ref(inds)=median(R.Spike,2);
        case 'mean'
            common_ref(inds)=mean(R.Spike,2);
        otherwise
            error(['options.av_mode:', options.av_mode,' not valid'])
    end
    clear R  
end
USECOMMONREFERENCE=true;

if isfield(options.runinfo,'session')
    options.runinfo = rmfield(options.runinfo,'session');
end
[status]=UTcached_file('save',filename,options,common_ref);