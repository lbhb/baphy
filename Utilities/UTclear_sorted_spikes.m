function UTclear_sorted_spikes(site)
% UTclear_sorted_spikes(site)
% Deletes all .spk.mat files and clears the celldB sorting database tables for site

a=questdlg(['Warning! You''re about to delete all .spk.mat files and clear the celldB sorting database tables for site ' site '. Continue?'],'Okay to Delete?','Continue','Cancel','Cancel');
if strcmp(a,'Cancel')
    fprintf('\n Cancelled')
    return
end

sql=['SELECT *',' FROM gDataRaw WHERE parmfile like "',site,'%" AND bad=0'];
dbopen;
S=mysql(sql);
status=dblink_purge_rawids([S.id]);

if status
    un_pth=unique({S.resppath});
    if length(un_pth)>1
        warning(['This site has multiple resppath folders in gDataRaw: ',un_pth{:},...
                 ' Deleting from the first one.'])
    end
    d = [un_pth{1} 'sorted'];
    files= dir([d, filesep, '*spk.mat']);
    %if ~isempty(files)
    %    files(1:2)=[];
    %end
    for i=1:length(files)
        savfile = [d filesep files(i).name];
        if endsWith(savfile,'spk.mat')
           file_exists=true;
           while file_exists
              lastwarn('')
              delete(savfile)
              [warnMsg, warnId] = lastwarn;
              if ~isempty(warnMsg) && strcmp(warnId,'MATLAB:DELETE:Permission')
                 a=questdlg(['Warning! Permission denied when trying to delete ' savfile '. Delete manually usuing sudo then Continue. Or Cancel saving.'],'Permission Denied','Continue','Cancel','Cancel');
                 if strcmp(a,'Cancel')
                    savfiles=[];
                    fprintf('\n Cancelled')
                    return
                 end
              else
                 file_exists = exist(savfile,'file');
              end
           end
        end
    end
end