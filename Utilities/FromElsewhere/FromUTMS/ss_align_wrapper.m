%run before line 355 in m_mespca, change XAXIS as needed
function [SPIKESET,XAXIS]=ss_align_wrapper(SPIKESET,EXTRAS,EVENTTIMES,FILEDATA)
if(FILEDATA(1).tetrode)
    spikes.waveforms=permute(reshape(SPIKESET,50,4,size(SPIKESET,2)),[3,1,2]);
else
    spikes.waveforms=SPIKESET';
end
if(EXTRAS.sigthreshold<0)
    spikes.waveforms=spikes.waveforms*-1;
end
spikes.info=struct;
spikes.params.max_jitter=.5;%ms
spikes.params.Fs=EXTRAS.rate;
spikes.info.detect.align_sample=12;
spikes.spiketimes=EVENTTIMES'/EXTRAS.rate;
spikes.info.detect.event_channel=ones(size(spikes.spiketimes));
spikes=ss_align(spikes);
if(EXTRAS.sigthreshold<0)
    SPIKESET=-1*spikes.waveforms';
else
    SPIKESET=spikes.waveforms';
end

XAXIS=[-10 size(SPIKESET,1)-11];
%XAXIS=[-10 15];
%XAXIS=[-10 24];

if(0)
    figure;
    for i=1:1000
    plot(spikes.waveforms(i,:),'.-')
    line(get(gca,'XLim'),5.76597e-05*-1*[1 1],'LineWidth',1)
    title([num2str(shifts(i)),' ',num2str(i)])
    ylim(3.4678e-04*[-1 1])
    pause();
    end
end