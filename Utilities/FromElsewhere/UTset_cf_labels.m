function UTset_cf_labels(ax,varargin)
%UTset_cf_labels(ax,multiplier,type,axis)
%type   0: 1,2,4,6
%       1: 1,5
%       2: 1,2,3,4,5,6,7,8,9
%       3: half-octave spacing from 4k
% LAS
    multi=1;
type=0;
if(length(varargin)>=1)
    multi=varargin{1};
end
if(length(varargin)>=2)
    type=varargin{2};
end
axstr='XTick';
if(length(varargin)>=3)
    switch lower(varargin{3})
        case 'xtick'
            axstr='XTick';
        case 'ytick'
            axstr='YTick';
        otherwise
            axstr='XTick';
    end
end
if(type<=2)
    t=[.06:.01:.1, .2:.1:1 2:1:10 20:10:100]*multi;
elseif(any(type==[3,4]))
    t=2.^(log2(4):.5:log2(64))*multi;
elseif(any(type==[5,6]))
    t=2.^(log2(3):.2:log2(96))*multi;
end
if(any(type==[4,6]))
        tl=arrayfun(@(x)num2str(round(x*10)/10),t/multi,'Uni',0);
elseif(any(type==[3,5]))
        tl=arrayfun(@(x)num2str(round(x*10)/10),t/multi,'Uni',0);
        [tl{2:2:end}]=deal('');
elseif(type==2)
        tl={'.06','.07','.08','.09','.1','.2','.3','.4','.5','.6','.7','.8','.9','1','2','3','4','5','6','7','8','9','10','20','30','40','50','60','70','80','90','100'};
elseif(type==1)
    tl={'','','','','.1','','','','.5','','','','','1','','','','5','','','','','10','','','','50','','','','','100'};
else
    tl={'.06','','','','.1','.2','','.4','','.6','','','','1','2','','4','','6','','','','10','20','','40','','60','','','','100'};
end
set(ax,axstr,t,[axstr,'Label'],tl,[axstr(1),'Minor',axstr(2:end)],'off')