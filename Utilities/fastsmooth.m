function SmoothY=fastsmooth(Y,w,type,ends,wind)
% fastbsmooth(Y,w,type,ends) smooths vector Y with smooth 
%  of width w. Version 2.0, May 2008.
% The argument "type" determines the smooth type:
%   If type=1, rectangular (sliding-average or boxcar) 
%   If type=2, triangular (2 passes of sliding-average)
%   If type=3, pseudo-Gaussian (3 passes of sliding-average)
% The argument "ends" controls how the "ends" of the signal 
% (the first w/2 points and the last w/2 points) are handled.
%   If ends=0, the ends are zero.  (In this mode the elapsed 
%     time is independent of the smooth width). The fastest.
%   If ends=1, the ends are smoothed with progressively 
%     smaller smooths the closer to the end. (In this mode the  
%     elapsed time increases with increasing smooth widths).
% fastsmooth(Y,w,type) smooths with ends=0.
% fastsmooth(Y,w) smooths with type=1 and ends=0.
% Example:
% fastsmooth([1 1 1 10 10 10 1 1 1 1],3)= [0 1 4 7 10 7 4 1 1 0]
% fastsmooth([1 1 1 10 10 10 1 1 1 1],3,1,1)= [1 1 4 7 10 7 4 1 1 1]
%  T. C. O'Haver, 2008.
re=false;
if(isrow(Y))
    Y=Y';
    re=true;
end
if nargin==2, ends=0; type=1; wind='rect';end
if nargin==3, ends=0; wind='rect'; end
if nargin==4, wind='rect'; end
  switch type
      case 0
      SmoothY=Y;
      case 1
       SmoothY=sa(Y,w,ends,wind);
    case 2   
       SmoothY=sa(sa(Y,w,ends,wind),w,ends,wind);
    case 3
       SmoothY=sa(sa(sa(Y,w,ends,wind),w,ends,wind),w,ends,wind);
      otherwise
          
         SmoothY=sa(Y,w,ends,wind);
          for i=2:type
              SmoothY=sa(SmoothY,w,ends,wind);
          end
  end
if(re)
    SmoothY=SmoothY';
end

function SmoothY=sa(Y,smoothwidth,ends,wind)
w=round(smoothwidth);
if(strcmp(wind,'triang'))
  win=repmat(window(@triang,w),size(Y,2));
else
    win=ones(w,size(Y,2));
end
%SumPoints=sum(Y(1:w,:));
SumPoints=sum(Y(1:w,:).*win);
s=zeros(size(Y));
halfw=round(w/2);
L=size(Y,1);
for k=1:L-w,
    if(strcmp(wind,'triang'))
        s(k+halfw-1,:)=sum(Y((1:w)+k-1,:).*win);
%         s(k+halfw-1,:)=SumPoints;
%         SumPoints=SumPoints-Y(k,:);
%         SumPoints=SumPoints+Y(k+w,:);
    else
        s(k+halfw-1,:)=SumPoints;
        SumPoints=SumPoints-Y(k,:);
        SumPoints=SumPoints+Y(k+w,:);
    end
end
s(k+halfw,:)=sum(Y(L-w+1:L,:).*win);
SmoothY=s./repmat(sum(win,1),size(s,1),1);
% Taper the ends of the signal if ends=1.
  if ends==1,
  startpoint=(smoothwidth + 1)/2;
  SmoothY(1,:)=(Y(1,:)+Y(2,:))./2;
  for k=2:startpoint,
     SmoothY(k,:)=mean(Y(1:(2*k-1),:));
     SmoothY(L-k+1,:)=mean(Y(L-2*k+2:L,:));
  end
  SmoothY(L,:)=(Y(L,:)+Y(L-1,:))./2;
  end