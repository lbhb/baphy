function [metrics,metrics_newT]=di_nolick(parmfile,force_use_original_target_window,trials,stop_respwin_offset,compute_these_metrics)
% function [metrics,metrics_newT]=di_nolick(parmfile)
% function [metrics,metrics_newT]=di_nolick(parmfile,force_use_original_target_window)
% function [metrics,metrics_newT]=di_nolick(parmfile,force_use_original_target_window,trials)
% function [metrics,metrics_newT]=di_nolick(parmfile,force_use_original_target_window,trials,stop_respwin_offset)
% function [metrics,metrics_newT]=di_nolick(parmfile,force_use_original_target_window,trials,stop_respwin_offset,compute_these_metrics)
%
% calculate behavioral performance on a per-token basis using "traditional"
% performance metrics
%
% Inputs:
% parmfile: can be either a string for the fullpath to the parmfile, 
%    or a structure containing the output of LoadMFile(parmfile)
% force_use_original_target_window: if true, forse hit anf FA analysis to
%    use target window used during data collection. Otherwise, if target
%    window is longer than RefSegLen, sets target window to RefSegLen.
% trials: trials over which to calculate metrics
% stop_respwin_offset: offset to add to end of time window over which di is
%   calculated (default 1). The window end is the end of the target window,
%   plus this offset value.
% compute_these_metrics: cell list of optional metrics to compute. Possibilities are: {'new trials','new trials both halves','cue trials only','user-defined trials'}
%
% Outputs:
% metrics and metrics_newT are structs containing the metrics
%    metrics_newT has metrics calculated only from trials immediately
%    following hits or misses (should be trials in which a new stimulus was
%    played)
% DI2 = (1+HR-FAR)/2;  % area under the ROC
% Bias2 = (HR+FAR)/2
% DI = area under ROC curved based on RTs
%
% 

if nargin<2
    force_use_original_target_window=0;
end
if nargin<3
    trials=[];
end
if nargin<4
    stop_respwin_offset=1;
end
if nargin <5
    compute_these_metrics={'new trials','new trials both halves'};
end

if ischar(parmfile)
    dat=LoadMFile(parmfile);
    dat.exptparams.OfflineAnalysis = 2;
elseif isstruct(parmfile)
    dat=parmfile;
else
    error('unknown input')
end

trialparms=dat.exptparams.TrialObject;
if isobject(trialparms), trialparms=get(trialparms); end
if isobject(trialparms.TargetHandle)
    trialparms.TargetHandle=get(trialparms.TargetHandle);
end
ReferenceHandle = trialparms.ReferenceHandle;
if isobject(trialparms.ReferenceHandle)
    trialparms.ReferenceHandle=get(trialparms.ReferenceHandle);
else
    dat.exptparams = convert_trial_behavior_structs_to_objects(dat.exptparams);
    ReferenceHandle = get(dat.exptparams.TrialObject,'ReferenceHandle');
end

try 
    TargetDistSet = trialparms.TargetDistSet;
catch err
    if ~strcmp(err.identifier,'MATLAB:nonExistentField')
        rethrow(err);
    end
    TargetDistSet = get(ReferenceHandle,'TargetDistSet');
end
    


if isfield(dat,'globalparams') && isfield(dat.globalparams,'rawfilecount')
    trialcount=dat.globalparams.rawfilecount;
else
    trialcount=length(dat.exptparams.Performance)-1;
end
perf=dat.exptparams.Performance(1:trialcount);

behaviorparams=dat.exptparams.BehaveObject;
if isobject(behaviorparams), behaviorparams=get(behaviorparams); end

TarWindowStart=behaviorparams.EarlyWindow;

if getparm(dat.exptparams,'OfflineAnalysis',0)==2
    tnum=1;
    [~,~,~,~,StimStart] = evtimes(dat.exptevents,'TrialStart*',tnum);
    [~,~,~,~,StimEnds] = evtimes(dat.exptevents,'PostStimSilence*',tnum);
    [~,~,~,~,TrialEnd] = evtimes(dat.exptevents,'TRIALSTOP',tnum);
    StimEvents = dat.exptevents(StimStart+1:StimEnds(end));
    dat.exptparams.temporary.BehaviorEvents = dat.exptevents(StimEnds(end)+1:TrialEnd);
   [w,dat.exptparams] = GenerateEventWindows(dat.exptparams.BehaveObject,dat.exptparams,StimEvents,behaviorparams,{'StimBeahviorPumpWindows','PumpActualWindows'});
   dat.exptparams.temporary.EventWindows = w;

end

%two_target=iscell(trialparms.TarIdx) && any(cellfun(@length,trialparms.TarIdx)>1);
%two_target=isfield(trialparms,'TargetDistSet') && any(trialparms.TargetDistSet>1);
two_target=dat.exptparams.temporary.EventWindows.two_target;

% "strict" - FA is response to any possible target slot preceeding the target
switch dat.exptparams.TrialObjectClass
    case {'MultiRefTar','RSSToneMono'}
        TarPreStimSilence=trialparms.TargetHandle.PreStimSilence;
        if trialparms.SingleRefSegmentLen>0,
            RefSegLen=trialparms.SingleRefSegmentLen;
            PossibleTarTimes=(find(trialparms.ReferenceCountFreq(:))-0).*...
                trialparms.SingleRefSegmentLen+perf(1).FirstRefTime;
            %     if isfield(perf,'FirstTarTime'),
            %        PossibleTarTimes=unique([perf.FirstTarTime])';
            %     else
            %        PossibleTarTimes=unique([perf.TarResponseWinStart])'-TarWindowStart;
            %     end
            if two_target
                PossibleTar2Offsets=(find(trialparms.Tar2SegCountFreq(:))).*...
                    trialparms.Tar2SegmentLen+perf(1).FirstRefTime;
                PossibleTar2Times=repmat(PossibleTarTimes,1,length(PossibleTar2Offsets))...
                    + repmat(PossibleTar2Offsets',length(PossibleTarTimes),1);
                PossibleTar2Times=unique(PossibleTar2Times(:));
                
                if ~isequal(trialparms.Tar1AloneOffsetSegCountFreq,0)
                    PossibleTar1Offsets=(find(trialparms.Tar1AloneOffsetSegCountFreq(:))-1).*...
                        trialparms.Tar2SegmentLen;
                    PossibleTar1Times_=repmat(PossibleTarTimes,1,length(PossibleTar1Offsets))...
                        + repmat(PossibleTar1Offsets',length(PossibleTarTimes),1);
                    %Currently not used, but these are the times thar Target 1 can take when it's alone
                end
                %PossibleTarTimes=unique(round(PossibleTar1Times_(:),4));
            end
        else
            RefSegLen=trialparms.ReferenceHandle.PreStimSilence+...
                trialparms.ReferenceHandle.Duration+...
                trialparms.ReferenceHandle.PostStimSilence;
            PossibleTarTimes=(find(trialparms.ReferenceCountFreq(:))-1).*...
                RefSegLen+TarPreStimSilence;
            PossibleTarTimes=unique([perf.FirstTarTime])';
            
            if two_target
                PossibleTar2Offsets=(find(trialparms.Tar2SegCountFreq(:))).*...
                    RefSegLen;
                PossibleTar2Times=repmat(PossibleTarTimes,1,length(PossibleTar2Offsets))...
                    + repmat(PossibleTar2Offsets',length(PossibleTarTimes),1);
                PossibleTar2Times=unique(PossibleTar2Times(:));
            end
            
        end
    case 'ReferenceTarget'
        FAStartTime = getparm(trialparms.ReferenceHandle, 'MinimumTargetTime',[]);
        if isempty(FAStartTime)
            FAStartTime=get(trialparms.ReferenceHandle,'PreStimSilence');
        end
        %PossibleTarTimes = getparm(trialparms.ReferenceHandle, 'PossibleTarTimes','ERROR');
        PossibleTarTimes='Various';
        RefSegLen=inf;
    otherwise
        error('Fix me for TrialObject %s',exptparams.TrialObjectClass)
end

if behaviorparams.ResponseWindow>RefSegLen && ~force_use_original_target_window
    TarWindowStop=TarWindowStart+RefSegLen;
else
    TarWindowStop=TarWindowStart+behaviorparams.ResponseWindow;
end

if ismethod(ReferenceHandle,'get_target_suffixes')
    tar_suffixes=get_target_suffixes(ReferenceHandle,[],trialparms.ReferenceHandle);
    for i=1:length(tar_suffixes)
        if two_target
            trialref_type(arrayfun(@(x)~isempty(strfind(x.ThisTargetNote{1},tar_suffixes{i})),perf(1:trialcount)))=i;
        else
            trialref_type(arrayfun(@(x)~isempty(strfind(x.ThisTargetNote,tar_suffixes{i})),perf(1:trialcount)))=i;
        end
    end
else
    tar_suffixes=cell(1);
     trialref_type=ones(1,trialcount);
end
trialtargetid=zeros(1,trialcount);
if isfield(dat.exptparams,'UniqueTargets') && length(dat.exptparams.UniqueTargets)>1,
    UniqueCount=length(dat.exptparams.UniqueTargets);
    for tt=1:trialcount,
        if ~isfield(perf,'NullTrial') || ~perf(tt).NullTrial,
            if two_target
                trialtargetid_all{tt}=cellfun(@(x)find(strcmp(x,...
                    dat.exptparams.UniqueTargets),1),perf(tt).ThisTargetNote);
                trialtargetid(tt)=trialtargetid_all{tt}(1);
            else
                trialtargetid(tt)=find(strcmp(perf(tt).ThisTargetNote,...
                    dat.exptparams.UniqueTargets),1);
            end
        end
    end
else
    UniqueCount=1;
    trialtargetid=ones(size(perf));
end
if ismethod(ReferenceHandle,'get_target_suffixes')
    reftype_by_tarid=nan(size(dat.exptparams.UniqueTargets));
    for i=1:length(tar_suffixes)
        if two_target
            reftype_by_tarid(unique([trialtargetid_all{trialref_type==i}]))=i;
        else
            reftype_by_tarid(unique(trialtargetid(trialref_type==i)))=i;
        end
    end
    %     if any(isnan(reftype_by_tarid))
    %         error('Unmatched reftypes')
    %     end
else
    reftype_by_tarid=ones(size(dat.exptparams.UniqueTargets));
end

%[~,LightTrials] = evtimes(dat.exptevents,'*+Light');
%LightTrials=unique(LightTrials(LightTrials<trialcount));
%optotrial=zeros(1,trialcount);
%optotrial(LightTrials)=1;

resptime=[];
resptimeperfect=[];
stimtype=[];
stimtime=[];
reftype=[];
tcounter=[];
%optostim=[];
trialnum=[];

% exclude misses at very beginning and end
Misses=cat(1,perf.Miss);
t1=find(~Misses, 1 );
if isempty(trials)
    t2=find(~Misses, 1, 'last' );
else
    t2=find(~Misses(1:max(trials)), 1, 'last' );
end
if isempty(t1),t1=1;t2=1;end
%t1=1;t2=trialcount;
if ismethod(ReferenceHandle,'GenerateTimesForDI')
    if getparm(dat.exptparams,'OfflineAnalysis',0)==2
        for tt=1:t2-1
            %Generate times for trial tt (saved in Persistent variable)
            GenerateTimesForDI(ReferenceHandle,perf(tt),...
                dat.exptparams.temporary.EventWindows,trialref_type,trialtargetid_all,tt,1:tt);
            
            %Generate EventWindows and BehaviorEvents for the next trial
            tnum=tt+1;
            [~,~,~,~,StimStart] = evtimes(dat.exptevents,'TrialStart*',tnum);
            [~,~,~,~,StimEnds] = evtimes(dat.exptevents,'PostStimSilence*',tnum);
            [~,~,~,~,TrialEnd] = evtimes(dat.exptevents,'TRIALSTOP',tnum);
            StimEvents = dat.exptevents(StimStart+1:StimEnds(end));
            dat.exptparams.temporary.BehaviorEvents = dat.exptevents(StimEnds(end)+1:TrialEnd);
            [w,dat.exptparams] = GenerateEventWindows(dat.exptparams.BehaveObject,dat.exptparams,StimEvents,behaviorparams,{'StimBeahviorPumpWindows','PumpActualWindows'});
            dat.exptparams.temporary.EventWindows = w;
        end
    end
    [stimtype,stimtime,resptime,tcounter,trialnum,reftype]=GenerateTimesForDI(ReferenceHandle,perf(trialcount),...
        dat.exptparams.temporary.EventWindows,trialref_type,trialtargetid_all,trialcount,t1:t2);
else
    for tt=t1:t2,    
        
        if ~isfield(perf,'FirstTarTime') || isempty(perf(tt).FirstTarTime),
            perf(tt).FirstTarTime=perf(tt).TarResponseWinStart-behaviorparams.EarlyWindow;
        end
        if ~isfield(perf,'FirstLickTime') || isempty(perf(tt).FirstLickTime),
            perf(tt).FirstLickTime = min(evtimes(dat.exptevents,'LICK',tt));
            if isempty(perf(tt).FirstLickTime),
                perf(tt).FirstLickTime=inf;
            end
        end
        
        Ntar_per_reftype=length(trialparms.TargetIdxFreq);
        if two_target
            Dist1inds=trialparms.TargetDistSet(mod(trialtargetid_all{tt}-1,Ntar_per_reftype)+1)==1;
            if ~any(Dist1inds)
                error('Make sure this works if a trial doesn''t have a target from slot 1')
            elseif sum(Dist1inds)>1
                error('There should only be one target from TargetDistSet 1 per trial. There are more somehow...');
            end
            tar_time=perf(tt).TarTimes(Dist1inds);
        else
            tar_time=perf(tt).FirstTarTime;
        end
        TarSlotCount=sum(PossibleTarTimes<tar_time);
        if TarSlotCount>0
            stimtime=cat(1,stimtime,PossibleTarTimes(1:TarSlotCount),tar_time);
        else
            stimtime=cat(1,stimtime,tar_time);
        end
        
        resptime=cat(1,resptime,ones(TarSlotCount+1,1).*perf(tt).FirstLickTime);
        
        
        stimtype=cat(1,stimtype,zeros(TarSlotCount,1),1);
        % 0: ref, 1:tar1, 2: tar2
        
        reftype=cat(1,reftype,trialref_type(tt)*ones(TarSlotCount+1,1));
        if two_target
            tcounter=cat(1,tcounter,ones(TarSlotCount+1,1).*trialtargetid_all{tt}(Dist1inds));
        else
            tcounter=cat(1,tcounter,ones(TarSlotCount+1,1).*trialtargetid(tt));
        end
        %optostim=cat(1,optostim,ones(TarSlotCount+1,1).*optotrial(tt));
        trialnum=cat(1,trialnum,ones(TarSlotCount+1,1).*tt);
        
        if two_target
            Dist2inds=trialparms.TargetDistSet(mod(trialtargetid_all{tt}-1,Ntar_per_reftype)+1)==2;
            if sum(Dist2inds)==1
                tar2_time=perf(tt).TarTimes(Dist2inds);
                if 0
                    Tar2SlotCount=sum(PossibleTar2Times<tar2_time);
                    PossibleTar2Times_this_trial=PossibleTar2Times(1:Tar2SlotCount);
                else
                    PossibleTar2Times_this_trial=tar_time+PossibleTar2Offsets;
                    PossibleTar2Times_this_trial(PossibleTar2Times_this_trial>=tar2_time)=[];
                    Tar2SlotCount=length(PossibleTar2Times_this_trial);
                end
                stimtime=cat(1,stimtime,PossibleTar2Times_this_trial,tar2_time);
                resptime=cat(1,resptime,ones(Tar2SlotCount+1,1).*perf(tt).FirstLickTime);
                stimtype=cat(1,stimtype,zeros(Tar2SlotCount,1),1);
                reftype=cat(1,reftype,trialref_type(tt)*ones(Tar2SlotCount+1,1));
                tcounter=cat(1,tcounter,ones(Tar2SlotCount+1,1).*trialtargetid_all{tt}(Dist2inds));
                trialnum=cat(1,trialnum,ones(Tar2SlotCount+1,1).*tt);
            elseif sum(Dist2inds)>1
                error('There should only be one target from TargetDistSet 2 per trial. There are more somehow...');
            end
        end
    end
end
resptime(resptime==0)=Inf;

NoLick=resptime>stimtime+TarWindowStop;
Lick=(resptime>=stimtime+TarWindowStart & resptime<stimtime+TarWindowStop);
ValidStim=resptime>=stimtime+TarWindowStart;
%ValidStim=resptime>stimtime; %PerformanceAnalysis uses this way...

stop_respwin=behaviorparams.EarlyWindow + ...
    behaviorparams.ResponseWindow + stop_respwin_offset;
early_window=behaviorparams.EarlyWindow;
%stop_respwin=1.25;
if isempty(trials)
    use_=true(size(trialnum));
else
    use_=ismember(trialnum,trials);
end
if isfield(dat.exptparams,'TrialParams')
    CueTrials=find([dat.exptparams.TrialParams.CueSeg]);
else
    CueTrials=[];
end
use=use_;
use(ismember(trialnum(use_),CueTrials))=0;
if two_target
    repTarDistSet=repmat(TargetDistSet,1,length(tar_suffixes));
else
    repTarDistSet=1;
end
metrics=compute_metrics(Lick(use),NoLick(use),stimtype(use),stimtime(use),resptime(use),tcounter(use),stop_respwin,ValidStim(use),trialtargetid,trialnum(use),reftype(use),reftype_by_tarid,early_window,repTarDistSet);
%,'trialcount',trialcount
%% metrics only for cue trials:
if any(strcmp(compute_these_metrics,'cue trials only'))
    use=use_;
    use(~ismember(trialnum(use_),CueTrials))=0;
    metrics.details.metrics_CueTrials=compute_metrics(Lick(use),NoLick(use),stimtype(use),stimtime(use),resptime(use),tcounter(use),stop_respwin,ValidStim(use),trialtargetid,trialnum(use),reftype(use),reftype_by_tarid,early_window,repTarDistSet);
end
%% metrics using only trials with new stimuli
if any(strcmp(compute_these_metrics,'new trials'))
    HorM_trials=find([dat.exptparams.Performance(1:end-1).Hit]|[dat.exptparams.Performance(1:end-1).Miss]);
    if isfield(trialparms,'IsCatch_')
        HtoCatch=arrayfun(@(x)any(trialparms.IsCatch_(x.uHit==1)),dat.exptparams.Performance(HorM_trials));
        HorM_trials(HtoCatch)=[];
    elseif isfield(trialparms,'IsCatch')
        HtoCatch=arrayfun(@(x)any(trialparms.IsCatch(x.uHit==1)),dat.exptparams.Performance(HorM_trials));
        HorM_trials(HtoCatch)=[];
    else
        fprintf('This file does not have catches\n')
    end
    use_trials=[1 HorM_trials+1];
    use_trials(ismember(use_trials,CueTrials))=[];
    if ~isempty(trials)
        use_trials(~ismember(use_trials,trials))=[];
    end
    if 0 %use only hit trials from first rep
        use_trials=find([dat.exptparams.Performance(1:end-1).Hit]);
        non_cue_start=find(~[dat.exptparams.TrialParams.CueSeg],1);
        use_trials(use_trials<non_cue_start)=[];
        use_trials(~ismember([dat.exptparams.TrialParams(use_trials).rep],[1]))=[];
    end
    use=ismember(trialnum,use_trials);
    metrics_newT=compute_metrics(Lick(use),NoLick(use),stimtype(use),stimtime(use),resptime(use),tcounter(use),stop_respwin,ValidStim(use),trialtargetid,trialnum(use),reftype(use),reftype_by_tarid,early_window,repTarDistSet);
    %metrics_newT=compute_metrics(Lick,NoLick,stimtype,stimtime,resptime,tcounter,stop_respwin,ValidStim&use,trialtargetid,trialnum,reftype,reftype_by_tarid,early_window);
    
    %% metrics using only trials with new stimuli, first half
    if any(strcmp(compute_these_metrics,'new trials both halves'))
        use_trials1=use_trials(use_trials<max(use_trials)/2);
    use=ismember(trialnum,use_trials1);
    metrics_newT.pt1=compute_metrics(Lick(use),NoLick(use),stimtype(use),stimtime(use),resptime(use),tcounter(use),stop_respwin,ValidStim(use),trialtargetid,trialnum(use),reftype(use),reftype_by_tarid,early_window,repTarDistSet);
    use_trials2=use_trials(use_trials>max(use_trials)/2);
    use=ismember(trialnum,use_trials2);
    metrics_newT.pt2=compute_metrics(Lick(use),NoLick(use),stimtype(use),stimtime(use),resptime(use),tcounter(use),stop_respwin,ValidStim(use),trialtargetid,trialnum(use),reftype(use),reftype_by_tarid,early_window,repTarDistSet);
    end
else
    metrics_newT=[];
end
%% metrics only for user-defined trials:
if any(strcmp(compute_these_metrics,'user-defined trials'))
    UDind=find(strcmp(compute_these_metrics,'user-defined trials'));
    use=use_;
    use(~ismember(trialnum(use_),compute_these_metrics{UDind+1}))=0;
    metrics.details.metrics_User=compute_metrics(Lick(use),NoLick(use),stimtype(use),stimtime(use),resptime(use),tcounter(use),stop_respwin,ValidStim(use),trialtargetid,trialnum(use),reftype(use),reftype_by_tarid,early_window,repTarDistSet);
end

if 0
%% By time window
%Use to compute di for licks only occurring in certain windows re target
sts = 0:.1:2.5;
windows = repmat(sts,2,1)+repmat([0 .5]',1,length(sts));
windows = [0 1.2;0 4;windows'];

nd = behaviorparams.ResponseWindow/2+behaviorparams.EarlyWindow+0.7;
sts = .1:.05:nd-.2;
windows = [sts; repmat(nd,1,length(sts))]';
for i=1:size(windows,1)
    use=use_;
    rel_resp = resptime-(stimtime+TarWindowStart);
    resptime2 = resptime;
    remove_licks = (rel_resp < windows(i,1) | rel_resp > windows(i,2));
    resptime2(remove_licks)=Inf;
    metrics.details.by_window(i)=compute_metrics(Lick(use),NoLick(use),stimtype(use),stimtime(use),resptime2(use),tcounter(use),stop_respwin,ValidStim(use),trialtargetid,trialnum(use),reftype(use),reftype_by_tarid,early_window,repTarDistSet);
    metrics.details.windows=windows;
end
end

%% metrics only using opto
%FIX metrics=compute_metrics(Lick,NoLick,stimtype,stimtime,resptime,tcounter,stop_respwin,ValidStim,trialtargetid,trialnum,reftype,reftype_by_tarid);
end

function m=compute_metrics(Lick,NoLick,stimtype,stimtime,resptime,tcounter,stop_respwin,ValidStim,trialtargetid,trialnum,reftype,reftype_by_tarid,early_window,repTarDistSet)
FA=Lick & ValidStim & stimtype==0;
CR=NoLick & ValidStim & stimtype==0;
Hit=Lick & ValidStim & stimtype==1;
Miss=NoLick & ValidStim & stimtype==1;
m.details=struct('Hits',sum(Hit),'Misses',sum(Miss),...
    'FAs',sum(FA),'CRs',sum(CR));
m.HR=sum(Hit)./(sum(Hit)+sum(Miss));
m.FAR=sum(FA)./(sum(FA)+sum(CR));
% calculate DI using reaction timebbbb
resptime(resptime==0)=inf;
m.DI=compute_di(stimtime(ValidStim),resptime(ValidStim),...
    stimtype(ValidStim),stop_respwin,100);
if all(~ValidStim)
    m.DI=NaN;
end
m.DI2=(1+m.HR-m.FAR)/2;
m.Bias2= (m.HR+m.FAR)/2;
m.Ntrials=length(unique(trialnum));


%%
NuniqueTars=length(reftype_by_tarid);
uHit=zeros(1,NuniqueTars);
uMiss=zeros(1,NuniqueTars);
uFA=zeros(1,NuniqueTars);
uET=zeros(1,NuniqueTars);
uRT=zeros(1,NuniqueTars);
sRT=zeros(1,NuniqueTars);
medRT=zeros(1,NuniqueTars);
qrRT=zeros(2,NuniqueTars);
for uu=1:NuniqueTars
    uN(uu)=length(unique(trialnum(ValidStim & tcounter==uu)));
    
    hitI=Lick & ValidStim & stimtype==1 & tcounter==uu;
    uHit(uu)=sum(hitI);
    uMiss(uu)=sum(NoLick & ValidStim & stimtype==1 & tcounter==uu);
    uFA(uu)=uN(uu)-uHit(uu)-uMiss(uu);
    uET(uu)=sum(resptime<stimtime & stimtype==0 & stimtime==min(stimtime) & tcounter==uu);
    RTs=resptime(hitI)-stimtime(hitI)-early_window;
    uRT(uu)=mean(RTs);
    sRT(uu)=std(RTs);
    medRT(uu)=median(RTs);
    qrRT(:,uu)=prctile(RTs,[25 75]);
    
    FAI=Lick & ValidStim & stimtype==0 & tcounter==uu;
    RTs=resptime(FAI)-stimtime(FAI)-early_window;
    
    inds=ValidStim & (tcounter==uu | stimtype==0) & reftype==reftype_by_tarid(uu);
    if any(repTarDistSet>1)
        tar_inds_using_this_set=find(repTarDistSet(uu)==repTarDistSet);
        inds(~ismember(tcounter,tar_inds_using_this_set))=false;
    end
    [uDI(uu),uDI_hits(uu,:),uDI_fas(uu,:),tsteps]=compute_di(stimtime(inds),resptime(inds),stimtype(inds),stop_respwin,100);
    if all(~inds)
        uDI(uu)=NaN;
    end
end
uDI(uN == 0)=NaN;
uHR=uHit./(uHit+uMiss);
uDI2=(1+uHR-m.FAR)./2;

m.details.uHit=uHit;
m.details.uMiss=uMiss;
m.details.uFA=uFA;
m.details.uET=uET;
m.details.uHR=uHR;
m.details.uRT=uRT;
m.details.medRT=medRT;
m.details.sRT=sRT;
m.details.qrRT=qrRT;
m.details.uDI=uDI;
m.details.uDI2=uDI2;
m.details.uN=uN;
m.details.uDI_hits=uDI_hits;
m.details.uDI_fas=uDI_fas;
m.details.tsteps=tsteps;
end

function o = ObjectSetFields ( o,fields,values)
for cnt1 = 1:3:length(fields)
    try % since objects are changing,
        o = set(o,fields{cnt1},values.(fields{cnt1}));
    catch
        %warning(['property ' fields{cnt1} ' can not be found, using default']);
    end
end
end