function map=cbcolormap(N,use_black,only_use_set_2)
% map=cbcolormap(N)
% map=cbcolormap(N,use_black)
% Colormap for lines of qualitative data (not sequential)
% Plot map with:
%   figure;rgbplot(map);hold on;colormap(map);colorbar('Ticks',[])
% LAS created from http://mkweb.bcgsc.ca/colorblind/ and http://mkweb.bcgsc.ca/biovis2012/
if nargin<2
    use_black=true;
end
if nargin<3
    only_use_set_2=0;
end
if use_black
    NN=N;
else
    NN=N+1;
end
if NN<9 && ~only_use_set_2
    map=[0, 0, 0
        230, 159, 0
        86, 180, 233
        0, 158, 115
        240, 228, 66
        0, 114, 178
        213, 94, 0
        204, 121, 167]/255;
    
else
    map=[0 0 0
        0 73 73
        0 143 146
        255 109 182
        255 182 119
        73 0 146
        0 109 219
        182 109 255
        109 182 255
        182 219 255
        146 0 0
        146 70 0
        219 209 0
        36 255 36
        255 255 109]/255;
    ord=[1 6 11 2 7 12 3 8 13 4 9 14 5 10 15];
    map=map(ord,:);
    if NN>15
        warning('The biggest map has %d colors, you asked for %d. Returning %d.',15-~use_black,N,15-~use_black)
        N=15;
    end
     usi=round(linspace(1,size(map,1)/3,ceil(NN/3)));
     usii=sort([(usi-1)*3+1,(usi-1)*3+2,(usi-1)*3+3]);
     if any(usii==15) && ~any(usii==12) && length(usii)<15
         usii(usii==15)=12;
     end
     map=map(usii,:);
end
if ~use_black
    map(1,:)=[];
end
map(N+1:end,:)=[];


