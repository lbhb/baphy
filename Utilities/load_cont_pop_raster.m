options = struct();

options.usesorted=0;
options.usetempsorted=0;
options.Electrodes=1;
options.unit=1;
options.datause='Per trial';
options.sigthreshold=4;
options.lfp=0;
options.compact=1;
options.lick=0;
options.psth=0;
options.psthfs=30;
options.runclass='PTD';
options.StimInds={[1]};
options.trials=1;
options.mergesubplots=1;
options.save_figs=0;
options.ReferenceClass='Torc2';
options.ElectrodeMatrix=0;
options.showinc=0;
options.subplot_by_geometry=0;
options.sortedunit=[];
options.rawtrace=1;
options.rasterfs=30000;
options.Nreps=4;
options.channel=1;
options.includeincorrect=0;
options.includeprestim=1;

channel=options.channel;
unit=options.unit;

mfile='/auto/data/daq/Tartufo/TAR017/TAR017b05_a_PTD';

[r,tags,trialset,exptevents,options]= raster_load(mfile,channel,unit,options);
r_pop=zeros(size(r,1),1,64);
r_pop(:,:,1)=r(:,1);
for i = 2:64
    channel=i;
    r = raster_load(mfile,channel,unit,options);
    r_pop(:,:,i)=r(:,1);
end

% common average
med = median(r_pop,3);
r_med = r_pop-med;

r_thresh=r_med;
% remove threshold crossings
for i =1:64
    for j =1:size(r,1)
        if r_med(j,1,i)>50 || r_med(j,1,i)<-100
            r_thresh(j,:,i)=0;
        end
    end
end

