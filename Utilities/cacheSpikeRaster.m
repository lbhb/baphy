function [] = cacheSpikeRaster(expParms, options)
    % wrapper around loadspikeraster to load spike raster for a given set of
    % conditions and cache it in the experiment folder under /sorted/cached
    %
    % inputs:
    %  expParms - struct containing unique experiment identifiers
    %   animal - string (animal name in db)
    %   exp_site - string (ex: TAR010c)
    %   runclass - string (ex: PTD) note - if more than one file meets
    %   criteria, load all files and cache separately
    %   runs - list of runs to cache (ex: [4, 5, 6]), DEFAULT: cache all
    %   runs for the given runclass
    %   behaviorClass - string (ex: passive), DEFAULT: cache all conditions
    %
    %
    %  options - structure can contain the following fields:
    %   channel - electrode channel (default 1)
    %   unit - unit number (if not specified, default is to load all units)
    %   rasterfs in Hz (default 1000)
    %   includeprestim - raster includes silent period before stimulus onset
    %   tag_masks - cell array of strings to filter tags, eg,
    %             {'torc','reference'} or {'target'}.  AND logic.  default ={}
    %   psthonly - shape of r (default -1, see below)
    %   sorter - preferentially load spikes sorted by sorter.  if not
    %            sorted by sorter, just take primary sorting
    %   lfpclean - [0] if 1, remove MSE prediction of spikes (single
    %              trial) predicted by LFP
    %   includeincorrect - if 1, load all trials, not just correct (default 0)
    %   runclass - if set, load only data for runclass when multiple runclasses
    %              in file
    %
    %
    % created 1-4-2018, CRH

    
    if ~isfield(expParms, 'animal')
       error('expParms.animal must specify animal name. Example: Tartufo') 
    else
        animal = expParms.animal;
    end
    
    if ~isfield(expParms, 'exp_site')
        site = expParms.exp_site;
        error('expParms.animal must experiment and site. Example: TAR010c')
    else
        site = expParms.exp_site;
    end
    
    if ~isfield(expParms, 'runclass')
        run = 'none';
        warning('Make sure tag-masks apply to all runclasses and that all files youre asking for were sorted together')
    else
        run = expParms.runclass;
    end
      
    if ~isfield(expParms, 'runs')
        runs = 'all';
    else
        runs = expParms.runs;
    end
    
    if ~isfield(expParms, 'behaviorClass')
        behaviorClass='all';
    elseif strcmp(expParms.behaviorClass,'passive')
        behaviorClass='p';
    elseif strcmp(expParms.behaviorClass,'active')
        behaviorClass='a';
    end
    
    % define the root directory for this experiement
    exp = site(1:(end-1));
    root_dir = strcat('/auto/data/daq/',animal,filesep,exp,filesep,'sorted');
    
    % create a cache subdirectory, if it doesn't already exist
    if ~(exist(strcat(root_dir,filesep,'cache'))==7)
        mkdir(strcat(root_dir,filesep,'cache'))
    end
    
    % list all files in the root directory
    all_files = dir(root_dir);
    nFiles = length(all_files);
    
    
    % find the files to load based on user input
    files_to_load=[];
    for i = 1:nFiles
        
        if strcmp(runs,'all')
        
            if (strcmp(run,'none') && contains(all_files(i).name, site))
                files_to_load=[files_to_load,string(all_files(i).name)];
            elseif (contains(all_files(i).name, site) && contains(all_files(i).name, run))
                files_to_load=[files_to_load,string(all_files(i).name)];
            end
            
        elseif ~strcmp(runs, 'all') && strcmp(behaviorClass, 'all')
            nRuns = length(runs);
            for i = 1:nRuns
                num = num2str(runs(i));
                if length(num)==1
                   runs_for_fn=(strcat('0',num));
                else
                    runs_for_fn=num;
                end
                                
               
                f = strcat(site,runs_for_fn,'_',behaviorClass,'_',run,'.spk.mat');
                if exist(strcat(root_dir,filesep,f))==2
                    files_to_load = [files_to_load,string(f)];
                else
                   f = strcat(site,runs_for_fn,'_','p_',run,'.spk.mat');
                   if exist(strcat(root_dir,filesep,f))==2
                        
                   else
                       f = strcat(site,runs_for_fn,'_','a_',run,'.spk.mat')
                       if ~(exist(strcat(root_dir,filesep,f))==2)
                            error('invalid experiment parameters')
                       end
                   end     
                end
                
                files_to_load = [files_to_load,string(f)];
        end
    end
    
    nFiles_toload = length(files_to_load);
    
    for i = 1:nFiles_toload
        
        % Check how many units are on this channel    
        meta = load(strcat(root_dir,filesep,char(files_to_load(i)))); 
        SI = meta.sortinfo;
        
        % perform a check to make sure that this channel actually exists in
        % this recording file. If it does, find n units
        if isa(SI{options.channel},'double')
            error('Not a valid channel for this recording')
        elseif isfield(options, 'unit')
            n_units=1;
            if options.unit>1
                error('not a valid unit for this channel')
            end
        elseif isa(SI{options.channel},'cell')
            n_units = size(SI{options.channel}{1},1);
        end
       
        
        % cache raster for all units on the given channel
        for un = 1:n_units
            if isfield(options,'unit')
                unit = options.unit;
            else
                unit = un;
                options.unit = un;
            end
            % Create name for the cached the spike raster
            [~ ,name,~] = fileparts(char(files_to_load(i)));
            [~ ,name,~] = fileparts(name);
            spkfile = name;
            fn = strcat(spkfile,'_ch',num2str(options.channel),'_',num2str(unit),'_fs',num2str(options.rasterfs),'_',options.tag_masks);
            save_fn = char(strcat(root_dir,filesep,'cache',filesep,fn,'.mat'));
             
            if exist(save_fn)==2
                continue % file already exists in cache, don't need to create it
            else
                [raster, tags, trialset, exptevents, sortextras, options] = loadspikeraster(char(strcat(root_dir,filesep,files_to_load(i))), options);  

                % cache the spike raster
                save(save_fn,'raster');
                
            end
        end
    end
    
end