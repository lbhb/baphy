%function psi_raster(parmfile)

close all

parmfile = '/auto/data/daq/Prince/PRN009/PRN009a01_a_NTD';
parmfile = '/auto/data/daq/Prince/PRN009/PRN009a02_a_NTD';
parmfile = '/auto/data/daq/Prince/PRN009/PRN009a03_a_NTD';
parmfile = '/auto/data/daq/Prince/PRN009/PRN009a04_a_NTD';
parmfile = '/auto/data/daq/Prince/PRN010/PRN010a04_a_NTD';
parmfile = '/auto/data/daq/Prince/PRN011/PRN011a01_a_NTD';
parmfile = '/auto/data/daq/Prince/PRN010/PRN010a02_a_NTD';
parmfile = '/auto/data/daq/Prince/PRN011/PRN011a02_a_NTD';

OEPdir = [parmfile '/raw'];
runinfo = OEPgetinfo6(OEPdir);

eventfile = fullfile(parmfile, "event_log.csv");
trialfile = fullfile(parmfile, "trial_log.csv");

T = readtable(trialfile);
E = readtable(eventfile);
event = E.event;
timestamp = E.timestamp;
info = E.info;

bg_starts = timestamp(strcmp(event,'background_added'));

trial_starts0 = timestamp(strcmp(event,'trial_start'));
trial_starts = timestamp(strcmp(event,'target_start'));
trial_ends = timestamp(strcmp(event,'trial_end'));

spike_rec = runinfo.spike_rec;

% convert OE timestamps to seconds since recording started
trial_starts_oe = runinfo.recordings(spike_rec).trial_onsets_ts;
trial_starts_oe(trial_starts_oe<0)=0.5; % weird -1 value set to 0
trial_starts0_oe = trial_starts_oe-trial_starts_oe(1);

trial_type = zeros(size(trial_starts0_oe))-1;
trial_outcome = zeros(size(trial_starts0_oe))-1;

trial_log_starts = T.trial_start;
[trial_starts0_oe(1:10) trial_starts0(1:10) trial_starts(1:10) trial_log_starts(1:10)]

fprintf('event trials: %d  OE trials: %d  trial logs: %d\n',  ...
    length(trial_starts), length(trial_starts_oe), length(trial_log_starts));

target_info = T.trial_type;
score = T.score;
snr = T.snr;
target_tone_frequency = T.target_tone_frequency(1);

go_trial = strcmp(target_info, 'go');
correct_trial = strcmp(score, 'HIT') | strcmp(score, 'CR');

trial_log_match = zeros(size(trial_starts_oe));
for tt = 1:length(trial_log_starts)
    match = find(abs(trial_log_starts(tt)-trial_starts0_oe)<0.02);
    if isempty(match)
        fprintf("No match for trial %d\n",tt);
    else
        fprintf("Trial log %d matches event trial %d (t=%.3f sec)\n", tt, match, trial_log_starts(tt));
        trial_log_match(tt)=match;
        trial_type(match)=go_trial(tt);
        trial_outcome(match)=correct_trial(tt);
    end
end

spikefs = runinfo.spikefs;
prestimbins = 0.5*spikefs;

trial_onsets = runinfo.recordings(spike_rec).trial_onsets - prestimbins;
trial_offsets = runinfo.recordings(spike_rec).trial_offsets - prestimbins;

rasterfs = 50;
trial_bins = 2 * rasterfs;
pre_bins = 0.5 * rasterfs;
channel_count=64;

trial_ends = trial_onsets;
r=zeros(trial_bins,channel_count,length(trial_onsets));

for trialidx=1:length(trial_onsets)
    trial_ends(trialidx) = trial_onsets(trialidx) + trial_bins/rasterfs*spikefs;
    for channel_unmapped = 1:channel_count
        OEPspiketimes = runinfo.recordings(runinfo.oespike_rec).spikes(channel_unmapped).sample_numbers;
        thistrialidx = find(OEPspiketimes>trial_onsets(trialidx) & OEPspiketimes<trial_ends(trialidx));
        spiketimes_this_trial = double(OEPspiketimes(thistrialidx) - trial_onsets(trialidx));
        expectedspikebins=double(trial_ends(trialidx)- trial_onsets(trialidx));
    
        r(:,channel_unmapped,trialidx)=histcounts(spiketimes_this_trial,0:spikefs/rasterfs:...
                                (expectedspikebins));
    end
end
for channel_unmapped = 1:channel_count
    r(:,channel_unmapped,:)=r(:,channel_unmapped,:) - mean(mean(r(1:pre_bins,channel_unmapped,:),1),3);
end

clim = [min(min(mean(r,3))),max(max(mean(r,3)))]
figure;
subplot(1,2,1);
imagesc(mean(r(:,:,trial_type==1),3)',clim);
hold on
plot([0.5*rasterfs, 0.5*rasterfs],[1,channel_count],'g--')
plot([1.5*rasterfs, 1.5*rasterfs],[1,channel_count],'g--')
hold off
axis xy
title(sprintf('Go (%.0f Hz, n=%d/%d)',target_tone_frequency, sum(trial_type==1),length(trial_type)));
subplot(1,2,2);
imagesc(mean(r(:,:,trial_type==0),3)',clim);
hold on
plot([0.5*rasterfs, 0.5*rasterfs],[1,channel_count],'g--')
plot([1.5*rasterfs, 1.5*rasterfs],[1,channel_count],'g--')
hold off
axis xy
title(sprintf('No-go (n=%d/%d)',sum(trial_type==0),length(trial_type)));
colormap(1-gray);
