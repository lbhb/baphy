% function [r,tags,trialset,exptevents]=loadevpraster(mfile,options)
%
% load evp, identify spike events by thresholding derivative, extract
% responses relative to each Stim* event
%
% inputs:
%  mfile - baphy parameter file name
%  options - struction can contain the following fields:
%   channel - electrode channel (default 1)
%   unit - unit number (default 1)
%   rasterfs sampling/binning rate in Hz (default 1000)
%   includeprestim - raster includes silent period before stimulus onset
%   tag_masks - cell array of strings to filter tags, eg,
%       {'torc','reference'} or {'target'}. AND logic. default ={}.
%       Some special values:
%       SPECIAL-TRIAL
%       SPECIAL-COLLAPSE-REFERENCE
%          -TARGET, -BOTH
%          -ORDER
%       SPECIAL-LICK
%   psthonly - shape of r (default -1, see below)
%   includeincorrect - if 1, load all trials, not just correct (default 0)
%   lfp - set to 1 to return LFP data instead of evp raster
%           (downsampled to rasterfs)
%       - if 2, load lick data (ignore channel)
%   mua - set to 1 to return MUA (instantaneous energy) instead of evp raster
%   pupil - set to 1 to return pupil diameter instead of evp raster
%   rawtrace - set to 1 to return raw EVP signal, downsampled to rasterfs
%   pupil_xxx - options passed through to loadpupiltrace
%   recache - (default 0) if 1 force reload of evp and overwrite any cache
%
% 2006-01-13 : added caching to speed evp loading during online analysis
% 2007-10-04: changed SVD, altered syntax to allow for arbitary number of
%             options. old syntax still works:
% 2018-01-09: added caching for case when pupil is loaded, CRH
% function [r,tags,trialset,exptevents]=...
%                loadevpraster(mfile,channel,rasterfs,sigthreshold,...
%                              includeprestim,tag_masks,lfp)
%
% created SVD 2005-12-01
%
function [r,tags,trialset,exptevents,detect_options,options]=loadevpraster(mfile,channel,rasterfs,...
    sigthreshold,includeprestim,tag_masks,lfp,trialrange)

ENABLE_CACHING=0;

global C_r C_raw C_mfilename C_rasterfs C_sigthreshold
global BAPHY_LAB

detect_options=struct; %LAS: added this to make it have the same inputs and outputs as loadevpraster.
%                      Currently it's blank but it could be filled with something.
%
% SORT OF HORRIBLY COMPLICATED CODE FOR DEALING WITH TWO DIFFERENT
% WAYS OF PASSING PARAMETERS. SORRY! SVD 2008-01-01
%
if ~exist('channel','var'),
    options=[];
    options.channel=1;
    verbose = 1;
elseif isstruct(channel),
    options=channel;
    channel=getparm(options,'channel',1);
    sigthreshold=getparm(options,'sigthreshold',4);
    rasterfs=getparm(options,'rasterfs',1000);
    includeprestim=getparm(options,'includeprestim',1);
    tag_masks=getparm(options,'tag_masks',{});
    psthonly=getparm(options,'psthonly',-1);
    lfp=getparm(options,'lfp',0);
    mua=getparm(options,'mua',0);
    pupil=getparm(options,'pupil',0);
    verbose=getparm(options,'verbose',0);
    trialrange=getparm(options,'trialrange',[]);
    includeincorrect=getparm(options,'includeincorrect',0);
    lfp_clean=getparm(options,'lfp_clean',0);
    rawtrace=getparm(options,'rawtrace',0);
    runinfo=getparm(options,'runinfo',[]);
else
    options=struct();
    if ~exist('channel'),
        channel=1;
    end
    if ~exist('sigthreshold','var'),
        sigthreshold=4;
    end
    if ~exist('rasterfs','var'),
        rasterfs=1000;
    end
    if ~exist('includeprestim','var'),
        includeprestim=0;
    end
    if ~exist('tag_masks','var'),
        tag_masks={};
    elseif isnumeric(tag_masks),
        psthonly=tag_masks;
        tag_masks={};
    end
    if ~exist('psthonly','var'),
        psthonly=-1;
    end
    if ~exist('lfp','var'),
        lfp=0;
    end
    if ~exist('trialset','var'),
        trialset=[];
    end
    if ~exist('rawtrace','var'),
        rawtrace=0;
    end
    includeincorrect=0;
    lfp_clean=0;
    mua=0;
    pupil=0;
end
options.recache=getparm(options,'recache',0);
options.pupil_derivative=getparm(options,'pupil_derivative','');
options.pupil_offset=getparm(options,'pupil_offset',0.75);
options.pupil_smooth=getparm(options,'pupil_smooth',0);
options.pupil_highpass=getparm(options,'pupil_highpass',0);
options.pupil_lowpass=getparm(options,'pupil_lowpass',0);
options.pupil_bandpass=getparm(options,'pupil_bandpass',[]);
options.pupil_median=getparm(options,'pupil_median',0);
options.pupil_algorithm=getparm(options,'pupil_algorithm','');
options.pupil_method=getparm(options,'pupil_method','');
options.pupil_variable_name=getparm(options,'pupil_variable_name','');
options.pupil_deblink=getparm(options,'pupil_deblink',0);
options.pupil_deblink_dur=getparm(options,'pupil_deblink_dur',1);
options.pupil_mm=getparm(options, 'pupil_mm',0);
options.pupil_exclude_frames=getparm(options, 'pupil_exclude_frames',0);
options.pupil_eyespeed = getparm(options, 'pupil_eyespeed',0);
if ~exist('verbose','var') verbose = 1; end

if ~isempty(findstr(mfile,'spk.mat')),
    disp('converting spk.mat filename to m-filename');
    mfile=strrep(mfile,'.spk.mat','.m');
    mfile=strrep(mfile,'/sorted/','/');
end

if ~isempty(findstr(mfile,'pup.mat')),
    disp('converting spk.mat filename to m-filename');
    mfile=strrep(mfile,'.pup.mat','.m');
end

%
% DONE SETTING/CHECKING PARAMETER VALUES
%

%% If pupil == 1, check to see if cached pupil file exists. If it does, load
%  it and return output. If not, continue and create it.

% don't ever want to load from cache if we've asked to load pupil from 
% python analysis (CRH 01-17-2019)
if ~isempty(options.pupil_method)
    if options.pupil_method=='cnn'
        ENABLE_CACHING=0;
    end
end

if ENABLE_CACHING && pupil==1
    
    % construct name of cache file
    
    % all parms
    if isfield(options,'runclass')
        run = strcat('run-',options.runclass);
    else
        run = 'run-all';
    end

    if length(includeprestim)==1 && includeprestim>0
        prestim = strcat('prestim-',num2str(includeprestim));
    elseif length(includeprestim)>1
        prestim=strcat('prestim-',strrep(strrep(strrep(mat2str(includeprestim),' ','-'),'[',''),']',''));
    else
        prestim=strcat('prestim-none');
    end

    if isempty(tag_masks)
        tm = 'tags-default';
    else
        tm = strrep(strcat('tags-',tag_masks{:}),'*','X');  % replace asterik with 'X'
    end

    if includeincorrect==0
        ic = 'correctTrials';
    else
        ic='allTrials';
    end
    
    % pupil parms
    if pupil_offset==0.75
        offset_str='';
    else
       offset_str=strcat('_offset-',num2str(pupil_offset)); 
    end
    
    if pupil_median==0
        med_str='';
    else
        med_str=strcat('_med-',num2str(pupil_median));
    end
    
    filt_str='';
    if pupil_highpass>0
       filt_str=[filt_str,sprintf('_hp%.2f',pupil_highpass)];
    end
    if pupil_lowpass>0
       filt_str=[filt_str,sprintf('_lp%.2f',pupil_lowpass)];
    end
    if ~isempty(options.pupil_derivative)
       filt_str=[filt_str,sprintf('_D%s',options.pupil_derivative)];
    end
        
    [~,name,~] = fileparts(mfile);
    [~,fn,~]=fileparts(name);

    cache_fn= strcat(fn, '_fs',num2str(rasterfs),'_',tm,'_',run,'_',prestim,'_',ic,'_psth',num2str(psthonly),offset_str,med_str,filt_str,'_pup.mat');


    % Create path to cache file variable based on mfile input
    if length(strsplit(mfile,filesep))>1
        [filepath,~,~] = fileparts(mfile);
        root_dir_for_cache=strcat(filepath, filesep,'tmp'); 
    else
        current_folder = pwd;
        root_dir_for_cache=strcat(current_folder,filesep,'tmp');
    end
    
    % This is just to make sure all cached files are writeable (So we can
    % over write "bad" versions.
    if isunix 
            status = unix(['sudo chmod 777 -R ', root_dir_for_cache]);
    end

    % Check for existence of file
    if options.recache
      %force reload
      run_evp_raster = 1;
    elseif exist(strcat(root_dir_for_cache,filesep,cache_fn))==2
        disp('loading from cache: ')
        disp(cache_fn)
        run_evp_raster=0;
        r_struct=load(strcat(root_dir_for_cache,filesep,cache_fn));
        r = r_struct.r;
        tags=r_struct.tags;
        trialset=r_struct.trialset;
        exptevents=r_struct.exptevents;
    else
        run_evp_raster=1;  % create cache file
    end
    if ~run_evp_raster
       return
    end
end

%%
    
tic;  % start timing
if verbose
    fprintf('loadevpraster: loading %s channel=%d auxflag=%d mua=%d\n',...
        basename(mfile),channel,lfp,mua);
end

LoadMFile(mfile);

%put these here for use by functions calling this function
options.PreStimSilence=exptparams.TrialObject.ReferenceHandle.PreStimSilence;
options.PostStimSilence=exptparams.TrialObject.ReferenceHandle.PostStimSilence;

if(~isfield(options,'map_channels'))
    if isfield(globalparams.HWparams,'DAQSystem') && ...
          strcmp(globalparams.HWparams.DAQSystem,'Open-Ephys')
        options.map_channels=true;
    else
        options.map_channels=false;
    end
end
if strcmpi(exptparams.runclass,'mts')              %add by PBY at July 26, 2007
    if ~isempty(tag_masks) && length(tag_masks{1})>=16 && strcmp(tag_masks{1}(1:16),'SPECIAL-COLLAPSE')
        exptevents=baphy_mts_evt_merge(exptevents,1);  %merge TORC and TS togather
    else
        exptevents=baphy_mts_evt_merge(exptevents);    %do not merge with TORC
    end
end
evpfile = globalparams.evpfilename;

if lfp==2,
    % load from tmp file if exists. much faster.
    [bb,pp]=basename(evpfile);
    tevpfile=[pp 'tmp' filesep bb];
    if exist(tevpfile,'file'),
        evpfile=tevpfile;
    end
end

[pp,bb,ee]=fileparts(evpfile);
bbpref=strsep(bb,'_');
bbpref=bbpref{1};
checkrawevp=[pp filesep 'raw' filesep bbpref filesep bb '.001.1' ee];
if exist(checkrawevp,'file'),
    evpfile=checkrawevp;
end
checktgzevp=[pp filesep 'raw' filesep bbpref '.tgz'];
if exist(checktgzevp,'file'),
    evpfile=checktgzevp;
    if lfp>0,
        evpv=evpversion(globalparams);
        evpfile=evpmakelocal(checktgzevp,evpv);
    end
end

if ~exist(evpfile,'file'),
    [bb,pp]=basename(mfile);
    evpfile=[pp basename(evpfile)];
end

if ~exist(evpfile,'file') && ~exist([evpfile '.gz'],'file') && ...
   (~isfield(globalparams.HWparams,'DAQSystem') || ...
   ~strcmp(globalparams.HWparams.DAQSystem,'Open-Ephys'))
    [bb,pp]=basename(evpfile);
    tevpfile=[pp 'tmp/' bb];
    ss_stat=onseil;
    %if lfp==2 && ss_stat,
    if lfp && ss_stat,
        trin=[tevpfile ' ' tevpfile '.gz'];
        if ss_stat==1,
            disp('mapping file to seil');
            sublocalstring='/homes/svd/data/';
        elseif ss_stat==2,
            disp('mapping file to OHSU');
            sublocalstring='/auto/data/';
        end
        trout=strrep(tevpfile,'/auto/data/',sublocalstring);
        trout=strrep(trout,'/homes/svd/data/',sublocalstring);
        %trout=strrep(tevpfile,'/auto/data/','/homes/svd/data/');
        [bb,pp]=basename(trout);
        if strcmp(bb(1:5),'model'),
            disp('model cell, forcing recopy of response');
            ['\rm ' trout]
            unix(['\rm ' trout]);
        end
        if ~exist(trout,'file'),
            unix(['mkdir -p ',pp]);
            ['scp svd@bhangra.isr.umd.edu:',trin,' ',pp]
            unix(['scp svd@bhangra.isr.umd.edu:',trin,' ',pp]);
        end
        evpfile=trout
        if ~exist(evpfile,'file') && ~exist([evpfile '.gz'],'file'),
            error('evp file not found');
        end
    elseif (lfp==1 || mua==1) && onseil,
        disp('loadevpraster: lfp on seil: deferring evp check til later');
    else
        error('evp file not found');
    end
end

if lfp_clean,
    evpfile=lfpclean(evpfile,channel,lfp_clean,1);
    channel=1;
end

% check to see if loading same evp file as cached data
%C_mfilename
%mfile
if ~strcmp(basename(C_mfilename),basename(mfile)),
    C_mfilename=mfile;
    C_rasterfs=rasterfs;
    C_sigthreshold=sigthreshold;
    C_raw={};
    C_r={};
elseif size(C_r,2)>=channel & ~isempty(C_r{1,channel}) & C_sigthreshold==sigthreshold & C_rasterfs==rasterfs,
    disp('Using cached PSTH.');
elseif ~isempty(C_raw) & size(C_raw{1},2)>=channel,
    C_sigthreshold=sigthreshold;
    C_r={};
    disp('Using cached EVP.');
else
    C_mfilename=mfile;
    C_rasterfs=rasterfs;
    C_sigthreshold=sigthreshold;
    C_raw={};
    C_r={};
end

if verbose disp('loadevpraster: getting header info'); end

if(isempty(runinfo))
    runinfo=rawgetinfo(evpfile,globalparams);
    options.runinfo=runinfo;
end

if (~exist('rasterfs','var') | rasterfs==0) & lfp==1,
    rasterfs=floor(runinfo.lfpfs);
    if evpv==5,
        rasterfs=1000;
    end
elseif (~exist('rasterfs','var') | rasterfs==0) & lfp==2,
    rasterfs=floor(runinfo.auxfs);
elseif (~exist('rasterfs','var') | rasterfs==0),
    rasterfs=1000;
end

if (lfp==1 & runinfo.lfpchancount<channel) | (~lfp & ~pupil & ~any(runinfo.spike_channels==channel)),
    channel_not_found=true;
    if isfield(runinfo,'spike_channels_mapped')
        if any(runinfo.spike_channels_mapped==channel)
            channel_not_found=false;
        end
    end
    if channel_not_found
        disp('ERROR: channel does not exist');
        r=[];
        tags={};
        return
    end
end


% filter disabled SVD 2006-08-03
% setup parameters for filtering the raw trace to extract spike times
%f1 = 310; f2 = 8000;
%f1 = f1/spikefs*2;
%f2 = f2/spikefs*2;
%[b,a] = ellip(4,.5,20,[f1 f2]);
%FilterParams = [b;a];

tcorr=0;
for tt=1:length(tag_masks),
    if strcmpi(tag_masks{tt},'SPECIAL-CORRECT'),
        if verbose disp('excluding shock trials'); end
        tcorr=tt;
    end
end
if tcorr,
    tag_masks={tag_masks{[1:(tcorr-1) (tcorr+1):end]}};
    exclude_error_trials=1;
else
    exclude_error_trials=0;
end

%figure out run class
rc=strsep(basename(mfile),'.',1);
rc=strsep(rc{1},'_');
using_rc_subset=0;
if isnumeric(rc{end})
    options.runclass=rc{end-1};
elseif length(unique(rc))<=3,
   runclass=rc{end};
   options.runclass=runclass;
elseif isfield(options,'runclass') && ~isempty(findstr(options.runclass,'_')),
    runclass=options.runclass;
else
   runclassset=rc(3:end);
   options.runclass=getparm(options,'runclass',runclassset{1});
   rcmatch=min(find(strcmp(runclassset,options.runclass)));
   runclass=runclassset{rcmatch};
   if isempty(tag_masks) || ...
         (length(tag_masks)==1 && strcmpi(tag_masks{1},'Ref') && ...
         ~strcmpi(tag_masks{1},'SPECIAL')),
      using_rc_subset=1;
      tag_masks{1}=sprintf('Reference%d',rcmatch);
      fprintf('Fixing tag_masks to %s for runclass=%s\n',tag_masks{1},runclass);
   end
end



% figure out the tag for each different stimulus
% 2013-08-07 -- SVD moved to separate function so that it can be
% shared with loadspikeraster.m
[eventtime,evtrials,Note,eventtimeoff,tags]= ...
    loadeventfinder(exptevents,tag_masks,includeprestim,...
    exptparams.runclass,evpfile);

if verbose fprintf('%s tags sorted: %.1f sec\n',mfilename,toc); end
if length(channel)>1 && ~rawtrace && ~lfp
    error(sprintf('This function can currently only load one channel at a time unless loading the raw trace or an lfp. Currently you''re asking it to load %d.',length(channel)))
end
% define output raster matrix
referencecount=length(tags);
last_rep_idx=zeros(size(tags));
r=inf*zeros(1,1,referencecount,length(channel));
trialset=zeros(1,referencecount);

repcounter=zeros(referencecount,1);
big_rs=[];

% figure out the time that each trial started and stopped
[starttimes,starttrials]=evtimes(exptevents,'TRIALSTART*');
[ontimes,ontrials]=evtimes(exptevents,'STIM,ON');
[stoptimes,stoptrials]=evtimes(exptevents,'TRIALSTOP');
[shockstart,shocktrials,shocknotes,shockstop]=...
    findshockevents(exptevents,exptparams);
[hittime,hittrials]=evtimes(exptevents,'OUTCOME,MATCH');
if isempty(hittrials),
    [hittime,hittrials]=evtimes(exptevents,'BEHAVIOR,PUMPON*');
    %    [hittime,hittrials]=evtimes(exptevents,'BEHAVIOR,PUMP*');
end
%if runinfo.trialcount<1 || ~globalparams.ExperimentComplete,
    runinfo.trialcount=min(runinfo.trialcount,exptevents(end).Trial);
%end
if ~exist('trialrange','var') || isempty(trialrange),
    if ~includeincorrect && ~isempty(hittrials),
        disp('including only correct trials.');
        trialrange=hittrials(:)';
    else
        trialrange=1:runinfo.trialcount;
    end
else
    trialrange=trialrange(trialrange<=runinfo.trialcount);
    trialrange=trialrange(:)';
end
if exclude_error_trials,
    trialrange=setdiff(trialrange,shocktrials);
end

for trialidx=trialrange,
    starttime=starttimes(starttrials==trialidx);
    stoptime=stoptimes(stoptrials==trialidx);
    expectedspikebins=(stoptime-starttime)*runinfo.spikefs;
    bb=basename(mfile);
    if (strcmp(bb(1:3),'lim') || strcmp(bb(1:3),'dnb') ||...
            strcmp(bb(1:3),'ama')) && ~isempty(ontimes) && ...
            ontimes(ontrials==trialidx)>starttime,
        disp('Shifting spike times by ontimes-starttimes');
        adjusttime=ontimes(trialidx)-starttimes(trialidx);
        evidxthistrial=find(evtrials==trialidx);
        eventtime(evidxthistrial)=eventtime(evidxthistrial)-adjusttime;
        eventtimeoff(evidxthistrial)=eventtimeoff(evidxthistrial)-adjusttime;
        
    else
        adjusttime=0;
    end
    
    if lfp==1,
        
        if isempty(big_rs),
            %[rs0,strialidx0,rs,atrialidx,big_rs,strialidx]=evpread(evpfile,[],[],trialidx:runinfo.trialcount,channel);
            options.globalparams=globalparams;
            [big_rs,strialidx]=evplfp(evpfile,channel,trialidx:runinfo.trialcount,options);
            strialidx=[zeros(trialidx-1,1); strialidx; length(big_rs)+1];
        end
        
        rs=(big_rs(strialidx(trialidx):(strialidx(trialidx+1)-1),:));
        % resample, but don't scale--unlike spike counts, where the
        % resampled signal is muliplied by fsin/fsout
        raster=rs; % already resampled?? resample(rs,rasterfs.*100,runinfo.lfpfs.*100);
        %keyboard
        
        if lfp==0, % lfp~=2,
            shockhappened=find(shocktrials==trialidx);
            for tt=1:length(shockhappened),
                shockidx=shockhappened(tt);
                if verbose
                    fprintf('trial %d: zeroing shock %.1f-%.1f sec\n',...
                        trialidx,shockstart(shockidx),shockstop(shockidx)+0.3);
                end
                raster(round(shockstart(shockidx).*rasterfs):...
                    round((shockstop(shockidx)+0.3).*rasterfs))=0;
            end
        end
    elseif rawtrace==1,
        
        % read the raw data from the evp file
        if isempty(big_rs),
            %[~,~,~,spikefs]=evpgetinfo(evpfile);
            runinfo=rawgetinfo(evpfile,globalparams);
            [big_rs,strialidx]=evpread(evpfile,'spikeelecs',channel,'trials',trialidx:runinfo.trialcount,...
               'globalparams',globalparams,'runinfo',runinfo); %,'filterstyle','butterhigh');
            strialidx=[zeros(trialidx-1,1); strialidx; size(big_rs,1)+1];
        end
        
        if(isnan(options.rasterfs))
            %don't resample
            options.rasterfs=runinfo.spikefs;
            rasterfs=options.rasterfs;
        end
        raster=resample(...
            double(big_rs(strialidx(trialidx):(strialidx(trialidx+1)-1),:)),...
            rasterfs,runinfo.spikefs);
        
        shockhappened=find(shocktrials==trialidx);
        for tt=1:length(shockhappened),
            shockidx=shockhappened(tt);
            if verbose
                fprintf('trial %d: zeroing shock %.1f-%.1f sec\n',...
                    trialidx,shockstart(shockidx),shockstop(shockidx)+0.3);
            end
            raster(round(shockstart(shockidx).*rasterfs):...
                round((shockstop(shockidx)+0.3).*rasterfs),:)=0;
        end
        
    elseif pupil==1,
        % flag set to load pupil
        if isempty(big_rs),
            % figure out pupil file
            pupilfile=[strrep(mfile,'.m','') '.pup.mat'];
            [big_rs,strialidx]=loadpupiltrace(pupilfile,options);
            strialidx=[strialidx; length(big_rs)+1];
        end
        
        raster=(big_rs(strialidx(trialidx):(strialidx(trialidx+1)-1)))';
        
    elseif mua==1,
        
        if isempty(big_rs),
            %[rs0,strialidx0,rs,atrialidx,big_rs,strialidx]=evpread(evpfile,[],[],trialidx:runinfo.trialcount,channel);
            [big_rs,strialidx]=evpmua(evpfile,channel,rasterfs,trialidx:runinfo.trialcount);
            strialidx=[zeros(trialidx-1,1); strialidx; length(big_rs)+1];
        end
        
        raster=(big_rs(strialidx(trialidx):(strialidx(trialidx+1)-1)));
        
        shockhappened=find(shocktrials==trialidx);
        for tt=1:length(shockhappened),
            shockidx=shockhappened(tt);
            if verbose
                fprintf('trial %d: zeroing shock %.1f-%.1f sec\n',...
                    trialidx,shockstart(shockidx),shockstop(shockidx)+0.3);
            end
            raster(round(shockstart(shockidx).*rasterfs):...
                round((shockstop(shockidx)+0.3).*rasterfs))=0;
        end
        
    elseif lfp>=2,
        channel=lfp-1;  % force lick
        if isempty(big_rs),
           if exist(globalparams.evpfilename,'file')
              en=globalparams.evpfilename;
           else
              en=globalparams.tempevpfile;
           end
           [rs0,strialidx0,big_rs,atrialidx]=evpread(en,[],channel,trialidx:runinfo.trialcount);
               %%LAS:
           atrialidx=[zeros(trialidx-1,1); atrialidx; length(big_rs)+1];
        end
        rs=(big_rs(atrialidx(trialidx):(atrialidx(trialidx+1)-1)));
        filtersize=round(runinfo.auxfs./rasterfs);
        if filtersize<1,
            raster=resample(rs,rasterfs.*10,runinfo.auxfs.*10).*runinfo.auxfs./rasterfs;
        else
            smfilt=ones(filtersize,1)./filtersize;
            raster=conv2(rs,smfilt,'same');
            raster=raster(round((runinfo.auxfs./rasterfs./2):(runinfo.auxfs./rasterfs):end));
        end
    elseif size(C_r,2)>=channel & size(C_r,1)>=runinfo.trialcount & ...
            ~isempty(C_r{trialidx,1}),
        % the raster has been successfully cached. just use it.
        raster=C_r{trialidx,channel};
    elseif (globalparams.ExperimentComplete && length(trialrange)>5) || sigthreshold==0,  
        
        % generate/use cachefile that already has spike events extracted from evp
        % for this channel/sigthreshold
        if isempty(big_rs),
            if strcmpi(BAPHY_LAB,'default') && (globalparams.HWSetup==3 || globalparams.HWSetup==11),
                %for SPR2: use 1.0 shockNaNwindow addby py@6/25/2013
                cachefile=cacheevpspikes(evpfile,channel,sigthreshold,options.recache,0,1.0,runinfo);
                %       elseif(globalparams.HWSetup==10)
                %           cachefile=cacheevpspikes(globalparams.rawfilename,channel,sigthreshold);
            else
                cachefile=cacheevpspikes(evpfile,channel,sigthreshold,options.recache,0,.3,runinfo);
            end
            %cacheevpspikes(evpfile,electrode,sigthreshold,options.recache,fixedsigma,ShockNanDur,runinfo)
            if(isempty(cachefile))
                r=[];
                tags=[];
                trialset=[];
                exptevents=[];
                return
            end
            try
                big_rs=load(cachefile);
            catch err
                if strcmp(err.identifier,'MATLAB:load:unableToReadMatFile')
                    pause(2)
                    delete(cachefile);
                    cachefile=cacheevpspikes(evpfile,channel,sigthreshold,options.recache,0,.3,runinfo);
                    big_rs=load(cachefile);
                end
            end
            
            if verbose, fprintf('%s big_rs loaded: %.1f sec\n',mfilename,toc); end
            options.absolute_threshold = big_rs.sigthreshold*big_rs.sigma; %Compute threhold in uV so it can be used by calling fns
            fprintf('Threshold is %0.1f uV\n',options.absolute_threshold);
        end
        
        % find spike events for this trial
        thistrialidx=find(big_rs.trialid==trialidx);
        spikeevents=big_rs.spikebin(thistrialidx);
        
        hithappened=find(hittrials==trialidx);
        if 0 && ~isempty(hittime),
            disp('Removing reward period');
            spikeevents=spikeevents(hittime(hithappened(1))*runinfo.spikefs);
        end
        
        if isempty(spikeevents),
            raster=zeros(fliplr(size(0:runinfo.spikefs/rasterfs:(expectedspikebins+runinfo.spikefs/rasterfs))));
        else
            raster=histc(spikeevents,0:runinfo.spikefs/rasterfs:(expectedspikebins+runinfo.spikefs/rasterfs));
            if length(spikeevents)==1
                %behvaior of histc is inconsistent, and creates a row 
                %vector is there is only one event, but a column vector otherwise
                raster=raster(:);
            end
        end
        
        if lfp~=2,
            shockhappened=find(shocktrials==trialidx);
            for tt=1:length(shockhappened),
                shockidx=shockhappened(tt);
                if verbose
                    fprintf('trial %d: nan-ing shock %.1f-%.1f sec\n',...
                        trialidx,shockstart(shockidx),shockstop(shockidx)+0.3);
                end
                raster(round(shockstart(shockidx).*rasterfs):...
                    round((shockstop(shockidx)+0.3).*rasterfs))=nan;
            end
        end
        C_r{trialidx,channel}=raster;
    else
        % need to regenerate the raster. maybe used the cached raw evp?
        if runinfo.evpv==6
            error('This case isn''t set up for OEP files. What is it for? Why are we here anyways? There are less than 5 trials?')
        end
        if length(C_raw)>=runinfo.trialcount & ~isempty(C_raw{trialidx}),
            % the raw evp has been cached
            rs=double(C_raw{trialidx}(:,channel));
            
        elseif 0,
            % just load the damn evp data one trial at a time from disk this
            % is very slow
            rs=evpread(evpfile,channel,[],trialidx);
            
        else
            % experimental: load entire evp at once to speed things up.
            % unfortunately it doesn't speed things up that much.
            if isempty(big_rs),
                [big_rs,strialidx]=evpread(evpfile,'spikechans',channel, ...
                    'trials',trialidx:runinfo.trialcount,'globalparams',globalparams);
                %[big_rs,strialidx]=loadmapdata(evpfile,channel,[],trialidx:runinfo.trialcount);
                strialidx=[zeros(trialidx-1,1); strialidx; length(big_rs)+1];
                
                if verbose fprintf('%s big_rs loaded: %d trials, %.1f sec\n',mfilename,length(strialidx)-1,toc); end
            end
            
            rs=big_rs(strialidx(trialidx):(strialidx(trialidx+1)-1));
        end
        if length(rs)<expectedspikebins,
            warning('length(rs)<expectedspikebins! padding tail with zeros.');
            rs((length(rs)+1):expectedspikebins)=0;
        end
        shockhappened=find(shocktrials==trialidx);
        if ~isempty(shockhappened) && length(rs)>ceil(shockstart(shockhappened(1)).*runinfo.spikefs),
            disp('Removing shock period');
            %rs=rs(1:ceil(shockstart(shockhappened(1))*runinfo.spikefs));
            winstart=ceil(shockstart(shockhappened(1))*runinfo.spikefs);
            winstop=min(ceil((shockstop(shockhappened(1))+0.5)*runinfo.spikefs),length(rs));
            rs(winstart:winstop)=nan;
        end
        hithappened=find(hittrials==trialidx);
        if 0 & ~isempty(hittime) & length(rs)>ceil(hittime(hithappened(1)).*runinfo.spikefs),
            disp('Removing reward period');
            rs=rs(1:ceil(hittime(hithappened(1))*runinfo.spikefs));
        end
        %if length(rs)./runinfo.spikefs>stoptime,
        %    rs=rs(1:ceil(stoptime*runinfo.spikefs));
        %end
        
        % filter and threshold to identify spike times
        rs=-rs(:);
        % disabling the filter:
        %         rs=filtfilt(FilterParams(1,:),FilterParams(2,:),rs);
        %         rs = rs - mean(rs);
        rs=[0;diff(rs>(sigthreshold*nanstd(rs)))>0];
        
        % bin the data
        spikeevents=find(rs);
        
        if isempty(spikeevents),
            raster=zeros(fliplr(size(0:runinfo.spikefs/rasterfs:(expectedspikebins+runinfo.spikefs/rasterfs))));
        else
            raster=histc(spikeevents,0:runinfo.spikefs/rasterfs:(expectedspikebins+runinfo.spikefs/rasterfs));
            if length(spikeevents)==1
                %behvaior of histc is inconsistent, and creates a row 
                %vector is there is only one event, but a column vector otherwise
                raster=raster(:);
            end
        end
        C_r{trialidx,channel}=raster;
    end
    
    %Luke - check to make sure raster is a column vector
    if(isvector(raster))
        %raster is a matrix when loading multiple channels at once
        if(isrow(raster))
            raster=raster(:);
            C_r{trialidx,channel}=raster;
            warning('Raster was a row vector for trial %d, transposing to a column vector.',trialidx)
        end
    end
    % figure out the start and stop time of each event during the trial
    % and place the corresponding spikes in the raster.
    
    evidxthistrial=find(evtrials==trialidx);
    if isfield(options,'tag_masks') && ~isempty(options.tag_masks) && ...
          strcmpi(options.tag_masks{1},'SPECIAL-TRIAL'),
       eventtimeoff(trialidx)=max(eventtimeoff(trialidx),length(raster)/rasterfs);
    end
    
    for evidx=evidxthistrial(:)',
        % figure out which event slot this is
        refidx=find(strcmp(tags,Note{evidx}));
        
        % if it doesn't match one of the expected tags then skip it (eg, a
        % target when we only want references)
        if ~isempty(refidx),
            %LAS change, keep track of trial index instead of searching for speed
                %repidx=find(any(isnan(r(:,:,refidx))), 1 );
                %if isempty(repidx),
                %    repidx=size(r,2)+1;
                %end
            repidx=last_rep_idx(refidx)+1;
            last_rep_idx(refidx)=repidx;
            %END change
            rlen=(eventtimeoff(evidx)-eventtime(evidx))*rasterfs;
            % make sure the raster matrix is big enough for the next event
            if size(r,1)<rlen || repidx>size(r,2) || refidx>size(r,3),
                r((size(r,1)+1):round(rlen),:,:,:)=inf;
                % LAS change, preallocate r for speed
                    %r(:,(size(r,2)+1):repidx,:)=nan;
                max_reps=max(cellfun(@(x)sum(strcmp(Note,x)),tags));
                next_empty_rep=size(r,2)+1;
                r(:,next_empty_rep:max_reps,:,:)=inf;
                %end change
            end
            %if ~isempty(findstr(Note{evidx},'TORC')),
            %    disp([num2str(trialidx) '  ' num2str(refidx) '  ' Note{evidx}]);
            %end
            if size(raster,1)>=round(eventtimeoff(evidx)*rasterfs),
                % actually put the responses in the output raster matrix
                if round(eventtime(evidx)*rasterfs+1)<1,
                    r(1:(-round(eventtime(evidx)*rasterfs)),repidx,refidx,:)=-1;
                    r((-round(eventtime(evidx)*rasterfs)+1):(round(eventtimeoff(evidx)*rasterfs)-round(eventtime(evidx)*rasterfs)),repidx,refidx,:)=...
                        raster(1:round(eventtimeoff(evidx)*rasterfs),:);
                else
                    r(1:(round(eventtimeoff(evidx)*rasterfs)-round(eventtime(evidx)*rasterfs)),repidx,refidx,:)=...
                        raster(round(eventtime(evidx)*rasterfs+1):round(eventtimeoff(evidx)*rasterfs),:);
                end
            elseif round(eventtime(evidx)*rasterfs)>0
                rl=size(raster,1)-round(eventtime(evidx)*rasterfs+1)+1;
                r(1:rl,repidx,refidx,:)=raster(round(eventtime(evidx)*rasterfs+1):end,:);
            else
                rl=size(raster,1);
                r(-round(eventtime(evidx)*rasterfs)+(1:rl),repidx,refidx,:)=raster;
            end
            trialset(repidx,refidx)=trialidx;
        end
    end
    if mod(trialidx,20)==0,
        %drawnow;
        if verbose
            fprintf('%s trial %d: %.1f sec\n',mfilename,trialidx,toc);
        end
    end
end

% LAS: r was preallocated, remove trials that weren't filled to be consistent
% with output before changing to prealloating
r(:,squeeze(all(all(all(isinf(r),1),3),4)),:,:)=[];
% LAS: set inf to nan to be consistent with previous. So short trials will
% still have NaNs at the end of them.
r(isinf(r))=nan;

if ~lfp
    r(r==-1)=nan;
end

StimTagNames=exptparams.TrialObject.ReferenceHandle.Names;
filename=mfile;
do_resort=true;%if false, won't be resorted, if true, still has to pass other criterea to be resorted
[r,tags,trialset]=sort_raster(r,tags,trialset,tag_masks,StimTagNames,filename,using_rc_subset,do_resort);

if psthonly==-1
    % do nothing?
    
elseif psthonly
    % time/record all in one long vector, averaged over reps
    r=nanmean(permute(r,[2 1 3]));
    r=r(:);
else
    % time/record X rep
    r=permute(r,[1 3 2]);
    r=reshape(r,size(r,1)*size(r,2),size(r,3));
end

if ENABLE_CACHING && pupil==1
    % cache pupil raster
    cache_file=strcat(root_dir_for_cache,filesep,cache_fn);
    save(cache_file,'r','tags','trialset','exptevents');
    
    if isunix
       
       % Make the cache file world writeable in case it needs to be overwritten
       % 666 means read+write but not execute. mat files aren't executable
       s = unix(['chmod 666 ', cache_file]);
       
       if s
          s = unix(['sudo chmod 666 ', cache_file]);
       end
    end
end

return


% code for generating example stimulus waveform:
Ref=Torc;
Ref=set(Ref,'PreStimSilence',ifstr2num(exptparams.RefTarObject.ReferenceHandle.PreStimSilence));
Ref=set(Ref,'PosStimSilence',ifstr2num(exptparams.RefTarObject.ReferenceHandle.PosStimSilence));
Ref=set(Ref,'Duration',ifstr2num(exptparams.RefTarObject.ReferenceHandle.Duration));
Ref=set(Ref,'FrequencyRange',(exptparams.RefTarObject.ReferenceHandle.FrequencyRange));
Ref=set(Ref,'Rates',(exptparams.RefTarObject.ReferenceHandle.Rates));
Ref=ObjUpdate(Ref);
[w,stimfs]=waveform(Ref,10);
w=[zeros(stimfs*get(Ref,'PreStimSilence'),1);
    w;
    zeros(stimfs*get(Ref,'PosStimSilence'),1)];
wresamp=resample(w,runinfo.spikefs,stimfs);


