% function LoadMFile(mfile)
%
% load baphy parameters from mfile into current workspace
%
% SVD 2005-11-20
%
function varargout=LoadMFile(mfile)

global CLEAR_MFILE_AFTER_LOAD BAPHYDATAROOT

if isempty(CLEAR_MFILE_AFTER_LOAD),
    CLEAR_MFILE_AFTER_LOAD=1;
end

if strcmp(computer,'PCWIN64') Sep = '\'; else Sep = '/'; end

if nargout>0
    return_as_struct=true;
    eval_location={};
    cmd=@eval;
else
    return_as_struct=false;
    eval_location{1}='caller';
    cmd=@evalin;
end

% DEPRECATED special code for copying temp files over to seil cluster
% if ~exist([mfile '.m'],'file') && ~exist(mfile,'file') && ...
%         strcmpi(mfile(1:4),BAPHYDATAROOT(1:4)),
%     ss_stat=onseil;
%     if ss_stat
%         trin=mfile;
%         if ~strcmp(trin((end-1):end),'.m'),
%             trin=[trin '.m'];
%         end
%         if ss_stat==1,
%             disp('mapping file to seil');
%             trout=strrep(trin,'/auto/data/','/homes/svd/data/');
%         else
%             trout=trin;
%         end
%         
%         [bb,pp]=basename(trout);
%         if strcmp(bb(1:5),'model'),
%             disp('model cell, forcing recopy of response');
%             ['\rm ' trout]
%             unix(['\rm ' trout]);
%         end
%         if ~exist(trout,'file'),
%             if ss_stat==2,
%                 disp('mapping file to OHSU');
%             end
%             unix(['mkdir -p ',pp]);
%             ['scp svd@bhangra.isr.umd.edu:',trin,' ',pp]
%             unix(['scp svd@bhangra.isr.umd.edu:',trin,' ',pp]);
%         end
%         mfile=trout;
%     end
% end

if ~exist([mfile '.m'],'file') && ~exist(mfile,'file')
    error('mfile %s does not exist',mfile)
end

[pp,bb]=fileparts(mfile);

% change to directory of m-file temporarily to run it
tpwd=pwd;
if ~isempty(pp),
   cd(pp);
else
   pp=pwd;
end

% error if the mfile is not locked, so check first:
if mislocked(bb)
    munlock(bb);
end
%clear(bb);

% SAVING FOR LOADING EFFICIENCY (benglitz)
if ~exist(['.',filesep,'tmp'],'dir') mkdir('tmp'); end
MatFile = ['tmp',Sep,bb,'.mat'];
MatFileInfo = dir(MatFile);
MFileInfo = dir([bb,'.m']);

if exist(mfile','dir')
    fprintf('Loading psi-format config %s\n', bb);
    
    eventfile = fullfile(mfile, "event_log.csv");
    trialfile = fullfile(mfile, "trial_log.csv");
    ee=dir(eventfile);
    
    globalparams=struct();
    globalparams.HWparams.DAQSystem = 'Open-Ephys';
    globalparams.rawfilename = fullfile(mfile, 'raw');
    globalparams.stim_system='psi';
    
    fprintf('***** Kludge alert!! Hard coding many baphy settings. INCLUDING PUPIL RECORDING AVI! ***** \n\n');
    globalparams.PupilFilename = strrep(fullfile(mfile, 'recording.avi'),'D:','E:');
    % no lick file
    globalparams.NumberOfElectrodes = 384;
    [root1,penname] = fileparts(pp);
    [root2,ferret] = fileparts(root1);
    parts = strsplit(bb,'_');
    siteid = parts{1}(1:(end-2));
    runclass = parts{3};
    if parts{2}=='a'
        globalparams.Physiology = 'Yes -- Behavior';
    else
        globalparams.Physiology = 'Yes -- Passive';
    end
    globalparams(1).Tester = 'Jereme';
    globalparams(1).Ferret = 'Prince';
    globalparams(1).Module = 'Psi';
    globalparams(1).Physiology = 'Yes -- Behavior';
    globalparams(1).SiteID = siteid;
    globalparams(1).HWSetup = 17;
    globalparams(1).NumberOfElectrodes = 384;
    globalparams(1).date = ee(1).date;
    globalparams(1).ExperimentComplete = 1;

    exptparams.runclass = runclass;
    exptparams.StartTime = datevec(ee(1).datenum);
    exptparams.TrialObject.ReferenceHandle.PreStimSilence = 1;
    exptparams.TrialObject.ReferenceHandle.PostStimSilence = 1;
    exptparams.TrialObject.ReferenceHandle.Names = {};
    exptparams.TrialObject.ReferenceHandle.descriptor = 'BigNat';
    exptparams.TrialObject.TargetHandle.descriptor = 'Tone';

    exptparams.TotalRepetitions = 1;
    exptparams.Repetition = 1;

    T = readtable(trialfile);
    E = readtable(eventfile);
    event_count = height(E);
    cols = E.Properties.VariableNames;

    exptevents=struct();
    for ee=1:event_count
        info = jsondecode(E.info{ee});
        if length(cols)==3
            exptevents(ee).Trial = 1;
            guess_trials=1;
        else
            exptevents(ee).Trial = E.trial(ee);
            guess_trials=0;
        end
        exptevents(ee).StartTime=E.timestamp(ee);
        if isfield(info,'duration')
            exptevents(ee).StopTime = E.timestamp(ee) + info.duration;
        else
            exptevents(ee).StopTime = E.timestamp(ee);
        end
        exptevents(ee).Note=E.event{ee};
        exptevents(ee).Info= info;
    end

    % adjust timestamps to reference current trial rather than absolute
    % expt time (baphy compatibility)
    [eventtime,evtrials,Note,eventtimeoff]=evtimes(exptevents,'target_start');
    if guess_trials
        for ee=1:length(exptevents)
            %tt=find(eventtime>=exptevents(ee).StartTime, 1);
            tt=max(find(eventtime<=exptevents(ee).StartTime));
            if ~isempty(tt)
                exptevents(ee).Trial = tt;
            end
        end
    end
    
    [eventtime,evtrials,Note,eventtimeoff]=evtimes(exptevents,'target_start');
    for ee=1:length(exptevents)
        tt= (evtrials==exptevents(ee).Trial);
        exptevents(ee).StartTime = exptevents(ee).StartTime - eventtime(tt);
        exptevents(ee).StopTime = exptevents(ee).StopTime - eventtime(tt);
    end
    for ee=1:length(evtrials)
        evcount=length(exptevents);
        exptevents(evcount+1).Trial=evtrials(ee);
        exptevents(evcount+1).StartTime = 0;
        exptevents(evcount+1).StopTime = 0;
        exptevents(evcount+1).Note = 'TRIALSTART';
        s = max([exptevents([exptevents.Trial]==evtrials(ee)).StartTime]);
        exptevents(evcount+2).Trial=evtrials(ee);
        exptevents(evcount+2).StartTime = s;
        exptevents(evcount+2).StopTime = s;
        exptevents(evcount+2).Note = 'TRIALSTOP';
    end
    globalparams.rawfilecount = length(eventtime);

    if nargout>0
        return_as_struct=true;
        varargout{1}.globalparams=globalparams;
        varargout{1}.exptparams=exptparams;
        varargout{1}.exptevents=exptevents;
    else
        return_as_struct=false;
        assignin("caller", 'globalparams', globalparams);
        assignin("caller", 'exptparams', exptparams);
        assignin("caller", 'exptevents', exptevents);
    end

    return

elif ~isempty(MatFileInfo) && ...
      datenum(MatFileInfo.date) > datenum(MFileInfo.date)
    cmd(eval_location{:},['load ',MatFile]);
else
    % only save matfile if experiment is complete (ie, not during online
    % analysis)
    cmd(eval_location{:},'clear exptevents');
    cmd(eval_location{:},bb);
    if cmd(eval_location{:},'exist(''globalparams'',''var'') && isfield(globalparams,''ExperimentComplete'') && globalparams.ExperimentComplete'),
        if ~exist('tmp','dir') mkdir('tmp'); end
        if ~cmd(eval_location{:},'exist(''exptevents'',''var'')') cmd(eval_location{:},'exptevents = [];'); end
            try 
                cmd(eval_location{:},['save tmp',Sep,bb,'.mat globalparams exptparams exptevents']);
            catch e
                if strcmp(e.identifier,'MATLAB:save:couldNotWriteFile')
                    warning(e.message)
                else
                    rethrow(e)
                end
            end
    end
end

% check if old (daqpc) mfile, in which case, don't continue
if cmd(eval_location{:},'~exist(''globalparams'',''var'')')
    disp(['NOTE: ' bb ' is an old format m-file']);
    cd(tpwd);
    return
end

if cmd(eval_location{:},'exist(''events'',''var'')')
    warning('old events renamed exptevents');
    cmd(eval_location{:},'caller','exptevents=events;');
end
%if it exists, set globalparams.rawfilename to correct path. (for Open-Ephys files)
cmd(eval_location{:},['if isfield(globalparams,''rawfilename''), TTT_oldbase_raw=[pcfileparts(globalparams.rawfilename) ''\'']; TTT_oldbase_raw(strfind(TTT_oldbase_raw,''raw''):end)=[]; globalparams.rawfilename=strrep(globalparams.rawfilename,TTT_oldbase_raw,''',[pp filesep],'''); end']);

%if mfile does not equal globalparams.mfilename, the file if being loaded remotely (likely on the server). Set paths appropriately
if ~cmd(eval_location{:},['strcmpi(''',mfile,''',globalparams.mfilename)']),
    cmd(eval_location{:},'TTT_oldbase=[pcfileparts(globalparams.mfilename) ''\''];');
    cmd(eval_location{:},['globalparams.mfilename=strrep(globalparams.mfilename,TTT_oldbase,''',[pp filesep],''');']);
    cmd(eval_location{:},['globalparams.evpfilename=strrep(globalparams.evpfilename,TTT_oldbase,''',[pp filesep],''');']);
    cmd(eval_location{:},['globalparams.tempevpfile=strrep(globalparams.tempevpfile,[TTT_oldbase ''tmp\''],''',[pp filesep 'tmp' filesep],''');']);
    cmd(eval_location{:},['if isfield(globalparams,''rawfilename''), TTT_oldbase_raw=[pcfileparts(globalparams.rawfilename) ''\'']; TTT_oldbase_raw(strfind(TTT_oldbase_raw,''raw''):end)=[]; globalparams.rawfilename=strrep(globalparams.rawfilename,TTT_oldbase_raw,''',[pp filesep],'''); end']);
    cmd(eval_location{:},['if isfield(globalparams,''PupilFilename''), globalparams.PupilFilename=strrep(globalparams.PupilFilename,TTT_oldbase,''',[pp filesep],'''); end']);
    cmd(eval_location{:},'clear TTT_oldbase');
end

% change back to working directory
cd(tpwd);
if CLEAR_MFILE_AFTER_LOAD,
    clear(bb);
end
if return_as_struct
    varargout{1}.globalparams=globalparams;
    varargout{1}.exptparams=exptparams;
    varargout{1}.exptevents=exptevents;
    
end
