function res=copyfiletree(src_folder, dest, exclude_names, gzip_names, unzip_to_local)

global MYSQL_BIN_PATH

if nargin<3
    exclude_names = {};
end

if nargin<4
    gzip_names={'AP\continuous.dat', 'AP\sample_numbers.npy',  'AP\timestamps.npy',...
        'AP/continuous.dat', 'AP/sample_numbers.npy',  'AP/timestamps.npy',...
        'LFP\continuous.dat', 'LFP\sample_numbers.npy',  'LFP\timestamps.npy',...
        'LFP/continuous.dat', 'LFP/sample_numbers.npy',  'LFP/timestamps.npy'};
end
if nargin<5
    unzip_to_local=0;
end


[base,folder]=fileparts(src_folder);
if isempty(folder)
    [base,folder]=fileparts(base);
end

cd(base)
[pathNames, dirNames, fileNames] = dirwalk(folder);
res=0;
for ii=1:length(pathNames)
    
    dest_path = [dest filesep pathNames{ii}];
    
    if ~exist(dest_path,'dir')
        [success, message, messageid] = mkdir(dest_path);
        fprintf('path: %s - %s\n', pathNames{ii}, message);
    end
    
    for jj = 1:length(fileNames{ii})
        fn = fileNames{ii}{jj};
        src_file = [base filesep pathNames{ii} filesep fn];
        s = dir(src_file);
        filesize = s.bytes/1024;
        if sum(contains(fn, exclude_names))
            fprintf('(%.1fK) Skipping %s\n',filesize,fn);
        elseif sum(contains(src_file, gzip_names))
            destfile = [dest filesep pathNames{ii} filesep fn '.gz'];
            if exist(destfile,'file')
                fprintf('(%.1fK) Already gzipped %s\n',filesize,[pathNames{ii} filesep fn]);
            else
                fprintf('(%.1fK) gzip %s -> %s\n',filesize,[pathNames{ii} filesep fn],[dest filesep pathNames{ii}]);
                cmd=[MYSQL_BIN_PATH,'gzip -c "',pathNames{ii}, filesep, fn,'" > "' destfile '"'];
                [w,s]=system(cmd);
                
                %zfile = [src_file '.gz'];
                %if ~exist(zfile,'file')
                %    filenames = gzip(src_file, [pathNames{ii} filesep]);

                %end
                %[success, message, messageid] = copyfile(src_file,[dest filesep pathNames{ii} filesep]);
            end
        elseif exist([dest filesep pathNames{ii} filesep fn],'file')
            fprintf('(%.1fK) Already copied %s\n',filesize,[pathNames{ii} filesep fn]);
        elseif strcmp(fn((end-2):end),'.gz') && unzip_to_local
            fprintf('(%.1fK) gunzip %s -> %s\n',filesize,[pathNames{ii} filesep fn],[dest filesep pathNames{ii}]);
            local_file = [dest, filesep, pathNames{ii}, filesep, fn(1:(end-3))];
            if ~exist(local_file, "file")
                cmd=['gunzip -c "',[pathNames{ii} filesep fn],'" > "',dest, filesep, pathNames{ii}, filesep, fn(1:(end-3)) , '"'];
                [w,s]=system(cmd);
            end
        else
            fprintf('(%.1fK) %s -> %s\n',filesize,[pathNames{ii} filesep fn],[dest filesep pathNames{ii}]);
            [success, message, messageid] = copyfile(src_file,[dest filesep pathNames{ii} filesep]);
            if success==0
                disp(message);
            end
            res = res+1;
        end
    end
end

