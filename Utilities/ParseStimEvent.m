% function varargout = ParseStimEvent (StimEvent,RemoveSpace);
function varargout = ParseStimEvent (StimEvent,RemoveSpace);
%
% This function parse the StimEvent.Note into stimuli name and
% ReferenceOrTarget field. It assumes the two are seperate by: ' , '

% Nima, November 2005
if nargin<2 , RemoveSpace = 1; end
comma = findstr(StimEvent.Note, ',');
comma = [1 comma length(StimEvent.Note)];
for cnt1 = 1:length(comma)-1
    varargout{cnt1} = StimEvent.Note(comma(cnt1):comma(cnt1+1));
    varargout{cnt1} = strrep(varargout{cnt1}, ',','');
    if cnt1==3,
        parts=strsep(varargout{cnt1},'+');
        varargout{cnt1}=parts{1};
        if length(parts)>1,
            varargout{cnt1+1}=strtrim(parts{2});
        else
            varargout{cnt1+1}='';
        end
    end
    if RemoveSpace
        varargout{cnt1} = strtrim(varargout{cnt1});
    end
end
while length(varargout)<nargout, 
    varargout{end+1}=[];
end