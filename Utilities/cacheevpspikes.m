function cachefile=cacheevpspikes(evpfile,electrode,sigthreshold,forceregen,fixedsigma,ShockNanDur,runinfo,detect_options)
% function cachefile=cacheevpspikes(evpfile,electrode,sigthreshold,forceregen)
%
% load evp and threshold to find candidate spike events.
% then save to cachefile (<evppath>\tmp\<evpbase>.sig<sigthreshold>.mat)
%
% created SVD 2007-01-17
% modified BE 2012-03-28

global USECOMMONREFERENCE VERBOSE
if isempty(VERBOSE) VERBOSE = 0; end 

%% PARSE ARGUMENTS
if ~exist('verbose','var') verbose = 0; end
if ~exist('fixedsigma','var') fixedsigma=0; end
if ~exist('sigthreshold','var') disp('default sigthreshold=4'); sigthreshold=4;end
sigthreshset=sigthreshold;
if ~exist('forceregen','var') forceregen=0; end
if ~exist('ShockNanDur','var') ShockNanDur=.3; end
if ~exist('detect_options','var') 
    detect_options=struct;
end
detect_options.threshold_channels=getparm(detect_options,'threshold_channels','mean');%{'mean','all',N} which channels to use for thresholding, etierh mean across channels, all channels, or a specific channel.
detect_options.shadow=getparm(detect_options,'shadow',0);%{N} remove spikes with an ISI less than this value (in seconds) to avoid noise triggering multiple detections of the same spike.
err=false;
if isnumeric(detect_options.threshold_channels)
    if(length(detect_options.threshold_channels)>1)
        %err=true;
    end
elseif ~any(strcmp(detect_options.threshold_channels,{'all','mean'}))
    err=true;
end
if(err)
   error('detect_options.threshold_channels must be either ''all'',''mean'', or a single number')
end
    


if fixedsigma  fs=['.f',num2str(fixedsigma)];
else  fs='';
end
if isempty(USECOMMONREFERENCE) || ~USECOMMONREFERENCE,
   commstr='.NOCOM';
else  commstr=''; end
if sigthreshold==0 commstr = ''; end; % MANTA file does not contain this information

%% PARSE FILE NAME AND FIND FILE
[pp,bb,ee]=fileparts(evpfile);
if strcmp(evpfile((end-8):end),'001.1.evp'),
  [pp,bb]=fileparts(evpfile);
  mfilepath=fileparts(fileparts(pp)); % strip off two subdirs
  mfilebase=strrep(bb,'.001.1','.m');
  mfile=fullfile(mfilepath,mfilebase);
  cachebase='.elec';
elseif strcmp(evpfile((end-3):end),'.tgz'),
  [pp,bb]=fileparts(evpfile);
  mfilepath=fileparts(pp); % strip off two subdirs
  dd=dir([mfilepath filesep bb '*m']);
  if ~isempty(dd),
    mfilebase=dd(1).name;
  else
    error('cannot find mfile');
  end
  mfile=fullfile(mfilepath,mfilebase);
  [mfilepath,bb]=fileparts(mfile);
  bb=[bb '.001.1'];
  cachebase='.elec';
else
  mfile=strrep(evpfile,'.evp','.m');
  mfilepath=fileparts(mfile);
  if(runinfo.evpv==6)
      cachebase='.elec';
      bb=[bb '.001.1'];
  else
      cachebase='.chan';
  end
end

cachefileset=cell(length(sigthreshset),1);
cacheexists=zeros(length(sigthreshset),1);
for ii=1:length(sigthreshset),
  if ~isempty(mfilepath),
    cachefile = [mfilepath filesep 'tmp' filesep bb cachebase strrep(num2str(electrode),'  ','+') ...
      '.sig' num2str(sigthreshset(ii)) fs commstr '.mat'];
    cachefile=strrep(cachefile,[filesep 'raw' filesep],filesep);
  else
    cachefile = ['tmp' filesep bb cachebase strrep(num2str(electrode),'  ','+') ...
      '.sig' num2str(sigthreshset(ii)) fs commstr '.mat'];
  end
  if exist(cachefile,'file') && forceregen~=1
    if verbose 
      disp(['found cached: ',cachefile]);
    end
    del_cache=false;
    warning('off','MATLAB:load:variableNotFound');
    cached_file=load(cachefile,'detect_options');
    if(isfield(cached_file,'detect_options'))
        if(~isequal(cached_file.detect_options,detect_options))
            fprintf('Cache file exists but detect options are not the same, regenerating.\n')
            del_cache=true;
        end
    else
        detect_options_old_files.shadow=0;
        detect_options_old_files.threshold_channels='mean';
        if(~isequal(detect_options_old_files,detect_options))
            fprintf('Cache file does not have detect options stored (old file), and requested detect options are not the same as that of the old files, regenerating.\n')
            del_cache=true;
        end
    end
    warning('on','MATLAB:load:variableNotFound');
    if(del_cache)
        delete(cachefile)
        cacheexists(ii)=0;
    else
        cacheexists(ii)=1;
    end
  end
  cachefileset{ii}=cachefile;
end

if length(sigthreshset)>1   cachefile=cachefileset;  end

if sum(cacheexists)==length(sigthreshset)  return; end

disp([mfilename ': caching ' (evpfile)]);
if sigthreshold==0 
  return;
end


LoadMFile(mfile);

baphyruninfo.trialcount=max(cat(1,exptevents.Trial));
if(exist('runinfo','var'))
    if baphyruninfo.trialcount<runinfo.trialcount,
        disp('Baphy parm file trial count shorter than evp.  Baphy crash?');
        fprintf('Truncating to %d/%d trials\n',baphyruninfo.trialcount,runinfo.trialcount);
        runinfo.trialcount=baphyruninfo.trialcount;
    end
else
    %if runinfo was not passed (old code, should be fixed to pass runinfo),
    % assume baphyruninfo has the correct value
    % get runinfo in caller by: runinfo=rawgetinfo(evpfile,globalparams);
    runinfo.trialcount=baphyruninfo.trialcount;
    warning('runinfo was not passed to cacheevpspikes. Assuming trialcount from baphyruninfo, but trialcount could be lower if the actual number of recorded trial was less. Fix this by passing runinfo to cacheevpspikes.')
end

%% DATA READ IN BLOCKS TO AVOID OUT OF MEMORY PROBLEMS
switch lower(exptparams.runclass)
  case {'mts','rts'}; BlockSize = 30;
  case {'spl'}; BlockSize = 9;
  case {'sht','bst'}; BlockSize = 20;
  otherwise BlockSize = runinfo.trialcount;
end

% PREPARE TO REMOVE SHOCK EVENTS
[shockstart,shocktrials,shocknotes,shockstop]=...
  findshockevents(exptevents,exptparams);

% PREPARE FOR SPIKESORTING
TrialIDs=cell(1,length(sigthreshset));
SpikeBins=cell(1,length(sigthreshset));
SpikeMatrices= cell(1,length(sigthreshset));

%% ITERATE OVER BLOCKS
NBlocks=ceil(runinfo.trialcount/BlockSize);     %read raw data in blocks
vsum=0;   Breaking = zeros(size(sigthreshset));

for iBlock=1:NBlocks                 %
  TrialsStart=(iBlock-1)*BlockSize+1;          %
  TrialsStop=min([iBlock*BlockSize runinfo.trialcount]);%
  R=evpread(evpfile,'spikeelecs',electrode,'trials',[TrialsStart:TrialsStop],'globalparams',globalparams,...
     'runinfo',runinfo,'filterstyle','butter');
  
  cIndices = R.STrialidx; cData = R.Spike; clear R;
  cData=single(cData);
  
  % added SVD - check for use of laser and try to remove artifacts if
  % necessary
  if isfield(exptparams.TrialObject,'LightPulseDuration') &&...
        sum(exptparams.TrialObject.LightPulseDuration)>0
     disp('removing laser artifacts');
     cData = remove_opto_artifacts(cData,cIndices,runinfo.spikefs,...
        exptparams,exptevents,0.1,0.002);
     disp('done');
  end
  
  if length(electrode)>1
      if strcmp(detect_options.threshold_channels,'mean'),
          rData=cData;
          cData=mean(rData,2);
      elseif strcmp(detect_options.threshold_channels,'all'),
      elseif length(detect_options.threshold_channels)<size(cData,2),
          rData=cData;
          cData=rData(:,detect_options.threshold_channels);
      end
  end
  cIndices(end+1) = size(cData,1) + 1;
  vsum=vsum+sum(double(mean(cData,2).^2));
  
  % REMOVE SHOCKEVENTS
  removedshock=0;
  cShockTrialsInd = find((shocktrials>=TrialsStart).*(shocktrials<=TrialsStop));
  for iT =1:length(cShockTrialsInd)
    cShockTrialInd = cShockTrialsInd(iT);
    ShockWindow = (round(((shockstart(cShockTrialInd)-0.05).*runinfo.spikefs):((shockstop(cShockTrialInd)+ShockNanDur).*runinfo.spikefs)));
    trialidx=shocktrials(cShockTrialInd) - TrialsStart + 1; % Reposition to current first trial
    fprintf('trial %d: removing shock %.1f-%.1f sec\n',...
      trialidx,shockstart(cShockTrialInd)-0.05,shockstop(cShockTrialInd)+ShockNanDur);
    cData(cIndices(trialidx)-1+ShockWindow,:)=nan;
    removedshock=1;
  end

  % look for artifacts and remove
  if iBlock==1
     ss=std(cData);
     bigartifacts=find(any(abs(cData)>ss*10,2));
     if fixedsigma,
        fprintf('sigma fixed at %3f\n',fixedsigma);
        sigma=fixedsigma;
     elseif ~isempty(bigartifacts) && length(bigartifacts)<size(cData,1)./2,
        % don't do this if EVERYTHING is an artifact
        disp('removing artifacts from sigma calc, but not from events.');
        cData_mean=mean(cData,2);%mean across channels (if there are multiple)
        tsig=nanstd(cData_mean);
        ff=find(abs(cData_mean)>tsig*5);
        td=zeros(size(cData_mean));
        td(ff)=1;
        td=conv2(td,ones(500,1),'same');
        tc=cData_mean(find(td==0));
        sigma=nanstd(tc);
      elseif removedshock,
        sigma=nanstd(mean(cData,2));
        
     else
        mData=mean(mean(cData));
        vData=vsum./(size(cData,1)-1)-mData.^2;
        sigma=sqrt(vData);
     end
  end
       
  cData(isnan(cData(:)))=0;
  cData = cData - repmat(mean(cData,1),size(cData,1),1);
  
  % PREPARE SPIKE COLLECTION
  SpikeWindow=[-10 39]; SpikeIndices=SpikeWindow(1):SpikeWindow(2);
  remove_isi_samples=round(detect_options.shadow*runinfo.spikefs);
  % LOOP OVER DIFFERENT THRESHOLDS
  for threshidx=1:length(sigthreshset),
    sigthreshold=sigthreshset(threshidx);
    
    % LOOP OVER TRIALS
    for trialidx=1:TrialsStop-TrialsStart+1
      cTrial = TrialsStart + trialidx - 1;
      if trialidx>length(cIndices) || cIndices(trialidx)>length(cData),
          break;
      end
      cTrialData=cData(cIndices(trialidx):(cIndices(trialidx+1)-1),:);
      
      % DETECT SPIKES
      if sigthreshold>1000,
          % FIND BINS WITH SPIKES basd on fixed threshold
          tspikebin=[double(cTrialData>sigthreshold)];
          tspikebin=diff(tspikebin)>0;
          [tspikebin,spikechan]=find(tspikebin);
          [tspikebin,ui]=unique(tspikebin);
          spikechan=spikechan(ui);
          cNSpikes = length(tspikebin);
          figure(1);
          plot(cTrialData);
          hold on
          plot([1 size(cTrialData,1)],[1 1].*sigthreshold,'k--');
          hold off
          title(num2str(trialidx));
          drawnow;
          if cNSpikes>0,
             cNSpikes
          end
      elseif 1 % isempty(bigartifacts)
         if sigthreshold>0
            tspikebin=diff(-cTrialData>(sigthreshold*sigma))>0;
         else
            tspikebin=diff(cTrialData>abs(sigthreshold*sigma))>0;
         end
         
         % FIND BINS WITH SPIKES
         [tspikebin,spikechan]=find(tspikebin);
         
         if ~isempty(bigartifacts)
            a = double(abs(cTrialData)>sigma*12);
            spikewid = 10;
            a = rconv2(a, ones(spikewid,1)) > 0;
            badspikes = tspikebin(a(tspikebin));
            tspikebin(a(tspikebin))=[];
            
            if verbose 
                fprintf('Trial %d: removed %d/%d spikes as bad\n',...
               trialidx, length(badspikes), length(badspikes)+length(tspikebin));
            end
            if 0
               figure;
               plot(cTrialData);
               hold on
               plot(tspikebin, cTrialData(tspikebin), 'ko');
               plot(badspikes, cTrialData(badspikes), 'ro');
               drawnow
            end
         end

         % REMOVE EVENTS TOO CLOSE TO BEGINNING AND END OF VECTOR
         rmi=tspikebin<=-SpikeWindow(1) | tspikebin>(size(cTrialData,1)-SpikeWindow(2));
         tspikebin(rmi)=[];
         spikechan(rmi)=[];
         i=1;
         while i<length(tspikebin)
             indz=tspikebin(i)==tspikebin;
             if(sum(indz)>1)
                 %duplicate spike, keep biggest channel
                 [~,imax]=max(range(cTrialData(tspikebin(i)+[0:remove_isi_samples],spikechan(indz))));
                 indz=find(indz);
                 indz(imax)=[];
                 tspikebin(indz)=[];
                 spikechan(indz)=[];
             end
             i=i+1;
         end
         
         [tspikebin,si]=sort(tspikebin);
         spikechan=spikechan(si);
         cNSpikes = length(tspikebin);
      else
         spikesize=-cTrialData./(sigthreshold*sigma);
         tspikebin=diff(spikesize>1)>0;
            
         % FIND BINS WITH SPIKES
         [tspikebin,spikechan]=find(tspikebin);
         [tspikebin,ui]=unique(tspikebin);
         spikechan=spikechan(ui);
         
         ff=find(any(spikesize(tspikebin,:)>7,2));
         if ~isempty(ff),
             ffextra=[];
             for ii=1:length(ff),
                 ffextra=union(ffextra,find(abs(tspikebin- ...
                                                tspikebin(ff(ii)))<500));
             end
             delidx=union(ff,ffextra);
             keepidx=setdiff(1:length(tspikebin),delidx);
             
             if 0
                 figure(3);
                 plot(spikesize);
                 hold on;
                 plot(tspikebin(keepidx),...
                      spikesize(tspikebin(keepidx)),'ko');
                 plot(tspikebin(delidx),...
                      spikesize(tspikebin(delidx))./20000,'rx');
                 hold off
                 title(num2str(trialidx));
                 drawnow
                 %keyboard;
             end
             
             tspikebin=tspikebin(keepidx);
             spikechan=spikechan(keepidx);
         end
         
         %if ~isempty(tspikebin),
         %   tspikebin=tspikebin(spikesize(tspikebin)<7);
         %end
         
         cNSpikes = length(tspikebin);
      end
      
      %if VERBOSE fprintf(['Trial : ',n2s(cTrial),' [ ',num2str(length(tspikebin)),' triggers found ]\n']); end
      % CHECK FOR EXCEEDINGLY HIGH SPIKE RATE (>1000HZ) AND BREAK TO AVOID MEMORY PROBLEMS
      if cNSpikes/(size(cTrialData,1)/runinfo.spikefs) > 1000 
        fprintf('SpikeRate exceeds 1000Hz. Stopping Spike collection.\n'); 
        %Breaking(threshidx) = 1; break; 
      end
      
      % REMOVE EVENTS TOO CLOSE TO BEGINNING AND END OF VECTOR
      rmi=tspikebin<=-SpikeWindow(1) | tspikebin>(size(cTrialData,1)-SpikeWindow(2));
      tspikebin(rmi)=[];
      spikechan(rmi)=[];
      
      
      %tspikebin(find(diff(tspikebin)<=remove_isi_samples)+1)=[]; %simple removal
      [tspikebin,spikechan]=UT_remove_spike_doubles(cTrialData,tspikebin,spikechan,remove_isi_samples); %keeps bigger spike, recursive.
      % COLLECT SPIKE WAVEFORMS
      if length(electrode)==1,
         cSpikes=zeros(length(SpikeIndices),length(tspikebin));
         for jj=1:length(tspikebin)    cSpikes(:,jj)=cTrialData(tspikebin(jj)+SpikeIndices);    end
      else
         cSpikes=zeros(length(SpikeIndices)*length(electrode),length(tspikebin));
         if(strcmp(detect_options.threshold_channels,'mean') || ...
           (isnumeric(detect_options.threshold_channels) && length(detect_options.threshold_channels)<size(rData,2))...
             )
             rTrialData=rData(cIndices(trialidx):(cIndices(trialidx+1)-1),:);
         else
             rTrialData=cData(cIndices(trialidx):(cIndices(trialidx+1)-1),:);
         end
         for jj=1:length(tspikebin)
            cSpikes(:,jj)=reshape(rTrialData(tspikebin(jj)+SpikeIndices,:),length(SpikeIndices)*length(electrode),1);
         end
      end
      
      % COLLECT TRIAL INFORMATION
      TrialIDs{threshidx} = [TrialIDs{threshidx} ; ones(length(tspikebin),1).*cTrial];
      SpikeBins{threshidx} = [SpikeBins{threshidx} ; tspikebin];
      SpikeMatrices{threshidx} = [ SpikeMatrices{threshidx} , cSpikes ];
      
    end % END TRIALS
  end % END THRESHOLDS
  clear cData;
end % END BLOCKS
%% SAVE THE SPIKE TIMES
for threshidx=1:length(sigthreshset)
  if Breaking(threshidx)  cachefile=[]; continue;  end
  
  sigthreshold = sigthreshset(threshidx);
  trialid = TrialIDs{threshidx};
  spikebin = SpikeBins{threshidx};
  spikematrix = SpikeMatrices{threshidx};
  
  fprintf('found %d events for electrode %d, sigthresh %.1f -> %s\n',...
    length(trialid),electrode,sigthreshold,basename(cachefileset{threshidx}));
  
  if ~exist([mfilepath filesep 'tmp'],'dir')   mkdir(mfilepath,'tmp');   end
  
  channel = electrode; % To preserve old naming convention
  save(cachefileset{threshidx},'trialid','spikebin','spikematrix',...
    'sigthreshold','sigma','channel','detect_options');
end

