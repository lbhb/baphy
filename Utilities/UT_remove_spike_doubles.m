function [crossings,channel]=UT_remove_spike_doubles(data,crossings,channel,shadow)
% remove  bad crossings, but remove them from channel first
double_start=diff(crossings) <= shadow;
if(~any(double_start))
    double=double_start;
    if(~isempty(double))
        double=[double; false];
    end
    double_first=nan(size(double));
    crossings_doubles=[];
    channel_doubles=[];
end
double_passes=0;
while(any(double_start))
    double_passes=double_passes+1;
    double=double_start;
    if(~isempty(double))
        double=[double;false];
    end
    db=1+find(double);
    
    %If a shadow period violation is adjecent to another violation, 
    %don't do the send violation on this pass (it will get done on the next pass).
    db(find(diff(db)<=1)+1)=[];
    
    double_first=nan(size(double));
    if(1)
        %remove double with lowest amplitude
        for i=1:length(db)
            try
                if(range(data(crossings(db(i))+[0:shadow],channel(db(i))))>=range(data(crossings(db(i)-1)+[0:shadow],channel(db(i)-1))))%inverted because threshold is negative
                    %second crossing is bigger than the first, remove first crossing
                    double(db(i))=true;
                    double_first(db(i))=false;
                    db(i)=db(i)-1;
                else
                     %first crossing is bigger than the second, remove second crossing
                    double_first(db(i)-1)=true;
                end
            catch err
                warning(err.msg);
                double_first(db(i)-1)=true;
            end
        end
    end
    channel_doubles=channel(db);    crossings_doubles=crossings(db);
    channel(db) = [];    crossings(db) = [];
    
    double(db)=[];
    double_first(db)=[];
    double_start=diff(crossings) <= shadow;
end