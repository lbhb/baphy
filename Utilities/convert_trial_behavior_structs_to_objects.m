function exptparams = convert_trial_behavior_structs_to_objects(exptparams)

if isfield(exptparams.TrialObject,'SelfTestMode')
    exptparams.TrialObject.SelfTestMode='No';
end
% create the behavior object:
BehaveObject = feval(exptparams.BehaveObjectClass);
fields = get(BehaveObject,'UserDefinableFields');
% CRH hack to update the LicksRequired. Dunno why the below func below
% doesn't handle it
BehaveObject = ObjectSetFields(BehaveObject, fields, exptparams.BehaveObject);
try BehaveObject = set(BehaveObject, 'LicksRequired', exptparams.BehaveObject.LicksRequired); end
% also, generate the reference and target objects:
RefObject = feval(exptparams.TrialObject.ReferenceClass);
fields = get(RefObject,'UserDefinableFields');
RefObject = ObjectSetFields(RefObject, fields, exptparams.TrialObject.ReferenceHandle);
TrialObject = feval(exptparams.TrialObjectClass);
fields = get(TrialObject, 'UserDefinableFields');
TrialObject = ObjectSetFields(TrialObject, fields, exptparams.TrialObject);
if ismethod(RefObject,'FinalObjUpdate')
    RefObject=FinalObjUpdate(RefObject,TrialObject,0);
end
TrialObject = set(TrialObject, 'ReferenceHandle',RefObject);
if ~strcmpi(exptparams.TrialObject.TargetClass,'none')
    TarObject = feval(exptparams.TrialObject.TargetClass);
    fields = get(TarObject, 'UserDefinableFields');
    TarObject = ObjectSetFields(TarObject, fields, exptparams.TrialObject.TargetHandle);
    TrialObject = set(TrialObject, 'TargetHandle',TarObject);
end
exptparams.TrialObject = TrialObject;
exptparams.BehaveObject = BehaveObject;

function o = ObjectSetFields ( o,fields,values)
for cnt1 = 1:3:length(fields)
    try % since objects are changing,
        o = set(o,fields{cnt1},values.(fields{cnt1}));
    catch
        %warning(['property ' fields{cnt1} ' can not be found, using default']);
    end
end
return