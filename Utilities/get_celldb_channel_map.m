function [channel_map, varargout] = get_celldb_channel_map(SiteID)
% channel_map = get_channel_map(SiteID)
% [channel_map, channel_map_path] = get_channel_map(SiteID)
% [channel_map, channel_map_path, channel_map_stuct] = get_channel_map(SiteID)
%
% Loads channel map from celldb
% Input: SiteID (ex. 'TNC055a')
% Outputs:
%   channel_map: channel map string (ex. '64D_slot1_bottom')
%   channel_map_path: path to channel map file
%   channel_map_stuct: result of load(channel_map_path)

global SERVER_PATH
dbopen;
sql=['SELECT gCellMaster.*,gPenetration.numchans,gPenetration.channel_map',...
    ' FROM gPenetration,gCellMaster',...
    ' WHERE gCellMaster.penid=gPenetration.id',...
    ' AND gCellMaster.siteid like "',SiteID,'"'];

sitedata=mysql(sql);

if length(sitedata) == 1
    channel_map = sitedata.channel_map;
elseif isempty(sitedata)
    error('No site matching %s found',SiteID)
elseif length(sitedata)>1
    error('Multiple sites matching %s found',SiteID)
end

if nargout>1
    if isempty(channel_map)
        varargout{1}='';
    else
        varargout{1} = [SERVER_PATH 'code' filesep 'KiloSort' filesep 'chanMap_' channel_map '.mat'];
    end
end
if nargout>2
    if isempty(varargout{1})
        warning('No channel map found in database, not loading anything')
        varargout{2}=struct;
    else
        varargout{2} = load(varargout{1});
    end
end