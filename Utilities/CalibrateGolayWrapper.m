function [offset] = CalibrateGolayWrapper(mic_or_speaker, varargin)


load GolayStims

fs=100000;

if strcmp(mic_or_speaker,'mic')
% step 1: record during presentation of calibration tone
yn=questdlg('Run microphone calibration?','Mic?');
if strcmpi(yn,'Yes'),
    uiwait(msgbox('Turn off amplifier. Insert microphone in calibration box, turn on calibration tone (114 dB SPL), then click ok',...
        'Ready?','modal'));
    
    [data_caltoneA_114] = SpeakerCalibrationGolay(zeros(size(StimA)));
    
    uiwait(msgbox('Turn on calibration tone (94 dB SPL), then click ok',...
        'Ready?','modal'));
    
    [data_caltoneA_94] = SpeakerCalibrationGolay(zeros(size(StimA)));
    
    
    respA=data_caltoneA_114;
    respB=data_caltoneA_94;
    
    mrespA=reshape(respA,8192,20);
    mrespA=mean(mrespA(:,2:19),2)';
    mrespB=reshape(respB,8192,20);
    mrespB=mean(mrespB(:,2:19),2)';
    
    %make frequency vector (in kHz, 100 kHz sampling rate)
    freqs=[0:((8192/2))-1]*fs/8192/1000;
    
    %make hanning window
    hwin(1:256,1)=hann(256);
    hwin(257:8192,1)=0;
    
    spec_golay = ((fft(A).*conj(fft(mrespA))+fft(B).*conj(fft(mrespB)))/2/L)';
    %compute ifft to get impulse response
    impulse_golay = ifft(spec_golay);
    impulse_golay2 = impulse_golay;%hwin; %hanning window impulse
    mag_golay_114 = fft(impulse_golay2);
    mag_golay_114 = 20*log10(abs(mag_golay_114(1:4096)));
    
    figure
    
    subplot(3,1,1),
    semilogx(freqs,mag_golay_114,'k')
    %xlabel('frequency, kHz')
    ylabel('Magnitude, dB re 1 volt')
    xlim([0.1 50])
    
    d=abs(freqs-1);
    nearest_freq_to_1k=find(d==min(d));
    
    db114cal=mag_golay_114(nearest_freq_to_1k);
    
    offset=114-db114cal;
    
    fprintf(['\n Offset is ',num2str(offset)])
    
    subplot(3,1,2),
    semilogx(freqs,mag_golay_114+offset,'k')
    %xlabel('frequency, kHz')
    ylabel('Magnitude, dB SPL')
    xlim([0.1 50])
    title(['114 dB cal, offset is ',num2str(offset)]);
    ylim([40 120])

    respA=data_caltoneA_94;
    respB=data_caltoneA_94;

    mrespA=reshape(respA,8192,20);
    mrespA=mean(mrespA(:,2:19),2)';
    mrespB=reshape(respB,8192,20);
    mrespB=mean(mrespB(:,2:19),2)';

    %make frequency vector (in kHz, 100 kHz sampling rate)
    freqs=[0:((8192/2))-1]*fs/8192/1000;    

    %make hanning window
    hwin(1:256,1)=hann(256);  
    hwin(257:8192,1)=0;

    spec_golay = ((fft(A).*conj(fft(mrespA))+fft(B).*conj(fft(mrespB)))/2/L)';
    %compute ifft to get impulse response
    impulse_golay = ifft(spec_golay);
    impulse_golay2 = impulse_golay;%hwin; %hanning window impulse
    mag_golay_94 = fft(impulse_golay2);
    mag_golay_94 = 20*log10(abs(mag_golay_94(1:4096)));

    subplot(3,1,3),
    semilogx(freqs,mag_golay_94+offset,'k')
    xlabel('frequency, kHz')
    ylabel('Magnitude, dB SPL')
    xlim([0.1 50])
    ylim([40 120])
    title(['94 dB cal, peak at 1k is ',num2str(mag_golay_94(nearest_freq_to_1k)+offset),'dB SPL']);
end
end

done=0;



%while not(done)

if strcmp(mic_or_speaker,'speaker')
    if length(varargin)==2
        offset = varargin{2};
    end
    if ~isempty(varargin)   % optional input argument specifying the hardware setup. will only be given when running from baphy gui and will be based on curren hardware setup
        hwsetup = num2str(varargin{1});
        yn=questdlg(strcat('Warning: using hardware setup ',hwsetup,' make sure this is correct before proceeding. Turn ON amplifier. Position mic in free field. Then click ok'),...
            'Ready?');
    else
        yn=questdlg(strcat('Warning: using hardware setup harcoded into SpeakerCalibrationGolay.m, make sure this is correct before proceeding. Turn ON amplifier. Position mic in free field. Then click ok'),...
            'Ready?');
    end
    
    if any(strcmpi(yn,{'Cancel','No'})),
        done=1;
        %break
        return
    end
    if exist('hwsetup', 'var')
        [dataA] = SpeakerCalibrationGolay(StimA, hwsetup);
        [dataB] = SpeakerCalibrationGolay(StimB, hwsetup);    
    else
        [dataA] = SpeakerCalibrationGolay(StimA);
        [dataB] = SpeakerCalibrationGolay(StimB);
    end

    
    mrespA=reshape(dataA,8192,20);
    mrespA=mean(mrespA(:,2:19),2)';
    mrespB=reshape(dataB,8192,20);
    mrespB=mean(mrespB(:,2:19),2)';
    
    %make frequency vector (in kHz, 100 kHz sampling rate)
    freqs=[0:((8192/2))-1]*fs/8192/1000;
    
    %make hanning window
    hwin(1:256,1)=hann(256);
    hwin(257:8192,1)=0;
    
    spec_golay = ((fft(A).*conj(fft(mrespA))+fft(B).*conj(fft(mrespB)))/2/L)';
    %compute ifft to get impulse response
    impulse_golay = ifft(spec_golay);
    impulse_golay2 = impulse_golay;%hwin; %hanning window impulse
    mag_golay = fft(impulse_golay2);
    mag_golay = 20*log10(abs(mag_golay(1:4096)));
    
    figure
    
    semilogx(freqs,mag_golay+offset,'k')
    xlabel('frequency, kHz')
    ylabel('Magnitude, dB SPL')
    xlim([0.1 50])
    
    freqmin=500;
    freqmax=10000;
    frange=find(freqs>=freqmin/1000 & freqs<=freqmax/1000);
    meanlevel=mean(mag_golay(frange))+offset;
    title(sprintf('Golay output. Mean level(%d-%d Hz): %.2f dB SPL',freqmin,freqmax,meanlevel))
    
    yn=questdlg('Rerun Golay stimulus at different level?',...
        'Rerun?');
    if any(strcmpi(yn,{'Cancel','No'})),
        done=1;
    end
    
end

% save('H:\daq\Calibrations\SB1\2016-11-08 16-04.mat','freqs','mag_golay','offset','fs')
% saveas(gcf,'H:\daq\Calibrations\SB1\2016-11-08 16-04.jpg','jpg')












