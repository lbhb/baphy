
function [idx, Frequencies] = target_select_TBP(LowFreq, HighFreq, Count, target)
    ff=linspace(log(LowFreq),log(HighFreq),Count+1);
    Frequencies=round(exp(ff(1:(end-1))+diff(ff)./2));
    idx = find(abs(Frequencies-target) == min(abs(Frequencies-target)));
end     