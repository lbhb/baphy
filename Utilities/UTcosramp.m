function ramp=UTcosramp(total,up,down,fs)
%ramp=UTcosramp(total,up,down,fs)
N=total*fs;

on_npts = up*fs;
%onFrequency = 1/up/2;
%onBuffer = -cos(2*pi*onFrequency*(0:on_npts-1)/fs);
%onBuffer = (onBuffer + 1)/2;
onBuffer = .5*(1 - cos(2*pi*(1:on_npts)'/(2*on_npts+1)));
off_npts = down*fs;
%offFrequency = 1/down/2;
%offBuffer = cos(2*pi*offFrequency*(1:off_npts)/fs);
%offBuffer = (offBuffer + 1)/2;
offBuffer = flipud(.5*(1 - cos(2*pi*(1:off_npts)'/(2*off_npts+1))));

ramp=ones(1,N);
ramp(1:length(onBuffer))=onBuffer;
ramp(end-length(offBuffer)+1:end)=offBuffer;

