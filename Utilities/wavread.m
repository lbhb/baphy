% wrapper for backward compatibility, now that wavread is replaced by
% audioread
function [w,fs]=wavread(file,samplecount)

% if exist('wavread','file')
%     [temp, fs] = wavread([soundpath filesep TorcFiles(1).name], 1);
% else
%     [temp, fs] = audioread(file);
% end

if verLessThan('matlab','8.4'),
    avpath=fileparts(which('soundsc'));
    tp=pwd;
    cd(avpath)
    [w,fs]=wavread(file,samplecount);
    cd(tp);
    return
end

try
    [w, fs] = audioread(file);
catch
    [w, fs] = audioread([file '.wav']);
end

