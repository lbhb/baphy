function FlushMessages=flush_data_to_server(sFrom,sTo,Verbose,NotificationEmail,exclude_current)
% function flush_data_to_server(sFrom,sTo);
%
% utility to copy baphy data files from the local machine to a central file
% server.  Also update cellDB to point those files to the new location.
%
% called by baphy_remote or the command line
%
% created SVD 2006-05-30
% updated BE 2012

if ~exist('sFrom','var') || isempty(sFrom) sFrom = MD_getDir('Kind','archive','DB',0); end
if ~exist('sTo','var') || isempty(sTo) sTo = MD_getDir('Kind','archive','DB',1); end
if ~exist('Verbose','var') Verbose = 0; end
if ~exist('NotificationEmail','var') NotificationEmail = 'boubenec@ens.fr'; end
if ~exist('exclude_current','var') exclude_current = 'yes'; end
global VERBOSE; VERBOSE = Verbose;
global BAPHY_LAB USECOMMONREFERENCE LOCAL_DATA_ROOT
USECOMMONREFERENCE = 0;

%% COLLECTING RECORDING FROM DATABASE
fprintf('FLUSHING from %s to %s...\n',sFrom,sTo);
DataPath=lower(strrep(sFrom,filesep,'/'));
Fields = 'parmfile,cellid,resppath,respfileevp,eyecalfile,training,id,trials,bad';
dbopen;
SQL=['SELECT ',Fields,' FROM gDataRaw WHERE replace(resppath,"\\","/") like "',DataPath,'%"',...
  ' AND not(cellid like "tst%") AND not(cellid like "Test%") AND not(parmfile like "%Test%") AND not(bad) order by masterid'];
if(strcmpi(exclude_current,'yes'))
    SQL=strrep(SQL,' order ',' AND trials IS NOT NULL order ');
end
RecsDB=mysql(SQL);
fprintf('Found %d recordings matching the FROM directory.\n',length(RecsDB));

host = HF_getHostname;
if strcmpi(host,'mole')
    ALT_LOCAL_DATA=1;
else
    ALT_LOCAL_DATA=0;
end

%% MODIFY PATHS FOR CERTAIN COMPUTERS
Ind = [];
for i=1:length(RecsDB)
    if ALT_LOCAL_DATA
        RecsDB(i).dbpath = RecsDB(i).resppath;
        RecsDB(i).resppath = strrep(RecsDB(i).resppath,sFrom,LOCAL_DATA_ROOT);
        RecsDB(i).respfileevp = strrep(RecsDB(i).respfileevp,sFrom,LOCAL_DATA_ROOT);
    end
    if exist(RecsDB(i).resppath,'dir') 
        Ind(end+1) = i; 
        RecsDB(i) = LF_evalScript(RecsDB(i));
    end
end
fprintf('Removed %d recordings not accessible from this computer.\n',length(RecsDB)-length(Ind));
RecsDB = RecsDB(Ind);

if ALT_LOCAL_DATA
    sFromOld = sFrom;
    sFrom = LOCAL_DATA_ROOT;
    fprintf('Adjusting sFrom from %s to %s\n',sFromOld,sFrom);
end

OldPath=pwd; FlushMessages = {}; Sep = filesep;

%% FIND EMPTY TRIAL COUNTS AND SET TO BAD 
% (ASSUMING THAT NO M-FILE AND AN UNFINISHED REP MAKE IT LIKELY IT  WAS AN ERROR)
% USER CAN PREVENT THIS BY SETTING THE TRIAL COUNT TO SOMETHING NON-NULL
if ~isempty(RecsDB) && ~strcmpi(BAPHY_LAB,'lbhb') && ~strcmpi(BAPHY_LAB,'yale')

  Trials = {RecsDB.trials}; Ind = cellfun(@isempty,Trials);
  RecsBAD = RecsDB(Ind);
  for i=1:length(RecsBAD)
     fprintf(['Setting [ ',n2s(sum(Ind)),' ] Recordings without finished repetition to ''BAD''\n']);
     R = mysql(['UPDATE gDataRaw SET bad=1 WHERE id=',n2s(RecsBAD(i).id),'']);
  end
  RecsDB = RecsDB(~Ind);
end

fprintf('Flushing these %d recordings:\n',length(RecsDB))
disp({RecsDB.parmfile}')
%RecsDB(~arrayfun(@(x)contains(x.cellid,'TBR008'),RecsDB))=[];  %use to only flush a particular site
%% LOOP OVER DATASETS (RECORDINGS/TRAININGS)
for ii=1:length(RecsDB)
  ErrorOccured=0; ErrorMessage=[]; % INITIALIZE ERROR STATE
  
  % one iteration per parmfile/gDataRaw entry
  MFileBase=RecsDB(ii).parmfile;
  fprintf(['\n',repmat('-',1,25),' < ',MFileBase,' >',repmat('-',1,25),'\n\n']);
  
  MFile=[strrep(RecsDB(ii).resppath,'/',Sep) MFileBase];
  if (MFile(2)==':') MFile(1)=upper(MFile(1)); end
  MFile=strrep(MFile,[Sep Sep],Sep);
  MFileRemote=strrep(MFile,MFile(1:length(sFrom)),sTo);
  
  %% On the first time, confirm paths:
  %if ii==1
  %    resp = questdlg(['Flushing from ' MFile ' to ' MFileRemote '. Is this correct?'],'Confirm correct path', 'Yes','Cancel','Yes');
  %    if strcmp(resp,'Cancel'), disp('Flush cancelled'); return; end
  %end
    
  %% TRY TO CONVERT ALPHA DATA TO EVP
  try
    if exist(MFile,'file') && length(MFileBase)>=1 && (MFile(1)<'0' || MFileBase(1)>'9'),
      clear exptparams globalparams exptevents
      if VERBOSE fprintf('Loading M-File %s...\n',basename(MFile)); end
      
      % check if some data stored in different path -- likely Psi saving to
      % non-raw machine
      EVPFile=strrep(RecsDB(ii).respfileevp,'/',Sep);
      EVPPath=fileparts(EVPFile);
      if ~strcmpi(EVPPath(1:3),MFile(1:3))
          % if drive letter is different....
          LoadMFile(EVPPath);
      else
          LoadMFile(MFile);
      end
      if ~isempty(findstr('Yes',globalparams.Physiology)) && ...
          ~sum(globalparams.HWSetup==[7,8,10,12]) && ...
          (~isfield(globalparams.HWparams,'DAQSystem') || ...
              strcmp(globalparams.HWparams.DAQSystem,'AO')),
        alpha2evp(MFile);
      end
    end
  catch ME
    ErrorMessage = ['Error: ',strtrim(ME.message)];
    ErrorOccured = 1;
    if VERBOSE keyboard; end
  end
  
  %% FIX cellDB
  if isempty(RecsDB(ii).trials) && exist('globalparams','var'),
     fprintf('fixing celldb entries for possibily crashed baphy file\n');
     dbSaveBaphyData(globalparams,exptparams);
  end
  
  %% COPY FILES (BUT CHECK IF NECESSARY)
  if ~ErrorOccured % continue to end if error already occured
    if exist(MFile,'file') || exist(MFile,'dir')
      if VERBOSE fprintf('transfering %s --> %s\n',MFile,MFileRemote); end

      [bdest,DestPath]=basename(MFileRemote);
      if ~exist(DestPath,'dir')  mkdir(DestPath); end
      if ~exist([DestPath Sep 'tmp'],'dir'),
        mkdir([DestPath Sep 'tmp']);
      end
      
      if isfield(globalparams, 'Module') && strcmp(globalparams.Module,'Psi')
          % psi-format file. anything special here?
          disp('psi-format file detected...');
          psi_format=true;
          MFilePath=MFile;
          [PenPath,MFileName]=fileparts(MFile);
          TmpFileGlob=[PenPath Sep 'tmp' Sep MFileName '*'];
          TmpFiles=dir(TmpFileGlob);
          DestTmpPath = [DestPath, 'tmp', Sep];
          LocalTmpPath = [PenPath, Sep, 'tmp', Sep];
          DestRawPath = [DestPath,  MFileName];
          DestRawPathTrue = [DestRawPath Sep 'raw'];
          LocalRawPath=MFilePath;
          RawLocalGlob = [LocalRawPath,Sep,'*'];
          %EVPFile=[MFilePath Sep 'reward_contact_analog.zarr'];
          EVPFile=strrep(RecsDB(ii).respfileevp,'/',Sep);
          %[xx,EVPPath]=basename(EVPFile);

      else
          psi_format=false;
          [MFilePath,MFileName]=fileparts(MFile);
          EVPFile=[MFilePath Sep MFileName '.evp'];
          TmpFileGlob=[MFilePath Sep 'tmp' Sep MFileName '*'];
          TmpFiles=dir(TmpFileGlob);
          DestTmpPath = [DestPath, 'tmp', Sep];
          LocalTmpPath = [MFilePath, Sep, 'tmp', Sep];
          DestRawPath =[DestPath 'raw' Sep];
          DestRawPathTrue=DestRawPath;
          LocalRawPath=[MFilePath Sep 'raw' Sep];
          tmp=strsep(MFileName,'_'); Identifier=tmp{1};
          RawLocalGlob = [LocalRawPath,Identifier,'*'];
      end
      
      try
        
        %% COPY FILES IN BASE DIRECTORY
        if psi_format
            
        elseif ~isempty(findstr('Yes',globalparams.Physiology))
          % gzip local file before copying
          if VERBOSE disp(['compressing before copy to server: ',EVPFile]); end
          [EVPFileZip,runinfo]=evpcompress(EVPFile,globalparams);
          if exist(EVPFileZip,'file'),
            if exist(EVPFile,'file')  delete(EVPFile); end
            copyfile(EVPFileZip,DestPath);
          elseif exist(EVPFile), 
             % this happens for MANTA systems where evp doesn't actually
             % get compressed
            copyfile(EVPFile,DestPath);
          end
        elseif exist(EVPFile,'file')  copyfile(EVPFile,DestPath);
        else          warning(['training: no evp file? ',EVPFile]);
        end
        
        %% COPY TMP FILES
        if VERBOSE fprintf(['Copying Tmp-Files [ ',escapeMasker(LocalTmpPath),' => ',escapeMasker(DestTmpPath),' ] \n']); end
        for fileidx=1:length(TmpFiles),
          TmpFile=[LocalTmpPath TmpFiles(fileidx).name];
          copyfile(TmpFile,DestTmpPath);
        end
        
        %% COPY FILES IN RAW DIRECTORY
        if ~isempty(findstr('Yes',globalparams.Physiology))
          if ~exist(DestRawPath,'dir') mkdir(DestRawPath); end
          
          % EXTRACT IDENTIFIER (up to Recording) FROM MFILE NAME
          
          RawFiles=dir(RawLocalGlob);
          if psi_format
              res = copyfiletree(LocalRawPath, DestPath);
              if ~strcmpi(LocalRawPath, EVPPath)
                  res = copyfiletree(EVPPath, DestPath);
              end
          elseif ~isempty(RawFiles)
            if RawFiles(1).isdir % EVP5 FORMAT
              for fileidx=1:length(RawFiles)
                % COMPRESS DIRECTORIES (EVP5 FORMAT)
                cRawFile = RawFiles(fileidx).name;
                if strcmpi(cRawFile,'tmp') continue; end % SKIP TMP DIRECTORY
                if VERBOSE, fprintf(['Copying/Zipping Raw-Files [ ',escapeMasker([LocalRawPath,cRawFile]),' => ',escapeMasker(DestRawPath),' ] \n']); end
                DestRawPathTrue=[DestRawPath,cRawFile,Sep];
                if ~strcmp(globalparams.HWparams.DAQSystem,'Open-Ephys')
                    disp('copying raw data')
                    localcompress([DestRawPath,cRawFile,'.tgz'],[LocalRawPath Sep cRawFile]);
                    if VERBOSE, fprintf('Done copying and zipping\n'); end
                else
                    src_folder = fullfile(LocalRawPath, cRawFile);
                    res = copyfiletree(src_folder, DestRawPath);
                    fprintf('Copied %d files\n', res);
                    if 0
                        %copy header files to avoid having to unzip
                        RawFiles2=dir([LocalRawPath cRawFile]);
                        RawFolders={[RawFiles2(3).name Sep]};
                        if ~exist([LocalRawPath cRawFile Sep RawFolders{1} 'settings.xml'],'file')
                            dd=dir([LocalRawPath cRawFile Sep RawFolders{1}]);
                            dd(1:2)=[];
                            RawFolders = arrayfun(@(x)[RawFolders{1} x.name filesep],dd,'Uni',0);
                        end
                        for fi = 1:length(RawFolders)
                            fprintf(['Copying selected metadata files from ',escapeMasker([LocalRawPath cRawFile Sep RawFolders{fi}]),'\n'])
                            mkdir([DestRawPath cRawFile Sep RawFolders{fi}]);
                            if exist([LocalRawPath cRawFile Sep RawFolders{fi},'all_channels.events'],'file')
                                %OEP datatype
                                runinfo.datatype = 'OEP';
                                filenames={'all_channels.events','messages.events','settings.xml','Continuous_Data.openephys'};
                            else
                                %binary datatype
                                filenames={'settings.xml'};%Possibly add more later? Events folder, header portion of continuous data?
                                runinfo.datatype = 'binary';
                            end
                            for k=1:length(filenames)
                                [status,msg,msgid]=copyfile([LocalRawPath cRawFile Sep RawFolders{fi} filenames{k}],[DestRawPath cRawFile Sep RawFolders{fi} filenames{k}]);
                                if(~status)
                                    error(['Error copying ',[LocalRawPath cRawFile Sep RawFolders{fi} filenames{k}],'. ',msg])
                                end
                            end
                            if strcmp(runinfo.datatype,'binary')
                                dd=dir([LocalRawPath cRawFile Sep RawFolders{fi}]);
                                dd(1:2)=[];
                                dd(~[dd.isdir])=[];
                                if length(dd) > 1
                                    error(['Expected just one folder for one recording (usually ''experiment1'').',...
                                        'Other functions (like OEPgetinfo, evpread) won''t know what to do with ',...
                                        'multiple folders. Fix this.'])
                                end
                                RawFolder = [RawFolders{fi} dd.name Sep];
                                mkdir([DestRawPath cRawFile Sep RawFolder]);
                                dd=dir([LocalRawPath cRawFile Sep RawFolder]);
                                dd(1:2)=[];
                                dd(~[dd.isdir])=[];
                                if length(dd) > 1
                                    error(['Expected just one folder for one recording (usually ''recording1'').',...
                                        'Other functions (like OEPgetinfo, evpread) won''t know what to do with ',...
                                        'multiple folders. Fix this.'])
                                end
                                RawFolder = [RawFolder dd.name Sep];
                                mkdir([DestRawPath cRawFile Sep RawFolder]);
                                dd=dir([LocalRawPath cRawFile Sep RawFolder]);
                                dd(1:2)=[];
                                dd(~[dd.isdir])=[];
                                filenames={'structure.oebin','sync_messages.txt'};
                                for k=1:length(filenames)
                                    [status,msg,msgid]=copyfile([LocalRawPath cRawFile Sep RawFolder filenames{k}],[DestRawPath cRawFile Sep RawFolder filenames{k}]);
                                    if(~status)
                                        error(['Error copying ',[LocalRawPath cRawFile Sep RawFolder filenames{k}],'. ',msg])
                                    end
                                end

                                %Copy files in events folder
                                json_files = {runinfo.json_file,runinfo.json_file_LFP, runinfo.json_file_spikes};
                                json_files(cellfun(@isempty,json_files))=[];
                                matchi = contains(json_files,RawFolder);
                                if sum(matchi)==1
                                    this_json_file = json_files{matchi};
                                else
                                    error('%d matching json files found in trying to copy events and spikes from %s. Expected exactly 1 match.',sum(matchi),RawFolder)
                                end
                                events_folders = list_open_ephys_binary(this_json_file,'events');
                                seps = strfind(events_folders{runinfo.event_ind_TTL},'/');
                                mkdir([DestRawPath cRawFile Sep RawFolder 'events']);
                                for i=1:length(seps)
                                    mkdir([DestRawPath cRawFile Sep RawFolder 'events' Sep events_folders{runinfo.event_ind_TTL}(1:seps(i))]);
                                end
                                evpath = [cRawFile Sep RawFolder 'events' Sep events_folders{runinfo.event_ind_TTL}];
                                dd=dir([LocalRawPath evpath]);
                                dd(1:2)=[];
                                %Copy .npy files in events TTL folder
                                for k=1:length(dd)
                                    [status,msg,msgid]=copyfile([LocalRawPath evpath dd(k).name], [DestRawPath evpath dd(k).name]);
                                    if(~status)
                                        error(['Error copying ',[LocalRawPath evpath dd(k).name],'. ',msg])
                                    end
                                end

                                %Copy files in spikes folder if it exists
                                spikes_folders = list_open_ephys_binary(this_json_file,'spikes');
                                if isempty(spikes_folders)
                                elseif length(spikes_folders) == 1
                                    mkdir([DestRawPath cRawFile Sep RawFolder 'spikes']);
                                    seps = strfind(spikes_folders{1},'/');
                                    spikes_path = [cRawFile Sep RawFolder 'spikes' Sep spikes_folders{1}(1:seps(1))];
                                    [status,msg,msgid]=copyfile([LocalRawPath spikes_path], [DestRawPath spikes_path]);
                                    if(~status)
                                        error(['Error copying ',[LocalRawPath spikes_path],'. ',msg])
                                    end
                                else
                                    error('Multiple spikes folders. Figure out what to do with this.')
                                end
                            end
                        end
                    end
                end
              end
            else % ALPHA OMEGA BASED FORMAT
              if VERBOSE disp('Copying Raw Files'); end
              % COPY FILES DIRECTLY
              if VERBOSE,
                disp(['tar-zipping during copy to server: ',DestRawPath,MFileName,'.tgz']);
              end
              cd(LocalRawPath);
              tMFileName=['i' MFileName(2:end) '*'];  % ALPHAOMEGA FILES ("i" prefixed)
              AOFiles=dir(tMFileName);
              FileList={RawFiles.name AOFiles.name};
              tar([DestRawPath,MFileName,'.tgz'],FileList);
              DestRawPathTrue=DestRawPath;
            end
          else
            fprintf(['WARNING : Files not found for :',escapeMasker(RawLocalGlob),'\n']);
          end
        else
          DestRawPath=''; DestRawPathTrue='';
        end
        
        %% IF EXISTS, COPY PUPIL VIDEO
        NewPupilFile='';
        if psi_format
            PupilDestPath=[DestRawPath Sep];
        else
            PupilDestPath=DestPath;
        end
        if isfield(globalparams,'PupilFilename'),
            if ALT_LOCAL_DATA
                PupilFilename = strrep(globalparams.PupilFilename, '\', Sep);
                PupilFilename = strrep(PupilFilename, sFromOld, sFrom);
            else
                PupilFilename = globalparams.PupilFilename;
            end
            if VERBOSE,
                fprintf(['Copying pupil file [ ',escapeMasker(PupilFilename),' => ',escapeMasker(PupilDestPath),' ] \n']);
            end
            try
                copyfile(PupilFilename,PupilDestPath);
                NewPupilFile = [PupilDestPath,basename(PupilFilename)];
            catch err
                try
                    copyfile(strcat(PupilFilename, '.avi'),PupilDestPath);
                    NewPupilFile = [PupilDestPath,basename(PupilFilename),'.avi'];
                catch err2
                    warning(err2.identifier,'Error copying pupil file!: %s',err2.message)
                end
            end
            [d,fn,ext]=fileparts(globalparams.PupilFilename);
            PupilFilename2=[d,filesep,fn,'_2',ext];
            if exist(PupilFilename2,'file')
                copyfile(PupilFilename2,PupilDestPath);
                if VERBOSE
                    fprintf(['Copying second pupil file [ ',escapeMasker(PupilFilename2),' => ',escapeMasker(PupilDestPath),' ] \n']);
                end
            end
        end
        
        %% IF EXISTS, COPY LICK VIDEO
        if isfield(globalparams,'LickFilename')
            if ALT_LOCAL_DATA
                LickFileName = strrep(globalparams.LickFilename, '\', Sep);
                LickFileName = strrep(LickFileName, sFromOld, sFrom);
            else
                LickFileName = globalparams.LickFilename;
            end
           
            if VERBOSE
                fprintf(['Copying lick file [ ',escapeMasker(LickFileName),' => ',escapeMasker(PupilDestPath),' ] \n']);
            end
            try
                copyfile(LickFileName,PupilDestPath);
            catch err
                try
                    copyfile(strcat(LickFileName, '.avi'),PupilDestPath);
                catch err2
                    warning('Error copying lick file!: %s',err2.message)
                end
            end
        end        
        %% LAST: COPY MFILE
        % so that it won't try to update database without this happening.
        if ~psi_format
            if VERBOSE
                fprintf(['Copying M-File [ ',escapeMasker(MFile),' => ',escapeMasker(DestPath),' ] \n']);
            end

            copyfile(MFile,DestPath);
        end
      catch ME
       % rethrow(ME);
        ErrorMessage = ['Error : ',strtrim(ME.message) ' Stack:'];
        warning(ErrorMessage)
        fprintf('Stack:\n')
        for i=1:length(ME.stack)
            disp(ME.stack(i))
            ErrorMessage = [ErrorMessage ' ' num2str(i) ': file: ' ME.stack(i).file ' line: ' num2str(ME.stack(i).line) ','];
            if strcmp(ME.stack(i).name,'flush_data_to_server')
                ErrorMessage(end)=[];
                break
            end
        end
        ErrorOccured = 1;
        if VERBOSE keyboard; end
      end
    else
      ErrorOccured = 1;
      ErrorMessage = ['Warning : m-File does not exist at ORIGIN (BAD or on other computer)'];
    end
  end
  
  %% UPDATE DB TO REFLECT DESTINATION AS NEW LOCATION
  if ~ErrorOccured
    if exist(MFileRemote,'file')
    
      [MFileRemoteName,MFileRemotePath]=basename(MFileRemote);
      EVPFileBase=basename(RecsDB(ii).respfileevp);
      if VERBOSE disp('Moving Location in CellDB'); end
      dbopen;  % reactivate connection to celldb
      SQL=['UPDATE gDataRaw SET resppath="',MFileRemotePath,'",',...
        'parmfile="',MFileRemoteName,'",',...
        'respfile="',DestRawPathTrue,'",',...
        'eyecalfile="',NewPupilFile,'",',...
        'respfileevp="',EVPFileBase,'"',...
        ' WHERE id=',num2str(RecsDB(ii).id)];
      mysql(SQL);
    else
      ErrorOccured = 1;
      ErrorMessage = ['Warning : m-File already exists at DESTINATION (Mark BAD!)'];
    end
  end
  
  % COLLECT ERROR MESSAGES
  if ErrorOccured Message = ErrorMessage; else Message = ' OK'; end
  FlushMessages{end+1} = LF_createFlushMessage(MFile,Message);
  fclose('all');
end % RECORDING LOOP

%% SEND AN EMAIL WITH WHAT HAS FLUSHED AND WHAT HASN'T
if ~isempty(NotificationEmail)
  WinVersion = evalc('! ver');
  Pos = strfind(WinVersion,'[Version ');
  WinVersion = str2num(WinVersion(Pos+9:Pos+11));
  if WinVersion >=6 % JAVA FRAME WORK FOR SENDING MAILS NOT AVAIL. BEFORE WIN7
    HF_sendEmail('To',NotificationEmail,...
        'Subject',['Flush Results from ',HF_getHostname],'Body',FlushMessages);
  end
end

fprintf('\n< FLUSH FINISHED >\n'); cd(OldPath);
FlushMessages
logfile=[sFrom,'Flush_logs',filesep,strrep(strrep(datestr(now),' ' ,'_'),':','-'),'.mat'];
if length(RecsDB)>1
    UTmkdir([sFrom,'Flush_logs',filesep])
    save(logfile,'FlushMessages');
    fprintf('\nSaved flush log to:  %s',logfile)
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function FlushMessage = LF_createFlushMessage(ParmFile,Message)
FlushMessage = [Message,' : ',ParmFile];

function DBEntry = LF_evalScript(DBEntry)

switch lower(HF_getHostname)
  case 'deepthought';
    NetPath = 'D:/Data/';
    LocalPath = MD_getDir('Kind','archive','DB',0);
    LocalPath(LocalPath=='\') = '/';
    SQL = ['UPDATE gDataRaw SET ',...
      'resppath=replace(resppath,"',NetPath,'","',LocalPath,'"), ',...
      'eyecalfile=replace(eyecalfile,"',NetPath,'","',LocalPath,'"), ',...
      'respfile=replace(respfile,"',NetPath,'","',LocalPath,'") ',...
      'WHERE parmfile like "',DBEntry.cellid,'%" AND not(bad);'];
   R = mysql(SQL);
   DBEntry.resppath = strrep(DBEntry.resppath,NetPath,LocalPath);
   DBEntry.respfileevp = strrep(DBEntry.respfileevp,NetPath,LocalPath);
   if ~R error('Something went wrong while executing the SQL command.'); end
  otherwise
end
