function [params,output]=ANfit_general(xdata,ydata,ydata_std,model,start_point,low_bounds,high_bounds,options)

%estimates = fminsearch(model, start_point);
options.MaxFunEvals=10^4;
options.MaxIter=10^4;
options.OutputFcn=[];
options.TolFun=10^-6;
options.TolX=10^-6;
options.Display='off';
fixed_params=getparm(options,'fixed_params',false(size(start_point)));
keep_track=getparm(options,'keep_track',0);

if isempty(ydata_std)
    ydata_std=zeros(size(ydata));
end
fixed_vals=start_point(fixed_params);

fitter = @fit_fun;
sse_=[];
params__=[];
start_point(fixed_params)=[];
low_bounds(fixed_params)=[];
high_bounds(fixed_params)=[];
start_point=double(start_point);
[estimates,trash,trash,output] = fminsearchbnd(fitter, start_point,low_bounds,high_bounds,options);
params(~fixed_params)=estimates;
params(fixed_params)=fixed_vals;


if(0)
    x_interp=linspace(min(xdata),max(xdata),length(xdata)*100);
    y_fit_interp=model(x_interp,params);
    figure;
    plot(xdata,ydata,'.');
    hold on;
    plot(x_interp,y_fit_interp);
end
a=2;    

    function [sse, FittedCurve] = fit_fun(params_,varargin)
        this_params(fixed_params)=fixed_vals;
        this_params(~fixed_params)=params_;
        FittedCurve = model(xdata,this_params);
        if(all(ydata_std==0))
            sse = nansum((FittedCurve - ydata).^2);
        else
            sse = nansum( robust_weights.*(1./ydata_std.^2).*(FittedCurve - ydata).^2);
        end
        if(keep_track)
            sse_(end+1)=sse;
            params__(end+1,:)=this_params;
        end
        %        ErrorVector = FittedCurve - ydata;
        %sse = nansum(ErrorVector .^ 2);
    end
end

