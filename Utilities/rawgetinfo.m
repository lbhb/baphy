function runinfo=rawgetinfo(evpfile,globalparams,evpv)
%runinfo=rawgetinfo(evpfile,globalparams)
%runinfo=rawgetinfo(evpfile,[],evpv)

if(isempty(globalparams))
    if(nargin<3)
        error('if globalparams is not defined, evpv must be defined')
    end
else
    if isfield(globalparams.HWparams,'DAQSystem') && ...
            strcmp(globalparams.HWparams.DAQSystem,'Open-Ephys')
        evpv=6;%6 means Open-Ephys format
    else
        evpv=evpversion(evpfile);
    end
end

if(evpv==6)
    not_filesep=setdiff({'/','\'},filesep);not_filesep=not_filesep{1};
    OEPdir=strrep(globalparams.rawfilename,not_filesep,filesep);

    % if old version of OE, this file should exist one step down from
    % OEPdir
    dd=glob([OEPdir filesep '*' filesep 'settings.xml']);
    if ~isempty(dd)
        try
            runinfo=OEPgetinfo(OEPdir);
        catch
            % otherwise OE 0.6
            runinfo=OEPgetinfo6(OEPdir);
        end
    else
        % otherwise OE 0.6
        runinfo=OEPgetinfo6(OEPdir);
    end

    % get aux info from baphy file still
%     [spikechancount,auxchancount,trialcount,spikefs,auxfs,lfpchancount,lfpfs]=...
%         evpgetinfo(strrep(evpfile,'.m',''));
    evpfilename=strrep(evpfile,'.m','');
    if ~isempty(globalparams) && isfield(globalparams,'evpfilename')
        evpfilename=globalparams.evpfilename;
        %use evpfile from globalparams. Otherwise trying to use tgz file sometimes.
    end
    try
        [spikechancount,auxchancount,trialcount,spikefs,auxfs,lfpchancount,lfpfs]=...
            evpgetinfo(evpfilename);
    catch
        disp('psi file?  not supporting aux data')
        auxchancount=0;
        auxfs=0;
    end
    runinfo.auxchancount=auxchancount;
    runinfo.auxfs=auxfs;
else
    [spikechancount,auxchancount,trialcount,spikefs,auxfs,lfpchancount,lfpfs]=...
        evpgetinfo(evpfile);
    runinfo.spikechancount=spikechancount;
    runinfo.auxchancount=auxchancount;
    runinfo.trialcount=trialcount;
    runinfo.spikefs=spikefs;
    runinfo.auxfs=auxfs;
    runinfo.lfpchancount=lfpchancount;
    runinfo.lfpfs=lfpfs;
    
    if evpv==5,
        lfpfs=1000;
    end
    runinfo.spike_channels=1:spikechancount;
end
runinfo.evpv=evpv;
