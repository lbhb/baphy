% function p=getparm(struct,parmname,parmdef);
function p=getparm(struct,parmname,parmdef,transform_YesNo)

if nargin<4
    transform_YesNo=false;
end
try_others=true;
if isfield(struct,parmname)
   p=struct.(parmname);
   if transform_YesNo
       if strcmpi(p,'no')
          p=false;
       elseif strcmpi(p,'Yes')
           p=true;
       end
   end
   try_others=false;
elseif isobject(struct)
    try
        p=get(struct,parmname);
        if transform_YesNo
            if strcmpi(p,'no')
                p=false;
            elseif strcmpi(p,'Yes')
                p=true;
            end
        end
        try_others=false;
    end
end
if try_others
    if strcmp(parmdef,'ERROR')
        error('parameter %s required!',parmname);
        p=[];
    else
        p=parmdef;
    end
end