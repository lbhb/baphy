function [rs,strialidx,ra,atrialidx,rl,ltrialidx,Info] = evpread(filename,varargin);
% function [rS,STrialIdx,rA,ATrialIdx,rL,LTrialIdx]=evpread(filename,spikechans[=all],
%                                                   auxchans[=none],trials[=all],lfpchans[=none]);
%
% Read EVP files versions 3-5
%
% Arguments: (can be provided in sequence or as name-value-pairs)
% - rawchans     : set of raw channels
% - spikechans  : set of spike channels [all] (not mapped; 1 and 2 are usually distant)
% - spikeelecs  : set of spike electrodes (mapped; 1 and 2 are close to eachother).
% - auxchans     : set of auxilliary channels (only V3-4) [none]
% - trials              : set of trials [all]
% - lfpchans       : set of LFP channels [none]
% - filterstyle      : how to filter the spike-data
%     'none';         : no filtering
%     'filtfilthum'   : combines faithulness with some speed increase [default]
%     'filtfiltold'     : old filtering, strong, possibly too strong: introduces some ringing
%     'butter'        : very fast, not strong enough for heavy noise, some distortion, a few steps shift.
% - dataformat  : 'linear' (Linear Data + TrialIndices) or 'separated' (Separated in a Cell)
%
% Return variables :
% NOTE: if only a single argument is returned, it is a struct with the
% requested fields, othewise the following arguments are returned:
% - rS               : spike data (time X spike channels)
% - STrialIdx   : the index of the first sample in each trial for the spike data
% - rA               : auxiliary data (time X aux channels)
% - ATrialIdx   : the index of the first sample in each trial for the auxiliary data
% - rL               : LFP data (time X lfp channels)
% - LTrialIdx   : the index of the first sample in each trial for the LFP data
% - Info            : Struct containing information about the recording
%
% Examples:
% For a single output argument (a struct with all results field)
% R = evpread('pathtoyourfilename.evp','spikeelecs',[1:10],'lfpchans',[1:5],'trials',[1:50]);
%
% For multiple output arguments
% [RSpike,SpikeIndices,tmp,tmp,RLFP,LFPIndices] = evpread('pathtoyourfilename.evp',[1:10],[],[1:50],[1:5]);
%
% Loading the first twp channels from an Open-Ephys file without filtering:
% R=evpread('/auto/data/daq/Fred/fre196/raw/fre196a01.tgz','spikeelecs',[1 2],'filterstyle','none')
%
% created SVD 2005-11-07
% modified BE 2010-12-14
global C_r C_raw C_lfp C_mfilename C_evpfilename C_ENABLE_CACHING USECOMMONREFERENCE COMMONREFERENCECHANNELS

if isempty(C_ENABLE_CACHING) C_ENABLE_CACHING=1; end
ENABLE_MAP_READ=1;
if isempty(USECOMMONREFERENCE) USECOMMONREFERENCE = 0; end
Info = [];

%% PARSE ARGUMENTS
if ~isempty(varargin) && ~ischar(varargin{1}) % OLD ARGUMENT STYLE
    %P.spikechans = varargin{1};
    P.spikeelecs = varargin{1};
    if length(varargin)>1 P.auxchans = varargin{2}; end
    if length(varargin)>2 P.trials = varargin{3}; end
    %if length(varargin)>3 P.lfpchans = varargin{4}; end
    if length(varargin)>3 P.lfpelecs = varargin{4}; end
else % NEW ARGUMENT STYLE
    P = parsePairs(varargin);
end
if ~isfield(P,'spikechans'),
    if ~isfield(P,'spikeelecs') | isempty(P.spikeelecs),
        %if isempty(P.spikeelecs),   %replaced this line with above -KJD  %6.25.2012
        P.spikechans = [];
        P.spikeelecs = [];
    else
        P.spikechans = inf;
    end
end
if ~isfield(P,'auxchans') P.auxchans = [ ]; end
if ~isfield(P,'lfpchans') P.lfpchans = [ ]; end
if ~isfield(P,'rawchans') P.rawchans = [ ]; end
if ~isfield(P,'trials') || isempty(P.trials), P.trials = inf; end
if(~isfield(P,'runinfo'))
    mf=strrep(strrep(filename,'.tgz','*'),'/raw','');
    mf=fileparts(mf);
    
    mf=dir(mf);mf(1:2)=[];
    mf=mf(find(arrayfun(@(x)strcmp(x.name(end-1:end),'.m'),mf),1));
    if strcmpi(filename(end-3:end),'.tgz') && ~isempty(mf) && exist([mf.folder,filesep,mf.name],'file')
        MF=LoadMFile([mf.folder,filesep,mf.name]);
        evpv=evpversion(MF.globalparams);
        if evpv==6
            P.globalparams=MF.globalparams;
        end
    elseif isempty(P.spikechans) && exist(filename,'file'),
        evpv=evpversion(filename,0);  % 0 means don't remap to
        % MANTA evp file
    else
        evpv=evpversion(filename);
    end
    if any(evpv==[4 5])
        P.runinfo=rawgetinfo(filename,[],evpv);
    elseif evpv==6
        P.runinfo=rawgetinfo(filename,P.globalparams,evpv);
    end
else
    evpv=P.runinfo.evpv;
end
if ~isfield(P,'output_common_ref') P.output_common_ref = false; end
%% CHECK EVP VERSION

if evpv==6 %6 means Open-Ophys format
    not_filesep=setdiff({'/','\'},filesep);not_filesep=not_filesep{1};
    P.globalparams.rawfilename=strrep(P.globalparams.rawfilename,not_filesep,filesep);
end

if ~isfield(P,'filterstyle'),
    if any(evpv==[5 6]),
        P.filterstyle = 'butter';
    else
        P.filterstyle = 'none';
    end
end
if ~isfield(P,'lfp_filter_freqs'), P.lfp_filter_freqs=[]; end
if ~isfield(P,'wrap') P.wrap = 0; end
if ~isfield(P,'SRlfp') P.SRlfp = 2000; end % downsample
if ~isfield(P,'dataformat') P.dataformat = 'linear'; end

%% CHECK FOR RESORTING OF CHANNELS (MAPS CHANNEL TO ELECTRODES)
if(evpv==6)
    if((isempty(P.spikechans)||any(isinf(P.spikechans))) && ~isempty(P.spikeelecs))
        if ~isfield(P.runinfo,'processors')
            P.spikechans=P.spikeelecs;
        elseif isfield(P.runinfo.processors(P.runinfo.raw_data.source_ind).channels(P.spikeelecs),'map')
            P.spikechans=[P.runinfo.processors(P.runinfo.raw_data.source_ind).channels(P.spikeelecs).map];
        elseif ~isempty(P.runinfo.raw_data_channel_map)
            P.spikechans=P.runinfo.raw_data_channel_map(P.spikeelecs);
        else
            P.spikechans=P.spikeelecs;
        end
        fprintf('Spike (El => Ch) : ');
        for i=1:length(P.spikechans) fprintf([' %d=>%d | '],P.spikeelecs(i),P.spikechans(i)); end
    end
    if((isempty(P.lfpchans)||any(isinf(P.lfpchans))) && isfield(P,'lfpelecs'))
        if strcmp(P.runinfo.processors(P.runinfo.raw_data.source_ind).channels(1).name, 'ADC1')
            P.lfpchans=1; % hack for adc channels
        elseif isfield(P.runinfo.processors(P.runinfo.raw_data.source_ind).channels(P.lfpelecs),'map')
            P.lfpchans=[P.runinfo.processors(P.runinfo.raw_data.source_ind).channels(P.lfpelecs).map];
        elseif isfield(P.runinfo.processors(P.runinfo.raw_data.source_ind+1).channels(P.lfpelecs),'map')
            P.lfpchans=[P.runinfo.processors(P.runinfo.raw_data.source_ind+1).channels(P.lfpelecs).map];
            warning('No channel map in saved data, using map in next node.')
        else
            error('No channel map in saved data.')
        end
        
        fprintf('LFP (El => Ch) : ');
        for i=1:length(P.lfpchans) fprintf([' %d=>%d | '],P.lfpelecs(i),P.lfpchans(i)); end
    end
else
    if dbopen,
        P = LF_checkElecs2Chans(filename,P);
    else
        disp('no celldb connection, skipping LF_checkElecs2Chans');
    end
end

if isempty(P.spikechans) && isempty(P.lfpchans) && isempty(P.auxchans)
    warning('spikechans empty, setting to all channels (unmapped)')
    %P.spikechans=1:P.runinfo.spikechancount;
end

%% Make local (or not)
%If (loading spikes or only zipped file exists) and this is not an open-ephys format directory (but do if it's a compressed file), make a local copy
if ( ~isempty(P.spikechans) || ~exist(filename,'file')) && (exist(filename,'file')==2 || evpv~=6),
    evplocal=evpmakelocal(filename,evpv);
    if isempty(evplocal)
        error('evp file not found');
    else
        filename=evplocal;
        if evpv==6 && ~contains(filename,'.evp')
            P.globalparams.rawfilename=evplocal;
            if isfield(P.runinfo,'json_file')
                raw_st_remote = strfind(P.runinfo.json_file,[filesep 'raw' filesep]);
                raw_st_local = strfind(evplocal,[filesep 'raw' filesep]);
                P.runinfo.json_file=[evplocal(1:raw_st_local) P.runinfo.json_file(raw_st_remote+1:end)];
            end
        end
    end
end
if (exist(filename,'file')==7 && evpv==6)
    P.globalparams.rawfilename=filename;
end



%% BRANCH FOR DIFFERENT EVP VERSIONS
switch evpv
    case 3;
        %disp('EVP version 3 ...');
        [rs,strialidx,ra,atrialidx]=...
            evpread3(filename,P.spikechans,P.auxchans,P.trials);
        rl=[]; ltrialidx=[];
        
    case 4;
        %disp('EVP version 4 ...');
        [fid,sError]=fopen(filename,'r','l');
        header(1:7)=double(fread(fid,7,'uint32'));
        header(8:10)=double(fread(fid,3,'single'));
        spikechancount=header(2);
        auxchancount=header(3);
        trialcount=header(6);
        lfpchancount=header(7);
        lfpfs=header(8);
        if isinf(P.spikechans) P.spikechans=1:spikechancount; end
        if isinf(P.trials)  P.trials=1:header(6); end
        
        if spikechancount<max(P.spikechans),
            error('invalid spike channel');
        end
        if auxchancount<max(P.auxchans),
            error('invalid aux channel');
        end
        if trialcount<max(P.trials),
            error('invalid trial range');
        end
        
        rs=[];
        ra=[];
        rl=[];
        strialidx=[];
        atrialidx=[];
        ltrialidx=[];
        if length(P.spikechans)==0;
            spikechansteps=spikechancount;
        else
            spikechansteps=diff([0; P.spikechans(:); spikechancount+1])-1;
        end
        if length(P.auxchans)==0;
            auxchansteps=auxchancount;
        else
            auxchansteps=diff([0; P.auxchans(:); auxchancount+1])-1;
        end
        if length(P.lfpchans)==0;
            lfpchansteps=lfpchancount;
        else
            lfpchansteps=diff([0; P.lfpchans(:); lfpchancount+1])-1;
        end
        
        % try to load from cache
        if C_ENABLE_CACHING && ...
                (~strcmp(basename(C_evpfilename),basename(filename)) || isempty(C_raw)),
            C_evpfilename=filename;
            C_mfilename=strrep(filename,'.evp','');
            C_raw={};
            C_lfp={};
            C_r={};
        end
        
        % try to load from map files... faster?
        [pp,bb]=fileparts(filename);
        mappath=[pp,filesep,'tmp',filesep];
        if ENABLE_MAP_READ && length(P.auxchans)==0 && exist([mappath sprintf([bb '%.3d.map'],P.trials(1))],'file'),
            if P.trials(1)==1,
                disp('loading from MAPs');
            end
            for i = 1:spikechancount
                channame{i} = ['SPK',num2str(i)];
            end
            for i = 1:lfpchancount
                lfpchanname{i} = ['LFP',num2str(i)];
            end
            
            for tt=1:length(P.trials),
                trialnum=P.trials(tt);
                evpdaq=[mappath sprintf([bb '%.3d.map'],trialnum)];
                
                data = [mapread(evpdaq,'Channels',channame,'DataFormat','Native')];
                ldata = [mapread(evpdaq,'Channels',lfpchanname,'DataFormat','Native')];
                
                if ~iscell(data) data = {data};end
                if C_ENABLE_CACHING,
                    if tt==1,
                        fprintf('%s: Creating evp cache for %s\n',mfilename,filename);
                    end
                    C_raw{trialnum}=cat(2,data{:});
                    C_lfp{trialnum}=cat(2,ldata{:});
                end
                strialidx=[strialidx; length(rs)+1];
                rs=cat(1,rs,double(C_raw{trialnum}(:,P.spikechans)));
                ltrialidx=[ltrialidx; length(rl)+1];
                rl=cat(1,rl,double(cat(2,ldata{:})));
            end
            
        elseif C_ENABLE_CACHING && length(P.auxchans)==0 && ...
                length(C_raw)>=max(P.trials) && ~isempty(C_raw{max(P.trials)}) &&...
                ~isempty(C_raw{1}),
            
            % ie, contents of evp file were already saved in C_raw
            for tt=P.trials(:)',
                strialidx=[strialidx; length(rs)+1];
                atrialidx=[atrialidx; length(ra)+1];
                ltrialidx=[ltrialidx; length(rl)+1];
                
                rs=cat(1,rs,C_raw{tt}(:,P.spikechans));
                rl=cat(1,rl,C_lfp{tt}(:,P.lfpchans));
            end
            rs=double(rs);
            rl=double(rl);
        else
            
            for tt=1:max(P.trials),
                %drawnow;
                
                trhead=fread(fid,3,'uint32');
                if isempty(trhead) break; end
                ts=[];
                ta=[];
                tl=[];
                if ismember(tt,P.trials),
                    
                    % read spike data
                    for ii=1:length(P.spikechans),
                        if spikechansteps(ii)>0,
                            % skip data from unwanted channels
                            fseek(fid,trhead(1)*2*spikechansteps(ii),0);
                        end
                        ts=[ts fread(fid,trhead(1),'short')];
                    end
                    if spikechansteps(length(P.spikechans)+1)>0,
                        fseek(fid,trhead(1)*2*spikechansteps(length(P.spikechans)+1),0);
                    end
                    
                    %read aux data
                    for ii=1:length(P.auxchans),
                        if auxchansteps(ii)>0,
                            fseek(fid,trhead(2)*2*auxchansteps(ii),0);
                        end
                        ta=[ta fread(fid,trhead(2),'short')];
                    end
                    fseek(fid,trhead(2)*2*auxchansteps(length(P.auxchans)+1),0);
                    
                    %read LFP data
                    for ii=1:length(P.lfpchans),
                        if lfpchansteps(ii)>0,
                            fseek(fid,trhead(3)*2*lfpchansteps(ii),0);
                        end
                        tl=[tl fread(fid,trhead(3),'short')];
                    end
                    fseek(fid,trhead(3)*2*lfpchansteps(length(P.lfpchans)+1),0);
                    
                    strialidx=[strialidx; length(rs)+1];
                    atrialidx=[atrialidx; length(ra)+1];
                    ltrialidx=[ltrialidx; length(rl)+1];
                    rs=cat(1,rs,ts);
                    ra=cat(1,ra,ta);
                    rl=cat(1,rl,tl);
                else
                    % jump to start of next trial
                    fseek(fid,(trhead(1)*spikechancount+trhead(2)*auxchancount+ ...
                        trhead(3)*lfpchancount)*2,0);
                end
            end
        end
        
        
        fclose(fid);
        
        %% FILTER LFP
        if 0 && ~isempty(P.lfpchans)
            order = 2; Nyquist = P.SRlfp/2;
            fHigh = 1; fLow = 0.3*Nyquist;
            [bLow,aLow] = butter(order,fLow/Nyquist,'low');
            [bHigh,aHigh] = butter(order,fHigh/Nyquist,'high');
            if P.wrap
                tmp = single(NaN*zeros(round((max(diff(cstrialidx))-1)/lfpfs*P.SRlfp),length(P.lfpchans),length(cstrialidx)-1));
                for i=1:length(cstrialidx)-1
                    tmp2 = single(resample(double(rl(cstrialidx(i):cstrialidx(i+1)-1,:)),P.SRlfp,lfpfs));
                    tmp(1:length(tmp2),:,i) = tmp2;
                end
                rl = tmp;
            else
                rl = single(resample(double(rl),P.SRlfp,lfpfs));
                ltrialidx = ceil(ltrialidx*P.SRlfp/lfpfs);
            end
            rl = filter(bLow,aLow,rl);
            rl = filter(bHigh,aHigh,rl);
            
            %bHumbug = [0.997995527211068  -5.987297083916456  14.967228743433322 -19.955854373444378  14.967228743433322  -5.987297083916456   0.997995527211068];
            %aHumbug = [1.000000000000000  -5.995310048314492  14.977237236960848 -19.955846338529373  14.957216231994666  -5.979292154433458   0.995995072333299];
            %rl = filter(bHumbug,aHumbug,rl);
        elseif ~isempty(P.lfpchans)
            %rl = single(resample(double(rl),P.SRlfp,lfpfs));
        end
        
    case 5; fprintf('EVP version 5 :   ');
        fileroot = filename(1:end-10);
        Info.SR = P.runinfo.spikefs;
        P.SR=P.runinfo.spikefs;
        
        if isinf(P.spikechans)  P.spikechans = [1:P.runinfo.spikechancount];  end
        if isinf(P.trials)             P.trials            = [1:P.runinfo.trialcount];   end
        
        loadchans = unique([P.rawchans,P.spikechans,P.lfpchans]);
        SortRaw = []; for iC = 1:length(P.rawchans) SortRaw(iC) = find(loadchans==P.rawchans(iC)); end
        SortSpike = []; for iC = 1:length(P.spikechans) SortSpike(iC) = find(loadchans==P.spikechans(iC)); end
        SortLFP = []; for iC = 1:length(P.lfpchans) SortLFP(iC) = find(loadchans==P.lfpchans(iC)); end
        
        %% LOOP OVER TRIALS & CHANNELS
        iCurrent = 0; Breaking = 0; rs = [];
        strialidx = []; ltrialidx = []; atrialidx = []; ra = []; rl = [];
        alltrialidx = zeros(length(P.trials),1); triallengths = zeros(length(P.trials),1);
        fprintf('Reading Trials  '); PrintCount = 0;
        NDots = round(length(P.trials)/10); Dots = repmat('.',1,NDots);
        for tt = 1:length(P.trials)
            trialidx = P.trials(tt);
            
            % PRINT PROGRESSBAR
            BackString = repmat('\b',1,PrintCount);
            Division = ceil(tt/length(P.trials)*NDots);
            PreDots = Dots(1:Division-1); PostDots = Dots(Division:end-1);
            PrintCount = fprintf([BackString,'[ ',PreDots,' %d ',PostDots,' ]'],trialidx);
            PrintCount = PrintCount - length(BackString)/2;
            
            for cc = 1:length(loadchans)
                spikeidx = loadchans(cc);
                cFilename=[fileroot,sprintf('.%03d.%d.evp',trialidx,spikeidx)];
                trs = []; AttemptCounter = 0;
                while isempty(trs) && AttemptCounter<6  % 15/03-YB: Multiple attempts when lfp cannot be loaded
                    AttemptCounter = AttemptCounter+1;
                    [trs,Header]=evpread5(cFilename);
                    if isempty(trs); disp('evpread: trial empty! I try again.'); end
                end
                if USECOMMONREFERENCE
                    % COMPUTE COMMONE REFERENCE
                    cFilename=[fileroot,sprintf('.%03d.%d.evp',trialidx,1)]; % COMMON REF INDEPENDENT OF EL.
                    if cc == 1
                        [CommonRef,BanksByChannel] = ...
                            evpread5commonref(cFilename,length(trs),USECOMMONREFERENCE);
                        % BREAK because error in CommonRef computation
                        if isnan(CommonRef) Breaking = 1; error('error computing common reference'); break; end
                    end
                    if length(trs)==size(CommonRef,1)
                        % FORCE RECOMPUTE MEAN, PROBABLY BROKEN DURING COMPUTATION
                        if spikeidx>length(BanksByChannel)
                            [CommonRef,BanksByChannel] = evpread5commonref(cFilename,length(trs),2);
                        end
                        trs = trs - CommonRef(:,BanksByChannel(spikeidx));
                    else  % Delete MeanFile
                        evpread5commonref(cFilename,length(trs),-1); Breaking = 1; break;
                    end
                end
                if isempty(trs),
                    error('evpread: trial empty!');
                end
                trs = [trs;trs(end)*ones(100,1)];
                ltrs = length(trs);
                if tt==1 && cc==1
                    EstimatedSteps = length(P.trials)*ltrs;
                    rall = zeros([EstimatedSteps,length(loadchans)],'single');
                    Info = transferFields(Info,Header);
                end
                rall(iCurrent+1:iCurrent+ltrs,cc) = trs - trs(1);
            end
            if Breaking break; end
            alltrialidx(tt) =  iCurrent+1;
            triallengths(tt) = ltrs;
            iCurrent = iCurrent + ltrs;
        end;
        if Breaking tt=tt-1; end
        alltrialidx = alltrialidx(1:tt);
        fprintf('\n');
        cstrialidx = [alltrialidx;iCurrent];
        [rs,strialidx,ra,atrialidx,rl,ltrialidx]=select_and_filter(rall(1:iCurrent,:),cstrialidx,P,SortRaw,SortSpike,SortLFP);
    case 6; fprintf('EVP version 6 (Open-Ephys) :   ');
        Info.SR = P.runinfo.spikefs;
        P.SR=P.runinfo.spikefs;
        
        if isinf(P.spikechans)  P.spikechans = [1:P.runinfo.spikechancount];  end
        if isinf(P.trials)             P.trials            = [1:P.runinfo.trialcount];   end
        
        loadchans = unique([P.rawchans,P.spikechans,P.lfpchans]);
        SortRaw = []; for iC = 1:length(P.rawchans) SortRaw(iC) = find(loadchans==P.rawchans(iC)); end
        SortSpike = []; for iC = 1:length(P.spikechans) SortSpike(iC) = find(loadchans==P.spikechans(iC)); end
        SortLFP = []; for iC = 1:length(P.lfpchans) SortLFP(iC) = find(loadchans==P.lfpchans(iC)); end
        switch P.runinfo.datatype
            case 'OEP'
                % backward compatibility... if OEPdir not in runinfo,
                % assume no Recording Nodes used.
                OEPdir=getparm(P.runinfo,'OEPdir',P.globalparams.rawfilename);
                [EVdata, EVinfo,EVtimestamps] = load_open_ephys_data_faster([OEPdir,filesep,'all_channels.events']);
                trial_onsets=EVtimestamps(EVinfo.eventId==1&EVinfo.eventType==3); % in samples
                trial_offsets=EVtimestamps(EVinfo.eventId==0&EVinfo.eventType==3); % in samples
                
                % Get TCP message events
                % baphyTcpOnsets = EVtimestamps(EVinfo.eventType==5); LAS: old way, now OEP is sending more TCP pulses so filter out only the TrialStart ones
                fid=fopen([OEPdir,filesep,'messages.events']);
                g = textscan(fid,'%s','delimiter','\n');
                MSGtimestamp=zeros(size(g{1}));
                MSGmsg=cell(size(g{1}));
                is_TrialStart=false(size(g{1}));
                for ii=1:length(g{1})
                    spaces=strfind(g{1}{ii},' ');
                    MSGtimestamp(ii)=str2double(g{1}{ii}(1:spaces(1)-1));
                    MSGmsg{ii}=g{1}{ii}(spaces(1)+1:end);
                    is_TrialStart(ii)=length(MSGmsg{ii})>11 && strcmp(MSGmsg{ii}(1:11),'TrialStart ');
                end
                baphyTcpOnsets=MSGtimestamp(is_TrialStart);
            case 'binary'
                EVdata = load_open_ephys_binary(P.runinfo.json_file,'events',P.runinfo.event_ind_TTL);
                trial_onsets=EVdata.Timestamps(EVdata.Data==1); % in samples
                trial_offsets=EVdata.Timestamps(EVdata.Data==-1); % in samples
                
                % Get TCP message events
                if 0
                    EVdata = load_open_ephys_binary(P.runinfo.json_file,'events',P.runinfo.event_ind_TCP);
                    %TBD, readNPY can't read text so this doesn't work.
                else
                    baphyTcpOnsets = NaN;
                end
            case 'Binary'  % OE 0.6
                spike_rec = P.runinfo.spike_rec;
                trial_onsets = P.runinfo.recordings(spike_rec).trial_onsets;
                trial_offsets = P.runinfo.recordings(spike_rec).trial_offsets;
                
                baphyTcpOnsets = NaN;
        end

        %assumes requested trials are continuous!
        if(length(trial_onsets)==length(trial_offsets)+1)
            %last trial lacks an offset TTL. Likely an error in data collection.
            trial_offsets(end+1)=trial_onsets(end)+mode(trial_offsets-trial_onsets(1:end-1));
            missing_last_offset=true;
        else
            missing_last_offset=false;
        end
        
        %% simple error checking: do the trial onset times logged in TCP events match the TTL pulses?
        
        if ~isequaln(baphyTcpOnsets, NaN)
            evcountdiff=length(baphyTcpOnsets) - length(trial_onsets);
            dd=trial_onsets(1:end+evcountdiff) - baphyTcpOnsets;
            if evcountdiff<0
                disp('');
                warning('more TTL pulse trial_onsets than TCP events!')
                %keyboard % MLE anotated out the keyboard, why is this here?
                dd=trial_onsets(1:end+evcountdiff) - baphyTcpOnsets;
                
                if 0
                    %LAS for figuring out why these are so offset!
                    shift_range=-10:10;
                    for ii=1:length(shift_range)
                        indsA=(1:length(baphyTcpOnsets))+shift_range(ii);
                        indsB=1:length(baphyTcpOnsets);
                        keep = indsA<=length(trial_onsets) & indsA>0;
                        dd=trial_onsets(indsA(keep)) - baphyTcpOnsets(indsB(keep));
                        mindf(ii)=min(dd);
                        maxdf(ii)=max(dd);
                        meandf(ii)=mean(dd);
                    end
                    [~,ii]=min(abs(meandf));
                    [~,ii]=min(abs(maxdf));
                    maxdf(ii+[0 1])/P.SR
                end
            elseif evcountdiff>0
                warning('more TCP events than TTL pulse trial_onsets!')
                dd=trial_onsets - baphyTcpOnsets((evcountdiff+1):end);
            end
            
            
            if exist('dd', 'var') && max(dd) > P.SR/10
                disp('');
                warning('trigger to TCP offset > 100 ms!');
                %keyboard %CRH got rid of this keyboard. Why is this here?
            end
        end
        
        %% Define time window over which to get data
        if(P.trials(end)==length(trial_onsets))
            inds=(trial_onsets(P.trials(1)):trial_offsets(P.trials(end)));
        else
            inds=(trial_onsets(P.trials(1)):trial_onsets(P.trials(end)+1)-1);
        end
        
        %% Get Data
        rall=zeros(length(inds),length(loadchans));
        if P.runinfo.info.VERSION_>=0.45 && 0
            suffix='_0';
        else
            suffix='';
        end
        
        % figure out range to load and check for dropped frames
        switch P.runinfo.datatype
            case 'binary'
                data = load_open_ephys_binary(P.runinfo.json_file,'continuous',P.runinfo.data_ind,'mmap');
                sti = find(data.Timestamps==inds(1));
                ndi = find(data.Timestamps==inds(end));
                time_inds = sti:ndi;
                Ndrop = length(inds) - length(time_inds);
                if Ndrop > 0
                    fp=fileparts(P.runinfo.json_file);
                    fp =[fp filesep 'continuous' filesep data.Header.folder_name(1:end-1) filesep 'timestamps.npy'];
                    error(['Dropped samples found in %s. Probably due to ',...
                        'OEP computer being too bogged down. Insert code ',...
                        'to use good samples if needed. %d dropped segments ',...
                        'totaling %d dropped samples (%.2f%%).'],...
                        fp,sum(diff(data.Timestamps(sti:ndi))>1), Ndrop,Ndrop/length(inds)*100)
                elseif Ndrop < 0
                    error('More samples loaded than requested? This shouldn''t happen')
                end
            case 'Binary'
                % TODO : deal with dropped frames if they are detected
                spike_rec = P.runinfo.spike_rec;
                ev = P.runinfo.session.recordNodes{spike_rec}.recordings{1}.ttlEvents;
                event_keys = ev.keys();
                k = event_keys{find(contains(event_keys,'AP') & contains(event_keys,'Neuropix'),1)};

                cont = P.runinfo.session.recordNodes{spike_rec}.recordings{1}.continuous;
                trial_onsets_ts = P.runinfo.recordings(spike_rec).trial_onsets_ts;
                trial_offsets_ts = P.runinfo.recordings(spike_rec).trial_offsets_ts;
                
                sti = P.runinfo.recordings(spike_rec).trial_onsets(P.trials(1)) - ...
                    cont(k).metadata.startTimestamp;
                %sti = find(cont(k).timestamps>=trial_onsets_ts(P.trials(1)),1);
                ndi = sti+length(inds)-1;
                time_inds = sti:ndi;
                
                for ii=1:length(trial_onsets_ts)
                   i1=find(cont(k).timestamps>=trial_onsets_ts(ii),1);
                   i2=find(cont(k).timestamps>=trial_offsets_ts(ii),1);
                   expected_frames = trial_offsets(ii)-trial_onsets(ii);
                   diff = (i2-i1)-expected_frames;
                   if abs(diff)>1
                       fprintf('Trial %2d: %7d-%7d, actual=%6d expected=%6d diff=%d\n', ii, i1, i2, i2-i1, expected_frames, diff);
                   end
                end
                
                if isempty(cont(k).samples)
                    json_file = fullfile(P.runinfo.recordings(spike_rec).path,'structure.oebin');
                    data_ind = P.runinfo.session.recordNodes{spike_rec}.recordings{1}.experimentIndex;
                    data = load_open_ephys_binary(json_file,'continuous',data_ind,'mmap');
                end
        end
        fprintf('\nReading Channel: ')
        for cc = 1:length(loadchans)
            fprintf([num2str(loadchans(cc)) ' '])
            switch P.runinfo.datatype
                case 'OEP'
                    % Think this fixes and issue with OE 0.5.5 wihtout breaking backward compatibility
                    % in new OE, record node ID is not the same as the
                    % source ID. Unclear how the new version deals with ADC
                    % vs. CH. Separate record nodes???
                    nodeId = num2str(P.runinfo.raw_data.source_Id);
                    
                    % in new OE, Source node is distinct from the
                    % record node. Record node specifies the folder, Source
                    % specifies the file.  So if there's a separate
                    % 'Sources/Rhythm FPGA' processor, look for files with
                    % that nodeID as a prefix
                    for procid=1:length(P.runinfo.processors)
                        if strcmpi(P.runinfo.processors(procid).processorName,'Sources/Rhythm FPGA')
                            nodeId = num2str(P.runinfo.processors(procid).nodeId);
                        end
                    end
                    if P.runinfo.spike_channels_are_adcs
                        alt_source_Id = [nodeId,'_ADC'];
                    else
                        alt_source_Id = [nodeId,'_CH'];
                    end
                    % The source/recording decoupling may(??) be why the
                    % CH/ADC prefixes are now no longer needed (though they
                    % seem to appear intermittently!!)
                    oefilename=[OEPdir,filesep,alt_source_Id,num2str(loadchans(cc)),suffix,'.continuous'];
                    if ~exist(oefilename,'file')
                        % file with CH prefix not found. Try without
                        alt_source_Id=[nodeId,'_'];
                        oefilename=[OEPdir,filesep,alt_source_Id,num2str(loadchans(cc)),suffix,'.continuous'];
                    end
                    [tr,info,timestamps] = load_open_ephys_data_faster(oefilename);
                    
                    %if P.runinfo.spike_channels_are_adcs
                        %[rall(:,cc),info] = load_open_ephys_data_faster([P.globalparams.rawfilename,filesep,num2str(P.runinfo.raw_data.source_Id),'_ADC',num2str(loadchans(cc)),suffix,'.continuous'],inds([1 end]));
                    %else
                        %[rall(:,cc),info,timestamps] = load_open_ephys_data_faster([P.globalparams.rawfilename,filesep,num2str(P.runinfo.raw_data.source_Id),'_CH',num2str(loadchans(cc)),suffix,'.continuous'],inds([1 end]));
                    %end
                    ii=(ismember(inds,timestamps));
                    jj=(ismember(timestamps,inds));
                    rall(ii,cc)=tr(jj);
                    if sum(ii)<length(inds)
                       warning('Dropped samples!!!!!!!!!!! Filling in with zeros.');
                    end
                    
                case {'binary', 'Binary'}
                    rall(:,cc) = double(data.Data.Data.mapped(loadchans(cc),time_inds)) * data.Header.channels(loadchans(cc)).bit_volts;
                    %Without memmap (much slower)
                    %data = load_open_ephys_binary(P.runinfo.json_file,'continuous',P.runinfo.data_ind);
                    %rall(:,cc) = data.Data(loadchans(cc),time_inds) * data.Header.channels(loadchans(cc)).bit_volts;
                %case 'Binary'
                %    bit_volts = 0.19499999284744262695;
                %    rall(:,cc) = double(cont(k).samples(loadchans(cc),time_inds)) * bit_volts;
            end
            if(cc==1)
                alltrialidx=trial_onsets(P.trials)-trial_onsets(P.trials(1))+1; %trial onset indexes
                alltrial_off_idx=trial_offsets(P.trials)-trial_onsets(P.trials(1))+1; %trial offset indexes
            end
            if USECOMMONREFERENCE
                % COMPUTE COMMON REFERENCE
                if cc == 1
                    cr_options.channels=P.runinfo.spike_channels;
                    if ~isempty(COMMONREFERENCECHANNELS)
                        cr_options.channels=cr_options.channels(COMMONREFERENCECHANNELS);
                    end
                    cr_options.trials=P.trials;
                    cr_options.globalparams=P.globalparams;
                    [d,fn,ext]=fileparts(P.globalparams.evpfilename);
                    comm_ref_filename=[d,filesep,'tmp',filesep,fn,'_comref.mat'];
                    cr_options.rawfilename=P.globalparams.rawfilename;
                    cr_options.runinfo=P.runinfo;
                    meta.trial_offsets=trial_offsets-trial_onsets(1)+1;
                    meta.trial_onsets=trial_onsets-trial_onsets(1)+1;
                    switch P.runinfo.datatype
                        case 'OEP'
                            meta.trial_starts=info.ts;
                    end
                    CommonRef=read_common_ref(comm_ref_filename,cr_options,meta);
                    % BREAK because error in CommonRef computation
                    if isnan(CommonRef) Breaking = 1; error('error computing common reference'); break; end
                end
                if(P.output_common_ref)
                    rall(:,cc)=CommonRef;
                else
                    rall(:,cc)=rall(:,cc) - CommonRef;
                end
            end
        end
        fprintf('\n')
        cstrialidx=[alltrialidx;length(rall)+1];
        [rs,strialidx,ra,atrialidx,rl,ltrialidx]=select_and_filter(rall,cstrialidx,P,SortRaw,SortSpike,SortLFP);
    otherwise error('Invalid evp version!');
end

%% COLLECT ALL RESULTS INTO A SINGLE STRUCT
if nargout == 1
    switch P.dataformat
        case 'linear'; %
            if ~isempty(P.rawchans) R.Raw = rr;  R.RTrialidx = rtrialidx; end; clear rr;
            if ~isempty(P.spikechans) R.Spike = rs; R.STrialidx = strialidx; end; clear rs;
            if ~isempty(P.lfpchans) R.LFP = rl; R.LTrialidx = ltrialidx; end; clear rl;
            if ~isempty(P.auxchans) R.AUX = ra; R.ATrialidx = atrialidx; end; clear ra;
            
        case 'separated'
            Fields = {'Raw','Spike','LFP','AUX'};
            for iF=1:length(Fields)
                cField = Fields{iF};
                cChans = P.([lower(cField),'chans']);
                if ~isempty(cChans)
                    eval(['cidx = ',lower(Fields{iF}(1)),'trialidx;']);
                    R.(Fields{iF}) = cell(length(cidx)-1,1);
                    eval(['cData = r',lower(cField(1)),';'])
                    cidx(end+1) = size(cData,1);
                    for iT=1:length(cidx)-1
                        R.(cField){iT} = cData(cidx(iT):cidx(iT+1)-1,:);
                    end
                end
            end
    end
    R.Info  = Info;
    rs = R;
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function P = LF_checkElecs2Chans(filename,P)
% Translates electrode numbers into lowlevel channel numbers
% see M_RecSystemInfo & M_ArrayInfo & M_Arrays & M
Sep = HF_getSep;

cSpike = isfield(P,'spikeelecs') && ~isempty(P.spikeelecs);
cRaw = isfield(P,'rawelecs') && ~isempty(P.rawelecs);
cLFP = isfield(P,'lfpelecs') && ~isempty(P.lfpelecs);
maxchan=1;
if cSpike,
    maxchan=max([maxchan;P.spikeelecs(:)]);
end
if cRaw,
    maxchan=max([maxchan;P.rawelecs(:)]);
end
if cLFP,
    maxchan=max([maxchan;P.lfpelecs(:)]);
end

if cSpike || cRaw || cLFP
    R = MD_dataFormat('FileName',filename);
    try,
        [ElectrodesByChannel,Electrode2Channel] ...
            = MD_getElectrodeGeometry('Identifier',R.FileName,'FilePath',fileparts(filename));
    catch
        ElectrodesByChannel=1:maxchan;
        Electrode2Channel=1:maxchan;
    end
end
if cSpike
    P.spikechans =Electrode2Channel(P.spikeelecs);
    fprintf('Spike (El => Ch) : ');
    for i=1:length(P.spikechans) fprintf([' %d=>%d | '],P.spikeelecs(i),P.spikechans(i)); end
    fprintf('\n');
end
if cRaw
    P.rawchans =Electrode2Channel(P.rawelecs);
    fprintf('Raw (El => Ch) : ');
    for i=1:length(P.rawchans) fprintf([' %d=>%d | '],P.rawelecs(i),P.rawchans(i)); end
    fprintf('\n');
end
if cLFP
    P.lfpchans =Electrode2Channel(P.lfpelecs);
    fprintf('LFP (El => Ch) : ');
    for i=1:length(P.lfpchans) fprintf([' %d=>%d | '],P.lfpelecs(i),P.lfpchans(i)); end
    fprintf('\n');
end
end

function [rs,strialidx,rr,rtrialidx,rl,ltrialidx]=select_and_filter(rall,cstrialidx,P,SortRaw,SortSpike,SortLFP)

%% SELECT CHANNELS FOR RAW, SPIKE & LFP
if ~isempty(SortRaw),
    rr = rall(:,SortRaw); rtrialidx = cstrialidx(1:end-1);
else
    rr=[];rtrialidx=[];
end
if ~isempty(SortSpike),
    rs = rall(:,SortSpike); strialidx = cstrialidx(1:end-1);
else
    rs=[];strialidx=[];
end
if ~isempty(SortLFP),
    rl = rall(:,SortLFP); ltrialidx = cstrialidx(1:end-1);
else
    rl=[];ltrialidx=[];
end
NN=P.SR./2;

%% FILTER SPIKES
if ~isempty(P.spikechans)
    lof=300;  hif=6000;
    
    bHumbug = [0.997995527211068  -5.987297083916456  14.967228743433322 -19.955854373444378  14.967228743433322  -5.987297083916456   0.997995527211068];
    aHumbug = [1.000000000000000  -5.995310048314492  14.977237236960848 -19.955846338529373  14.957216231994666  -5.979292154433458   0.995995072333299];
    
    switch P.filterstyle
            
        case 'butterhigh'
            order = 2;
            [bHigh,aHigh] = butter(order,lof/NN,'high');
            for j=1:length(strialidx)
                rs(cstrialidx(j):cstrialidx(j+1)-1,:) = filter(bHigh,aHigh,rs(cstrialidx(j):cstrialidx(j+1)-1,:));
            end
            
        case 'butter'
            order = 2;
            [bLow,aLow] = butter(order,hif/NN,'low');
            [bHigh,aHigh] = butter(order,lof/NN,'high');
            for j=1:length(strialidx)
                tmp = filter(bLow,aLow,rs(cstrialidx(j):cstrialidx(j+1)-1,:));
                %tmp = filter(bHumbug,aHumbug,tmp);
                tmp = filter(bLow,aLow,tmp);
                rs(cstrialidx(j):cstrialidx(j+1)-1,:) = filter(bHigh,aHigh,tmp);
            end
            
        case 'filtfiltsep'
            orderhp=50; orderlp=10;
            f_hp = firls(orderhp,[0 (0.95.*lof)/NN lof/NN 1],[0 0 1 1])';
            f_lp = firls(orderlp,[0 hif/NN (hif./0.95)./NN 1],[1 1 0 0])';
            for j=1:length(strialidx)
                tmp = double(rs(cstrialidx(j):cstrialidx(j+1)-1,:));
                %tmp = filter(bHumbug,aHumbug,tmp);
                tmp = filtfilt(f_lp,1,tmp);
                rs(cstrialidx(j):cstrialidx(j+1)-1,:) = single(filtfilt(f_hp,1,tmp));
            end
            
        case 'filtfiltold'
            for j=1:length(strialidx)
                orderbp=min(floor(length(rs(cstrialidx(j):cstrialidx(j+1)-1,:))/3),round(SR./lof*5));
                f_bp = firls(orderbp,[0 (0.95.*lof)/NN lof/NN  hif/NN (hif./0.95)./NN 1],[0 0 1 1 0 0])';
                rs(cstrialidx(j):cstrialidx(j+1)-1,:) = single(filtfilt(f_bp,1,double(rs(cstrialidx(j):cstrialidx(j+1)-1,:))));
            end
            
        case 'filtfilthum'
            orderbp=50;
            f_bp = firls(orderbp,[0 (0.95.*lof)/NN lof/NN  hif/NN (hif./0.95)./NN 1],[0 0 1 1 0 0])';
            for j=1:length(strialidx)
                tmp = filter(bHumbug,aHumbug,rs(cstrialidx(j):cstrialidx(j+1)-1,:));
                rs(cstrialidx(j):cstrialidx(j+1)-1,:) = single(filtfilt(f_bp,1,double(tmp)));
            end
            
        case 'none'; % no Filtering
        otherwise error('Filter not implemented.');
    end
end

%% FILTER LFP
if ~isempty(P.lfpchans)
  
            
            if P.wrap
                %Luke: data was recast to single precison here before
                tmp = double(NaN*zeros(round((max(diff(cstrialidx))-1)/SR*P.SRlfp),length(P.lfpchans),length(cstrialidx)-1));
                for i=1:length(cstrialidx)-1
                    tmp2 = single(resample(double(rl(cstrialidx(i):cstrialidx(i+1)-1,:)),P.SRlfp,SR));
                    tmp(1:length(tmp2),:,i) = tmp2;
                end
                rl = tmp;
            else
                %Luke: data was recast to single precison here before
                rl = resample(double(rl),P.SRlfp,P.SR);
                ltrialidx = ceil(cstrialidx*P.SRlfp/P.SR);
            end
  switch P.filterstyle
        case 'butter'
            order = 2; Nyquist = P.SRlfp/2;
            if(~isempty(P.lfp_filter_freqs))
                fHigh = P.lfp_filter_freqs(1); fLow = P.lfp_filter_freqs(2);
            else
                fHigh = 1; fLow = 0.3*Nyquist;
            end
            [bLow,aLow] = butter(order,fLow/Nyquist,'low');
            [bHigh,aHigh] = butter(order,fHigh/Nyquist,'high');
            
            %Luke: Humbug filtering was introducing ringing artifacts?
            %bHumbug=[0.995386247699319  -5.972013278489225  14.929576915653460  -19.905899769726052  14.929576915653460  -5.972013278489225  0.995386247699319 ];
            %aHumbug = [ 1.000000000000000  -5.990446012819222  14.952579842917430  -19.905857198474035  14.906552701679249  -5.953623115411301  0.990793782108932  ];
            %LHumbug = length(bHumbug)-1;
            %Raw=double(rl)';
            % IVHumbug = zeros(LHumbug,size(Raw,2));
            %[Raw] = filter(bHumbug,aHumbug,Raw);
            %[Raw] = fliplr(filter(bHumbug,aHumbug,fliplr(Raw)));
            %rl = filtfilt(bHumbug,aHumbug,rl);
            %rl = Raw';
            
            %rl = filter(bLow,aLow,rl);
            %rl = filter(bHigh,aHigh,rl);
            
            rl = filtfilt(bLow,aLow,rl);
            rl = filtfilt(bHigh,aHigh,rl);
            
            %bHumbug = [0.997995527211068  -5.987297083916456  14.967228743433322 -19.955854373444378  14.967228743433322  -5.987297083916456   0.997995527211068];
            %aHumbug = [1.000000000000000  -5.995310048314492  14.977237236960848 -19.955846338529373  14.957216231994666  -5.979292154433458   0.995995072333299];
            %rl = filter(bHumbug,aHumbug,rl);
        case 'none'
            rl = resample(double(rl),P.SRlfp,P.SR);
        otherwise error('Filter not implemented.');
    end
end
end
