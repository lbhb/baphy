
MG=struct();
MG.cam = videoinput('winvideo', 1,'UYVY_640x480');
%MG.cam = videoinput('winvideo', 1,'UYVY_1920x1200');

%MG.cam = videoinput('pointgrey', 1,'F7_Mono16_480x300_Mode5');
MG.cam.ReturnedColorSpace = 'RGB';
res = MG.cam.VideoResolution;
nbands = MG.cam.NumberOfBands;

figure
ax=gca();

himage = image(zeros(res(2), res(1), nbands), 'Parent', ax);
preview(MG.cam, himage)

%hwInfo = imaqhwinfo('pointgrey')
hwInfo = imaqhwinfo('winvideo')
hwInfo.DeviceInfo(1)