% function p=getparmC(varargin_cell,varargin_ind,parmdef);
function p=getparmC(varargin_cell,varargin_ind,parmdef)

if length(varargin_cell)>=varargin_ind,
    if(isempty(varargin_cell{varargin_ind}))
        p=parmdef;
    else
        p=varargin_cell{varargin_ind};
    end
else
    p=parmdef;
end
