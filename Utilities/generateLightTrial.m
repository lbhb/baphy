function [LightBand,evout]=generateLightTrial(par,events,TrialBins,TrialIndex)
% function [LightBand,evout]=generateLightTrial(par,events,TrialBins)
%
% RefTarOpt-specific code: add light channel, change event strings to
% reflect light on or off
%

if ~exist('TrialIndex','var')
    TrialIndex=1;
    LightPulseDuration = par.LightPulseDuration;
    LightPulseRate = par.LightPulseRate;
else
    LightPulseDuration = par.PulseDurations(TrialIndex);
    LightPulseRate = par.PulseRates(TrialIndex);
end

TrialSamplingRate=par.SamplingRate;

%'LightPulseRate','edit',50,...
%'LightPulseDuration','edit',0.01,...
%'LightPulseShift','edit',0,...
%'LightEpoch','popupmenu','Sound|Sound Onset|Whole'};
evout=events;

LightBand=zeros(TrialBins,1);
RampTime=0.000;
RampOn=linspace(0,5,round(TrialSamplingRate*RampTime));
RampOff=linspace(5,0,round(TrialSamplingRate*RampTime));

LightStepSize=TrialSamplingRate./LightPulseRate;
LightOnBins=round(LightPulseDuration.*TrialSamplingRate);
switch par.LightEpoch
    case 'WholeTrial'
        maxevent=events(end-1);
        maxtrialbins=round(maxevent.StopTime.*TrialSamplingRate);
        
        ii=round(par.LightPulseShift.*TrialSamplingRate);
        while ii<maxtrialbins
            if ii+LightOnBins>maxtrialbins
                LightBand((ii+1):end)=5;
            else
                LightBand(ii+(1:LightOnBins))=5;
            end
            LightStartTime=ii/TrialSamplingRate;
            evout=AddEvent(evout,'LightStim',evout(end).Trial,LightStartTime,LightStartTime+LightPulseDuration);
                
            % find next light onset time
            ii=round(ii+LightStepSize);
        end
        
    case 'SoundOnset'
        for evidx=2:3:length(events)
            StimStartTime=events(evidx).StartTime;
            LightStartTime=StimStartTime+par.LightPulseShift;
            LightStartBin=round(LightStartTime*TrialSamplingRate);
            LightBand(LightStartBin+(1:LightOnBins))=5;
            evout=AddEvent(evout,'LightStim',evout(end).Trial,LightStartTime,LightStartTime+LightPulseDuration);
        end
        
    case 'Sound'
        for evidx=2:3:length(events)
            StimStartTime=events(evidx).StartTime;
            StimStopTime=events(evidx).StopTime;
            LightStartTime=StimStartTime+par.LightPulseShift;
            while LightStartTime<StimStopTime
                LightStartBin=round(LightStartTime*TrialSamplingRate);
                LightBand(LightStartBin+(1:LightOnBins))=5;
                LightBand(LightStartBin+(1:length(RampOn)))=RampOn;
                LightBand(LightStartBin+((LightOnBins-length(RampOff)+1):LightOnBins))=RampOff;
                evout=AddEvent(evout,'LightStim',evout(end).Trial,LightStartTime,LightStartTime+LightPulseDuration);
                
                % find next light onset time
                LightStartTime=LightStartTime+1./LightPulseRate;
            end
        end
        
    case 'Target'
       
       evidx=length(events)-1;
       if ~isempty(findstr(events(evidx).Note,'Target'))
           StimStartTime=events(evidx).StartTime;
           LightStartTime=StimStartTime+par.LightPulseShift;
           LightStartBin=round(LightStartTime*TrialSamplingRate);
           LightBand(LightStartBin+(1:LightOnBins))=5;
       else
           error ('no target event found. LightStim configured incorrectly?');
       end
       evout=AddEvent(evout,'LightStim',evout(end).Trial,LightStartTime,LightStartTime+LightPulseDuration);
    case 'PostTrial'
        
        PostTrialSilence = par.PostTrialSilence;
        if PostTrialSilence<=0.5
            error('PostTrialSilence > 0.5 required for PostTrial stimulation');
        end
        
        startpad=1; % wait at least 1 sec into PostTrialSilence for stim
        endpad=(LightOnBins+1)/TrialSamplingRate; % leave enough time for the pulse to fit in the Trial
        StimRangeStart=length(LightBand)/TrialSamplingRate-PostTrialSilence+startpad;
        StimRangeStop=length(LightBand)/TrialSamplingRate-endpad;
        
        LightStartTime=StimRangeStart+rand*(PostTrialSilence-1-endpad);
        fprintf('Post stim: %.2f (range: %.2f-%.2f)\n',...
            LightStartTime,StimRangeStart,StimRangeStop);
        LightStartBin=round(LightStartTime*TrialSamplingRate);
        LightBand(LightStartBin+(1:LightOnBins))=5;
        
        evout=AddEvent(evout,'LightStim',evout(end).Trial,LightStartTime,LightStartTime+LightPulseDuration);
        
    otherwise
        error([par.LightEpoch, ' LightEpoch not supported yet']);
end

% make sure we always return to zero at the end
LightBand(end)=0;

