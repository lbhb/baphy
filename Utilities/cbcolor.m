function col=cbcolor(n,use_black,only_use_set_2)
% map=cbcolor(n)
% map=cbcolor(n,use_black)
% Get a single color from cbcolormap for lines of qualitative data (not sequential)
% LAS created from http://mkweb.bcgsc.ca/colorblind/ and http://mkweb.bcgsc.ca/biovis2012/
if nargin<2
    use_black=true;
end
if nargin<3
    only_use_set_2=0;
end
if ~only_use_set_2 && all(n<8)
    N=8;
else
    N=15;
end
map=cbcolormap(N,use_black,only_use_set_2);
col=map(n,:);