function varargout = ParameterGUI(varargin)
% 
% values = ParameterGui (params)
% 
% params is a structure with following fields:
%   text
%   style
%   default

% Last Modified by GUIDE v2.5 20-Nov-2005 20:15:17

% Begin initialization code - DO NOT EDIT
gui_Singleton = 0;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ParameterGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @ParameterGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ParameterGUI is made visible.
function ParameterGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ParameterGUI (see VARARGIN)

pset=varargin{1};
if length(varargin)>3 
    Alignment = varargin{4};
else
    Alignment = 'left';
end
if length(varargin)>4
    width = varargin{5};
else
    width = 430;
end
% User can optionally pass the font weight in varargin{3)
if length(varargin) > 2  
    FontWeight = varargin{3};
else
    FontWeight = 'normal';
end
if length(varargin)>1,
    title=varargin{2};
else
    title='Parameters';
end

% Choose default command line output for ParameterGUI
handles.output = hObject;
set(handles.figure1,'Units','pixels','Name',title);
aa=get(handles.figure1,'Position');
set(handles.figure1,'Position',[aa(1:2) width 23*length(pset)+80]);

moveguiPlusRemote(hObject,'center');

set(handles.pushbutton1,'Units','pixels','FontWeight','bold');
aa=get(handles.pushbutton1,'Position');
set(handles.pushbutton1,'Position',[aa(1) 20 aa(3:4)]);
set(handles.pushbutton2,'Units','pixels','FontWeight','bold');
aa=get(handles.pushbutton2,'Position');
set(handles.pushbutton2,'Position',[aa(1) 20 aa(3:4)]);

figure(handles.figure1);
pos = [0 23*length(pset)+30];
text_width=round(width*.42);
editable_left_edge=round(width*.43);
editable_width=round(width*.51);

for cnt1 = 1:length(pset)
   % had the field name, its style and its default value.
   switch pset(cnt1).style
       case 'info_button'
       otherwise
           uicontrol('Style','text','String',pset(cnt1).text,'FontWeight',FontWeight,...
             'HorizontalAlignment','right','position',[pos-[0 23*(cnt1-1)+4] text_width 20]);
   end
   value=pset(cnt1).default;
   
   % some controls (like check box) dont have strings, but have Value
   % property:
   switch pset(cnt1).style
       case {'edit','text'}, 
           DefValue = 0;
       case 'popupmenu',
           if iscell(value)
               value = value{1};
               DefValue = pset(cnt1).default{2};
           else
               DefValue = 1;
           end
       case 'checkbox', 
           DefValue = ifstr2num(value);
           value='';
           if isempty(DefValue) 
               DefValue =0;
           end
       case 'sound',
           DefValue = value;
           value='Edit ...';
           if isempty(DefValue) 
               DefValue=0;
           end
       case 'rawid',
           DefValue = value;
           if DefValue==-1,
               value='New';
           else
               rawdata=dbget('gDataRaw',value);
               if ~isempty(rawdata),
                   value=basename(rawdata.parmfile);
               else
                   value='New';
               end
           end
           if isempty(DefValue) 
               DefValue=0;
           end
   end
   % Default is not necessary, also for checkbox its empty
   if (isempty(value)) 
       value = '';
   end
   if isnumeric(value),
      value=mat2str(value);
   end
   if strcmpi(pset(cnt1).style,'sound'),
       paramHandle(cnt1) = uicontrol('Style','pushbutton','String',value,...
           'UserData',DefValue,'HorizontalAlignment','center',...
           'BackgroundColor',[1 1 1],'position',[pos-[-editable_left_edge 23*(cnt1-1)] editable_width 20]);
       set(paramHandle(cnt1),'Callback',...
           'ParameterGUI(''buttSound_Callback'',gcbo,[],guidata(gcbo))');
   elseif strcmpi(pset(cnt1).style,'rawid'),
       paramHandle(cnt1) = uicontrol('Style','pushbutton','String',value,...
           'UserData',DefValue,'HorizontalAlignment','center',...
           'BackgroundColor',[1 1 1],'position',[pos-[-editable_left_edge 23*(cnt1-1)] editable_width 20]);
       set(paramHandle(cnt1),'Callback',...
           'ParameterGUI(''buttRawID_Callback'',gcbo,[],guidata(gcbo))');
   elseif strcmpi(pset(cnt1).style,'checkbox'),
       paramHandle(cnt1) = uicontrol('Style',pset(cnt1).style,'String',value,...
           'Value',DefValue,'HorizontalAlignment',Alignment,...
           'position',[pos-[-editable_left_edge 23*(cnt1-1)] editable_width 20]);
          %'position',[pos-[0 23*(cnt1-1)+4] 180 20]);    
   elseif strcmpi(pset(cnt1).style,'info_button'),
       paramHandle(cnt1) = uicontrol('Style','pushbutton','String',pset(cnt1).text,...
           'HorizontalAlignment',Alignment,...
           'position',[pos-[-editable_left_edge 23*(cnt1-1)] editable_width 20],...
           'Callback',{@msgbox_callback,pset(cnt1).default});
   elseif strcmpi(pset(cnt1).style,'popstruct'),
       paramHandle(cnt1) = uicontrol('Style','pushbutton','String','   edit    ',...
           'HorizontalAlignment',Alignment,...
           'position',[pos-[-editable_left_edge 23*(cnt1-1)] editable_width 20],...
           'UserData',pset(cnt1).default,...
          'Callback',{@popstruct_callback,pset(cnt1).text,pset(cnt1).fields});
   else
       paramHandle(cnt1) = uicontrol('Style',pset(cnt1).style,'String',value,...
           'Value',DefValue,'HorizontalAlignment',Alignment,...
           'BackgroundColor',[1 1 1],'position',[pos-[-editable_left_edge 23*(cnt1-1)] editable_width 20]);
   end

   if isfield(pset,'tooltip') && ~isempty(pset(cnt1).tooltip),
        set(paramHandle(cnt1),'ToolTipString',pset(cnt1).tooltip);
   end
end

handles.paramHandle = paramHandle;
handles.pset = pset;

% Update handles structure
guidata(hObject, handles);

set(handles.figure1,'WindowStyle', 'modal')
% UIWAIT makes ParameterGUI wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ParameterGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% close taken care of elsewhere?
close(handles.figure1);

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% OK button
global Output;
paramHandle = handles.paramHandle;

if ~isempty(paramHandle)
    Output = cell(0);
    for cnt1 = 1:length(paramHandle)
        % now get the value:
        tempSt = get(handles.paramHandle(cnt1),'String');
        tempIn = get(handles.paramHandle(cnt1),'Value');
        % check to see if its an edit box:
        switch handles.pset(cnt1).style,
            case 'edit'
                value = (tempSt);
                %if ~isempty(str2num(value)),
                %   value=str2num(value);
                %end
                if isnumeric(handles.pset(cnt1).default),
                    value=str2num(value);
                end
            case 'popupmenu'
                value=tempSt(tempIn,:);
            case 'checkbox'
                value = tempIn;
            case {'sound','rawid'},
                % get properties of sound object that have been saved to a
                % structure in UserData
                value=get(handles.paramHandle(cnt1),'UserData');
            case 'popstruct'
                value=get(handles.paramHandle(cnt1),'UserData');
            otherwise
                value=[];
        end
        
        % update the field
        Output{cnt1} = value;
    end
end
handles.output = Output;
guidata(gcbo, handles);
uiresume;
%close(handles.figure1);

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% delete taken care of elsewhere?
handles.output = {};
uiresume;


function buttSound_Callback(hObject, eventdata, handles)

to=SoundGUI(get(hObject,'UserData'),'Sound');
if ~isempty(to),
    set(hObject,'UserData',get(to));
    guidata(gcbo, handles);
end


function buttRawID_Callback(hObject, eventdata, handles)

rawdata=dbchooserawfile(1,'Choose existing raw file or cancel for new');
if ~isempty(rawdata),
    set(hObject,'UserData',rawdata.rawid);
    set(hObject,'String',basename(rawdata.parmfile));
else
    set(hObject,'UserData',-1);
    set(hObject,'String','New');
end

guidata(gcbo, handles);

function msgbox_callback(hObject, eventdata, tex)
msgbox(tex)

function popstruct_callback(hObject, eventdata, structname,fields)
%fields is UserDefinableFields for this substructure
h=guidata(gcbo);
set(h.paramHandle,'Enable','off');
set(h.pushbutton1,'Enable','off');
set(h.pushbutton2,'Enable','off');
    current_values=get(hObject,'UserData');
    for cnt1 = 1:3:length(fields)-1;
        param(1+(cnt1-1)/3).text = fields{cnt1};
        param(1+(cnt1-1)/3).style = fields{cnt1+1};
        if strcmp(fields{cnt1+1},'popupmenu')
            % if its a popupmenu, find the 'value' property:
            tmp1 = strfind(fields{cnt1+2},'|');
            if (strcmp(fields{cnt1+2},'Yes|No') || strcmp(fields{cnt1+2},'No|Yes')) && isnumeric(current_values.(fields{cnt1}))
                if current_values.(fields{cnt1})
                    currval='Yes';
                else
                    currval='No';
                end
            else
                currval=current_values.(fields{cnt1});
            end
            tmp2 = strfind(fields{cnt1+2},currval);
            if isempty(tmp2)
                popvalue = length(tmp1)+1;
            else
                popvalue = find(tmp1>tmp2,1);
            end
            if isempty(popvalue), popvalue = length(tmp1)+1;end
            default = fields{cnt1+2};
            default = {fields{cnt1+2}, popvalue};
        else
            default = current_values.(fields{cnt1});
        end

        param(1+(cnt1-1)/3).default = default;
    end
    UserInput = ParameterGUI(param,structname,'bold','center');
    if iscell(UserInput) % user did not press cancel, so change them:
        for cnt1 = 1:length(param);
            if isnumeric(UserInput{cnt1})
                current_values.(fields{1+(cnt1-1)*3})=UserInput{cnt1};
            else
                current_values.(fields{1+(cnt1-1)*3})=strtok(UserInput{cnt1});
            end
        end
        set(hObject,'UserData',current_values);
    end
    set(h.paramHandle,'Enable','on');
set(h.pushbutton1,'Enable','on');
set(h.pushbutton2,'Enable','on');