function o = HearsPeaks (varargin)
%
% Positive Reinforcement behavior control, hacked from RewardTarget
%
% SVD 2012-06-01

switch nargin
    case 0
        % if no input arguments, create a default object
        o.Threshold       = .9; %mean time of response re response window
        %To find optimum mean lick time:
        %sum(b.exptparams.TrialObject.ReferenceCountFreq.*(b.exptparams.TrialObject.SingleRefSegmentLen*cumsum(ones(size(b.exptparams.TrialObject.ReferenceCountFreq)))+b.exptparams.BehaveObject.EarlyWindow))
        o.ResponseTimeDelay=.35;
        o.ResponseProbabilityPerPeak=.5;
        o.ResponseTimeStd           = .21;
        o.TrialSound=[];
        o.DesiredResponseTimes      = [];
        o = class(o,'HearsPeaks');
        o = ObjUpdate(o);
    case 1
        % if single argument of class SoundObject, return it
        if isa(varargin{1},'HearsPeaks')
            o = varargin{1};
        else
            error('Wrong argument type'); 
        end
    otherwise
        error('Wrong number of input arguments');
end
