function [ev,o] = GetResponse(o,BehaviorObject,StimEvents,exptparams)
fs=get(exptparams.TrialObject,'SamplingRate');
if 1
    N=4; Fc=1000;
    Wc=(Fc+[-100 100])/fs*2;
    [B,A] = butter(N,Wc);
    sound=abs(hilbert(filter(B,A,o.TrialSound(:,1))));
else
    sound=abs(hilbert(o.TrialSound(:,2)));
end
peak_inds=find(sound>o.Threshold);
ri=[];
if ~isempty(peak_inds)
    peak_onset_inds=peak_inds([1; find(diff(peak_inds)>1)+1]);
    for i=1:length(peak_onset_inds)
        if rand>(1-o.ResponseProbabilityPerPeak)
            ri=i;
            break
        end
    end
end
%ri=[];
if isempty(ri)
    o.DesiredResponseTimes(end+1)=NaN;
    ev.Note='OUTCOME,MISS';
    ev.StartTime=exptparams.LogDuration;
    ev.StopTime=ev.StartTime;
else
    desired_time=peak_onset_inds(ri)/fs + get(BehaviorObject,'EarlyWindow') + o.ResponseTimeDelay;
    o.DesiredResponseTimes(end+1)=desired_time;
    response_time = desired_time + o.ResponseTimeStd*randn;
    ev.Note='LICK';
    ev.StartTime=response_time;
    ev.StopTime=response_time;
end
if 0
    t=(0:(size(o.TrialSound,1)-1))/fs;
    figure;plot(t,o.TrialSound(:,2:3));
    hold on;
    for i=1:length(peak_onset_inds)
        line(peak_onset_inds([i i])/fs,[0 1],'LineStyle',':','Color','k')
    end
    line(desired_time([1 1]),[0 1],'LineStyle',':','Color','r')
    line(response_time([1 1]),[0 1],'LineStyle',':','Color','m')
end
if 0 && mod(exptparams.TotalTrials,exptparams.TrialBlock)==0 || isfield(exptparams,'StopTime')
    ax=get(99,'Children');
    for i=1:length(peak_onset_inds)
        line(ax(2),peak_onset_inds([i i])/fs,[0 1],'LineStyle','--','Color','k')
    end
    if ~isempty(desired_time)
        lh=line(ax(2),desired_time([1 1]),[0 1],'LineStyle','--','Color','r');
        lh=line(ax(2),response_time([1 1]),[0 1],'LineStyle','--','Color','g');
    end
end
end