function [ev,o] = GetResponse(o,BehaviorObject,StimEvents,exptparams)
%     SR=get(get(exptparams.TrialObject,'ReferenceHandle'),'SamplingRate');
%     N=4; Fc=100; Wc=Fp/SR*2;
%     [B,A] = butter(N,Wc);
%     env=abs(hilbert(o.TrialSound));
%     envf=filter(B,A,env);
fs=get(exptparams.TrialObject,'SamplingRate');
%win_length=0.1875;
%win_length=.25;
wl=round(o.SamplingWinLength*fs); wo=round(wl*o.SamplingWinOverlap);
win=rectwin(wl);
Tenv=get(exptparams.TrialObject,'TrialEnvelope');
[~,f,t,envP]=spectrogram(Tenv(:,1),win,wo,wl,fs);
envP=10*log10(abs(envP));
[~,~,~,envP2]=spectrogram(Tenv(:,2),win,wo,wl,fs);
envP2=10*log10(abs(envP2));
if 0
    %%
    figure(98);
    ax=subplot1(3,1);
    axes(ax(1))
    cla;imagesc(t,f,envP);
    set(gca,'YDir','normal','YLim',[0 100]+diff(f(1:2))/2*[-1 1])
    hold on;
    t2=(0:(size(Tenv,1)-1))/fs;
    plot(t2,Tenv(:,1)*100,'w')
    colorbar
    set(gca,'CLim',[-70 -15],'XLim',t2([1 end]))
    set(gca,'Color','k')
    set(gca,'TickDir','out','Box','off')
    xlabel('Time re lick (s)')
    ylabel({'Frequency (Hz)','HCT A'})

    axes(ax(2))
    cla;imagesc(t,f,envP2);
    set(gca,'YDir','normal','YLim',[0 100]+diff(f(1:2))/2*[-1 1])
    hold on;
    t2=(0:(size(Tenv,1)-1))/fs;
    plot(t2,Tenv(:,2)*100,'w')
    colorbar
    set(gca,'CLim',[-70 -15],'XLim',t2([1 end]))
    set(gca,'Color','k')
    set(gca,'TickDir','out','Box','off')
    xlabel('Time re lick (s)')
    ylabel({'Frequency (Hz)','HCT A'})
    
    th=get(o,'Thresholds');
    axes(ax(3))
    [~,fi]=min(abs(f-o.AMFrequencies(1)));
    plot(t,envP(fi,:))
    hold on;line(t([1 end]),th([1 1]))
    lh=plot(t,envP2(fi,:));
    line(t([1 end]),th([2 2]),'LineStyle','--','Color',get(lh,'Color'))
    p=get(ax(3),'Position');
    p1=get(ax(1),'Position');
    p(3)=p1(3);
    set(ax(3),'Position',p,'XLim',t2([1 end]),'YLim',[-80 -10])
end
hit_times=[];
for i=1:length(o.AMFrequencies)
    [~,fi]=min(abs(f-o.AMFrequencies(i)));
    if o.AMChannels(i)==1
        hit_times=[hit_times t(envP(fi,:)>o.Thresholds(i))];
    elseif o.AMChannels(i)==2
        hit_times=[hit_times t(envP2(fi,:)>o.Thresholds(i))];
    else
        error('Invalid channel')
    end
end
hit_times=sort(hit_times);
ri=[];
if ~isempty(hit_times)
    %hit_onset_inds=hit_inds([1; find(diff(hit_inds)>1)+1]);
    for i=1:length(hit_times)
        %this_prob_per_hit=o.ResponseProbabilityPerHit;
        this_prob_per_hit=o.ResponseProbabilityPerHitTime(1)+...
            o.ResponseProbabilityPerHitTime(2)*hit_times(i);
        this_prob_per_hit=min([this_prob_per_hit o.ResponseProbabilityPerHitTime(3)]);
            
        rv=rand;
        %fprintf('%f\n',rv);
        if rv>(1-this_prob_per_hit)
            ri=i;
            break
        end
    end
end
%ri=[];
if isempty(ri)
    o.DesiredResponseTimes(end+1)=NaN;
    ev.Note='OUTCOME,MISS';
    ev.StartTime=exptparams.LogDuration;
    ev.StopTime=ev.StartTime;
else
    desired_time=hit_times(ri) + get(BehaviorObject,'EarlyWindow') + o.ResponseTimeDelay;
    o.DesiredResponseTimes(end+1)=desired_time;
    response_time = desired_time + o.ResponseTimeStd*randn;
    ev.Note='LICK';
    ev.StartTime=response_time;
    ev.StopTime=response_time;
end
if 0
    t=(0:(size(Tenv,1)-1))/fs;
    figure;plot(t,Tenv);
    hold on;
    for i=1:length(peak_onset_inds)
        line(peak_onset_inds([i i])/fs,[0 1],'LineStyle',':','Color','k')
    end
    line(desired_time([1 1]),[0 1],'LineStyle',':','Color','r')
    line(response_time([1 1]),[0 1],'LineStyle',':','Color','m')
end
if 0 && mod(exptparams.TotalTrials,exptparams.TrialBlock)==0 || isfield(exptparams,'StopTime')
    ax=get(99,'Children');
    for i=1:length(peak_onset_inds)
        line(ax(2),peak_onset_inds([i i])/fs,[0 1],'LineStyle','--','Color','k')
    end
    if ~isempty(desired_time)
        lh=line(ax(2),desired_time([1 1]),[0 1],'LineStyle','--','Color','r');
        lh=line(ax(2),response_time([1 1]),[0 1],'LineStyle','--','Color','g');
    end
end
end