function o = HearsAM (varargin)
%
% Positive Reinforcement behavior control, hacked from RewardTarget
%
% SVD 2012-06-01

switch nargin
    case 0
        % if no input arguments, create a default object
        
        %16 Hz: -16 1 perfect -27 .4
        %8 Hz tar is -21.35 -22
        o.SamplingWinLength=.25;
        o.SamplingWinOverlap=.75;
        o.AMFrequencies=[16 16];
        o.AMChannels=[1 2];
        o.Thresholds= [-27 -27];   %-27     
        o.ResponseTimeDelay=.35;
        o.ResponseProbabilityPerHit=NaN;%.3
        o.ResponseProbabilityPerHitTime=[0 .1 .5];% intercept, slope, saturation
        o.ResponseTimeStd=.01;
        o.TrialSound=[];
        o.DesiredResponseTimes= [];
        o = class(o,'HearsAM');
        o = ObjUpdate(o);
    case 1
        % if single argument of class SoundObject, return it
        if isa(varargin{1},'HearsAM')
            o = varargin{1};
        else
            error('Wrong argument type'); 
        end
    otherwise
        error('Wrong number of input arguments');
end
