function [ev,o] = GetResponse(o,BehaviorObject,StimEvents,exptparams)
    ti=find(arrayfun(@(x)~isempty(strfind(x.Note,'Target')) && ~isempty(strfind(x.Note,'Stim , ')),StimEvents));
    if length(ti)~=1
        warning('There are %d targets. HearsTarget will just respond to the first.')
        [~,ii]=min([StimEvents(ti).StartTime]);
        ti=ti(ii);
    end
    
    desired_time=StimEvents(ti).StartTime + ...
                 get(BehaviorObject,'EarlyWindow') + ...
                 o.ResponseTimeReResponseWindow;
    o.DesiredResponseTimes(end+1)=desired_time;
    response_time = desired_time + o.ResponseTimeStd*randn;
    if response_time<0
        error('response time less than 0!')
     end
    ev.Note='LICK';
    ev.StartTime=response_time;
    ev.StopTime=response_time;
    
    if any(o.RandomLickNumRange)>0
       NLicks = randi(diff(o.RandomLickNumRange)+1,1)+o.RandomLickNumRange(1)-1;
       RandomLickTimeRange=o.RandomLickTimeRange{1};
       if ischar(o.RandomLickTimeRange{2}) && strcmp(o.RandomLickTimeRange{2},'end')
           RandomLickTimeRange(2)=max([StimEvents.StopTime]);
       end
       RandomLickTimes = RandomLickTimeRange(1) + diff(RandomLickTimeRange).*rand(NLicks,1);
       for i=1:NLicks
           ev(end+1).Note = 'LICK';
           ev(end).StartTime = RandomLickTimes(i);
           ev(end).StopTime = RandomLickTimes(i);
       end
       [~,so]=sort([ev.StartTime]);
       ev=ev(so);
    end

end