function o = HearsTarget (varargin)
%
% Positive Reinforcement behavior control, hacked from RewardTarget
%
% SVD 2012-06-01

switch nargin
    case 0
        % if no input arguments, create a default object
        o.ResponseTimeReResponseWindow       =  .3; %mean time of response re response window
        %To find optimum mean lick time:
        %sum(b.exptparams.TrialObject.ReferenceCountFreq.*(b.exptparams.TrialObject.SingleRefSegmentLen*cumsum(ones(size(b.exptparams.TrialObject.ReferenceCountFreq)))+b.exptparams.BehaveObject.EarlyWindow))
        o.ResponseTimeStd           = .1;
        
        o.RandomLickNumRange=[0 0];
        %o.RandomLickNumRange=[4 10];%For each trial pull from a uniform distribution in this range and add this number of licks
        o.RandomLickTimeRange={0,'end'};
        o.TrialSound=[];
        o.DesiredResponseTimes      = [];
        o = class(o,'HearsTarget');
        o = ObjUpdate(o);
    case 1
        % if single argument of class SoundObject, return it
        if isa(varargin{1},'HearsTarget')
            o = varargin{1};
        else
            error('Wrong argument type'); 
        end
    otherwise
        error('Wrong number of input arguments');
end
