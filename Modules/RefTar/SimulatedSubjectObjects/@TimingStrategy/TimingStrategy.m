function o = TimingStrategy (varargin)
%
% Positive Reinforcement behavior control, hacked from RewardTarget
%
% SVD 2012-06-01

switch nargin
    case 0
        % if no input arguments, create a default object
        o.ResponseTimeInitial       = 2.7;
        %To find optimum mean lick time:
        %sum(b.exptparams.TrialObject.ReferenceCountFreq.*(b.exptparams.TrialObject.SingleRefSegmentLen*cumsum(ones(size(b.exptparams.TrialObject.ReferenceCountFreq)))+b.exptparams.BehaveObject.EarlyWindow))
        o.ResponseTimeStd           = 0.2;
        o.ResponseTimeStep          = 0.2;
        o.TrialSound=[];
        o.DesiredResponseTimes      = [];
        o = class(o,'TimingStrategy');
        o = ObjUpdate(o);
    case 1
        % if single argument of class SoundObject, return it
        if isa(varargin{1},'TimingStrategy')
            o = varargin{1};
        else
            error('Wrong argument type'); 
        end
    otherwise
        error('Wrong number of input arguments');
end
