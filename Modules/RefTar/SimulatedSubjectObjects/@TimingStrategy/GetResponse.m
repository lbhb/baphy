function [ev,o] = GetResponse(o,BehaviorObject,StimEvents,exptparams)
    if ~isfield(exptparams,'Performance') || ismember(exptparams.Performance(end).ThisTrial,{'Hit','Miss'})
        desired_time=o.ResponseTimeInitial;
    else
        desired_time=o.DesiredResponseTimes(end)+o.ResponseTimeStep;
    end
    o.DesiredResponseTimes(end+1)=desired_time;
    response_time = desired_time + o.ResponseTimeStd*randn;
    ev.Note='LICK';
    ev.StartTime=response_time;
    ev.StopTime=response_time;
    
    if 0
        SR=get(get(exptparams.TrialObject,'ReferenceHandle'),'SamplingRate');
        N=4; Fc=100; Wc=Fp/SR*2;
        [B,A] = butter(N,Wc);
        env=abs(hilbert(o.TrialSound));
        envf=filter(B,A,env);
    end
end






