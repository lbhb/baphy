function [w,exptparams] = GenerateEventWindows(o,exptparams,StimEvents,par,GenerateThese)
% [w,exptparams] = GenerateEventWindows(o,exptparams,StimEvents,par,GenerateThese)
%
% Generates w, a structure containing stimulus and pump event windows.
% Modifies exptparams, possibly changing UniqueTargets and UniqueTarResponseWinStart
%
% Inputs:
% o - BehaviorObject
% exptparams - baphy exptparams
% StimEvents - structure of stimulus events (made in waveform)
% par - BehaviorObject parameters to use.
%   Used to pass parameters than may have been modified just for this
%   trial by CueModifications, etc.
% GenerateThese - a cell that lists which windows to generate
%    Can contain: {'StimBeahviorPumpWindows','PumpActualWindows'}
%    When di_nolick is called directly for post-hoc
%    analysis, both windows categories are made.
%    When running behavior, these are StimBeahviorPumpWindows are made in the
%    call from CanStart, and PumpActualWindows are made in the call from
%    PerformanceAnalysis
%
% Outputs:
% w - structure w containing requested windows.
% exptparams - modified copy of exptparams. Field possibly changed are
%   UniqueTargets and UniqueTarResponseWinStart

% LAS 2021

if nargin < 4
    par=[];
end
if nargin < 5
    GenerateThese={'StimBeahviorPumpWindows','PumpActualWindows'};
end

if isempty(par)
    par = get(o);
end

if any(strcmp(GenerateThese,'StimBeahviorPumpWindows'))
    %Fills out structure w with the fields initialized below.
    % Updates exptparams.UniqueTargets and UniqueTarResponseWinStart
    %%
    w.AllTargetOrCatchNotes={};
    w.FirstRefTime=inf;
    w.TarTimes=[];
    w.CatchTimes=[];
    w.TarOrCatchTimes=[]; %accumulate these in this loop so they stay in order
    w.TarOrCatchResponseWin=NaN(0,2);
    w.TarOrCatchEarlyWin=NaN(0,2);
    w.TarResponseWin=NaN(0,2);
    w.TarEarlyWin=NaN(0,2);
    w.CatchResponseWin=NaN(0,2);
    w.CatchEarlyWin=NaN(0,2);
    w.RefEarlyWin = zeros(0,2);
    w.RefResponseWin = zeros(0,2);
    w.OptoLight=-1;
    w.NullTrial=1;
    w.NumRef = 0;
    w.NullTrial=1;
    w.RemTrial=0;
    w.RemStartTime=NaN;
    w.RemEarlyWin = zeros(0,2);
    w.RemResponseWin = zeros(0,2);
    %% TrialObject-specific code
    if strcmpi(get(exptparams.TrialObject, 'Descriptor'),'RepDetect')
        tar=get(exptparams.TrialObject,'TargetIdx');
        exptparams.UniqueTargets=cell(1,length(tar));
        for ii=1:length(tar)
            exptparams.UniqueTargets{ii}=sprintf('d%02d',tar(ii));
            exptparams.UniqueTargets{ii+length(tar)}=sprintf('s%02d',tar(ii));
        end
    end
    
    %% generate lists of behaviorally relevant event times
    for cnt1 = 1:length(StimEvents)
        [Type, Note, StimRefOrTar]=ParseStimEvent(StimEvents(cnt1));
        if ~isempty(findstr(StimEvents(cnt1).Note,'+NoLight'))
            w.OptoLight=0;
        elseif ~isempty(findstr(StimEvents(cnt1).Note,'+Light'))
            w.OptoLight=1;
        end
        if strcmpi(Type,'Stim')
            % svd moved reset for these variables inside the if statement so
            % that they preserve non-empty values following the last iteration
            % of this loop (which is a PostStim rather than Stim event)
            w.ThisTargetNote=[];
            ThisCatchNote=[];
            if strcmpi(StimRefOrTar,'Reference')
                w.NumRef=w.NumRef+1;
                if w.FirstRefTime > StimEvents(cnt1).StartTime
                    w.FirstRefTime = StimEvents(cnt1).StartTime;
                end
                w.RefEarlyWin(w.NumRef,:) = [StimEvents(cnt1).StartTime ...
                    StimEvents(cnt1).StartTime + par.EarlyWindow];
                w.RefResponseWin(w.NumRef,:) = [StimEvents(cnt1).StartTime + par.EarlyWindow ...
                    StimEvents(cnt1).StartTime + par.EarlyWindow  + par.ResponseWindow];
            elseif strcmpi(StimRefOrTar,'Target') || strcmpi(StimRefOrTar,'Catch')
                if strcmpi(StimRefOrTar,'Target')
                    w.TarTimes(end+1)= StimEvents(cnt1).StartTime;
                    w.TarResponseWin(end+1,:) = [StimEvents(cnt1).StartTime + par.EarlyWindow ...
                        StimEvents(cnt1).StartTime + par.ResponseWindow + ...
                        par.EarlyWindow];
                    w.TarResponseWin(end,:)=round(w.TarResponseWin(end,:)*100)./100;
                    w.TarEarlyWin(end+1,:) = [StimEvents(cnt1).StartTime ...
                        StimEvents(cnt1).StartTime + par.EarlyWindow];
                    w.ThisTargetNote=Note;
                    w.AllTargetOrCatchNotes{end+1}=w.ThisTargetNote;
                else %Catch
                    w.CatchTimes(end+1)= StimEvents(cnt1).StartTime;
                    w.CatchResponseWin(end+1,:) = [StimEvents(cnt1).StartTime + par.EarlyWindow ...
                        StimEvents(cnt1).StartTime + par.ResponseWindow + ...
                        par.EarlyWindow];
                    w.CatchResponseWin(end,:)=round(w.CatchResponseWin(end,:)*100)./100;
                    w.CatchEarlyWin(end+1,:) = [StimEvents(cnt1).StartTime ...
                        StimEvents(cnt1).StartTime + par.EarlyWindow];
                    ThisCatchNote=Note;
                    w.AllTargetOrCatchNotes{end+1}=ThisCatchNote;
                end
                w.NullTrial=0; % target occurs, not a null trial
                w.TarOrCatchTimes(end+1)= StimEvents(cnt1).StartTime;
                w.TarOrCatchResponseWin(end+1,:) = [StimEvents(cnt1).StartTime + par.EarlyWindow ...
                    StimEvents(cnt1).StartTime + par.ResponseWindow + ...
                    par.EarlyWindow];
                w.TarOrCatchResponseWin(end,:)=round(w.TarOrCatchResponseWin(end,:)*100)./100;
                w.TarOrCatchEarlyWin(end+1,:) = [StimEvents(cnt1).StartTime ...
                    StimEvents(cnt1).StartTime + par.EarlyWindow];
                % force end of reference window to match begining of early
                % window.  no FAs allowed at all.
                %TarOffTime=StimEvents(cnt1).StopTime; Unused
                w.NullTrial=0;
                
                if exptparams.TotalTrials==1
                    switch exptparams.TrialObjectClass
                        case {'MultiRefTar','RSSToneMono'}
                            tar=get(exptparams.TrialObject,'TargetHandle');
                            exptparams.UniqueTargets=get(tar,'Names');
                        case 'RepDetect'
                            tar=get(exptparams.TrialObject,'TargetIdx');
                            exptparams.UniqueTargets=cell(1,length(tar));
                            for ii=1:length(tar)
                                exptparams.UniqueTargets{ii}=sprintf('d%02d',tar(ii));
                                exptparams.UniqueTargets{ii+length(tar)}=sprintf('s%02d',tar(ii));
                            end
                        case 'ReferenceTarget'
                            ref_par = get(get(exptparams.TrialObject,'ReferenceHandle'));
                            if isfield(ref_par,'UniqueTargets')
                                exptparams.UniqueTargets = ref_par.UniqueTargets;
                            end
                    end
                    exptparams.UniqueTarResponseWinStart=[];
                end
                if strcmpi(get(exptparams.TrialObject,'descriptor'),'RepDetect')
                    w.ThisTargetNote=strsep(w.ThisTargetNote,'+',1);
                    if length(w.ThisTargetNote) > 1
                        w.ThisTargetNote = ['d', w.ThisTargetNote{1}];
                    else
                        w.ThisTargetNote = ['s', w.ThisTargetNote{1}];
                    end
                end
                
                if ~isfield(exptparams,'UniqueTargets') || isempty(exptparams.UniqueTargets)
                    exptparams.UniqueTargets={w.ThisTargetNote};
                    exptparams.UniqueTarResponseWinStart=[];
                elseif ~iscell(exptparams.UniqueTargets)
                    td=exptparams.UniqueTargets;
                    exptparams.UniqueTargets={};
                    for taridx=1:length(td)
                        exptparams.UniqueTargets{taridx}=num2str(td(taridx));
                    end
                    exptparams.UniqueTargets=union(exptparams.UniqueTargets,{w.ThisTargetNote});
                elseif ~any(strcmpi(w.ThisTargetNote,exptparams.UniqueTargets)) && ~isempty(w.ThisTargetNote)
                    exptparams.UniqueTargets=union(exptparams.UniqueTargets,{w.ThisTargetNote});
                end
                if ~any(strcmpi(ThisCatchNote,exptparams.UniqueTargets)) && ~isempty(ThisCatchNote)
                    exptparams.UniqueTargets=union(exptparams.UniqueTargets,{ThisCatchNote});
                end
                exptparams.UniqueTarResponseWinStart=...
                    union(exptparams.UniqueTarResponseWinStart,w.TarOrCatchResponseWin(:,1));
            elseif strcmpi(StimRefOrTar,'Reminder')
                w.RemResponseWin = [StimEvents(cnt1).StartTime + par.EarlyWindow ...
                    StimEvents(cnt1).StartTime + par.ResponseWindow + par.EarlyWindow];
                w.RemEarlyWin = [StimEvents(cnt1).StartTime ...
                    StimEvents(cnt1).StartTime + par.EarlyWindow];
                % force end of reference window to match begining of early
                % window.  no FAs allowed at all.
                w.RemStartTime=StimEvents(cnt1-1).StartTime;  % time to stop sound if hit
                w.RemTrial=1;  % remember that reminder occurs
            end
        end
    end
    w.Ntar=size(w.TarResponseWin,1);
    w.NtarOrcatch=size(w.TarOrCatchResponseWin,1);
    w.RefResponseWinMerged=min(w.RefResponseWin(:,1));
    if w.Ntar==1
        w.RefResponseWinMerged(1,2)=w.TarEarlyWin(1,1);
    elseif w.Ntar==2
        [~,tar1i]=min(w.TarEarlyWin(:,1));
        tar2i=setdiff(1:2,tar1i);
        w.RefResponseWinMerged(1,2)=w.TarEarlyWin(tar1i,1);
        if w.TarResponseWin(tar1i,2) < w.TarResponseWin(tar2i,1)
            %If first target window ends before second target window starts,
            %add another reference window between them
            w.RefResponseWinMerged(2,1)=w.TarResponseWin(tar1i,2);
            w.RefResponseWinMerged(2,2)=w.TarResponseWin(tar2i,1);
        end
    else
        error('3 targets not yet implemented. Make general code to snip out target periods from ref.')
    end
    
    trialparms=get(exptparams.TrialObject);
    %w.two_target=iscell(trialparms.TarIdx) && any(cellfun(@length,trialparms.TarIdx)>1);
    w.two_target=(isfield(trialparms,'TargetDistSet') && any(trialparms.TargetDistSet>1));
    
    if par.TrainingPumpDur>0
        TrainingPumpOnsetReTar = getparm(par,'TrainingPumpOnsetReTar',NaN);
        if isnan(TrainingPumpOnsetReTar)
            TrainingPumpOnset = mean(w.TarResponseWin);
        else
            TrainingPumpOnset = w.TarTimes(1) + TrainingPumpOnsetReTar;
        end
        w.TrainingPumpWin = TrainingPumpOnset + [0 par.TrainingPumpDur];
    end
    
    if ismethod(get(exptparams.TrialObject,'ReferenceHandle'),'GenerateEventWindowsExtras')
        ReferenceHandle = get(exptparams.TrialObject,'ReferenceHandle');
        w=GenerateEventWindowsExtras(ReferenceHandle,w,exptparams,StimEvents);
    end
end

if any(strcmp(GenerateThese,'PumpActualWindows'))
    PumpEvents=exptparams.temporary.BehaviorEvents(arrayfun(@(x)contains(x.Note,',Pump'),exptparams.temporary.BehaviorEvents));
    TrainingPumpEvents=exptparams.temporary.BehaviorEvents(arrayfun(@(x)contains(x.Note,',TrainingPump'),exptparams.temporary.BehaviorEvents));
    if length(TrainingPumpEvents)==1
        w.TrainingPumpWinActual = [TrainingPumpEvents(1).StartTime TrainingPumpEvents(1).StopTime];
    else
        w.TrainingPumpWinActual = NaN;
    end
    if length(PumpEvents)==1
        w.PumpWinActual = [PumpEvents(1).StartTime PumpEvents(1).StopTime];
    else
        w.PumpWinActual = NaN;
    end
end