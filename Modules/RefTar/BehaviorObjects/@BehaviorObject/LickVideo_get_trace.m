function Data = LickVideo_get_trace(o,HW,Data)
%Data = LickVideo_get_trace(o,HW,Data)
% Gets lick trace via TCPIP and puts it in the first column of Data.Responses

%Get thresholded lick trace
MSG = ['GETVAR',HW.Lick.COMterm,'[MG.lick_trace_dig]',HW.Lick.MSGterm];
LTd=[];
while isempty(LTd) || int8(LTd(1))==76 || int8(LTd(1))==83
    LTd=IOSendMessageTCPIP(HW.Lick,MSG,[],'',0,0);
end
LTd_=str2num(LTd);

%Get frame times
MSG = ['GETVAR',HW.Lick.COMterm,'MG.t',HW.Lick.MSGterm];
t=[];
while isempty(t) || int8(t(1))==76
    t=IOSendMessageTCPIP(HW.Lick,MSG,[],'',0,0);
end
t_=str2num(t);

%Truncate LTd_and t_ to be 20 samples after the last lick.
last_ind=min([find(LTd_,1,'last')+20,length(t_),length(LTd_)]);
t__=t_;
LTd__=LTd_;
LTd__(last_ind+1:end)=[];
t__(last_ind+1:end)=[];

%Interpolate from asynchronously-sampled lick trace to evenly-sampled data format from DAC
%Put VidoLicks in first row, move the rest down
tAI=(0:(size(Data.Responses)-1))'/HW.params.fsAI;
Data.Responses(:,1)=[interp1(t__,LTd__,tAI) , Data.Responses];

%Old way (get licks as discrete time events). Doesn't work great because
%you can't visualize at what times the video frames were analyzed
%               MSG = ['GETVAR',HW.Pupil.COMterm,'[MG.lick_times]',HW.Pupil.MSGterm];
%               LTchar=[];
%               while isempty(LTchar) || int8(LTchar(1))==76
%                   LTchar=IOSendMessageTCPIP(HW.Pupil,MSG);
%               end
%               LT=str2num(LTchar);
%               Data.Responses(:,2)=0;
%               Data.Responses(round(HW.params.fsAI*LT),2)=1;