function LickTCPIP(o,obj, eventdata)
global TCPIP_Lick Verbose
    aa=2;
    if ~obj.BytesAvailable
        fprintf('WARNING : No Bytes Available.\n');
        return;
    end
    MSGterm=33;
    COMterm=124;
    ArrivalTime = now;
    tmp = char(fread(obj,obj.BytesAvailable))'; %flushinput(obj);
    Terms = find(tmp==MSGterm); Terms = [0,Terms];
    for i=2:length(Terms) Messages{i-1} = tmp(Terms(i-1)+1:Terms(i)-1); end
    Pos = find(int8(Messages{end})==COMterm);
    if isempty(Pos)  Pos = length(Messages{end})+1; end
    [TV,TS] = datenum2time(ArrivalTime);
    
    if Verbose==2
        fprintf([' <---> TCPIP message received: ',escapeMasker(Messages{end}),' (',TS{1},')\n']);
    end
    
    COMMAND = Messages{end}(1:Pos-1);
    DATA = Messages{end}(Pos+1:end);

    switch COMMAND
        case 'LICK'
            %fprintf('Lick message received on LickTCPIP\n')
            %o.TCPIP_Lick=1; %Doesn't work
            TCPIP_Lick=1;
            flushinput(obj);
        otherwise
            fprintf('Unknown message received on LickTCPIP\n')
    end
end