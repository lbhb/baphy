function b = BehaviorObject(varargin)
% BehaviorObject Creates a behavior object that is the base class of
% RewardTargetLBHB and ClassicalConditioning

% Luke Shaheen April 2021

switch nargin
case 1
    % if no input arguments, create a default object
    b=struct;
    b.descriptor = varargin{1};
    %If you add/change fields here, change them in RewardTargetLBHB., too
    %Both objects inherit the BehaviorDisplay method from @BehaviorObject
    b.DisplayParams.show_Misses=1;
    b.DisplayParams.show_EarlyLicks=1;
    b.DisplayParams.show_RecentHitRate=1;
    b.DisplayParams.show_RecentFalseAlarmRate=1;
    b.DisplayParams.legend_overtime_unique=0;
    b.DisplayParams.show_UniqueHitRatebyTime=1;
    b.DisplayParams.cum_rt_FA_histogram_style='re_ref'; % Luke's way (re-each ref onset. Lick before first reference are negative'
    %b.DisplayParams.cum_rt_FA_histogram_style='re_trial'; %original behavior
    %b.DisplayParams.cum_rt_source='BehaviorDisplay';
    b.DisplayParams.cum_rt_source='di_nolick';
    %b.DisplayParams.plot_performance_over='trial_start_time'; % Marmoset-initiated trials way: plot behavior by trial start time
    b.DisplayParams.plot_performance_over='index'; % default, original behavior';
    b = class(b,'BehaviorObject');
    b = ObjUpdate (b);
    otherwise
        error('Too many inputs')
end