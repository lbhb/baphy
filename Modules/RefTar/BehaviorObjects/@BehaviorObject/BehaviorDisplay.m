function exptparams=BehaviorDisplay(o,HW,StimEvents,globalparams,exptparams,TrialIndex, ...
    AIData,TrialSound,varargin)
% exptparams=BehaviorDisplay(o,HW,StimEvents,globalparams,exptparams,TrialIndex, ...
%    AIData,TrialSound)
% exptparams=BehaviorDisplay(o,HW,StimEvents,globalparams,exptparams,TrialIndex, ...
%    AIData,TrialSound,TrialStartTimes)
% BehaviorDisplay method of RewardTargetLBHB behavior (modified from RewardTarget)
% Main duties of this method is to display a figure with:
%  Title that has the info about ferret, ref, tar, date, time
%  plot of Hit and False Alarm rate
%  a plot of lick in the last trial with correct labling
%  a histogram of first lick for ref and tar
%  Hit and False Alarm for each position
%  after each TrialBlock, display the performance data
DisplayParams = get(o,'DisplayParams');
show_Misses=getparm(DisplayParams,'show_Misses',1,1);
show_EarlyLicks=getparm(DisplayParams,'show_EarlyLicks',1,1);
show_RecentHitRate=getparm(DisplayParams,'show_RecentHitRate',1,1);
show_RecentFalseAlarmRate=getparm(DisplayParams,'show_RecentFalseAlarmRate',1,1);
legend_overtime_unique=getparm(DisplayParams,'legend_overtime_unique',1,1);
show_UniqueHitRatebyTime=getparm(DisplayParams,'show_UniqueHitRatebyTime',1,1);
cum_rt_FA_histogram_style=getparm(DisplayParams,'cum_rt_FA_histogram_style','re_ref');
cum_rt_source=getparm(DisplayParams,'cum_rt_source','BehaviorDisplay');
plot_performance_over=getparm(DisplayParams,'plot_performance_over','index');
w = exptparams.temporary.EventWindows; % Created in CanStart
RefMethods=methods(get(exptparams.TrialObject,'ReferenceClass'));
    
IsSimulation=false;
try
    IsSimulation=get(o,'IsSimulation');
end
num_non_cue=sum(~[exptparams.TrialParams(1:TrialIndex).CueSeg]);
if IsSimulation && mod(num_non_cue-1,exptparams.TrialBlock) && ~isfield(exptparams,'StopTime')
    return
end

TrialStartTimes=getparmC(varargin,1,[]);
if strcmp(plot_performance_over,'index') || isempty(TrialStartTimes)
    TrialStartTimes=1:TrialIndex;
    xlab='Trial Number'; 
    xl=[1 TrialIndex+1];
    Start_Times_are_Indexes=true;
else
    xlab='Trial onset time (min re start)'; 
    xl=[0 (max(TrialStartTimes)+1)];
    %calculate RecentStartTmeMean
    RecentIndex = max(1, TrialIndex-exptparams.TrialBlock+1):TrialIndex;
    exptparams.Performance(end-1).RecentStartTimeMean=mean(TrialStartTimes(RecentIndex));
    Start_Times_are_Indexes=false;
end

if size(AIData,2)>1,
    %Nima, april 2006: if there is paw signal, take it out:
    %AIData = AIData(:,1);
    %LAS, Feb 2018: if there is paw signal, keep it in but scale:
    AIData(:,2) = AIData(:,2)*.9;
end
try
    limit_view = strcmp(get(exptparams.TrialObject,'SelfTestMode'),'Yes');
catch
    limit_view = 0;
end

if isfield(exptparams,'StopTime')
    limit_view=false;
end
fs = HW.params.fsAI;
figure(exptparams.ResultsFigure);

if ~limit_view
    subplot(4,4,1:4);
    % if Trial block is reached or the experiment has ended, then
    % show the block on the screen:
    num_non_cue=sum(~[exptparams.TrialParams(1:TrialIndex).CueSeg]);
    if isempty(AIData) && isempty(TrialSound) % if this it the last one:
        ind = 1-(ceil(num_non_cue/exptparams.TrialBlock))/25;
    else
        ind = 1-(floor(num_non_cue/exptparams.TrialBlock))/25;
    end
    %subplot(4,4,[11 12 15 16]);axis off;
    subplot(5,4,[ 15 16 19 20]);axis off;
    if TrialIndex==1 % this it the first time:
        text(  0,1  ,'Trial','FontWeight','bold','HorizontalAlignment','center');
        text(0.1,1  ,'HR','FontWeight','bold','HorizontalAlignment','center','color','b');
        text(0.2,1  ,'FR','FontWeight','bold','HorizontalAlignment','center');
        text(0.3,1  ,'DR','FontWeight','bold','color','b','HorizontalAlignment','center');
        text(0.4,1  ,'DI','FontWeight','bold','HorizontalAlignment','center');
        text(0.5,1  ,'WaR','FontWeight','bold','color','b','HorizontalAlignment','center');
        text(0.6,1  ,'bHR','FontWeight','bold','color','b','HorizontalAlignment','center');
        text(0.7,1  ,'bFR','FontWeight','bold','HorizontalAlignment','center');
        text(0.8,1  ,'bDR','FontWeight','bold','color','b','HorizontalAlignment','center');
    end
    if (~exptparams.TrialParams(TrialIndex).CueSeg && num_non_cue>0 && ~mod(num_non_cue,exptparams.TrialBlock)) || (isempty(AIData) && isempty(TrialSound))
        
        % display trialnumber, SR, HR, DR, hitnum, Snooze Hit Rate, Snooze, shams,
        text(0, ind, num2str(TrialIndex),'FontWeight','bold','HorizontalAlignment','center');
        if isfield(exptparams, 'Performance')
            text(.1, ind, num2str(exptparams.Performance(end).HitRate,'%2.0f'),'FontWeight','bold','HorizontalAlignment','center','color','b');
            text(.2, ind, num2str(exptparams.Performance(end).FalseAlarmRate,'%2.0f'),'FontWeight','bold','HorizontalAlignment','center');
            text(.3, ind, num2str(exptparams.Performance(end).DiscriminationRate,'%2.0f'),'FontWeight','bold','HorizontalAlignment','center','color','b');
            text(.4, ind, [num2str(exptparams.Performance(end).DiscriminationIndex,'%2.0f'),'/',num2str(exptparams.Performance(end).DiscriminationIndex_NewT,'%2.0f')],'FontWeight','bold','HorizontalAlignment','center');
            %text(.5, ind, num2str(exptparams.Performance(end).WarningRate,'%2.0f'),'FontWeight','bold','color','b','HorizontalAlignment','center');
            %text(.6, ind, num2str(exptparams.Performance(end).IneffectiveRate,'%2.0f'),'FontWeight','bold','HorizontalAlignment','center');
            text(.6, ind, num2str(exptparams.Performance(end).RecentHitRate,'%2.0f'),'FontWeight','bold','color','b','HorizontalAlignment','center');
            text(.7, ind, num2str(exptparams.Performance(end).RecentFalseAlarmRate,'%2.0f'),'FontWeight','bold','HorizontalAlignment','center');
            text(.8, ind, num2str(exptparams.Performance(end).RecentDiscriminationRate,'%2.0f'),'FontWeight','bold','color','b','HorizontalAlignment','center');
            %         text(.14, ind, num2str(exptparams.Performance(end).LickRate,'%2.0f'),'FontWeight','bold','color','b','HorizontalAlignment','center');
            %         text(.14, ind, num2str(exptparams.Performance(end).LickRate,'%2.0f'),'FontWeight','bold','color','b','HorizontalAlignment','center');
            %         text(.14, ind, num2str(exptparams.Performance(end).LickRate,'%2.0f'),'FontWeight','bold','color','b','HorizontalAlignment','center');
            %         text(.7, ind, [num2str(exptparams.Performance(end).WarningTrial(1),'%2.0f') '/' num2str(exptparams.Performance(end).WarningTrial(2),'%2.0f')],'FontWeight','bold','color','b','HorizontalAlignment','center');
            %         text(.84, ind, [num2str(exptparams.Performance(end).Ineffective(1),'%2.0f') '/' num2str(exptparams.Performance(end).Ineffective(2),'%2.0f')],'FontWeight','bold','HorizontalAlignment','center');
            %         text( 1, ind, [num2str(exptparams.Performance(end).EarlyTrial(1),'%2.0f') '/' num2str(exptparams.Performance(end).EarlyTrial(2),'%2.0f')],'FontWeight','bold','color','b','HorizontalAlignment','center');
        end
    end
end
%disp('stopping in BehaviorDisplay');
%keyboard


%% display Hitrate, FalseAlarmRate, DI across trials
HWSetup = BaphyMainGuiItems('HWSetup');
titleMes = ['Animal: ' globalparams.Ferret '     Reference: ' ...
    get(exptparams.TrialObject,'ReferenceClass') '     Target: ' ...
    get(exptparams.TrialObject,'TargetClass') '     Rig: ' , HWSetup{1+globalparams.HWSetup} '       ' ...
    num2str(exptparams.StartTime(1:3),'Date: %1.0f-%1.0f-%1.0f') '     ' ...
    num2str(exptparams.StartTime(4:6),'Start: %1.0f:%1.0f:%1.0f')];
if isfield(exptparams,'StopTime')
    titleMes = [titleMes '     ' num2str(exptparams.StopTime(4:6),'Stop: %1.0f:%1.0f:%1.0f')];
end
if isempty(AIData) && isempty(TrialSound)
    % this is the end, things has been displayed already:
    subplot(4,4,1:4);
    title(titleMes,'interpreter','none');
    % also turn off the light if BehaveorPassive:
    if strcmpi(get(o,'TurnOnLight'),'BehaveOrPassive') && ~isfield(exptparams,'OfflineAnalysis')
        [ll,ev] = IOLightSwitch (HW, 0);
    end
    drawnow;
    return;
end

if isfield(exptparams,'UniqueTargets') && length(exptparams.UniqueTargets)>1
    cmN=length(exptparams.UniqueTargets);
else
    cmN=1;
end
colormtx=cbcolormap(cmN,0);
while size(colormtx,1)<cmN
    colormtx=[colormtx; colormtx];
end

% if ismethod(get(exptparams.TrialObject,'ReferenceClass'),'get_target_suffixes')
%     tar_suffixes=get_target_suffixes(get(exptparams.TrialObject,'ReferenceClass'),[],get(exptparams.TrialObject,'ReferenceHandle'));
%     for i=1:length(tar_suffixes)
%         if w.two_target
%             trialref_type(arrayfun(@(x)~isempty(strfind(x.ThisTargetNote{1},tar_suffixes{i})),exptparams.Performance(end-1)))=i;
%         else
%             trialref_type(arrayfun(@(x)~isempty(strfind(x.ThisTargetNote,tar_suffixes{i})),perf(1:trialcount)))=i;
%         end
%     end
% else
%     tar_suffixes=cell(1);
%      trialref_type=ones(1,trialcount);
% end

%colormtx=jet;
%colormtx=parula;
%colormtx=colormtx(round(linspace(1,56,length(exptparams.UniqueTargets))),:); %go to 56/64 to cutoff some of the yellow

if ~limit_view
    if TrialIndex>100
        ms=5;
    else
        ms=8;
    end
    subplot(4,4,1:4);
    cla;
    if isfield(exptparams,'UniqueTargets') && length(exptparams.UniqueTargets)>1
        slegend = {};
        hl=[];
        
        hold on;
        UniqueCount=length(exptparams.UniqueTargets);
        if show_UniqueHitRatebyTime
            HR_ = 100*cat(1,exptparams.Performance(1:end-1).uHitRate);
        else
            HR_=repmat(100*cat(1,exptparams.Performance(1:end-1).HitRate),1,UniqueCount);
        end
        if ~show_UniqueHitRatebyTime
            hl(1)=plot(TrialStartTimes,HR_(:,1),'-',...
                'LineWidth',2,'color',[1 .5 .5]);
            slegend{length(hl)} = 'HR';
        end
        targetid={exptparams.Performance(1:TrialIndex).ThisTargetNote};
        ff = fields(get(exptparams.TrialObject));
        if ~isempty(find(contains(ff,'IsCatch_')))
            IsCatch_=get(exptparams.TrialObject,'IsCatch_');
            
            IsCatch_=IsCatch_(:);
        else
            IsCatch_=zeros(UniqueCount,1);
        end
        for jj=1:UniqueCount
            if w.two_target
                thistargetii=find(cellfun(@(x)any(strcmp(x,exptparams.UniqueTargets{jj})),targetid));
            else
                thistargetii=find(strcmp(targetid,exptparams.UniqueTargets{jj}));
            end
            if ~isempty(thistargetii)
                if IsCatch_(jj)==0
                    mark='s';
                else
                    mark='o';
                end
                this_h=plot(TrialStartTimes(thistargetii),HR_(thistargetii,jj),'-','Marker',mark,...
                    'MarkerFaceColor',colormtx(jj,:),'color',colormtx(jj,:),'MarkerSize',3);
                if ~show_UniqueHitRatebyTime
                    set(this_h,'LineStyle','none');
                end
                ids=thistargetii(arrayfun(@(x)x.uHit(jj)==1,exptparams.Performance(thistargetii))); %hits
                plot(TrialStartTimes(ids),HR_(ids,jj),'Marker',mark,'LineStyle','none',...
                    'MarkerFaceColor',colormtx(jj,:),'color',colormtx(jj,:),'MarkerSize',5);
                ids=thistargetii(arrayfun(@(x)x.uHit(jj)==0,exptparams.Performance(thistargetii)));%misses
                plot(TrialStartTimes(ids),HR_(ids,jj),'Marker',mark,'LineStyle','none',...
                    'MarkerFaceColor','w','color',colormtx(jj,:),'MarkerSize',5);
                
                if legend_overtime_unique
                    hl(length(hl)+1)=this_h;
                    slegend{length(hl)} = sprintf('HR %s',strrep(exptparams.UniqueTargets{jj},'_','\_'));
                end
            end
        end
    else
        hl(1)=plot(TrialStartTimes,100*cat(1,exptparams.Performance(1:end-1).HitRate),'o-',...
            'LineWidth',2,'MarkerFaceColor',[1 .5 .5],'MarkerSize',5,'color',[1 .5 .5]);
        slegend = {'HR'};
    end
    hold on;
    hl(length(hl)+1)=plot(TrialStartTimes,100*cat(1,exptparams.Performance(1:end-1).FalseAlarmRate),'<-',...
        'LineWidth',2,'MarkerFaceColor',[.1 .5 .1],'MarkerSize',4,'color',[.1 .5 .1]);
    uistack(hl(end),'bottom')
    slegend{length(hl)} = 'FAR';
    hl(length(hl)+1)=plot(TrialStartTimes,100*cat(1,exptparams.Performance(1:end-1).DiscriminationIndex),'-',...
        'LineWidth',1,'color',[0 0 0]);
    slegend{length(hl)} = 'DI';
    
    %recent:
    if(show_RecentHitRate || show_RecentFalseAlarmRate)
        % if TrialIndex > 1
        if Start_Times_are_Indexes
            xv=[exptparams.Performance(1:end-1).RecentIndexMean];
        else
            xv=[exptparams.Performance(1:end-1).RecentStartTimeMean];
        end
        % else
        %     xv=TrialStartTimes;
        % end
    end
    if(show_RecentHitRate)
        hl2(1)=plot(xv,...
            100*cat(1,exptparams.Performance(1:end-1).RecentHitRate),'-',...
            'LineWidth',1,'color',[1 .5 .5]);
    end
    if(show_RecentFalseAlarmRate)
        hl2(2)=plot(xv,...
            100*cat(1,exptparams.Performance(1:end-1).RecentFalseAlarmRate),'-',...
            'LineWidth',1,'color',[.1 .5 .1]);
    end
    
    % also, show which trials were Ineffective:
    AllIneffective = cat(1,exptparams.Performance(1:TrialIndex).FalseAlarm);
    AllIneffective(AllIneffective==0)=nan;
    if ~isempty(AllIneffective)
        hl(length(hl)+1)=plot(TrialStartTimes,110*AllIneffective,'r*','markersize',ms);
        slegend{length(hl)} = 'FA';
    end
    AllEarly = cat(1,exptparams.Performance(1:TrialIndex).EarlyTrial);
    AllEarly(AllEarly==0)=nan;
    if(show_EarlyLicks)
        if ~isempty(AllEarly)
            hl(length(hl)+1)=plot(TrialStartTimes,110*AllEarly,'+','Color',[1 .5 .5],'markersize',ms);
            slegend{length(hl)} = 'Early';
        end
    end
    AllMiss = cat(1,exptparams.Performance(1:TrialIndex).Miss);
    AllMiss(AllMiss==0)=nan;
    if(show_Misses)
        if ~isempty(AllMiss)
            hl(length(hl)+1)=plot(TrialStartTimes,110*AllMiss,'bo','markersize',ms);
            slegend{length(hl)} = 'Miss';
        end
    end
    if isfield(exptparams.Performance,'NullTrial'),
        AllNull = cat(1,exptparams.Performance(1:TrialIndex).NullTrial);
        AllNull=find(AllNull==1);
        if ~isempty(AllNull),
            plot(TrialStartTimes,AllNull,110,'rs','markersize',ms);
        end
    end
    if ~Start_Times_are_Indexes
        mark_these_trials=10:10:length(TrialStartTimes);
        trial_marks=nan(1,length(mark_these_trials));
        for i=1:length(mark_these_trials)
            trial_marks(i)=text(TrialStartTimes(mark_these_trials(i)),0,...
                num2str(mark_these_trials(i)),...
                'HorizontalAlignment','center','VerticalAlignment','bottom');
        end
    end
    axis([xl 0 115])
    title(titleMes,'FontWeight','bold','interpreter','none');
    h=legend(hl,slegend,'Location','SouthWest');
    LegPos = get(h,'position');
    set(h,'fontsize',8);
    LegPos(1) = 0.005; % put the legend on the far left of the screen
    set(h,'position', LegPos);
    xlabel(xlab,'FontWeight','bold');
    
    if isfield(exptparams,'TrialParams'),
        CueTrials=find([exptparams.TrialParams.CueSeg]==1);
        CueTrials(CueTrials>TrialIndex)=[];
        for i=1:length(CueTrials)
            text(TrialStartTimes(CueTrials(i)),100,'C','HorizontalAlignment','center');
        end
        Cue2Trials=find([exptparams.TrialParams.CueSeg]==2);
        Cue2Trials(Cue2Trials>TrialIndex)=[];
        for i=1:length(Cue2Trials)
            text(TrialStartTimes(Cue2Trials(i)),100,'c','HorizontalAlignment','center');
        end
    end
    if isfield(exptparams,'feedback_text')
        try
            delete(exptparams.feedback_text)
        end
    end
end


%% display the lick signal and the boundaries for last trial
if ~limit_view %%|| ~(exptparams.Performance(TrialIndex).FalseAlarm || exptparams.Performance(TrialIndex).EarlyTrial)
    h = subplot(4,4,5:8);
    cla;
    
    if ~isempty(AIData),
        t=(0:(size(AIData,1)-1))/fs;
        plot(t,AIData);
        axis ([0 t(end) 0 1.5]);
    end
    if limit_view
        title(titleMes,'FontWeight','bold','interpreter','none');
    end
    %set(h,'XTickLabel',get(h,'Xtick')/fs); % convert to seconds
    xlabel('Time (seconds)','FontWeight','bold');
    % First, draw the boundries of Reference and Target
    if any(strcmp(RefMethods,'plot_stimevents_on_BehaviorDisplay'))
        plot_stimevents_on_BehaviorDisplay(get(exptparams.TrialObject,'ReferenceHandle'),StimEvents);
    else
        ref_starts=[];
        for cnt1 = 1:length(StimEvents)
            [Type, StimName, StimRefOrTar] = ParseStimEvent (StimEvents(cnt1));
            if strcmpi(Type,'Stim')
                if strcmpi(StimRefOrTar,'Reference'), c=[.1 .5 .1]; ref_starts=[ref_starts StimEvents(cnt1).StartTime]; else c=[1 .5 .5]; end
                line([StimEvents(cnt1).StartTime StimEvents(cnt1).StartTime],...
                    [0 .5],'color',c,'LineStyle','--','LineWidth',2);
                line([StimEvents(cnt1).StopTime StimEvents(cnt1).StopTime],...
                    [0 .5],'color',c,'LineStyle','--','LineWidth',2);
                line([StimEvents(cnt1).StartTime StimEvents(cnt1).StopTime],...
                    [.5 .5],'color',c,'LineStyle','--','LineWidth',2);
                text((StimEvents(cnt1).StartTime+StimEvents(cnt1).StopTime)/2,...
                    .6, StimRefOrTar(1),'color',c,...
                    'FontWeight','bold','HorizontalAlignment','center');
            end
        end
    end
    % Second, draw the boundry of response window, and early window:
    for cnt1 = 1:size(w.RefResponseWinMerged,1)
        line([w.RefResponseWinMerged(cnt1,1) w.RefResponseWinMerged(cnt1,2)],[1.1 1.1],'color','k','LineStyle','-','LineWidth',2);
        text((mean(w.RefResponseWinMerged(cnt1,:))), 1.19, 'Res','Color',[.1 .5 .1],'HorizontalAlignment','center');
    end
    c=[.1 .5 .1];
    if ~isempty(w.TarResponseWin)
        for tari=1:size(w.TarResponseWin,1)
            line([w.TarResponseWin(tari,1) w.TarResponseWin(tari,2)],[1.1 1.1]+tari*.05,...
                'color','k','LineStyle','-','LineWidth',2);
            text((mean(w.TarResponseWin(tari,:))), 1.19+tari*.05, 'Res','Color',c,...
                'HorizontalAlignment','center');
            line([w.TarEarlyWin(tari,1) w.TarEarlyWin(tari,2)],[1.3 1.3]+tari*.05,...
                'color','k','LineStyle','-','LineWidth',2);
            text((mean(w.TarEarlyWin(tari,:))), 1.39+tari*.05, 'Erl','Color',c,...
                'HorizontalAlignment','center');
        end
    end
    c=[.1 .1 .5];
    if ~isempty(w.CatchResponseWin)
        for tari=1:size(w.CatchResponseWin,1)
            line([w.CatchResponseWin(tari,1) w.CatchResponseWin(tari,2)],[1.1 1.1]+tari*.05,...
                'color',[.4 .4 .4],'LineStyle','-','LineWidth',1);
            text((mean(w.CatchResponseWin(tari,:))), 1.19+tari*.05, 'Cat','Color',c,...
                'HorizontalAlignment','center');
            line([w.CatchEarlyWin(tari,1) w.CatchEarlyWin(tari,2)],[1.3 1.3]+tari*.05,...
                'color',[.4 .4 .4],'LineStyle','-','LineWidth',1);
            text((mean(w.CatchEarlyWin(tari,:))), 1.39+tari*.05, 'Erl','Color',c,...
                'HorizontalAlignment','center');
        end
    end
    %If Ref object has a plot_stimulus_on_behaviorDisplay method, use it
    if any(strcmp(RefMethods,'plot_stimulus_on_BehaviorDisplay'))
        TrialEnvelope=[];
        try
            TrialEnvelope=get(exptparams.TrialObject,'TrialEnvelope');
        end
        plot_stimulus_on_BehaviorDisplay(get(exptparams.TrialObject,'ReferenceHandle'),get(exptparams.TrialObject,'TargetHandle'),[],TrialSound,TrialEnvelope,max(get(exptparams.TrialObject,'RelativeTarRefdB')));
        %,TrialSamplingRate,RefTrialIndex(cnt1),TrialIndex,ThisTarIdx,events,tenv)
    end
    if isfield(exptparams.temporary,'BehaviorEvents')
        PumpEvents=exptparams.temporary.BehaviorEvents(arrayfun(@(x)contains(x.Note,',Pump'),exptparams.temporary.BehaviorEvents));
        TrainingPumpEvents=exptparams.temporary.BehaviorEvents(arrayfun(@(x)contains(x.Note,',TrainingPump'),exptparams.temporary.BehaviorEvents));
        if length(TrainingPumpEvents)==1
            training_pump_time=[TrainingPumpEvents(1).StartTime TrainingPumpEvents(1).StopTime];
            yl=get(gca,'YLim');
            ph=patch(training_pump_time([1 2 2 1]),yl([1 1 2 2]),[0 .5 .5],'EdgeColor','none','FaceAlpha',.3);
            uistack(ph,'bottom');
        end
        if length(PumpEvents)==1
            pump_time=[PumpEvents(1).StartTime PumpEvents(1).StopTime];
            yl=get(gca,'YLim');
            ph=patch(pump_time([1 2 2 1]),yl([1 1 2 2]),[.5 .5 0],'EdgeColor','none','FaceAlpha',.3);
            uistack(ph,'bottom');
        end
    end
    
    stim_off_ind=0; 
    xlim_end_time=max([[StimEvents.StopTime]';w.TarOrCatchResponseWin(:);w.RefResponseWinMerged(:)]);
    if exist('pump_time','var')
        xlim_end_time = max(xlim_end_time,pump_time(2));
    end
    xlim_end_time = xlim_end_time+.05;
    if isfield(exptparams.temporary,'BehaviorEvents')
        stim_off_ind=arrayfun(@(x)strcmp(x.Note,'STIM,OFF'),exptparams.temporary.BehaviorEvents);
        plot_stim_cutoff=1;
        if sum(stim_off_ind)==0
            plot_stim_cutoff=0;
            stim_off_ind=arrayfun(@(x)strcmp(x.Note,'OUTCOME,MISS'),exptparams.temporary.BehaviorEvents);
        end
    end
    if sum(stim_off_ind)>0
            stim_off_time=exptparams.temporary.BehaviorEvents(stim_off_ind).StartTime;
            if plot_stim_cutoff
                yl=get(gca,'YLim');
                ph=patch([stim_off_time xlim_end_time xlim_end_time stim_off_time],yl([1 1 2 2]),[.9 .9 .9],'EdgeColor','none','FaceAlpha',.7);
                uistack(ph,'bottom');
            end
    end
    xl=get(gca,'XLim');
    set(gca,'XLim',[xl(1) xlim_end_time]);
end

%% First lick Histogram for target and reference
if ~limit_view
    if ~isempty(exptparams.temporary.metrics)
        subplot(4,4,9:10)
        if strcmp(cum_rt_source,'di_nolick')
            if all([exptparams.TrialParams(1:TrialIndex).CueSeg])
                di_details=exptparams.temporary.metrics_CueTrials.details;
                di_details_newT=exptparams.temporary.metrics_CueTrials.details;
                cuestr=', over cue trials';
            else
                di_details=exptparams.temporary.metrics.details;
                di_details_newT=exptparams.temporary.metrics_newT.details;
                cuestr='';
            end
        end
        legh = plot_reaction_time_histograms(exptparams,di_details,di_details_newT,TrialIndex);
        
        if strcmp(cum_rt_source,'di_nolick') && ~isempty(exptparams.temporary.metrics.details.metrics_User)
            subplot(4,4,15:16)
            di_details = exptparams.temporary.metrics.details.metrics_User.details;
            di_details_newT = [];
            legh = plot_reaction_time_histograms(exptparams,di_details,di_details_newT,TrialIndex);
            str=get(legh,'String');
            for i=1:length(str)
                DIi=strfind(str{i},'DI:');
                cpi=strfind(str{i},')');
                if isempty(DIi)
                    %str2{i}='';
                else
                    str2{i}=str{i}(DIi:cpi-1);
                end
            end 
            if exist('str2','var')
                LegPos = [ 0.6    0.25  1e-10 1e-10];% put the legend above the behavior performance table
                set(legh,'String',str2,'position', LegPos);
            end
            title('Cumulative RT Histogram (this trial)')
        end
    end
    %% and last, show the hit and false alarm for each trial length:
    subplot(4,4,13:14); hold off;
    HR=100*exptparams.PositionHit(:,1)./exptparams.PositionHit(:,2);
    stTimes=exptparams.UniqueTarResponseWinStart;
    N=min(length(HR),length(stTimes));
    plot(stTimes(1:N),HR(1:N),'marker','o',...
        'markersize',10,'color',[1 .5 .5],'linewidth',2);
    hold on;
    if isfield(exptparams,'PositionFalseAlarm')
        FAR=100*exptparams.PositionFalseAlarm(:,1)./exptparams.PositionFalseAlarm(:,2);
        N=min(length(FAR),length(stTimes));
        plot(stTimes(1:N),FAR(1:N),...
            'marker','o','markersize',10,'color',[.1 .5 .1],'linewidth',2);
    end
    axis tight;
    if length(stTimes)>2
        xlim(stTimes([1 end]))
    end
    ylim([ 0 100]);
    %xlabel('Position');
    xlabel('Target Start Time');
    h=legend({'Hit','FlsAl'});
    LegPos = get(h,'position');
    set(h,'fontsize',8);
    LegPos(1) = 0.005; % put the legend on the far left of the screen
    set(h,'position', LegPos);
    drawnow;
end


function h = plot_reaction_time_histograms(exptparams_,di_details,di_details_newT,TrialIndex)
    hold off;
    BinSize = 0.04;
    MaxBinTime=nanmax([exptparams_.FirstLick.Tar exptparams_.FirstLick.Tar])+BinSize;
    if isnan(MaxBinTime)
        MaxBinTime=BinSize;
    end
    PLOT_CUMLICK=1;
    tar_freqs=cellfun(@(x)str2double(x),exptparams_.UniqueTargets);
    tar_levels=get(exptparams_.TrialObject,'RelativeTarRefdB');
    if length(tar_levels)<length(tar_freqs)
        tar_levels = ones(size(tar_freqs)) * tar_levels(1);
    end
    has_tar_str=false;
    if isfield(exptparams_,'UniqueTargets')
        UniqueCount=length(exptparams_.UniqueTargets);
    else
        UniqueCount=0;
    end
    if UniqueCount>1
        targetid={exptparams_.Performance(1:TrialIndex).ThisTargetNote};
        
        %% tar_str_ (or don't), a cell that
        if strcmpi(get(exptparams_.TrialObject, 'Descriptor'),'RepDetect')
            has_tar_str=true;
            tarstr_ = exptparams_.UniqueTargets;
        elseif all(isnan(tar_freqs)) && isnan(str2double(exptparams_.UniqueTargets{1}))
            ts=strsep(exptparams_.UniqueTargets{1},':');
            for i=2:length(exptparams_.UniqueTargets)
                ts(i,:)=strsep(exptparams_.UniqueTargets{i},':');
            end
            for j=1:size(ts,2)
                ie(j)=all(cellfun(@(x)isequal(ts{1,j},x),ts(:,j)));
            end
            if sum(~ie)==1 && length(unique(cellfun(@length,ts(:,~ie))))==1 && all(diff(cell2mat(ts(:,~ie)))==1)
                %only one parameter is different, and it's just a numeric index.
                %use TrialParams to create legend
                has_tar_str=true;
                check_fields={'RelativeTarRefdB','TargetChannel','EnvTarRefRatio'};%,'TargetDistSet'};
                pre={'','Ch ','TRR '}; suf={'dB','',''};
                for j=1:length(check_fields)
                    vals=get(exptparams_.TrialObject,check_fields{j});
                    if length(vals)==size(ts,1) && length(unique(vals))>1
                        [ts{:,end+1}]=deal(' ');
                        ie(end+1)=0;
                        for i=1:length(exptparams_.UniqueTargets)
                            ts{i,end}=[pre{j},num2str(vals(i)),suf{j}];
                        end
                    end
                end
                for i=1:length(exptparams_.UniqueTargets)
                    pars=[cellfun(@(x)[num2str(x),':'],ts(i,~ie),'Uni',0)];
                    tarstr_{i}=[pars{:}];
                    tarstr_{i}(end)=[];
                end
            end
        end
        % if multiple different targets (eg, catch trials) plot first lick
        % histograms for each separately.
        FAcount=sum(cat(1,exptparams_.Performance(1:TrialIndex).FalseAlarm));
        FAR=FAcount./TrialIndex;
        HR=zeros(UniqueCount,1);
        RT=zeros(UniqueCount,1);
        DI=exptparams_.Performance(end).uDiscriminationIndex;
        for jj=1:UniqueCount
            thisFAcount=sum(cat(1,exptparams_.Performance(thistargetii).FalseAlarm));
            thisETcount=sum(cat(1,exptparams_.Performance(thistargetii).EarlyTrial));
            if TrialIndex > thisFAcount
                HR(jj)=exptparams_.Performance(end).uHitRate(jj)/100;
                %HR(jj)=sum(cat(1,exptparams_.Performance(thistargetii).Hit))./...
                %    (length(thistargetii)-thisFAcount-thisETcount);
            end
            RT(jj)=exptparams_.Performance(end).uReactionTime(jj);
            %RT(jj)=nanmean(exptparams_.FirstLick.Tar(thistargetii));
            switch cum_rt_source
                case 'BehaviorDisplay'
                    if w.two_target
                        thistargetii=find(cellfun(@(x)any(strcmp(x,exptparams_.UniqueTargets{jj})),targetid));
                    else
                        thistargetii=find(strcmp(targetid,exptparams_.UniqueTargets{jj}));
                    end
                    h1{jj}=hist(exptparams_.FirstLick.Tar(thistargetii),0:BinSize:MaxBinTime);
                    if ~isempty(h1{jj})
                        % normalize so it becomes the probability of lick
                        h1{jj}=h1{jj}/sum(h1{jj}).*HR(jj);
                        if PLOT_CUMLICK, h1{jj}=cumsum(h1{jj}); end
                        h1{jj}=stairs(0:BinSize:MaxBinTime,h1{jj},'color',colormtx(jj,:),'linewidth',2);
                        hold on;
                    end
                    uN(jj)=length(thistargetii)-thisFAcount-thisETcount;
                    uNN(j)=uN(j);%LAS FIX ME!
                case 'di_nolick'
                    h1{jj}=stairs(di_details.tsteps,di_details.uDI_hits(jj,:),'color',colormtx(jj,:),'linewidth',2);
                    hold on;
                    uN(jj)=di_details.uHit(jj)+di_details.uMiss(jj);
                    uNN(jj)=di_details.uHit(jj)+di_details.uMiss(jj)+di_details.uFA(jj)+di_details.uET(jj);
            end
            if has_tar_str
                tarstr=tarstr_{jj};
                endstr='';
            else
                endstr='';
                if(length(tar_freqs)>1 && all(diff(tar_freqs)==1))
                    if(jj==1)
                        endstr=sprintf(' %g kHz',str2double(exptparams_.UniqueTargets{jj})/1000);
                    end
                    tarstr=sprintf('%g dB',tar_levels(jj));
                else
                    if(jj==1)
                        endstr=sprintf(' %g dB',tar_levels(jj));
                    end
                    this_tar_str=str2double(exptparams_.UniqueTargets{jj});
                    if isnan(this_tar_str)
                        tarstr=strrep(exptparams_.UniqueTargets{jj},'_','\_');
                    else
                        if(jj==1)
                            endstr=sprintf(' %g dB',tar_levels(jj));
                        end
                        this_tar_str=str2double(exptparams_.UniqueTargets{jj});
                        if isnan(this_tar_str)
                            tarstr=strrep(exptparams_.UniqueTargets{jj},'_','\_');
                        else
                            tarstr=sprintf('%g kHz',str2double(exptparams_.UniqueTargets{jj})/1000);
                        end
                    end
                end
            end
            if strcmp(get(o,'Descriptor'),'RewardTargetLBHB') && ~isempty(di_details_newT)
                    LegendLabels{jj}=[sprintf('%s (HR:%.2f RT:%.2f DI:%.0f/%.0f n:%d)',...
                        tarstr,HR(jj),RT(jj),di_details.uDI(jj)*100,...
                        di_details_newT.uDI(jj)*100,...
                        uN(jj)),endstr];
            else
                %'ClassicalConditioning' or hsitograms for just one trial
                    LegendLabels{jj}=[sprintf('%s (HR:%.2f RT:%.2f DI:%.0f n:%d)',...
                        tarstr,HR(jj),RT(jj),di_details.uDI(jj)*100,...
                        uN(jj)),endstr];
            end
        end
        if length(exptparams_.Performance(TrialIndex+1).uFalseAlarmRate)==1
            LegendLabels{end+1}=sprintf('Ref (FAR:%.2f)',FAR);
            uNN(end+1)=1;
        else
            ReferenceHandle=get(exptparams_.TrialObject,'ReferenceHandle');
            tar_suffixes=get_target_suffixes(ReferenceHandle);
%             try
%                 UnTargetDistSet=unique(get(ReferenceHandle,'TargetDistSet'));
%                 if length(UnTargetDistSet)>length(tar_suffixes)
%                     tar_suffixes = cellfun(@(x)['Tardist',num2str(x)],num2cell(UnTargetDistSet),'Uni',0);
%                 end
%             end
            for i=1:length(tar_suffixes)
                LegendLabels{end+1}=sprintf('Ref %s (FAR:%.2f)',...
                    strrep(tar_suffixes{i},'_','\_'),...
                    exptparams_.Performance(TrialIndex+1).uFalseAlarmRate(i)/100);
                uNN(end+1)=1;
            end
        end
    else
        jj=1;
        switch cum_rt_source
            case 'BehaviorDisplay'
                h1{jj}=hist(exptparams_.FirstLick.Tar(1:TrialIndex),0:BinSize:MaxBinTime);
                HitOrMissCount=sum(cat(1,exptparams_.Performance(1:TrialIndex).Hit) | cat(1,exptparams_.Performance(1:TrialIndex).Miss));
                uNN=[HitOrMissCount 1];% The 1 is a dummy for the FA just to make it always be in the legend
                if ~isempty(h1{jj})
                    h1{jj}=h1{jj}/HitOrMissCount; % normalize to make h probability of lick
                    if PLOT_CUMLICK, h1{jj}=cumsum(h1{jj}); end
                    h1{jj}=stairs(0:BinSize:MaxBinTime,h1{jj},'color',[1 .5 .5],'linewidth',2);
                    hold on;
                end
            case 'di_nolick'
                h1{jj}=stairs(di_details.tsteps,di_details.uDI_hits(jj,:),'color',[1 .5 .5],'linewidth',2);
                hold on;
                uN=[di_details.uHit(jj)+di_details.uMiss(jj) 1];
                uNN=[1 1]; % Make both Hit and FA always be in the legend
        end
        
      
        LegendLabels={'Tar','Ref'};
    end
    %     ct=cat(1,exptparams_.Performance(1:TrialIndex).FirstCatchTime);
    %     fct=find(~isnan(ct) & ct<cat(1,exptparams_.Performance(1:TrialIndex).FirstLickTime));
    %     if ~isempty(fct),
    %         h1=hist(exptparams_.FirstLick.Catch(fct),0:BinSize:MaxBinTime);
    %
    %         if ~isempty(h1)
    %             h1=h1/length(fct); % normalize to convert to probability
    %             if PLOT_CUMLICK, h1=cumsum(h1); end
    %             stairs(0:BinSize:MaxBinTime,h1,'color',[.5 .5 .5],'linewidth',2);
    %             RT=nanmean(exptparams_.FirstLick.Catch(fct));
    %             HR=sum(~isnan(exptparams_.FirstLick.Catch(fct)))/length(fct);
    %             LegendLabels{end+1}=LegendLabels{end};
    %             LegendLabels{end-1}=sprintf('Catch(HR:%.2f RT:%.2f DI:%.0f n:%d)',...
    %                 HR,RT,exptparams_.Performance(end).cDiscriminationIndex,length(fct));
    %         end
    %     end
    %inter-reference interval
    switch cum_rt_source
        case 'BehaviorDisplay'
            switch cum_rt_FA_histogram_style
                case 're_ref'
                    PreStimSilence=get(get(exptparams_.TrialObject,'ReferenceHandle'),'PreStimSilence');
                    PostStimSilence=get(get(exptparams_.TrialObject,'ReferenceHandle'),'PostStimSilence');
                    Duration=get(get(exptparams_.TrialObject,'ReferenceHandle'),'Duration');
                    IRI=PreStimSilence+PostStimSilence+Duration;
                    times=exptparams_.FirstLick.Ref(1:TrialIndex)-PreStimSilence;
                    lick_times_re_refs=mod(times,IRI);
                    lick_times_re_refs(times<0)=times(times<0);
                    MaxBinTime_Ref=IRI;
                    Ref_bins=-PreStimSilence:BinSize:MaxBinTime_Ref;
                    if exptparams_.Performance(1).PunishFA
                        ETorFA=cat(1,exptparams_.Performance(1:TrialIndex).uEarlyTrial) +...
                            cat(1,exptparams_.Performance(1:TrialIndex).uFalseAlarm);
                    else
                        ETorFA=ones(TrialIndex,1);
                    end
                    FAhist=nan(size(ETorFA,2),length(Ref_bins));
                    for i=1:size(ETorFA,2)
                        FAhist(i,:)=hist(lick_times_re_refs(ETorFA(:,i)==1),Ref_bins);
                    end
                case 're_trial'
                    PreStimSilence=get(get(exptparams_.TrialObject,'ReferenceHandle'),'PreStimSilence');
                    times=exptparams_.FirstLick.Ref(1:TrialIndex)-PreStimSilence;
                    MaxBinTime_Ref=max(ceil(max([exptparams_.Performance(1:end-1).TarResponseWinStart])),MaxBinTime);
                    Ref_bins=0:BinSize:MaxBinTime_Ref;
                    ETorFA=cat(1,exptparams_.Performance(1:TrialIndex).uEarlyTrial) +...
                        cat(1,exptparams_.Performance(1:TrialIndex).uFalseAlarm);
                    FAhist=nan(size(ETorFA,2),length(Ref_bins));
                    for i=1:size(ETorFA,2)
                        FAhist(i,:)=hist(times(ETorFA(:,i)==1),Ref_bins);
                    end
                otherwise
                    error('cum_rt_FA_histogram_style must be ''re_ref'' or ''re_trial''')
            end
            if ~isempty(FAhist),
                switch cum_rt_FA_histogram_style
                    case {'re_ref','re_trial'}
                        %norm=sum([exptparams_.Performance(1:TrialIndex).FalseAlarm]);
                        %norm=length(exptparams_.FirstLick.Ref);
                        N_per_ref=sum(~isnan(ETorFA),1)';
                        N_per_ref(N_per_ref==0)=nan;%to avoid division by 0
                        norm=repmat(N_per_ref,1,length(Ref_bins));
                    case 're_trial'
                        norm=length(exptparams_.FirstLick.Ref);
                end
                FAhist=FAhist./norm; % normalize to convert to probability
                if PLOT_CUMLICK, FAhist=cumsum(FAhist,2); end
                FA_hist_colors=[.1 .5 .1; .3 .7 .3; .2 .7 .2]; %If you have more than 3 reference types you're crazy!
                for i=1:size(FAhist,1)
                    h1{end+1}=stairs(Ref_bins,FAhist(i,:),'color',FA_hist_colors(i,:),'linewidth',2);
                end
            end
            hist_source_str=' (from BehaviorDisplay)';
        case 'di_nolick'
            UC=UniqueCount;
            if length(exptparams_.Performance(TrialIndex+1).uFalseAlarmRate)==1
                UC=1;
            end
            for jj=1:UC
                if UC==1
                    col=[.1 .5 .1];
                else
                    col=colormtx(jj,:);
                end
                h1{end+1}=stairs(di_details.tsteps,di_details.uDI_fas(jj,:),'color',col,'linewidth',1.5,'LineStyle','--');
            end
            hist_source_str=' (from di\_nolick)';
    end
    yl=[0 1.01];
    set(gca,'YLim',yl);
    if exist('training_pump_time','var')
        pump_time_re_target = training_pump_time - w.TarTimes(1);
        tp_ph=patch(pump_time_re_target([1 2 2 1]),yl([1 1 2 2]),[0 .5 .5],'EdgeColor','none','FaceAlpha',.3);
        uistack(tp_ph,'bottom');
        tp_str='Training Pump';
    else
        tp_ph=[];
        tp_str=[];
    end
    li=uNN>0;
    if(any(li))
        h=legend([h1{li},tp_ph],[LegendLabels(li),tp_str],'Location','SE');
    end
    %LegPos = get(h,'position');
    set(h,'fontsize',8);
    %xlim=get(gca,'Xlim');
    %ylim=get(gca,'Ylim');
    if(length(LegendLabels)>4)
        set(h,'FontSize',7)
    end
    if length(LegendLabels)>2 && any(li)
        %LegPos(1:2) = [0.005 ylim(2)]; % put the legend on the far left of the screen
        %LegPos(1:2) = [0.4 0.425]; % put the legend on the far left of the screen
        LegPos = [0.72    0.475 1e-10 1e-10];% put the legend above the behavior performance table
        set(h,'position', LegPos);
    end
    if ~strcmp(cum_rt_source,'di_nolick')
        switch cum_rt_FA_histogram_style
            case 're_ref'
                stop_respwin=get(exptparams_.BehaveObject,'EarlyWindow')+...
                    get(exptparams_.BehaveObject,'ResponseWindow')+1;
                % set(gca,'XLim',[0 max([IRI,exptparams_.BehaveObject.ResponseWindow])]) % exptparams_.BehaveObject.ResponseWindow+exptparams_.BehaveObject.EarlyWindow??
                set(gca,'XLim',[0 min(stop_respwin,IRI)]) % very few licks in the end of the response window
                set(gca,'XLim',[0 max(IRI,MaxBinTime)])
        end
    end

            
    if PLOT_CUMLICK,
        title(['Cumulative RT Histogram',cuestr,hist_source_str]);
    else
        title('RT Histogram');
    end
    xlabel('Time (seconds)');
end
end