function LickData = PreprocessLickData(o,LickData)

% if there is paw signal, take it out:
if size(LickData,2)>1
    LickData = LickData(:,1);
end
% Digitize LickData:
LickData(LickData<0.5)=0;
LickData(LickData>=0.5)=1;

% HACK: if started licking before trial started, change samples to make
% sure we catch it. Also fixes bar press bug where all(LickData == 1)
% becuase marmo releases bar before trial starts recording.
if LickData(1) == 1
    LickData(1:2)=[0 1];
end

%Change LickData to Lick Onsets
LickData = max(0,diff(LickData));
end