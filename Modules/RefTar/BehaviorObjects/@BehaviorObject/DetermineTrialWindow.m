function [StimPos,EarlyPos,Ref,CorrectRejectThisTrial,CurrentTarIndex] = DetermineTrialWindow(o,CurrentTime,w)

CurrentTarIndex=NaN;
EarlyPos=NaN;
CorrectRejectThisTrial=0;

Ntar=size(w.TarResponseWin,1); % For purposes of online feedback to the animal, the number of targets doesn't include catches.
if w.NullTrial && CurrentTime>=w.RefResponseWinMerged(end)
    % reference period has ended, correct rejection
    fprintf('Current time: %.3f\nEnd of RefResponseWin: %.3f\n',...
        CurrentTime, w.RefResponseWinMerged(end));
    StimPos = length(find(w.RefResponseWinMerged<CurrentTime));
    Ref = 0;
    CorrectRejectThisTrial=1;
elseif ~isempty(w.RefResponseWinMerged) && Ntar<2 && CurrentTime<w.RefResponseWinMerged(end)
    % stim pos tells us whether we
    % are "IN" the windows calculated above or outside of it
    StimPos = length(find(w.RefResponseWinMerged<CurrentTime));
    Ref = 1;  % we are in reference part of the sound
elseif Ntar<2 % if we are in target part
    StimPos = length(find(w.TarResponseWin<CurrentTime));
    EarlyPos = length(find(w.TarEarlyWin<CurrentTime));
    Ref=0;
elseif Ntar>1
    
    if CurrentTime<w.RefEarlyWin(2)
        Ref=[1 0]; %Response during Reference early window
    else
        Ref = CurrentTime<w.RefResponseWinMerged(:,2) & CurrentTime>w.RefResponseWinMerged(:,1);
    end
    if sum(Ref)==1
        RefPeriod=find(Ref);
        Ref=1;
        StimPos = length(find(w.RefResponseWinMerged(RefPeriod,:)<CurrentTime));
        EarlyPos=NaN;
    else
        Tar = CurrentTime<w.TarResponseWin(:,2) & CurrentTime>w.TarResponseWin(:,1);
        TarEW = CurrentTime<w.TarEarlyWin(:,2) & CurrentTime>w.TarEarlyWin(:,1);
        if (sum(TarEW)==1 && sum(Tar)==0) || (sum(TarEW)==0 && sum(Tar)==1)
            %Current time is in a single tar or tarEW time
            Ref=0;
            CurrentTarIndex=min([find(Tar),find(TarEW)]);
            StimPos = length(find(w.TarResponseWin(CurrentTarIndex,:)<CurrentTime));
            EarlyPos = length(find(w.TarEarlyWin(CurrentTarIndex,:)<CurrentTime));
        elseif sum(TarEW)==1 && sum(Tar)==1
            %Current time is in a single tar and a single tarEW time
            %If they respond in this time window, count it as a hit.
            %Ends up being the same code as above, but repeated here for clarity.
            Ref=0;
            FirstTarStart=min(w.TarResponseWin(Tar,1),w.TarResponseWin(TarEW,1));
            CurrentTarIndex=find(w.TarResponseWin(:,1)==FirstTarStart);
            StimPos = length(find(w.TarResponseWin(CurrentTarIndex,:)<CurrentTime));
            EarlyPos = length(find(w.TarEarlyWin(CurrentTarIndex,:)<CurrentTime));
        elseif sum(TarEW)==0 && sum(Tar)>1
            %Current time is in multiple tar and no tarEW times
            %Count this as a hit for the first target.
            %Ends up being the same code as above, but repeated here for clarity.
            [~,min_tar_ind]=min(w.TarResponseWin(Tar,1));
            Tar(:)=false; Tar(min_tar_ind)=true;
            Ref=0;
            CurrentTarIndex=find(Tar);
            StimPos = length(find(w.TarResponseWin(CurrentTarIndex,:)<CurrentTime));
            EarlyPos = length(find(w.TarEarlyWin(CurrentTarIndex,:)<CurrentTime));
        elseif sum(TarEW)==0 && sum(Tar)==0
            %Miss
            Ref=0;
            CurrentTarIndex=Ntar;
            StimPos = length(find(w.TarResponseWin(CurrentTarIndex,:)<CurrentTime));
            EarlyPos = length(find(w.TarEarlyWin(CurrentTarIndex,:)<CurrentTime));
        else
            warning('CurrentTime matches multiple times!')
        end
    end
end