function exptparams = ComputePerformance_from_Windows_and_Licks(o,exptparams,par,w,FAStartTime,LickData,UnalteredLickData,fs,tnum)
% exptparams = ComputePerformance_from_Windows_and_Licks(o,exptparams,par,w,FAStartTime,LickData,UnalteredLickData,fs,tnum)
% Shared by ClassicalConditioning and RewardTargetLBHB
% Does 3 main things:
%   Analyzes lick vector to find out when animal licked with respect to target
%   Calculates HR, FAR, etc based on this
%   Calls di_nolick to calculate DI


%% analyze lick vector to find out when animal licked with respect to target
% window and false alarm window -- defined as the periods of time before
% the target where the target might have occurred.
% "strict" - FA is response to any possible target slot preceeding the target
TarResponseBin=min(length(LickData),round(fs*w.TarOrCatchResponseWin));
TarEarlyBin=min(length(LickData),round(fs*w.TarOrCatchEarlyWin));
if 1
    %New RewardTargetLBHB Way
    RefFalseAlarm = zeros(size(w.RefResponseWin,1),1);RefFirstLick = nan(size(w.RefResponseWin,1),1);
    for rwi = 1:size(w.RefResponseWin,1)
        if fs*w.RefEarlyWin(rwi,1) > max(TarResponseBin(:,1))
            break
        end
        RefResponseLicks = UnalteredLickData(max(1,round(fs*w.RefResponseWin(rwi,1))):min(length(UnalteredLickData),round(fs*w.RefResponseWin(rwi,2))));
        RefEarlyLicks = UnalteredLickData(max(1,round(fs*w.RefEarlyWin(rwi,1))):min(length(UnalteredLickData),round(fs*w.RefEarlyWin(rwi,2))));
        temp = find([RefEarlyLicks; RefResponseLicks],1)/fs; %Find the first lick in either the RefEarly or RefResponse windows.
        if ~isempty(temp), RefFirstLick(rwi) = temp; else RefFirstLick(rwi) = nan;end
        RefFalseAlarm(rwi) = double(~isempty(temp));
    end
    if ~any(UnalteredLickData(1:max(TarResponseBin(:,1))))
        RefFalseAlarm(:)=0; %LAS added this because in situations where a reference window overlaps with a target (two-stream sounds), if the animal licks after the target but still in the reference it should be called a miss, not a FA.
    end
else
    %Oldest way
    RefFalseAlarm = 0;RefFirstLick = NaN;
    for cnt2 = 1:w.NumRef
        cnt1 = (cnt2-1)*2+1;
        RefResponseLicks{cnt2} = UnalteredLickData(max(1,round(fs*w.RefResponseWin(cnt1))):min(length(UnalteredLickData),round(fs*w.RefResponseWin(cnt1+1))));
        RefEarlyLicks{cnt2} = UnalteredLickData(max(1,round(fs*w.RefEarlyWin(cnt1))):min(length(UnalteredLickData),round(fs*w.RefEarlyWin(cnt1+1))));
        temp = find([RefEarlyLicks{cnt2}; RefResponseLicks{cnt2}],1)/fs;
        if ~isempty(temp), RefFirstLick(cnt2) = temp; else RefFirstLick(cnt2) = nan;end
        RefFalseAlarm(cnt2) = double(~isempty([find(RefEarlyLicks{cnt2});find(RefResponseLicks{cnt2})]));
    end
    
    %ClassicalConditioning Way
    RefResponseLicks=cell(w.NumRef,1);
    RefEarlyLicks=cell(w.NumRef,1);
    RefFalseAlarm=zeros(w.NumRef,1);
    RefFalseAlarm = 0;RefFirstLick = NaN;
    for cnt1 = 1:w.NumRef
        RefResponseLicks{cnt1} = LickData(max(1,round(fs*w.RefResponseWin(cnt1,1))):min(length(LickData),round(fs*w.RefResponseWin(cnt1,2))));
        RefEarlyLicks{cnt1} = LickData(max(1,round(fs*w.RefEarlyWin(cnt1,1))):min(length(LickData),round(fs*w.RefEarlyWin(cnt1,2))));
        temp = find([RefEarlyLicks{cnt1}; RefResponseLicks{cnt1}],1)/fs;
        if ~isempty(temp)
            RefFirstLick(cnt1) = temp;
        else
            RefFirstLick(cnt1) = nan;
        end
        RefFalseAlarm(cnt1) = double(~isempty([find(RefEarlyLicks{cnt1});find(RefResponseLicks{cnt1})]));
    end
end

% Archaic
%FalseAlarm = sum(RefFalseAlarm)/w.NumRef;

FAStartBin=min(round(fs*FAStartTime+1),length(LickData));
EarlyTrial=double(sum(LickData(1:FAStartBin))>0); % SVD maybe this has been superceded by code below?
if ~any(RefFalseAlarm) && (w.NtarOrcatch>0)
    EarlyTrial = double(sum(LickData(1:min(TarEarlyBin(:,1))))>0);
end

LicksRequired = getparm(par, 'LicksRequired',1);
if length(LicksRequired)>1
    LicksRequired = par.LicksRequired(tnum);
end

if w.NullTrial
    FalseAlarm=~EarlyTrial & sum(LickData(FAStartBin:min(length(LickData),round(fs*w.RefResponseWin(end)))))>0;
    RefFalseAlarm=FalseAlarm;    
    TarResponseLick=0;
    TarEarlyLick=0;
    TarFirstLick=nan;
else
    % svd modified 2015-01-23 -- FA only if in window when target could
    % have occurred.  Responses before that time are not "real" FAs
    TarHit = zeros(w.NtarOrcatch,1);
    TarEW = zeros(w.NtarOrcatch,1);
    for rwi = 1:w.NtarOrcatch
        TarResponseLick = LickData((TarResponseBin(rwi,1)+1):TarResponseBin(rwi,2));
        TarEarlyLick = LickData((TarEarlyBin(rwi,1)+1):TarEarlyBin(rwi,2));
        TarEW(rwi) = any(TarEarlyLick);
        TarHit(rwi) = sum(TarResponseLick)>=LicksRequired;
    end
    Hit = any(TarHit);
    
    if Hit
        HitInd=find(TarHit);
        if length(HitInd) > 1
            catches = ismember(w.TarOrCatchTimes,w.CatchTimes);
            HitInd(catches)=0;
            if sum(HitInd>0)>1
                HitInd_=find(HitInd);
                [~,min_tar_ind]=min(w.TarOrCatchResponseWin(HitInd_,1));
                HitInd=HitInd_(min_tar_ind);
                warning('Hit multiple targets, assigning hit to first');
            else
                HitInd=find(HitInd);
            end
            
        end
        
        %Find lick time re target onset, assuming equal EW for all targets
        TarFirstLick= find([zeros(size(TarEarlyLick)) ;
            LickData((TarResponseBin(HitInd,1)+1):TarResponseBin(HitInd,2))],1)/fs;
        RefFirstLick=nan;
        FalseAlarm=false;
    elseif ~EarlyTrial && (any(RefFalseAlarm) || any(TarEW))
        %FA to reference or during EW
        HitInd=[];
        RefFirstLick=(min(find(UnalteredLickData(FAStartBin:max(TarResponseBin(:,1)))>0))+FAStartBin-1)./fs;
        TarFirstLick=nan;
        FalseAlarm=true;
        if any(TarEW)
            EWInd=find(TarEW);
            TarFirstLick = find(LickData((TarEarlyBin(EWInd,1)+1):TarEarlyBin(EWInd,2)),1)/fs;
        end
    else
        %Miss
        HitInd=[];
        TarFirstLick=NaN;
        RefFirstLick=NaN;
        FalseAlarm=false;
    end
end
if ~isempty(w.CatchTimes)
    CatchResponseLick = LickData(max(1,round(fs*w.CatchResponseWin(1))):min(length(LickData),round(fs*w.CatchResponseWin(2))));
    CatchEarlyLick = LickData(round(fs*max(1,w.CatchEarlyWin(1))):round(min(length(LickData),fs*w.CatchEarlyWin(2))));
    CatchFirstLick = find([zeros(size(CatchEarlyLick)) ;CatchResponseLick],1)/fs;
    if isempty(CatchFirstLick)
        CatchFirstLick = nan;
    end
else
    CatchFirstLick=nan;
end

% a special performance is calculated here for the graph that shows the hit
% and false alram based on the position of the target/reference:
% record the first lick time for ref and tar
if ~isfield(exptparams,'FirstLick')
   % its the first time:
   exptparams.FirstLick=struct();
end

exptparams.FirstLick.Tar(tnum) = TarFirstLick;
exptparams.FirstLick.Ref(tnum) = min(RefFirstLick);
exptparams.FirstLick.Catch(tnum) = CatchFirstLick;

% now calculate the performance:
if tnum>1
    perf = exptparams.Performance(1:end-1);
end
perf(tnum).ThisTrial    = '??';

if TarEW
    % response during early window counts as a false alarm
    perf(tnum).FalseAlarm = 1;
elseif w.NumRef
    perf(tnum).FalseAlarm = double(FalseAlarm); %Per-trail
else
    perf(tnum).FalseAlarm = NaN;
end

Miss=~w.NullTrial & ~EarlyTrial & ~perf(tnum).FalseAlarm & ~Hit;

perf(tnum).EarlyTrial = EarlyTrial; % lick before target could have occurred.
perf(tnum).Hit          = double(Hit);
perf(tnum).Miss         = double(Miss);
perf(tnum).CorrectReject= double(w.NullTrial & ~EarlyTrial & ~perf(tnum).FalseAlarm & ~perf(tnum).Hit);
perf(tnum).NullTrial = w.NullTrial;
perf(tnum).PunishFA =  double(strcmpi(getparm(par, 'PunishFA', 'Yes'), 'Yes'));

NullTrials = cat(1,perf.NullTrial);
HitOrMissTrials=sum(cat(1,perf.Hit) | cat(1,perf.Miss));
perf(tnum).HitRate          = sum(cat(1,perf.Hit)) / HitOrMissTrials;
perf(tnum).MissRate         = sum(cat(1,perf.Miss)) / HitOrMissTrials;

INCLUDE_EARLY_TRIALS_IN_FA=1;
if INCLUDE_EARLY_TRIALS_IN_FA
    % this is for trials without Reference. We dont count them in FA calculation:
    validFATrials = find(~isnan(cat(1,perf.FalseAlarm)));
else
    validFATrials = find(~isnan(cat(1,perf.FalseAlarm)) & ~cat(1,perf.EarlyTrial));
end
TotalValidFA = length(validFATrials);
perf(tnum).FalseAlarmRate = sum(cat(1,perf(validFATrials).EarlyTrial) | cat(1,perf(validFATrials).FalseAlarm))/TotalValidFA;

perf(tnum).CorrectRejectRate = sum(cat(1,perf.CorrectReject))./sum(NullTrials);
perf(tnum).DiscriminationRate = perf(tnum).HitRate * (1-perf(tnum).FalseAlarmRate);

if w.Check_both_FA_ET && perf(tnum).FalseAlarm && perf(tnum).EarlyTrial
    disp('both FA and Early?');
    keyboard
end
if length(perf(tnum).Hit)>1
    disp('Multibin hit???');
    keyboard
end

perf(tnum).ReferenceLickTrial = double((perf(tnum).FalseAlarm>0));
perf(tnum).LickRate = length(find(LickData)) / length(LickData);

perf(tnum).FirstLickTime = min([find(LickData,1)./fs Inf]);
perf(tnum).LickTimes = find(LickData)./fs;
if isempty(perf(tnum).LickTimes)
    perf(tnum).LickTimes = Inf;
end
fprintf(['LickTimes: ',num2str(perf(tnum).LickTimes') '\n'])
perf(tnum).FirstRefTime = w.FirstRefTime;
perf(tnum).CatchTimes = w.CatchTimes;
perf(tnum).TarTimes = w.TarOrCatchTimes;
if isempty(w.TarTimes)
    perf(tnum).FirstTarTime = NaN;
else
    perf(tnum).FirstTarTime = min(w.TarTimes);
end
if isempty(w.CatchTimes)
    perf(tnum).FirstCatchTime = NaN;
else
    perf(tnum).FirstCatchTime = min(w.CatchTimes);
end
% Now calculate hit and miss rates:
%also, calculate the stuff for this trial block:
RecentIndex = max(1, tnum-exptparams.TrialBlock+1):tnum;
tt = cat(1,perf(RecentIndex).FalseAlarm);
tt=tt(~isnan(tt));
perf(tnum).RecentFalseAlarmRate   = sum(tt)/length(tt);
RecentHitOrMissCount=sum(cat(1,perf(RecentIndex).Hit) | cat(1,perf(RecentIndex).Miss));
perf(tnum).RecentIndexMean=mean(RecentIndex);
perf(tnum).RecentHitRate         = sum(cat(1,perf(RecentIndex).Hit) & ~NullTrials(RecentIndex))/RecentHitOrMissCount;
perf(tnum).RecentDiscriminationRate = perf(tnum).RecentHitRate * (1-perf(tnum).RecentFalseAlarmRate);

perf(tnum).TarResponseWinStart=min(w.TarOrCatchResponseWin(:,1));
if exist('w.CatchResponseWin','var') && ~isempty(w.CatchResponseWin)
    perf(tnum).CatResponseWinStart=w.CatchResponseWin(1);
else
    perf(tnum).CatResponseWinStart=nan;
end

if w.two_target
    perf(tnum).ThisTargetNote=w.AllTargetOrCatchNotes;
else
    perf(tnum).ThisTargetNote=w.ThisTargetNote;
end
perf(tnum).OptoLight=w.OptoLight;

%% target-specific HR / RT
ReferenceHandle=get(exptparams.TrialObject,'ReferenceHandle');
if ismethod(ReferenceHandle,'get_target_suffixes')
    tar_suffixes=get_target_suffixes(ReferenceHandle);
else
    tar_suffixes=cell(1);
end

if isfield(exptparams,'UniqueTargets') && length(exptparams.UniqueTargets)>1
    trialtargetid=zeros(tnum,1);
    trialref_type=zeros(tnum,1);
    if w.two_target
        trialtargetid_all=cell(tnum,1);
    end
    UniqueCount=length(exptparams.UniqueTargets);
    for tt=1:tnum
        if ~perf(tt).NullTrial
            if w.two_target
                trialtargetid_all{tt}=cellfun(@(x)find(strcmp(x,...
                    exptparams.UniqueTargets),1),perf(tt).ThisTargetNote);
                trialtargetid(tt)=trialtargetid_all{tt}(1);
            else
                trialtargetid(tt)=find(strcmp(perf(tt).ThisTargetNote,...
                    exptparams.UniqueTargets),1);
            end
            if ismethod(ReferenceHandle,'get_target_suffixes')
                trialref_type(tt)=find(cellfun(@(x)~isempty(strfind(exptparams.UniqueTargets{trialtargetid(tt)},x)),tar_suffixes));
            else
                trialref_type(tt)=1;
            end
        end
    end
    if ismethod(ReferenceHandle,'get_target_suffixes')
        reftype_by_tarid=nan(size(exptparams.UniqueTargets));
        for i=1:length(tar_suffixes)
            reftype_by_tarid(unique(trialtargetid(trialref_type==i)))=i;
        end
        %     if any(isnan(reftype_by_tarid))
        %         error('Unmatched reftypes')
        %     end
    else
        reftype_by_tarid=ones(size(exptparams.UniqueTargets));
    end
else
    UniqueCount=1;
    trialtargetid=double(~NullTrials);
    trialref_type=ones(tnum,1);
    reftype_by_tarid=1;
end

if 0 %turn this on to remove unique reference type code
    reftype_by_tarid(:)=1;
    trialref_type(:)=1;
    tar_suffixes=cell(1);
end
UniqueCount_reftype=length(tar_suffixes);

% save HR and RT data
perf(tnum).uHit=zeros(1,UniqueCount).*nan;
perf(tnum).ReactionTime=exptparams.FirstLick.Tar(tnum);
perf(tnum).uReactionTime=zeros(1,UniqueCount).*nan;

if w.NtarOrcatch==1  && trialtargetid(tnum)>0
    perf(tnum).uHit(trialtargetid(tnum))=perf(tnum).Hit;
    perf(tnum).uReactionTime(trialtargetid(tnum))=perf(tnum).ReactionTime;
elseif w.NtarOrcatch>1
    if perf(tnum).Hit
        perf(tnum).uHit(trialtargetid_all{tnum}(HitInd))=perf(tnum).Hit;
        perf(tnum).uReactionTime(trialtargetid_all{tnum}(HitInd))=perf(tnum).ReactionTime;
        Missed_Tars = TarResponseBin(:,1) < TarResponseBin(HitInd,1);
        %Mark targets whose response windows started before target that was hit as missed
    elseif perf(tnum).FalseAlarm || perf(tnum).EarlyTrial
        Missed_Tars = TarResponseBin(:,1) < RefFirstLick*fs;
        %Mark targets that whose response windows started before the false alarm as missed
    elseif perf(tnum).Miss
        Missed_Tars = true(w.NtarOrcatch,1);
    end
    perf(tnum).uHit(trialtargetid_all{tnum}(Missed_Tars))=0;
end
perf(tnum).uHitRate = nanmean(cat(1,perf.uHit),1);

%FA and ET rates by stimulus
perf(tnum).uFalseAlarm=zeros(1,UniqueCount_reftype).*nan;
perf(tnum).uFalseAlarm(trialref_type(tnum))=perf(tnum).FalseAlarm;
perf(tnum).uEarlyTrial=zeros(1,UniqueCount_reftype).*nan;
perf(tnum).uEarlyTrial(trialref_type(tnum))=perf(tnum).EarlyTrial;

ETorFA=cat(1,perf(validFATrials).uEarlyTrial) + cat(1,perf(validFATrials).uFalseAlarm);
perf(tnum).uFalseAlarmRate = nansum(ETorFA,1)./sum(~isnan(ETorFA),1);

IsCatchTrial=~isnan(cat(1,perf.FirstCatchTime));
CatchIdx=find(IsCatchTrial);
if isfield(perf(tnum),'FirstCatchTime') && ~isempty(CatchIdx)
    cEarly=cat(2,perf(CatchIdx).EarlyTrial);
    crt=exptparams.FirstLick.Catch(CatchIdx);
    cHit=crt>par.EarlyWindow & crt<par.EarlyWindow+par.ResponseWindow;
    rrt=exptparams.FirstLick.Ref(CatchIdx);
    cFA=rrt>FAStartTime & rrt<=cat(2,perf(CatchIdx).FirstCatchTime)+par.EarlyWindow;
    perf(tnum).cHitRate=mean(cHit(~cEarly & ~cFA));
    
    cTarTimes=min(cat(2,perf.FirstTarTime),max(cat(2,perf(CatchIdx).FirstCatchTime)));
    cEarly=cat(2,perf.EarlyTrial);
    cFAidx=find(~cEarly);
    rrt=exptparams.FirstLick.Ref(cFAidx);
    cFA=rrt>FAStartTime & rrt<=cTarTimes(cFAidx)+par.EarlyWindow;
    perf(tnum).cFARate=mean(cFA);
else
    % no catch stim yet (or at all)
    perf(tnum).cFARate=nan;
    perf(tnum).cHitRate=nan;
end

% save catch RT data
if isfield(perf(tnum),'FirstCatchTime') && ~isnan(perf(tnum).FirstCatchTime),
    perf(tnum).cReactionTime=exptparams.FirstLick.Catch(tnum);
else
    perf(tnum).cReactionTime=nan;
end


% now determine what this trial outcome is:
if perf(tnum).Hit, perf(tnum).ThisTrial = 'Hit'; end
if perf(tnum).Miss && ~perf(tnum).NullTrial, perf(tnum).ThisTrial = 'Miss'; end
if perf(tnum).CorrectReject, perf(tnum).ThisTrial = 'Corr.Rej.'; end
if perf(tnum).EarlyTrial, perf(tnum).ThisTrial = 'Early'; end
if perf(tnum).FalseAlarm, perf(tnum).ThisTrial = 'FA'; end
%if perf(tnum).Ineffective, perf(tnum).ThisTrial = 'Ineffective';end

%% compute DI based on FAR, HR and RT, using di_nolick
% LAS: Previously, the guts of di_nolick were here and di_nolick wasn't
% called. Go back to commit 08c0ec2 to see the first commit where I
% made this change, and commit edc708d to see code before I finally removed
% the old way (that wasn't being used).

dat.exptparams=exptparams;
dat.exptparams.Performance=perf;
dat.exptparams.Performance(end+1).ThisTrial='Dummy';
%dat.globalparams=globalparams;
force_use_original_target_window=1;
%persistent metrics_ metrics_newT
compute_these_metrics={'new trials'};
if all([exptparams.TrialParams(1:tnum).CueSeg])
    compute_these_metrics{end+1}='cue trials only';
end

if 1
    compute_these_metrics{end+1} = 'user-defined trials';
    compute_these_metrics{end+1} = tnum; 
end
%LAS change 9-23-20: di_nolick use to analyze responses until 1 sec after the response window ended by default
% This change makes it uses a shorter widnows if all the responses have been less than 1 sec after the response window.
% This gives more accurate di if you have a really long target but the animal is responding quickly.
if size(w.TarResponseWin,1)==1
    RespWindow=diff(w.TarResponseWin);
    stop_respwin=max(1,max([dat.exptparams.Performance(1:end-1).FirstLickTime])+.1); %make window 1 sec or the longest response time, whichever is greater.
    stop_respwin_offset=min(-RespWindow+stop_respwin,1); %Limit at 1 sec re response window
end
%stop_respwin_offset=1; LAS: use this line to get back to the way it was before
[metrics,metrics_newT]=di_nolick(dat,force_use_original_target_window,[],stop_respwin_offset,compute_these_metrics);
perf(tnum).DiscriminationIndex=metrics.DI;
perf(tnum).DiscriminationIndex_NewT=metrics_newT.DI;
perf(tnum).uDiscriminationIndex=metrics.details.uDI;
perf(tnum).uDiscriminationIndex_NewT=metrics_newT.details.uDI;
perf(tnum).cDiscriminationIndex=NaN;
perf(tnum).perfectDI=NaN;
if all([exptparams.TrialParams(1:tnum).CueSeg])
    exptparams.temporary.metrics_CueTrials=metrics.details.metrics_CueTrials; %separate out so it doesn't get overwritten once we get into non-tue trials
end
exptparams.temporary.metrics=metrics;
exptparams.temporary.metrics_newT=metrics_newT;

%% Change all rates to percentage. 
%If its not rate, put the sum and 'out of' at the end
PerfFields = fieldnames(perf);
for cnt1 = 1:length(PerfFields)
    if ~iscell(perf(tnum).(PerfFields{cnt1})) && all(isinf(perf(tnum).(PerfFields{cnt1})))
        perf(tnum).(PerfFields{cnt1}) = 0;
    end
    if ~isempty(strfind(PerfFields{cnt1},'ReactionTime')) && ...
            ~isempty(strfind(PerfFields{cnt1},'uHit'))&& ...
            isnan(perf(tnum).(PerfFields{cnt1}))
        perf(tnum).(PerfFields{cnt1}) = 0;
    end
    if strcmpi(PerfFields{cnt1},'uHit') || strcmpi(PerfFields{cnt1},'uWarningTrial') ...
            || strcmpi(PerfFields{cnt1},'uFalseAlarm') || strcmpi(PerfFields{cnt1},'uEarlyTrial')
        perfPer.(PerfFields{cnt1}) = nansum(cat(1,perf.(PerfFields{cnt1})),1);
    elseif ~isempty(strfind(PerfFields{cnt1},'ReactionTime')) % RT, just average:
        perfPer.(PerfFields{cnt1}) = nanmean(cat(1,perf.(PerfFields{cnt1})),1);
    elseif ~isempty(strfind(PerfFields{cnt1},'Rate')) % if its a rate, do
        %not divide by number of trials, just make it percentage:
        perfPer.(PerfFields{cnt1}) = round(perf(tnum).(PerfFields{cnt1})*100);
    elseif ~isempty(strfind(PerfFields{cnt1},'Index')) % if its an index
        %not divide by number of trials, just make it percentage:
        perfPer.(PerfFields{cnt1}) = round(perf(tnum).(PerfFields{cnt1})*100);
    elseif strcmp(PerfFields{cnt1},'TarTimes')
        perfPer.(PerfFields{cnt1})=NaN;
    else
        if isnumeric(perf(tnum).(PerfFields{cnt1})) && ~isempty(perf(tnum).(PerfFields{cnt1}))
            perfPer.(PerfFields{cnt1})(1) = sum(cat(1,perf.(PerfFields{cnt1})));
            perfPer.(PerfFields{cnt1})(2) = HitOrMissTrials; % by default
        else
            perfPer.(PerfFields{cnt1}) = perf(tnum).(PerfFields{cnt1});
        end
    end
end

perfPer.EarlyTrial(2) = tnum;
perfPer.ReferenceLickTrial = tnum;
perfPer.FalseAlarm(2) = tnum;
perfPer.NullTrial(2) = tnum;
perfPer.CorrectReject(2) = sum(NullTrials);

%
% now position hit and false alarm:
if tnum==1 && isfield(exptparams,'PositionHit')
    exptparams.PositionHit(:)=0;
    exptparams.PositionFalseAlarm(:)=0;
end
if isfield(exptparams,'UniqueTarResponseWinStart')
    if ~w.NullTrial
        if w.two_target && w.Ntar>1
            if isempty(HitInd)
                PositionThisTrial=find(exptparams.UniqueTarResponseWinStart==w.TarOrCatchResponseWin(1,1));
            else
                PositionThisTrial=find(exptparams.UniqueTarResponseWinStart==w.TarOrCatchResponseWin(HitInd,1));
            end
        else
            PositionThisTrial=find(exptparams.UniqueTarResponseWinStart==w.TarOrCatchResponseWin(1));
        end
        if ~isfield(exptparams,'PositionHit')  || (size(exptparams.PositionHit,1)<PositionThisTrial)
            exptparams.PositionHit(PositionThisTrial,1:2) = 0;
        end
        exptparams.PositionHit(PositionThisTrial,1) = exptparams.PositionHit(PositionThisTrial,1) + perf(tnum).Hit;
        exptparams.PositionHit(PositionThisTrial,2) = exptparams.PositionHit(PositionThisTrial,2) + 1;
        if ~isempty(PositionThisTrial) && (PositionThisTrial>0) && ((~isfield(exptparams,'PositionFalseAlarm')) ...
                || (size(exptparams.PositionFalseAlarm,1)<PositionThisTrial))
            exptparams.PositionFalseAlarm(PositionThisTrial,1:2) = 0;
        end
        for cnt1 = 1:PositionThisTrial
            exptparams.PositionFalseAlarm (cnt1,1) = exptparams.PositionFalseAlarm(cnt1,1) + RefFalseAlarm(1);
            exptparams.PositionFalseAlarm (cnt1,2) = exptparams.PositionFalseAlarm(cnt1,2) + 1;
        end
    end
else
    if ~isfield(exptparams,'PositionHit')  || (size(exptparams.PositionHit,1)<w.NumRef+1)
        exptparams.PositionHit(w.NumRef+1,1:2) = 0;
    end
    if (w.NumRef>0) && ((~isfield(exptparams,'PositionFalseAlarm')) ...
            || (size(exptparams.PositionFalseAlarm,1)<w.NumRef))
        exptparams.PositionFalseAlarm(w.NumRef,1:2) = 0;
    end
    exptparams.PositionHit(w.NumRef+1,1) = exptparams.PositionHit(w.NumRef+1,1) + perf(tnum).Hit;
    exptparams.PositionHit(w.NumRef+1,2) = exptparams.PositionHit(w.NumRef+1,2) + 1;
    for cnt1 = 1:w.NumRef
        exptparams.PositionFalseAlarm (cnt1,1) = exptparams.PositionFalseAlarm(cnt1,1) + RefFalseAlarm(cnt1);
        exptparams.PositionFalseAlarm (cnt1,2) = exptparams.PositionFalseAlarm(cnt1,2) + 1;
    end
end
% perf(tnum)
exptparams.Performance(tnum) = perf(tnum);
exptparams.Performance(tnum+1) = perfPer;