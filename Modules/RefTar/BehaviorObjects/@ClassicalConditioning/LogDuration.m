function Duration = LogDuration (o, HW, StimEvents, globalparams, exptparams, TrialIndex);
% LogDuration method for ReferenceAvoidance object
% the log duration is the end of the sound plus response time
StopTimes=cat(1,StimEvents.StopTime);
Duration=max(StopTimes)+1; % + get(o,'ResponseWindow');