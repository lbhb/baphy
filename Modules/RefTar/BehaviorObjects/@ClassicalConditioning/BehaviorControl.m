function [LickEvents, exptparams] = BehaviorControl(o, HW, StimEvents, ...
    globalparams, exptparams, TrialIndex)
global TCPIP_Lick
% Behavior Control function for Classical Conditioning object
%
% SVD, June 2017, LBHB
% algorithm:
%   Drastically simplified from RewardTargetLBHB
%   Play sound, record licks.
%   Present water automatically at some point relative to target
OA=false;
try
    OA=exptparams.OfflineAnalysis;
end
par=get(o);
if ~OA
    CueSeg=get(exptparams.TrialObject,'CueSeg');
    if CueSeg(exptparams.InRepTrials)==1
        if ~isempty(par.CueModifications)
            eval(par.CueModifications);
        end
    end
end
par.TrainingPumpDur=ifstr2num(par.TrainingPumpDur);
if isfield(get(exptparams.TrialObject),'SelfTestMode')
    limit_view = strcmp(get(exptparams.TrialObject,'SelfTestMode'),'Yes');
else
    limit_view=false;
end
if isfield(exptparams,'StopTime')
    limit_view=false;
end
% see if the results figure exists, otherwise create it:
if ~isfield(exptparams, 'ResultsFigure')
    if ~limit_view
        exptparams.ResultsFigure = figure('position',[1 1 900 900]);
        [~,basename]=fileparts(globalparams.mfilename);
        set(exptparams.ResultsFigure,'Name',basename);
        movegui(exptparams.ResultsFigure,'northwest');
    else
        pos=get(exptparams.FigureHandle,'Position');
        screen=get(0,'ScreenSize');
        screen(4)=1020;
        exptparams.ResultsFigure = figure('Units','pixels','position',[0 pos(2) pos(1) screen(4)-pos(2)]);
        feedback_str='';
        exptparams.feedback_text=uicontrol('style','text','string',feedback_str,'FontSize',30,...
            'Units','normalized','Position',[.13 .2 .77 .15],...
            'Parent',exptparams.ResultsFigure,'BackgroundColor',get(gcf,'Color'));
        drawnow
    end
end
cs=get(exptparams.TrialObject,'CueSeg');
isCue=cs(exptparams.InRepTrials)==1;
isCue2=cs(exptparams.InRepTrials)==2;
if limit_view
    feedback_str='FALSE ALARM';
    figure(exptparams.ResultsFigure)
    ax=subplot(4,4,5:8);
    if isCue
        feedback_str='CUE trial. This is the target sound.';
        set(exptparams.feedback_text,'String',feedback_str);
        drawnow
    end
end
if ~OA && ~limit_view && isempty(findall(exptparams.FigureHandle,'Tag','SkipThisTrial'))
    skip_button=uicontrol('style','pushbutton','string','Skip','FontSize',10,...
        'Units','normalized','Position',[.371 .012 .066 .03],...
        'Parent',exptparams.FigureHandle,'Callback',@do_skip,'Tag','SkipThisTrial');
    
    play_cue_button=uicontrol('style','pushbutton','string','Play Cue','FontSize',10,...
        'Units','normalized','Position',[.28 .012 .09 .03],...
        'Parent',exptparams.FigureHandle,'Callback',@do_play_cue,'Tag','PlayCue');
    global SkipThisTrial PlayCue
    SkipThisTrial=false;
    PlayCue=false;
end

w = exptparams.temporary.EventWindows; % Windows created in CanStart


disp('');
disp('------------------------------------------------------------------');
fprintf(' ClassicalConditioning:BehaviorControl -- trial %d (%d for this rep)\n',...
   exptparams.TotalTrials,exptparams.InRepTrials);
disp('------------------------------------------------------------------');
disp('');

% static variable to track 
persistent HitsSinceLastReward
if TrialIndex==1 || isempty(HitsSinceLastReward)
    HitsSinceLastReward=100;
end

RewardFAs=0;
RewardHits=1;

TrialObject=exptparams.TrialObject;
TrialObjectParms=get(TrialObject);

RefFlag=[];
TarFlag=[];
FalseAlarm = 0;
StopTargetFA = par.StopTargetFA;
LickEvents = [];
if ~isfield(exptparams,'Water'), exptparams.Water = 0; end
% we monitor the lick until the end plus response time and postargetlick
LastLick = 0;
HitThisTrial=0;
RemHitThisTrial=0;
FAThisTrial=0;
CorrectRejectThisTrial=0;
if par.TrainingPumpDur>0,
  TrainingRewardGiven=0;
else
  TrainingRewardGiven=1;
end

try
    self_test=strcmp(get(exptparams.TrialObject,'SelfTestMode'),'Yes');
catch err
    self_test=false;
end
if ~self_test
    if isCue
        cuestr='. This is a cue trial.';
    elseif isCue2
        cuestr='. This is a cue2 trial.';
    else
        cuestr='';
    end
    if w.NullTrial
        fprintf('Null trial: no target window%s\n',cuestr);
    else
        for i=1:size(w.TarResponseWin,1)
            fprintf('Target window: %.2f-%.2f sec%s\n',w.TarResponseWin(i,1:2),cuestr);
        end
    end
    if w.RemTrial
        fprintf('Reminder window: %.2f-%.2f sec\n',w.RemResponseWin(1:2));
    end
end

LickCount=0;
LastLickTime=0;
CurrentTime = IOGetTimeStamp(HW);
TCPIP_Lick=0;
while CurrentTime < exptparams.LogDuration  % while trial is not over
    if o.IsSimulation
        [ev,SSO] = GetResponse(exptparams.BehaveObject.SimulatedSubjectObject,o,StimEvents,exptparams);
        exptparams.BehaveObject.SimulatedSubjectObject=SSO;
        LickEvents=AddEvent(LickEvents,ev,TrialIndex);
        clear ev
        ev.Note = 'BEHAVIOR,PUMPON,TrainingPump';
        ev.StartTime = w.TrainingPumpWin(1);
        ev.StopTime = w.TrainingPumpWin(2);
        LickEvents = AddEvent(LickEvents, ev, TrialIndex);
        exptparams.temporary.BehaviorEvents=LickEvents;
        if abs(diff(w.TarResponseWin(:,1))) < o.ResponseWindow
            error('')
        end
        return
    end    
    % Find out if there was a new lick
    if (globalparams.HWSetup==0 && CurrentTime<0.3) || CurrentTime<LastLickTime+0.05
        % don't allow lick detections in certain time windows
        ThisLick=0;
        Lick=0;
    else
        %
        if strcmpi(o.ResponseMethodVar,'LickVideo')
            %set lick based on last TCPIP setting (set by LickTCPIP)
            ThisLick = TCPIP_Lick;
        else
            % query lick DIO
            ThisLick = IOLickRead(HW,{o.ResponseMethodVar});           
        end
        Lick = ThisLick & ~LastLick;
        if Lick
            ev=[];
            ev.Note='LICK';
            ev.StartTime=IOGetTimeStamp(HW);
            ev.StopTime=ev.StartTime;
            LickEvents=AddEvent(LickEvents,ev,TrialIndex);
            fprintf('Lick %.3f sec\n',CurrentTime);
            LickCount = LickCount+1;
            LastLickTime = CurrentTime;
        else
        end
        LastLick = ThisLick;
    end
    % Find out if we are in the CR, reference, or target phase
    [StimPos,EarlyPos,Ref,CorrectRejectThisTrial,CurrentTarIndex] = DetermineTrialWindow(o,CurrentTime,w);

    % CHECK FOR FALSE ALARM - Any lick before target window
    if Lick && (Ref || mod(EarlyPos,2))
        FalseAlarm=1;
        ev=[];
        ev.Note='OUTCOME,FALSEALARM';
        ev.StartTime=IOGetTimeStamp(HW);
        ev.StopTime=ev.StartTime;
        LickEvents=AddEvent(LickEvents,ev,TrialIndex);
        if RewardFAs
            if strcmpi(get(o,'TurnOnLight'),'FalseAlarm')
                [~,ev] = IOLightSwitch (HW, 1, .2);
                LickEvents = AddEvent(LickEvents, ev, TrialIndex);
            end
            if strcmpi(get(o,'TurnOffLight'),'FalseAlarm')
                FATimeOut=ifstr2num(get(o,'TimeOut'));
                if length(FATimeOut)==1
                    ev = IOLightSwitch(HW,0,FATimeOut,'Start',0,0,'Light2');
                else
                    ev = IOLightSwitch(HW,0,FATimeOut(1),'Start',FATimeOut(2),FATimeOut(3),'Light2');
                end
            end
            if strcmpi(get(o,'Shock'),'FalseAlarm')
                ev = IOControlShock (HW, .2, 'Start');
                LickEvents = AddEvent(LickEvents, ev, TrialIndex);
            end
            disp('FA -- Stopping sound');
            ev = IOStopSound(HW);
            LickEvents = AddEvent(LickEvents, ev, TrialIndex);
            FAThisTrial=1;
            break;
        else
            disp('FA not rewarded/punished, recording lick, no behavior impact');
        end
    end
    
    if (Lick) && mod(StimPos,2) && ~isequal(TarFlag,StimPos) && ~Ref && RewardHits,
       HitThisTrial = 1;
       if RewardHits, 
          fprintf('Hit -- ');
          if StopTargetFA<1
             WaterFraction = 1-FalseAlarm;
          else
             WaterFraction = 1;
          end
          PumpDuration = get(o,'PumpDuration') * WaterFraction;
          if length(PumpDuration)>1 && ...
                  ismember('TargetIndices',fields(exptparams.TrialObject))
              InRepTrials=exptparams.InRepTrials;
              TargetIndices=get(exptparams.TrialObject,'TargetIndices');
              ThisTargetIdx=TargetIndices{InRepTrials};
              if length(TrialObjectParms.TargetChannel)==1 && any(ThisTargetIdx>1)
                  TargetChannel=TrialObjectParms.TargetChannel(1);
              else
                  TargetChannel=TrialObjectParms.TargetChannel(ThisTargetIdx);
              end
              rmi = isnan(TargetChannel);
              ThisTargetIdx(rmi)=[]; %remove dummy targets and CatchTargets
              if w.NtarOrcatch>1
                  ThisTargetIdx=ThisTargetIdx(CurrentTarIndex);
              end
              if length(ThisTargetIdx)>1
                  error('ThisTargetIdx contains multiple targets. Somehow this hit matches multiple targets? Must be a bug somewhere...');
              end
              if length(PumpDuration)>=ThisTargetIdx
                  PumpDuration=PumpDuration(ThisTargetIdx);
              else
                  PumpDuration=PumpDuration(1);
              end
              fprintf('Trial # in current rep: %d Target Index: %d  Pump Duration: %.2f\n',...
                  InRepTrials,ThisTargetIdx,PumpDuration);
          else
              PumpDuration=PumpDuration(1);
          end
            
          HitRewardProbability=get(o,'HitRewardProbability');
          if PumpDuration > 0 && ...
                (HitsSinceLastReward>2/HitRewardProbability || rand<HitRewardProbability),
            HitsSinceLastReward=0;
            ev = IOControlPump (HW,'start',PumpDuration);
            LickEvents = AddEvent(LickEvents, ev, TrialIndex);
            exptparams.Water = exptparams.Water+PumpDuration;
            if strcmpi(get(exptparams.BehaveObject,'TurnOnLight'),'Reward')
                [~,ev] = IOLightSwitch (HW, 1, get(o,'PumpDuration'),'Start',0,0);
                LickEvents = AddEvent(LickEvents, ev, TrialIndex);
            end
          else
              HitsSinceLastReward=HitsSinceLastReward+1;
              fprintf('p>%.2f: not rewarding hit (Hits since last reward=%d)\n',...
                  HitRewardProbability,HitsSinceLastReward);
              ev = struct('Note','BEHAVIOR,NOPUMP','StartTime',IOGetTimeStamp(HW),'StopTime',[]);
              LickEvents = AddEvent(LickEvents, ev, TrialIndex);
              if strcmpi(get(exptparams.BehaveObject,'TurnOnLight'),'Reward')
                [~,ev] = IOLightSwitch (HW, 1, get(o,'PumpDuration'),'Start',0,0);
                LickEvents = AddEvent(LickEvents, ev, TrialIndex);
              end
          end
          TarFlag = StimPos;
       end
    end

    % FIXED REWARD FOR TRAINING
    if max(par.TrainingPumpDur)>0 && ~TrainingRewardGiven && ~HitThisTrial && ...
        ((~w.NullTrial && ~w.RemTrial && CurrentTime>mean(w.TrainingPumpWin(1))) ||...
         (w.RemTrial && CurrentTime>mean(w.RemResponseWin(1:2)))),
      fprintf('Time %.2f: Giving training auto-reward in middle of target response window\n',CurrentTime);
      if length(par.TrainingPumpDur)>1 && ...
          ismember('TargetIndices',fields(exptparams.TrialObject)),
          InRepTrials=exptparams.InRepTrials;
          TargetIndices=get(exptparams.TrialObject,'TargetIndices');
          ThisTargetIdx=TargetIndices{InRepTrials};
          if length(par.TrainingPumpDur)>=ThisTargetIdx,
              PumpDuration=par.TrainingPumpDur(ThisTargetIdx);
          else
              PumpDuration=par.TrainingPumpDur(1);
          end
          fprintf('Trial # in current rep: %d Target Index: %d  Pump Duration: %.2f\n',...
              InRepTrials,ThisTargetIdx,PumpDuration);
      else
          PumpDuration=par.TrainingPumpDur(1);
      end
      
      ev = IOControlPump(HW,'start',PumpDuration);
      ev.Note=strrep(ev.Note,',Pump',',TrainingPump');
      LickEvents = AddEvent(LickEvents, ev, TrialIndex);
      TrainingRewardGiven=1;
    end
    
    if (w.RemTrial && HitThisTrial && CurrentTime>RemStartTime) || CorrectRejectThisTrial,
      disp('Stopping sound before reminder plays');
      break;
    end
    
    TCPIP_Lick=0; %Does nothing unless ResponseMethodVar is LickVideo
    
    CurrentTime = IOGetTimeStamp(HW);
    if globalparams.HWSetup==0
        pause(0.01);
    end
end
IOStopSound(HW);

if FAThisTrial && RewardFAs,
   PauseTime = ifstr2num(get(o,'TimeOut'));
   fprintf('FA -- time out ITI %.1f sec\n',PauseTime);

elseif HitThisTrial && RewardHits,
  PauseTime = ifstr2num(get(o,'CorrectITI'));
  fprintf('Hit -- ITI %.1f sec\n',PauseTime);
  
elseif HitThisTrial && ~RewardHits,
  PauseTime = ifstr2num(get(o,'CorrectITI'));
  fprintf('Hit -- not rewarded -- ITI %.1f sec\n',PauseTime);
  
elseif CorrectRejectThisTrial,
  RewardNullPause=get(o,'RewardNullPause');
  fprintf('Correct reject/null trial -- pause %.1f sec before pump\n',RewardNullPause);
  pause(get(o,'RewardNullPause'));
  
  PauseTime = ifstr2num(get(o,'CorrectITI'));
  PumpDuration = get(o,'PumpDuration') * NullFrac;
  if PumpDuration
      ev = IOControlPump (HW,'start',PumpDuration);
      LickEvents = AddEvent(LickEvents, ev, TrialIndex);
      exptparams.Water = exptparams.Water+PumpDuration;
  end
  ev.Note='OUTCOME,CR';
  ev.StartTime=IOGetTimeStamp(HW);
  ev.StopTime=ev.StartTime;
  LickEvents=AddEvent(LickEvents,ev,TrialIndex);
  fprintf('Correct reject/null trial -- ITI %.1f sec\n',PauseTime);
  
elseif RewardHits,
  % TRUE MISS
  PauseTime = ifstr2num(get(o,'MissTimeOut'));
  ev=[];
  if RemHitThisTrial,
      ev.Note='OUTCOME,REM';
      ev.StartTime=IOGetTimeStamp(HW);
      ev.StopTime=ev.StartTime;
      LickEvents=AddEvent(LickEvents,ev,TrialIndex);
  end
  ev.Note='OUTCOME,MISS';
  ev.StartTime=IOGetTimeStamp(HW);
  ev.StopTime=ev.StartTime;
  LickEvents=AddEvent(LickEvents,ev,TrialIndex);
  fprintf('Miss -- time out ITI %.1f sec\n',PauseTime);
  
else
  PauseTime = ifstr2num(get(o,'CorrectITI'));
  if RemHitThisTrial,
      ev.Note='OUTCOME,REM';
      ev.StartTime=IOGetTimeStamp(HW);
      ev.StopTime=ev.StartTime;
      LickEvents=AddEvent(LickEvents,ev,TrialIndex);
  end
  fprintf('Correct reject/unrewarded trial -- pausing %.1f sec\n',PauseTime);
end

fprintf('Overriding pause and setting PauseTime=0\n');
PauseTime=0;

if strcmpi(par.ITICarryOverToPreTrial,'No'),
    ThisTime = clock;
    while etime(clock,ThisTime) < PauseTime,
        drawnow;
    end
    exptparams.NeutralPreTrialDelay=0;
else
    exptparams.NeutralPreTrialDelay=PauseTime;
end

exptparams.temporary.BehaviorEvents=LickEvents;

end

function do_skip(hObject, eventdata, handles)
    global SkipThisTrial
    SkipThisTrial=true;
    fprintf('Set SkipThisTrial to True\n')
end

function do_play_cue(hObject, eventdata, handles)
    global PlayCue
    PlayCue=true;
    fprintf('Set PlayCue to True\n')
end
