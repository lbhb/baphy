function [StopExperiment,exptparams] = CanStart (o, HW, StimEvents, globalparams, exptparams, TrialIndex)

% Make cue modifications for this trial
par=get(o);
CueSeg = get(exptparams.TrialObject,'CueSeg');
if CueSeg(exptparams.InRepTrials)==1
    if ~isempty(par.CueModifications)
       eval(par.CueModifications); 
    end
end


% generate lists of behaviorally relevant event windows
[w,exptparams] = GenerateEventWindows(o,exptparams,StimEvents,par,{'StimBeahviorPumpWindows'});
exptparams.temporary.EventWindows = w;

StopExperiment = [];

% start immediately