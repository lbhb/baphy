function exptparams = PerformanceAnalysis (o, HW, StimEvents, globalparams, exptparams, TrialIndex, LickData)
% Performance analaysis for ClassicalConditioning behavior object
%
% EarlyResponse: lick during the Early window after target. Early response causes the trial to stop immediately and a timeout.
% Hit: lick during Response window after the target. This is a correct response to the target and the water reward will be given.
% Miss: no lick during Response window after the target. A timeout will be given.
% FalseAlarm: lick during corresponding Response window for a reference.
% IneffectiveTrial: a trial the animal licks to all the references. In other word, a trial has a False alarm rate of 1. The trial will be stopped right before a target appears.
% InstructionTrial: a trial which instruct the animal stopping lick to a reference sound. It repeats play a reference sequence with an interval (Early window + Response window) until the animal stop lick to 2 successive references. Instruction trail is triggered when a Ineffective trial detected.
% ReferenceLickTrial: a trial the animal lick during reference period (before the target appears).
% HitRate: total number of False Alarm divides by total number of warning trials.
% EarlyRate: total number of Early response divides by total number of warning trials.
% MissRate: total number of missed trials divides by total number of warning trials.
% FalseAlarmRate: total number of False Alarm divides by total number of references across all warning trials.

% Nima, April 2006

% Trial-object dependent code:
%Line 48

trialparms=get(exptparams.TrialObject);

% Recalculate response windows to permit offline analysis
par=get(o);
if isfield(exptparams,'TrialParams') && exptparams.TrialParams(TrialIndex).CueSeg==1
    if ~isempty(par.CueModifications)
        eval(par.CueModifications);
    end
end

%% figure out trial number
if isfield(exptparams, 'Performance')
    perf = exptparams.Performance(1:end-1);
    tnum = length(perf) + 1;
else
    tnum = 1;
end

%if tnum ~=TrialIndex, error('check'); end
%% generate lists of behaviorally relevant event windows
if getparm(exptparams,'OfflineAnalysis',0)
   [w,exptparams] = GenerateEventWindows(o,exptparams,StimEvents,par,{'StimBeahviorPumpWindows','PumpActualWindows'});
else
    w = exptparams.temporary.EventWindows; % Created in CanStart
    [PumpActualWindows,~] = GenerateEventWindows(o,exptparams,StimEvents,par,{'PumpActualWindows'});
    w.TrainingPumpWinActual = PumpActualWindows.TrainingPumpWinActual;
    w.PumpWinActual = PumpActualWindows.PumpWinActual;
end

%% Reduce response window to before conditioned reward (to only count times when they respond before it as hits)
for i=1:size(w.TarOrCatchResponseWin,1)
    RewardTime  = mean(w.TarOrCatchResponseWin(i,:));
    w.TarOrCatchResponseWin(i,2) = RewardTime;
end
for i=1:size(w.TarResponseWin,1)
    RewardTime  = mean(w.TarResponseWin(i,:));
    w.TarResponseWin(i,2) = RewardTime;
end
for i=1:size(w.CatchResponseWin,1)
    RewardTime  = mean(w.CatchResponseWin(i,:));
    w.CatchResponseWin(i,2) = RewardTime;
end
exptparams.temporary.EventWindows = w;
%% TrialObject-specific code
%% Define FAStartTime, the time after which a lick is considered a FA
exptparams.temporary.EventWindows = w;

switch exptparams.TrialObjectClass
    case {'MultiRefTar','RSSToneMono'}
        % "strict" - FA is response to any possible target slot preceeding the target
        TarPreStimSilence=get(trialparms.TargetHandle,'PreStimSilence');
        if trialparms.SingleRefSegmentLen>0
            PossibleTarTimes=(find(trialparms.ReferenceCountFreq(:))-0).*...
                trialparms.SingleRefSegmentLen+w.FirstRefTime;
            if w.two_target
                PossibleTar2Offsets=(find(trialparms.Tar2SegCountFreq(:))).*...
                    trialparms.Tar2SegmentLen+w.FirstRefTime;
                PossibleTar2Times=repmat(PossibleTarTimes,1,length(PossibleTar2Offsets))...
                    + repmat(PossibleTar2Offsets',length(PossibleTarTimes),1);
                PossibleTarTimes=[PossibleTarTimes; PossibleTar2Times(:)];
            end
        else
            RefSegLen=get(trialparms.ReferenceHandle,'PreStimSilence')+...
                get(trialparms.ReferenceHandle,'Duration')+...
                get(trialparms.ReferenceHandle,'PostStimSilence');
            PossibleTarTimes=(find(trialparms.ReferenceCountFreq(:))-1).*...
                RefSegLen;
        end
        FAStartTime=min(PossibleTarTimes);
        if FAStartTime>w.TarOrCatchEarlyWin
            warning('Kludge Alert! Shifting FA start time to handle last trial in block that is too short!');
            FAStartTime=w.TarOrCatchEarlyWin;
        end
    case 'ReferenceTarget'
        FAStartTime = getparm(trialparms.ReferenceHandle, 'MinimumTargetTime',[]);
        if isempty(FAStartTime)
            FAStartTime=get(trialparms.ReferenceHandle,'PreStimSilence');
        end
    otherwise
        FAStartTime=get(trialparms.ReferenceHandle,'PreStimSilence');
end

%% Preprocess LickData
LickData = PreprocessLickData(o,LickData);
% LickData has all licks

%% Analyze lick vector to find out when animal licked with respect to target, call di_nolick
fs = HW.params.fsAI;
w.Check_both_FA_ET=false;
exptparams = ComputePerformance_from_Windows_and_Licks(o,exptparams,par,w,FAStartTime,LickData,LickData,fs,tnum);
if isfield(exptparams,'Water')
    exptparams.Performance(end).Water = exptparams.Water .* globalparams.PumpMlPerSec.Pump;
end