function Duration = LogDuration (o,HW, StimEvents, globalparams, exptparams, TrialIndex)
% Specifies how long we want to collect data

% the duration is when sound ends plus response time plus
% PosTargetLickWindow:
EndTimes = cat(1,StimEvents.StopTime);
Duration = max(EndTimes)+0.1;
