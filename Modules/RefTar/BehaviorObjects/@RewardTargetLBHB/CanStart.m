function [StopExperiment,exptparams] = CanStart (o, HW, StimEvents, globalparams, exptparams, TrialIndex)
% CanStart for ReferenceAvoidance
% We wait until the animal does not lick for at least NoResponseTime

% Make cue modifications for this trial
par=get(o);
CueSeg = get(exptparams.TrialObject,'CueSeg');
if CueSeg(exptparams.InRepTrials)==1
    if ~isempty(par.CueModifications)
       eval(par.CueModifications); 
    end
end

disp('RewardTargetLBHB/CanStart:  intializing trial');
global StopExperiment  % records if user pressed "Stop" in RefTarGui
global SkipThisTrial TCPIP_Lick
% turn on the light if its needed:
if strcmpi(par.TurnOffLight,'Ineffective')
    ev = IOLightSwitch(HW,1);
end
if 0 && TrialIndex==1
    PumpDuration = 2*par.PumpDuration;
    tic;  %added by pby
    IOControlPump (HW,'start',PumpDuration);
    pause(PumpDuration);
    if strcmpi(par.TurnOnLight,'BehaveOrPassive')
        IOLightSwitch (HW, 1);
    end
    pause(1);
end

% figure out neutral delay (if any)
if isfield(exptparams,'NeutralPreTrialDelay'),
    NeutralDelay=exptparams.NeutralPreTrialDelay;
else
    NeutralDelay=0;
end

try
    PostMissResponseTime=par.PostMissResponseTime;
    if length(PostMissResponseTime)==1
        WaitAfterNMisses=1;
    else
        WaitAfterNMisses=PostMissResponseTime(2);
        PostMissResponseTime(2)=[];
    end
catch
    PostMissResponseTime=0;
    WaitAfterNMisses=1;
end

% variable no lick time
NoLickTime=par.NoResponseTimeFixed + random('exp',par.NoResponseTimeVar);
CueSeg=get(exptparams.TrialObject,'CueSeg');
try
    if CueSeg(exptparams.InRepTrials)==1
    NoLickTime=0;
    end
catch
   aa=2; 
end


% set up sound to play during neutral delay and NoLickTime
% (if ReferenceDuringPreTrial=='Yes')
RefCounter=0;
TrialObject=exptparams.TrialObject;
TrialObjectParms=get(TrialObject);

% figure out which AO channel is playing common targets
if isfield(HW.AO,'NumChannels'),
   AOChannels=HW.AO.NumChannels;
   if AOChannels>1,
      ChannelNames=strsep(HW.AO.Names,',',1);
      AOChannelOrder=zeros(AOChannels,1);
      for ii=1:AOChannels,
         tc=find(strcmp(['SoundOut',num2str(ii)],ChannelNames));
         if ~isempty(tc),
            AOChannelOrder(ii)=tc;
         else
            AOChannelOrder(ii)=ii;
         end
      end
      if isfield(TrialObjectParms,'TargetIdxFreq') && isfield(TrialObjectParms,'TargetChannel'),
         CommonTarget=find(TrialObjectParms.TargetIdxFreq==max(TrialObjectParms.TargetIdxFreq),1);
         if CommonTarget>length(TrialObjectParms.TargetChannel),
            CommonTarget=AOChannelOrder(1);
         else
            CommonTarget=AOChannelOrder(TrialObjectParms.TargetChannel(CommonTarget));
         end
      else
         CommonTarget=1;
      end
   else
      CommonTarget=1;
   end
else
   CommonTarget=1;
end

if strcmpi(par.Light1,'OnTarget1Blocks') && CommonTarget==1
    IOLightSwitch(HW, 1, 0,'Start',0,0,'Light');
elseif strcmpi(par.Light2,'OnTarget1Blocks') && CommonTarget==1
    IOLightSwitch(HW, 1, 0,'Start',0,0,'Light2');
elseif strcmpi(par.Light3,'OnTarget1Blocks') && CommonTarget==1
    IOLightSwitch(HW, 1, 0,'Start',0,0,'Light3');
elseif strcmpi(par.Light1,'OnTarget2Blocks') && CommonTarget==2
    IOLightSwitch(HW, 1, 0,'Start',0,0,'Light');
elseif strcmpi(par.Light2,'OnTarget2Blocks') && CommonTarget==2
    IOLightSwitch(HW, 1, 0,'Start',0,0,'Light2');
elseif strcmpi(par.Light3,'OnTarget2Blocks') && CommonTarget==2
    IOLightSwitch(HW, 1, 0,'Start',0,0,'Light3');
end

if strcmpi(par.ReferenceDuringPreTrial,'Yes')
   TrialObjectName=TrialObjectParms.descriptor;
   TrialFs=get(TrialObject,'SamplingRate');
   RefSet={};
   RefLen=[];
   if strcmpi(TrialObjectName,'TwoStreamRefTar'),
      TrialObject=set(TrialObject,'RelativeTarRefdB',-100);
      TrialObject=set(TrialObject,'PostTrialSilence',0);
      NumberOfTrials=get(TrialObject,'NumberOfTrials');
      while sum([0 RefLen])<NeutralDelay+NoLickTime.*2,
         RefCounter=RefCounter+1;
         RefSet{RefCounter}=waveform(TrialObject,ceil(rand.*NumberOfTrials));
         RefLen(RefCounter)=length(RefSet{RefCounter})./TrialFs;
      end
   else
      RefObject=get(TrialObject,'ReferenceHandle');
      RefMaxIdx=get(RefObject,'MaxIndex');
      RefFs=TrialFs;
      RefObject=set(RefObject,'SamplingRate',RefFs);
      while sum([0 RefLen])<NeutralDelay+NoLickTime.*2,
         RefCounter=RefCounter+1;
         RefSet{RefCounter}=waveform(RefObject,ceil(rand.*RefMaxIdx));
         RefLen(RefCounter)=length(RefSet{RefCounter})./TrialFs;
      end
   end
end  

if globalparams.HWSetup==0, 
    disp('RewardTargetLBHB/CanStart:  intializing trial');
    %if HW.params.HWSetup==0 && isfield(HW,'AI') && ~isempty(HW.AI),
    %    start(HW.AI);
    %end
else
    % 
    ClearSound=zeros(11000,2);
    %IOStopSound(HW);
    HW = IOLoadSound(HW, ClearSound);
    IOStartSound(HW);
    pause(0.1);
    IOStopSound(HW);
end

%% Interlude... Now that sound buffer has been forced to zeros and any cue lights are set up, figure out time windows for this trial
% generate lists of behaviorally relevant event windows
[w,exptparams] = GenerateEventWindows(o,exptparams,StimEvents,par,{'StimBeahviorPumpWindows'});
exptparams.temporary.EventWindows = w;

%% Now actually loop until we're ready to start
if PostMissResponseTime>0 && TrialIndex>WaitAfterNMisses && ...
    all([exptparams.Performance(TrialIndex-WaitAfterNMisses:TrialIndex-1).Miss]) && ... %Miss trial
    all(arrayfun(@(x)x.FirstLickTime==0,exptparams.Performance(TrialIndex-WaitAfterNMisses))) % Didn't move lever at all during trial
    %Used for marmoset task where lever press is required to initiate trials
    % If last trial was a miss, marmo may be holding the bar continuously
    % without attention. If PostMissResponseTime>0, wait for it to release the
    % bar for PostMissResponseTime seconds before moving onto the next code
    % block (where we wait for it to hold it down to initiate a trial).
    BarReleaseTimeComplete=0;
    BarReleaseStartTime=clock;
    if WaitAfterNMisses==1
        fprintf('Post-miss trial, waiting for bar to be released (%.2f sec)\n',PostMissResponseTime);
    else
        fprintf('%d misses in a row, waiting for bar to be released (%.2f sec)\n',WaitAfterNMisses,PostMissResponseTime);
    end
    while ~BarReleaseTimeComplete && ~StopExperiment && ~SkipThisTrial
        if globalparams.HWSetup~=0
            BarReleased=IOLickRead(HW,{o.ResponseMethodVar});
            if ~BarReleased
                BarReleaseStartTime = clock;
            end
            if etime(clock,BarReleaseStartTime)>PostMissResponseTime
                BarReleaseTimeComplete=1;
                fprintf('Bar released, moving on.\n');
            end
        end
        drawnow
        if SkipThisTrial
            fprintf('................Skipping\n')
            SkipThisTrial=false;
            break
        end
    end 
end


NeutralDelayComplete=0;
if NeutralDelay>0,
    fprintf('Waiting fixed time (%.2f sec)\n',NeutralDelay);
end
NoLickTimeComplete=0;
RefIdx=0;
NextRefStart=-1;
tic;
switch par.SignalAvailableTrials
    case 'No'
        SignalAvailableTrials=0;
    case 'Buzz'
        SignalAvailableTrials=1;
    otherwise
        error('SignalAvailableTrials was %s, it must be either ''No'' or ''Buzz''.',par.SignalAvailableTrials)
end

NoLickStartTime=clock;
LastAvailableSignalStartTime=clock;
do_SignalAvailableClockReset=0;
while ~NoLickTimeComplete && ~StopExperiment && NoLickTime>0
    if RefCounter && toc>NextRefStart,
        RefIdx=RefIdx+1;
        if RefIdx>RefCounter,
            RefIdx=1;  % loop around to first reference when depleted
        end
        IOStopSound(HW);
        HW = IOLoadSound(HW, RefSet{RefIdx});
        IOStartSound(HW);
        NextRefStart=toc+RefLen(RefIdx);
    end
    if ~NeutralDelayComplete,
        if toc>NeutralDelay,
            NeutralDelayComplete=1;
            NoLickStartTime=clock;
            fprintf('Waiting for no response time (%.2f sec)\n',NoLickTime);
        end
    else
        if globalparams.HWSetup~=0 
            if strcmpi(o.ResponseMethodVar,'LickVideo')
                %set get VideoLick status via TCPIP
                %MSG = ['GETVAR',HW.Pupil.COMterm,'[MG.lick]',HW.Pupil.MSGterm];
                MSG = ['GETVAR',HW.Lick.COMterm,'[MG.lick]',HW.Lick.MSGterm];
                Lick_=[];
                while isempty(Lick_) || int8(Lick_(1))==76
                    %Lick_=IOSendMessageTCPIP(HW.Pupil,MSG,[],'',0,0);
                    Lick_=IOSendMessageTCPIP(HW.Lick,MSG,[],'',0,0);
                end
                Lick=str2num(Lick_);
            else
                % query lick DIO
                Lick = IOLickRead(HW,{o.ResponseMethodVar});
            end
            if Lick
                NoLickStartTime = clock;
                if SignalAvailableTrials && do_SignalAvailableClockReset
                    LastAvailableSignalStartTime=clock;
                    do_SignalAvailableClockReset=0;
                end
            elseif ~Lick && SignalAvailableTrials 
                %Reset Aviable Signal clock if animal is starting to initiate a trial
                % for marmoset bar press, this happens if they press to
                % initiate a trial, but don't hold long enough to actually
                % initiate one. Every time this happens, reset the
                % SignalAvailable clock.
                do_SignalAvailableClockReset=1;
            end
        end
        if etime(clock,NoLickStartTime)>NoLickTime
            NoLickTimeComplete=1;
        end
        if SignalAvailableTrials && Lick
            if etime(clock,LastAvailableSignalStartTime)>par.SignalAvailableISI
               LastAvailableSignalStartTime = clock;
                   fprintf('Buzzing %.2f sec\n',par.SignalAvailableDuration);
                   ev = IOLightSwitch(HW,1,par.SignalAvailableDuration,'Start',0,0,'Buzzer');
            end
        end
    end
    try
        drawnow
    catch err
        aa=2;
    end
end

if SignalAvailableTrials
   ev = IOLightSwitch(HW,0,0,'Start',0,0,'Buzzer');
end
%if HW.params.HWSetup==0 && isfield(HW,'AI') && ~isempty(HW.AI),
%    stop(HW.AI);
%end
if RefCounter,
    % make sure pre trial sound has stopped playing
    IOStopSound(HW);
end
TCPIP_Lick=false;
