function [LickEvents, exptparams] = BehaviorControl(o, HW, StimEvents, ...
    globalparams, exptparams, TrialIndex)
global TCPIP_Lick
% Behavior Object for RewardTargetLBHB object
%
% SVD, July 2012, LBHB
% algorithm:
%   Define time windows for FA (reference window) and hits (target window)
%   monitor the lick,
%     if animal licks in response window or early window, terminate the
%     sound and stop the trial
%     if animal licks in the response window after target, give water
%     turn on light at appropriate phases
OA=false;
try
    OA=exptparams.OfflineAnalysis;
end
par=get(o);
if ~OA
    CueSeg=get(exptparams.TrialObject,'CueSeg');
    if CueSeg(exptparams.InRepTrials)==1
        if ~isempty(par.CueModifications)
            eval(par.CueModifications);
        end
    end
end
par.TrainingPumpDur=ifstr2num(par.TrainingPumpDur);
IsCatch = get(exptparams.TrialObject,'IsCatch_');
if isfield(get(exptparams.TrialObject),'SelfTestMode')
    limit_view = strcmp(get(exptparams.TrialObject,'SelfTestMode'),'Yes');
else
    limit_view=false;
end
if isfield(exptparams,'StopTime')
    limit_view=false;
end
% see if the results figure exists, otherwise create it:
if ~isfield(exptparams, 'ResultsFigure')
    if ~limit_view
        exptparams.ResultsFigure = figure('position',[1 1 900 900]);
        [~,basename]=fileparts(globalparams.mfilename);
        set(exptparams.ResultsFigure,'Name',basename);
        movegui(exptparams.ResultsFigure,'northwest');
    else
        pos=get(exptparams.FigureHandle,'Position');
        screen=get(0,'ScreenSize');
        screen(4)=1020;
        exptparams.ResultsFigure = figure('Units','pixels','position',[0 pos(2) pos(1) screen(4)-pos(2)]);
        feedback_str='';
        exptparams.feedback_text=uicontrol('style','text','string',feedback_str,'FontSize',30,...
            'Units','normalized','Position',[.13 .2 .77 .15],...
            'Parent',exptparams.ResultsFigure,'BackgroundColor',get(gcf,'Color'));
        drawnow
    end
end
cs=get(exptparams.TrialObject,'CueSeg');
isCue=cs(exptparams.InRepTrials)==1;
isCue2=cs(exptparams.InRepTrials)==2;
if limit_view
    feedback_str='FALSE ALARM';
    figure(exptparams.ResultsFigure)
    ax=subplot(4,4,5:8);
    if isCue
        feedback_str='CUE trial. This is the target sound.';
        set(exptparams.feedback_text,'String',feedback_str);
        drawnow
    end
end
if ~OA && ~limit_view && isempty(findall(exptparams.FigureHandle,'Tag','SkipThisTrial'))
    skip_button=uicontrol('style','pushbutton','string','Skip','FontSize',10,...
        'Units','normalized','Position',[.371 .012 .066 .03],...
        'Parent',exptparams.FigureHandle,'Callback',@do_skip,'Tag','SkipThisTrial');
    
    play_cue_button=uicontrol('style','pushbutton','string','Play Cue','FontSize',10,...
        'Units','normalized','Position',[.28 .012 .09 .03],...
        'Parent',exptparams.FigureHandle,'Callback',@do_play_cue,'Tag','PlayCue');
    global SkipThisTrial PlayCue
    SkipThisTrial=false;
    PlayCue=false;
end

w = exptparams.temporary.EventWindows; % Windows created in CanStart

if isfield(exptparams,'OfflineAnalysis') && exptparams.OfflineAnalysis
    LickEvents = [];
    return
end

disp('');
disp('------------------------------------------------------------------');
fprintf('  BehaviorControl --- starting trial %d (%d for this rep)\n',...
    exptparams.TotalTrials,exptparams.InRepTrials);
disp('------------------------------------------------------------------');
disp('');

% static variable to track
persistent HitsSinceLastReward
if TrialIndex==1 || isempty(HitsSinceLastReward),
    HitsSinceLastReward=100;
end

if strcmpi(par.RewardNullTrials,'No'),
    NullFrac=0;
elseif strcmpi(par.RewardNullTrials,'Yes'),
    NullFrac=1;
else
    NullFrac=ifstr2num(par.RewardNullTrials);
end

if w.NullTrial && ~NullFrac,
    %PunishFAs=0;
    RewardHits=0;
    disp('Null trial: CR not rewarded');
elseif w.NullTrial,
    RewardHits=1;
    disp('Null trial: CR rewarded');
else
    RewardHits=1;
end
if strcmpi(par.PunishFA,'yes'),
    PunishFAs=1;
else
    PunishFAs=0;
end

TrialObject=exptparams.TrialObject;
TrialObjectParms=get(TrialObject);

if strcmp(TrialObjectParms.descriptor,'MultiRefTar'),
    if par.RewardTargetMinFrequency>0 && w.NtarOrcatch>1
        error('Fix this if needed')
    end
    TargetIndices = get(TrialObject,'TargetIndices');
    ThisTargetIdx = TargetIndices{exptparams.InRepTrials};
    TargetIdxFreq = get(TrialObject,'TargetIdxFreq');
    TargetMaxIndex = get(TrialObject,'TargetMaxIndex');
    
    if isempty(TargetIdxFreq),
        TargetIdxFreq=1;
    elseif length(TargetIdxFreq)>TargetMaxIndex,
        TargetIdxFreq=TargetIdxFreq(1:TargetMaxIndex);
    end
    if sum(TargetIdxFreq)==0,
        TargetIdxFreq(:)=1;
    end
    TargetIdxFreq=TargetIdxFreq./sum(TargetIdxFreq);
    if TargetIdxFreq(ThisTargetIdx)<par.RewardTargetMinFrequency,
        RewardHits=0;
        disp('Rare target: Not rewarding this trial');
    end
end


CommonTarget=1;
if isfield(HW.AO,'NumChannels'),
    AOChannels=HW.AO.NumChannels;
    if AOChannels>1,
        ChannelNames=strsep(HW.AO.Names,',',1);
        AOChannelOrder=zeros(AOChannels,1);
        for ii=1:AOChannels,
            tc=find(strcmp(['SoundOut',num2str(ii)],ChannelNames));
            if ~isempty(tc),
                AOChannelOrder(ii)=tc;
            else
                AOChannelOrder(ii)=ii;
            end
        end
        if isfield(TrialObjectParms,'TargetIdxFreq') && isfield(TrialObjectParms,'TargetChannel'),
            CommonTarget=find(TrialObjectParms.TargetIdxFreq==max(TrialObjectParms.TargetIdxFreq),1);
            if CommonTarget>length(TrialObjectParms.TargetChannel),
                CommonTarget=AOChannelOrder(1);
            else
                CommonTarget=AOChannelOrder(TrialObjectParms.TargetChannel(CommonTarget));
            end
        else
            CommonTarget=1;
        end
    end
end

if ~o.IsSimulation
    % turn on trial-length lights
    if strcmpi(par.Light1,'OnTarget1Blocks') && CommonTarget==1,
        IOLightSwitch(HW, 1, 0,'Start',0,0,'Light');
    elseif strcmpi(par.Light2,'OnTarget1Blocks') && CommonTarget==1,
        IOLightSwitch(HW, 1, 0,'Start',0,0,'Light2');
    elseif strcmpi(par.Light3,'OnTarget1Blocks') && CommonTarget==1,
        IOLightSwitch(HW, 1, 0,'Start',0,0,'Light3');
    elseif strcmpi(par.Light1,'OnTarget2Blocks') && CommonTarget==2,
        IOLightSwitch(HW, 1, 0,'Start',0,0,'Light');
    elseif strcmpi(par.Light2,'OnTarget2Blocks') && CommonTarget==2,
        IOLightSwitch(HW, 1, 0,'Start',0,0,'Light2');
    elseif strcmpi(par.Light3,'OnTarget2Blocks') && CommonTarget==2,
        IOLightSwitch(HW, 1, 0,'Start',0,0,'Light3');
    end
end


RefFlag=[];
TarFlag=[];
FalseAlarm = 0;
StopTargetFA = par.StopTargetFA;
LickEvents = [];
if ~isfield(exptparams,'Water'), exptparams.Water = 0; end
% we monitor the lick until the end plus response time and postargetlick
LastLick = 0;
HitThisTrial=0;
RemHitThisTrial=0;
FAThisTrial=0;
CorrectRejectThisTrial=0;
if par.TrainingPumpDur>0,
    TrainingRewardGiven=0;
else
    TrainingRewardGiven=1;
end


if strcmpi(get(o,'StopStim'),'Immediately'),
    StopFlag = 1;
else
    StopFlag=0;
end
try
    self_test=strcmp(get(exptparams.TrialObject,'SelfTestMode'),'Yes');
catch err
    self_test=false;
end
if ~self_test
    if isCue
        cuestr='. This is a cue trial.';
    elseif isCue2
        cuestr='. This is a cue2 trial.';
    else
        cuestr='';
    end
    if w.NullTrial
        fprintf('Null trial: no target window%s\n',cuestr);
    else
        for i=1:size(w.TarResponseWin,1)
            fprintf('Target window: %.2f-%.2f sec%s\n',w.TarResponseWin(i,1:2),cuestr);
        end
    end
    if w.RemTrial
        fprintf('Reminder window: %.2f-%.2f sec\n',w.RemResponseWin(1:2));
    end
end


ResponseCountDistrib = o.ResponseCountDistrib;
if length(ResponseCountDistrib) > 1 && sum(ResponseCountDistrib(2:end))>0
    ResponseCountDistrib = ResponseCountDistrib/sum(ResponseCountDistrib);
    r=cumsum(ResponseCountDistrib);
    LicksRequired = find(r>rand, 1);
    exptparams.BehaveObject.LicksRequired(TrialIndex) = LicksRequired; % save for performance analysis
    fprintf('LicksRequired %d\n',LicksRequired);
else
    LicksRequired=1;
    exptparams.BehaveObject.LicksRequired(TrialIndex) = LicksRequired;
end

if ~isempty(w.TarResponseWin) && par.TrainingPumpDur>0
    tar_auto_reward_time=mean(w.TarResponseWin(1,:)); %Default: give auto-reward in middle of target window
    %tar_auto_reward_time=w.TarResponseWin(1,1)+.4;
    %tar_auto_reward_time=w.TarResponseWin(1,1)+.2*diff(w.TarResponseWin(1,1:2));
    fprintf('tar_auto_reward_time: %.3f\n', tar_auto_reward_time);
    rem_auto_reward_time=mean(w.RemResponseWin);
end
    
LickCount=0;
LastLickTime=0;
got_hit=false;
CurrentTime = IOGetTimeStamp(HW);
while CurrentTime < exptparams.LogDuration  % while trial is not over
    if o.IsSimulation
        [ev,SSO] = GetResponse(exptparams.BehaveObject.SimulatedSubjectObject,o,StimEvents,exptparams);
        exptparams.BehaveObject.SimulatedSubjectObject=SSO;
        LickEvents=AddEvent(LickEvents,ev,TrialIndex);
        exptparams.temporary.BehaviorEvents=LickEvents;
        if abs(diff(w.TarResponseWin(:,1))) < o.ResponseWindow
            error('')
        end
        return
    end
    % Find out if there was a new lick
    if (globalparams.HWSetup==0 && CurrentTime<0.3) || CurrentTime<LastLickTime+0.05
        % don't allow lick detections in certain time windows
        ThisLick=0;
        Lick=0;
    else
        %
        if strcmpi(o.ResponseMethodVar,'LickVideo')
            %set lick based on last TCPIP setting (set by LickTCPIP)
            ThisLick = TCPIP_Lick;
        else
            % query lick DIO
            ThisLick = IOLickRead(HW,{o.ResponseMethodVar});
        end
        Lick = ThisLick & ~LastLick;
        if Lick
            ev=[];
            ev.Note='LICK';
            ev.StartTime=IOGetTimeStamp(HW);
            ev.StopTime=ev.StartTime;
            LickEvents=AddEvent(LickEvents,ev,TrialIndex);
            % CRH try to prevent double counting of licks
            %if round(CurrentTime, 1) ~= LastLickTime
            %    LickCount=LickCount+1;
            %    fprintf('Lick %.1f sec\n',CurrentTime);
            %else
            %    fprintf('Excluded Lick %.1f sec\n',CurrentTime);
            %end
            fprintf('Lick %.3f sec (BehaviorControl)\n',CurrentTime);
            LickCount = LickCount+1;
            LastLickTime = CurrentTime;
            TCPIP_Lick=0; %Does nothing unless ResponseMethodVar is LickVideo
        end
        LastLick = ThisLick;
    end
    
    % Find out if we are in the CR, reference, or target phase
    [StimPos,EarlyPos,Ref,CorrectRejectThisTrial,CurrentTarIndex] = DetermineTrialWindow(o,CurrentTime,w);
    
    
    % CHECK FOR FALSE ALARM - Any lick before target window
    if Lick && (Ref || mod(EarlyPos,2)) && ~got_hit
        FalseAlarm=1;
        if PunishFAs
            ev=[];
            ev.Note='OUTCOME,FALSEALARM';
            ev.StartTime=IOGetTimeStamp(HW);
            ev.StopTime=ev.StartTime;
            LickEvents=AddEvent(LickEvents,ev,TrialIndex);
            if strcmpi(get(o,'TurnOnLight'),'FalseAlarm')
                [~,ev] = IOLightSwitch (HW, 1, .2);
                LickEvents = AddEvent(LickEvents, ev, TrialIndex);
            end
            if strcmpi(get(o,'TurnOffLight'),'FalseAlarm') || strcmpi(get(o,'TurnOffLight'),'FalseAlarm_and_Miss')
                FATimeOut=ifstr2num(get(o,'TimeOut'));
                if length(FATimeOut)==1
                    ev = IOLightSwitch(HW,0,FATimeOut,'Start',0,0,'Light2');
                else
                    ev = IOLightSwitch(HW,0,FATimeOut(1),'Start',FATimeOut(2),FATimeOut(3),'Light2');
                end
            end
            if strcmpi(get(o,'Shock'),'FalseAlarm')
                ev = IOControlShock (HW, .2, 'Start');
                LickEvents = AddEvent(LickEvents, ev, TrialIndex);
            end
            disp('FA -- Stopping sound');
            fprintf('CurrentTime is %f',CurrentTime)
            ev = IOStopSound(HW);
            LickEvents = AddEvent(LickEvents, ev, TrialIndex);
            FAThisTrial=1;
            break;
        else
            ev=[];
            ev.Note='LICK,ALLOWEDFA';
            ev.StartTime=IOGetTimeStamp(HW);
            ev.StopTime=ev.StartTime;
            LickEvents=AddEvent(LickEvents,ev,TrialIndex);
            disp('FA not rewarded/punished, recording lick, no behavior impact.');
        end
        if limit_view
            if ~isCue
                feedback_str={'FALSE ALARM',[num2str(TrialIndex),' Total Trials']};
            end
            set(exptparams.feedback_text,'String',feedback_str);
            cla(ax)
            drawnow
        end
    end
    
    % CHECK FOR HIT - Lick in target response window
    if (LickCount>=LicksRequired) && mod(StimPos,2) && ~isequal(TarFlag,StimPos) && ~Ref && RewardHits && ~got_hit
        HitThisTrial = 1;
        if RewardHits
            TarFlag = StimPos;
            fprintf('Hit -- ');
            if StopTargetFA<1
                WaterFraction = 1-FalseAlarm;
            else
                WaterFraction = 1;
            end
            PumpDuration = get(o,'PumpDuration') * WaterFraction;
            if length(PumpDuration)>1 && ...
                    ismember('TargetIndices',fields(exptparams.TrialObject))
                InRepTrials=exptparams.InRepTrials;
                TargetIndices=get(exptparams.TrialObject,'TargetIndices');
                ThisTargetIdx=TargetIndices{InRepTrials};
                if length(TrialObjectParms.TargetChannel)==1 && any(ThisTargetIdx>1)
                    TargetChannel=TrialObjectParms.TargetChannel(1);
                else
                    TargetChannel=TrialObjectParms.TargetChannel(ThisTargetIdx);
                end
                rmi = isnan(TargetChannel);
                ThisTargetIdx(rmi)=[]; %remove dummy targets and CatchTargets
                if w.Ntar>1
                        ThisTargetIdx=ThisTargetIdx(CurrentTarIndex);
                elseif w.NtarOrcatch>1
                    ThisTargetIdx(IsCatch(ThisTargetIdx))=[]; 
                end 
                if length(ThisTargetIdx)>1
                    error('ThisTargetIdx contains multiple targets. Somehow this hit matches multiple targets? Must be a bug somewhere...'); 
                end
                if length(PumpDuration)>=ThisTargetIdx
                    PumpDuration=PumpDuration(ThisTargetIdx);
                elseif length(PumpDuration)>1
                    error('PumpDuration is >1 but <ThisTargetIdx. Incorrect number of PumpDuration values?');
                end
                fprintf('Trial # in current rep: %d Target Index: %d  Pump Duration: %.2f\n',...
                    InRepTrials,ThisTargetIdx,PumpDuration);
            else
                PumpDuration=PumpDuration(1);
            end
            
            HitRewardProbability=get(o,'HitRewardProbability');
            if PumpDuration > 0 && ...
                    (HitsSinceLastReward>2/HitRewardProbability || rand<HitRewardProbability),
                HitsSinceLastReward=0;

                ev = IOControlPump (HW,'start',PumpDuration);
                LickEvents = AddEvent(LickEvents, ev, TrialIndex);
                exptparams.Water = exptparams.Water+PumpDuration;
                if strcmpi(get(exptparams.BehaveObject,'TurnOnLight'),'Reward')
                    [~,ev] = IOLightSwitch (HW, 1, get(o,'PumpDuration'),'Start',0,0);
                    LickEvents = AddEvent(LickEvents, ev, TrialIndex);
                end
                if ~strcmpi(get(o,'HitSoundCue'),'none')
                    %ADD sound cue for reward
                    delay = get(o,'HitSoundCueDelay');
                    switch lower(get(o,'HitSoundCue'))
                        case 'ding'
                            [y,fs]=audioread('C:\Windows\Media\ding.wav');
                            intensity=get(o,'HitSoundCueIntensity');
                            y=resample(y,HW.params.fsAO,fs);
                            attend_db=80-intensity-HW.SoftwareAttendB;
                            level_scale=10.^(-attend_db./20);
                            y=y./max(y(:)).*level_scale;
                            str=', playing ding';
                        case 'silence'
                            y=[];
                            str='';
                        otherwise
                            error('HitSoundCue = ''%d'' is not a valid option.',lower(get(o,'HitSoundCue')))
                    end
                    if delay==0
                        fprintf('Stopping sound%s\n',str)
                    else
                        fprintf('Stopping sound%s when target has been playing for %.1f sec\n',str,delay)
                        ts=IOGetTimeStamp(HW);
                        while IOGetTimeStamp(HW)< (LickEvents(1).StartTime + delay)
                            a=2;%pause(0.001);
                        end
                    end
                    ev = IOStopSound(HW);
                    ev2 = IOStartSound(HW,[y;zeros(20*HW.params.fsAO,HW.AO.NumChannels)]);
                    LickEvents = AddEvent(LickEvents, ev, TrialIndex);
                    ev2.Note=['HitSoundCue:',get(o,'HitSoundCue')];
                    LickEvents = AddEvent(LickEvents, ev2, TrialIndex);
                    pause(.4);
                    break
                end
            else
                HitsSinceLastReward=HitsSinceLastReward+1;
                fprintf('p>%.2f: not rewarding hit (Hits since last reward=%d)\n',...
                    HitRewardProbability,HitsSinceLastReward);
                ev = struct('Note','BEHAVIOR,NOPUMP','StartTime',IOGetTimeStamp(HW),'StopTime',[]);
                LickEvents = AddEvent(LickEvents, ev, TrialIndex);
                if strcmpi(get(exptparams.BehaveObject,'TurnOnLight'),'Reward')
                    [~,ev] = IOLightSwitch (HW, 1, get(o,'PumpDuration'),'Start',0,0);
                    LickEvents = AddEvent(LickEvents, ev, TrialIndex);
                end
            end
            if limit_view
                if ~isCue
                    feedback_str={'HIT!',...
                    ['+',num2str(round(PumpDuration*10)),' points, you have ',num2str(round(exptparams.Water*10)),' points total.'],...
                    [num2str(TrialIndex),' Total Trials']};
                end
                if 0
                    feedback_str{1}=[feedback_str{1},' Target ',num2str(TrialObjectParms.TargetIndices{TrialIndex})];
                end
                set(exptparams.feedback_text,'String',feedback_str);
                cla(ax)
                drawnow
            end
            
        end
        got_hit=true;
    end
    
    % CHECK FOR Reminder Hit - Lick in reminder response window
    if (Lick) && w.RemTrial && ~RemHitThisTrial && w.RemResponseWin(1)<CurrentTime && w.RemResponseWin(2)>=CurrentTime,
        RemHitThisTrial = 1;
        fprintf('Reminder Hit (20%% std reward) -- ');
        WaterFraction = 0.2;
        PumpDuration = get(o,'PumpDuration') * WaterFraction;
        if PumpDuration > 0
            ev = IOControlPump (HW,'start',PumpDuration);
            LickEvents = AddEvent(LickEvents, ev, TrialIndex);
            exptparams.Water = exptparams.Water+PumpDuration;
            if strcmpi(get(exptparams.BehaveObject,'TurnOnLight'),'Reward')
                [~,ev] = IOLightSwitch (HW, 1, get(o,'PumpDuration'),'Start',0,0);
                LickEvents = AddEvent(LickEvents, ev, TrialIndex);
            end
        end
    end
    
    
    % FIXED REWARD FOR TRAINING
    % if par.TrainingPumpDur>0 && ~TrainingRewardGiven && ~HitThisTrial
    if isempty(par.TrainingPumpDur)
        tpumpdur = 0;
    else
        tpumpdur = par.TrainingPumpDur;
    end
    if tpumpdur>0 && par.PumpDuration(ThisTargetIdx)>0 && ~TrainingRewardGiven && ~HitThisTrial
        give_auto_reward=false;
        if (~w.NullTrial && ~w.RemTrial && CurrentTime>tar_auto_reward_time)
            fprintf('Giving training auto-reward at %0.2f sec re trial onset\n',tar_auto_reward_time);
            give_auto_reward=true;
        elseif (w.RemTrial && CurrentTime>rem_auto_reward_time)
             fprintf('Giving training auto-reward at %0.2f sec re trial onset\n',rem_auto_reward_time);
             give_auto_reward=true;
        end
        if give_auto_reward
            PumpDuration = par.TrainingPumpDur;
            ev = IOControlPump(HW,'start',PumpDuration);
            ev.Note=strrep(ev.Note,',Pump',',TrainingPump');
            LickEvents = AddEvent(LickEvents, ev, TrialIndex);
            TrainingRewardGiven=1;
        end
    end
    
    if (w.RemTrial && HitThisTrial && CurrentTime>RemStartTime) || CorrectRejectThisTrial,
        disp('Stopping sound before reminder plays');
        break;
    end
    
    CurrentTime = IOGetTimeStamp(HW);
    if globalparams.HWSetup==0,
        pause(0.001);
    end
end
ev=IOStopSound(HW);

if FAThisTrial && PunishFAs,
    PauseTime = ifstr2num(get(o,'TimeOut'));
    if length(PauseTime)==1
        fprintf('FA -- time out ITI %.1f sec\n',PauseTime);
    else
        fprintf('FA -- time out ITI %.1f sec, flashed lights at %d Hz, %.1f duty cycle.\n',PauseTime);
    end
    
elseif HitThisTrial && RewardHits,
    PauseTime = ifstr2num(get(o,'CorrectITI'));
    fprintf('Hit -- ITI %.1f sec\n',PauseTime);
    
elseif HitThisTrial && ~RewardHits,
    PauseTime = ifstr2num(get(o,'CorrectITI'));
    fprintf('Hit -- not rewarded -- ITI %.1f sec\n',PauseTime);
    
elseif CorrectRejectThisTrial,
    RewardNullPause=get(o,'RewardNullPause');
    fprintf('Correct reject/null trial -- pause %.1f sec before pump\n',RewardNullPause);
    pause(get(o,'RewardNullPause'));
    
    PauseTime = ifstr2num(get(o,'CorrectITI'));
    PumpDuration = get(o,'PumpDuration') * NullFrac;
    if PumpDuration
        ev = IOControlPump (HW,'start',PumpDuration);
        LickEvents = AddEvent(LickEvents, ev, TrialIndex);
        exptparams.Water = exptparams.Water+PumpDuration;
    end
    ev.Note='OUTCOME,CR';
    ev.StartTime=IOGetTimeStamp(HW);
    ev.StopTime=ev.StartTime;
    LickEvents=AddEvent(LickEvents,ev,TrialIndex);
    fprintf('Correct reject/null trial -- ITI %.1f sec\n',PauseTime);
    
elseif RewardHits,
    % TRUE MISS
    PauseTime = ifstr2num(par.MissTimeOut);
    ev=[];
    if RemHitThisTrial,
        ev.Note='OUTCOME,REM';
        ev.StartTime=IOGetTimeStamp(HW);
        ev.StopTime=ev.StartTime;
        LickEvents=AddEvent(LickEvents,ev,TrialIndex);
    end
    ev.Note='OUTCOME,MISS';
    ev.StartTime=IOGetTimeStamp(HW);
    ev.StopTime=ev.StartTime;
    LickEvents=AddEvent(LickEvents,ev,TrialIndex);
    if length(PauseTime)==1
        fprintf('Miss -- time out ITI %.1f sec\n',PauseTime);
        else
        fprintf('Miss -- time out ITI %.1f sec, flashed lights at %d Hz, %.1f duty cycle.\n',PauseTime);
    end
    if strcmpi(get(o,'TurnOffLight'),'FalseAlarm_and_Miss') && ~isCue
        if length(PauseTime)==1
            fprintf('Turning off lights\n')
            ev = IOLightSwitch(HW,0,PauseTime,'Start',0,0,'Light2');
        else
            fprintf('Flashing lights\n')
            ev = IOLightSwitch(HW,0,PauseTime(1),'Start',PauseTime(2),PauseTime(3),'Light2');
        end
    end
    if limit_view
        if ~isCue
            feedback_str={'MISS',...
                    ['+0 points, you have ',num2str(round(exptparams.Water*10)),' points total.'],...
                    [num2str(TrialIndex),' Total Trials']};
        end
        set(exptparams.feedback_text,'String',feedback_str);
        cla(ax)
        drawnow
    end
else
    PauseTime = ifstr2num(get(o,'CorrectITI'));
    if RemHitThisTrial,
        ev.Note='OUTCOME,REM';
        ev.StartTime=IOGetTimeStamp(HW);
        ev.StopTime=ev.StartTime;
        LickEvents=AddEvent(LickEvents,ev,TrialIndex);
    end
    fprintf('Correct reject/unrewarded trial -- pausing %.1f sec\n',PauseTime);
end
if limit_view && ~iscell(feedback_str)
    feedback_str={feedback_str,...
        ['+0 points, you have ',num2str(round(exptparams.Water*10)),' points total.'],...
        [num2str(TrialIndex),' Total Trials']};
    set(exptparams.feedback_text,'String',feedback_str);
    cla(ax)
    drawnow
end
if strcmpi(par.ITICarryOverToPreTrial,'No'),
    ThisTime = clock;
    while etime(clock,ThisTime) < PauseTime(1)
        drawnow;
    end
    fprintf('Done pausing\n')
    exptparams.NeutralPreTrialDelay=0;
else
    exptparams.NeutralPreTrialDelay=PauseTime(1);
end

%disp('Block lights off?');
if strcmpi(par.Light1,'OnTarget1Blocks') || strcmpi(par.Light1,'OnTarget2Blocks'),
    [~,ev] = IOLightSwitch(HW, 0, 0,'Stop',0,0,'Light');
end
if strcmpi(par.Light2,'OnTarget1Blocks') || strcmpi(par.Light2,'OnTarget2Blocks'),
    [~,ev] = IOLightSwitch(HW, 0, 0,'Stop',0,0,'Light2');
end
if strcmpi(par.Light3,'OnTarget1Blocks') || strcmpi(par.Light3,'OnTarget2Blocks'),
    [~,ev] = IOLightSwitch(HW, 0, 0,'Stop',0,0,'Light3');
end

exptparams.temporary.BehaviorEvents=LickEvents;

end

function do_skip(hObject, eventdata, handles)
    global SkipThisTrial
    SkipThisTrial=true;
    fprintf('Set SkipThisTrial to True\n')
end

function do_play_cue(hObject, eventdata, handles)
    global PlayCue
    PlayCue=true;
    fprintf('Set PlayCue to True\n')
end
