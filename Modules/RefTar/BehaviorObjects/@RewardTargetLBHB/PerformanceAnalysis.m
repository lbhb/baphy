function exptparams = PerformanceAnalysis (o, HW, StimEvents, globalparams, exptparams, TrialIndex, LickData)
% Performance analaysis for RewardTargetLBHB behavior object
%
% EarlyResponse: lick during the Early window after target. Early response causes the trial to stop immediately and a timeout.
% Hit: lick during Response window after the target. This is a correct response to the target and the water reward will be given.
% Miss: no lick during Response window after the target. A timeout will be given.
% FalseAlarm: lick during corresponding Response window for a reference.
% IneffectiveTrial: a trial the animal licks to all the references. In other word, a trial has a False alarm rate of 1. The trial will be stopped right before a target appears.
% InstructionTrial: a trial which instruct the animal stopping lick to a reference sound. It repeats play a reference sequence with an interval (Early window + Response window) until the animal stop lick to 2 successive references. Instruction trail is triggered when a Ineffective trial detected.
% ReferenceLickTrial: a trial the animal lick during reference period (before the target appears).
% HitRate: total number of False Alarm divides by total number of warning trials.
% EarlyRate: total number of Early response divides by total number of warning trials.
% MissRate: total number of missed trials divides by total number of warning trials.
% FalseAlarmRate: total number of False Alarm divides by total number of references across all warning trials.

% Nima, April 2006

% Trial-object dependent code:
%Line 48

trialparms=get(exptparams.TrialObject);

par=get(o);
if isfield(exptparams,'TrialParams') && exptparams.TrialParams(TrialIndex).CueSeg==1
    if ~isempty(par.CueModifications)
        eval(par.CueModifications);
    end
end

%% figure out trial number
if isfield(exptparams, 'Performance')
    perf = exptparams.Performance(1:end-1);
    tnum = length(perf) + 1;
else
    tnum = 1;
end

%% generate lists of behaviorally relevant event windows
if getparm(exptparams,'OfflineAnalysis',0)
   [w,exptparams] = GenerateEventWindows(o,exptparams,StimEvents,par,{'StimBeahviorPumpWindows','PumpActualWindows'});
    exptparams.temporary.EventWindows = w;
else
    w = exptparams.temporary.EventWindows; % Created in CanStart
    [PumpActualWindows,~] = GenerateEventWindows(o,exptparams,StimEvents,par,{'PumpActualWindows'});
    w.TrainingPumpWinActual = PumpActualWindows.TrainingPumpWinActual;
    w.PumpWinActual = PumpActualWindows.PumpWinActual;
end

%% TrialObject-specific code
%% Define FAStartTime, the time after which a lick is considered a FA
%w.two_target=iscell(trialparms.TarIdx) && any(cellfun(@length,trialparms.TarIdx)>1);
w.two_target=(isfield(trialparms,'TargetDistSet') && any(trialparms.TargetDistSet>1)) || strcmp(trialparms.ReferenceClass,'NaturalStreams');

switch exptparams.TrialObjectClass
    case {'MultiRefTar','RSSToneMono'}
        % "strict" - FA is response to any possible target slot preceeding the target
        TarPreStimSilence=get(trialparms.TargetHandle,'PreStimSilence');
        if trialparms.SingleRefSegmentLen>0
            PossibleTarTimes=(find(trialparms.ReferenceCountFreq(:))-0).*...
                trialparms.SingleRefSegmentLen+w.FirstRefTime;
            if w.two_target
                PossibleTar2Offsets=(find(trialparms.Tar2SegCountFreq(:))).*...
                    trialparms.Tar2SegmentLen+w.FirstRefTime;
                PossibleTar2Times=repmat(PossibleTarTimes,1,length(PossibleTar2Offsets))...
                    + repmat(PossibleTar2Offsets',length(PossibleTarTimes),1);
                PossibleTarTimes=[PossibleTarTimes; PossibleTar2Times(:)];
            end
        else
            RefSegLen=get(trialparms.ReferenceHandle,'PreStimSilence')+...
                get(trialparms.ReferenceHandle,'Duration')+...
                get(trialparms.ReferenceHandle,'PostStimSilence');
            PossibleTarTimes=(find(trialparms.ReferenceCountFreq(:))-0).*...
                RefSegLen;
        end
        FAStartTime=min(PossibleTarTimes);
        if any(FAStartTime>w.TarOrCatchEarlyWin)
            warning('FAStartTime>Tartime?????');
            keyboard
            warning('Kludge Alert! Shifting FA start time to handle last trial in block that is too short!');
            FAStartTime=min(w.TarOrCatchEarlyWin);
        end
    case 'ReferenceTarget'
        FAStartTime = getparm(trialparms.ReferenceHandle, 'MinimumTargetTime',[]);
        if isempty(FAStartTime)
            FAStartTime=get(trialparms.ReferenceHandle,'PreStimSilence');
        end
    otherwise
        FAStartTime=get(trialparms.ReferenceHandle,'PreStimSilence');
end

%% Preprocess LickData
LickData = PreprocessLickData(o,LickData);

UnalteredLickData=LickData;

if strcmpi(getparm(par, 'PunishFA', 'Yes'), 'No')
    % svd 2019-04: For tasks where FA is not punished
    TarResponseBin=min(length(LickData),round(fs*TarOrCatchResponseWin));
    LickData(1:TarResponseBin(1))=0;
    %figure;
    %plot(LickData);
    %hold on
    %plot(TLickData+1.1);
    %ylim([-0.1 2.2]);
end

LicksRequired = getparm(par, 'LicksRequired',1);
if length(LicksRequired)>1
    LicksRequired = par.LicksRequired(tnum);
end

% svd 2015-01-21  :  only care about first lick time now?!??!
% svd 2019-09-13  : now we do care about later licks
% LAS 2019-10-09  : Only if LicksRequired is greater than 1
if LicksRequired<2
    ff=find(LickData, 1);
    if ~isempty(ff)
        LickData((ff+1):end)=0;
    end
    ff=find(UnalteredLickData, 1);
    if ~isempty(ff)
        UnalteredLickData((ff+1):end)=0;
    end
end

% By default, LickData has has only the first lick
% If PunishFA is 'No', Licks before the first tar or catch are removed
% If LicksRequired>=2, LickData has all licks

% If LicksRequired<2, UnalteredLickData has only the first lick
% UnalteredLickData doesn't have pre-tar/catch licks removed
% If LicksRequired>=2, UnalteredLickData has all licks

%% Analyze lick vector to find out when animal licked with respect to target, call di_nolick
fs = HW.params.fsAI;
w.Check_both_FA_ET=true;
exptparams = ComputePerformance_from_Windows_and_Licks(o,exptparams,par,w,FAStartTime,LickData,UnalteredLickData,fs,tnum);
if isfield(exptparams,'Water')
    exptparams.Performance(end).Water = exptparams.Water .* globalparams.PumpMlPerSec.Pump;
end