function o = ObjUpdate (o)
% this can be used for validating the settings.
    switch o.ResponseMethod
        case 'LickSpout' %user-friendly name for "Touch" update to name used by a bunch of baphy functions
            o.ResponseMethodVar='Touch';
        case {'Touch','LeverPress','LickVideo'}
            o.ResponseMethodVar=o.ResponseMethod;
        otherwise
            error('Response Method "%s" Unknown',o.ResponseMethod)
    end
    
    switch o.Simulation
        case 'No'
            o.IsSimulation=false;
        otherwise
            o.IsSimulation=true;
            o.SimulatedSubjectObject=feval(o.Simulation);
    end