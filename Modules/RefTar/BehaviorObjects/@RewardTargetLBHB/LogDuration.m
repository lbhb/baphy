function Duration = LogDuration (o, HW, StimEvents, globalparams, exptparams, TrialIndex);
% LogDuration method for ReferenceAvoidance object
% the log duration is the end of the sound plus response time

EndTimes = cat(1,StimEvents.StopTime);
Duration = max(EndTimes) + o.ResponseWindow + o.EarlyWindow + 0.05;
