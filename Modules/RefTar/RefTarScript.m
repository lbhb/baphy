
function varargout = RefTarScript (globalparams, exptparams, HW)
% This function is the main script for Reference Target module
% The function reads the ReferenceTarget object from config/ReferenceTargetObject
% file and setup the experiment based on the parameters of the object. This object has been
% saved from RefTarGui. This includes:
%   Initializing the trial sequence
%   Looping over the trials
%   Putting out the sound and gathering the data
%   Analysing the lick and shocking the animal
%   Returning the event structure to the main program.
%
% Nima Mesgarani, November 2005
% BE, modified & polished, 2011/7
% SVD, added NIDAQMX support 2012/05

global StopExperiment; StopExperiment = 0; % Corresponds to User button
global exptparams_Copy; exptparams_Copy = exptparams; % some code needs exptparams
global BAPHY_LAB LoudnessAdjusted
global SAVEPUPIL SAVEPHOTOMETRY SAVELICK

BehaveObject = exptparams.BehaveObject;
IsSimulation=false;
try
    IsSimulation=get(BehaveObject,'IsSimulation');
    exptparams.TrialObject=set(exptparams.TrialObject,'IsSimulation',IsSimulation);
end
if IsSimulation && HW.params.HWSetup>0
    error('Use Hardware Setup "0: Test" when running a simulation.')
end
if strcmpi(exptparams.BehaveObjectClass,'MriPassive') % && get(BehaveObject,'DelayAfterScanTTL')==0
   disp('MriPassive: press a key to synchronize with start of MRI scan ...');
   pause
end

exptevents = []; ContinueExp = 1; exptparams.TotalRepetitions = 0; TrialIndex = 0;
exptparams.StartTime = clock; exptparams.Water = 0;
laststoptime=now;

% ADD DESCRIPTIVE COMMENTS
exptparams.comment = [...
  'Experiment: ',class(exptparams.BehaveObject),' ',...
  'TrialObject: ',class(exptparams.TrialObject),' ',...
  'Reference Class: ',get(exptparams.TrialObject, 'ReferenceClass'),' ',...
  'Target Class: ',get(exptparams.TrialObject, 'TargetClass')];

if(0)
    CueTarget(globalparams, exptparams, HW)
end

TrialStartTimes=nan(1000,1);

IOLickRead(HW,[],1)
% again, not sure if we need the ResponeMethodVar anymore
if SAVELICK && isfield(get(BehaveObject),'ResponseMethodVar') && strcmpi(get(BehaveObject,'ResponseMethodVar'),'LickVideo')
    %Setup callback for video lick detector to send lick signal over TCPIP
    callbackfcn=@(hObject,eventdata)LickTCPIP(BehaveObject,hObject,eventdata);
    status = IOReceiveMessageTCPIP(HW.Lick,callbackfcn);
end

%% MAIN LOOP
exptparams.TotalTrials = 0;
while ContinueExp == 1
  if exist('iRep')==0
    exptparams.TrialObject = ObjUpdate(exptparams.TrialObject);
    TrialIndexLst = 1:(exptparams.TrialBlock*exptparams.Repetition);    % List of trial nb sent to <waveform>; modified during reinsertion
     if (isfield(struct(exptparams.TrialObject),'TrialIndexLst') && isempty(get(exptparams.TrialObject,'TrialIndexLst'))); exptparams.TrialObject = set(exptparams.TrialObject,'TrialIndexLst',TrialIndexLst); end
  elseif isfield(struct(exptparams.TrialObject),'TrialIndexLst')
    TrialIndexLst = get(exptparams.TrialObject,'TrialIndexLst');
    TrialIndexLst = [ TrialIndexLst , max(TrialIndexLst) + (1:(exptparams.TrialBlock*exptparams.Repetition)) ];    % List of trial nb sent to <waveform>; modified during reinsertion
     exptparams.TrialObject = set(exptparams.TrialObject,'TrialIndexLst',TrialIndexLst);
  end  
 
  iRep = 0;
  
  while iRep < exptparams.Repetition; % REPETITION LOOP
    iRep = iRep+1;
    if ~ContinueExp, break; end
    % AT BEGINNING OF EACH REPETITION, RANDOMIZE SEQUENCE OF INDICES
    % via RandomizeSequence method of the Trial Object with flag = 1 (Repetition Call)
    exptparams = RandomizeSequence(exptparams.TrialObject, exptparams, globalparams, iRep, 1);
    
    iTrial=0;
    while iTrial<get(exptparams.TrialObject,'NumberOfTrials') % TRIAL LOOP
      TrialIndex = TrialIndex + 1; % MAIN TRIAL COUNTER
      iTrial = iTrial+1;  % TRIAL COUNTER WITHIN REPETITION
      exptparams.InRepTrials = iTrial;
      exptparams.TotalTrials = TrialIndex;
      fprintf('----Trial %d -----------------------------------------------------\n',TrialIndex)
      %% PREPARE TRIAL
      TrialObject = get(exptparams.TrialObject);
      % 2013/12 YB: VISUAL DISPLAY--Back to grey screen on the second monitor if we are in a psychophysics experiment
      if isfield(TrialObject,'VisualDisplay') && TrialObject.VisualDisplay; 	[VisualDispColor,exptparams] = VisualDisplay(TrialIndex,'GREY',exptparams); end
        
      %Create pump control
      if isfield(TrialObject,'PumpProfile')
          PumpProfile = TrialObject.PumpProfile;
          handles = guihandles(WaterPumpControl(PumpProfile, TrialIndex));
          PumpProfile = str2num(get(handles.edit1,'string'));
          exptparams.TrialObject = set(exptparams.TrialObject,'PumpProfile',PumpProfile);
      end
      
      % Yves; 2013/11: I added an input to 'waveform' methods
      if any(strcmp(fieldnames(exptparams.TrialObject),'TrialIndexLst'))
        [TrialSound, StimEvents, exptparams.TrialObject] = waveform(exptparams.TrialObject, iTrial,TrialIndexLst(TrialIndex));
      else
        [TrialSound, StimEvents, exptparams.TrialObject] = waveform(exptparams.TrialObject, iTrial);
      end
      [HW,globalparams,exptparams] = LF_setSamplingRate(HW,globalparams,exptparams);
      Attenuation=80-get(exptparams.TrialObject, 'OveralldB');
      try
          CueSeg=get(exptparams.TrialObject,'CueSeg');
          if CueSeg(iTrial)==1
              CM=get(exptparams.TrialObject,'CueModifications');
              if ~isempty(CM) && contains(CM,'OveralldB')
                  ind=strfind(CM,'OveralldB=');
                  ind_semicolon=strfind(CM(ind:end),';');
                  if isempty(ind_semicolon), ind_semicolon=length(CM)+1; end
                  OveralldB=str2double(CM(ind+10:ind_semicolon(1)+ind-2));
                  Attenuation=80-OveralldB;
              end
          end
      end
      HW = IOSetLoudness(HW, Attenuation);
      
      % CONSTRUCT FILENAME FOR MANTA RECORDINGS (ONLY USED IN MANTA SETUP)
      HW.Filename = M_setRawFileName(globalparams.mfilename,TrialIndex);

      % GET & SET LOGGING DURATION FOR PRESENT TRIAL
      exptparams.LogDuration = LogDuration(BehaveObject, HW, ...
        StimEvents, globalparams, exptparams, TrialIndex);
      LogSteps = exptparams.LogDuration*HW.params.fsAI;
      IOSetAnalogInDuration(HW,exptparams.LogDuration);
      
      % MATCH SOUND AND ACQUISITION DURATION
      TrialSound(floor(exptparams.LogDuration.*HW.params.fsAO),end) = 0;
      
      exptparams = GUIUpdateStatus (globalparams, exptparams, TrialIndex, iTrial); drawnow;
      
      % CHECK WHETHER TRIAL CAN BE STARTED
      BehaviorEvents = 1; % Just initialize
      while BehaviorEvents
        [BehaviorEvents,exptparams] = ... % CHECK PRETRIAL CONDITION
          CanStart(BehaveObject, HW, StimEvents, globalparams, exptparams, TrialIndex);
        if StopExperiment ContinueExp = ContinueOrNot(HW); end; % USER PRESSED STOP
        if ~ContinueExp break; end
      end; if ~ContinueExp, TrialIndex = TrialIndex - 1; break; end
      
      % svd 2012-10-27: moved IOLoadSound after CanStart to allow sounds to
      % be played during CanStart prior to beginning of the aquisition
      % period of the trial. Shouldn't cause any serious changes in timing
      %using the 2nd SOUNDOUT as pumpcontrol by PY @ 9-2/2012
      if IsSimulation
          exptparams.BehaveObject.SimulatedSubjectObject.TrialSound=TrialSound;
      else
          if strcmpi(BAPHY_LAB,'nsl') && globalparams.HWSetup==3 
            if size(TrialSound,2)==2
              HW = IOLoadSound(HW, TrialSound(:,[2 1]));
            else
              HW = IOLoadSound(HW, TrialSound(:,[1 1]));
            end      
          elseif strcmp( class(BehaveObject) , 'RewardTargetContinuous' )

          else
              %HW = IOLoadSound(HW, TrialSound(:,1:2));
              HW = IOLoadSound(HW, TrialSound);
          end
      end

      % force at least 500 ms pause between trials in SPR2
      if ~niIsDriver(HW) && (HW.params.HWSetup == 3 || HW.params.HWSetup == 11),
        while str2num(datestr(now-laststoptime,'SS.FFF'))<0.1,
          pause(0.05);
          fprintf('.');
        end
      end
      
      if SAVEPUPIL
          MSG = ['GETVAR',HW.Pupil.COMterm,...
              '[now,MG.frames_written]',HW.Pupil.MSGterm];
          RESP=[];
          while isempty(RESP) || int8(RESP(1))==76
              % Keep trying to get responses (avoids missed communication causing errors)
              RESP=IOSendMessageTCPIP(HW.Pupil,MSG);
          end
          PupilEvent=AddEvent([],['PUPIL,',RESP],TrialIndex,0,0);
          RESP=str2num(RESP);
          fprintf('Remote time: %s, frames: %d\n',datestr(RESP(1)),RESP(2));
      else
          PupilEvent=[];
      end
      if SAVELICK
          MSG = ['GETVAR',HW.Lick.COMterm,...
              '[now,MG.frames_written]',HW.Lick.MSGterm];
          RESP=[];
          while isempty(RESP) || int8(RESP(1))==76
              % Keep trying to get responses (avoids missed communication causing errors)
              RESP=IOSendMessageTCPIP(HW.Lick,MSG);
          end
          LickEvent=AddEvent([],['LICK,',RESP],TrialIndex,0,0);
          RESP=str2num(RESP);
          fprintf('Remote time: %s, frames: %d\n',datestr(RESP(1)),RESP(2));
      else
          LickEvent=[];
      end
      if SAVEPHOTOMETRY
          MSG = ['GETVAR',HW.Photometry.COMterm,...
              '[now,MG.frames_written]',HW.Photometry.MSGterm];
          RESP=IOSendMessageTCPIP(HW.Photometry,MSG);
          PhotoEvent=AddEvent([],['PHOTOMETRY,',RESP],TrialIndex,0,0);
          RESP=str2num(RESP);
          fprintf('Remote time: %s, frames: %d\n',datestr(RESP(1)),RESP(2));
      else
          PhotoEvent=[];
      end
      
      if(strcmp(HW.params.DAQSystem,'Open-Ephys'))
          zeroMQrr('Send',HW.OEP.url,['TrialStart ',num2str(TrialIndex)]);  
      end
        
      %% MAIN ACQUISITION SECTION
      %HW.params.trigger_length=.025; LAS Fall 2017: Use this to specify a trigger length (needed for OIS recording with Roe lab). Make a GUI option later if needed.
      if ~strcmp( class(BehaveObject) , 'RewardTargetContinuous' )  % Acquisition starts within BehaviorControl.m
        if IsSimulation 
            StartEvent.Note=['TRIALSTART , ' datestr(now,'yyyy-mm-dd HH:MM:SS.FFF')];
            StartEvent.StartTime=0;
            StartEvent.StopTime=0;
            HW.params.StartClock=clock;
        else
            [StartEvent,HW] = IOStartAcquisition(HW);
        end
      end
      
      % HAND CONTROL TO LICK MONITOR TO CONTROL REWARD/SHOCK
      [BehaviorEvents, exptparams] = ...
        BehaviorControl(BehaveObject, HW, StimEvents, globalparams, exptparams, TrialIndex);
      
      % STOP ACQUISITION
      [TrialStopEvent,HW] = IOStopAcquisition(HW);
      % IF THERE WAS A PROBLEM ON THE ACQUISITION SIDE, RERUN LAST TRIAL
      if strcmp(HW.params.DAQSystem,'MANTA') &&...
          isfield(HW.MANTA,'RepeatLast') && HW.MANTA.RepeatLast
        TrialIndex = TrialIndex - 1; iTrial = iTrial - 1; HW.MANTA.RepeatLast = 0; continue;
      end
      
      laststoptime=now;
      
      %% COLLECT TRIAL INFORMATION: EVENTS, RESPONSES, MICROPHONE
      exptevents = AddMultiEvent(exptevents,{PupilEvent,PhotoEvent,LickEvent,StartEvent,StimEvents,BehaviorEvents,TrialStopEvent},TrialIndex);
      
      % COLLECT ANALOG CHANNELS
      [Data.Aux, Data.Spike, AINames] = IOReadAIData(HW); RespIndices = [];
      
      for i=1:length(AINames)
        Data.(AINames{i}) = Data.Aux(:,i);
        if isfield(get(BehaveObject),'ResponseMethodVar')
            if strcmpi(AINames{i},get(BehaveObject,'ResponseMethodVar')) RespIndices(end+1) = i; end
        else
            if strcmpi(AINames{i}(1:min(end,5)),'Touch') RespIndices(end+1) = i; end
        end
        if strcmpi(AINames{i}(1:min(end,4)),'walk') RespIndices(end+1) = i; end
      end
      if ~IsSimulation
          if isfield(get(BehaveObject),'ResponseMethodVar') &&... 
             strcmpi('LeverPress',get(BehaveObject,'ResponseMethodVar')) &&...
             any(strcmp(AINames,'Touch'))
                 RespIndices(end+1)=find(strcmp(AINames,'Touch'));
          end
          Data.Responses = Data.Aux(:,RespIndices);
          if any(RespIndices) && length(BehaviorEvents)>1 && strcmp(BehaviorEvents(2).Note,'BEHAVIOR,PUMPON,Pump') && BehaviorEvents(2).StartTime<.1 && any(Data.Responses(:,1)==0)
              sti=BehaviorEvents(1).StartTime*HW.params.fsAI;
              AIst=find(Data.Responses(:,1),1)/HW.params.fsAI;
              Data.Responses(1:sti,1)=0;
              fprintf('AI too fast? Changing bar release time from %f to %f based on timestamp from BehaviorControl.\n',AIst,BehaviorEvents(1).StartTime)
          end
          if any(RespIndices) && ~isempty(BehaviorEvents) && all(Data.Responses(:,1)==1) && strcmp(BehaviorEvents(1).Note,'LICK')
              sti=BehaviorEvents(1).StartTime*HW.params.fsAI;
              Data.Responses(1:sti,1)=0;
              fprintf('AI too slow to detect early bar release. Setting to %f based on timestamp from BehaviorControl.\n',BehaviorEvents(1).StartTime)
          end
          exptparams.RespSensors = AINames(RespIndices);
      end
      %Data.Microphone = [];
      
      % LICK DATA FOR TEST MODE
      if ~HW.params.HWSetup
          t=evtimes(exptevents,'LICK',TrialIndex);
          Data.Responses = zeros(round(single(LogSteps)),1);
          Data.Responses(round(t.*HW.params.fsAux))=1;
          %Data.Responses = double(rand(single(LogSteps),1)<0.005); 
      end
      
      if SAVEPUPIL
          % record frames collected by pupil monitor at the end of trial
          MSG = ['GETVAR',HW.Pupil.COMterm,'[now,MG.frames_written]',HW.Pupil.MSGterm];
          RESP=[];
          while isempty(RESP) || int8(RESP(1))==76
              % Keep trying to get responses (avoids missed communication causing errors)
              RESP=IOSendMessageTCPIP(HW.Pupil,MSG);
          end
          if strcmp(RESP,'SIZE must be greater than 0.')
              warning('Pupil returned "SIZE must be greater than 0." instead of a number for the number of frames. Fix before continuing or baphy will error below.')
              keyboard
          end
          PupilEvent=AddEvent([],['PUPILSTOP,',RESP],TrialIndex,0,0);
          RESP=str2num(RESP);
          fprintf('PUPILSTOP. Remote time: %s, frames: %d\n',datestr(RESP(1)),RESP(2));
          exptevents = AddMultiEvent(exptevents,{PupilEvent},TrialIndex);
      end
      if SAVEPHOTOMETRY
          % record frames collected by pupil monitor at the end of trial
          MSG = ['GETVAR',HW.Photometry.COMterm,'[now,MG.frames_written]',HW.Photometry.MSGterm];
          RESP=IOSendMessageTCPIP(HW.Photometry,MSG);
          PhotoEvent=AddEvent([],['PHOTOSTOP,',RESP],TrialIndex,0,0);
          RESP=str2num(RESP);
          fprintf('PHOTOSTOP. Remote time: %s, frames: %d\n',datestr(RESP(1)),RESP(2));
          exptevents = AddMultiEvent(exptevents,{PhotoEvent},TrialIndex);
      end
      if SAVELICK
          % record frames collected by pupil monitor at the end of trial
          MSG = ['GETVAR',HW.Lick.COMterm,'[now,MG.frames_written]',HW.Lick.MSGterm];
          RESP = [];
          while isempty(RESP) || int8(RESP(1))==76
              % Keep trying to get responses (avoids missed communication causing errors)
              RESP=IOSendMessageTCPIP(HW.Lick,MSG);
          end
          LickEvent=AddEvent([],['LICKSTOP,',RESP],TrialIndex,0,0);
          RESP=str2num(RESP);
          fprintf('LICKSTOP. Remote time: %s, frames: %d\n',datestr(RESP(1)),RESP(2));
          exptevents = AddMultiEvent(exptevents,{LickEvent},TrialIndex);
          % not actually sure it makes sense to have the ResponseMethodVar
          % anymore, since now we have the checkbox in main GUI? crh
          if isfield(get(BehaveObject),'ResponseMethodVar') && strcmpi(get(BehaveObject,'ResponseMethodVar'),'LickVideo') 
                Data = LickVideo_get_trace(BehaveObject,HW,Data);
          end
      end
      
      % breaking hardware abstraction rule! Fix this!  SVD 2012-05-31
      % this is to deal with the fact that we have to leave the AI task
      % running in order to read in the AI data.
      if strcmpi(IODriver(HW),'NIDAQMX'),
          %disp('Stopping AI task');
          niStop(HW);
      end
      % IF COMMUNICATING WITH MANTA
      if strcmp(HW.params.DAQSystem,'MANTA')
          MSG = ['STOP',HW.MANTA.COMterm,HW.MANTA.MSGterm];
          [RESP,HW] = IOSendMessageManta(HW,MSG,'STOP OK','',1);
      end
      
      if ismethod(exptparams.TrialObject,'StoreTrialParams')
          %exptparams=StoreTrialParams(exptparams.TrialObject, exptparams, iRep);
          exptparams=StoreTrialParams(exptparams.TrialObject, exptparams, exptparams.TotalRepetitions+1);
      end
      
      if ~strcmp( class(BehaveObject) , 'RewardTargetContinuous' )  && ~IsSimulation
          time=strsep(StartEvent.Note,',');
          time=datenum(time{2},'yyyy-mm-dd HH:MM:SS.FFF');
          if exptparams.TotalTrials==1 %% iRep == 1 && iTrial==1 LAS: this olf stuff doesn't work if you get out to the outer loop again.
              RunStartTime=time;
          end
          TrialStartTimes(TrialIndex)=sum([0 0 0 60 1 1/60] .* datevec(time - RunStartTime));
      end
      
      % DISPLAY PERFORMANCE DATA IN RefTarGui
      % CRH added "exptparams."BehaveObject. BehaveObject gets updated
      % during call to Behavior Control above, and this need to be passed
      % on into PerformanceAnalysis
      exptparams = PerformanceAnalysis(exptparams.BehaveObject, HW, StimEvents, ...
        globalparams, exptparams, TrialIndex, Data.Responses);
      exptparams = GUIUpdateStatus(globalparams, exptparams, TrialIndex, iTrial);

      % PLOT BEHAVIOR ANALYSIS
      exptparams = BehaviorDisplay(BehaveObject, HW, StimEvents, globalparams, ...
          exptparams, TrialIndex, Data.Responses, TrialSound,TrialStartTimes(1:TrialIndex));
      
      % SAVE LICK/TOUCH/MICROPHONE DATA TO EVP FILE
      if ~isempty(globalparams.mfilename) && ~IsSimulation,
          R=Data.Responses;
          if isfield(Data,'Microphone')
              MicScale=0.1 * ((2^16)/2 - 1); %Assume it's currently -10 to 10, scale to fit in short range (-32,768 to 32,767).
              R=[R round(Data.Microphone*MicScale)];
          end
          evpwrite(globalparams.localevpfile,Data.Spike,R,HW.params.fsSpike,HW.params.fsAux);
      end
      
      % DISPLAY MICROPHONE WAVEFORM
      if strcmpi(exptparams.OnlineWaveform,'Yes') && isfield(Data,'Microphone')
          exptparams=LF_showMic(Data.Microphone,exptparams,HW,TrialSound); 
      end
      
      % CHECK WHETHER TO STOP
      if (~mod(TrialIndex, exptparams.TrialBlock) ...  % end of trialblock
          && ~isempty(strfind(globalparams.Physiology,'No')) ... % but no physiology
          && ~exptparams.ContinuousTraining) ...      % and no continuous training
          || StopExperiment, % stop button pressed
        ContinueExp = ContinueOrNot(HW);
        if ~ContinueExp, break; end
      end
      
      if strcmpi(BAPHY_LAB,'lbhb') && ~mod(TrialIndex,20) && ...
              (globalparams.HWSetup==0 || iTrial<get(exptparams.TrialObject,'NumberOfTrials')) &&...
              ~isempty(globalparams.mfilename),
          fprintf('Saving parmfile for safety.\n');
          WriteMFile(globalparams,exptparams,exptevents,1);
      end
      
      %% RANDOMIZE WITH FLAG 0 (TRIAL CALL)
      % Used in adaptive schemes, where trialset is modified based on animals performance
      % Needs to change NumberOfTrials and Modify the IndexSets
      exptparams = RandomizeSequence(exptparams.TrialObject, exptparams, globalparams, iRep, 0);
      if any(strcmp(fieldnames(exptparams.TrialObject),'TrialIndexLst')); TrialIndexLst = get(exptparams.TrialObject,'TrialIndexLst'); end
      
    end % END OF TRIAL LOOP
    exptparams.TotalRepetitions = exptparams.TotalRepetitions + 1;
    
    %% FINISH UP REPETITION
    % TELL MANTA TO SAVE ONLINE SPIKETIMES
    if strcmp(HW.params.DAQSystem,'MANTA')
        MSG = ['SETVAR',HW.MANTA.COMterm,...
            'M_saveSpiketimes; ',HW.MANTA.MSGterm];
        IOSendMessageManta(HW,MSG,'SETVAR OK');
    end
    
    % WRITE LOCAL FILE TO TEMP AND WRITE M-FILE
    if ~isempty(globalparams.mfilename) && exist(globalparams.localevpfile,'file')
      if strcmp(HW.params.DAQSystem,'MANTA') || strcmp(HW.params.DAQSystem,'Open-Ephys') || ...
              strcmp(globalparams.Physiology,'No'),
        copyfile(globalparams.localevpfile, globalparams.evpfilename);
      else
        % when acquiring data with A-O, save evp to tmp and let flush take
        % care of generating the final evp.
        copyfile(globalparams.localevpfile, globalparams.tempevpfile);
      end
    end
    
    % WRITE MFILE
    if ~isempty(globalparams.mfilename),
      WriteMFile(globalparams,exptparams,exptevents,1);
    else
      disp('TEST MODE. Not saving mfile');
    end
  end % END OF REPETITION LOOP
  
  if HW.params.HWSetup ~= 0 %Checking if using Test mode
    if ~isempty(exptparams.Repetition) && iRep==exptparams.Repetition && ContinueExp
        if isempty(strfind(globalparams.Physiology,'No'))
            ContinueExp = ContinueOrNot(HW);  % END OF REPETITION : CHECK IF USE WANTS TO CONTINUE
        else
            sql=['SELECT species FROM gAnimal WHERE animal=''',globalparams.Ferret,''';'];
            s=mysql(sql);
            if strcmp(s.species,'human') && isfield(get(exptparams.TrialObject),'SelfTestMode') && strcmp(get(exptparams.TrialObject,'SelfTestMode'),'Yes')
                ContinueExp = ContinueOrNot(HW);
            end
        end
    end
  else
    if (iRep==exptparams.Repetition) && ContinueExp
      ContinueExp = ContinueOrNot(HW);  % END OF REPETITION : CHECK IF USE WANTS TO CONTINUE
    end
  end
end % CHECK FOR CONTINUING EXPERIMENT
if ~IsSimulation, IOLickRead(HW,[],-1); end
exptparams.StopTime = clock;
% svd commented out line 2014-12-01 because it isn't needed any more?
% if ~isfield(exptparams,'volreward') exptparams.volreward = exptparams.Water; end

%% POSTPROCESSING
switch HW.params.DAQSystem
    case 'MANTA'
        MSG = ['RUNFUN',HW.MANTA.COMterm,'M_startEngine; ',HW.MANTA.MSGterm];
        IOSendMessageManta(HW,MSG,'');
    case 'Open-Ephys'
        zeroMQrr('Send',HW.OEP.url,'StopRecord')
end

% Tell pupil system to stop
if SAVEPUPIL && isfield(HW,'Pupil'),
    MSG = ['STOP',HW.Pupil.COMterm,HW.Pupil.MSGterm];
    [RESP,HW.Pupil] = IOSendMessageTCPIP(HW.Pupil,MSG,'STOP OK','',1);
end
if SAVEPHOTOMETRY && isfield(HW,'Photometry')
    MSG = ['STOP',HW.Photometry.COMterm,HW.Photometry.MSGterm];
    [RESP,HW.Photometry] = IOSendMessageTCPIP(HW.Photometry,MSG,'STOP OK','',1);
end
if SAVELICK && isfield(HW,'Lick')
    MSG = ['STOP',HW.Lick.COMterm,HW.Lick.MSGterm];
    [RESP,HW.Lick] = IOSendMessageTCPIP(HW.Lick,MSG,'STOP OK','',1);
end


% GET AMOUNT OF WATER GIVEN
if ~isfield(exptparams,'WaterUnits') || strcmp(exptparams.WaterUnits,'seconds')
  exptparams.Water = exptparams.Water .* globalparams.PumpMlPerSec.Pump;
end

% UPDATE DISPLAY WITH WATER AND SOUND
exptparams = BehaviorDisplay(BehaveObject, HW, StimEvents, globalparams, exptparams, TrialIndex, [], TrialSound,TrialStartTimes(1:TrialIndex));

% MAKE SURE PUMP, SHOCK AND LIGHT ARE OFF
if ~strcmpi(exptparams.BehaveObjectClass,'MriPassive'),
    try IOControlPump(HW,'stop'); IOControlShock(HW,0,'stop'); IOLightSwitch(HW,0); end
end

% SEND PARAMETERS & DATA TO DATABASE
LF_writetoDB(globalparams,exptparams)

% WRAP UP DISPLAY & RETURN VALUES
close(exptparams.FigureHandle);
if isfield (exptparams,'FigureHandle'), exptparams = rmfield(exptparams,'FigureHandle');end
if isfield (exptparams,'TempDisp'),     exptparams = rmfield(exptparams,'TempDisp');end
if isfield (exptparams,'wavefig'),      exptparams = rmfield(exptparams,'wavefig');end
varargout{1} = exptevents; varargout{2} = exptparams;

%%
%%======== LOCAL FUNCTIONS =======================================
%%
function ContinueExp = ContinueOrNot(HW)
global StopExperiment;
FP = get(0,'DefaultFigurePosition');
MP = get(0,'MonitorPosition');
SS = get(0,'ScreenSize');
set(0,'DefaultFigurePosition',[10,MP(4)/2-SS(2),FP(3:4)]);
%UserInput = questdlg('Continue the experiment?');
UserInput='ding';
while strcmpi(UserInput, 'ding')
    UserInput = questdlg('Continue the experiment?','Continue?','Yes','No','ding','No');
    if strcmpi(UserInput, 'ding')
        PlayDing([],[],HW);
    end
end
set(0,'DefaultFigurePosition',FP);
ContinueExp = strcmpi(UserInput, 'Yes') | strcmpi(UserInput,'Cancel');
if ContinueExp==1, StopExperiment = 0; end

function [HW,globalparams,exptparams] = LF_setSamplingRate(HW,globalparams,exptparams)
if strcmpi(exptparams.OnlineWaveform,'Yes')
  HW.params.fsAI = get(exptparams.TrialObject, 'SamplingRate');
  HW.params.fsAux = HW.params.fsAI;
  HW = IOSetSamplingRate(HW, [HW.params.fsAI HW.params.fsAI]);
  globalparams.HWparams = HW.params;
else
  HW = IOSetSamplingRate(HW, get(exptparams.TrialObject, 'SamplingRate'));
end

function exptparams=LF_showMic(MicData,exptparams,HW,TrialSound)
if ~isempty(MicData)
  if isfield(exptparams,'wavefig') figure(exptparams.wavefig);
  else exptparams.wavefig = figure;
  end
  winsize=100;%ms
  winAO=hanning(HW.params.fsAO*winsize/1000);
  winAI=hanning(HW.params.fsAI*winsize/1000);
  ax=subplot1(2,2,'Gap',[0.1 0.02],'Max',[1 .9],'XTickL','Margin','YTickL','All');
 t=(0:(length(TrialSound)-1))/HW.params.fsAO;plot(t,TrialSound,'Parent',ax(1)); title(ax(1),'computer waveform')
  axes(ax(2)); spectrogram(TrialSound,winAO,[],[],HW.params.fsAO,'yaxis'); title(ax(2),'computer spectrogram');
 t=(0:(length(MicData)-1))/HW.params.fsAI;plot(t,MicData,'Paren',ax(3)); title(ax(3),'microphone waveform');
  linkaxes(ax([1 3]),'x')
  try
      axes(ax(4)); spectrogram(MicData,winAI,[],[],HW.params.fsAI,'yaxis'); title(ax(4),'microphone spectrogram');
  catch e
      warning(e.message)
  end
  linkaxes(ax([2 4]))
  set(ax([2 4]),'YLim',[0 20]);
end

