function o = ObjUpdate (o)
%
% For RefTar Object, ObjUpdate does the following:
% run ObjUpdate for Reference and Target objectds

% Nima, november 2005
ref = get(o,'ReferenceHandle');
tar = get(o,'TargetHandle');
SR=get(o,'SamplingRate');
tar_str_suffixes=[];
TarNames={};
if ~isempty(ref)
    ref = ObjUpdate(ref);
    o = set(o,'ReferenceHandle',ref);
    o = set(o,'ReferenceClass',class(ref));
    o = set(o,'ReferenceMaxIndex',get(ref,'MaxIndex'));
    %[w,e] = waveform(ref,1);
    %o = set(o,'NumOfEvPerStim',length(e));
    %o = set(o,'NumOfEvPerRef',length(e));
    SR=max(SR,get(ref,'SamplingRate'));
    if ismethod(ref,'get_target_suffixes')
        tar_str_suffixes=get_target_suffixes(ref);
    end
end
if ~isempty(tar)
    tar = ObjUpdate(tar);
    o = set(o,'TargetHandle',tar);
    o = set(o,'TargetClass',class(tar));
    o = set(o,'TargetMaxIndex',get(tar,'MaxIndex'));
    %[w,e] = waveform(tar,1);
    %o = set(o,'NumOfEvPerTar',length(e));
    %if isfield(get(tar),'NumOfEvPerTar')
    %    o = set(o,'NumOfEvPerTar',get(tar,'NumOfEvPerTar'));
    %end
    SR=max(SR,get(tar,'SamplingRate'));
    TarNames=get(tar,'Names');
    if ~isempty(tar_str_suffixes)
        for i=1:length(TarNames)
            for j=1:length(tar_str_suffixes)
                Names2{i,j}=[TarNames{i},':',tar_str_suffixes{j}];
            end
        end
        Names2=Names2(:);
        tar=set(tar,'Names',Names2);
        o = set(o,'TargetHandle',tar);  
    end

    if length(TarNames) > length(unique(TarNames))
        error('There are some targets that have the same name (tar.Names has duplicates). This is going to mess up identifying which target was played later on. Fix?')
    end
        
else
    o = set(o,'TargetClass','None');
end

%set IsCatch_array
if iscell(o.IsCatch) && ~isempty(o.IsCatch{1}) && (length(tar_str_suffixes)>1 || length(TarNames)>1 ) && ~isempty(tar)
    o.IsCatch_=false(length(TarNames),max(length(tar_str_suffixes),1));
    if length(o.IsCatch)==length(TarNames) || length(o.IsCatch)==1
        for i=1:length(TarNames) 
            if length(o.IsCatch)==1
                iset=length(TarNames);
            else
                iset=i;
            end
            if i>1 && length(o.IsCatch)==1
                IsCatch=str2num(o.IsCatch{1});
            elseif length(o.IsCatch)<i
                error('length(MultiRefTar.IsCatch) is %d. It must either be length 1, or have a length equal to the number of targets.',length(o.IsCatch))
            else
                IsCatch=str2num(o.IsCatch{i});
            end
            if isempty(IsCatch)
                mi=cellfun(@(x)contains(o.IsCatch{i},x),tar_str_suffixes);
                if sum(mi)==0
                    error('MultiRefTar.IsCatch element %d was ''%s'' which did not match any tar_str_suffixes created by the reference object.',i,o.IsCatch{i});
                end
                o.IsCatch_(iset,mi)=1;
            elseif IsCatch==0
                o.IsCatch_(iset,:)=0;
            elseif IsCatch==1
                o.IsCatch_(iset,:)=1;
            else
                error('Error setting IsCatch_')
            end
        end
    else
        error('Length of IsCatch must be length 1 or equal to the number of targets.')
    end
end

if length(o.TargetChannel)==1 && length(o.TargetIdxFreq)>1
    o=set(o,'TargetChannel',repmat(o.TargetChannel,1,length(o.TargetIdxFreq)));
end

% Also, set the runclass:
o = set(o,'RunClass', RunClassTable(ref,tar));
o = set(o,'SamplingRate',SR);
