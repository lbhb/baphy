function exptparams = StoreTrialParams (o, exptparams, RepIndex)
% exptparams = StoreTrialParams (o, exptparams, globalparams, RepIndex)
% Log Trial Paramters

trialidx=exptparams.InRepTrials;
trialidx_whole_run=exptparams.TotalTrials;
tiwr=trialidx_whole_run;
par = get(o);

exptparams.TrialParams(tiwr).rep=RepIndex;
exptparams.TrialParams(tiwr).trial_this_rep=trialidx;
exptparams.TrialParams(tiwr).TargetIndices=par.TargetIndices{trialidx};
exptparams.TrialParams(tiwr).ReferenceIndices=par.ReferenceIndices{trialidx};
exptparams.TrialParams(tiwr).CatchIndices=par.CatchIndices{trialidx};
exptparams.TrialParams(tiwr).CatchSeg=par.CatchSeg(trialidx);
exptparams.TrialParams(tiwr).CueSeg=par.CueSeg(trialidx);
exptparams.TrialParams(tiwr).TarOnset=par.TarOnset(trialidx);
exptparams.TrialParams(tiwr).LightTrial=par.LightTrial(trialidx);
exptparams.TrialParams(tiwr).CatchHitStreak=par.CatchHitStreak;

