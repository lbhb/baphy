function o = set (o,varargin)
% o = set (o,name,value,do_ObjUpdate)
% o = set (o,name,value)
% by default, do_ObjUpdate is true
% This is a generic set function for all userdefined classes. Copy this 
% function in your objects directory.

% Nima Mesgarani, October 2005, mnima@umd.edu
do_ObjUpdate=true;
switch nargin
    case 1
    % if one argument, display all the fields
        get(o);
    % or two inputs, just return the field
    case 2 
        get(o,varargin{1});
    case {3,4}
        % if single property, change its value
        fields = fieldnames(o);
        index = find(strcmpi (fields,varargin{1})==1);
        if isempty(index)
            object_field = o.(fields{end});
            if isobject(object_field)
                o.(fields{end}) = set(object_field,varargin{1},varargin{2});
            else
                error('%s \n%s', ['There is no ''' varargin{1} ''' property in specified class']);
            end
        else
            o.(fields{index})=varargin{2};
        end
        if nargin==4
            do_ObjUpdate=varargin{end};
        end
        % Only one property is allowed
    otherwise 
        error('%s \n%s','Error using ==> set','Too many input arguments.');
end

if do_ObjUpdate && length(varargin)==2
    if(any(strcmp(varargin{1},{'SamplingRate','PreStimSilence','PostStimSilence'})))
        do_ObjUpdate=false;
    end
end

if do_ObjUpdate
    caller = dbstack;
    if length(caller)>1
        if strcmpi(caller(2).name, 'ObjUpdate')
            do_ObjUpdate=false;% if its not called from ObjUpdate function, run it. 
            % without this check, it will become a close loop.
    %         disp(sprintf('called by %s',caller(2).name));        
        end
    end
end
if do_ObjUpdate
    o = ObjUpdate(o);
end