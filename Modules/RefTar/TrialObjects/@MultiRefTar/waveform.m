function [TrialSound, events , o] = waveform (o,TrialIndex)
% This function generates the actual waveform for each trial from
% parameters specified in the fields of object o. This is a generic script
% that works for all passive cases, all active cases that use a standard
% SoundObject (e.g. tone). You can overload it by writing your own waveform.m
% script and copying it in your object's folder.

% Nima Mesgarani, October 2005

global BAPHY_LAB
global HW

par = get(o); % get the parameters of the trial object
RefObject = par.ReferenceHandle; % get the reference handle
TarObject = par.TargetHandle; % getthe target handle
RefSamplingRate = ifstr2num(get(RefObject,'SamplingRate'));
TarSamplingRate = ifstr2num(get(TarObject,'SamplingRate'));

old_catch_stuff=0;

TarTrialIndex=[];
if (par.NumberOfTarPerTrial~=0) && ~strcmpi(par.TargetClass,'none')
    TarTrialIndex = par.TargetIndices{TrialIndex};
    if isempty(TarTrialIndex)  % if its a Sham
        % this means there is a target but this trial is sham. ALthough
        % there is no target, we need to adjust the amplitude based on
        % RefTardB.
        %TarObject = -1;
    end
end
if old_catch_stuff
    CatchTrialIndex=par.CatchIndices{TrialIndex};
end

if par.CueSeg(TrialIndex)==1
    if ~isempty(par.CueModifications)
       eval(par.CueModifications); 
    end
end
if par.CatchSeg(TrialIndex)==1
    if ~isempty(par.CatchModifications)
        statements=split(par.CatchModifications);
        statements(isempty(statements))=[];
        vals=zeros(length(statements),2);
        evstr=cell(length(statements),1);
        for i=1:length(statements)
            eqs=strfind(statements{i},'=');
            if length(eqs)==1 % a variable was reassigned
              left=statements{i}(1:eqs-1);
              dot=strfind(left,'.');
              parl=strfind(left,'(');
              parr=strfind(left,')');
              if length(dot)==1 && length(parl)==1 && length(parr)==1
                evstr{i}=['get(',left(1:dot-1),',''',left(dot+1:parl-1),''',',left(parl+1:parr-1),');'];
                vals(i,1)=eval(evstr{i});
              else
                 % vals(i,1)=eval(statements{i}(1:eqs-1));
              end
            else %something more complicated happened
            end
        end
        eval(par.CatchModifications);
        for i=1:length(statements)
            eqs=strfind(statements{i},'=');
            
            if length(eqs)==1 && ~isempty(evstr{i})
                vals(i,2)=eval(evstr{i});
                if vals(i,1)~=vals(i,2)
                    fprintf('Catch Trial, executed CatchModifications: ''%s'';\n',statements{i})
                    fprintf('%s was %d, is now %d for this trial.\n',statements{i}(1:eqs-1), vals(i,1), vals(i,2))
                end
            else %something more complicated happened
                fprintf('Catch Trial, executed CatchModifications: ''%s'';\n',statements{i})
            end
        end
    end
end

% is light on or off?
LightDuringRef=0;
LightDuringTar=0;
if par.LightPulseDuration==0 || length(par.LightTrial)<TrialIndex,
    LightTrial=0;
else
    LightTrial = par.LightTrial(TrialIndex);
    if LightTrial,
       if ismember(strtrim(par.LightEpoch),{'Sound','SoundOnset','WholeTrial'})
          LightDuringRef=1;  % for accurate event notes
       end
       if ismember(strtrim(par.LightEpoch),{'Target'})
          LightDuringTar=1;  % for accurate event notes
       end
    end
end
   

TrialSamplingRate = max(RefSamplingRate, TarSamplingRate);
RefObject = set(RefObject, 'SamplingRate', TrialSamplingRate);
TarObject = set(TarObject, 'SamplingRate', TrialSamplingRate);
par.SamplingRate=TrialSamplingRate;
OverlapRefTar=strcmpi(par.OverlapRefTar,'Yes');

refpar=get(RefObject);
rpre=get(RefObject,'PreStimSilence');
rdur=get(RefObject,'Duration');
rpos=get(RefObject,'PostStimSilence');

% get the index of reference sounds for current trial
RefTrialIndex = par.ReferenceIndices{TrialIndex};
if ~isempty(par.SingleRefSegmentLen) && par.SingleRefSegmentLen>0,
    if OverlapRefTar && ~isempty(TarTrialIndex),
        TarStartTime=(par.TarOnset{TrialIndex}+...
          get(RefObject,'PreStimSilence'));
        TarStartBin=round(TarStartTime.*TrialSamplingRate);
        AdjustRefDur=max(par.TarOnset{TrialIndex})+get(TarObject,'Duration')+...
            get(TarObject,'PreStimSilence')+get(TarObject,'PostStimSilence');
        AdjustRefDur=round(AdjustRefDur,10);
    else
        AdjustRefDur=max(par.TarOnset{TrialIndex});
    end
else
    if OverlapRefTar,
        TarStartTime=(rpre+rdur+rpos).*(length(RefTrialIndex)-1);
    else
        TarStartTime=(rpre+rdur+rpos).*length(RefTrialIndex);
    end
    TarStartBin=round(TarStartTime.*TrialSamplingRate)+1;
    AdjustRefDur=get(RefObject,'Duration');
end
PostTrialSilence = par.PostTrialSilence;
PostTrialBins=round(PostTrialSilence.*TrialSamplingRate);

TrialSound = []; % initialize the waveform use [:,2]?
ind = 0;
events = [];
LastEvent = 0;
TarMatchContour=strcmpi(par.TargetMatchContour,'Yes');

% generate the reference sound
refenv=[];

if ~isempty(TarTrialIndex)
    ThisTarIdx = par.TargetIndices{TrialIndex}; % get the index of reference sounds for current trial
else
    ThisTarIdx = par.TargetIndices{1};
end

% go through all the reference sounds in the trial
for cnt1 = 1:length(RefTrialIndex)
    
    if get(RefObject,'Duration')==AdjustRefDur %&& all(par.TarOnset>length(RefTrialIndex))
        % simple case: Reference duration matches what we want it to be
        if RefTrialIndex(cnt1)==0
            % zero Reference during catch without overlap
            [w,ev]=waveform(RefObject, 1,TrialIndex);
            w(:)=0;
        else
            [w,ev]=waveform(RefObject, RefTrialIndex(cnt1),TrialIndex);
        end
        % svd 2009-06-29 make sure that sound is in a column
        if size(w,2)>size(w,1)
            w=w';
        end
    else
        CumRefDur=0;
        fs=get(RefObject,'SamplingRate');
        RefObject=set(RefObject,'PostStimSilence',0);
        rcount=0;
        w=[];ev=[];
        while CumRefDur<AdjustRefDur,
            if get(RefObject,'Duration')>AdjustRefDur-CumRefDur,
                RefObject=set(RefObject,'Duration',AdjustRefDur-CumRefDur);
            end
            %fprintf('Adding ref seg %.2f sec\n',get(RefObject,'Duration'));
            switch o.TargetMode
                case {'Envelope','Carrier'}
                ThisTarParams.TargetMode=o.TargetMode;
                ThisTarParams.TarStartBin=TarStartBin-round(rpre.*fs);
                ThisTarParams.ThisTarIdx=ThisTarIdx;
                fields={'EnvTarRefRatio','RelativeTarRefdB','EnvTarRefRampDur','TargetChannel'};
                for j=1:length(ThisTarIdx)
                    for i=1:length(fields)
                        L = length(par.(fields{i}));
                        if L < ThisTarIdx(j) && L > 1
                            error(['MultiRefTar.%s has length %i but requested ',...
                                'target index is %i. Its length must be either ',...
                                '1 or the same as the number of unique targets'],...
                                fields{i},L,ThisTarIdx(j))
                        elseif L < ThisTarIdx(j)
                            ThisTarParams.(fields{i})(j)=par.(fields{i});
                        else
                            ThisTarParams.(fields{i})(j)=par.(fields{i})(ThisTarIdx(j));
                        end
                    end
                end
                [tw,tev,tenv]=waveform(RefObject, RefTrialIndex(cnt1),TrialIndex,TarObject,ThisTarParams);
                if 0 %Use this if you want to save the trial waveform to a file. Especially useful if you use simulation mode. LAS 2020
                    %sf=0.999/max([tw,tenv]);
                    sf=1/5;
                    dd='P:\LBHB\SPS\SIM 7-9 R5\';
                    UTmkdir(dd)
                    if par.CueSeg(TrialIndex)==1 && TrialIndex==1
                        tar=[zeros(round(1,TrialSamplingRate*.2)),tw(ThisTarParams.TarStartBin:(ThisTarParams.TarStartBin+round(get(TarObject,'Duration')*TrialSamplingRate)))];
                        audiowrite([dd 'Target.wav'],tar*sf,TrialSamplingRate)
                    end
                    audiowrite([dd 'Stim T' num2str(TrialIndex),'.wav'],tw*sf,TrialSamplingRate)
                    audiowrite([dd 'Stim R' num2str(TrialIndex),'.wav'],tenv*sf,TrialSamplingRate)
                end
                if size(o.IsCatch_,2)>1
                    ref_type_ind=strcmp(get_target_suffixes(o.ReferenceHandle),get_target_suffixes(o.ReferenceHandle,RefTrialIndex(cnt1)));
                else
                    ref_type_ind=1;
                end
                for ci=find(o.IsCatch_(ThisTarIdx,ref_type_ind)')
                    evi=find(arrayfun(@(x)contains(x.Note,'Stim ,') && x.StartTime*get(RefObject,'SamplingRate')==ThisTarParams.TarStartBin(ci),tev));
                    if length(evi)>1, error('fix me'); end
                    tarname=strsep(tev(evi).Note,',');
                    evi=find(arrayfun(@(x)contains(x.Note,tarname{2}),tev));
                    if length(evi)~=3, error('There should be pre-stim, stim, and post-stim here.'); end
                    for i=1:length(evi)
                        tev(evi(i)).Note=strrep(tev(evi(i)).Note,'Target','Catch');
                    end
                end
                for ti=1:length(ThisTarIdx)
                    fprintf('TarTrialIndex=%d (%d dB, channel %d)\n',ThisTarIdx(ti),...
                        ThisTarParams.RelativeTarRefdB(ti),ThisTarParams.TargetChannel(ti));
                end
                case 'Waveform'
                    ThisTarParams.TargetMode='Waveform';
                    [tw,tev,tenv]=waveform(RefObject, RefTrialIndex(cnt1),TrialIndex);
                    %tenv=[];
                case 'RefDependentWaveform'
                    ThisTarParams.TargetMode='RefDependentWaveform';
                    ThisTarParams.TarStartBin=TarStartBin-round(rpre.*fs);
                    ThisTarParams.ThisTarIdx=ThisTarIdx;
                    [tw,tev,tenv,TarObject,ThisTarParams]=waveform(RefObject, RefTrialIndex(cnt1),TrialIndex,TarObject,ThisTarParams);
                    TarStartBin=ThisTarParams.TarStartBin;
                    TarStartTime=(TarStartBin-1)/TrialSamplingRate;
                otherwise
                    error('Unknown TargetMode (''%s'') in MultiRefTar',o.TargetMode)
            end
            
            CumRefDur=CumRefDur+get(RefObject,'Duration');
            
            % svd 2009-06-29 make sure that sound is in a column
            if size(tw,2)>size(tw,1),
                tw=tw';
            end
            w=cat(1,w,tw);
            if TarMatchContour && ismethod(RefObject,'env'),
              te=env(RefObject, RefTrialIndex(cnt1));
              refenv=cat(1,refenv,te);
            end
            if isempty(ev),
                ev=tev;
            else
                t0=ev(end).StopTime;
                for ii=1:length(tev),
                    tev(ii).StartTime=tev(ii).StartTime+t0;
                    tev(ii).StopTime=tev(ii).StopTime+t0;
                end
                ev=cat(2,ev,tev);
            end
            RefObject=set(RefObject,'PreStimSilence',0);
        end
        w=cat(1,w,zeros(round(rpos.*fs),size(w,2)));
        ev(end).StopTime=ev(end).StopTime+rpos;
    end
  % add "Reference" to the note, correct the time stamp in respect to
  % last event, and concatenate w onto TrialSound
  for cnt2 = 1:length(ev)
      if ~contains(ev(cnt2).Note,', Target') && ~contains(ev(cnt2).Note,', Catch')
          ev(cnt2).Note = [ev(cnt2).Note ' , Reference'];
      end
      if par.LightPulseDuration==0 || ~LightDuringRef,
      elseif LightTrial
          ev(cnt2).Note = [ev(cnt2).Note '+Light'];
      else
          ev(cnt2).Note = [ev(cnt2).Note '+NoLight'];
      end
      ev(cnt2).StartTime = ev(cnt2).StartTime + LastEvent;
      ev(cnt2).StopTime = ev(cnt2).StopTime + LastEvent;
      ev(cnt2).Trial = TrialIndex;
  end
  LastEvent = ev(end).StopTime;
  
   % added 08/03/15 to deal with splitchnls for RSS
    % pad higher channels with zero
    if isempty(TrialSound)
        %w=tw;
    elseif size(w,2)>size(TrialSound,2)
        TrialSound=[TrialSound zeros(length(TrialSound),...
            size(w,2)-size(TrialSound,2))];
    elseif size(w,2)<size(TrialSound,2)
        w=[w zeros(length(w),size(TrialSound,2)-size(w,2))];
    end
            
  TrialSound = [TrialSound ;w];
  events = [events ev];
end

TrialSound0=TrialSound;
chancount=size(TrialSound,2);


TargetChannel=par.TargetChannel;
if isempty(TargetChannel),
  TargetChannel=1;
elseif any(length(TargetChannel)<ThisTarIdx)
  if length(TargetChannel)==1
      TargetChannel=repmat(TargetChannel(1),size(ThisTarIdx));
  else
      error('MultiRefTar.TargetChannel doesn''t match the number of targets (%d)',length(ThisTarIdx))
  end
else
  TargetChannel=TargetChannel(ThisTarIdx);
end

RelativeTarRefdB=par.RelativeTarRefdB;
if isempty(RelativeTarRefdB)
    RelativeTarRefdB=0;
end
if length(RelativeTarRefdB) < max(ThisTarIdx)
    RelativeTarRefdB = ones(max(ThisTarIdx),1) * RelativeTarRefdB(1);
end
RelativeTarRefdB=RelativeTarRefdB(ThisTarIdx);
ScaleBy=10.^(RelativeTarRefdB/20);

if ~isempty(TarTrialIndex) && (strcmp(o.TargetMode,'Waveform') || strcmp(o.TargetMode,'RefDependentWaveform'))
    
    TarObject = set(TarObject, 'SamplingRate', TrialSamplingRate);
    
    for tar_cnt = 1:length(TarTrialIndex)
        ThisTar=ThisTarIdx(tar_cnt);
        ThisTarSlot=par.TarOnset{TrialIndex}(tar_cnt);
        ThisTargetChannel=TargetChannel(tar_cnt);
        ThisScaleBy=ScaleBy(tar_cnt);
        [w, ev] = waveform(TarObject, ThisTar, 0); % 0 means its target
        if ismethod(RefObject,'get_target_suffixes')
            ref_type_str=get_target_suffixes(RefObject,RefTrialIndex(cnt1));
            for i=1:length(ev)
                ev(i).Note=[ev(i).Note,':',ref_type_str{1}];
            end
        end
        % generate the target sound:
        %[w, ev] = waveform(TarObject, ThisTarIdx, 0); % 0 means its target
        ev_save=ev;

        % svd 2009-06-29 make sure that sound is in a column
        if size(w,2)>size(w,1),
            w=w';
        end

        if size(w,2)==1 && ThisTargetChannel>1,
            w=[repmat(zeros(size(w)),[1 TargetChannel-1]) w];
        end        
        
        % shift single-channel target to appropriate output channel
        %if size(w,2)==1 && TargetChannel>1,
        %    w=[repmat(zeros(size(w)),[1 TargetChannel-1]) w];
        %end
        % pad higher channels with zero
        w=[w zeros(length(w),chancount-size(w,2))];

        % this trial has a regular target, save it
        fprintf('TarTrialIndex=%d (%d dB, channel %d)\n',TarTrialIndex,RelativeTarRefdB(tar_cnt), TargetChannel);
        if OverlapRefTar,
            TarBins=TarStartBin(tar_cnt)+round(get(TarObject,'PreStimSilence').*TrialSamplingRate)+....
                (1:round((get(TarObject,'Duration').*TrialSamplingRate)));

            RefMaxDuringTar=max(abs(TrialSound(TarBins,ThisTargetChannel)));
            if RefMaxDuringTar~=5
                ThisScaleBy=ThisScaleBy.*RefMaxDuringTar./5;
                fprintf('Ref max during tar: %.1f (max is 5). Adjusted Tar ScaleBy from %.2f to %.2f \n',RefMaxDuringTar,ScaleBy(tar_cnt),ThisScaleBy);
            end
            RefMeanDuringTar=mean(abs(TrialSound(TarBins,ThisTargetChannel)));
            %ThisScaleBy=ThisScaleBy.*RefMeanDuringTar./5;
            %fprintf('Ref mean during tar: %.1f. Adjusted Tar ScaleBy: %.2f\n',RefMeanDuringTar,ThisScaleBy);
            if ~isinf(ThisScaleBy),
                w=w.*ThisScaleBy;
            end
            if TarMatchContour && ~isempty(refenv),
                tarenv=refenv(TarStartBin(tar_cnt)-1+(1:size(w,1)),ThisTarIdx);
                %tarenv=tarenv.*0.75+0.25;
                w(:,ThisTargetChannel)=w(:,ThisTargetChannel).*tarenv;
                if isinf(ThisScaleBy),
                    TrialSound(TarStartBin(tar_cnt)-1+(1:size(w,1)),:)=...
                        w.*TarMod;
                else
                    TrialSound(TarStartBin(tar_cnt)-1+(1:size(w,1)),:)=...
                        TrialSound(TarStartBin(tar_cnt)-1+(1:size(w,1)),:)+w;
                end
            else
                if ~isinf(ThisScaleBy) && ThisScaleBy>1, ThisScaleBy=1; end
                if par.OnsetRampTime>0,
                    % add a ramp
                    PreZeros=round(get(TarObject,'PreStimSilence').*TrialSamplingRate);
                    PostZeros=round(get(TarObject,'PostStimSilence').*TrialSamplingRate);

                    TarRampBins=round(par.OnsetRampTime.*TrialSamplingRate);
                    NonRampBins=length(w)-TarRampBins.*2-PreZeros-PostZeros;
                    RefMod=repmat([ones(1,PreZeros) ...
                        linspace(1,1-ThisScaleBy,TarRampBins) ...
                        ones(1,NonRampBins).*(1-ThisScaleBy)...
                        linspace(1-ThisScaleBy,1,TarRampBins)...
                        ones(1,PostZeros)]',[1 size(w,2)]);
                    TarMod=repmat([ones(1,PreZeros) ...
                        linspace(0,1,TarRampBins) ...
                        ones(1,NonRampBins)...
                        linspace(1,0,TarRampBins)...
                        ones(1,PostZeros)]',[1 size(w,2)]);
                    zz= (std(w)==0);
                    RefMod(:,zz)=1;
                else
                    RefMod=1;
                    TarMod=1;
                end
                if isinf(ThisScaleBy),
                    TrialSound(TarStartBin(tar_cnt)-1+(1:size(w,1)),:)=...
                        w.*TarMod;
                else
                    TrialSound(TarStartBin(tar_cnt)-1+(1:size(w,1)),:)=...
                        TrialSound(TarStartBin(tar_cnt)-1+(1:size(w,1)),:).*RefMod+...
                        w.*TarMod;
                end

            end
            LastEvent=TarStartTime(tar_cnt); %Hack to make target start at TarStartTime. Using LastEvent variable but that isn't necessarily rhe time of the last event

        else
            % no overlap
            fprintf('Tar ScaleBy: %.2f\n',ThisScaleBy);
            w=w.*ThisScaleBy;
            % now, figure out if this target needs to be inserted earlier,
            % otherwise, just tack on the end
            if (ThisTarSlot<=length(RefTrialIndex)) && (ThisTarSlot>0)&& (get(TarObject, 'Duration')==get(RefObject, 'Duration')) ...
                    && (get(TarObject, 'PreStimSilence')==get(RefObject, 'PreStimSilence')) ...
                    && (get(TarObject, 'PostStimSilence')==get(RefObject, 'PostStimSilence'))
                
                nrefs_prior = ThisTarSlot-1;
                reflentot = get(RefObject, 'Duration') + get(RefObject, 'PreStimSilence') + get(RefObject, 'PostStimSilence');
                tidxstart = reflentot * TrialSamplingRate * nrefs_prior;
                TrialSound = [TrialSound(1:tidxstart); w; TrialSound(tidxstart:end)];
                
                % keep track of offset
                for e = 1:length(ev)
                    ev(e).offset = reflentot * nrefs_prior;
                end
                %Also add offset to reference events that were shifted back
                for e=find( ([events.StartTime] >= reflentot * nrefs_prior) & arrayfun(@(x)contains(x.Note,'Reference'),events) )
                    events(e).StartTime = events(e).StartTime + reflentot;
                    events(e).StopTime = events(e).StopTime + reflentot;
                end
            
            elseif (ThisTarSlot<length(RefTrialIndex)) && (ThisTarSlot>0)
                error("Supposed to insert target earlier, but if target and reference sounds are different lenghts, this is complicated")
            else
                TrialSound = [TrialSound ; w ];
            end
            
        end

        % now, add Target to the event list, correct the time stamp with
        % respect to last event, and add Trial
        if size(o.IsCatch_,2)>1
            ref_type_ind=strcmp(get_target_suffixes(o.ReferenceHandle),get_target_suffixes(o.ReferenceHandle,RefTrialIndex(1)));
        else
            ref_type_ind=1;
        end
        %if o.IsCatch_(ThisTarIdx,ref_type_ind)
        if o.IsCatch_(ThisTar, ref_type_ind)
            tarCatchStr=' , Catch';
        else
            tarCatchStr=' , Target';
        end
        for cnt2 = 1:length(ev)
            if par.LightPulseDuration==0,
                ev(cnt2).Note = [ev(cnt2).Note tarCatchStr];
            elseif LightTrial
                ev(cnt2).Note = [ev(cnt2).Note tarCatchStr '+Light'];
            else
                ev(cnt2).Note = [ev(cnt2).Note tarCatchStr '+NoLight'];
            end
            if isfield(ev(cnt2), 'offset')
                ev(cnt2).StartTime = ev(cnt2).StartTime + ev(cnt2).offset;
                ev(cnt2).StopTime = ev(cnt2).StopTime + ev(cnt2).offset;
            else
                ev(cnt2).StartTime = ev(cnt2).StartTime + LastEvent;
                ev(cnt2).StopTime = ev(cnt2).StopTime + LastEvent;
            end
            ev(cnt2).Trial = TrialIndex;
        end
        if isfield(ev(cnt2), 'offset')
            LastEvent = max([events.StopTime]);
        else
            LastEvent = ev(end).StopTime;
        end
        if isfield(ev, 'offset')
            ev=rmfield(ev, 'offset');
        end
            events = [events ev];
        
    end
% else
%     % is this relevant to anythign???
%     w=w.*ScaleBy;
end

% if specified, tack a final "reminder target" on the end of the trial
% to provide an easy (low-reward) hit if the actual target was missed.
if par.ReminderTarget,
    % use same waveform as target
    ev=ev_save;
    
    ReminderStartTime=size(TrialSound,1)/TrialSamplingRate;
    TrialSound=[TrialSound ; w];
    for cnt2 = 1:length(ev),
        ev(cnt2).Note = [ev(cnt2).Note ' , Reminder'];
        ev(cnt2).StartTime = ev(cnt2).StartTime + ReminderStartTime;
        ev(cnt2).StopTime = ev(cnt2).StopTime + ReminderStartTime;
        ev(cnt2).Trial = TrialIndex;
    end
    LastEvent = ev(end).StopTime;
    events = [events ev];
end    

%SVD Disabled trial-level sound normalization.  Now it's up to the sound
%objects to do the normalization correctly.
% normalize the sound, because the level control is always from attenuator.
if ~isempty(TarTrialIndex)
    %TrialSound = 5 * TrialSound / max(abs(TrialSound0(:)));
else
    %TrialSound = 5 * TrialSound / max(abs(TrialSound(:)));
end

%if max(abs(TrialSound(:)))>10,
%    error('TrialSound too loud');
%end

if old_catch_stuff && ~isempty(CatchTrialIndex)
    CatchChannel=par.TargetChannel;
    if isempty(CatchChannel),
      CatchChannel=1;
    elseif length(CatchChannel)<CatchTrialIndex,
      CatchChannel=CatchChannel(1);
    else
      CatchChannel=CatchChannel(CatchTrialIndex);
    end
    
    RelativeTarRefdB=par.RelativeTarRefdB;
    if isempty(RelativeTarRefdB),
      RelativeTarRefdB=0;
    elseif length(RelativeTarRefdB)<CatchTrialIndex,
      RelativeTarRefdB=RelativeTarRefdB(1);
    else
      RelativeTarRefdB=RelativeTarRefdB(CatchTrialIndex);
    end
    ScaleBy=10^(RelativeTarRefdB/20);
    
    TarObject = set(TarObject, 'SamplingRate', TrialSamplingRate);
    % generate the target sound:
    [w, ev] = waveform(TarObject, CatchTrialIndex, 0); % 0 means its target
    
    if par.SingleRefSegmentLen>0
        CatchStartTime=(par.SingleRefSegmentLen*par.CatchSeg(TrialIndex)+...
           get(RefObject,'PreStimSilence'));
    else
        CatchStartTime=(rpre+rdur+rpos)*(par.CatchSeg(TrialIndex));
    end
    CatchStartBin=round(CatchStartTime.*TrialSamplingRate);
    
    % svd 2009-06-29 make sure that sound is in a column
    if size(w,2)>size(w,1)
        w=w';
    end
    
    % shift single-channel target to appropriate output channel
    if size(w,2)==1 && CatchChannel>1,
        w=[repmat(zeros(size(w)),[1 CatchChannel-1]) w];
    end
    % pad higher channels with zero
    w=[w zeros(length(w),chancount-size(w,2))];
    
    fprintf('CatchTrialIndex=%d (%d dB, channel %d)\n',CatchTrialIndex,RelativeTarRefdB, CatchChannel);
    
    if OverlapRefTar
        CatchBins=CatchStartBin+round(get(TarObject,'PreStimSilence').*TrialSamplingRate)+....
            (1:round((get(TarObject,'Duration').*TrialSamplingRate)));
        RefMaxDuringCatch=max(abs(TrialSound(CatchBins,CatchChannel)));
        ScaleBy=ScaleBy.*RefMaxDuringCatch./5;
        fprintf('Ref max during catch: %.1f. Adjusted Catch ScaleBy: %.2f\n',RefMaxDuringCatch,ScaleBy);
        %RefMeanDuringCatch=mean(abs(TrialSound(CatchBins,CatchChannel)));
        %ScaleBy=ScaleBy.*RefMeanDuringCatch./5;
        %fprintf('Ref mean during catch: %.1f. Adjusted Catch ScaleBy: %.2f\n',RefMeanDuringCatch,ScaleBy);
        w=w.*ScaleBy;
        
        if par.OnsetRampTime>0
           % add a ramp
           PreZeros=round(get(TarObject,'PreStimSilence').*TrialSamplingRate);
           PostZeros=round(get(TarObject,'PostStimSilence').*TrialSamplingRate);
           
           TarRampBins=round(par.OnsetRampTime.*TrialSamplingRate);
           NonRampBins=length(w)-TarRampBins.*2-PreZeros-PostZeros;
           RefMod=repmat([ones(1,PreZeros) ...
              linspace(1,1-ScaleBy,TarRampBins) ...
              ones(1,NonRampBins).*(1-ScaleBy)...
              linspace(1-ScaleBy,1,TarRampBins)...
              ones(1,PostZeros)]',[1 size(w,2)]);
           TarMod=repmat([ones(1,PreZeros) ...
              linspace(0,1,TarRampBins) ...
              ones(1,NonRampBins)...
              linspace(1,0,TarRampBins)...
              ones(1,PostZeros)]',[1 size(w,2)]);
           zz= (std(w)==0);
           RefMod(:,zz)=1;
        else
           RefMod=1;
           TarMod=1;
        end
        
        w = 5 * w / max(abs(TrialSound0(:)));
        
        TrialSound(CatchStartBin-1+(1:size(w,1)),:)=...
           TrialSound(CatchStartBin-1+(1:size(w,1)),:).*RefMod+...
           w.*TarMod;
    else
       TrialSound(CatchStartBin+(1:size(w,1)),:)=w .* ScaleBy;
       %error('non-overlapping catch stimuli not supported');
    end
    LastEvent=CatchStartTime;
    
    % now, add Target to the event list, correct the time stamp with
    % respect to last event, and add Trial
    for cnt2 = 1:length(ev)
        ev(cnt2).Note = [ev(cnt2).Note ' , Catch'];
        ev(cnt2).StartTime = ev(cnt2).StartTime + LastEvent;
        ev(cnt2).StopTime = ev(cnt2).StopTime + LastEvent;
        ev(cnt2).Trial = TrialIndex;
    end
    events = [events(1:(end-3)) ev events((end-2):end)];
    LastEvent = max(cat(1,events.StopTime));
end

% sort events to be in temporal order
starts=cat(1,events(1:3:end).StartTime);
[~,si] = sort(starts);
si=[si*3-2 si*3-1 si*3]';
events=events(si(:));

if PostTrialBins>0
   TrialSound=cat(1,TrialSound,zeros(PostTrialBins,chancount));
   stops = cat(1,events.StopTime);
   last_evidx = find(stops == max(stops), 1, 'last');
   events(last_evidx).StopTime=events(last_evidx).StopTime+PostTrialSilence;
end

if 0 %Use this if you want to save the trial waveform to a file. Especially useful if you use simulation mode. LAS 2020
    sf=0.999/10^((80-par.OveralldB)/20)/5;
    dd='P:\LBHB\SPS\TestCMR\';
    UTmkdir(dd)
    audiowrite([dd 'Stim T' num2str(TrialIndex),'.wav'],TrialSound*sf,TrialSamplingRate)
    save([dd 'Stim T' num2str(TrialIndex),' env.mat'],'tenv')
    %audiowrite([dd 'Stim R' num2str(TrialIndex),'.wav'],tenv*sf,TrialSamplingRate)
end
                    
%
% Optogenetics-related stuff
%
TrialBins=size(TrialSound,1);

if HW.params.HWSetup==0
    NumChannels=2;
else
    NumChannels=HW.AO.NumChannels;
end
if strcmp(o.TargetMode,'Envelope')
    o=set(o,'TrialEnvelope',tenv,0);
end
if par.LightPulseDuration==0 || length(par.LightTrial)<TrialIndex,
    % don't do anything
else
    % generate the light control signal
    if LightTrial
        disp('Light/stim on trial');
        [LightBand, events]=generateLightTrial(par,events,TrialBins);
    else
        disp('Light/stim off trial');
        LightBand=zeros(TrialBins,1);
    end
    
    % append light band to sound matrix, should always be channel 3
    if size(TrialSound,2)>=NumChannels,
        error('Not enough output channels to contain light');
    elseif size(TrialSound,2)==NumChannels-1,
        TrialSound=cat(2,TrialSound,LightBand);
    elseif size(TrialSound,2)==NumChannels-2,
        TrialSound=cat(2,TrialSound,zeros(size(TrialSound)),LightBand);
    end
end


if strcmp(BAPHY_LAB,'lbhb') && strcmp(o.SelfTestMode,'No') && ~o.IsSimulation
   % debug code
   sfigure(99);
   clf
   m=methods(RefObject);
   if any(strcmp(m,'plot_stimulus'))
       plot_stimulus(RefObject,TarObject,ThisTarParams,TrialSound./10^((80-par.OveralldB)/20),TrialSamplingRate,RefTrialIndex(cnt1),TrialIndex,ThisTarIdx,events,tenv)
   else
       dfs=50000;

       subplot(3,1,2);
       tw=resample(TrialSound,dfs,TrialSamplingRate);
       plot((1:length(tw))./dfs,tw);
       aa=axis;
       axis([0 length(tw)./dfs aa(3:4)]);

       subplot(3,1,1);
       tw=resample(TrialSound0,dfs,TrialSamplingRate);
       plot((1:length(tw))./dfs,tw);
       axis([0 length(tw)./dfs aa(3:4)]);

       subplot(3,1,3);
       h = spectrum.welch;        % Create a Welch spectral estimator.
       Hpsd = psd(h,tw,'Fs',dfs); % Calculate the PSD
       plot(Hpsd);                % Plot the PSD.
   end
   drawnow
end

Refloudness = get(RefObject,'Loudness');
if isobject(TarObject)
    Tarloudness = get(TarObject,'Loudness');
else
    Tarloudness = 0;
end
%disp(['outWFM:' num2str(max(abs(TrialSound)))])

loudness = max(Refloudness,Tarloudness);

if loudness(min(max(RefTrialIndex),length(loudness)))>0
    o = set(o,'OveralldB', loudness(min(max(RefTrialIndex),length(loudness))));    
end
o = set(o, 'SamplingRate', TrialSamplingRate);

%2nd AO channel use for control MASTEWRFLEX flow rate.....pby added 10/06/2011
if isfield(par,'PumpProfile'),
    pumppro=ifstr2num(par.PumpProfile);
    if length(pumppro)==1 & pumppro(1)==0
        return;  %not required for pump control
    else
        pumppro(2:end)=10*pumppro(2:3)/6  %convert the flow rate (ml/min)to VDC. 10 V= 6.0 ml/min
        TrialSound(:,2)=pumppro(2);    %set constant speed
        if length(pumppro)==3
            if pumppro(1)==1     %low flow rate inter_trial_interval
                delay1=events(1).StopTime/2;                %high flow rate start before stimulus on
                delay1=(delay1*TrialSamplingRate);
                delay2=ev(3).StopTime;  %high flow rate extended during post-silence
                delay2=round(delay2*TrialSamplingRate)-10;   %10 samples for set flow rate back to low
                TrialSound(delay1:delay2,2)=pumppro(3);
            else pumppro(1)==2   %low flow arte during both ISI ans ITI, high during stimulus
                TrialSound(find(TrialSound(:,1)),2)=pumppro(2); %high speed during sound
            end
        end
    end
end






