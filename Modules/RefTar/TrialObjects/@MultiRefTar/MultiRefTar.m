function RefTarO = MultiRefTar(varargin)
% MultiRefTar -- modeled on WM trial object which itself was hacked from
% ReferenceTarget
%
% User-editable fields:
% OveralldB - Max sound level of any stimulus (reference target)
% RelativeTarRefdB - If a list of numbers, relative dB sound level of each
%  target to reference. If only one value, all targets have same level.
% ReferenceCountFrequency - Probability distribution of trial lengths.
%  Units are either integer number of references (if SingleRefSegmentLen=0)
%  or reference steps (if SingleRefSegmentLen>0).
% OverlapRefTar
% TargetIdxFreq
% OnsetRampTime - Duration of target onset ramp in sec (to reduce pop-out)
% PostTrialSilence - Tack on this many sec of silence after target and
%  reference are complete.
% SaveData - If no, test stimuli without saving anything (for debugging!)
% TargetMatchContour - May not be working. Match envelope of target to
%  reference (if it does work, it will require SpNoise reference or any
%  sound object with an env() method)
%
% created SVD - 2012-10
%
switch nargin
case 0
    % if no input arguments, create a default object
    RefTarO.descriptor = 'MultiRefTar';
    RefTarO.ReferenceClass = 'none';
    RefTarO.ReferenceHandle = [];
    RefTarO.TargetClass = 'none';
    RefTarO.TargetHandle = [];
    RefTarO.SamplingRate = 100000;
    RefTarO.OveralldB = 60;
    RefTarO.RelativeTarRefdB = 0;
    RefTarO.RefTarFlipFreq = 0;  % fraction of trials in which reference and target classes are reversed
    RefTarO.ReferenceCountFreq=[0 0.4 0.3 0.2 0.1 0];
    RefTarO.TargetIdxFreq=1;
    RefTarO.TargetChannel=1;
    RefTarO.IsCatch={'0'};
    RefTarO.IsCatch_=0;
    RefTarO.RemoveSecondTarget='No';
    RefTarO.CatchIdxFreq=0;
    RefTarO.ReminderTarget=0;
    RefTarO.CatchChannel=1;
    RefTarO.CueTrialCount=5;
    RefTarO.CueTar='MaxPumpDur';
    RefTarO.CueTarIdx=1;
    RefTarO.SingleRefSegmentLen = 0;
    RefTarO.TarIdxSet=[];
    RefTarO.TarIdx=[];
    RefTarO.CatchIdxSet=[];
    RefTarO.CatchSeg=[];
    RefTarO.CueSeg=[];
    RefTarO.OverlapRefTar='No';
    RefTarO.OverlapRefIdx=1;
    RefTarO.RepeatMisses='Yes';
    RefTarO.PostTrialSilence=1;
    RefTarO.OnsetRampTime=0;
    RefTarO.SaveData='Yes';
    RefTarO.TargetMode='Waveform';%{'Waveform','Envelope'};
    RefTarO.EnvTarRefRatio=1;%[0,1] If target mode is Envelope, controls ratio between target and reference enevelope mixing.
    RefTarO.EnvTarRefRampDur=.01;%Rise/Fall time of cos^2 ramp on transition of envelope ratio.
    RefTarO.BalanceTargetsByReftypeAndReflen='No';
    RefTarO.LightPulseRate=0;
    RefTarO.LightPulseDuration=0;
    RefTarO.LightPulseShift=0;
    RefTarO.LightEpoch='Sound';
    RefTarO.LightTrial=[];
    RefTarO.LightConditions='Interleaved';
    RefTarO.TargetMatchContour='No';
    RefTarO.shuffle_block_TarIdx=[];
    RefTarO.shuffle_block_RefLen=[];
    RefTarO.TargetCount=50;
    RefTarO.RefLenCount=50;
    %2nd target parameters
    RefTarO.TargetDistSet=1;
    RefTarO.Tar2SegCountFreq=0;
    RefTarO.Tar2SegmentLen=0;
    RefTarO.Tar1AloneOffsetSegCountFreq=0;
    RefTarO.CueModifications='';
    RefTarO.CatchModifications='';
    RefTarO.CatchHitStreak=0;
    RefTarO.shuffle_block_Tar2Offset=[];
    RefTarO.shuffle_block_Tar1AloneOffset=[];
    RefTarO.IsSimulation=0;
    
    % following are a bunch of old parameters that are mostly here for
    % backwards compatibility....
    RefTarO.NumberOfTrials = 30;  % In memory of Torcs!
    RefTarO.NumberOfRefPerTrial = [];
    RefTarO.NumberOfTarPerTrial = 1; % default 
    RefTarO.ReferenceMaxIndex = 0;
    RefTarO.TargetMaxIndex = 0;
    RefTarO.ReferenceIndices = [];
    RefTarO.TargetIndices = [];
    RefTarO.CatchIndices = [];
    RefTarO.TarOnset=[];
    RefTarO.ShamPercentage = 0;
    RefTarO.TrialEnvelope = [];
    
    RefTarO.NumOfEvPerStim = 3;  % how many stim each event produces??
    % having the two following fields is enough, we actually do not need
    % NumOfEvPerStim anymore, but for backward compatibility we keep it!
    RefTarO.NumOfEvPerRef = 3;  % how many stim each reference produces??
    RefTarO.NumOfEvPerTar = 3;  % how many stim each Target produces??
    RefTarO.RunClass = '[]';
    RefTarO.SelfTestMode='No';
    RefTarO.UserDefinableFields = ...
        {'OveralldB','edit',60,...
         'RelativeTarRefdB','edit',0,...
         'ReferenceCountFreq','edit',[0 0.4 0.3 0.2 0.1 0],...
         'SingleRefSegmentLen','edit',0,...
         'CueTrialCount','edit','5',...
         'CueTar','edit','MaxPumpDur',...
         'CueModifications','edit','',...
         'OverlapRefTar','popupmenu','Yes|No',...
         'OverlapRefIdx','edit','1',...
         'TargetIdxFreq','edit','1',...
         'TargetChannel','edit','1',...
         ...%'CatchIdxFreq','edit','0',... LAS: Depricated, replaced with IsCatch
         'IsCatch','edit','0',...,
         'RemoveSecondTarget', 'popupmenu', 'Yes|No',...
         'CatchModifications','edit','',...
         'RepeatMisses','popupmenu','Yes|No',...
         'ReminderTarget','edit',0,...
         'OnsetRampTime','edit',0,...
         'TargetMatchContour','popupmenu','Yes|No',...
         'TargetMode','popupmenu','Waveform|Envelope|Carrier|RefDependentWaveform',...
         'EnvTarRefRatio','edit',0,...
         'EnvTarRefRampDur','edit',.01,...
         'PostTrialSilence','edit',1,...
         'BalanceTargetsByReftypeAndReflen','popupmenu','No|Yes',...
         'LightPulseRate','edit',0,...
         'LightPulseDuration','edit',0,...
         'LightPulseShift','edit',0,...
         'LightEpoch','popupmenu','Sound|SoundOnset|WholeTrial|Target|PostTrial',...
         'LightConditions','popupmenu','Interleaved|All_on|All_off|Interleave8020',...
         'SaveData','popupmenu','Yes|No',...
         'SelfTestMode','popupmenu','No|Yes',...
         'TargetDistSet','edit',1,...
         'Tar2SegCountFreq','edit',0,...
         'Tar2SegmentLen','edit',0,...
         'Tar1AloneOffsetSegCountFreq','edit',0,...
         'TargetCount','edit',50,...
         'RefLenCount','edit',50,...
        };

    RefTarO = class(RefTarO,'MultiRefTar');
    RefTarO = ObjUpdate(RefTarO);
case 1
    % if single argument of class SoundObject, return it
    if isa(varargin{1},'ReferenceTarget')
        RefTarO = varargin{1};
    else
        error('Wrong argument type');
    end
otherwise 
    error('Wrong number of input arguments');
end
