function [exptparams] = RandomizeSequence (o, exptparams, globalparams, RepIndex, RepOrTrial)
% Function Trial Sequence is one of the methods of class ReferenceTarget
% This function is responsible for producing the random sequence of
% references, targets in each trial. Called at the beginning of each
% repetition.
%
% SVD 2011-06-06, ripped off of Reference Target
%

persistent RefLenSet Tar2OffsetSet
global PlayCue

if ~exist('RepOrTrial','var'), RepOrTrial = 0;end   % default is its a trial call
if ~exist('RepIndex','var'), RepIndex = 1;end
if isempty(PlayCue)
    PlayCue=false;
end

balance_ref_types_by_Nref=1;
%balance_targets_by_reftype_and_reflen=false; Now a parameter of the MultiRefTar object

% read the trial parameters
par = get(o);
bpar = get(exptparams.BehaveObject);
if strcmp(o.SelfTestMode,'Yes')
    pos=get(exptparams.FigureHandle,'Position');
    screen=get(0,'ScreenSize');
    pos(1)=screen(3)-pos(3);
    pos(2)=45;
    set(exptparams.FigureHandle,'Position',pos);
    
   bps=uicontrol('style','pushbutton','string','','FontSize',10,...
        'Units','normalized','Position',[.01 .04 .69 .88],...
        'Parent',exptparams.FigureHandle,'Tag','BlankDisplay','Enable','off');
end

ReferenceMaxIndex = par.ReferenceMaxIndex;
TargetMaxIndex = par.TargetMaxIndex;
if strcmpi(o.BalanceTargetsByReftypeAndReflen,'Yes')
    rcf_new=round(par.ReferenceCountFreq*par.RefLenCount)/par.RefLenCount;
    if any(rcf_new~=par.ReferenceCountFreq)
        fprintf(['**Rounding MultiRefTar.ReferenceCountFreq from [%s] to [%s] ',...
            'because BalanceTargetsByReftypeAndReflen is set to Yes, and this ',...
            'will ensure integer numbers of each reference length.\n'],...
            regexprep(num2str(par.ReferenceCountFreq),' +',' '),...
            regexprep(num2str(rcf_new),' +',' '));
        par.ReferenceCountFreq=rcf_new;
    end
end
ReferenceCountFreq=par.ReferenceCountFreq./sum(par.ReferenceCountFreq);
if strcmpi(o.BalanceTargetsByReftypeAndReflen,'Yes')
    tif_new=round(par.TargetIdxFreq*par.TargetCount)/par.TargetCount;
    if any(tif_new~=par.TargetIdxFreq)
        fprintf(['**Rounding MultiRefTar.TargetIdxFreq from [%s] to [%s] ',...
            'because BalanceTargetsByReftypeAndReflen is set to Yes, and this ',...
            'will ensure integer numbers of each reference length.\n'],...
            regexprep(num2str(par.TargetIdxFreq),' +',' '),...
            regexprep(num2str(tif_new),' +',' '));
        par.TargetIdxFreq=tif_new;
    end
end
TargetIdxFreq=ifstr2num(par.TargetIdxFreq);
CatchIdxFreq=ifstr2num(par.CatchIdxFreq);
Tar2SegCountFreq=ifstr2num(par.Tar2SegCountFreq);
if ~isequal(Tar2SegCountFreq,0)
    Tar2SegCountFreq=Tar2SegCountFreq./sum(Tar2SegCountFreq);
end

RefIdx=1:ReferenceMaxIndex;
if isempty(TargetIdxFreq),
    TargetIdxFreq=1;
elseif length(TargetIdxFreq)>TargetMaxIndex,
    TargetIdxFreq=TargetIdxFreq(1:TargetMaxIndex);
end
if sum(TargetIdxFreq)==0,
    TargetIdxFreq(:)=1;
end
if all(par.TargetDistSet==1)
    two_target=false;
    TargetIdxFreq=TargetIdxFreq./sum(TargetIdxFreq);
elseif all(par.TargetDistSet<=2 & par.TargetDistSet>0)
    two_target=true;
    for tari=1:2
        idx=par.TargetDistSet==tari;
        TargetIdxFreq(idx)=TargetIdxFreq(idx)./sum(TargetIdxFreq(idx));
    end
else
    error(['Invalid TargetDistSet = [',num2str(par.TargetDistSet),']. This is only set up for up to two targets.'])
end
nRefTypes=1;
if ismethod(o.ReferenceHandle,'get_target_suffixes')
    un_ref_types=get_target_suffixes(o.ReferenceHandle);
    nRefTypes=length(un_ref_types);
    nRefTypesBal=2;
end
if isempty(CatchIdxFreq)
    CatchIdxFreq=0;
end

if isfield(exptparams,'TotalTrials')
    TotalTrials=exptparams.TotalTrials;
else
    TotalTrials=0;
end

ignore_these_beh_objects={'Passive','ClassicalConditioning'};
skip_repeat = 0;
force_repeat = 0;
% special cases, depending on idiosyncracies of BehaviorObject
if any(strcmpi(exptparams.BehaveObjectClass,ignore_these_beh_objects))
    skip_repeat = 1;
end
if strcmpi(exptparams.BehaveObjectClass,'RewardTargetLBHB') && ...
        strcmpi(get(exptparams.BehaveObject,'PunishFA'), 'No')
    skip_repeat = 1;
end

if skip_repeat && (RepOrTrial == 0)
    if PlayCue && strcmpi(exptparams.BehaveObjectClass,'ClassicalConditioning')
        trialidx=exptparams.InRepTrials;   
        exptparams = insertCue(o,exptparams,trialidx);
        PlayCue=false;
    end
    return
elseif RepOrTrial == 0
    trialidx=exptparams.InRepTrials;   
    
    if strcmpi(exptparams.BehaveObjectClass,'RewardTargetLBHB') && bpar.RepeatUnrewardedHitN>0
        % UnrewardedHitStreak is streak preceeding this trial
        % thus the > and not >=
        if  bpar.RepeatUnrewardedHitN > bpar.UnrewardedHitStreak && ...
                strcmpi(exptparams.Performance(TotalTrials).ThisTrial,'Hit')
            LastTargetIndex = find(exptparams.Performance(TotalTrials).uHit==1); %LAS: fix for multiple targets, finds the one that was hit
            PumpDurPerTarget = bpar.PumpDuration;
            LastTargetIndex = min(LastTargetIndex, length(PumpDurPerTarget));
            if PumpDurPerTarget(LastTargetIndex)==0
                % hit on unrewarded trial. Treat as False alarm and repeat?
                exptparams.BehaveObject = set(exptparams.BehaveObject,'UnrewardedHitStreak', bpar.UnrewardedHitStreak+1);
                fprintf('Hit on unrewarded target %d/%d, repeating\n', bpar.UnrewardedHitStreak+1, bpar.RepeatUnrewardedHitN);
                force_repeat = 1;
            end
            
        elseif  bpar.RepeatUnrewardedHitN >= bpar.UnrewardedHitStreak && ...
                strcmpi(exptparams.Performance(TotalTrials).ThisTrial,'FA')
            % FA. In this case, don't want to reset unrewarded hit streak to 0
            LastTargetIndex = par.TargetIndices{trialidx};
            PumpDurPerTarget = bpar.PumpDuration;
            LastTargetIndex = min(LastTargetIndex, length(PumpDurPerTarget));
            if PumpDurPerTarget(LastTargetIndex)==0
                fprintf('FA on unrewarded target trial %d/%d, repeating\n', bpar.UnrewardedHitStreak+1, bpar.RepeatUnrewardedHitN);
            end
            
        else
            if bpar.UnrewardedHitStreak~=0
                %If it's already 0 no need to add more text to the screen.
                exptparams.BehaveObject = set(exptparams.BehaveObject,'UnrewardedHitStreak',0);
                fprintf('Reset UnrewardedHitStreak to 0 \n');
            end
        end
    end
    if strcmpi(exptparams.BehaveObjectClass,'RewardTargetLBHB') &&...
            sum(bpar.PumpDuration==0) > 0 && bpar.RepeatUnrewardedHitN>0
        LastTargetIndex = par.TargetIndices{trialidx};
            
        LastTargetUnrewarded = (bpar.PumpDuration(LastTargetIndex)==0);
        if LastTargetUnrewarded && ...
                strcmpi(exptparams.Performance(TotalTrials).ThisTrial,'Miss')
            %ACTUALLY A CORR REJ BC not supposed to respond to unrewarded
            %target
            skip_repeat=1;
            fprintf('Correctly rejected unrewarded target, not repeating.\n');
        end
    end
    
    % figure out if the last trial was a cue trial.
    TotalHits = exptparams.Performance(end).Hit(1);
    if TotalHits <= par.CueTrialCount
        LastTrialWasCue = 1;
    else
        LastTrialWasCue = 0;
    end
    ignored_miss=0;
    if ~force_repeat && ...
            ( skip_repeat ||...
            (strcmpi(exptparams.Performance(TotalTrials).ThisTrial,'Hit') && ~any(o.IsCatch_(exptparams.Performance(TotalTrials).uHit==1))) || ... % a hit but not a Catch Hit
            strcmpi(exptparams.Performance(TotalTrials).ThisTrial,'Corr.Rej.'))
        % either passive or last trial was correct. either way, we don't need
        % to adjust anything
        % Also disable for Classical Conditioning because trials should not
        % be adjusted to reflect behavioral performance.
        if ~par.CueSeg(trialidx)
            o.CatchHitStreak=0;
        end
        exptparams.TrialObject = o;
    else
        % special case: catch trial where responded ("missed") after Catch
        
        % was either a false alarm or miss, need to repeat current trial
        if ~isempty(par.TargetIndices{trialidx}) && ...
                (strcmpi(exptparams.Performance(TotalTrials).ThisTrial,'Miss') || ...
                (two_target && ...
                 isfinite(exptparams.Performance(TotalTrials).CatResponseWinStart) && ...
                 any(exptparams.Performance(TotalTrials).uHit==0) && ...
                 exptparams.Performance(TotalTrials).FalseAlarm==1) ...
                ) %FA (lights turned out), but missed first target so count as a miss for purposes of deciding what to do on the next trial.
            if ~par.CueSeg(trialidx)
                o.CatchHitStreak=0;
            end
            if strcmp(o.RepeatMisses,'No') && ~par.CueSeg(trialidx)
                ignored_miss=1;
                exptparams.TrialObject = o;
            else
                NewNumberOfTrials=par.NumberOfTrials+1;
                if par.CueSeg(trialidx)
                    %Miss on a cue trial, insert trial with same parameters (including target index)
                    %into random place within cue trials (first few trials)
                    last_cue_trial=find(par.CueSeg,1,'last');
                    newslot=ceil(rand*(last_cue_trial+1-trialidx))+trialidx;
                    NewTargetIndex=par.TargetIndices{trialidx};
                    NewTargetIndices={par.TargetIndices{1:(newslot-1)} ...
                        NewTargetIndex ...
                        par.TargetIndices{newslot:end}};
                    fprintf(['Miss on cue trial: Inserting reference and target '...
                        'into trial %d.\n'],newslot);
                else
                    %Miss on  non-cue trial, insert trial with same reference parameters
                    %into random place between current trials and end of sequence. Pull new target
                    newslot=ceil(rand*(NewNumberOfTrials-trialidx))+trialidx;
                    [o,NewTargetIndex]=get_TI_from_TISet(o);
                    if 0
                        %Keep this target
                        NewTargetIndices=[par.TargetIndices,NewTargetIndex];
                        fprintf(['Miss: Inserting reference into trial %d, ',...
                            'pulling new targetindex=%d from TarIdxSet and appending ',...
                            'to end of TargetIndices.\n'],...
                            newslot,NewTargetIndex);
                    else
                        %Pull new target
                        NewTargetIndices={par.TargetIndices{1:(newslot-1)} ...
                            NewTargetIndex ...
                            par.TargetIndices{newslot:end}};
                        fprintf(['Miss: Inserting reference into trial %d, ',...
                            'pulling new targetindex=%d from TarIdxSet and inserting ',...
                            'into same trial.\n'],...
                            newslot,NewTargetIndex);
                    end
                    
                end
            end
        else %FALSE ALARM
            NewNumberOfTrials=par.NumberOfTrials+1;
            NewTargetIndex=par.TargetIndices{trialidx};
            newslot=trialidx+1;
            NewTargetIndices={par.TargetIndices{1:(newslot-1)} ...
                NewTargetIndex ...
                par.TargetIndices{newslot:end}};
            if par.CueSeg(trialidx)==1
                cuestr=' on cue trial';
            elseif par.CueSeg(trialidx)==2
                cuestr=' on cue2 trial';
            else
                cuestr='';
            end
            if strcmpi(exptparams.Performance(TotalTrials).ThisTrial,'Hit') && any(o.IsCatch_(exptparams.Performance(TotalTrials).uHit==1))
                FAs='FA to catch';
                o.CatchHitStreak=o.CatchHitStreak+1;
            else
                FAs='FA';
                %o.CatchHitStreak=0; Don't change catch hit streak
            end
            fprintf('%s%s: repeating and upping trial count from %d to %d\n',...
                FAs,cuestr,par.NumberOfTrials,NewNumberOfTrials);
        end
        
        if ~ignored_miss
            NewReferenceIndices={par.ReferenceIndices{1:(newslot-1)} ...
            par.ReferenceIndices{trialidx} ...
            par.ReferenceIndices{newslot:end}};
        NewCatchIndices={par.CatchIndices{1:(newslot-1)} ...
            par.CatchIndices{trialidx} ...
            par.CatchIndices{newslot:end}};
        NewCatchSeg=[par.CatchSeg(1:(newslot-1));  ...
            par.CatchSeg(trialidx);  ...
            par.CatchSeg(newslot:end)];
        NewCueSeg=[par.CueSeg(1:(newslot-1));  ...
            par.CueSeg(trialidx);  ...
            par.CueSeg(newslot:end)];
        
        do_ObjUpdate=false;
        o=set(o,'ReferenceIndices',NewReferenceIndices,do_ObjUpdate);
        o=set(o,'TargetIndices',NewTargetIndices,do_ObjUpdate);
        o=set(o,'CatchIndices',NewCatchIndices,do_ObjUpdate);
        o=set(o,'CatchSeg',NewCatchSeg,do_ObjUpdate);
        o=set(o,'CueSeg',NewCueSeg,do_ObjUpdate);
        % svd fixed bug 2014-11-24 where RefDuration was assigned to the
        % next trail rather than the new slot for misses.
        o=set(o,'TarOnset',...
            [par.TarOnset(1:(newslot-1)) ...
            par.TarOnset(trialidx) ...
            par.TarOnset(newslot:end)],do_ObjUpdate);
        o=set(o,'LightTrial',...
            [par.LightTrial(1:(newslot-1)) ...
            par.LightTrial(trialidx) ...
            par.LightTrial(newslot:end)],do_ObjUpdate);
        o=set(o,'NumberOfTrials',par.NumberOfTrials+1,do_ObjUpdate);
        exptparams.TrialObject = o;
        end
    end
    
    if PlayCue
        exptparams = insertCue(o,exptparams,trialidx);
        PlayCue=false;
    end
    
    return;
end


% overlaprefset -- if specific references will overlap with the target (for
% target in noise)
if isnumeric(par.OverlapRefIdx)
    OverlapRefSet=par.OverlapRefIdx;
else
    if ~isempty(strtrim(par.OverlapRefIdx)),
        OverlapRefSet=eval(par.OverlapRefIdx);
    else
        OverlapRefSet=[];
    end
end
OverlapRefSet=OverlapRefSet(OverlapRefSet>0 & OverlapRefSet<=length(RefIdx));


% if Count of ref or tar is less than 10, repeat so that it's
% closer to 10.  not really a "repetition" any more, but this
% allows full length trials.
ReferenceCount=length(RefIdx);
if 10/ReferenceCount>=2
    repcount=floor(10./ReferenceCount);
else
    repcount=1;
end
RefIdxSet=[];

if strcmpi(par.OverlapRefTar,'Yes') && ~isempty(OverlapRefSet),
    RefIdx=cat(2,RefIdx,repmat(OverlapRefSet,[1 5]));
elseif isempty(OverlapRefSet)
    OverlapRefSet=RefIdx;
end

for ii=1:repcount
    RefIdxSet=[RefIdxSet shuffle(RefIdx)];
end

% Figure out pool of trial lengths
if ~TotalTrials
    RefLenSet=[];
    Tar2OffsetSet=[];
end

% make a big pool of targets with appropriate fraction of each type. Pool
% of targets carries over across Reference repetitions (in par.TarIdxSet)
% to ensure even sampling.
% This only happens once (the first time RandomizeSequence is called)
if TotalTrials == 0
    if(length(TargetIdxFreq)>1)
        if(length(unique(TargetIdxFreq))==1) && ~two_target
            o.TargetCount=ceil(o.TargetCount/length(TargetIdxFreq))*length(TargetIdxFreq);
            o.TarIdx=repmat(1:length(TargetIdxFreq),1,o.TargetCount/length(TargetIdxFreq));
            o.shuffle_block_TarIdx=length(TargetIdxFreq);
        else
            if two_target
                %shuffle blocks that will have an integer number of Each Target
                good_shuffle_blocks_tar1=find(all(mod(repmat(TargetIdxFreq(par.TargetDistSet==1)',1,o.TargetCount).*repmat(1:o.TargetCount,sum((par.TargetDistSet==1)),1),1)<10*eps));
                good_shuffle_blocks_tar2=find(all(mod(repmat(TargetIdxFreq(par.TargetDistSet==2)',1,o.TargetCount).*repmat(1:o.TargetCount,sum((par.TargetDistSet==2)),1),1)<10*eps));
                if(isempty(good_shuffle_blocks_tar1) || isempty(good_shuffle_blocks_tar2))
                    o.shuffle_block_TarIdx=o.TargetCount;
                    error('No good shuffle blocks found. Make this work.')
                else
                    sb1=good_shuffle_blocks_tar1(1);
                    sb2=good_shuffle_blocks_tar2(1);
                    o.shuffle_block_TarIdx= sb1 * sb2; %lcm(sb1,sb2);
                end
                tar1freq=TargetIdxFreq(par.TargetDistSet==1);
                tar2freq=TargetIdxFreq(par.TargetDistSet==2);
                Tar1Idx=[];
                for t1i=1:length(tar1freq)
                    Tar1Idx=cat(2,Tar1Idx,ones(1,round(sb1.*tar1freq(t1i))).*t1i);
                end
                Tar2Idx=[];
                for t2i=1:length(tar2freq)
                    Tar2Idx=cat(2,Tar2Idx,ones(1,round(sb2.*tar2freq(t2i))).*(t2i+length(tar1freq)));
                end
                uTar1Idx=unique(Tar1Idx);
                uTar2Idx=unique(Tar2Idx);
                uTarIdx=cell(1,length(uTar1Idx)*length(uTar2Idx));
                for ii=1:length(uTar1Idx)
                    for jj=1:length(uTar2Idx)
                         uTarIdx{(ii-1)*length(uTar2Idx)+jj}=[uTar1Idx(ii) uTar2Idx(jj)];
                    end
                end
                TarIdx=cell(1,o.shuffle_block_TarIdx);
                for ii=1:length(Tar1Idx)
                    for jj=1:length(Tar2Idx)
                        TarIdx{(ii-1)*length(Tar2Idx)+jj}=[Tar1Idx(ii) Tar2Idx(jj)];
                    end
                end
                o.TarIdx=repmat(TarIdx,1,ceil(o.TargetCount/o.shuffle_block_TarIdx));
                o.TargetCount=length(o.TarIdx);
                
                %If wanted, remove 2nd target if there are no catch targets in the trial 
                if strcmp(o.RemoveSecondTarget, 'Yes')
                    for ii = 1:length(o.TarIdx)
                       if ~any(o.IsCatch_(o.TarIdx{ii}))
                           % None of the "targets" are catches. Keep the
                           % first target only
                           o.TarIdx{ii} = o.TarIdx{ii}(1);
                       end
                    end
                end
                
            else
                %shuffle blocks that will have an integer number of Each Target
                good_shuffle_blocks=find(all(mod(repmat(TargetIdxFreq',1,o.TargetCount).*repmat(1:o.TargetCount,length(TargetIdxFreq),1),1)<10*eps));
                if(isempty(good_shuffle_blocks))
                    o.shuffle_block_TarIdx=o.TargetCount;
                else
                    o.shuffle_block_TarIdx=good_shuffle_blocks(1);
                end
                TarIdx_=[];
                for ii=1:length(TargetIdxFreq),
                    TarIdx_=cat(2,TarIdx_,ones(1,round(o.shuffle_block_TarIdx.*TargetIdxFreq(ii))).*ii);
                end
                o.TarIdx=repmat(TarIdx_,1,ceil(o.TargetCount/o.shuffle_block_TarIdx));
                o.TargetCount=length(o.TarIdx);
                if(isempty(good_shuffle_blocks))
                    o.shuffle_block_TarIdx=o.TargetCount;
                    warning(['No shuffle block length was found that could satisfy the ',...
                        'TargetIdxFreq given, [',num2str(TargetIdxFreq),']. Shuffling all ',...
                        '%d target indicies together'],o.TargetCount)
                end
            end
        end
    else
        for ii=1:length(TargetIdxFreq)
            o.TarIdx=cat(2,o.TarIdx,ones(1,round(o.TargetCount.*TargetIdxFreq(ii))).*ii);
        end
        o.shuffle_block_TarIdx=o.TargetCount;
    end
end

% same for catch targets
CatchCount=max(o.TargetCount,ReferenceCount);
CatchIdx=[];
for ii=1:length(CatchIdxFreq)
    CatchIdx=cat(2,CatchIdx,ones(1,round(CatchCount.*CatchIdxFreq(ii))).*ii);
end
CatchIdxSet=par.CatchIdxSet;
bCatchTrial=zeros(CatchCount,1);
bCatchTrial(1:round(sum(CatchIdxFreq).*CatchCount))=1;
bCatchTrial=shuffle(bCatchTrial);


%Figure out which target to use for cues
% This only happens once (the first time RandomizeSequence is called)
if TotalTrials == 0
    switch o.CueTar 
        case 'MaxPumpDur' 
            if strcmpi(exptparams.BehaveObjectClass,'RewardTargetLBHB')
                PumpDuration = get(exptparams.BehaveObject,'PumpDuration');
                if sum(PumpDuration)>0
                    max_reward_idx = find(PumpDuration==max(PumpDuration),1, 'first');
                else
                    max_reward_idx=1;
                end
            else
                warning('CueTar=''MaxPumpDur'' is only set up for RewardTargetLBHB, defaulting to the first target.')
                max_reward_idx=1;
            end
            if two_target
                o.CueTarIdx = uTarIdx{find(cellfun(@(x)any(x==max_reward_idx),uTarIdx),1)};
                if strcmp(o.RemoveSecondTarget, 'Yes')
                    o.CueTarIdx = o.CueTarIdx(1);
                end
            else
                o.CueTarIdx = max_reward_idx;
            end
        case 'MaxTarIdxFreq'
            if two_target
                [unti,~,inds]=uniquecell(o.TarIdx);
                NperTar=arrayfun(@(x)sum(inds==x),1:length(unti));
                [~,tmaxidx]=max(NperTar);
                o.CueTarIdx = unti{tmaxidx};
            else
                tmaxidx=find(o.TargetIdxFreq==max(o.TargetIdxFreq),1);
                o.CueTarIdx = find(o.TarIdx==tmaxidx,1);
            end
        otherwise
            try
                if contains(o.CueTar,'_')
                    o.CueTarIdx = str2num(strrep(o.CueTar,'_',' '));
                else
                    o.CueTarIdx = str2double(o.CueTar);
                end
                if isnan(o.CueTarIdx)
                    error('Error turning string MultiRefTar.CueTar into number MultiRefTar.CueTarIdx')
                end
            catch err
                rethrow(err)
            end
    end
end


%create reference length set
good_shuffle_blocks=find(all(mod(repmat(ReferenceCountFreq',1,o.RefLenCount).*repmat(1:o.RefLenCount,length(ReferenceCountFreq),1),1)<10*eps));
if(isempty(good_shuffle_blocks))
    o.shuffle_block_RefLen=o.RefLenCount;
    warning(['No shuffle block length was found that could satisfy the ',...
        'ReferenceCountFreq given, [',num2str(ReferenceCountFreq),']. Shuffling all ',...
        '%d reference count indicies together'],o.RefLenCount)
else
    o.shuffle_block_RefLen=good_shuffle_blocks(1);
end
RefLen_=[];
for ii=1:length(ReferenceCountFreq),
    RefLen_=cat(2,RefLen_,ones(1,round(o.shuffle_block_RefLen.*ReferenceCountFreq(ii))).*ii);
end
if balance_ref_types_by_Nref && nRefTypes>1
    %ref_types=get_target_suffixes(o.ReferenceHandle,RefIdxSet);
    %     RefLen_=repmat(RefLen_,1,nRefTypes);
    %     o.shuffle_block_RefLen=o.shuffle_block_RefLen*nRefTypes;
    while length(RefIdxSet) < length(RefLen_)*nRefTypes
        RefIdxSet=[RefIdxSet shuffle(RefIdx)];
    end
end

RefLen=repmat(RefLen_,1,ceil(o.RefLenCount/o.shuffle_block_RefLen));

good_shuffle_blocks=find(all(mod(repmat(Tar2SegCountFreq',1,o.RefLenCount).*repmat(1:o.RefLenCount,length(Tar2SegCountFreq),1),1)<10*eps));
if(isempty(good_shuffle_blocks))
    o.shuffle_block_Tar2Offset=o.RefLenCount;
    warning(['No shuffle block length was found that could satisfy the ',...
        'Tar2SegCountFreq given, [',num2str(Tar2SegCountFreq),']. Shuffling all ',...
        '%d target 2 offset indicies together'],o.RefLenCount)
else
    o.shuffle_block_Tar2Offset=good_shuffle_blocks(1);
end
if two_target
    Tar2Offset_=[];
    for ii=1:length(Tar2SegCountFreq)
        Tar2Offset_=cat(2,Tar2Offset_,ones(1,round(o.shuffle_block_Tar2Offset.*Tar2SegCountFreq(ii))).*ii);
    end
    Tar2Offset=repmat(Tar2Offset_,1,ceil(o.RefLenCount/o.shuffle_block_Tar2Offset));
end
trialidx=0;
RefTrialIndex={};
TargetIndex={};
CatchIndex={};
CatchSeg=[];
CueSeg=[];
RefIdxSetMatched=[];
if(RepIndex==1)
    init=true;
else
    init=false;
end
add_trial=true;
alls=[];
while add_trial
    % generate sequences of references for each trial, making as many trials
    % as required to deplete the pool in RefIdxSet
    trialidx=trialidx+1;
    
    % make sure the vector of reference lengths is not empty
    if (balance_ref_types_by_Nref && nRefTypes>1 ) || strcmpi(o.BalanceTargetsByReftypeAndReflen,'Yes')
        if isempty(par.SingleRefSegmentLen) || par.SingleRefSegmentLen==0,
            error('Figure this out for experiment designs with multiple references for trial')
        else
            generate_RefLenSet=length(RefLenSet)<1;
        end
        if generate_RefLenSet
            %decimate RefLen
                dec_fact=nRefTypes;
                dec_start=mod(RepIndex,dec_fact);
                if dec_start==0, dec_start=dec_fact; end
                RefLenDec=RefLen(dec_start:dec_fact:end);
                
            if (isempty(par.SingleRefSegmentLen) || par.SingleRefSegmentLen==0)
                if nRefTypes==1
                    RefLenSet=cat(2,RefLenSet,shuffle(RefLen));
                    RefIdxSetMatched=RefIdxSet;
                else
                    error('Figure this out if needed (If you have multiple reference types but more than one reference per trial and you want to balance them in some way).')%LAS
                end
            else
                refren_shuf_block=10;
                reftype_shuf_block=nRefTypes*2;
                nBlocks=ceil(length(RefLenDec)/refren_shuf_block);
                max_match_shuf_block = ceil(length(RefLenDec)/nBlocks);
                block_order=randperm(nBlocks);
                for ti=2:nRefTypes
                    block_order(ti,:)=circshift(block_order(1,:),ti-1);
                end
                for bn=1:nBlocks
                    r=nan(nRefTypes,max_match_shuf_block,2);
                    for ti=1:nRefTypes
                        RefLenDecBlock=RefLenDec(block_order(ti,bn):nBlocks:end);
                        if nRefTypes==1
                            inds=1:length(RefLenDecBlock);
                        else
                            ref_types=get_target_suffixes(o.ReferenceHandle,RefIdxSet);
                            inds=find(strcmp(ref_types,un_ref_types(ti)),length(RefLenDecBlock));
                        end
                        
                        if length(inds) < length(RefLenDecBlock)
                            error(['Did''t find enough references of type ',un_ref_types{ti},' in RefIdxSet'])
                        end
                        RefIdxs = RefIdxSet(inds);
                        RefIdxSet(inds)=[];
                        r(ti,1:length(RefLenDecBlock),:)=...
                            shuffle_blocked([RefLenDecBlock;RefIdxs]',length(RefLenDecBlock));
                    end
                    r=reshape(r,nRefTypes*max_match_shuf_block,2);
                    r(isnan(r(:,1)),:)=[];
                    r=shuffle_blocked(r,reftype_shuf_block,1)';
                    RefLenSet=cat(2,RefLenSet,r(1,:));
                    RefIdxSetMatched=cat(2,RefIdxSetMatched,r(2,:));
                end
                if length(RefLenSet) ~= length(o.TarIdx) && strcmpi(o.BalanceTargetsByReftypeAndReflen,'Yes')
                    error('RefLenSet and TarIdx are not matched in length, so they can''t be balanced. Adjust probabilities to make them balanced.')
                end
            end
        end
    else
        RefLenDec=RefLen;
        dec_fact=1;
        if length(RefLenSet)<o.RefLenCount,
            r=shuffle_blocked(RefLen,o.shuffle_block_RefLen);
            RefLenSet=cat(2,RefLenSet,r);
        end
    end
    if two_target && length(Tar2OffsetSet)<1
        Tar2OffsetSet=shuffle_blocked(Tar2Offset,o.shuffle_block_Tar2Offset);
    end
    
    if isempty(CatchIdxSet),
        CatchIdxSet=shuffle(CatchIdx);
    end    
    % placeholders for possible catch stimuli
    CatchIndex{trialidx}=[];
    CatchSeg(trialidx,1)=0;
    
    %
    % choose number of references for this trial -- refsegcount
    if trialidx<=par.CueTrialCount(1) && ~TotalTrials
        % this is a cue trial. Use special target and don't deplete RefIdxSet
        CueSeg(trialidx,1)=1;
        CueStr='Cue ';
        trcf=ReferenceCountFreq(1:(end-1));
        trcf=trcf./sum(trcf);
        %refsegcount=find(rand>[0 cumsum(trcf)*1.7], 1, 'last' ); %LAS: This part is still hacky. With this line I was giving a little randomness into the reference length for cue trials.
        refsegcount=find(trcf, 1); %With this line I just make it always the shortest possible reference.
        rmUsedFromSets=false;
        if two_target
            if ~isempty(o.Tar1AloneOffsetSegCountFreq) && o.Tar1AloneOffsetSegCountFreq~=0
                refsegcount=refsegcount-find(o.Tar1AloneOffsetSegCountFreq,1,'last')+1;
                Tar2OffsetCount=0;%Tar2Offset(end);
            else
                Tar2OffsetCount=0;%Tar2Offset(end);
            end
        end
    elseif length(par.CueTrialCount)>1 && trialidx<=sum(par.CueTrialCount) && ~TotalTrials
        CueSeg(trialidx,1)=2;
        CueStr='Cue2 ';
        trcf=ReferenceCountFreq(1:(end-1));
        trcf=trcf./sum(trcf);
        refsegcount=find(trcf);
        %For each Cue2 trial, start the reference length at the shortest possible length, then it one step longer with each Cue2 trial. 
        cue2ind=sum(CueSeg(1:trialidx)==2);
        if cue2ind>length(refsegcount), cue2ind=length(refsegcount); end %Keep it at the max possible length if you get there.
        refsegcount=refsegcount(cue2ind);
        rmUsedFromSets=false;
        if two_target
            if ~isempty(o.Tar1AloneOffsetSegCountFreq) && o.Tar1AloneOffsetSegCountFreq~=0
                refsegcount=refsegcount-find(o.Tar1AloneOffsetSegCountFreq,1,'last')+1;
                Tar2OffsetCount=0;%Tar2Offset(end);
            else
                Tar2OffsetCount=0;%Tar2Offset(end);
            end
        end        
        
    else
        CueSeg(trialidx,1)=0;
        CueStr='';
        refsegcount=RefLenSet(1);
        if two_target
            Tar2OffsetCount=Tar2OffsetSet(1);
        end
        %RefLenSet=RefLenSet(2:end);
        %LAS 4/26/19: remove below so matching can happen with targets if wanted
        rmUsedFromSets=true;
        
        % catch stim only possible in non-cue trials
        if rand<sum(CatchIdxFreq),
            % this trial gets a catch stimulus
            CatchIndex{trialidx}=CatchIdxSet(1);
            CatchIdxSet=CatchIdxSet(2:end);
            if refsegcount<max(find(ReferenceCountFreq>0)),
                refsegcount=refsegcount+1;
            end
            CatchSeg(trialidx,1)=refsegcount-ceil(rand*3);
            CatchStr=['Catch=' mat2str(CatchIndex{trialidx})];
        end
    end
    
    %
    % convert refsegcount to refcount. trivial if fixed-length references
    if isempty(par.SingleRefSegmentLen) || par.SingleRefSegmentLen==0
        % fixed length references
        refcount=refsegcount;
        if two_target
            TarOnset{trialidx}=refsegcount+1;
            TarOnset{trialidx}(2)=TarOnset{trialidx}+Tar2OffsetCount;  % +1;  % svd not adding 1
            refcount=refcount+Tar2OffsetCount-1;
%             if exist('single_tar_trials','var') && any(single_tar_trials==trialidx)
%                 single_tar=true;
%             else
                  single_tar=false;
%             end
%             if trialidx>1 && any(isnan(o.TargetChannel)) && single_tar %If this isn't a cue only trial (Target 1 only), add 2nd target at
%                 TarOnset{trialidx}(2)=TarOnset{trialidx}(2)+Tar1_offsets(single_tar_trials==trialidx)*par.SingleRefSegmentLen;
%             end
            if TarOnset{trialidx}(2)<1
                error('Tar2Onset is less than 1!')
            end
            if single_tar
                RefSegStr=sprintf(' TarSlot=%g',TarOnset{trialidx});
            else
                RefSegStr=sprintf(' TarSlots=[%g %g]',TarOnset{trialidx});
            end

        else
            TarOnset{trialidx}=0;
            RefSegStr='';
        end

    else
        refcount=1;
        if two_target
            %LAS: This section is confusing because there are two ways to get trials that have only 1 target in them.
            %1: par.TargetChannel is set to NaN for one of the targets. Every time this "target" is selected, nothing is 
            %    actually played (but the cue target is played). This is the HCT scheme
            %2: par.RemoveSecondTarget is set to 'Yes', so the targets from the 2nd target distribution (Cue targets) 
            %    are removed for trials that don't contain catches (see line 446)
            % In either case single_tar_trials(trialidx) should equal 1 for these single-tar trials (set in line 854)
            TarOnset{trialidx}=par.SingleRefSegmentLen.*refsegcount;
            TarOnset{trialidx}(2)=TarOnset{trialidx}+par.Tar2SegmentLen*Tar2OffsetCount;
            if exist('single_tar_trials','var') && any(single_tar_trials==trialidx)
                single_tar=true;
                if length(o.TarIdxSet{1})==1
                    TarOnset{trialidx}(2)=[];
                end
            else
                single_tar=false;
            end
            if trialidx>1 && any(isnan(o.TargetChannel)) && single_tar
                %Cue-only trial (HCT-style). Adjust Cue by Tar1_offsets. (Make it earlier to keep the animal from relying on waiting until the cue)
                TarOnset{trialidx}(2)=TarOnset{trialidx}(2)+Tar1_offsets(single_tar_trials==trialidx)*par.SingleRefSegmentLen;
            end
            if length(TarOnset{trialidx})>1 && TarOnset{trialidx}(2)<0
                error('Tar2Onset is less than 0!')
            end
            if ~single_tar && strcmpi(exptparams.BehaveObjectClass,'RewardTargetLBHB') && abs(diff(TarOnset{trialidx})) < get(exptparams.BehaveObject,'ResponseWindow') && ~CueSeg(trialidx)
                warning('Second target overlaps with the first! Check distributions and segment lengths to make sure they''re set up right. You might want this but BehaviorControl and PerformanceAnalysis aren''t set up to do this yet :-)')
            end
            if single_tar
                RefSegStr=sprintf(' RefLen=%g sec, TarOnset=%g sec',TarOnset{trialidx},TarOnset{trialidx});
            else
                RefSegStr=sprintf(' RefLen=%g sec, TarOnsets=%g, %g sec',min(TarOnset{trialidx}),TarOnset{trialidx});
            end
        else
            TarOnset{trialidx}=round(par.SingleRefSegmentLen.*refsegcount,10); %round because otherwise you get rounding errors (SRSL 0.4*6=2.4+e-16 )
            RefSegStr=sprintf(' RefLen=%g sec',TarOnset{trialidx});
        end
    end
    
    %
    %Assign which references will be played for this trial
    if (balance_ref_types_by_Nref && nRefTypes>1) || (strcmpi(o.BalanceTargetsByReftypeAndReflen,'Yes') && (~isempty(par.SingleRefSegmentLen) && par.SingleRefSegmentLen>0))
        if refcount>length(RefIdxSetMatched),
            error('Should we pad RefIdxSet with extra Refs to fill out length of final trial in rep?');
            %             tridx=shuffle(RefIdx);
            %             RefIdxSet=[RefIdxSet tridx(1:(refcount-length(RefIdxSet)))];
        end
        if CueSeg(trialidx)
            try
                PossibleCueRefs=find(strcmp(get_target_suffixes(o.ReferenceHandle,1:ReferenceMaxIndex),get(o.ReferenceHandle,'CueRefType')));
            catch err
                if contains(err.message,'There is no ''CueRefType'' property in specified class')
                    PossibleCueRefs=1:ReferenceMaxIndex;
                else
                    rethrow(err)
                end
            end
                shufi=shuffle(PossibleCueRefs);
                RefTrialIndex{trialidx}=shufi(1:refcount);
        else
            RefTrialIndex{trialidx}=RefIdxSetMatched(1:refcount);
            %RefIdxSetMatched=RefIdxSetMatched((refcount+1):end);
            %LAS 4/26/19: remove below so matching can happen with targets if wanted
        end

    elif CueSeg(trialidx)
        %If this is a cue, draw a random set of references (don't deplete RefIdxSet because we want non-cue trials have a complete set for physiology)
        try
            PossibleCueRefs=find(strcmp(get_target_suffixes(o.ReferenceHandle,1:ReferenceMaxIndex),get(o.ReferenceHandle,'CueRefType')));
        catch err
            if contains(err.message,'There is no ''CueRefType'' property in specified class') || ...
                contains(err.message,'Undefined function ''get_target_suffixes'' for input arguments of type')
                PossibleCueRefs=1:ReferenceMaxIndex;
            else
                rethrow(err)
            end
        end
        shufi=shuffle(PossibleCueRefs);
        RefTrialIndex{trialidx}=shufi(1:refcount);
    else
        % "standard" case, just grab next references from list
        if refcount>length(RefIdxSet)
            tridx=shuffle(RefIdx);
            if strcmpi(o.BalanceTargetsByReftypeAndReflen,'Yes')
                RefIdxSet=[RefIdxSet tridx];
            else
                disp('padding RefIdxSet with extra Refs to fill out length of final trial in rep');
                RefIdxSet=[RefIdxSet tridx(1:(refcount-length(RefIdxSet)))];
            end
        end
        preTarRefcount = TarOnset{trialidx}(1)-1;
        extraRefcount = refcount-preTarRefcount;
        if extraRefcount < o.Tar1AloneOffsetSegCountFreq
            extraRefcount = o.Tar1AloneOffsetSegCountFreq;
            refcount = preTarRefcount+extraRefcount;
        end
        shRefIdx = shuffle(RefIdx);
        % preTarRefcount "legitimate" references plus extras to pad but not
        % count for analysis, since they occur after the target
        RefTrialIndex{trialidx} = [RefIdxSet(1:preTarRefcount) shRefIdx(1:extraRefcount)];
        RefIdxSet = RefIdxSet((preTarRefcount+1):end);
    end
    
    %
    % figure out target index
    if refsegcount<length(ReferenceCountFreq) %There are targets in this trial
        if strcmpi(o.BalanceTargetsByReftypeAndReflen,'Yes')
            if trialidx==1
                if nRefTypes==1
                    RefTypes=repmat({'A'},1,length(RefLenDec));
                else
                    RefTypes=get_target_suffixes(o.ReferenceHandle,RefIdxSetMatched);
                end
                create_matched_set=init || strcmp(o.RepeatMisses,'No');
                [o,TargetIndex{trialidx}]=get_TI_from_TISet(o,create_matched_set,TotalTrials,RefLenDec,RefTypes,RefLenSet,round(RepIndex/dec_fact));
                if two_target 
                    nani=find(isnan(o.TargetChannel));
                    tar_inds=[TargetIndex{trialidx},o.TarIdxSet];
                    if any(isnan(o.TargetChannel))
                        single_tar_trials=find(cellfun(@(x)length(x)==1 || any(x==nani),tar_inds));
                    else
                        single_tar_trials=find(cellfun(@length,tar_inds)==1);
                    end
                    single_tar_trials_non_cue=single_tar_trials;
                    if ~TotalTrials
                        NcueTrials=sum(o.CueTrialCount); %Cue trials
                        single_tar_trials_non_cue(1:NcueTrials)=[];
                    else
                        NcueTrials=0;
                    end
                    if ~isequal(o.Tar1AloneOffsetSegCountFreq,0)
                        Nstt=length(single_tar_trials_non_cue);
                        if length(unique(o.Tar1AloneOffsetSegCountFreq))==1
                            o.shuffle_block_Tar1AloneOffset=length(o.Tar1AloneOffsetSegCountFreq);
                        else
                            good_shuffle_blocks=find(all(mod(repmat(o.Tar1AloneOffsetSegCountFreq',1,Nstt).*repmat(1:Nstt,length(o.Tar1AloneOffsetSegCountFreq),1),1)<10*eps));
                            if(isempty(good_shuffle_blocks))
                                o.shuffle_block_Tar1AloneOffset=Nstt;
                            else
                                o.shuffle_block_Tar1AloneOffset=good_shuffle_blocks(1);
                            end
                        end
                        Tar1_offsets_=[];
                        for ii=1:length(o.Tar1AloneOffsetSegCountFreq)
                            Tar1_offsets_=cat(2,Tar1_offsets_,ones(1,round(o.shuffle_block_Tar1AloneOffset.*o.Tar1AloneOffsetSegCountFreq(ii))).*(ii-1));
                        end
                        Tar1_offsets=repmat(Tar1_offsets_,1,ceil(Nstt/o.shuffle_block_Tar1AloneOffset));
                        Tar1_offsets=shuffle_blocked(Tar1_offsets,o.shuffle_block_Tar1AloneOffset);
                        Tar1_offsets(Nstt+1:end)=[]; %trim to length of single_tar_trials_non_cue
                        RefLenSet(single_tar_trials)=RefLenSet(single_tar_trials)-Tar1_offsets;
                        cue_offset=max(Tar1_offsets);
                        Tar1_offsets=[repmat(cue_offset,NcueTrials,1), Tar1_offsets];
                    end
                    if single_tar_trials(1)==1 %Fix TarOnset and RefSegStr for first trial (since they got computed before randomizing targets)
                        TarOnset{trialidx}(1)=par.SingleRefSegmentLen.*RefLenSet(1);
                        if length(TargetIndex{trialidx})==1
                            single_tar=true;
                            TarOnset{trialidx}(2)=[];
                        end
                        RefSegStr=sprintf(' RefLen=%g sec, Tar2Onset=%g sec',TarOnset{trialidx}(1),TarOnset{trialidx}(1));
                    end
                end
            else
                if nRefTypes==1
                    RefTypes=repmat({'A'},1,length(RefLenDec));
                else
                    RefTypes=get_target_suffixes(o.ReferenceHandle,RefIdxSet);
                end
                [o,TargetIndex{trialidx}]=get_TI_from_TISet(o,init,TotalTrials,RefLenDec,RefTypes);
            end
        else
            %pull Target from list of well-sampled targets. This list includes cue targets if they were requested.
            [o,TargetIndex{trialidx}]=get_TI_from_TISet(o,init,TotalTrials);
        end
        % add extra "junk" reference to align with target if OverlapRefTar.
        if strcmpi(par.OverlapRefTar,'Yes') &&...
                (isempty(par.SingleRefSegmentLen) || par.SingleRefSegmentLen==0)
            RefTrialIndex{trialidx}=...
                cat(2,RefTrialIndex{trialidx},OverlapRefSet(ceil(rand*length(OverlapRefSet))));
        end
        if nRefTypes>1
            ref_type_ind=find(strcmp(un_ref_types,get_target_suffixes(o.ReferenceHandle,RefTrialIndex{trialidx})));
        else
            ref_type_ind=1;
        end
        if par.SingleRefSegmentLen>0 && ~isequal(o.IsCatch_,0) && any(any(o.IsCatch_(TargetIndex{trialidx},ref_type_ind))) %% && ref_type_ind<=2 LAS: removed when adding single-stream trials to behavior
            CatchSeg(trialidx)=1;
            CatchStr=sprintf('Catch=%d',TargetIndex{trialidx}(o.IsCatch_(TargetIndex{trialidx},ref_type_ind)));
            if strcmpi(par.OverlapRefTar,'No')
                %error('Test this')
                 % if catch stim is included, put reference back from that slot
                RefIdxSet = [RefTrialIndex{trialidx}(CatchSeg(trialidx)) RefIdxSet];
                RefTrialIndex{trialidx}(CatchSeg(trialidx))=0;
            end
        else
            CatchStr='Catch=[]';
        end
    else
        TargetIndex{trialidx}=[];
        CatchStr='Catch=[]';
    end
    
    if rmUsedFromSets
        %remove down here so matching can happen with targets if wanted
        RefLenSet=RefLenSet(2:end);
        RefIdxSetMatched=RefIdxSetMatched((refcount+1):end);
        if two_target
            Tar2OffsetSet=Tar2OffsetSet(2:end);
        end
    end
    if nRefTypes>1
        rt=get_target_suffixes(o.ReferenceHandle,RefTrialIndex{trialidx});
        ref_type_str=['(',rt{1},')'];
    else
        ref_type_str='';
    end
    s=sprintf('%sTrial %d: Ref=%s%s Tar=%s %s%s\n',CueStr,TotalTrials+trialidx,...
       mat2str(RefTrialIndex{trialidx}),ref_type_str,mat2str(TargetIndex{trialidx}),...
       CatchStr,RefSegStr);
    fprintf(s);
    alls=[alls s];
    if strcmpi(exptparams.BehaveObjectClass,'RewardTargetLBHB') && two_target && ~single_tar && (~isempty(par.SingleRefSegmentLen) && par.SingleRefSegmentLen>0) ...
        && abs(diff(TarOnset{trialidx})) < get(exptparams.BehaveObject,'ResponseWindow') && all(~isnan(o.TargetChannel(TargetIndex{trialidx})))
%        warning('Second target overlaps with the first! Check distributions and segment lengths to make sure they''re set up right. You might want this but BehaviorControl and PerformanceAnalysis aren''t set up to do this yet :-)')
    end
    init=false;
    
    if (balance_ref_types_by_Nref && nRefTypes>1 ) || strcmpi(o.BalanceTargetsByReftypeAndReflen,'Yes')
        if par.SingleRefSegmentLen>0
            %One ref per trial, use RefIdxSetMatched to tell when we're done (could also use RefLenSet since they are drawn from together?)
            add_trial=~isempty(RefIdxSetMatched);
        else
            %Multiple refs per trial, RefIdxSetMatched isn't doing anything, use RefLenSet
            add_trial=~isempty(RefLenSet);
        end
    else
        add_trial=~isempty(RefIdxSet);
    end
end
if 0
    %Popup window with text of trials to be played.
    fh=figure('Name','TrialParms','Color','w','Position',[1401         100         514         894]);
    [~,basename]=fileparts(globalparams.mfilename);
    set(fh,'Name',['TrialParams: ',basename]);
    feedback_text=uicontrol('style','text','string',sprintf(alls),'FontSize',9,'HorizontalAlignment','left','BackGroundColor','w','Units','normalized','Position',[.01 .01 .98 .98],'Parent',fh);
end
TotalTrials=trialidx;
if TotalTrials>length(TarOnset),
    warning('TotalTrials>length(TarOnset)');
end

% Inserting new stuff for Optical channel.  Repeat all trials,  shuffle to interleave
%
if par.LightPulseDuration==0
    % no opto
    LightTrial=zeros(1,TotalTrials);
else

    switch par.LightConditions,
        case 'Interleaved',
            LightTrial=[zeros(1,TotalTrials) ones(1,TotalTrials)];
            RefTrialIndex=cat(2,RefTrialIndex,RefTrialIndex);
            TotalTrials=TotalTrials*2;
        case 'Interleave8020',
            LightTrial=ones(1,TotalTrials);
            LightTrial(1:round(TotalTrials*0.2))=0;
        case 'All_on',
            LightTrial=[ones(1,TotalTrials)];
        case 'All_off',
            LightTrial=[zeros(1,TotalTrials)];
    end
    if contains(par.LightConditions,'Interleave')
        if any(CueSeg)
            error('LAS: MultiRefTar/RandomizeSequence.m isn''t set up to correctly play cue trials without laser, then play a full set of "real" trials with and without laser. Should be an easy add if needeed.')
        end
    end
    %[~,si]=sort(rand(1,TotalTrials)); %LAS: added logic for cue trials
    nCueTrials=sum(CueSeg);
    si=[1:nCueTrials shuffle(nCueTrials+1:TotalTrials)];
    
    LightTrial=LightTrial(si);
    RefTrialIndex=RefTrialIndex(si);
    TarOnset=cat(2,TarOnset,TarOnset);
    TarOnset=TarOnset(si);
    
    if ~isempty(TargetIndex),
        TargetIndex=cat(2,TargetIndex,TargetIndex);
        TargetIndex=TargetIndex(si);
    end
    if ~isempty(CatchIndex),
        CatchIndex=cat(2,CatchIndex,CatchIndex);
        CatchIndex=CatchIndex(si);
        CatchSeg=cat(1,CatchSeg,CatchSeg);
        CatchSeg=CatchSeg(si);
    end
    if length(CueSeg)<TotalTrials
        CueSeg=cat(1,CueSeg,zeros(TotalTrials-length(CueSeg),1));
    end
end

% save results back to the TrialObject
o = set(o,'LightTrial',LightTrial);
o = set(o,'ReferenceIndices',RefTrialIndex);
o = set(o,'TargetIndices',TargetIndex);
o = set(o,'CatchIndices',CatchIndex);
o = set(o,'CatchIdxSet',CatchIdxSet);
o = set(o,'CatchSeg',CatchSeg);
o = set(o,'CueSeg',CueSeg);
o = set(o,'NumberOfTrials',TotalTrials);
o = set(o,'TarOnset',TarOnset);
exptparams.TrialObject = o;

end
%%
function IdxSet=shuffle_blocked(Idx,shuffle_block,varargin)
% IdxSet=shuffle_blocked(Idx,shuffle_block,end_shuf_cond)
if nargin==2
    end_shuf_cond=0;
else
    end_shuf_cond=varargin{1};
end
%shuffles first dimension of Idx
do_transform = isrow(Idx);
if do_transform
    Idx=Idx';
end
if shuffle_block > size(Idx,1)
    warning(['Shuffle block was set to %d, which is more than the length ',...
        'of the target index set (%d). Setting to %d.'],shuffle_block,size(Idx,1),size(Idx,1))
    shuffle_block = size(Idx,1);
end
if(shuffle_block~=size(Idx,1))
    separate_shuffles=size(Idx,1)/shuffle_block;
    if(mod(separate_shuffles,1)~=0)
        if end_shuf_cond==0
            warning(['The length of the index (%d) is not evenly divisible ',...
                'by the computed shuffle block (%d). Shuffling the last block ',...
                'and remainder together.'],size(Idx,1),shuffle_block)
        end
        if end_shuf_cond<2
            separate_shuffles=floor(separate_shuffles)-1;
        else
            separate_shuffles=floor(separate_shuffles);
        end
        do_end_shuffle=true;
    else
        do_end_shuffle=false;
    end
    if iscell(Idx)
        IdxSet=cell(size(Idx));
    else
        IdxSet=nan(size(Idx));
    end
    for i=1:separate_shuffles
        shufInd=randperm(shuffle_block);
        thisIdx=Idx((i-1)*shuffle_block+(1:shuffle_block),:);
        IdxSet((i-1)*shuffle_block+(1:shuffle_block),:)=thisIdx(shufInd,:);
        %IdxSet((i-1)*shuffle_block+(1:shuffle_block))=shuffle(Idx((i-1)*shuffle_block+(1:shuffle_block)));
    end
    if(do_end_shuffle)
        i=i+1;
        thisIdx=Idx((i-1)*shuffle_block+1:end,:);
        shufInd=randperm(size(thisIdx,1));
        IdxSet((i-1)*shuffle_block+1:end,:)=thisIdx(shufInd,:);
        %IdxSet((i-1)*shuffle_block+1:end)=shuffle(Idx((i-1)*shuffle_block+1:end));
    end
else
    shufInd=randperm(size(Idx,1));
    IdxSet=Idx(shufInd,:);
end
if do_transform
    IdxSet=IdxSet';
end
end
%%
function [o,ti]=get_TI_from_TISet(o,init,TotalTrials,RefLenDec,RefTypes,RefLenSet,RepOffset)
balance_targets_by_reftype_and_reflen_max_allowed_adjacent=2;
if(nargin<2), init=false; end
if(nargin<3), TotalTrials=false; end
if(nargin<4),
    balance_targets_by_reftype_and_reflen=false;
else
    balance_targets_by_reftype_and_reflen=true;
end
two_target=iscell(o.TarIdx) && any(cellfun(@length,o.TarIdx)>1);
% create a set of possible targets if none are remaining
if isempty(o.TarIdxSet) && ~isempty(o.TargetIdxFreq)
    if init
        % ie first trials, force frequency target for first 5 trials
%         if two_target
%             [unti,~,inds]=uniquecell(o.TarIdx);
%             NperTar=arrayfun(@(x)sum(inds==x),1:length(unti));
%             [~,tmaxidx]=max(NperTar);
%             cue_tar=unti(1);
%         else
%             if strcmpi(exptparams.BehaveObjectClass,'RewardTargetLBHB')
%                 PumpDuration = get(exptparams.BehaveObject,'PumpDuration');
%                 if sum(PumpDuration)>0
%                     max_reward_idx = find(PumpDuration==max(PumpDuration),1, 'first');
%                 else
%                     max_reward_idx=1;
%                 end
%                 TargetIndex{trialidx} = max_reward_idx;
%             end
%             tmaxidx=find(o.TargetIdxFreq==max(o.TargetIdxFreq),1);
%             cue_tar=find(o.TarIdx==tmaxidx,1);
%         end
        if balance_targets_by_reftype_and_reflen && length(o.TargetIdxFreq)>1

            %Find the most common target index, set all trials to that by
            %default. Then we'll go through the rest and replace them.
            if two_target
                [unti,~,inds]=uniquecell(o.TarIdx);
                NperTar=arrayfun(@(x)sum(inds==x),1:length(unti));
                [~,tmaxidx]=max(NperTar);
                default_ti=unti(tmaxidx);
                assign_tis=unti;
                assign_tis(tmaxidx)=[]; %Remove the default from the list to be assigned
            else
                [~,default_ti]=max(o.TargetIdxFreq);
                %[~,assign_ti]=min(o.TargetIdxFreq);
                assign_tis=setdiff(1:length(o.TargetIdxFreq),default_ti);
                [~,si]=sort(o.TargetIdxFreq(assign_tis),'descend');
                assign_tis=assign_tis(si);
            end
            
            
            unRefTypes=unique(RefTypes);
            nRefTypes=length(unRefTypes);
            nEachRefType=cellfun(@(x)sum(strcmp(x,RefTypes)),unRefTypes);
            if any(nEachRefType~=nEachRefType(1))
                error('Reference type probabiliy must be equal or 1.')
            end
            for ti_ind=1:length(assign_tis)
                % Loop through each target index to be assigned.
                % determine N_ti, the number of targets that match this target index
                if two_target
                    N_ti=sum(cellfun(@(x)isequal(x,assign_tis{ti_ind}),o.TarIdx));
                else
                    N_ti=sum(o.TarIdx==assign_tis(ti_ind));
                end
                N_ti_bt=N_ti/nRefTypes; % number of target indicies by type
                if mod(weekday(now),2)==1
                    N_ti_bt=floor(N_ti_bt);
                else
                    N_ti_bt=ceil(N_ti_bt);
                end
                space=length(RefLenDec)/N_ti_bt;
                indsets={};
                for i=1:2*space
                    inds_=round(i:space:length(RefLenDec));
                    if length(inds_)==N_ti_bt
                        indsets{i}=inds_;
                    end
                end
                if length(indsets)<3
                    indsets={};
                    for i=1:2*space
                        inds_=round(linspace(i,length(RefLenDec),N_ti_bt+1))+i-1;
                        inds_(end)=[];
                        if inds_(end)<=length(RefLenDec)
                            indsets{i}=inds_;
                        end
                    end
                end
                try_count=0;
                sti_offset=RepOffset-2;
                while try_count<100
                    do_break=false;
                    sti=rem(round(length(indsets)/2)+sti_offset,length(indsets))+1;
                    try_count=try_count+1;
                    if ti_ind==1
                        TarIdxSet=repmat(default_ti,size(o.TarIdx));
                    else
                        TarIdxSet=TarIdxSet_last;
                    end
                    %Shuffle so that within the rare targets the order of reference
                    %lengths is randomized
                    inds=indsets{sti};
                    needed_RLs=shuffle(RefLenDec(inds));
                    rti=randperm(nRefTypes);
                    for i=1:nRefTypes
                        for j=1:length(needed_RLs)
                            %find indicies that match reflength and reftype
                            if two_target
                                mis=find(RefLenSet==needed_RLs(j) & strcmp(RefTypes,unRefTypes{i}) & cellfun(@(x)isequal(x,default_ti{1}),TarIdxSet));
                            else
                                mis=find(RefLenSet==needed_RLs(j) & strcmp(RefTypes,unRefTypes{i}) & TarIdxSet==default_ti);
                            end
                            if isempty(mis)
                                sti_offset=sti_offset+1;
                                if two_target
                                    warning('Can''t find match for TarIdx [%s] (%d/%d; RefLenSet==%d, RefTypes==%s), increasing sti_offset to %d',num2str(assign_tis{ti_ind}),ti_ind,length(assign_tis),needed_RLs(j),unRefTypes{i},sti_offset)
                                else
                                    warning('Can''t find match for TarIdx %d (%d/%d; RefLenSet==%d, RefTypes==%s), increasing sti_offset to %d',assign_tis(ti_ind),ti_ind,length(assign_tis),needed_RLs(j),unRefTypes{i},sti_offset)
                                end
                                if sti_offset>2*length(indsets)
                                    error('sti offset too big, no matches?!')
                                end
                                do_break=true;
                                break
                            end
                            %Of these indicies, use the one whose proportinal
                            %position in the sequence is closest to the sequence of
                            %needed ref lengths. This should distribute the rare
                            %targets across the sequence.
                            if rti(i)==1
                                [~,ind]=min(abs(j/length(needed_RLs) - (1:length(mis))/length(mis)));
                            else
                                [~,ind]=min(abs((length(needed_RLs)-j+1)/length(needed_RLs) - (1:length(mis))/length(mis)));
                            end
                            TarIdxSet(mis(ind))=assign_tis(ti_ind); % +i-1 for debugging
                        end
                        if do_break
                            break
                        end
                    end
                    if do_break
                        continue
                    end
                    if two_target
                        rares=[0 cellfun(@(x)isequal(x,assign_tis{ti_ind}),TarIdxSet) 0];
                    else
                        rares=[0 TarIdxSet==assign_tis(ti_ind) 0];
                    end
                    sizes = find(diff(rares) == -1) - find(diff(rares) == 1);
                    if all(sizes <= balance_targets_by_reftype_and_reflen_max_allowed_adjacent)
                        break
                    end
                end
                TarIdxSet_last=TarIdxSet;
            end
            if 0
                %%
                centers=unique(RefLenSet);
                edges=centers-diff(centers(1:2))/2;
                edges(end+1)=edges(end)+diff(edges(1:2));
                nc=nan(length(o.TargetIdxFreq),length(edges)-1,2);
                for i=1:length(o.TargetIdxFreq)
                    for j=1:length(unRefTypes)
                        inds=TarIdxSet==i & strcmp(RefTypes,unRefTypes{j});
                        n(i,:,j)=histcounts(RefLenSet(inds),edges);
                    end
                end
                na=permute(n,[2 1 3]); na=na(:,:)';
                figure;bar(centers',na')
                set(gca,'XTick',centers)
                figure;bar(centers',sum(na)')
            end
            if strcmp(o.ReferenceClass,'SpVowels')
                IC=any(get(o,'IsCatch_'),2);
                TCs=get(o,'TargetChannel');
 		if length(TCs)==1, TCs=repmat(TCs,length(IC),1); end

                Cch=TCs(IC);
                if Cch==1
                    CatchStreamName='SgA'; TarStreamName='SgB'; Tch=2;
                else
                    CatchStreamName='SgB'; TarStreamName='SgA'; Tch=1;
                end
                SingleCatches=strcmp(RefTypes,CatchStreamName);
                %[TarIdxSet{SingleCatches}]=deal([1 find(TCs==Cch,1,'last')]);
                if two_target
                    [TarIdxSet{SingleCatches}]=deal([1 find(isnan(TCs),1)]);
                else
                    [TarIdxSet(SingleCatches)]=deal([1 find(isnan(TCs),1)]);
                end
                SingleTars=strcmp(RefTypes,TarStreamName);
                if any(SingleTars)
                    if two_target
                        [TarIdxSet{SingleTars}]=deal([1 find(TCs==Tch,1,'last')]);
                    else
                        [TarIdxSet(SingleTars)]=deal([1 find(TCs==Tch,1,'last')]);
                    end
                end
            end
        else
            TarIdxSet=shuffle_blocked(o.TarIdx,o.shuffle_block_TarIdx);
        end
        
        if sum(o.CueTrialCount)>0 && ~TotalTrials
            if two_target
                CueTarIdx={o.CueTarIdx};
            else
                CueTarIdx=o.CueTarIdx;
            end
            o.TarIdxSet=[repmat(CueTarIdx,1,sum(o.CueTrialCount)) TarIdxSet];
        else
            o.TarIdxSet=TarIdxSet;
        end
    else
        o.TarIdxSet=shuffle_blocked(o.TarIdx,o.shuffle_block_TarIdx);
    end
end

if two_target
    ti=o.TarIdxSet{1};
else
    ti=o.TarIdxSet(1);
end
o.TarIdxSet=o.TarIdxSet(2:end);

end

function exptparams = insertCue(o,exptparams,trialidx)
if exptparams.TrialParams(1).CueSeg
    par = get(exptparams.TrialObject);
    newslot=trialidx+1;
    cueidx=1;
    
    
    NewTargetIndices={par.TargetIndices{1:(newslot-1)} ...
        exptparams.TrialParams(cueidx).TargetIndices ...
        par.TargetIndices{newslot:end}};
    NewReferenceIndices={par.ReferenceIndices{1:(newslot-1)} ...
        exptparams.TrialParams(cueidx).ReferenceIndices ...
        par.ReferenceIndices{newslot:end}};
    NewCatchIndices={par.CatchIndices{1:(newslot-1)} ...
        exptparams.TrialParams(cueidx).CatchIndices ...
        par.CatchIndices{newslot:end}};
    NewCatchSeg=[par.CatchSeg(1:(newslot-1));  ...
        exptparams.TrialParams(cueidx).CatchSeg;  ...
        par.CatchSeg(newslot:end)];
    NewCueSeg=[par.CueSeg(1:(newslot-1));  ...
        exptparams.TrialParams(cueidx).CueSeg;  ...
        par.CueSeg(newslot:end)];
    
    do_ObjUpdate=false;
    o=set(o,'ReferenceIndices',NewReferenceIndices,do_ObjUpdate);
    o=set(o,'TargetIndices',NewTargetIndices,do_ObjUpdate);
    o=set(o,'CatchIndices',NewCatchIndices,do_ObjUpdate);
    o=set(o,'CatchSeg',NewCatchSeg,do_ObjUpdate);
    o=set(o,'CueSeg',NewCueSeg,do_ObjUpdate);
    % svd fixed bug 2014-11-24 where RefDuration was assigned to the
    % next trial rather than the new slot for misses.
    o=set(o,'TarOnset',...
        [par.TarOnset(1:(newslot-1)) ...
        exptparams.TrialParams(cueidx).TarOnset ...
        par.TarOnset(newslot:end)],do_ObjUpdate);
    o=set(o,'LightTrial',...
        [par.LightTrial(1:(newslot-1)) ...
        exptparams.TrialParams(cueidx).LightTrial ...
        par.LightTrial(newslot:end)],do_ObjUpdate);
    o=set(o,'NumberOfTrials',par.NumberOfTrials+1,do_ObjUpdate);
    fprintf('Inserting Cue trial\n');
    exptparams.TrialObject = o;
else
    fprintf('Not playing cue on next trial because no cue trials have ever been played (so RandomizeSequence doesn''t know how to set them up.\n')
end
end
    