function exptparams = StoreTrialParams (o, exptparams, RepIndex)
% exptparams = StoreTrialParams (o, exptparams, globalparams, RepIndex)
% Log Trial Paramters

trialidx=exptparams.InRepTrials;
trialidx_whole_run=exptparams.TotalTrials;
tiwr=trialidx_whole_run;
par = get(o);

exptparams.TrialParams(tiwr).rep=RepIndex;
exptparams.TrialParams(tiwr).trial_this_rep=trialidx;
if ~isempty(par.TargetIndices)
    exptparams.TrialParams(tiwr).TargetIndices=par.TargetIndices{trialidx};
end
exptparams.TrialParams(tiwr).ReferenceIndices=par.ReferenceIndices{trialidx};
exptparams.TrialParams(tiwr).CueSeg=par.CueSeg(trialidx);

if ismethod(o.ReferenceHandle,'StoreTrialParams')
     exptparams = StoreTrialParams (o.ReferenceHandle, exptparams, RepIndex);
end
