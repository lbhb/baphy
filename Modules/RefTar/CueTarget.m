function CueTarget(globalparams, exptparams, HW)

reps=10;
RewardDur=get(exptparams.BehaveObject,'PumpDuration');

BehaveObject = exptparams.BehaveObject;
BehaviorParms=get(BehaveObject);

exptparams.TrialBlock=1;
exptparams.Repetition=1;
exptparams.TrialObject=set(exptparams.TrialObject,'ReferenceCountFreq',[1 0]);

%adjust levels so that reference is inaudible
overalldB=get(exptparams.TrialObject,'OverallDB');
RelativeTarRefdB=get(exptparams.TrialObject,'RelativeTarRefdB');
RelativeTarRefdB=mean(RelativeTarRefdB);

refdB=-20;
RelativeTarRefdB=RelativeTarRefdB+overalldB-refdB;
overallDB=refdB;

exptparams.TrialObject=set(exptparams.TrialObject,'RelativeTarRefdB',RelativeTarRefdB);
exptparams.TrialObject=set(exptparams.TrialObject,'overallDB',overallDB);

exptparams.TrialObject = ObjUpdate(exptparams.TrialObject);
iRep=1;
exptparams = RandomizeSequence(exptparams.TrialObject, exptparams, globalparams, iRep, 1);
for iTrial=1:reps
    [TrialSound, StimEvents, exptparams.TrialObject] = waveform(exptparams.TrialObject, iTrial);
    HW = IOSetLoudness(HW, 80-get(exptparams.TrialObject, 'OveralldB'));
    exptparams.LogDuration = LogDuration(BehaveObject, HW, ...
        StimEvents, globalparams, exptparams, iTrial);
    ind=arrayfun(@(x)~isempty(strfind(x.Note,'Target')) && ~isempty(strfind(x.Note,'Stim ')),StimEvents);
    RewardOnset = StimEvents(ind).StartTime + get(exptparams.BehaveObject,'EarlyWindow');
    TrainingRewardGiven=0;
    HW = IOLoadSound(HW, TrialSound);
    [StartEvent,HW] = IOStartAcquisition(HW);
    CurrentTime = IOGetTimeStamp(HW);
    while CurrentTime < exptparams.LogDuration  % while trial is not over
        if CurrentTime>RewardOnset && ~TrainingRewardGiven
            disp('Giving reward');
            ev = IOControlPump(HW,'start',RewardDur);
            TrainingRewardGiven=1;
        end
        CurrentTime = IOGetTimeStamp(HW);
    end
    [TrialStopEvent,HW] = IOStopAcquisition(HW);    
end