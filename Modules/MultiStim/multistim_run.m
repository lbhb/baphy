% function [exptevents,exptparams]=multistim_run(globalparams,exptparams,HW);
%
% run the multi-stimulus module in baphy. uses standartad parameter syntax:
%  globalparams - defined by BaphyMainGUI
%  exptparams - defined by dms_init
%  HW - created by InitializeHW
%
% created SVD 2010-04-13
%
function [exptevents,exptparams]=multistim_run(globalparams,exptparams,HW);

global StopExperiment; StopExperiment = 0; % Corresponds to User button
global exptparams_Copy; exptparams_Copy = exptparams; % some code needs exptparams
global BAPHY_LAB LoudnessAdjusted
global SAVEPUPIL

disp([mfilename ': Initializing...']);

if globalparams.HWSetup==0
    disp(['NOTE---RUNNING IN TEST MODE']);
end

stimcount=exptparams.stimcount;

disp('Figuring out multiple parmfiles');
mfiles={globalparams.mfilename};
evpfiles={globalparams.evpfilename};
rawids=globalparams.rawid;

% save first mfile name for converting raw data to evps for stimuli 2..N
globalparams.daqmfilename=globalparams.mfilename;

texptparams=exptparams;
for ii=2:stimcount,
    texptparams.runclass=exptparams.runclass{ii};
    [mfiles{ii},evpfiles{ii},rawids(ii,1)]=...
        dbmfilename(globalparams,texptparams);
end

disp('Setting up sound objects...');
RO=cell(stimcount,1);
maxfs=0;
for ii=1:exptparams.stimcount,
    sii=['Stim',num2str(ii)];
    if isfield(exptparams.(sii),'SamplingRate'),
        maxfs=max(maxfs,exptparams.(sii).SamplingRate);
    end
end
exptparams.fs=maxfs;

maxidxset=zeros(stimcount,1);
for ii=1:exptparams.stimcount,    
    sii=['Stim',num2str(ii)];
    fprintf('%s: Creating %s object...\n',sii,exptparams.(sii).descriptor);
    RO{ii}=eval(exptparams.(sii).descriptor);
    ff=fieldnames(exptparams.(sii));
    for jj=1:length(ff),
        RO{ii}=set(RO{ii},ff{jj},getfield(exptparams.(sii),ff{jj}));
    end
    RO{ii}=set(RO{ii},'SamplingRate',exptparams.fs);
    exptparams.(sii).SamplingRate=exptparams.fs;
    
    %exptparams.TrialObject.(sii)=get(RO{ii});
    maxidxset(ii)=get(RO{ii},'MaxIndex');
end

HW = IOSetTrigger(HW, 'HwDigital');    % AI and AO start synchronously with IOStartAcquisition


% play dummy sound to deal with no-first-play bug. perhaps not necessary?
%[ev,HW]=IOStartSound(HW, zeros(10,1));
%pause(0.05);

% start up JobMonitor
exptevents=cell(stimcount,1);
jh=JobMonitor(globalparams,exptevents{1});

% loop trials indefinitely (until break at end of trial block)
originalmaxreps=exptparams.repcount;
repcount=0;
TrialIndex=0;
trialcounters=zeros(stimcount,1);
playedset=ones(sum(maxidxset),1);
exptparams.triallog=[];
while 1,
    %%% basic structure of a trial:
    % 0. figure out if new repetition and, if so, reinitialize
    % 1. figure out which sound to play
    % 2. choose one of those sounds that hasn't been played yet on this rep
    % 
    
    % figure out if new rep, and reinitialize if necessary
    if sum(playedset)==length(playedset),
        repcount=repcount+1;
        fprintf('\n************************\n Starting repetition %d\n************************\n\n',repcount);
        
        % array length=total number of exemplars for all stimuli. so all we
        % need to do to pick the next stimulus is choose one that hasn't
        % been played yet.
        stimidx=[];
        stimsubidx=[];
        for ii=1:stimcount,
            stimidx=cat(1,stimidx,ii.*ones(maxidxset(ii),1));
            stimsubidx=cat(1,stimsubidx,(1:maxidxset(ii))');
        end
        playedset=zeros(sum(maxidxset),1);
    end
    
    TrialIndex=TrialIndex+1;
    % figure out next stimulus
    ff=find(~playedset);
    nextid=ff(ceil(rand*length(ff)));
    nextstim=stimidx(nextid);
    nextsubstim=stimsubidx(nextid);
    trialcounters(nextstim)=trialcounters(nextstim)+1;
    exptparams.triallog(:,TrialIndex)=[nextstim;nextsubstim];
    fprintf('Next: Stim %d idx %d\n',nextstim,nextsubstim);
    
    [TrialSound,stimevents]=waveform(RO{nextstim},nextsubstim);
    
    % show current waveform in job monitor
    JobMonitor('JobMonitor_PlotWaveform',jh,[],[],TrialSound,exptparams.fs);
    
    %
    % TRIAL STARTS HERE
    %
    % SVD 2016-06-08: Pasting in from RefTarScript
    HW = IOSetSamplingRate(HW, maxfs);
    HW = IOSetLoudness(HW, 80-exptparams.soundlevel);

    % CONSTRUCT FILENAME FOR MANTA RECORDINGS (ONLY USED IN MANTA SETUP)
    HW.Filename = M_setRawFileName(globalparams.mfilename,TrialIndex);
    
    % GET & SET LOGGING DURATION FOR PRESENT TRIAL
    exptparams.LogDuration = 2+length(TrialSound)./exptparams.fs;
    LogSteps = exptparams.LogDuration*HW.params.fsAI;
    IOSetAnalogInDuration(HW,exptparams.LogDuration);
    
    % MATCH SOUND AND ACQUISITION DURATION
    TrialSound(floor(exptparams.LogDuration.*HW.params.fsAO),end) = 0;
    
    % load sound, but don't start any acquisition
    HW=IOLoadSound(HW,TrialSound);

    if SAVEPUPIL
        MSG = ['GETVAR',HW.Pupil.COMterm,...
            '[now,MG.frames_written]',HW.Pupil.MSGterm];
        RESP=IOSendMessageTCPIP(HW.Pupil,MSG);
        PupilEvent=AddEvent([],['PUPIL,',RESP],TrialIndex,0,0);
        RESP=str2num(RESP);
        fprintf('Remote time: %s, frames: %d\n',datestr(RESP(1)),RESP(2));
    else
        PupilEvent=[];
    end
    
    % start AI/spike acquisition here
    exptevents{nextstim}=AddEvent(exptevents{nextstim},IOStartAcquisition(HW),trialcounters(nextstim));
    for jj=1:length(stimevents),
        exptevents{nextstim}=AddEvent(exptevents{nextstim},stimevents(jj),trialcounters(nextstim));
        exptevents{nextstim}(end).Note=[exptevents{nextstim}(end).Note ' , Reference'];
    end

    while IOIsPlaying(HW),
        %if exptparams.distobject | IOGetTimeStamp(HW) > exptevents(end).StopTime,
        %if ~QuickError | IOGetTimeStamp(HW)>RO_duration(1)-get(RO,'PostStimSilence'),
        %    IOStopSound(HW);
        %    disp('stopped now');
        %end
        pause(0.01);
    end
    
    exptevents{nextstim}=AddEvent(exptevents{nextstim},IOStopAcquisition(HW),trialcounters(nextstim));

    % IF THERE WAS A PROBLEM ON THE ACQUISITION SIDE, RERUN LAST TRIAL
    %if strcmp(HW.params.DAQSystem,'MANTA') &&...
    %        isfield(HW.MANTA,'RepeatLast') && HW.MANTA.RepeatLast
    %    TrialIndex = TrialIndex - 1; iTrial = iTrial - 1; HW.MANTA.RepeatLast = 0; continue;
    %end
    
    stopnow=get(jh,'UserData');
    JobMonitor('JobMonitor_Refresh',jh,[],[],globalparams,exptevents{nextstim});
    
    playedset(nextid)=1;
    
    % end of repetition, save to more than one mfile??
    if sum(playedset)==length(playedset),
        disp('saving intermediate parameters in case of crash...');
        save_parameters(globalparams,exptparams,exptevents,rawids,mfiles,evpfiles);
    end

    if stopnow || (sum(playedset)==length(playedset) && repcount>=exptparams.repcount),
        disp('---------------------------------------------------------');
        fprintf('Finished %d reps\n',repcount-1+(sum(playedset)==length(playedset)));
        yn=questdlg('Continue running?','Multi-Stim','Yes','No','Yes');
        if strcmp(yn,'No'),
            break
        elseif sum(playedset)re==length(playedset) && repcount>=exptparams.repcount,
            % go for a few more reps if we're hit the pre-determined limit.
            exptparams.repcount=exptparams.repcount+originalmaxreps;
        end
    end

end

if exist('jh','var') & ~isempty(jh),
    close(jh);
end

globalparams.ExperimentComplete=1;
save_parameters(globalparams,exptparams,exptevents,rawids,mfiles,evpfiles);

% that's it!
disp([mfilename ': Complete']);



%
%function save_parameters(globalparams,exptparams,exptevents,rawids,mfiles,evpfiles);
%
function save_parameters(globalparams,exptparams,exptevents,rawids,mfiles,evpfiles);

% save appropriate mfiles!
for ii=find(rawids(:)'>0),
    fprintf('Generating m-file: %s...\n',mfiles{ii});
    sii=['Stim',num2str(ii)];
    exptparams.TrialObject=[];
    exptparams.TrialObject.ReferenceHandle=exptparams.(sii);
    exptparams.thisstimidx=ii;
    globalparams.mfilename=mfiles{ii};
    globalparams.evpfilename=evpfiles{ii};
    globalparams.rawid=rawids(ii);
    WriteMFile(globalparams,exptparams,exptevents{ii},1);
    
    disp('saving performance/parameter data to cellDB...');
    
    % code morphed from Nima's PrepareDatabaseData
    Parameters  = [];
    Parameters.Module='Multi-Stimulus';
    Parameters.Stimulus_Total=exptparams.stimcount;
    Parameters.This_Stimulus_Idx=ii;
    Parameters.Trial_OverallDB=exptparams.soundlevel;
    RefHandle = exptparams.TrialObject.ReferenceHandle;
    if ~isempty(RefHandle)
        Parameters.Reference = '______________';
        Parameters.ReferenceClass = RefHandle.descriptor;
        field_names = RefHandle.UserDefinableFields;
        for cnt1 = 1:3:length(field_names)
            Parameters.(['Ref_' field_names{cnt1}]) = getfield(RefHandle, field_names{cnt1});
        end
    end
    field_names = fieldnames(Parameters);
    for cnt1 = 1:length(field_names)
        if ischar(Parameters.(field_names{cnt1}))
            Parameters.(field_names{cnt1}) = strrep(Parameters.(field_names{cnt1}),'<','^<');
            Parameters.(field_names{cnt1}) = strrep(Parameters.(field_names{cnt1}),'>','^>');
        end
    end
    % too long a string for varchar(255). Also not really interesting once
    % the data it pre-processed?
    %Parameters.triallog=exptparams.triallog;
    dbWriteData(globalparams.rawid,Parameters,0,0);
end



%%
%%======== LOCAL FUNCTIONS =======================================
%%
function ContinueExp = ContinueOrNot
global StopExperiment;
FP = get(0,'DefaultFigurePosition');
MP = get(0,'MonitorPosition');
SS = get(0,'ScreenSize');
set(0,'DefaultFigurePosition',[10,MP(4)/2-SS(2),FP(3:4)]);
UserInput = questdlg('Continue the experiment?');
set(0,'DefaultFigurePosition',FP);
ContinueExp = strcmpi(UserInput, 'Yes') | strcmpi(UserInput,'Cancel');
if ContinueExp==1, StopExperiment = 0; end


function LF_showMic(MicData,exptparams,HW,TrialSound)
if ~isempty(MicData)
  if isfield(exptparams,'wavefig') figure(exptparams.wavefig);
  else exptparams.wavefig = figure;
  end
  subplot(2,2,1); plot(TrialSound); axis tight; title('computer waveform')
  subplot(2,2,2); spectrogram(TrialSound,256,[],[],HW.params.fsAO,'yaxis'); title('computer spectrogram');
  subplot(2,2,3); plot(MicData); axis tight; title('microphone waveform');
  subplot(2,2,4); spectrogram(MicData,256,[],[],HW.params.fsAux,'yaxis'); title('microphone spectrogram');
end

function LF_writetoDB(globalparams,exptparams)
global DB_USER SAVEPUPIL
if globalparams.rawid>0 && dbopen,
  [Parameters, Performance] = PrepareDatabaseData ( globalparams, exptparams);
  dbWriteData(globalparams.rawid, Parameters, 0, 0);  % this is parameter and dont keep previous data
  dbWriteData(globalparams.rawid, Performance, 1, 0); % this is performance and dont keep previous data
  if isfield(Performance,'HitRate') && isfield(Performance,'Trials')
    sql=['UPDATE gDataRaw SET corrtrials=',num2str(round(Performance.HitRate*Performance.Trials)),',',...
      ' trials=',num2str(Performance.Trials),' WHERE id=',num2str(globalparams.rawid)];
    mysql(sql);
  elseif isfield(Performance,'Hit') && isfield(Performance,'FalseAlarm')
    sql=['UPDATE gDataRaw SET corrtrials=',num2str(Performance.Hit(1)),',',...
      ' trials=',num2str(Performance.FalseAlarm(2)),' WHERE id=',num2str(globalparams.rawid)];
    mysql(sql);
  end
  if SAVEPUPIL
    sql=['UPDATE gDataRaw SET eyewin=1,eyecalfile="',globalparams.PupilFilename,'"',...
        ' WHERE id=',num2str(globalparams.rawid)];
    mysql(sql);
  end
  % also, if 'water' is a field, make it accumulative:
  if isfield(exptparams, 'Water')
    %%%%%%%%%%%%%% new water:
    sql=['SELECT gAnimal.id as animal_id,gHealth.id,gHealth.water'...
      ' FROM gAnimal LEFT JOIN gHealth ON gHealth.animal_id=gAnimal.id'...
      ' WHERE gAnimal.animal like "',globalparams.Ferret,'"',...
      ' AND date="',datestr(now,29),'" LIMIT 1'];
    hdata=mysql(sql);
    if ~isempty(hdata),
      % gHealth entry already exists, update
      if isempty(hdata.water) hdata.water = 0; end
      swater=sprintf('%.2f',hdata.water+exptparams.Water);
      sql=['UPDATE gHealth set schedule=1,trained=1,water=',...
        swater,' WHERE id=',num2str(hdata.id)];
    else
      % create new gHealth entry
      sql=['SELECT * FROM gAnimal WHERE animal like "',globalparams.Ferret,'"'];
      adata=mysql(sql);
      sql=['INSERT INTO gHealth (animal_id,animal,date,water,trained,schedule,addedby,info) VALUES'...
        '(',num2str(adata.id),',"',globalparams.Ferret,'",',...
        '"',datestr(now,29),'",'...
        num2str(exptparams.Water),',1,1,"',DB_USER,'","dms_run.m")'];
    end
    mysql(sql);
  end
end
