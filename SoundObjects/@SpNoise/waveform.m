function [w,event,e]=waveform (o,index,IsRef,TarObject,ThisTarParams)
% function w=waveform(o, index,IsRef);
%
% generate waveform for SpNoise object
%

global SPNOISE_EMTX

if nargin<3
   IsRef=1;
end

if nargin<4
    TarObject=[];
    ThisTarParams=[];
end

LowFreq=get(o,'LowFreq');
HighFreq=get(o,'HighFreq');
RelAttenuatedB=get(o,'RelAttenuatedB');
SplitChannels=get(o,'SplitChannels');
bSplitChannels=strcmpi(SplitChannels,'Yes');
SamplingRate = get(o,'SamplingRate');
Duration = get(o,'Duration'); % duration is second
PreStimSilence = get(o,'PreStimSilence');
PostStimSilence = get(o,'PostStimSilence');
IterateStepMS=get(o,'IterateStepMS');
IterationCount=get(o,'IterationCount');
TonesPerBurst=get(o,'TonesPerBurst');
Names = get(o,'Names');
SamplingRateEnv = get(o,'SamplingRateEnv');
UseBPNoise=get(o,'UseBPNoise');
BaseSound=strtrim(get(o,'BaseSound'));
Subsets = get(o,'Subsets');
BaselineFrac=get(o,'BaselineFrac');
idxset=get(o,'idxset');
ShuffledOnsetTimes=get(o,'ShuffledOnsetTimes');
SingleBandFrac=get(o,'SingleBandFrac');

EnvVarName=[BaseSound,num2str(Subsets)];
%emtx = get(o,'emtx');
emtx = SPNOISE_EMTX.(EnvVarName);

N=round(Duration*SamplingRate);
timesamples = (1:N)' / SamplingRate;
w=zeros(N+(PreStimSilence+PostStimSilence)*SamplingRate,1);

% force same carrier signal each time!!!!  preserve same carrier for
% single-band versions but keep backward compatible with previous
if SingleBandFrac>0,
   % figure out idxs of component bands
   rseed=idxset(find(idxset(1:index,1)>0, 1, 'last' ),1);
else
   rseed=index;
end
saveseed=rand('seed');
savenseed=randn('seed');
rand('seed',rseed*20);
randn('seed',rseed*20);

if isempty(RelAttenuatedB) || length(RelAttenuatedB)<length(LowFreq),
  RelAttenuatedB=zeros(size(LowFreq));
end

bandcount=min([length(HighFreq),length(LowFreq),length(RelAttenuatedB)]);

if length(IterateStepMS)<bandcount,
   IterateStepMS=repmat(IterateStepMS(1),[1 bandcount]);
end
if length(IterationCount)<bandcount,
   IterationCount=repmat(IterationCount(1),[1 bandcount]);
end

[e,event]=env(o,index,IsRef,TarObject,ThisTarParams);

for bb=1:bandcount,
    if UseBPNoise,
       % use Utility to make consistent across sound objects
        tw=BandpassNoise(LowFreq(bb),HighFreq(bb),Duration,SamplingRate);
        mm=max(abs(tw));
        if IterateStepMS(bb),
           IterateGain=1;
           IterateStepSize=round(IterateStepMS(bb)./1000.*SamplingRate);
           for ii=1:IterationCount(bb),
              tw=tw+shift(tw,IterateStepSize).*IterateGain;
           end
           tw=tw./max(abs(tw)).*mm;
        end
        
        if length(tw)>N,
            disp('trimming');
            tw=tw(1:N);
        elseif length(tw)<N,
            disp('padding');
            tw(end+1:N)=0;
        end
        
    else
        tw=zeros(size(w,1),1);
        if TonesPerBurst(bb)==1,
            lfrange=mean(log([LowFreq(bb) HighFreq(bb)]));
        else
            lfrange=linspace(log(LowFreq(bb)),log(HighFreq(bb)),TonesPerBurst(bb));
        end
        
        % add a bunch of tones at random phase
        for lf=lfrange,
            phase=rand* 2.*pi;
            tw = tw + sin(2*pi*round(exp(lf))*timesamples+phase);
        end
    end
    
    tw=tw./max(abs(tw(:)));%normalize to 1 so that there can be no clipping.
    
    % Now, put it in the silence:
    tw = [zeros(PreStimSilence*SamplingRate,1) ; 
    tw ; zeros(PostStimSilence*SamplingRate,1)];
  
    % apply envelope
    tw=tw.*e(:,bb);
    
    if SingleBandFrac && max(abs(tw))>0,
       tw=tw./max(abs(tw));
    end
    
    % adjust level relative to other bands
    %done in env. Good?
    %level_scale=10.^(-RelAttenuatedB(bb)./20);
    level_scale=1/o.mean; %scale to so that mean envelope is 1. Thus, RMS will be 1, turned into 5 later = 80 dB?
    if bSplitChannels && bb>1,
        w=cat(2,w,tw.*level_scale);
    else
        w=w+tw.*level_scale;
    end
end

%want this?
%w=w./max(abs(w(:)));
% 10ms ramp at onset and offset:
%done in env. Good?
%ramp = hanning(round(.01 * SamplingRate*2));
%ramp = ramp(1:floor(length(ramp)/2));
%ramp=repmat(ramp,[1,chancount]);
%w(1:length(ramp),:) = w(1:length(ramp),:) .* ramp;
%w(end-length(ramp)+1:end,:) = w(end-length(ramp)+1:end,:) .* flipud(ramp);

% normalize min/max +/-5
%w = 5 ./ max(abs(w(:))) .* w;
w=w*5;%5 is 80 dB. Both envelope and carrier max out at 1.

% generate the event structure:
%event = struct('Note',['PreStimSilence , ' Names{index}],...
%    'StartTime',0,'StopTime',PreStimSilence,'Trial',[]);
%event(2) = struct('Note',['Stim , ' Names{index}],'StartTime'...
%    ,PreStimSilence, 'StopTime', PreStimSilence+Duration, 'Trial',[]);
%event(3) = struct('Note',['PostStimSilence , ' Names{index}],...
%    'StartTime',PreStimSilence+Duration, 'StopTime',PreStimSilence+Duration+PostStimSilence,'Trial',[]);

% return random seed to previous state
rand('seed',saveseed);
randn('seed',savenseed);
