function plot_stimulus(RefObject,TarObject,ThisTarParams,TrialSound,TrialSamplingRate,RefTrialIndex,TrialIndex,TarIdx,events,TrialEnv)

if 0 && ThisTarParams.TargetMode
    curr_sr=get(RefObject,'SamplingRate');
    Tenv_dur=size(TrialEnv,1)/curr_sr;
    sr=get(RefObject,'SamplingRateEnv');
    TrialEnv=resample(TrialEnv,sr,curr_sr);
    lf=get(RefObject,'LowFreq');
    hf=get(RefObject,'HighFreq');
    
    subplot(3,1,1);
    hold on;
    for i=1:size(TrialEnv,2)
        imagesc(0:.01:Tenv_dur,[lf(i):hf(i)],repmat(TrialEnv(:,i)',hf(i)-lf(i)+1,1));
    end
    colormap('gray')
    set(gca,'YScale','log')
    
    set(gca,'YLim',[20 20000])
    UTset_cf_labels(gca,1000,1,'ytick');
    set(gca,'XLim',[0 events(end).StopTime]);
     set(gca,'Color','k')
     
    if(1)
        win=1.5;
        set(gca,'YLim',[min(lf)/win max(hf)*win])
        UTset_cf_labels(gca,1000,2,'ytick');
    end
   
    subplot(3,1,2);
    t=(0:(size(TrialEnv,1)-1))/sr;
    plot(t,TrialEnv);
    set(gca,'XLim',[0 events(end).StopTime],'YLim',[0 max(1.01,10^(ThisTarParams.RelativeTarRefdB/20))]);
elseif 0
    
    subplot(2,1,1);
    sr=get(RefObject,'SamplingRateEnv');
    RefObject2=set(RefObject,'SamplingRate',sr);
    RefObject2=set(RefObject2,'PreStimSilence',0);
    RefObject2=set(RefObject2,'PostStimSilence',0);
    [RefEnv,~]=env(RefObject2, RefTrialIndex,TrialIndex);
    lf=get(RefObject,'LowFreq');
    hf=get(RefObject,'HighFreq');
    freq=20:40000;
    dur=length(TrialSound)/TrialSamplingRate;
    dat=zeros(dur*sr,length(freq));
    ref_ind=arrayfun(@(x)~isempty(strfind(x.Note,'Reference')),events) & arrayfun(@(x)~isempty(strfind(x.Note,'Stim ,')),events);
    hold on;
    for i=1:size(RefEnv,2)
        imagesc(events(ref_ind).StartTime:.01:events(ref_ind).StopTime,[lf(i):hf(i)],repmat(RefEnv(:,i)',hf(i)-lf(i)+1,1));
    end
    TarObject2=set(TarObject,'SamplingRate',sr);
    TarObject2=set(TarObject2,'PreStimSilence',0);
    TarObject2=set(TarObject2,'PostStimSilence',0);
    [TarEnv,~] = env(TarObject2, TarIdx, 0);
    TarEnv=TarEnv./max(TarEnv);
    tar_ind=arrayfun(@(x)~isempty(strfind(x.Note,'Target')),events) & arrayfun(@(x)~isempty(strfind(x.Note,'Stim ,')),events);
    tar_lf=get(TarObject,'LoBounds');
    tar_hf=get(TarObject,'HiBounds');
    imagesc(events(tar_ind).StartTime:.01:events(tar_ind).StopTime,[tar_lf:tar_hf],repmat(TarEnv',hf(i)-lf(i)+1,1));
    
    colormap('gray')
    set(gca,'YScale','log')
    
    set(gca,'YLim',[20 20000])
    UTset_cf_labels(gca,1000,1,'ytick');
    set(gca,'XLim',[0 events(end).StopTime]);
    
    if(1)
        win=1.5;
        set(gca,'YLim',[min(lf)/win max(hf)*win])
        UTset_cf_labels(gca,1000,2,'ytick');
    end
    
end
% subplot(3,1,3);
% dfs=50000;
% tw=resample(TrialSound,dfs,TrialSamplingRate);
% plot((1:length(tw))./dfs,tw,'k');
% set(gca,'XLim',[0 events(end).StopTime]);
% set(gca,'YLim',[min(tw) max(tw)])
% ylabel('Signal to DAC (V)');xlabel('Time (sec)')
end