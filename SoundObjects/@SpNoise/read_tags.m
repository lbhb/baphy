function params=read_tags(o,tags)
for i=1:length(tags);
    t=strsep(tags{i});
    tagr{i}=t{3};
    t=strsep(tagr{i},'+');
    tagr_{i}=t;
    if length(tagr_{i})==2
        if strcmp(tagr_{i}{1},'BNB')
            t=strsep(tagr_{i}{2},':');
            params.Sp(i,1)=str2double(t{1}(3:end));
          %  params.Lev(i,1)=t{2};
        else
            t=strsep(tagr_{i}{3},':');
            params.Sp(i,2)=str2double(t{1}(3:end));
           % params.Lev(i,2)=t{2};
        end
    else
        t=strsep(tagr_{i}{2},':');
        params.Sp(i,1)=str2double(t{1}(3:end));
        %params.Lev(i,1)=t{2};
        t=strsep(tagr_{i}{3},':');
        params.Sp(i,2)=str2double(t{1}(3:end));
        %params.Lev(i,2)=t{2};
    end
end
params.runidx=3*ones(length(tags),1);
params.runidx(isnan(params.Sp(:,1)) | isnan(params.Sp(:,2)))=1;
params.runidx(params.Sp(:,1) == params.Sp(:,2))=2;
% 1 - single stream
% 2 - coherent
% 3 - incoherent