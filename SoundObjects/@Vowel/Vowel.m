function o = Vowel (varargin)

% LAS: copied from FreqLevelTone Dec 2019

switch nargin
case 0
    % if no input arguments, create a default object
    s = SoundObject ('Vowel', 40000, 0,0, 0, ...
        {''}, 1, {'Formants','edit',[900 1200 2440],...
        'BaseF0','edit',100,'OctaveBelow','edit',2,...
        'OctaveAbove','edit',3,'F0sPerOctave','edit',5,'LowdB','edit',50,...
        'HighdB','edit',80,'Steps','edit',3 ,'amHz','edit',0,'ModDepth','edit',1, ...
        'Duration','edit',1000,'SamplingRate','edit',40000,...
        'RefRepCount','edit',1});
    o.Formants=[900 1200 2440];
    o.BaseF0 = 1000;
    o.OctaveBelow = 2;
    o.OctaveAbove = 3;
    o.F0sPerOctave = 5;
    o.amHz = 0;
    o.ModDepth=1;
    o.RefRepCount=1;
    o.Duration = 1;  %ms
    o.LowdB = 30;
    o.HighdB = 80;
    o.Steps = 5;
    o.OverrideAutoscale=1;  %don't force +/- 5v normalization
    o = class(o,'Vowel',s);
    o = ObjUpdate (o);
case 1
    % if single argument of class SoundObject, return it
    if isa(varargin{1},'SoundObject')
        o = varargin{1};
    else
        error('Wrong argument type');
    end
case 9
    s = SoundObject ('Vowel', varargin{1}, varargin{2},varargin{3}, varargin{4}, ...
        {''}, 1, {'BaseF0','edit',varargin{5},'OctaveBelow','edit',varargin{6},...
        'OctaveAbove','edit',varargin{7},'F0sPerOctave','edit',varargin{8},...
        'Duration','edit',varargin{9},'SamplingRate','edit',varargin{1}});
    o.BaseF0 = varargin{5};
    o.OctaveBelow = varargin{6};
    o.OctaveAbove = varargin{7};
    o.F0sPerOctave = varargin{8};
    o.Duration = varargin{9};  %ms
    o.LowdB = varargin{10};
    o.HighdB = varargin{11};
    o.Steps = varargin{12};
    o = class(o,'FreqLevelTone',s);
    o = ObjUpdate (o);        
otherwise
    error('Wrong number of input arguments');
end