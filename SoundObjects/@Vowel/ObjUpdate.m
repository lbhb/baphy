function o = ObjUpdate (o);
%

% LAS, Dec 2019
BaseF0 = ifstr2num(get(o,'BaseF0'));
OctaveBelow = ifstr2num(get(o,'OctaveBelow'));
OctaveAbove = ifstr2num(get(o,'OctaveAbove'));
F0sPerOctave = ifstr2num(get(o,'F0sPerOctave'));
LowdB = get(o,'LowdB');
HighdB = get(o,'HighdB');
Steps = get(o,'Steps');


% now, generate a list of all frequencies needed:
LowFrequency = (BaseF0 / 2^OctaveBelow);
Frequencies = LowFrequency * 2.^(0:1/F0sPerOctave:OctaveBelow+OctaveAbove);
%
dBs = LowdB:Steps:HighdB;
count= 0;
for cnt1 = 1:length(Frequencies)-1
    for cnt2 = 1:length(dBs)
        count= count +1;
        %RandomFrequency = ceil(Frequencies(cnt1) + rand(1) * (Frequencies(cnt1+1)-Frequencies(cnt1)));
        Names{count,1} = [num2str(ceil(Frequencies(cnt1)))];
        Names{count,2} = dBs(cnt2);
    end
end
%o = set(o,'Loudness',dBs);
o = set(o,'Names',Names);
o = set(o,'MaxIndex',length(Names));