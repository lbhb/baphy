function [w, ev, dB]=waveform (o,index, IsRef)
% function w=waveform(t);
% this function is the waveform generator for object FrequencyTuning


SamplingRate = ifstr2num(get(o,'SamplingRate'));
PreStimSilence = ifstr2num(get(o,'PreStimSilence'));
PostStimSilence = ifstr2num(get(o,'PostStimSilence'));
amHz = ifstr2num(get(o,'amHz'));
ModDepth = ifstr2num(get(o,'ModDepth'));

% the parameters of tone object
SamplingRate = ifstr2num(get(o,'SamplingRate'));
Duration = ifstr2num(get(o,'Duration')); % duration is second
Names = get(o,'Names');
Frequency = ifstr2num(Names{index,1});
% now generate a Vowel with specified frequency:
%t = Tone(SamplingRate, 0, 0,0,Frequency, Duration);
%[w, ev] = waveform(t);
%clear t;
tw=MakeVowel(o,round(SamplingRate*Duration),Frequency,SamplingRate,o.Formants(1),o.Formants(2),o.Formants(3))';
sf=max(abs(tw));
tw=tw./sf;

f=ones(size(tw));
pn=round(SamplingRate*0.005);    % 5 ms rise/fall time
up = sin(2*pi*(0:pn-1)/(4*pn)).^2;   %add sinramp
down = sin(2*pi*(pn+1:2*pn)/(4*pn)).^2;
f = [up ones(1,length(tw)-2*pn) down]';
tw=tw.*f;

w = 5 * tw;
w = [zeros(PreStimSilence*SamplingRate,1) ; w(:) ;zeros(PostStimSilence*SamplingRate,1)];

dB = ifstr2num(Names{index,2});

atten_db=80-dB;
scaleby=10.^(-atten_db./20);
fprintf('db: %d, attenuating by %d in software\n',dB,atten_db');
w=w*scaleby;

Name=[Names{index,1},':',num2str(dB),'dB'];
ev = struct('Note',['PreStimSilence , ' Name],...
    'StartTime',0,'StopTime',PreStimSilence,'Trial',[]);
ev(2) = struct('Note',['Stim , ' Name],...
    'StartTime',PreStimSilence, 'StopTime', PreStimSilence+Duration,'Trial',[]);
ev(3) = struct('Note',['PostStimSilence , ' Name],...
    'StartTime',PreStimSilence+Duration, 'StopTime',PreStimSilence+Duration+PostStimSilence,'Trial',[]);
%disp(['freq= ' num2str(Frequency)])
%disp(['dB= ' num2str(dB)])