function [w,event]=waveform (o,index,IsRef)
% function w=waveform(o, index);
% This is a generic waveform generator function for objects inherit from
% SoundObject class. It simply reads the Names field and load the one
% indicated by index. It assumes the files are in 'Sounds' subfolder in the
% object's folder.

% Nima, nov 2005
maxIndex = get(o,'MaxIndex');
if index > maxIndex
    error (sprintf('Maximum possible index is %d',maxIndex));
end
%
event=[];
SamplingRate = ifstr2num(get(o,'SamplingRate'));
PreStimSilence = ifstr2num(get(o,'PreStimSilence'));
PostStimSilence = ifstr2num(get(o,'PostStimSilence'));
% If more than two values are specified, choose a random number between the
% two:
if length(PreStimSilence)>1
    PreStimSilence = PreStimSilence(1) + diff(PreStimSilence) * rand(1);
end
if length(PostStimSilence)>1
    PostStimSilence = PostStimSilence(1) + diff(PostStimSilence) * rand(1);
end

soundpath=get(o,'SoundPath');
files = get(o,'Names');
tFileName=files{index};
tFileName=strsep(tFileName,':');
if(length(tFileName)>1)
    chnl=tFileName{2};
else
    chnl=1;
end
tFileName=tFileName{1};
if exist([soundpath filesep tFileName],'file')
    [w,fs] = wavread([soundpath filesep tFileName]);
else
    object_spec = what('RSS');
    soundpath3 = [object_spec(1).path filesep 'sounds3'];
     [w,fs] = wavread([soundpath3 filesep tFileName]);
end



% Check the sampling rate:
if fs~=SamplingRate
    w = resample(w, SamplingRate, fs);
end

% If the object has Duration parameter, cut the sound to match it, if
% possible:
if isfield(get(o),'Duration')
    Duration = ifstr2num(get(o,'Duration'));
    totalSamples = floor(Duration * SamplingRate);
    w = w(1:min(length(w),totalSamples));
else
    Duration = length(w) / SamplingRate;
end

% Now, put it in the silence:
w = [zeros(ceil(PreStimSilence*SamplingRate),1) ; w(:) ;zeros(ceil(PostStimSilence*SamplingRate),1)];

% hard code scale by 5 to standard "80dB" level
%I added a factor of 3 because the average max is 0.33, not 1. This way
%average across RSS set 1 is "80 dB" at peak of waveform (not true for
%RMS).
%w=w*5;
w=w*15;

if chnl>1
    w=cat(2,zeros(length(w),chnl-1),w);
end

% and generate the event structure:
event = struct('Note',['PreStimSilence , ' files{index}],...
    'StartTime',0,'StopTime',PreStimSilence,'Trial',[]);
event(2) = struct('Note',['Stim , ' files{index}],'StartTime'...
    ,PreStimSilence, 'StopTime', PreStimSilence+Duration, 'Trial',[]);
event(3) = struct('Note',['PostStimSilence , ' files{index}],...
    'StartTime',PreStimSilence+Duration, 'StopTime',PreStimSilence+Duration+PostStimSilence,'Trial',[]);

