function [w,event,e,TarObject,ThisTarParams]=waveform (o,index,IsRef,TarObject,ThisTarParams)
% function w=waveform(o, index,IsRef);
%
% generate waveform for SpNoise object
%

global SPNOISE_EMTX
if nargin<3, IsRef=1; end
if(nargin<4)
    TarObject=[];
    ThisTarParams=[];
end

RelAttenuatedB=get(o,'RelAttenuatedB');
%SplitChannels=get(o,'SplitChannels');
%bSplitChannels=strcmpi(SplitChannels,'Yes');
SamplingRate = get(o,'SamplingRate');
Duration = get(o,'Duration'); % duration is second
PreStimSilence = get(o,'PreStimSilence');
PostStimSilence = get(o,'PostStimSilence');
Names = get(o,'Names');
SamplingRateEnv = get(o,'SamplingRateEnv');
BaseSound=strtrim(get(o,'BaseSound'));
Subsets = get(o,'Subsets');
BaselineFrac=get(o,'BaselineFrac');
idxset=get(o,'idxset');
ShuffledOnsetTimes=get(o,'ShuffledOnsetTimes');
SingleBandFrac=get(o,'SingleBandFrac');

EnvVarName=[BaseSound,num2str(Subsets)];
%emtx = get(o,'emtx');
if ~strcmp(BaseSound,'blnoise')
    emtx = SPNOISE_EMTX.(EnvVarName);
end

N=round(Duration*SamplingRate);
timesamples = (1:N)' / SamplingRate;
w=zeros(N+(PreStimSilence+PostStimSilence)*SamplingRate,1);



if ~isempty(TarObject) && (strcmp(ThisTarParams.TargetMode,'Envelope') || strcmp(ThisTarParams.TargetMode,'RefDependentWaveform'))
    [e,event,idx,TarObject,ThisTarParams]=env(o,index,IsRef,TarObject,ThisTarParams);
else
    [e,event,idx]=env(o,index,IsRef);
end

bandcount=size(e,2);

if isempty(RelAttenuatedB) || length(RelAttenuatedB)<bandcount
  RelAttenuatedB=zeros(bandcount,1);
end

if o.FreezeNoise
    saveseed=rng;%saveseed=rand('seed');
    rng('default');
end
for bb=1:bandcount
    if ischar(o.CarrierObj) && strcmp(o.CarrierObj,'blnoise')
         if bb==1
              if o.FreezeNoise
                     rng(idx(bb)+1000);
              end
             [tw_,X0]=gen_vocoded_noise(SamplingRate,Duration,[o.MaskFc-o.MaskBW/2 o.MaskFc+o.MaskBW/2],e(:,bb,1),1);
         else
             Fc=[o.FlankFcs(bb-1)-o.MaskBW/2 o.FlankFcs(bb-1)+o.MaskBW/2];
             if idx(bb)==idx(1) %Coherent
                [tw_]=gen_bandlimited_noise(SamplingRate,Duration,Fc,X0);
             else
                 if o.FreezeNoise
                     rng(idx(bb)+1000);
                 end
                [tw_]=gen_vocoded_noise(SamplingRate,Duration,Fc,e(:,bb,1),1);
             end
         end
         %% Set Levels
         rms_(bb)=rms(tw_);
         peak_(bb)=max(abs(tw_));
        tw_=tw_/rms_(bb)*10^(-25/20);%Set RMS to 0.0562 (-25 dB)
        if bb>1
            tw_=tw_*10^(-RelAttenuatedB(bb-1)/20);
        end
        
        %% Apply syllable-speed envelope
        tw=e(:,bb,2).*tw_;

    else
        error('Add stuff here to use tone/other carriers.'); % See SpTones for inspiration
    end
    w=w+tw;    
   
 
    
    % adjust level relative to other bands
    %done in env. Good?
    %level_scale=10.^(-RelAttenuatedB(bb)./20);
    %level_scale=1/o.mean; %scale to so that mean envelope is 1. Thus, RMS will be 1, turned into 5 later = 80 dB?
    %tw_(:,bb)=tw.*level_scale;
end
if max(abs(w))>1
    warning('Sound too loud! max(abs(w)) is %f',max(abs(w)))
end
if o.FreezeNoise
    rng(saveseed);
end
% if all(cellfun(@(x)isa(x,'ComplexTone'),o.CarrierObj))
%     sum_amps=cellfun(@(x)sum(get(x,'ComponentAmplitudes')),o.CarrierObj);
% elseif isa(o.CarrierObj{1},'ComplexTone')
%     %hack for ComplexTone vs Noise
%     sum_amps=cellfun(@(x)sum(get(x,'ComponentAmplitudes')),o.CarrierObj(1));
%     sum_amps=sum_amps([1 1]);
% end
%  nfs=sum_amps * max(nf)/max(sum_amps);
% %rms_per=cellfun(@(x)sqrt(sum(get(x,'ComponentAmplitudes').^2)/2),o.CarrierObj);
% %nfs=rms_per * max(nf) / max(rms_per);
% for bb=1:bandcount
%     if bSplitChannels
%         w(:,bb)=tw_(:,bb)/nfs(bb);
%     else
%         %w=w+tw.*level_scale;
%         w=w + tw_(:,bb)/nfs(bb);
%     end
% end
%want this?
%w=w./max(abs(w(:)));
% 10ms ramp at onset and offset:
%done in env. Good?
%ramp = hanning(round(.01 * SamplingRate*2));
%ramp = ramp(1:floor(length(ramp)/2));
%ramp=repmat(ramp,[1,chancount]);
%w(1:length(ramp),:) = w(1:length(ramp),:) .* ramp;
%w(end-length(ramp)+1:end,:) = w(end-length(ramp)+1:end,:) .* flipud(ramp);

% normalize min/max +/-5
%w = 5 ./ max(abs(w(:))) .* w;
w=w*5;%5 is 80 dB. Both envelope and carrier max out at 1.

% generate the event structure:
%event = struct('Note',['PreStimSilence , ' Names{index}],...
%    'StartTime',0,'StopTime',PreStimSilence,'Trial',[]);
%event(2) = struct('Note',['Stim , ' Names{index}],'StartTime'...
%    ,PreStimSilence, 'StopTime', PreStimSilence+Duration, 'Trial',[]);
%event(3) = struct('Note',['PostStimSilence , ' Names{index}],...
%    'StartTime',PreStimSilence+Duration, 'StopTime',PreStimSilence+Duration+PostStimSilence,'Trial',[]);

