function [e,event,idx,TarObject,ThisTarParams]=env(o,index,IsRef,TarObject,ThisTarParams)
% function w=env(o, index,IsRef);
%
% generate envelope for CMRnoise object
%
global SPNOISE_EMTX

if(nargin<4)
    TarObject=[];
end

FlankFcs=get(o,'FlankFcs');
RelAttenuatedB=get(o,'RelAttenuatedB');
SamplingRate = get(o,'SamplingRate');
Duration = get(o,'Duration'); % duration is second
PreStimSilence = get(o,'PreStimSilence');
PostStimSilence = get(o,'PostStimSilence');
Names = get(o,'Names');
BaseSound=strtrim(get(o,'BaseSound'));
Subsets = get(o,'Subsets');
BaselineFrac=get(o,'BaselineFrac');
MaskStart=get(o,'MaskStart');

if strcmp(BaseSound,'blnoise')
    SamplingRateEnv=SamplingRate;
else
    EnvVarName=[BaseSound,num2str(Subsets)];
%emtx = get(o,'emtx');
if 0
    emtx = SPNOISE_EMTX.(EnvVarName);
    SamplingRateEnv = get(o,'SamplingRateEnv');
else
   emtx = SPNOISE_EMTX.([EnvVarName,'_resampled_',num2str(o.SamplingRate)]);
   SamplingRateEnv = get(o,'SamplingRate');
end
end

timesamples = (1 : round(Duration*SamplingRate))' / SamplingRate;


% force same carrier signal each time!!!!
%not needed for env?
%saveseed=rand('seed');
%rand('seed',index*20);

if isempty(RelAttenuatedB) || length(RelAttenuatedB)<length(FlankFcs)+1
  RelAttenuatedB=zeros(1,length(FlankFcs)+1);
end
%create empty event structure
event=struct('Note','','StartTime',0,'StopTime',0,'Trial',[]);event(1)=[];
bandcount = length(FlankFcs) + 1;
idxset=get(o,'idxset');
idxset2=get(o,'idxset2');
ShuffledOnsetTimes=get(o,'ShuffledOnsetTimes');
e=zeros(length(timesamples),bandcount,2); %1 is fine envelope, 2 is syllable-speed envelope

%ensure that target band is created first if there is a target, so it can
%be used if o.ForceTargetEnvelope is 'Coherent'
% if ~isempty(TarObject)
%     Ntar=length(ThisTarParams.TargetChannel);
%     bandorder=[ThisTarParams.TargetChannel(1) setdiff(bandorder,ThisTarParams.TargetChannel(1))];
%     if MaskStart(2)>0 && MaskStart(1)==0
%         MaskStart(1)=bandorder(2);
%         if length(bandorder)>2
%             error('This isn''t set up for 3 streams')
%         end
%     end
% end
idx_str=repmat('%d,',1,size(idxset,2)); idx_str(end)=[];
fprintf(['Index=[',idx_str,']\n'],idxset(index,:))
idx=idxset(index,:);
if ~isempty(idxset2)
    idx2=idxset2(index,:);
end
impose_correlation = any(mod(idxset(index,:),1)~=0);
if impose_correlation
    idx = floor(idx);
    corr = mod(idxset(index,mod(idxset(index,:),1)~=0),1);
    corr = round(corr*1000)/1000;
end

%% Create syllable-speed envelope
Esingle=[UTcosramp(o.Ton,o.Tramp,o.Tramp,SamplingRateEnv),zeros(1,round((o.Trep-o.Ton)*SamplingRateEnv))];
Npulses=ceil(o.Duration/o.Trep);

if o.MaskerShift~=0 && o.MaskerShiftSet(index)~=0
    Npad=round(o.MaskerShift*SamplingRateEnv);
    pad=zeros(1,Npad);   
    Tarpulse=round(ThisTarParams.TarStartBin/SamplingRateEnv/o.Trep,10)+1;
    if all(mod(Tarpulse,1)~=0), error('Target isn''t synchronous with masker'); end
    if length(Tarpulse)==1
        Em=[pad,repmat(Esingle,1,Tarpulse-2),...
        [UTcosramp(o.Ton,o.Tramp,o.Tramp,SamplingRateEnv),zeros(1,round((o.Trep-o.Ton-o.MaskerShiftSet(index)/2)*SamplingRateEnv))],...
        [UTcosramp(o.Ton,o.Tramp,o.Tramp,SamplingRateEnv),zeros(1,round((o.Trep-o.Ton+o.MaskerShiftSet(index)/2)*SamplingRateEnv))],...%tar
        repmat(Esingle,1,Npulses-Tarpulse)]; %Masker Envelope
    e(:,1,2)=Em(1:end-Npad);
    Ef=[repmat(Esingle,1,Tarpulse-2),...
        [UTcosramp(o.Ton,o.Tramp,o.Tramp,SamplingRateEnv),zeros(1,round((o.Trep-o.Ton+o.MaskerShiftSet(index)/2)*SamplingRateEnv))],...
        [UTcosramp(o.Ton,o.Tramp,o.Tramp,SamplingRateEnv),zeros(1,round((o.Trep-o.Ton-o.MaskerShiftSet(index)/2)*SamplingRateEnv))],...%tar
        repmat(Esingle,1,Npulses-Tarpulse)];
    elseif length(Tarpulse)==2
        if diff(Tarpulse)<2, error('This only works if there''s at least 1 pulse of reference between the two targets'); end
        Tarpulse=sort(Tarpulse);
         Em=[pad,repmat(Esingle,1,Tarpulse(1)-2),...
        [UTcosramp(o.Ton,o.Tramp,o.Tramp,SamplingRateEnv),zeros(1,round((o.Trep-o.Ton-o.MaskerShiftSet(index)/2)*SamplingRateEnv))],...
        [UTcosramp(o.Ton,o.Tramp,o.Tramp,SamplingRateEnv),zeros(1,round((o.Trep-o.Ton+o.MaskerShiftSet(index)/2)*SamplingRateEnv))],...%tar
        repmat(Esingle,1,diff(Tarpulse)-2),...
        [UTcosramp(o.Ton,o.Tramp,o.Tramp,SamplingRateEnv),zeros(1,round((o.Trep-o.Ton-o.MaskerShiftSet(index)/2)*SamplingRateEnv))],...
        [UTcosramp(o.Ton,o.Tramp,o.Tramp,SamplingRateEnv),zeros(1,round((o.Trep-o.Ton+o.MaskerShiftSet(index)/2)*SamplingRateEnv))],...%tar2
        repmat(Esingle,1,Npulses-Tarpulse(2))]; %Masker Envelope
    e(:,1,2)=Em(1:end-Npad);
    Ef=[repmat(Esingle,1,Tarpulse(1)-2),...
        [UTcosramp(o.Ton,o.Tramp,o.Tramp,SamplingRateEnv),zeros(1,round((o.Trep-o.Ton+o.MaskerShiftSet(index)/2)*SamplingRateEnv))],...
        [UTcosramp(o.Ton,o.Tramp,o.Tramp,SamplingRateEnv),zeros(1,round((o.Trep-o.Ton-o.MaskerShiftSet(index)/2)*SamplingRateEnv))],...%tar
        repmat(Esingle,1,diff(Tarpulse)-2),...
        [UTcosramp(o.Ton,o.Tramp,o.Tramp,SamplingRateEnv),zeros(1,round((o.Trep-o.Ton+o.MaskerShiftSet(index)/2)*SamplingRateEnv))],...
        [UTcosramp(o.Ton,o.Tramp,o.Tramp,SamplingRateEnv),zeros(1,round((o.Trep-o.Ton-o.MaskerShiftSet(index)/2)*SamplingRateEnv))],...%tar
        repmat(Esingle,1,Npulses-Tarpulse(2))];
    else
        error('Fix me')
    end
    TarShiftN=round(o.MaskerShiftSet(index)/2*SamplingRateEnv);
    ThisTarParams.TarStartBin=ThisTarParams.TarStartBin+TarShiftN;
    TarObject=set(TarObject,'PostStimSilence',get(TarObject,'PostStimSilence')-TarShiftN/SamplingRateEnv);
    e(:,2:end,2)=repmat(Ef',1,bandcount-1);
else
    Ef=repmat(Esingle',Npulses,1);
    e(:,:,2)=repmat(Ef,1,bandcount);
end
% %Et=zeros(size(Em));
% %Et(tari)=Esingle;
% te=0:1/SamplingRateEnv:(o.Ton-1/SamplingRateEnv);
% AMenv=(1-cos(2*pi*fAM*te))/2;
% AMenv=[AMenv.*UTcosramp(o.Ton,o.Tramp,o.Tramp,SamplingRateEnv),zeros(1,round((Trep-o.Ton)*SamplingRateEnv))];
% EtAM(tari)=AMenv;
% 
% %Zero-pad start
% %pad_length=0.5;%seconds
% Ef=[zeros(1,pad_length*fs),Ef,zeros(1,pad_length*fs)];
% Em=[zeros(1,pad_length*fs),Em,zeros(1,pad_length*fs)];
% Et=[zeros(1,pad_length*fs),Et,zeros(1,pad_length*fs)];
% EtAM=[zeros(1,pad_length*fs),EtAM,zeros(1,pad_length*fs)];
% 
% Masker=Em.*Masker_(1:length(Et));
% if any(make([1 3]))
%     FlankersI=repmat(Ef,length(Ff),1).*FlankersI_(:,1:length(Et));
% end
% FlankersC=repmat(Ef,length(Ff),1).*FlankersC_(:,1:length(Et));
% TargetAM=EtAM.*TargetAM_(1:length(Et));
% Target=Et.*Target_(1:length(Et));
% TargetMask=Et>0;

saveseed=rng;%saveseed=rand('seed');
rng('default');

for bb=1:bandcount
    %Masker when bb=1, flankers for the rest
    if idxset(index,bb)==0
        continue
    end
    tw=zeros(length(timesamples),1);
    
    % Make masker envelope
    if strcmp(BaseSound,'blnoise')
        rng(idx(bb));    
        if bb==1 || 1
            [BLMasker_]=gen_bandlimited_noise(SamplingRateEnv,Duration,[o.MaskFc-o.MaskBW/2 o.MaskFc+o.MaskBW/2]);
        else
            [BLMasker_]=gen_bandlimited_noise(SamplingRateEnv,Duration,[o.FlankFcs(bb-1)-o.MaskBW/2 o.FlankFcs(bb-1)+o.MaskBW/2]);
        end
        sp=abs(hilbert(BLMasker_));
        sp=(sp-min(sp))/range(sp);
    else
        sp=emtx(:,idx(bb));
        sp=sp(1:find(~isnan(sp), 1, 'last' ));
    end

    % repeat envelope if Duration longer than orginal waveform
    % (3-sec, typically)
    segrepcount=ceil(Duration ./ (length(sp)./SamplingRate));
    sp=repmat(sp,[segrepcount 1]);
    if ShuffledOnsetTimes(index,bb)>0,
        sp=shift(sp,round(ShuffledOnsetTimes(index,bb)*SamplingRateEnv));
    end
    if BaselineFrac>0 && BaselineFrac<1,
        sp=sp.*(1-BaselineFrac) + BaselineFrac;
    end
    if o.ModDepth<1
       sp= sp.*o.ModDepth + o.mean*(1-o.ModDepth);%sets mean to stay at overall mean of all envelopes (o.mean)
       %sp= sp.*o.ModDepth + 0.5*(1-o.ModDepth);%sets mean of range to stay at 0.5
    end
    sp=resample(sp,SamplingRate,SamplingRateEnv);
    if ~isempty(idxset2) && idx(bb)~=idx2(bb)
        sp2=emtx(:,idx2(bb));
        sp2=sp2(1:find(~isnan(sp2), 1, 'last' ));
        % repeat envelope if Duration longer than orginal waveform
        % (3-sec, typically)
        segrepcount=ceil(Duration ./ (length(sp2)./SamplingRate));
        sp2=repmat(sp2,[segrepcount 1]);
        if ShuffledOnsetTimes(index,bb)>0,
            sp2=shift(sp2,round(ShuffledOnsetTimes(index,bb)*SamplingRateEnv));
        end
        if BaselineFrac>0 && BaselineFrac<1,
            sp2=sp2.*(1-BaselineFrac) + BaselineFrac;
        end
        if o.ModDepth<1
           sp2= sp2.*o.ModDepth + o.mean*(1-o.ModDepth);%sets mean to stay at overall mean of all envelopes (o.mean)
           %sp2= sp2.*o.ModDepth + 0.5*(1-o.ModDepth);%sets mean of range to stay at 0.5
        end
        sp2=resample(sp2,SamplingRate,SamplingRateEnv);
        idx2inds=((SamplingRate*o.IncSwitchTime):length(sp))';
        RampDur=.02;
        ramp=ones(size(idx2inds));
        sinePeriod = 2*RampDur;
        npts = sinePeriod*o.SamplingRate;
        sineFrequency = 1/sinePeriod;

        sineBuffer = -cos(2*pi*sineFrequency*(0:npts-1)/o.SamplingRate);
        sineBuffer = (sineBuffer + 1)/2;
        N = round(length(sineBuffer)/2);
        ramp(1:N)=sineBuffer(1:N);
        sp(idx2inds)=ramp.*sp2(idx2inds)+(1-ramp).*sp(idx2inds);
    
    end
    % make sure envelope and noise have same duration
    if length(sp)<length(tw),
        disp('desired Duration too long, trimming!!!');
        [length(sp) length(tw)]
        tw=tw(1:length(sp));
    elseif length(sp)>length(tw),
        sp=sp(1:length(tw));
    end
    
    if MaskStart(2)>0 && MaskStart(1)==bb
        if length(bandorder)>2
            error('This isn''t set up for 3 streams')
        end
        sp(1:round(SamplingRate*MaskStart(2)))=0;
    end
    
    if impose_correlation
        if bb==bandorder(1)
            sp1=sp;
        elseif bb==bandorder(2)
            means=mean([sp1 sp]);
            env_orthog=UTgs_orthog(sp1-means(1),sp-means(2))+means(2);
            sp=corr.*sp1+sqrt(1-corr.^2).*env_orthog;
        else
            error('Why are we here?')
        end
    end
    
    
    if ~isempty(TarObject) && strcmp(ThisTarParams.TargetMode,'Envelope')
        %target is a change in envelope. Apply it here before multipying by carrier
        for tari=1:Ntar
            if isnan(ThisTarParams.TargetChannel(tari)), continue; end
            if ThisTarParams.TargetChannel(tari)==bb
                TarPreStim=get(TarObject,'PreStimSilence');
                TarPostStim=get(TarObject,'PostStimSilence');
                TarObject=set(TarObject,'PreStimSilence',0);
                TarObject=set(TarObject,'PostStimSilence',0);
                [tarenv,tarevent]=env(TarObject,ThisTarParams.ThisTarIdx(tari), 0);%envelope comes out from 0 to 1 for ModDepth=1.
                TarAddTime=PreStimSilence+TarPreStim+ThisTarParams.TarStartBin(tari)/SamplingRate;
                for cnt2 = 1:length(tarevent)
                    tarevent(cnt2).Note = [tarevent(cnt2).Note ' , Target'];
                    tarevent(cnt2).StartTime = tarevent(cnt2).StartTime + TarAddTime;
                    tarevent(cnt2).StopTime = tarevent(cnt2).StopTime + TarAddTime;
                end
                tarevent(1).StartTime=tarevent(1).StartTime-TarPreStim;
                tarevent(3).StopTime=tarevent(end).StopTime+TarPostStim;
                event(length(event)+(1:length(tarevent)))=tarevent;
                tarenv=tarenv*10^(ThisTarParams.RelativeTarRefdB(tari)/20);%Scale peak of envelope (usually to RMS-match).
                tarinds=ThisTarParams.TarStartBin(tari)+round(TarPreStim*SamplingRate)+(1:length(tarenv));
                EnvTarRefRatio=ThisTarParams.EnvTarRefRatio(tari);
                RampDur=ThisTarParams.EnvTarRefRampDur(tari);
                TRRramp=EnvTarRefRatio*ones(size(tarenv));
                sinePeriod = 2*RampDur;
                npts = sinePeriod*o.SamplingRate;
                sineFrequency = 1/sinePeriod;
                
                sineBuffer = -cos(2*pi*sineFrequency*(0:npts-1)/o.SamplingRate);
                sineBuffer = EnvTarRefRatio*(sineBuffer + 1)/2;
                N = round(length(sineBuffer)/2);
                N2=length(sineBuffer)-N;
                TRRramp(1:N)=sineBuffer(1:N);
                TRRramp(end-N2+1:end)=sineBuffer(N+1:end);
                switch o.ForceTargetEnvelope
                    case 'No        '
                        %do nothing
                    case 'Coherent  '
                        spTar=sp;
                        if Ntar>1, error('Fix me'); end
                end
                sp(tarinds)=TRRramp.*tarenv+(1-TRRramp).*sp(tarinds);
                if(0)
                    t=(1:length(sp))/SamplingRate*1000;
                    figure;plot(t,sp);
                    hold on;plot(t(tarinds),TRRramp,'k');
                    ylim([0 1])
                end
            else
                switch o.ForceTargetEnvelope
                    case 'No        '
                        %do nothing
                    case 'Coherent  '
                        %force non-target streams to be coherent with target
                        %stream during the target period for PSTH analysis
                        nf=max(TRRramp);
                        if nf==0, nf=1; end
                        TCramp=TRRramp./nf;%use ramp used for tar:ref ratio, but set tar:ref ratio to 1.
                        sp(tarinds)=TCramp.*spTar(tarinds)+(1-TCramp).*sp(tarinds);
                        if(0)
                            t=(1:length(sp))/SamplingRate*1000;
                            figure;plot(t,[sp,sp2,spTar]);
                            hold on;plot(t(tarinds),TRRramp,'k');
                            ylim([0 1])
                        end
                end
            end
        end
    end
    
    % apply envelope
    tw(:)=sp;
    
    % adjust level relative to other bands
    level_scale=10.^(-RelAttenuatedB(bb)./20);
    e(:,bb,1)=tw.*level_scale;
    
    % 10ms ramp at onset and offset:
%     ramp = hanning(round(.01 * SamplingRate*2));
%     ramp = ramp(1:floor(length(ramp)/2));
%     e(1:length(ramp),bb,1) = e(1:length(ramp),bb,1) .* ramp;
%     e(end-length(ramp)+1:end,bb,1) = e(end-length(ramp)+1:end,bb,1) .* flipud(ramp);
end
rng(saveseed);
%e=e./max(abs(e(:)));

% Now, put it in the silence:
e = [zeros(PreStimSilence*SamplingRate,bandcount,2) ; e ;zeros(PostStimSilence*SamplingRate,bandcount,2)];

% generate the event structure:
if ~isempty(TarObject)
    coh_str=get_target_suffixes(o,index);
    for i=1:length(event)
        comma_inds=strfind(event(i).Note,',');
        event(i).Note=[event(i).Note(1:comma_inds(2)-2),':',coh_str{1},event(i).Note(comma_inds(2)-1:end)];
    end  
end
thisevent(1) = struct('Note',['PreStimSilence , ' Names{index}],...
    'StartTime',0,'StopTime',PreStimSilence,'Trial',[]);
thisevent(2) = struct('Note',['Stim , ' Names{index}],'StartTime'...
    ,PreStimSilence, 'StopTime', PreStimSilence+Duration, 'Trial',[]);
thisevent(3) = struct('Note',['PostStimSilence , ' Names{index}],...
    'StartTime',PreStimSilence+Duration, 'StopTime',PreStimSilence+Duration+PostStimSilence,'Trial',[]);
event=[thisevent event];%if targets exist, put them at the end to keep convention
% return random seed to previous state
%not needed for env?
%rand('seed',saveseed);


