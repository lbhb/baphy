function stim_sig=get_target_suffixes(o,varargin)
%(o,indexes,o_struct)
indexes=getparmC(varargin,1,[]);
o_struct=getparmC(varargin,2,[]);
if ~isempty(o_struct)
    idxset=o_struct.idxset;
else
    idxset=o.idxset;
end
if isempty(indexes)
    do_unique=true;
    indexes=1:size(idxset,1);
else
    do_unique=false;
end
for k=1:length(indexes)
    coh_str{k}=[];
    type=zeros(size(idxset,2),size(idxset,2));
    for i=1:size(idxset,2)
        for j=i+1:size(idxset,2)
            if size(idxset,2) == 2
                str='';
            else
                str=sprintf('S%d-%d',i,j);
            end
            if idxset(indexes(k),i)==idxset(indexes(k),j)
                coh_str{k}=[coh_str{k},[str,'Coh ']];
                type(i,j)=1;
            else
                coh_str{k}=[coh_str{k},[str,'Inc ']];
                type(i,j)=2;
            end
        end
    end
    coh_str{k}(end)=[];
    TotalComparisons = size(idxset,2)*(size(idxset,2)-1)/2;
    if size(idxset,2)>2
        if sum(sum(type==1)) == TotalComparisons
            coh_str{k}='Coh';
        elseif sum(sum(type==2)) == TotalComparisons
           coh_str{k}='Inc';
        end
    end
    if isequal(o.MaskerShift,0)
        shift_str{k}='';
    else
        shift_str{k}=sprintf('-Sf%d',o.MaskerShiftSet(indexes(k))*1000);
    end
end
stim_sig=cellfun(@(x,y)[x,y],coh_str,shift_str,'Uni',0);
if do_unique
    stim_sig=unique(stim_sig);
end
end