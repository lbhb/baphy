function o = ObjUpdate (o);
%
% piggyback on top of speech object to get waveform
global FORCESAMPLINGRATE SPNOISE_EMTX


SamplingRate = get(o,'SamplingRate');
BaseSound=strtrim(get(o,'BaseSound'));
Subsets = get(o,'Subsets');
SetSizeMult = get(o,'SetSizeMult');
CoherentFrac = get(o,'CoherentFrac');
SingleBandFrac = get(o,'SingleBandFrac');
ShuffleOnset=get(o,'ShuffleOnset');  % if 1, randomly rotate stimulus waveforms in time
RepIdx=get(o,'RepIdx');
Duration=get(o,'Duration');  % if 1, randomly rotate stimulus waveforms in time

%error checking
if o.ModDepth < 0 || o.ModDepth > 1
    error('ModDepth must be between 0 and 1')
end

%Update syllable-speed envelope parameters
o.Trep=1/o.RepRate;
o.Ton=o.OnPerc/o.RepRate;
    
    
object_spec = what(class(o));
envpath = [object_spec(1).path filesep 'envelope' filesep];

if strcmp(BaseSound,'blnoise')
    %Envelopes generated on the fly
    Env.MaxIndex=1000;
elseif Subsets==1 && ~strcmp(BaseSound,'Synthetic')
    % eg "Speech.subset1.mat"   and "Speech1"
    str='';
    %str='_H_LPfilt100HzN6_2x_shift';
    str='_H_LPfilt50HzN6_filtfilt2x';
    EnvFileName=[envpath BaseSound '.subset',num2str(Subsets),str,'.mat'];
    EnvVarName=[BaseSound,num2str(Subsets)];
    load(EnvFileName);
elseif isequal(Subsets,'6old') || Subsets==6  || strcmp(BaseSound,'Synthetic')
    EnvFileName=[envpath BaseSound '.subset',num2str(Subsets),'.mat'];
    EnvVarName=[BaseSound,num2str(Subsets)];
    Env=load(EnvFileName,'emtx','MaxIndex','fs','Names');
    if isequal(Subsets,'6old')
        Subsets=6;
    end
else
    EnvFileName=[envpath BaseSound '.subset',num2str(Subsets),'.mat'];
    EnvVarName=[BaseSound,num2str(Subsets)];
    load(EnvFileName); 
end

saveseed=rng;%saveseed=rand('seed');
rng('default');
rng(Subsets*20);%rand('seed',Subsets*20);
bandcount=max([length(o.FlankFcs),length(o.RelAttenuatedB)])+1;
idxset2=[];
if o.Sub_Subset==0
    if bandcount>1,
        MaxIndex=round(Env.MaxIndex.*SetSizeMult);
        idxset=repmat((1:Env.MaxIndex)',[ceil(SetSizeMult) bandcount]);
        ShuffledOnsetTimes=zeros(MaxIndex,bandcount);
        coherentcount=round(MaxIndex.*CoherentFrac);
        incoherentset=1:(size(idxset,1)-coherentcount);
        MaskerShiftSet=zeros(MaxIndex,1);
        for jj=2:bandcount,
            kk=0;
            % slight kludge, keep shuffling until there are no matches between
            % index values in columns 1 and column jj, except where intended.
            while sum(idxset(incoherentset,1)==idxset(incoherentset,jj))>0 && kk<20,
                ff=find(idxset(incoherentset,1)==idxset(incoherentset,jj));
                if length(ff)==1;
                    ff=union(ff,[1;2]);
                end
                idxset(ff,jj)=shuffle(idxset(ff,jj));
                kk=kk+1;
                if kk==20
                    error('Couldn''t shuffle enough to make band %d incoherent',jj)
                end
            end
        end
        %Make bands 2 thought bandcount incoherent with eachother as well
        if bandcount>2
            run_cross_compare_and_shuffle=1;
            run_times=0;
            while run_cross_compare_and_shuffle
                run_times=run_times+1;
                if run_times>20
                    error('Couldn''t shuffle enough to make all bands incoherent')
                end
                run_cross_compare_and_shuffle=0;
                for jj=2:bandcount
                    kk=0;
                    % slight kludge, keep shuffling until there are no matches between
                    % index values in columns 1 and column jj, except where intended.
                    ff=find(any(idxset(incoherentset,setdiff(1:bandcount,jj))==idxset(incoherentset,jj),2));
                    while ~isempty(ff) && kk<20
                        if length(ff)==1
                            ff=union(ff,[1;2]);
                        end
                        idxset(ff,jj)=shuffle(idxset(ff,jj));
                        run_cross_compare_and_shuffle=1;
                        kk=kk+1;
                        ff=find(any(idxset(incoherentset,setdiff(1:bandcount,jj))==idxset(incoherentset,jj),2));
                    end
                end
            end
        end
        MaxIncoherent=MaxIndex-coherentcount;
        idxset=idxset([1:MaxIncoherent (end-coherentcount+1):end],:);
        
        % shift signals in the different bands randomly to avoid correlations
        % from average cadence of sentences
        if ShuffleOnset,
            if ShuffleOnset==1,
                d=Duration;
            elseif ShuffleOnset==2,
                % don't make it depend on the stimulus length!!!!
                % fixes bug in MultiRefTar that changes reference Duration
                d=3;
            end
            ShuffledOnsetTimes=floor(rand(size(ShuffledOnsetTimes))*d*2)./2;
            ShuffledOnsetTimes(MaxIncoherent+1:end,2:end)=...
                repmat(ShuffledOnsetTimes(MaxIncoherent+1:end,1),[1 bandcount-1]);
        end
        
        if bandcount==3
            idxset=[
                1 2 1;
                2 3 2;
                3 4 3;
                4 5 4;
                5 1 5;
                2 1 1;
                3 2 2;
                4 3 3;
                5 4 4;
                1 5 5;
                ];
            MaxIndex=10;
            ShuffledOnsetTimes=zeros(size(idxset));
        end
        
        % set some ids to zero so that only one band has sound
        if SingleBandFrac>0,
            SingleBandCount=round(MaxIndex.*SingleBandFrac./bandcount);
            SingleBandIdx=repmat(1:SingleBandCount,[bandcount+1 1]);
            LeftoverIdx=SingleBandCount+(1:(MaxIndex-length(SingleBandIdx(:))));
            idxset=idxset([SingleBandIdx(:);LeftoverIdx(:)],:);
            ShuffledOnsetTimes=ShuffledOnsetTimes([SingleBandIdx(:);LeftoverIdx(:)],:);
            for kk=1:SingleBandCount,
                for jj=1:bandcount,
                    idxset((kk-1)*(bandcount+1)+jj+1,[1:(jj-1) (jj+1):end])=0;
                end
            end
        end
        if o.MaskerShiftFrac>0
            NCohShift=round(coherentcount*o.MaskerShiftFrac);
            NIncShift=round((MaxIndex-coherentcount)*o.MaskerShiftFrac);
            MaskerShiftSet(incoherentset(1:NIncShift))=o.MaskerShift;
            coherentset=setdiff(1:MaxIndex,incoherentset);
            MaskerShiftSet(coherentset(1:NCohShift))=o.MaskerShift;
        end
        dbScale=[zeros(size(idxset,1),1),o.RelAttenuatedB(1)*ones(size(idxset,1),bandcount-1)];
    else
        MaxIndex=Env.MaxIndex;
        idxset=(1:MaxIndex)';
        ShuffledOnsetTimes=zeros(size(idxset));
        dbScale=zeros(size(idxset));
    end
elseif o.Sub_Subset==1
    class_len=20;
    idxset=[(1:class_len)' zeros(class_len,1);
        zeros(class_len,1) (1:class_len)';
        ];
    dbScale=[zeros(class_len,2);
        zeros(class_len,2);
        ];
    repinds=0:class_len:(class_len*3);
    for i=1:length(o.RelAttenuatedB)
        dbScale=[dbScale;
            zeros(class_len,1) -1*o.RelAttenuatedB(i)*ones(class_len,1);
            zeros(class_len,1) -1*o.RelAttenuatedB(i)*ones(class_len,1);
            ];
        if(mod(i,2)==1)
            ssi=[5 15];%second start inds
        else
            ssi=[10 10];
        end
        if o.IncSwitchTime==0
            idxset=[idxset;
                ([1:4 ssi(1)+(0:15)])' ([1:4 ssi(1)+(0:15)])';
                ([1:4 ssi(2)+(0:15)])' ([2 1 4 3 ssi(2)+([1:15 0])])';
                ];
        else
            idxset2=[idxset;
                ([1 2 4 3 ssi(1)+(0:7) ssi(1)+([9:15 8])])' ([2 1 3 4 ssi(1)+([1:7 0]) ssi(1)+(8:15)])';
                ([1 2 4 3 ssi(1)+(0:7) ssi(1)+([9:15 8])])' ([2 1 3 4 ssi(1)+([1:7 0]) ssi(1)+(8:15)])';
                ];
            idxset=[idxset;
                ([1 2 3 4 ssi(1)+(0:7) ssi(1)+(8:15)])' ([1 2 3 4 ssi(1)+(0:7) ssi(1)+(8:15)])';
                ([1 2 4 3 ssi(1)+(0:7) ssi(1)+([9:15 8])])' ([2 1 3 4 ssi(1)+([1:7 0]) ssi(1)+(8:15)])';
                ];
                
        end
    end
    ShuffledOnsetTimes=zeros(size(idxset));
elseif o.Sub_Subset==2
     idxset=[
         0 1;
         1 0;
         1 1;
         1 2;
         2 1];
    ShuffledOnsetTimes=zeros(size(idxset));
    dbScale=zeros(size(idxset));
else
    error('Sub_Subset %d not defined',o.Sub_Subset)
end

rng(saveseed);%rand('seed',saveseed);
if ischar(o.CarrierObj) && strcmp(o.CarrierObj,'blnoise')
else
    params=fieldnames(o);
    for i=1:bandcount
        c=ComplexTone;
        u=get(c,'UserDefinableFields');
        u=u(1:3:end);
        u=setdiff(u,{'AnchorFrequency'});
        for ii=1:length(u),
            if any(strcmp(params,u{ii}))
                if length(o.(u{ii})) == 1
                    ind=1;
                else
                    ind=i;
                end
                c=set(c,u{ii},o.(u{ii})(ind));
            end
        end
        c=set(c,'AnchorFrequency',o.F0s(i));
        c=set(c,'ComplexToneDur',o.Duration);
        c=set(c,'SamplingRate',o.SamplingRate);
        c=set(c,'PoolSize',1);
        c=set(c,'AMFrequency',0);
        if(i==1)
            % c=set(c,'ComponentAmplitudes',[1 1 1 0 1 1 0 1 0 1 0 1]);
            %c=set(c,'ComponentAmplitudes',[1 1 1 0 1 0 1 0 1 0 1 1]);
            %                c=set(c,'ComponentAmplitudes',[1 1 1 1 1 0 1 0 1 0 1]);
            %                if o.ComponentsNumber(i) ~=length(get(c,'ComponentAmplitudes'))
            %                    warning('Could not set component amplitudes as desired.')
            %                end
        elseif i==2
            % c=set(c,'ComponentAmplitudes',[1 0 0 0]);
        end
        c = ObjUpdate (c);
        o.CarrierObj{i}=c;
        
        if i==2 && 0
            c=Noise;
            c.Count=1;
            bw=1/2;
            c.LowFreq=o.F0s(i)*2^(-bw/2);
            c.HighFreq=o.F0s(i)*2^(bw/2);
            c=set(c,'Duration',o.Duration);
            c=set(c,'SamplingRate',o.SamplingRate);
            u=get(c,'UserDefinableFields');
            u=u(1:3:end);
            for ii=1:length(u),
                if any(strcmp(params,u{ii}))
                    if length(o.(u{ii})) == 1
                        ind=1;
                    else
                        ind=i;
                    end
                    c=set(c,u{ii},o.(u{ii})(ind));
                end
            end
            c=set(c,'PreStimSilence',0);
            c=set(c,'PostStimSilence',0);
            c = ObjUpdate (c);
            o.CarrierObj{i}=c;
        end
    end
end
% if length(RepIdx)>=2 && RepIdx(1)>0 && RepIdx(2)>1,
%     RepSet=1:RepIdx(1);
%     RepCount=RepIdx(2);
%     idxset=cat(1,repmat(idxset(RepSet,:),[RepCount 1]),...
%                idxset((RepIdx(1)+1):end,:));
%     ShuffledOnsetTimes=cat(1,repmat(ShuffledOnsetTimes(RepSet,:),[RepCount 1]),...
%                ShuffledOnsetTimes((RepIdx(1)+1):end,:));
%     MaxIndex=size(idxset,1);
% end
if length(RepIdx)>=2 && RepIdx(1)>0 && RepIdx(2)>1,
    RepSet=repmat(1:RepIdx(1),length(repinds),1)+repmat(repinds',1,RepIdx(1));
    RepSet=RepSet(:)';
    %RepSet=[1:RepIdx(1) (1:RepIdx(1))+10 (1:RepIdx(1))+20 (1:RepIdx(1))+40];
    RepCount=RepIdx(2)-1;
    idrep=repmat(idxset(RepSet,:),[RepCount 1]);
    if ~isempty(idxset2)
        idrep2=repmat(idxset2(RepSet,:),[RepCount 1]);
    end
    dbrep=repmat(dbScale(RepSet,:),[RepCount 1]);
    SOTrep=repmat(ShuffledOnsetTimes(RepSet,:),[RepCount 1]);
end
if length(o.RemoveIdx)>=0 && o.RemoveIdx>0
    rmi=o.RemoveIdx:class_len;
    Ntypes=size(idxset,1)/class_len;
    rmi=repmat(rmi',1,Ntypes)+repmat((0:class_len:(Ntypes-1)*class_len),length(rmi),1);
    rmi=rmi(:)';
    idxset(rmi,:)=[];
    if ~isempty(idxset2)
        idxset2(rmi,:)=[];
    end
    dbScale(rmi,:)=[];
    ShuffledOnsetTimes(rmi,:)=[];
end
if length(RepIdx)>=2 && RepIdx(1)>0 && RepIdx(2)>1,
    idxset=[idxset; idrep];
    if ~isempty(idxset2)
        idxset2=[idxset2; idrep2];
    end
    dbScale=[dbScale; dbrep];
    ShuffledOnsetTimes=[ShuffledOnsetTimes; SOTrep];
end

% if o.AddSquares
%     idxset_synth=[0 Env.MaxIndex+1;
%          Env.MaxIndex+1 0;
%          Env.MaxIndex+1 Env.MaxIndex+1;
%          Env.MaxIndex+1 Env.MaxIndex+2;
%          Env.MaxIndex+2 Env.MaxIndex+1];
%      idxset=[idxset; repmat(idxset_synth,o.AddSquares,1)];
%      if ~isempty(idxset2)
%          idxset2=[idxset2; repmat(idxset_synth,o.AddSquares,1)];
%      end
%      ShuffledOnsetTimes(end+1:end+5*o.AddSquares,:)=0;
%     dbScale(end+1:end+5*o.AddSquares,:)=0;
% end

MaxIndex=size(idxset,1);

if ~strcmp(BaseSound,'blnoise')
    o=set(o,'SamplingRateEnv',Env.fs);
    SPNOISE_EMTX.(EnvVarName)=Env.emtx;
    resampled_name=[EnvVarName,'_resampled_',num2str(o.SamplingRate)];
    if ~isfield(SPNOISE_EMTX,resampled_name)
        fprintf('**resampling/n')
        SPNOISE_EMTX.(resampled_name)=resample(Env.emtx,o.SamplingRate,o.SamplingRateEnv);
    end
    
    if o.AddSquares && size(SPNOISE_EMTX.(resampled_name),2)==Env.MaxIndex
        EnvSynth=zeros(o.SamplingRate*o.Duration,2);
        EnvSynth(1:o.SamplingRate*2,1)=1;
        EnvSynth(o.SamplingRate*1:o.SamplingRate*3,2)=1;
        sigma=32/1000/(2*sqrt(2*log(2)))*o.SamplingRate;
        EnvSynth=gsmooth(EnvSynth,sigma);
        EnvSynth=EnvSynth/max(EnvSynth(:))*rms(Env.emtx(:));
        SPNOISE_EMTX.(resampled_name)(:,end+1:end+2)=EnvSynth;
        if size(SPNOISE_EMTX.(resampled_name),2)~=Env.MaxIndex+2
            error('Error adding square envelopes')
        end
    elseif ~o.AddSquares && size(SPNOISE_EMTX.(resampled_name),2)>Env.MaxIndex
        SPNOISE_EMTX.(resampled_name)(:,end-1:end)=[];
        if size(SPNOISE_EMTX.(resampled_name),2)~=Env.MaxIndex
            error('Error removing square envelopes')
        end
    end
    o.mean=nanmean(Env.emtx(:));
end


%RMS is 0.2984 (.2931 on Alpaca??). m=1 envelope (0 to 1 sine wave) RMS is
%.6124. 20*log10(0.6124/0.2984)=6.2443 20*log10(0.6124/0.2931)=6.4004
%o=set(o,'emtx',Env.emtx);
o=set(o,'idxset',idxset);
o=set(o,'idxset2',idxset2);
o=set(o,'MaskerShiftSet',MaskerShiftSet);
o=set(o,'dbScale',dbScale);
o=set(o,'ShuffledOnsetTimes',ShuffledOnsetTimes);
o=set(o,'MaxIndex',MaxIndex);
% removed phoneme labels to save space in parmfiles
%o=set(o,'Phonemes',Env.Phonemes);
% if o.AddSquares
%     Env.Names(end+1:end+2)={'Square_0_2','Square_1_3'};
% end

Names=cell(1,MaxIndex);
for ii=1:MaxIndex,
    Names{ii}=['T'];
    for bb=1:bandcount,
        if idxset(ii,bb)
            if strcmp(BaseSound,'blnoise')
                Names{ii}=[Names{ii} '+' sprintf('%.3d',idxset(ii,bb))];
            else
                Names{ii}=[Names{ii} '+' Env.Names{idxset(ii,bb)}];
            end
            if ~isempty(idxset2) && idxset(ii,bb)~=idxset2(ii,bb)
                if strcmp(BaseSound,'blnoise')
                    Names{ii}=[Names{ii} 'to' sprintf('%.3d',idxset2(ii,bb))];
                else
                    Names{ii}=[Names{ii} 'to' Env.Names{idxset2(ii,bb)}];
                end
            end
        else
            Names{ii}=[Names{ii} '+null'];
        end
    end
end

o = set(o,'Names',Names);
