function params=read_tags(o,tags,sep_spaces)
if nargin==2
    sep_spaces=1;
end
if ~sep_spaces
    tagr=tags;
end
params.Sp=nan(length(tags),2);
for i=1:length(tags);
    if sep_spaces
        t=strsep(tags{i});
        tagr{i}=t{3};
    end
    t=strsep(tagr{i},'+');
    tagr_{i}=t;
    if length(tagr_{i})==2
        if strcmp(tagr_{i}{1},'BNB')
            t=strsep(tagr_{i}{2},':');
            params.Sp(i,1)=str2double(t{1}(3:end));
          %  params.Lev(i,1)=t{2};
        else
            t=strsep(tagr_{i}{3},':');
            params.Sp(i,2)=str2double(t{1}(3:end));
           % params.Lev(i,2)=t{2};
        end
    else
        switch tagr_{i}{2}(1:2)
            case 'si'
                t=strsep(tagr_{i}{2},':');
                params.Sp(i,1)=str2double(t{1}(3:end));
                %params.Lev(i,1)=t{2};
                t=strsep(tagr_{i}{3},':');
                params.Sp(i,2)=str2double(t{1}(3:end));
                %params.Lev(i,2)=t{2};
            case 'TS'
                t=strsep(tagr_{i}{2},'_');
                params.Sp(i,1)=str2double(t{1}(3:end))*1000 + t{2};
                if t{2}>1000
                    error('Fix me')
                end
                t=strsep(tagr_{i}{3},'_');
                params.Sp(i,2)=str2double(t{1}(3:end))*1000 + t{2};
                if t{2}>1000
                    error('Fix me')
                end
            otherwise 
                error('Unreadable tag %s',tagr_{i})
        end
    end
end
params.runidx=3*ones(length(tags),1);
params.runidx(isnan(params.Sp(:,1)) | isnan(params.Sp(:,2)))=1;
params.runidx(params.Sp(:,1) == params.Sp(:,2))=2;
% 1 - single stream
% 2 - coherent
% 3 - incoherent