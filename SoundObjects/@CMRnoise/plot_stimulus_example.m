function plot_stimulus(RefObject,TarObject,ThisTarParams,TrialSound,TrialSamplingRate,RefTrialIndex,TrialIndex,TarIdx,events,TrialEnv,ax)

switch ThisTarParams.TargetMode
    case {'Envelope','Carrier'}
        
  if 0
      % plot spectrogram
      curr_sr=get(RefObject,'SamplingRate');
        Tenv_dur=size(TrialEnv,1)/curr_sr;
        sr=get(RefObject,'SamplingRateEnv');
        TrialEnv=resample(TrialEnv,sr,curr_sr);
        t=(0:(size(TrialEnv,1)-1))/sr-get(RefObject,'PreStimSilence');
        bandcount=size(TrialEnv,2);
        co=get(gca,'ColorOrder');
        ls={'-','--',':'};
        NF=50;
        F=logspace(log10(.1),log10(1),NF);
        stim=zeros(length(t),length(F),bandcount);
        for bb=1:bandcount,
            freqs= get(RefObject.CarrierObj{bb},'AnchorFrequency')*cumprod([1 ; get(RefObject.CarrierObj{bb},'ComponentRatios')]);
            amps=get(RefObject.CarrierObj{bb},'ComponentAmplitudes');
            for j=1:length(freqs)
                [~,ind]=min(abs(F-freqs(j)/1000));
                stim(:,ind,bb)=1;
                %plot(freqs([j j]),[0 amps(j)],'Color',co(bb,:),'LineWidth',2,'Linestyle',ls{bb})
            end
            stim(:,:,bb)=stim(:,:,bb).*repmat(TrialEnv(:,bb),1,NF);
        end
        im(:,:,1)=1-stim(:,:,1)'/max(max(stim(:,:,1)));
        im(:,:,3)=1-stim(:,:,2)'/max(max(stim(:,:,2)));
        im(:,:,2)=0.5*im(:,:,1)+0.5*im(:,:,3);
        saturation_multiplier=1.5;
        HSV = rgb2hsv(im);
        % "20% more" saturation:
        HSV(:, :, 2) = HSV(:, :, 2) * saturation_multiplier;
        % or add:
        % HSV(:, :, 2) = HSV(:, :, 2) + 0.2;
        HSV(HSV > 1) = 1;  % Limit values
        im = hsv2rgb(HSV);
        hold(ax,'off')
        imagesc(t,1:NF,im,'Parent',ax);
        set(ax,'YDir','normal')
        set(ax,'XLim',t([1 end]))
        
        ylabels=[.2 .4 .8 2 4 8 20];
        ylabels=[.25 .5 1 2 4 8 16];
        ylabels=[.1 .2 .5 1];
        p=polyfit(log10(F),1:NF,1);
        yp=polyval(p,log10(ylabels));
        ylim=[.1 1];
        yp_lim=polyval(p,log10(ylim));
        set(ax,'Ytick',yp,'YTickLabel',ylabels,'YLim',yp_lim);
        set(ax,'Box','off')
        %xlabel('Time (s)','FontSize',26)
        %ylabel('Frequency (kHz)','FontSize',26)
        %set(ax,'FontSize',16)
        pos1=get(ax,'Position');pos1o=pos1;
        pos1(4)=pos1(4)*.7;set(ax(1),'Position',pos1)
        pos2=pos1;pos2(2)=pos1(2)+pos1(4);pos2(4)=(pos1o(2)+pos1o(4))-pos2(2);
        ax(2)=axes('Position',pos2);
  else
      ax(2)=ax;
  end
        %envs=load('/auto/users/luke/baphy/SoundObjects/@SpNoise/envelope/Speech.subset1.mat');
        %set(ax(2),'ColorOrder',[0 .5 1; 1 .5 0]);
       sr=get(RefObject,'SamplingRate');
        t=(0:(size(TrialEnv,1)-1))/sr-get(RefObject,'PreStimSilence');
        plot(t,TrialEnv(:,1),'Color',[0 .5 1],'Parent',ax(2),'LineWidth',1);
        hold(ax(2),'on')
        plot(t,TrialEnv(:,2),'Color',[1 .5 0],'LineStyle','--','Parent',ax(2),'LineWidth',1);
        set(ax(2),'Visible','off')
        set(ax(2),'YLim',[0 1]);
        %linkaxes(ax(1:2),'x')
    case 'Waveform'
        
        subplot(2,1,1);
        sr=get(RefObject,'SamplingRateEnv');
        RefObject2=set(RefObject,'SamplingRate',sr);
        RefObject2=set(RefObject2,'PreStimSilence',0);
        RefObject2=set(RefObject2,'PostStimSilence',0);
        [RefEnv,~]=env(RefObject2, RefTrialIndex,TrialIndex);
        lf=get(RefObject,'LowFreq');
        hf=get(RefObject,'HighFreq');
        freq=20:40000;
        dur=length(TrialSound)/TrialSamplingRate;
        dat=zeros(dur*sr,length(freq));
        ref_ind=arrayfun(@(x)~isempty(strfind(x.Note,'Reference')),events) & arrayfun(@(x)~isempty(strfind(x.Note,'Stim ,')),events);
        hold on;
        for i=1:size(RefEnv,2)
            imagesc(events(ref_ind).StartTime:.01:events(ref_ind).StopTime,[lf(i):hf(i)],repmat(RefEnv(:,i)',hf(i)-lf(i)+1,1));
        end
        TarObject2=set(TarObject,'SamplingRate',sr);
        TarObject2=set(TarObject2,'PreStimSilence',0);
        TarObject2=set(TarObject2,'PostStimSilence',0);
        [TarEnv,~] = env(TarObject2, TarIdx, 0);
        TarEnv=TarEnv./max(TarEnv);
        tar_ind=arrayfun(@(x)~isempty(strfind(x.Note,'Target')),events) & arrayfun(@(x)~isempty(strfind(x.Note,'Stim ,')),events);
        tar_lf=get(TarObject,'LoBounds');
        tar_hf=get(TarObject,'HiBounds');
        imagesc(events(tar_ind).StartTime:.01:events(tar_ind).StopTime,[tar_lf:tar_hf],repmat(TarEnv',hf(i)-lf(i)+1,1));
        
        colormap('gray')
        set(gca,'YScale','log')
        
        set(gca,'YLim',[20 20000])
        UTset_cf_labels(gca,1000,1,'ytick');
        set(gca,'XLim',[0 events(end).StopTime]);
        
        if(1)
            win=1.5;
            set(gca,'YLim',[min(lf)/win max(hf)*win])
            UTset_cf_labels(gca,1000,2,'ytick');
        end
        
end
end