function o = CMRnoise(varargin)
% CMRnoise is based on SpNoise object, but the carrier is tonal (single,
% harmonic, or inharmonic)
%
% properties:
%   PreStimSilcence
%   PostStimSilence
%   SamplingRate
%   Loudness
%   Subsets: can be 1, 2, 3 or 4. each contains 30 different sentences from
%       Timit database. each subset is 30 sentences spoken by 30 different
%       speakers (15 male 15 female). sentences in subset 1 and 4 are three
%       seconds long, subset 2 and 3 are four seconds. Subset 4 is all the
%       same sentence: "She had your dark suit in greasy wash water all
%       year"
%   Phonemes: contains the phoneme events for the specified names.
%   Words: contains the word events for the specified names.
%   Sentences: contains the sentece events for the specified names.
% 
% methods: waveform, LabelAxis, set, get
% 

% Nima Mesgarani, Oct 2005

switch nargin
case 0
    % if no input arguments, create a default object
    s = SoundObject ('CMRnoise', 100000, 0, 0, 0, {}, 1, ...
                     {'MaskFc','edit',1000,...
                      'FlankFcs','edit',[250 500 2000 4000],...
                      'MaskBW','edit',20,...
                      'RelAttenuatedB','edit',0,...
                      'BaseSound','popupmenu','blnoise|Speech|FerretVocal|Synthetic',...
                      'Subsets','edit',1,...
                      'Sub_Subset','edit',0,...
                      'MaskerShift','edit',0,...
                      'MaskerShiftFrac','edit',0,...
                      'MaskStart','edit',[0 0],...
                      'CoherentFrac','edit',0.1,...
                      'SingleBandFrac','edit',0,...
                      'BaselineFrac','edit',0,...
                      'ModDepth','edit',1,...
                      'RepRate','edit',2.5,...
                      'ForceTargetEnvelope','popupmenu','No|Coherent|Incoherent',...
                      'RepIdx','edit',[0 1],...
                      'RemoveIdx','edit',0,...
                      'Duration','edit',10,...
                      'RefRepCount','edit',5});
    o.MaskFc = 1000;
    o.FlankFcs=[250 500 2000 4000];
    o.MaskBW=20;
    o.RelAttenuatedB=0;
    o.BaseSound = 'blnoise';
    o.Subsets = 1;
    o.Sub_Subset = 0;
    o.AddSquares = 0;
    o.SNR = 1000;
    o.ShuffleOnset=0;  % if 1, randomly rotate stimulus waveforms in time
    o.SetSizeMult=2;  % for multi-channel stim, how many times larger should MaxIndex be than the original set
    o.MaskStart=[0 0];
    o.CoherentFrac=0.5;  % for multi-channel stim, what fraction should be the same in all channels
    o.SingleBandFrac=0;  % for multi-channel stim, what fraction should be the same in all channels
    o.BaselineFrac=0;  % minimum sound level (as a fraction of peak)
    %o.Phonemes = {struct('Note','','StartTime',0,'StopTime',0)};
    o.ModDepth=1; % peak-to-peak of envelope. (center of range is set to .5).
    % Generally, use this with BaselineFrac=0 and use OveralldB to set level.    %o.Phonemes = {struct('Note','','StartTime',0,'StopTime',0)};
    %o.Words= {struct('Note','','StartTime',0,'StopTime',0)};    
    %o.Sentences = {''};
    o.RepRate=2.5;%Hz
    o.Trep=1/o.RepRate;
    o.OnPerc=.75;
    o.Ton=o.OnPerc/o.Trep;
    o.Tramp=.02;
    o.MaskerShift=0;
    o.ForceTargetEnvelope='No';
    o.emtx=[];
    o.idxset=[];
    o.idxset2=[];
    o.MaskerShiftSet=[];
    o.MaskerShiftFrac=0;
    o.ShuffledOnsetTimes=[];
    o.SamplingRateEnv=2000;
    o.RemoveIdx=0;
    o.Duration = 3;
    o.SamplingRate=100000;
    o.RepIdx=[0 1];
    o.mean=[];
    o.dbScale=[];
    o.CarrierObj='blnoise';
    o.FreezeNoise=1;
    o.RefRepCount=1;
    %
    o = class(o,'CMRnoise',s);
    o = ObjUpdate(o);
    
case 1
    % if single argument of class SoundObject, return it
    if isa(varargin{1},'CMRnoise')
        o = varargin{1};
    else
        error('Wrong argument type');
    end
    
case 7
    error('SpeechPhoneme format not supported for this object');
    
otherwise
    error('Wrong number of input arguments');
end