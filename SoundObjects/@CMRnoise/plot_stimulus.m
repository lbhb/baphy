function plot_stimulus(RefObject,TarObject,ThisTarParams,TrialSound,TrialSamplingRate,RefTrialIndex,TrialIndex,TarIdx,events,TrialEnv)
try 
    ThisTarParams.TargetMode;
catch
    ThisTarParams.TargetMode='Envelope';
    ThisTarParams.RelativeTarRefdB=0;
end
if TrialIndex==1
    fh=sfigure(99);
    set(fh,'Position',[12        1824        1904         248])
end
        curr_sr=get(RefObject,'SamplingRate');
        Tenv_dur=size(TrialEnv,1)/curr_sr;
        sr=get(RefObject,'SamplingRateEnv');
        sr=6000;
        TrialEnv=resample(TrialEnv,sr,curr_sr);
        
        bandcount=size(TrialEnv,2);
        subplot(2,1,1);
        ax(1)=gca;
        cla
        hold on;
        co=get(gca,'ColorOrder');
        ls={'-','--',':'};
        t=(0:(size(TrialEnv,1)-1))/sr;
        FlankFcs=get(RefObject,'FlankFcs');
        MaskFc=get(RefObject,'MaskFc');
        Fcs=unique([FlankFcs,MaskFc]);
        TarFc=get(TarObject,'Frequencies');
        try
            TarFc=TarFc(TarIdx(1));
        catch
            TarFc=TarFc(1);
        end
        for bb=bandcount:-1:1
            if ischar(RefObject.CarrierObj) && strcmp(RefObject.CarrierObj,'blnoise')
                if bb==1
                    offset=log2(MaskFc/TarFc);
                    color=co(1,:);
                    ls='--';
                else
                    offset=log2(FlankFcs(bb-1)/TarFc);
                    color=co(2,:);
                    ls='-';
                end
                sf=0.5;
                plot(t,TrialEnv(:,bb,1).*TrialEnv(:,bb,2)*sf+offset,'Color',color,'LineStyle',ls);
            end
        end

        tar_evis=find(cellfun(@(x)contains(x,', Target') && contains(x,'Stim ,'),{events.Note}));
        for  tar_evi=tar_evis
            tar_time=[events(tar_evi).StartTime events(tar_evi).StopTime];
            tar_env=zeros(size(t));
            tar_env(t>tar_time(1)&t<tar_time(2))=1;
            sf=0.5;
            plot(t,tar_env*sf,'Color',co(3,:));
        end
        catch_evis=find(cellfun(@(x)contains(x,', Catch') && contains(x,'Stim ,'),{events.Note}));
        for  catch_evi=catch_evis
            tar_time=[events(catch_evi).StartTime events(catch_evi).StopTime];
            tar_env=zeros(size(t));
            tar_env(t>tar_time(1)&t<tar_time(2))=1;
            sf=0.5;
            plot(t,tar_env*sf,'--','Color',co(4,:));
        end
        
        ytls=unique([Fcs,0]);
        yts=log2(ytls./TarFc); 
        set(gca,'YTick',yts,'YTickLabel',ytls);
        ylabel('Frequency (Hz)')
        hold off;
                
        subplot(2,1,2);
        ax(2)=gca;
        
%         plot(t,TrialEnv);
%         set(gca,'XLim',[0 max([events.StopTime])],'YLim',[0 max(1.01,max(10.^(ThisTarParams.RelativeTarRefdB/20)))],'XTickLabels','');
%         
%         subplot(3,1,3);
%         ax(3)=gca;
%     case 'Waveform'
%         
%         subplot(2,1,1);
%         sr=get(RefObject,'SamplingRateEnv');
%         RefObject2=set(RefObject,'SamplingRate',sr);
%         RefObject2=set(RefObject2,'PreStimSilence',0);
%         RefObject2=set(RefObject2,'PostStimSilence',0);
%         [RefEnv,~]=env(RefObject2, RefTrialIndex,TrialIndex);
%         lf=get(RefObject,'LowFreq');
%         hf=get(RefObject,'HighFreq');
%         freq=20:40000;
%         dur=length(TrialSound)/TrialSamplingRate;
%         dat=zeros(dur*sr,length(freq));
%         ref_ind=arrayfun(@(x)~isempty(strfind(x.Note,'Reference')),events) & arrayfun(@(x)~isempty(strfind(x.Note,'Stim ,')),events);
%         hold on;
%         for i=1:size(RefEnv,2)
%             imagesc(events(ref_ind).StartTime:.01:events(ref_ind).StopTime,[lf(i):hf(i)],repmat(RefEnv(:,i)',hf(i)-lf(i)+1,1));
%         end
%         TarObject2=set(TarObject,'SamplingRate',sr);
%         TarObject2=set(TarObject2,'PreStimSilence',0);
%         TarObject2=set(TarObject2,'PostStimSilence',0);
%         [TarEnv,~] = env(TarObject2, TarIdx, 0);
%         TarEnv=TarEnv./max(TarEnv);
%         tar_ind=arrayfun(@(x)~isempty(strfind(x.Note,'Target')),events) & arrayfun(@(x)~isempty(strfind(x.Note,'Stim ,')),events);
%         tar_lf=get(TarObject,'LoBounds');
%         tar_hf=get(TarObject,'HiBounds');
%         imagesc(events(tar_ind).StartTime:.01:events(tar_ind).StopTime,[tar_lf:tar_hf],repmat(TarEnv',hf(i)-lf(i)+1,1));
%         
%         colormap('gray')
%         set(gca,'YScale','log')
%         
%         set(gca,'YLim',[20 20000])
%         UTset_cf_labels(gca,1000,1,'ytick');
%         set(gca,'XLim',[0 events(end).StopTime]);
%         
%         if(1)
%             win=1.5;
%             set(gca,'YLim',[min(lf)/win max(hf)*win])
%             UTset_cf_labels(gca,1000,2,'ytick');
%         end
%         subplot(2,1,2);
%         ax(2)=gca;      
% end

Nchannels=1;
dfs=50000;
tw=resample(TrialSound(:,1:Nchannels),dfs,TrialSamplingRate);
if Nchannels==1
    plot((1:length(tw))./dfs,tw(:,Nchannels),'k','Parent',ax(end));
elseif Nchannels==2
    plot((1:length(tw))./dfs,tw(:,1),'Color',co(1,:),'Parent',ax(end));
    hold(ax(end),'on');
    plot((1:length(tw))./dfs,tw(:,2),'Color',co(2,:),'Parent',ax(end));
end
set(ax(end),'XLim',[0 max([events.StopTime])]);
set(ax(end),'YLim',[min(min(tw(:,1:Nchannels))) max(max(tw(:,1:Nchannels)))])
ylabel(ax(end),'Signal to DAC (V)');xlabel(ax(end),'Time (sec)')
linkaxes(ax,'x')
switch ThisTarParams.TargetMode
    case {'Envelope','Carrier'}
        p2=get(ax(2),'Position');
        p3=get(ax(3),'Position');
        edge_diff=(p2(2)-(p3(2)+p3(4)))*.9;
        p2n=p2;p2n(2)=p2n(2)-edge_diff/2;p2n(4)=p2n(4)+edge_diff/2;
        p3n=p3;p3n(4)=p3n(4)+edge_diff/2;
        %p2n(2)=p2n(2)+.1;p3n(2)=pn(2)+.1;
        set(ax(2),'Position',p2n);
        set(ax(3),'Position',p3n);
end
end