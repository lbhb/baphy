function [AMNBnoise,X0]=gen_vocoded_noise(fs,T,Fc,E,make_lnn)


N=round(T*fs);
filter_in_time=0;    

% bpFilt = designfilt('bandpassiir', 'DesignMethod','cheby1','FilterOrder', 4,...
%         'PassbandFrequency1',Fc(1),'PassbandFrequency2',Fc(2),...
%         ...%'StopbandAttenuation1',60,'StopbandAttenuation2',60,...
%         'SampleRate',fs);
if filter_in_time
    L = 2000;         % length of the FIR filter
    bpFilt = fir1(L , 2*[Fc(1),Fc(2)]/fs ,'bandpass');    % bandpass FIR filter.
    noise=randn(1,N);
    NBnoise2=filter(bpFilt,1,noise);
else
    freqbins  = (0:N/2)'/N*fs;
    
    % Zero out the frequency components outside the desired band
    iIn=round([Fc(1)/fs*N+1 Fc(2)/fs*N+1]);
    X = [1, exp(1i*2*pi*rand(1,N/2-1)), 1]; % X(1) and X(NFFT/2) must be real
    X(1:iIn(1)-1)=0;
    X(iIn(2)+1:end)=0;
    % Use the complex conjugate symmetry property of the FFT (for real signals) to
    % generate the other half of the frequency-domain signal
    X2         = [X, conj(fliplr(X(2:end-1)))];
    zero_inds=[1:iIn(1)-1,iIn(2)+1:N-iIn(2)+1,N-iIn(1)+3:N];
    nonzero_inds=iIn(1):iIn(2);
    % IFFT to convert to time-domain
    NBnoise0     = real(ifft(X2,N))';
end    

if make_lnn
    NBnoise_=NBnoise0;
    for i=1:10
        %%
        Enoise=abs(hilbert(NBnoise_));
        NBnoise_e=NBnoise_./Enoise;
        if filter_in_time
            NBnoise_f=filter(bpFilt,1,NBnoise_e);
        else
            X=fft(NBnoise_e,N);
            X(zero_inds)=0;
            NBnoise_f = real(ifft(X,N));
        end
        X=X(nonzero_inds);
        NBnoise_=NBnoise_f;
    end
    NBnoise=NBnoise_;
else
    NBnoise=NBnoise0;
end
    
AMNBnoise=NBnoise.*E;

X=fft(AMNBnoise,N);
X(zero_inds)=0;
X0=X(nonzero_inds);
AMNBnoise = real(ifft(X,N));
% bpFilt = designfilt('bandpassfir', 'DesignMethod','window','FilterOrder', 300,...
%         'CutoffFrequency1',Fc(1),'CutoffFrequency2',Fc(2),...
%         ...%'StopbandAttenuation1',60,'StopbandAttenuation2',60,...
%         'SampleRate',fs);
if 0
    %%
    t=(0:1/fs:(T-1/fs));
    D=[noise;NBnoise;AMNBnoise;E];
    D=D./repmat(max(D,[],2),1,size(D,2))+repmat([0 1 2 2]',1,size(D,2));
    figure;plot(t,D')
    legend({'noise','NBnoise','AM\_NBnoise','AM'},'FontSize',20)
    xlabel('Time(s)','FontSize',20)
    
    %%
    t=(0:1/fs:(T-1/fs));
    D=[NBnoise;Enoise;NBnoise_e;NBnoise_f];
    D=D./repmat(max(D,[],2),1,size(D,2))+repmat([0 0 2 4]',1,size(D,2));
    figure;plot(t,D')
    %legend({'noise','NBnoise','AM\_NBnoise','AM'},'FontSize',20)
    legend({'NBnoise','Enoise','NBnoise_LNN','NBnoise_LNN_f'},'FontSize',20)
    xlabel('Time(s)','FontSize',20)
end

if 0
    NBnoise_eq=NBnoise0;
    NBnoise_LNN_eq=NBnoise_;
    NBnoise_LNN_eq=NBnoise_LNN_eq./rms(NBnoise_LNN_eq).*rms(NBnoise_eq);
    mv=max([NBnoise_LNN_eq,NBnoise_eq]);
    NBnoise_LNN_eq=NBnoise_LNN_eq/mv;
    NBnoise_eq=NBnoise_eq/mv;
    
   %%
    t=(0:1/fs:(T-1/fs));
    D=[NBnoise_eq;NBnoise_LNN_eq;NBnoise_eq.*E;NBnoise_LNN_eq.*E;E.*max(NBnoise_eq);E.*max(NBnoise_LNN_eq)];
    D=D+repmat([0 2 4 6 4 6]',1,size(D,2));
    figure;ax=subplot1(1,2,'XTickL','All','YTickL','All');
    axes(ax(1));
    ph=plot(t,D');
    %legend({'noise','NBnoise','AM\_NBnoise','AM'},'FontSize',20)
    legend({'NBnoise','NBnoise\_LNN','NBnoise*AM','NBnoise\_LNN*AM','AM','AM'},'FontSize',20)
    xlabel('Time(s)','FontSize',20)
    axes(ax(2));
    PTplot_fft(abs(hilbert(NBnoise_eq)),fs,[],get(ph(1),'Color'))
    PTplot_fft(abs(hilbert(NBnoise_LNN_eq)),fs,[],get(ph(2),'Color'))
    PTplot_fft(abs(hilbert(NBnoise_eq.*E)),fs,[],get(ph(3),'Color'))
    PTplot_fft(abs(hilbert(NBnoise_LNN_eq.*E)),fs,[],get(ph(4),'Color'))
    
    figure;
    PTplot_fft(NBnoise_eq,fs)
    hold on;
    PTplot_fft(NBnoise_LNN_eq,fs)
    PTplot_fft(NBnoise_eq.*E,fs)
    PTplot_fft(NBnoise_LNN_eq.*E,fs)
end
