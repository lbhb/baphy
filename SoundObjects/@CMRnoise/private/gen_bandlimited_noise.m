function [noise,X0]=gen_bandlimited_noise(fs,T,Fc,varargin)
N=round(fs*T);
% In frequency domain, white noise has constant amplitude but uniformly
% distributed random phase. We generate this here. Only half of the
% samples are generated here, the rest are computed later using the complex
% conjugate symmetry property of the FFT (of real signals).


% Identify the locations of frequency bins. These will be used to zero out
% the elements of X that are not in the desired band
freqbins  = (0:N/2)'/N*fs;

% Zero out the frequency components outside the desired band 
iIn=round([Fc(1)/fs*N+1 Fc(2)/fs*N+1]);
if nargin>=4
   X=zeros(size(freqbins));
   %1X((freqbins >= Fc(1)) & (freqbins <= Fc(2)))=varargin{1};
   X(iIn(1):iIn(2))=varargin{1};
   X0=[];
else
    X = [1; exp(1i*2*pi*rand(N/2-1,1)); 1]; % X(1) and X(NFFT/2) must be real
    %X(find((freqbins < Fc(1)) | (freqbins > Fc(2)))) = 0;
    %X0=X((freqbins >= Fc(1)) & (freqbins <= Fc(2)));
    X(1:iIn(1)-1)=0;
    X(iIn(2)+1:end)=0;
    X0=X(iIn(1):iIn(2));
end
% Use the complex conjugate symmetry property of the FFT (for real signals) to
% generate the other half of the frequency-domain signal
X         = [X; conj(flipud(X(2:end-1)))];

% IFFT to convert to time-domain
noise     = real(ifft(X,N));

% Normalize such that 99.7% of the times signal lies between �2
%noise     = 2*noise'/prctile(noise, 99.7);
end