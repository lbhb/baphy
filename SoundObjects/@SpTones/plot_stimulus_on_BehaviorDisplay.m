function plot_stimulus_on_BehaviorDisplay(RefObject,TarObject,ThisTarParams,TrialSound,TrialEnv,maxRelTarRefdB)
try 
    ThisTarParams.TargetMode;
catch
    ThisTarParams.TargetMode='Envelope';
    ThisTarParams.RelativeTarRefdB=0;
end
if isempty(TrialEnv)
    return
end
switch ThisTarParams.TargetMode
    case {'Envelope','Carrier'}
        curr_sr=get(RefObject,'SamplingRate');
        Tenv_dur=size(TrialEnv,1)/curr_sr;
        sr=get(RefObject,'SamplingRateEnv');
        TrialEnv=resample(TrialEnv,sr,curr_sr);
        level_scale=10.^(-maxRelTarRefdB./20);
        t=(0:(size(TrialEnv,1)-1))/sr;
        hold on;
        ph=plot(t,TrialEnv*level_scale);
        set(gca,'XLim',[0 t(end)+0.5])
    case 'Waveform'
       error('Fix me') 
       
end

end