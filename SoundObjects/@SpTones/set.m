function o = set (o,varargin)
% This is a generic set function for all userdefined classes. Copy this 
% function in your objects directory.

% Nima Mesgarani, October 2005, mnima@umd.edu

% modified svd 2006-11-27 to allow for setting multiple properties at once
% (allowing for just a single ObjUpdate call)
do_ObjUpdate=true;
switch nargin
    case 1
    % if one argument, display all the fields
        get(o);
    % or two inputs, just return the field
    case 2 
        get(o,varargin{1});
    case {3,4,5,7,9,11,13,15,17}
        % if single property, change its value
        fields = fieldnames(o);
        for ii=0:2:(nargin-3)
            index = find(strcmpi (fields,varargin{1+ii})==1);
            if isempty(index)
                object_field = o.(fields{end});
                if isobject(object_field)
                    o.(fields{end}) = set(object_field,varargin{1+ii},varargin{2+ii});
                else
                    error('%s \n%s', ['There is no ''' varargin{1+ii} ''' property in specified class']);
                end
            else
                o.(fields{index})=varargin{2+ii};
            end
        end
        if mod(nargin,2)==0
            do_ObjUpdate=varargin{end};
        end
    otherwise
        error('%s \n%s','Error using ==> set','Too many input arguments.');
end
if do_ObjUpdate && length(varargin)==2
    if(any(strcmp(varargin{1},{'SamplingRate','PreStimSilence','PostStimSilence'})))
        do_ObjUpdate=false;
    end
    if strcmp(varargin{1},'Duration') && o.ShuffleOnset==2
        for i=1:length(o.CarrierObj)
            if isa(o.CarrierObj{i},'ComplexTone')
                o.CarrierObj{i}=set(o.CarrierObj{i},'ComplexToneDur',o.Duration);
            elseif isa(o.CarrierObj{i},'Noise')
                o.CarrierObj{i}=set(o.CarrierObj{i},'Duration',o.Duration);
            else
                error('fix me')
            end
        end
        do_ObjUpdate=false;
    end
end

if do_ObjUpdate
    caller = dbstack;
    if length(caller)>1
        if strcmpi(caller(2).name, 'ObjUpdate')
            do_ObjUpdate=false;% if its not called from ObjUpdate function, run it. 
            % without this check, it will become a close loop.
    %         disp(sprintf('called by %s',caller(2).name));        
        end
    end
end
if do_ObjUpdate
    o = ObjUpdate(o);
end