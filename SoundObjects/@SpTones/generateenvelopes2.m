

SamplingRate=40000;
SamplingRateFinal=2000;
smoothwindow=0.003.*SamplingRate;

subsetcount=5;
s=Speech;
s=set(s,'SamplingRate',SamplingRate);

fc=[64];Wc=fc/SamplingRate*2;
[B,A] = butter(6,Wc,'low');
fc=64;Wc=fc/SamplingRate*2;
[B_80,A_80] = butter(6,Wc);
fc=25;Wc=fc/SamplingRate*2;
[B_2,A_2] = butter(6,Wc);
for subidx=1:subsetcount,
   s=set(s,'Subsets',subidx);
   imax=get(s,'MaxIndex');
   emtx=zeros(round(get(s,'Duration').*SamplingRateFinal),imax);
   for idx=1:imax,
      sp=waveform(s,idx);
%       
%       sp2=gsmooth(abs(sp),smoothwindow);
%       %sp2=sp2./max(sp2);
%       sp2=(sp2-min(sp2))./range(sp2);
%       emtx(:,idx)=resample(sp2,SamplingRateFinal,SamplingRate);
%       
      sp3=abs(hilbert(sp));
      sp3=filtfilt(B,A,sp3);
      sp3=filtfilt(B,A,sp3);
      sp3=(sp3-min(sp3))./range(sp3);
      
%       sp3_f=abs(hilbert(sp));
%       sp3_f=filter(B_80,A_80,sp3_f);
%       sp3_f=(sp3_f-min(sp3_f))./range(sp3_f);
%       [~,mi]=max(x);
%       sv=v(mi);
%       sp3_f=[sp3_f(-sv:end);zeros(-sv-1,1)];
% 
%       sp3_f2=abs(hilbert(sp));
%       sp3_f2=filter(B_2,A_2,sp3_f2);
%       sp3_f2=(sp3_f2-min(sp3_f2))./range(sp3_f2);
%       [~,mi]=max(x);
%       sv=v(mi);
%       sp3_f2=[sp3_f2(-sv:end);zeros(-sv-1,1)];
%       
%       [pks,locs]=findpeaks(sp3_f2);
%       df=diff(sp3_f2);
%       sp3_fs=sp3_f;
%       for i=1:length(pks);
%           st_ind=find(df(1:locs(i)-1)<0,1,'last');
%           nd_ind=find(df(locs(i):end)>0,1)+locs(i);
%           if i==1
%               st_ind=round(locs(i)/2);
%           end
%           if ~isempty(st_ind) && ~isempty(nd_ind)
%               nd_ind=round(nd_ind-(nd_ind-locs(i))/2);
%               N_=nd_ind-st_ind+1;
%              ramp=pks(i)*ones(1,N_);
%              RampDur=.01;
%             sinePeriod = 2*RampDur;
%             npts = sinePeriod*SamplingRate;
%             sineFrequency = 1/sinePeriod;
% 
%            sineBuffer = -cos(2*pi*sineFrequency*(0:npts-1)/SamplingRate);
%            sineBuffer =(sineBuffer + 1)/2;
%             N = round(length(sineBuffer)/2);
%             N2=length(sineBuffer)-N;
%             ramp(1:N)=sineBuffer(1:N)*(pks(i)-sp3_fs(st_ind))+sp3_fs(st_ind);
%             ramp(end-N2+1:end)=sineBuffer(N+1:end)*(pks(i)-sp3_fs(nd_ind))+sp3_fs(nd_ind);
%             
%             %sp3_fs(st_ind:nd_ind)=ramp;
%             sp3_fs(st_ind:locs(i))=ramp(1:locs(i)-st_ind+1);
%           end
%       end
%       sp4=sp;sp4(sp4<0)=0;
%       sp4=filtfilt(B,A,sp4);
%       sp4=(sp4-min(sp4))./range(sp4);
%       
%       cf=2000;
%       bw=1.019*24.7*(4.37e-3*cf+1);
%       [bm,env,instf,delay]=gammatone(sp,SamplingRate,cf,bw,false);
%       sp5=abs(hilbert(bm))';
%       sp5=filtfilt(B,A,sp5);
%       %sp5=filter(B,A,sp5);
%       %sp5(sp5<0)=0;
%       sp5=(sp5-min(sp5))./range(sp5);
      emtx(:,idx)=resample(sp3,SamplingRateFinal,SamplingRate);
   end
   
   clear Env;
   Env.emtx=emtx;
   Env.fs=SamplingRateFinal;
   Env.Duration=get(s,'Duration');
   Env.Names=get(s,'Names');
   Env.Phonemes=get(s,'Phonemes');
   Env.MaxIndex=get(s,'MaxIndex');
   
   outfile=['envelope/Speech.subset',num2str(subidx),'.mat'];
   %outfile=['envelope/Speech.subset',num2str(subidx),'_H_LPfilt100HzN6_2x_shift.mat'];
   save(outfile,'Env');
end



subsetcount=5;
s=FerretVocal;
s=set(s,'SamplingRate',SamplingRate);

for subidx=1:subsetcount,
   s=set(s,'Subsets',subidx);
   imax=get(s,'MaxIndex')
   emtx=zeros(round(get(s,'Duration').*SamplingRateFinal),imax).*nan;
   for idx=1:imax,
      sp=waveform(s,idx);
      
      sp=gsmooth(abs(sp),smoothwindow);
      sp=sp./max(sp);
      sp=resample(sp,SamplingRateFinal,SamplingRate);
      
      if length(sp)>size(emtx,1),
         emtx=cat(1,emtx,ones(length(sp)-size(emtx,1),imax).*nan);
      end
      emtx(1:length(sp),idx)=sp;
   end
   
   clear Env
   Env.emtx=emtx;
   Env.fs=SamplingRateFinal;
   Env.Duration=get(s,'Duration');
   Env.Names=get(s,'Names');
   Env.Phonemes={};
   Env.MaxIndex=get(s,'MaxIndex');
   outfile=['envelope/FerretVocal.subset',num2str(subidx),'.mat'];
   save(outfile,'Env');
   
end
