function plot_stimulus(RefObject,TarObject,ThisTarParams,TrialSound,TrialSamplingRate,RefTrialIndex,TrialIndex,TarIdx,events,TrialEnv)
try 
    ThisTarParams.TargetMode;
catch
    ThisTarParams.TargetMode='Envelope';
    ThisTarParams.RelativeTarRefdB=0;
end
if TrialIndex==1
    fh=sfigure(99);
    set(fh,'Position',[12        1824        1904         248])
end
switch ThisTarParams.TargetMode
    case {'Envelope','Carrier'}
        curr_sr=get(RefObject,'SamplingRate');
        Tenv_dur=size(TrialEnv,1)/curr_sr;
        sr=get(RefObject,'SamplingRateEnv');
        TrialEnv=resample(TrialEnv,sr,curr_sr);
        
        bandcount=size(TrialEnv,2);
        subplot(3,1,1);
        ax(1)=gca;
        cla
        hold on;
        co=get(gca,'ColorOrder');
        ls={'-','--',':'};
        for bb=1:bandcount,
            if isa(RefObject.CarrierObj{bb},'ComplexTone')
                freqs= get(RefObject.CarrierObj{bb},'AnchorFrequency')*cumprod([1 ; get(RefObject.CarrierObj{bb},'ComponentRatios')]);
                amps=get(RefObject.CarrierObj{bb},'ComponentAmplitudes');
                for j=1:length(freqs)
                    plot(freqs([j j]),[0 amps(j)],'Color',co(bb,:),'LineWidth',2,'Linestyle',ls{bb})
                end
            elseif isa(RefObject.CarrierObj{bb},'Noise')
                f=get(RefObject.CarrierObj{bb},'LowFreq');
                f(2)=get(RefObject.CarrierObj{bb},'HighFreq');
                ph=patch(f([1 2 2 1]),[0 0 1 1],co(bb,:),'FaceAlpha',.8);
            end
        end
        hold off;
                
        subplot(3,1,2);
        ax(2)=gca;
        t=(0:(size(TrialEnv,1)-1))/sr;
        plot(t,TrialEnv);
        set(gca,'XLim',[0 max([events.StopTime])],'YLim',[0 max(1.01,max(10.^(ThisTarParams.RelativeTarRefdB/20)))],'XTickLabels','');
        
        subplot(3,1,3);
        ax(3)=gca;
    case 'Waveform'
        
        subplot(2,1,1);
        sr=get(RefObject,'SamplingRateEnv');
        RefObject2=set(RefObject,'SamplingRate',sr);
        RefObject2=set(RefObject2,'PreStimSilence',0);
        RefObject2=set(RefObject2,'PostStimSilence',0);
        [RefEnv,~]=env(RefObject2, RefTrialIndex,TrialIndex);
        lf=get(RefObject,'LowFreq');
        hf=get(RefObject,'HighFreq');
        freq=20:40000;
        dur=length(TrialSound)/TrialSamplingRate;
        dat=zeros(dur*sr,length(freq));
        ref_ind=arrayfun(@(x)~isempty(strfind(x.Note,'Reference')),events) & arrayfun(@(x)~isempty(strfind(x.Note,'Stim ,')),events);
        hold on;
        for i=1:size(RefEnv,2)
            imagesc(events(ref_ind).StartTime:.01:events(ref_ind).StopTime,[lf(i):hf(i)],repmat(RefEnv(:,i)',hf(i)-lf(i)+1,1));
        end
        TarObject2=set(TarObject,'SamplingRate',sr);
        TarObject2=set(TarObject2,'PreStimSilence',0);
        TarObject2=set(TarObject2,'PostStimSilence',0);
        [TarEnv,~] = env(TarObject2, TarIdx, 0);
        TarEnv=TarEnv./max(TarEnv);
        tar_ind=arrayfun(@(x)~isempty(strfind(x.Note,'Target')),events) & arrayfun(@(x)~isempty(strfind(x.Note,'Stim ,')),events);
        tar_lf=get(TarObject,'LoBounds');
        tar_hf=get(TarObject,'HiBounds');
        imagesc(events(tar_ind).StartTime:.01:events(tar_ind).StopTime,[tar_lf:tar_hf],repmat(TarEnv',hf(i)-lf(i)+1,1));
        
        colormap('gray')
        set(gca,'YScale','log')
        
        set(gca,'YLim',[20 20000])
        UTset_cf_labels(gca,1000,1,'ytick');
        set(gca,'XLim',[0 events(end).StopTime]);
        
        if(1)
            win=1.5;
            set(gca,'YLim',[min(lf)/win max(hf)*win])
            UTset_cf_labels(gca,1000,2,'ytick');
        end
        subplot(2,1,2);
        ax(2)=gca;      
end

Nchannels=strcmp(get(RefObject,'SplitChannels'),'Yes')+1;
dfs=50000;
tw=resample(TrialSound(:,1:Nchannels),dfs,TrialSamplingRate);
if Nchannels==1
    plot((1:length(tw))./dfs,tw(:,Nchannels),'k','Parent',ax(end));
elseif Nchannels==2
    plot((1:length(tw))./dfs,tw(:,1),'Color',co(1,:),'Parent',ax(end));
    hold(ax(end),'on');
    plot((1:length(tw))./dfs,tw(:,2),'Color',co(2,:),'Parent',ax(end));
end
set(ax(end),'XLim',[0 max([events.StopTime])]);
set(ax(end),'YLim',[min(min(tw(:,1:Nchannels))) max(max(tw(:,1:Nchannels)))])
ylabel(ax(end),'Signal to DAC (V)');xlabel(ax(end),'Time (sec)')

switch ThisTarParams.TargetMode
    case {'Envelope','Carrier'}
        p2=get(ax(2),'Position');
        p3=get(ax(3),'Position');
        edge_diff=(p2(2)-(p3(2)+p3(4)))*.9;
        p2n=p2;p2n(2)=p2n(2)-edge_diff/2;p2n(4)=p2n(4)+edge_diff/2;
        p3n=p3;p3n(4)=p3n(4)+edge_diff/2;
        %p2n(2)=p2n(2)+.1;p3n(2)=pn(2)+.1;
        set(ax(2),'Position',p2n);
        set(ax(3),'Position',p3n);
end
end