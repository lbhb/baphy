
if 0
    %%
    SamplingRate=40000;
    SamplingRateFinal=2000;
    smoothwindow=0.003.*SamplingRate;
    
    subsetcount=5;
    s=Speech;
    s=set(s,'SamplingRate',SamplingRate);
    
    for subidx=1:subsetcount,
        s=set(s,'Subsets',subidx);
        imax=get(s,'MaxIndex')
        emtx=zeros(round(get(s,'Duration').*SamplingRateFinal),imax);
        for idx=1:imax,
            sp=waveform(s,idx);
            
            sp=gsmooth(abs(sp),smoothwindow);
            sp=sp./max(sp);
            
            emtx(:,idx)=resample(sp,SamplingRateFinal,SamplingRate);
        end
        
        clear Env;
        Env.emtx=emtx;
        Env.fs=SamplingRateFinal;
        Env.Duration=get(s,'Duration');
        Env.Names=get(s,'Names');
        Env.Phonemes=get(s,'Phonemes');
        Env.MaxIndex=get(s,'MaxIndex');
        
        outfile=['envelope/Speech.subset',num2str(subidx),'.mat'];
        save(outfile,'Env');
    end
end
if 0
    %%
    subsetcount=5;
    s=FerretVocal;
    s=set(s,'SamplingRate',SamplingRate);
    
    for subidx=1:subsetcount,
        s=set(s,'Subsets',subidx);
        imax=get(s,'MaxIndex')
        emtx=zeros(round(get(s,'Duration').*SamplingRateFinal),imax).*nan;
        for idx=1:imax,
            sp=waveform(s,idx);
            
            sp=gsmooth(abs(sp),smoothwindow);
            sp=sp./max(sp);
            sp=resample(sp,SamplingRateFinal,SamplingRate);
            
            if length(sp)>size(emtx,1),
                emtx=cat(1,emtx,ones(length(sp)-size(emtx,1),imax).*nan);
            end
            emtx(1:length(sp),idx)=sp;
        end
        
        clear Env
        Env.emtx=emtx;
        Env.fs=SamplingRateFinal;
        Env.Duration=get(s,'Duration');
        Env.Names=get(s,'Names');
        Env.Phonemes={};
        Env.MaxIndex=get(s,'MaxIndex');
        outfile=['envelope/FerretVocal.subset',num2str(subidx),'.mat'];
        save(outfile,'Env');
        
    end
end
if 1
    %%
    clear
    rng default
    SamplingRateFinal=2000;
    %_H_LPfilt50HzN6_filtfilt2x
    pth='C:\Users\lbhb\Downloads\';
    pth='/auto/data/lbhb/Stimulus_Library/speech_samples/Librivox/';
    fns={'confession_01_tolstoy_64kb.wav','confession_02_tolstoy_64kb.wav','confession_03_tolstoy_64kb.wav'};
    fs_hilbert=22050;
    fs=22050;
    lpFilt2 = designfilt('lowpassiir', 'FilterOrder', 3, ...
        'PassbandFrequency', 10, 'PassbandRipple',.2,...
        'SampleRate', fs);
    lpFilt = designfilt('lowpassiir', 'FilterOrder', 2, ...
        'PassbandFrequency', 50, 'PassbandRipple', 0.2,...
        'SampleRate', fs);
%      lpFilt2CB2 = designfilt('lowpassiir', 'FilterOrder', 3, ...
%         'StopBandFrequency',60,'StopbandAttenuation',65,...
%         'SampleRate', fs,'DesignMethod', 'Cheby2');
%     MA_B=ones(20,1)/20;
%     MA_H=dfilt.df2t(MA_B,1);
%     MMA_H=dfilt.cascade(MA_H,MA_H,MA_H,MA_H,MA_H); 
    
    st_buf=.02;
    dur_range=[25 35]-st_buf;
    normalization_range=[3 9];
    normalization_range_sample_start=[2 8];
    normalization_range_target=4;
    normalization_range_target_sample_start=3;
    silence_thresh=.01; silence_time_thresh=.1;
    max_silence=.6; shortened_silence_range=[.3 max_silence];
    
    st_buf_samples=st_buf*fs;
    dur_range_samples=dur_range*fs;
    normalization_range_samples=normalization_range*fs;
    normalization_range_sample_start_samples=normalization_range_sample_start*fs;
    normalization_range_target_samples=normalization_range_target*fs;
    normalization_range_target_sample_start_samples=normalization_range_target_sample_start*fs;
    
    
    total=round(.02*fs)/fs;rise=total;fall=0;
    sil_ramp=UTcosramp(total,rise,fall,fs)'; NSR=length(sil_ramp);
    
    seg_ind=1;
    nf_seg_ind=1;
    for idx=1:length(fns)
        fprintf('\nIdx %d: ',idx)
        [sp,fs_]=audioread([pth,fns{idx}]);
        if fs~=fs_
            sp=resample(sp,fs_hilbert,fs_);
        end
        %fl_t=1;
        %fl=round(fl_t*fs);
        %env=envelope(sp,fl);
        
        env_h=abs(hilbert(sp));
        env_h=resample(env_h,fs,fs_hilbert);
        %env_hf_=filter(lpFilt,env_h);
        %env_hf0=flipud(filter(lpFilt,flipud(env_hf_)));
        %env_hs=fastsmooth(env_h,round(.02*fs),1,1);
        %env_hf=fastsmooth(env_hs,round(.025*fs),1,1);
        env_hf=filtfilt(lpFilt,env_h);
        env_hf=abs(env_hf);
        %      env_hf=filtfilt(lpFilt,env_hf);
        Env.file_norm_factor(idx)=max(env_hf);
        env_hf=env_hf./Env.file_norm_factor(idx);
        
        silent_inds_=env_hf < silence_thresh;
        %apply a 1-second smoothing filter
        silent_inds1 = smooth(silent_inds_, fs*silence_time_thresh);
        silent_inds = silent_inds1 > 1-10*eps;
        df=diff(silent_inds);
        sil_st=[1; find(df==1)];
        sil_nd=[find(df==-1); length(env_hf)];
        if length(sil_st) ~= length(sil_nd), error('Unequal silence interval markers'), end
        sil_mids=round(sil_st+(sil_nd-sil_st)/2);
        sil_durs=(sil_nd-sil_st)/fs;
        rmi=false(size(env_hf));
        for i=1:length(sil_durs)
            if (sil_nd(i)-sil_st(i))>2*NSR
                %apply ramp to make silences truly silent
                env_hf(sil_st(i)+(0:NSR-1))=env_hf(sil_st(i)+(0:NSR-1)).*flipud(sil_ramp);
                env_hf(sil_nd(i)+(-NSR+1:0))=env_hf(sil_nd(i)+(-NSR+1:0)).*sil_ramp;
                env_hf((sil_st(i)+NSR):(sil_nd(i)-NSR))=0;
            else
                aa=2;
                %figure;plot(sil_st(i-2)-1e2:sil_nd(i+2),env_hf(sil_st(i-2)-1e2:sil_nd(i+2)))
            end
            if sil_durs(i)>max_silence
                new_sil_dur=rand*diff(shortened_silence_range)+shortened_silence_range(1);
                half_rm_samps=ceil((sil_durs(i)-new_sil_dur)*fs/2);
                rm_st_ind=sil_mids(i)-half_rm_samps;
                rmi(rm_st_ind+[0:half_rm_samps*2])=true;
            end
        end
        env_hf(rmi)=[];
        silent_inds_=env_hf < silence_thresh;
        %apply a 1-second smoothing filter
        silent_inds1 = smooth(silent_inds_, fs*silence_time_thresh);
        silent_inds = silent_inds1 > 1-10*eps;
        df=diff(silent_inds);
        sil_st=[1; find(df==1)];
        sil_nd=[find(df==-1); length(env_hf)];
        if length(sil_st) ~= length(sil_nd), error('Unequal silence interval markers'), end
        sil_mids=round(sil_st+(sil_nd-sil_st)/2);
        sil_durs=(sil_nd-sil_st)/fs;
        sound_lengths=(sil_st(2:end)-sil_nd(1:end-1))/fs;
        max_sound_length=max(sound_lengths);
        
        %Normalize each section
        sil_ind=1;
        Env.segment_nf_ind_st(idx)=nf_seg_ind;
        while 1
            possible_lengths=sil_mids(sil_ind+1:end)-sil_nd(sil_ind);
            good_lengths=find(possible_lengths>=normalization_range_samples(1) & possible_lengths<normalization_range_samples(2));
            if sum(good_lengths)>0
                [~,bli]=min(abs(possible_lengths(good_lengths)-normalization_range_target_samples));
                best_length=good_lengths(bli);
            elseif all(possible_lengths<normalization_range_samples(1))
                break
            else
                error('No good lengths, possible lengths are: %.03f %.03f %.03f %.03f %.03f %.03f ',possible_lengths(1:6)/fs)
                ind=sil_st(sil_ind)+(0:normalization_range_samples(2)*2);
                figure;plot(ind/fs,env_hf(ind));
            end
            inds=sil_nd(sil_ind):sil_mids(sil_ind+best_length);
            this_seg=env_hf(inds);
            Env.segment_norm_factor(nf_seg_ind)=max(env_hf(inds));
            env_hf(inds)=env_hf(inds)./Env.segment_norm_factor(nf_seg_ind);
            nf_seg_ind=nf_seg_ind+1;
            sil_ind=sil_ind+best_length;
        end
        
        
        sil_ind=1;
        while 1
            if (sil_nd(sil_ind)-sil_st(sil_ind))/fs<st_buf
                sil_ind=sil_ind+1;
                continue;
            end
            possible_lengths=sil_mids(sil_ind+1:end)-sil_nd(sil_ind);
            good_lengths=find(possible_lengths>=dur_range_samples(1) & possible_lengths<dur_range_samples(2));
            if sum(good_lengths)>0
                [~,bli]=min(abs(possible_lengths(good_lengths)-mean(dur_range_samples)));
                best_length=good_lengths(bli);
            elseif all(possible_lengths<dur_range_samples(1))
                break
            else
                error('No good lengths')
            end
            inds=sil_nd(sil_ind)-st_buf_samples:sil_mids(sil_ind+best_length);
            this_seg=env_hf(inds);
            %this_seg=this_seg./max(this_seg); already normed above
            emtx_{seg_ind}=resample(this_seg,SamplingRateFinal,fs);
            Env.downsampled_seg_nf(seg_ind)=max(emtx_{seg_ind});
            emtx_{seg_ind}=emtx_{seg_ind}./Env.downsampled_seg_nf(seg_ind);
            if any(emtx_{seg_ind}<-1e-5), error('Envelopes less than 0.'), end
            emtx_{seg_ind}(emtx_{seg_ind}<0)=0;
            sil_sts_orig_fs{seg_ind}=[1; sil_st(sil_st>=inds(1) & sil_st<=inds(end))-inds(1)+1];
            sil_nds_orig_fs{seg_ind}=[sil_nd(sil_nd>=inds(1) & sil_nd<=inds(end))-inds(1)+1; length(this_seg)];
            sil_sts{seg_ind}=[1; round((sil_st(sil_st>=inds(1) & sil_st<=inds(end))-inds(1)+1)/fs*SamplingRateFinal)];
            sil_nds{seg_ind}=[round((sil_nd(sil_nd>=inds(1) & sil_nd<=inds(end))-inds(1)+1)/fs*SamplingRateFinal); length(emtx_{seg_ind})];
            sil_mids_orig_fs=round(sil_sts_orig_fs{seg_ind}+(sil_nds_orig_fs{seg_ind}-sil_sts_orig_fs{seg_ind})/2);
            sil_mids_=round(sil_sts{seg_ind}+(sil_nds{seg_ind}-sil_sts{seg_ind})/2);
            
            Names{seg_ind}=sprintf('TS%d_%d',idx,round(inds(1)/fs));
            
            %normalize first part of this segment to ensure each stream starts with a high amplitude
            sil_ind2=1;
            possible_lengths2=sil_mids_orig_fs(sil_ind2+1:end)-sil_nds_orig_fs{seg_ind}(sil_ind2);
            good_lengths2=find(possible_lengths2>=normalization_range_sample_start_samples(1) & possible_lengths2<normalization_range_sample_start_samples(2));
            if sum(good_lengths2)>0
                [~,bli]=min(abs(possible_lengths2(good_lengths2)-normalization_range_target_sample_start_samples));
                best_length2=good_lengths2(bli);
            elseif all(possible_lengths2<normalization_range_sample_start_samples(1))
                break
            else
                error('No good lengths, possible lengths are: %.03f %.03f %.03f %.03f %.03f %.03f ',possible_lengths2(1:6)/fs)
            end
            inds2=sil_nds{seg_ind}(sil_ind2):sil_mids_(sil_ind2+best_length2);
            Env.segment_norm_factor2(seg_ind)=max(emtx_{seg_ind}(inds2));
            emtx_{seg_ind}(inds2)=emtx_{seg_ind}(inds2)./Env.segment_norm_factor2(seg_ind);
            %figure;plot((1:length(emtx_{seg_ind}))/SamplingRateFinal,emtx_{seg_ind},'-')
            
            
            if 0
                t1=(0:(length(this_seg)-1))/fs;
                figure;plot(t1,this_seg);hold on;
                for i=1:length(sil_sts{seg_ind})
                    line(sil_sts_orig_fs{seg_ind}([i i])/fs,[0 1],'LineStyle','--','Color','r');
                    line(sil_nds_orig_fs{seg_ind}([i i])/fs,[0 1],'LineStyle','--','Color','g');
                end
                ax=gca;
                t2=(0:(length(emtx_{seg_ind})-1))/SamplingRateFinal;
                figure;plot(t2,emtx_{seg_ind});hold on;
                for i=1:length(sil_sts{seg_ind})
                    line(sil_sts{seg_ind}([i i])/SamplingRateFinal,[0 1],'LineStyle','--','Color','r');
                    line(sil_nds{seg_ind}([i i])/SamplingRateFinal,[0 1],'LineStyle','--','Color','g');
                end
                ax(2)=gca;
                linkaxes(ax);
            end
            if length(sil_sts{seg_ind}) ~= length(sil_nds{seg_ind})
                error('Silent inds mismatch')
            end
            seg_ind=seg_ind+1;
            sil_ind=sil_ind+best_length;
            fprintf('%d ',seg_ind)
        end
    end
    if length(unique(Names))<length(Names)
        error('Non-unique names')
    end
    %    figure;hold on;
    %    for i=1:3:length(emtx_)
    %        plot((0:(length(emtx_{i})-1))/fs,emtx_{i})
    %    end
    ml=max(cellfun(@length,emtx_));
    emtx=nan(ml,length(emtx_));
    for i=1:length(emtx_)
        emtx(1:length(emtx_{i}),i)=emtx_{i};
    end
    if 0
        figure;imagesc(emtx);
        xlim([.5 6000]);ylim([.5 30.5]);set(gca,'CLim',[0 1])
        d=load('C:\code\baphy-current\SoundObjects\@SpTones\envelope\Speech.subset1_H_LPfilt50HzN6_filtfilt2x.mat');
        figure;imagesc(d.Env.emtx');
        xlim([.5 6000]);ylim([.5 30.5]);set(gca,'CLim',[0 1])
    end
    clear Env;
    Env.emtx=emtx;
    Env.Nsamples=cellfun(@length,emtx_);
    Env.fs=SamplingRateFinal;
    Env.Duration=min(Env.Nsamples)/Env.fs;%mean(dur_range+st_buf);
    Env.sil_sts=sil_sts;
    Env.sil_nds=sil_nds;
    Env.sil_durs=cellfun(@(x,y)(y-x)/SamplingRateFinal,sil_sts,sil_nds,'Uni',0);
    Env.Names=Names;
    
    %    Env.Phonemes=get(s,'Phonemes');
    Env.MaxIndex=length(emtx_);
    Env.notes=['Three chapters from librovox at 22.05 kHz (https://librivox.org/a-confession-version-2-by-leo-tolstoy/)'...
        ,' abs(hilbert) then'...
        ' LP filtered cheby1 N=6 50 Hz, 0.2 ripple. Then resampled to 2 kHz,'...
        ' and split into sections 25-35 sec long. Each section starts with 20 ms'...
        ' of silence. Rise times are variable.'];
    Env.fns=fns;
    Env.lpFilt=lpFilt;
    vars={'st_buf','dur_range','normalization_range','normalization_range_sample_start',...
        'normalization_range_target','normalization_range_target_sample_start','silence_thresh',...
        'silence_time_thresh','max_silence','shortened_silence_range','fs'};
    for i=1:length(vars)
        Env.params.(vars{i})=eval(vars{i});
    end
    
    
    if 1 %shorten to save resampling time during experiments
        dur_range2=[12 18];
        dur_range2_samples=dur_range2*SamplingRateFinal;
        Env.emtx_all=Env.emtx;
        Env.Nsamples_all=Env.Nsamples;
        Env.Duration_all=Env.Duration;
        Env.emtx=nan(dur_range2_samples(end),length(emtx_));
        for seg_ind=1:length(emtx_)
            sil_ind2=1;
            sil_mids2=round(sil_sts{seg_ind}+(sil_nds{seg_ind}-sil_sts{seg_ind})/2);
            possible_lengths2=sil_mids2(sil_ind2+1:end);
            good_lengths2=find(possible_lengths2>=dur_range2_samples(1) & possible_lengths2<dur_range2_samples(2));
            if sum(good_lengths2)>0
                %possible_lengths2(good_lengths2)/SamplingRateFinal
                [~,bli]=min(abs(possible_lengths2(good_lengths2)-mean(dur_range2_samples)));
                best_length2=good_lengths2(bli);
            elseif all(possible_lengths2<dur_range2_samples(1))
                break
            else
                error('No good lengths, possible lengths are: %.03f %.03f %.03f %.03f %.03f %.03f ',possible_lengths2(1:6)/SamplingRateFinal)
            end
            inds2=1:sil_mids2(sil_ind2+best_length2);
            Env.Nsamples(seg_ind)=length(inds2); %length(inds2)/SamplingRateFinal
            Env.emtx(1:length(inds2),seg_ind)=Env.emtx_all(1:length(inds2),seg_ind);
        end
        Env.emtx(max(Env.Nsamples)+1:end,:)=[];
        Env.Duration=min(Env.Nsamples)/Env.fs;
    end
    
    dd=fileparts(which('Sptones'));
    outfile=['/envelope/Speech.subset6_N2.mat'];
    %save([dd outfile],'-Struct','Env');
    
    if 0
        sil_durs=cell2mat(cellfun(@(x)x',Env.sil_durs,'Uni',0));
        figure;h7=histogram(sil_durs,50);
        figure;imagesc(Env.emtx')
        xlim([.5 6000])
    end
end

if 0
    %%
    fs=22050;
    fs=4000;
    env_h=abs(hilbert(sp));
    env_h=resample(env_h,fs,fs_hilbert);
    lpFilt = designfilt('lowpassiir', 'FilterOrder', 6, ...
        'PassbandFrequency', 50, 'PassbandRipple', 0.2,...
        'SampleRate', fs);
    env_hf0=filtfilt(lpFilt,env_h);
    
    lpFilt = designfilt('lowpassiir', 'FilterOrder', 6, ...
        'PassbandFrequency', 10, 'PassbandRipple',.2,...
        'SampleRate', fs);
    env_hf=filtfilt(lpFilt,env_h);
    
    lpFilt = designfilt('lowpassiir', 'FilterOrder', 2, ...
        'PassbandFrequency', 50, 'PassbandRipple', 0.2,...
        'SampleRate', fs);
    %env_hs=fastsmooth(env_h,round(.02*fs),1,1);
    env_hf2=filtfilt(lpFilt,env_h);
    
    tHMs=.001*2.^(0:9);
    FWHMs=round(2*tHMs.*fs);
    sigmas=FWHMs./(2*sqrt(2*log(2)));
    env_hs2=nan(length(sigmas),length(env_hs));
    for i=1:length(sigmas)
        env_hs2(i,:)=gsmooth(env_hf2,sigmas(i));
    end
    
    t=(1:length(env_hf))/fs;
    pi=1:1e3/200*fs;
    figure;
    %ph=plot(t(pi),env_h(pi)); hold on;
    ph=plot(t(pi),env_hf0(pi)); hold on
    ph(2)=plot(t(pi),env_hf(pi));
    ph(3)=plot(t(pi),env_hf2(pi));
    cols=parula(size(env_hs2,1)+4);
    set(gca,'ColorOrder',cols)
    ph_=plot(t(pi),env_hs2(:,pi));
    xlabel('Time')
    legs=arrayfun(@(x)['Cheby LP 50 Hz N=2, then gauss FWHM=',num2str(x),'ms'],tHMs*2*1000,'Uni',0);
    legend([ph';ph_],['Cheby1 LP 50Hz N=6','Cheby1 LP 10Hz N=6','Cheby LP 50 Hz N=2',legs])
    %env_hf=filtfilt(lpFilt,env_h);
    %env_hf=abs(env_hf);
    
   %% Comparing iterated moving averaging and gaussian kernel smoothing
    imp=[zeros(10000,1);1;zeros(10000,1)];
    %imp=[zeros(100,1);1;zeros(10000,1);1;zeros(10000,1)]; %weird edge effects
    fs=200;
    t=(1:length(imp))/fs - 10001/fs;t=t*1000;
    figure;plot(t,imp);hold on;
   % s=fastsmooth(imp,round(.02*fs),1,1); plot(t,s./max(s));
    %s=fastsmooth(imp,round(.02*fs),3,1); plot(t,s./max(s));
    wN=round(.02*fs);wN=3;
    s=fastsmooth(imp,wN,1,1); plot(t,s./max(s));
    s=fastsmooth(imp,wN,2,1); plot(t,s./max(s));
    tic;s=fastsmooth(imp,wN,3,1); toc;
    plot(t,s./max(s));
    s=s./max(s);
    st=find(s>.4,1); nd=find(s>.51,1);
    tHM=abs(interp1(s(st:nd)./max(s),t(st:nd)/1000,.5,'spline'));
    FWHM=round(2*tHM*fs);
    sigma=FWHM/(2*sqrt(2*log(2)));
    tic;s=gsmooth(imp,1.55); toc;
    plot(t,s./max(s));
    legend('Original','fastsmooth','gsmooth')
    %conclusion: They yield very similar results, guassian is obviouslly
    %more precisely a gaussian, fast smooth is pretty close even with 3
    %iterations though. They take an equal amoutn of time at approximately
    %7 fastsmooth iterations
    %Both have wierd edge effects
end