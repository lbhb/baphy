function [w,event,e]=waveform (o,index,IsRef,TarObject,ThisTarParams)
% function w=waveform(o, index,IsRef);
%
% generate waveform for SpNoise object
%

global SPNOISE_EMTX
if nargin<3, IsRef=1; end
if(nargin<4)
    TarObject=[];
    ThisTarParams=[];
end

F0s=get(o,'F0s');
RelAttenuatedB=get(o,'RelAttenuatedB');
SplitChannels=get(o,'SplitChannels');
bSplitChannels=strcmpi(SplitChannels,'Yes');
SamplingRate = get(o,'SamplingRate');
Duration = get(o,'Duration'); % duration is second
PreStimSilence = get(o,'PreStimSilence');
PostStimSilence = get(o,'PostStimSilence');
Names = get(o,'Names');
SamplingRateEnv = get(o,'SamplingRateEnv');
BaseSound=strtrim(get(o,'BaseSound'));
Subsets = get(o,'Subsets');
BaselineFrac=get(o,'BaselineFrac');
idxset=get(o,'idxset');
ShuffledOnsetTimes=get(o,'ShuffledOnsetTimes');
SingleBandFrac=get(o,'SingleBandFrac');

EnvVarName=[BaseSound,num2str(Subsets)];
%emtx = get(o,'emtx');
emtx = SPNOISE_EMTX.(EnvVarName);

N=round(Duration*SamplingRate);
timesamples = (1:N)' / SamplingRate;
w=zeros(N+(PreStimSilence+PostStimSilence)*SamplingRate,bSplitChannels+1);


if isempty(RelAttenuatedB) || length(RelAttenuatedB)<length(F0s),
  RelAttenuatedB=zeros(size(F0s));
end

bandcount=length(F0s);

if ~isempty(TarObject) && strcmp(ThisTarParams.TargetMode,'Envelope')
    [e,event]=env(o,index,IsRef,TarObject,ThisTarParams);
else
    [e,event]=env(o,index,IsRef);
end

for bb=1:bandcount,
    co=o.CarrierObj{bb};
    if 0
        %for 2 HCs F0 ratio 3:2, 3rd note on 3H of C1 (2H of C2)
        if any(ThisTarParams.ThisTarIdx == [1 3])
            if bb==1
                co=set(co,'ComponentAmplitudes',[ 1 1 1 1]');
            else
                co=set(co,'ComponentAmplitudes',[ 1 0 1 1]');
            end
        else
            if bb==1
                co=set(co,'ComponentAmplitudes',[ 1 1 0 1]');
            else
                co=set(co,'ComponentAmplitudes',[ 1 1 1 1]');
            end
        end
    end
    if ~isempty(TarObject) && strcmp(ThisTarParams.TargetMode,'Carrier') && ThisTarParams.TargetChannel==bb
        [tw,carrier_event]=waveform(co,1,IsRef,TarObject,ThisTarParams);
        tar_event=carrier_event(arrayfun(@(x)~isempty(strfind(x.Note,'Target')),carrier_event));
        coh_str=get_target_suffixes(o,index);
        for i=1:length(tar_event)
            comma_inds=strfind(tar_event(i).Note,',');
            tar_event(i).Note=[tar_event(i).Note(1:comma_inds(2)-2),':',coh_str{1},tar_event(i).Note(comma_inds(2)-1:end)];
        end
        event=[event tar_event];
    else
        tw=waveform(co,1);
    end
    
    %tw=tw./max(abs(tw(:)));%normalize to 1 so that there can be no clipping.
    nf(bb)=max(abs(tw(:)));
    
    % Now, put it in the silence:
    tw = [zeros(PreStimSilence*SamplingRate,1) ; 
    tw ; zeros(PostStimSilence*SamplingRate,1)];
  
    % apply envelope
    tw=tw.*e(:,bb);
    
    if SingleBandFrac && max(abs(tw))>0,
       tw=tw./max(abs(tw));
    end
    
    % adjust level relative to other bands
    %done in env. Good?
    %level_scale=10.^(-RelAttenuatedB(bb)./20);
    level_scale=1/o.mean; %scale to so that mean envelope is 1. Thus, RMS will be 1, turned into 5 later = 80 dB?
    tw_(:,bb)=tw.*level_scale;
end

if all(cellfun(@(x)isa(x,'ComplexTone'),o.CarrierObj))
    sum_amps=cellfun(@(x)sum(get(x,'ComponentAmplitudes')),o.CarrierObj);
elseif isa(o.CarrierObj{1},'ComplexTone')
    %hack for ComplexTone vs Noise
    sum_amps=cellfun(@(x)sum(get(x,'ComponentAmplitudes')),o.CarrierObj(1));
    sum_amps=sum_amps([1 1]);
end
 nfs=sum_amps * max(nf)/max(sum_amps);
%rms_per=cellfun(@(x)sqrt(sum(get(x,'ComponentAmplitudes').^2)/2),o.CarrierObj);
%nfs=rms_per * max(nf) / max(rms_per);
for bb=1:bandcount
    if bSplitChannels
        w(:,bb)=tw_(:,bb)/nfs(bb);
    else
        %w=w+tw.*level_scale;
        w=w + tw_(:,bb)/nfs(bb);
    end
end
%want this?
%w=w./max(abs(w(:)));
% 10ms ramp at onset and offset:
%done in env. Good?
%ramp = hanning(round(.01 * SamplingRate*2));
%ramp = ramp(1:floor(length(ramp)/2));
%ramp=repmat(ramp,[1,chancount]);
%w(1:length(ramp),:) = w(1:length(ramp),:) .* ramp;
%w(end-length(ramp)+1:end,:) = w(end-length(ramp)+1:end,:) .* flipud(ramp);

% normalize min/max +/-5
%w = 5 ./ max(abs(w(:))) .* w;
w=w*5;%5 is 80 dB. Both envelope and carrier max out at 1.

% generate the event structure:
%event = struct('Note',['PreStimSilence , ' Names{index}],...
%    'StartTime',0,'StopTime',PreStimSilence,'Trial',[]);
%event(2) = struct('Note',['Stim , ' Names{index}],'StartTime'...
%    ,PreStimSilence, 'StopTime', PreStimSilence+Duration, 'Trial',[]);
%event(3) = struct('Note',['PostStimSilence , ' Names{index}],...
%    'StartTime',PreStimSilence+Duration, 'StopTime',PreStimSilence+Duration+PostStimSilence,'Trial',[]);

