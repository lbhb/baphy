function o = ObjUpdate (o);
%
% piggyback on top of speech object to get waveform
global FORCESAMPLINGRATE SPNOISE_EMTX


SamplingRate = get(o,'SamplingRate');
BaseSound=strtrim(get(o,'BaseSound'));
Subsets = get(o,'Subsets');
SetSizeMult = get(o,'SetSizeMult');
CoherentFrac = get(o,'CoherentFrac');
SingleBandFrac = get(o,'SingleBandFrac');
ShuffleOnset=get(o,'ShuffleOnset');  % if 1, randomly rotate stimulus waveforms in time
RepIdx=get(o,'RepIdx');
Duration=get(o,'Duration');  % if 1, randomly rotate stimulus waveforms in time

%error checking
if o.ModDepth < 0 || o.ModDepth > 1
    error('ModDepth must be between 0 and 1')
end

object_spec = what(class(o));
envpath = [object_spec(1).path filesep 'envelope' filesep];

if Subsets==1 && ~strcmp(BaseSound,'Synthetic')
    % eg "Speech.subset1.mat"   and "Speech1"
    str='';
    %str='_H_LPfilt100HzN6_2x_shift';
    str='_H_LPfilt50HzN6_filtfilt2x';
    EnvFileName=[envpath BaseSound '.subset',num2str(Subsets),str,'.mat'];
    EnvVarName=[BaseSound,num2str(Subsets)];
    load(EnvFileName);
elseif isequal(Subsets,'6old') || Subsets==6 || strcmp(BaseSound,'Synthetic')
    EnvFileName=[envpath BaseSound '.subset',num2str(Subsets),'.mat'];
    EnvVarName=[BaseSound,num2str(Subsets)];
    try
        if o.LPfilt_N==2
            EnvVarName=[EnvVarName '_N2'];
            EnvFileName=[EnvFileName(1:end-4) '_N2.mat'];
        end
    catch
        aa=2;
    end
        Env=load(EnvFileName,'emtx','MaxIndex','fs','Names');
    if isequal(Subsets,'6old')
        Subsets=6;
    end
else
    EnvFileName=[envpath BaseSound '.subset',num2str(Subsets),'.mat'];
    EnvVarName=[BaseSound,num2str(Subsets)];
    load(EnvFileName); 
end

saveseed=rng;%saveseed=rand('seed');
rng('default');
rng(Subsets*20);%rand('seed',Subsets*20);
bandcount=length(o.F0s);
if o.Sub_Subset==0
    if bandcount>1,
        MaxIndex=round(Env.MaxIndex.*SetSizeMult);
        idxset=repmat((1:Env.MaxIndex)',[ceil(SetSizeMult) bandcount]);
        ShuffledOnsetTimes=zeros(MaxIndex,bandcount);
        coherentcount=round(MaxIndex.*CoherentFrac);
        incoherentset=1:(size(idxset,1)-coherentcount);
        for jj=2:bandcount,
            kk=0;
            % slight kludge, keep shuffling until there are no matches between
            % index values in columns 1 and column jj, except where intended.
            while sum(idxset(incoherentset,1)==idxset(incoherentset,jj))>0 && kk<20,
                ff=find(idxset(incoherentset,1)==idxset(incoherentset,jj));
                if length(ff)==1;
                    ff=union(ff,[1;2]);
                end
                idxset(ff,jj)=shuffle(idxset(ff,jj));
                kk=kk+1;
            end
        end
        MaxIncoherent=MaxIndex-coherentcount;
        idxset=idxset([1:MaxIncoherent (end-coherentcount+1):end],:);
        
        % shift signals in the different bands randomly to avoid correlations
        % from average cadence of sentences
        if ShuffleOnset,
            if ShuffleOnset==1,
                d=Duration;
            elseif ShuffleOnset==2,
                % don't make it depend on the stimulus length!!!!
                % fixes bug in MultiRefTar that changes reference Duration
                d=3;
            end
            ShuffledOnsetTimes=floor(rand(size(ShuffledOnsetTimes))*d*2)./2;
            ShuffledOnsetTimes(MaxIncoherent+1:end,2:end)=...
                repmat(ShuffledOnsetTimes(MaxIncoherent+1:end,1),[1 bandcount-1]);
        end
        
        if bandcount==3
            idxset=[
                1 2 1;
                2 3 2;
                3 4 3;
                4 5 4;
                5 1 5;
                2 1 1;
                3 2 2;
                4 3 3;
                5 4 4;
                1 5 5;
                ];
            MaxIndex=10;
            ShuffledOnsetTimes=zeros(size(idxset));
        end
        
        % set some ids to zero so that only one band has sound
        if SingleBandFrac>0,
            SingleBandCount=round(MaxIndex.*SingleBandFrac./bandcount);
            SingleBandIdx=repmat(1:SingleBandCount,[bandcount+1 1]);
            LeftoverIdx=SingleBandCount+(1:(MaxIndex-length(SingleBandIdx(:))));
            idxset=idxset([SingleBandIdx(:);LeftoverIdx(:)],:);
            ShuffledOnsetTimes=ShuffledOnsetTimes([SingleBandIdx(:);LeftoverIdx(:)],:);
            for kk=1:SingleBandCount,
                for jj=1:bandcount,
                    idxset((kk-1)*(bandcount+1)+jj+1,[1:(jj-1) (jj+1):end])=0;
                end
            end
        end
        dbScale=[zeros(size(idxset,1),1),o.RelAttenuatedB(1)*ones(size(idxset,1),bandcount-1)];
    else
        MaxIndex=Env.MaxIndex;
        idxset=(1:MaxIndex)';
        ShuffledOnsetTimes=zeros(size(idxset));
        dbScale=zeros(size(idxset));
    end
elseif o.Sub_Subset==1
    class_len=20;
    idxset=[(1:class_len)' zeros(class_len,1);
        zeros(class_len,1) (1:class_len)';
        ];
    dbScale=[zeros(class_len,2);
        zeros(class_len,2);
        ];
    repinds=0:class_len:(class_len*3);
    for i=1:length(o.RelAttenuatedB)-1
        dbScale=[dbScale;
            zeros(class_len,1) -1*o.RelAttenuatedB(i)*ones(class_len,1);
            zeros(class_len,1) -1*o.RelAttenuatedB(i)*ones(class_len,1);
            ];
        if(mod(i,2)==1)
            ssi=[5 15];%second start inds
        else
            ssi=[10 10];
        end
        idxset=[idxset;
            ([1:4 ssi(1)+(0:15)])' ([1:4 ssi(1)+(0:15)])';
            ([1:4 ssi(2)+(0:15)])' ([2 1 4 3 ssi(2)+([1:15 0])])';
            ];
    end
    ShuffledOnsetTimes=zeros(size(idxset));
elseif o.Sub_Subset==2
     idxset=[
         0 1;
         1 0;
         1 1;
         1 2;
         2 1];
    ShuffledOnsetTimes=zeros(size(idxset));
    dbScale=zeros(size(idxset));
else
    error('Sub_Subset %d not defined',o.Sub_Subset)
end

% if length(RepIdx)>=2 && RepIdx(1)>0 && RepIdx(2)>1,
%     RepSet=1:RepIdx(1);
%     RepCount=RepIdx(2);
%     idxset=cat(1,repmat(idxset(RepSet,:),[RepCount 1]),...
%                idxset((RepIdx(1)+1):end,:));
%     ShuffledOnsetTimes=cat(1,repmat(ShuffledOnsetTimes(RepSet,:),[RepCount 1]),...
%                ShuffledOnsetTimes((RepIdx(1)+1):end,:));
%     MaxIndex=size(idxset,1);
% end
if length(RepIdx)>=3 && RepIdx(1)>0 && RepIdx(2)>1,
    RepSet=repmat(1:RepIdx(1),length(repinds),1)+repmat(repinds',1,RepIdx(1));
    RepSet=[RepSet; RepIdx(1)+1];
    RepSet=RepSet(:)';
    %RepSet=[1:RepIdx(1) (1:RepIdx(1))+10 (1:RepIdx(1))+20 (1:RepIdx(1))+40];
    RepCount=RepIdx(2)-1;
    idrep=repmat(idxset(RepSet,:),[RepCount 1]);
    dbrep=repmat(dbScale(RepSet,:),[RepCount 1]);
    SOTrep=repmat(ShuffledOnsetTimes(RepSet,:),[RepCount 1]);
elseif length(RepIdx)>=2 && RepIdx(1)>0 && RepIdx(2)>1,
    RepSet=repmat(1:RepIdx(1),length(repinds),1)+repmat(repinds',1,RepIdx(1));
    RepSet=RepSet(:)';
    %RepSet=[1:RepIdx(1) (1:RepIdx(1))+10 (1:RepIdx(1))+20 (1:RepIdx(1))+40];
    RepCount=RepIdx(2)-1;
    idrep=repmat(idxset(RepSet,:),[RepCount 1]);
    dbrep=repmat(dbScale(RepSet,:),[RepCount 1]);
    SOTrep=repmat(ShuffledOnsetTimes(RepSet,:),[RepCount 1]);
end
if length(o.RemoveIdx)>=0 && o.RemoveIdx>0
    rmi=o.RemoveIdx:class_len;
    Ntypes=size(idxset,1)/class_len;
    rmi=repmat(rmi',1,Ntypes)+repmat((0:class_len:(Ntypes-1)*class_len),length(rmi),1);
    rmi=rmi(:)';
    idxset(rmi,:)=[];
    dbScale(rmi,:)=[];
    ShuffledOnsetTimes(rmi,:)=[];
end
if length(RepIdx)>=2 && RepIdx(1)>0 && RepIdx(2)>1,
    idxset=[idxset; idrep];
    dbScale=[dbScale; dbrep];
    ShuffledOnsetTimes=[ShuffledOnsetTimes; SOTrep];
end

MaxIndex=size(idxset,1);

SPNOISE_EMTX.(EnvVarName)=Env.emtx;
resampled_name=[EnvVarName,'_resampled_',num2str(o.SamplingRate)];
if o.Gauss_filter_FWHM~=0
    resampled_name=[resampled_name,'_GaussFWHM_',num2str(o.Gauss_filter_FWHM)];
end
if ~isfield(SPNOISE_EMTX,resampled_name)
    if o.Gauss_filter_FWHM~=0
        fprintf('**smoothing\n')
        sigma=o.Gauss_filter_FWHM/1000/(2*sqrt(2*log(2)))*o.SamplingRateEnv;
        E=gsmooth(Env.emtx,sigma);
        E=E/max(E(:));
    else
        E=Env.emtx;
    end
    fprintf('**resampling\n')
    SPNOISE_EMTX.(resampled_name)=resample(E,o.SamplingRate,o.SamplingRateEnv);
end
o.mean=nanmean(Env.emtx(:));
%RMS is 0.2984 (.2931 on Alpaca??). m=1 envelope (0 to 1 sine wave) RMS is
%.6124. 20*log10(0.6124/0.2984)=6.2443 20*log10(0.6124/0.2931)=6.4004
%o=set(o,'emtx',Env.emtx);
o=set(o,'idxset',idxset);
o=set(o,'dbScale',dbScale);
o=set(o,'ShuffledOnsetTimes',ShuffledOnsetTimes);
o=set(o,'MaxIndex',MaxIndex);
% removed phoneme labels to save space in parmfiles
%o=set(o,'Phonemes',Env.Phonemes);
o=set(o,'SamplingRateEnv',Env.fs); 

Names=cell(1,MaxIndex);
for ii=1:MaxIndex,
    Names{ii}=['T'];
    for bb=1:bandcount,
        if idxset(ii,bb)
            Names{ii}=[Names{ii} '+' Env.Names{idxset(ii,bb)}];
        else
            Names{ii}=[Names{ii} '+null'];
        end
    end
end

o = set(o,'Names',Names);

rng(saveseed)