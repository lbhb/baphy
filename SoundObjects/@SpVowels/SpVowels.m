function o = SpVowels(varargin)
% SpVowels is based on SpTones object, but the carrier is a synthetic vowel
%
% properties:
%   PreStimSilcence
%   PostStimSilence
%   SamplingRate
%   Loudness
%   Subsets: can be 1, 2, 3 or 4. each contains 30 different sentences from
%       Timit database. each subset is 30 sentences spoken by 30 different
%       speakers (15 male 15 female). sentences in subset 1 and 4 are three
%       seconds long, subset 2 and 3 are four seconds. Subset 4 is all the
%       same sentence: "She had your dark suit in greasy wash water all
%       year"
%   Phonemes: contains the phoneme events for the specified names.
%   Words: contains the word events for the specified names.
%   Sentences: contains the sentece events for the specified names.
% 
% methods: waveform, LabelAxis, set, get
% 

% Nima Mesgarani, Oct 2005

switch nargin
case 0
    % if no input arguments, create a default object
    s = SoundObject ('SpVowels', 100000, 0, 0, 0, {}, 1, ...
                     {'F0s','edit',100,...
                      'RelAttenuatedB','edit',0,...
                      'FormantsA','edit',[900 1200 2440],...
                      'FormantsB','edit',[270 2290 3010],...
                      'SplitChannels','popupmenu','No|Yes',...
                      'BaseSound','popupmenu','Speech|FerretVocal|Synthetic',...
                      'Subsets','edit',1,...
                      'Sub_Subset','edit',0,...
                      'LPfilt_N','edit',6,...
                      'ShuffleOnset','edit',0,...
                      'SetSizeMult','edit',2,...
                      'MaskStart','edit',[0 0],...
                      'CoherentFrac','edit',0.1,...
                      'SingleBandFrac','edit',0,...
                      'BaselineFrac','edit',0,...
                      'ModDepth','edit',1,...
                      'Gauss_filter_FWHM','edit',1,...
                      'ForceTargetEnvelope','popupmenu','No|Coherent|Incoherent',...
                      'RepIdx','edit',[0 1],...
                      'RemoveIdx','edit',0,...
                      'Duration','edit',3});
    o.F0s = [100 160];
    o.RelAttenuatedB=[0 0];
    o.FormantsA=[900 1200 2440];
    o.FormantsB=[270 2290 3010];
    o.SplitChannels='No';
    o.BaseSound = 'Speech';
    o.Subsets = 1;
    o.Sub_Subset = 0;
    o.LPfilt_N = 6;
    o.SNR = 1000;
    o.ShuffleOnset=0;  % if 1, randomly rotate stimulus waveforms in time
    o.SetSizeMult=2;  % for multi-channel stim, how many times larger should MaxIndex be than the original set
    o.MaskStart=[0 0];
    o.CoherentFrac=0.1;  % for multi-channel stim, what fraction should be the same in all channels
    o.SingleBandFrac=0;  % for multi-channel stim, what fraction should be the same in all channels
    o.BaselineFrac=0;  % minimum sound level (as a fraction of peak)
    %o.Phonemes = {struct('Note','','StartTime',0,'StopTime',0)};
    o.ModDepth=1; % peak-to-peak of envelope. (center of range is set to .5).
    % Generally, use this with BaselineFrac=0 and use OveralldB to set level.    %o.Phonemes = {struct('Note','','StartTime',0,'StopTime',0)};
    %o.Words= {struct('Note','','StartTime',0,'StopTime',0)};    
    %o.Sentences = {''};
    o.ForceTargetEnvelope='No';
    o.emtx=[];
    o.idxset=[];
    o.ShuffledOnsetTimes=[];
    o.SamplingRateEnv=2000;
    o.RemoveIdx=0;
    o.Duration = 3;
    o.Gauss_filter_FWHM=0;
    o.SamplingRate=100000;
    o.CueRefType='Inc';
    o.RepIdx=[0 1];
    o.mean=[];
    o.dbScale=[];
    % 128 ms is a lot but still some modulation
    % 4-8 ms is about what 50 Hz, N=6 Cheby LP is (what we've been doing).
    o = class(o,'SpVowels',s);
    o = ObjUpdate(o);
    
case 1
    % if single argument of class SoundObject, return it
    if isa(varargin{1},'SpVowels')
        o = varargin{1};
    else
        error('Wrong argument type');
    end
    
case 7
    error('SpeechPhoneme format not supported for this object');
    
otherwise
    error('Wrong number of input arguments');
end