function [e,event]=env(o,index,IsRef,TarObject,ThisTarParams)
% function w=env(o, index,IsRef);
%
% generate envelope for SpNoise object
%
global SPNOISE_EMTX

if(nargin<4)
    TarObject=[];
end

F0s=get(o,'F0s');
RelAttenuatedB=get(o,'RelAttenuatedB');
SamplingRate = get(o,'SamplingRate');
Duration = get(o,'Duration'); % duration is second
PreStimSilence = get(o,'PreStimSilence');
PostStimSilence = get(o,'PostStimSilence');
Names = get(o,'Names');
BaseSound=strtrim(get(o,'BaseSound'));
Subsets = get(o,'Subsets');
BaselineFrac=get(o,'BaselineFrac');
MaskStart=get(o,'MaskStart');

EnvVarName=[BaseSound,num2str(Subsets)];
%emtx = get(o,'emtx');
if 0
    emtx = SPNOISE_EMTX.(EnvVarName);
    SamplingRateEnv = get(o,'SamplingRateEnv');
else
    if o.LPfilt_N==2
        EnvVarName=[EnvVarName '_N2'];
    end
    EnvVarName=[EnvVarName,'_resampled_',num2str(o.SamplingRate)];
    if o.Gauss_filter_FWHM~=0
        EnvVarName=[EnvVarName,'_GaussFWHM_',num2str(o.Gauss_filter_FWHM)];
    end
    emtx = SPNOISE_EMTX.(EnvVarName);
    SamplingRateEnv = get(o,'SamplingRate');
end

timesamples = (1 : round(Duration*SamplingRate))' / SamplingRate;


% force same carrier signal each time!!!!
%not needed for env?
%saveseed=rand('seed');
%rand('seed',index*20);

if isempty(RelAttenuatedB) || length(RelAttenuatedB)<length(F0s),
  RelAttenuatedB=zeros(size(F0s));
end
%create empty event structure
event=struct('Note','','StartTime',0,'StopTime',0,'Trial',[]);event(1)=[];
bandcount=min([length(F0s),length(RelAttenuatedB)]);
idxset=get(o,'idxset');
ShuffledOnsetTimes=get(o,'ShuffledOnsetTimes');
e=zeros(length(timesamples),bandcount);
%ensure that target band is created first if there is a target, so it can
%be used if o.ForceTargetEnvelope is 'Coherent'
bandorder=1:bandcount;
if ~isempty(TarObject)
    Ntar=length(ThisTarParams.TargetChannel);
    bandorder=[ThisTarParams.TargetChannel(1) setdiff(bandorder,ThisTarParams.TargetChannel(1))];
    if MaskStart(2)>0 && MaskStart(1)==0
        MaskStart(1)=bandorder(2);
        if length(bandorder)>2
            error('This isn''t set up for 3 streams')
        end
    end
end
fprintf('Index=[%d,%d]\n',idxset(index,:))
idx=idxset(index,:);
impose_correlation = any(mod(idxset(index,:),1)~=0);
if impose_correlation
    idx = floor(idx);
    corr = mod(idxset(index,mod(idxset(index,:),1)~=0),1);
    corr = round(corr*1000)/1000;
end
for bb=bandorder,
    if idxset(index,bb)==0
        if ~isempty(TarObject) && any(ThisTarParams.TargetChannel==bb)
            %At least one of the targets is a change in envelope on this
            %channel. Don't continue so target can be applied (even though
            %enevelope is zero for this channel
        else
            continue
        end
    end
    tw=zeros(length(timesamples),1);
    
    % extract appropriate envelope and resample to match desired output fs
    if idxset(index,bb)==0
        sp=e(:,bb);
    else
        sp=emtx(:,idx(bb));
    end
    sp=sp(1:find(~isnan(sp), 1, 'last' ));
    % repeat envelope if Duration longer than orginal waveform
    % (3-sec, typically)
    segrepcount=ceil(Duration ./ (length(sp)./SamplingRate));
    sp=repmat(sp,[segrepcount 1]);
    if ShuffledOnsetTimes(index,bb)>0,
        sp=shift(sp,round(ShuffledOnsetTimes(index,bb)*SamplingRateEnv));
    end
    if BaselineFrac>0 && BaselineFrac<1,
        sp=sp.*(1-BaselineFrac) + BaselineFrac;
    end
%     if o.Gauss_filter_FWHM~=0
%         sigma=o.Gauss_filter_FWHM/1000/(2*sqrt(2*log(2)))*SamplingRateEnv;
%         sp=gsmooth(sp,sigma);
%         mv=interp1(SPNOISE_EMTX.gauss_smooth_FWHMs,SPNOISE_EMTX.max_after_gauss_smooth,o.Gauss_filter_FWHM/1000);
%         if isnan(mv)
%             error('norm factor mv is NaN!')
%         end
%         sp=sp/mv;
%     end
    if o.ModDepth<1 && o.ModDepth>0 && idxset(index,bb)~=0
       sp= sp.*o.ModDepth + o.mean*(1-o.ModDepth);%sets mean to stay at overall mean of all envelopes (o.mean)
       %sp= sp.*o.ModDepth + 0.5*(1-o.ModDepth);%sets mean of range to stay at 0.5
    end
    sp=resample(sp,SamplingRate,SamplingRateEnv);
    
        
    % make sure envelope and noise have same duration
    if length(sp)<length(tw),
        disp('desired Duration too long, trimming!!!');
        [length(sp) length(tw)]
        tw=tw(1:length(sp));
    elseif length(sp)>length(tw),
        sp=sp(1:length(tw));
    end
    
    if MaskStart(2)>0 && MaskStart(1)==bb
        if length(bandorder)>2
            error('This isn''t set up for 3 streams')
        end
        sp(1:round(SamplingRate*MaskStart(2)))=0;
    end
    
    if impose_correlation
        if bb==bandorder(1)
            sp1=sp;
        elseif bb==bandorder(2)
            means=mean([sp1 sp]);
            env_orthog=UTgs_orthog(sp1-means(1),sp-means(2))+means(2);
            sp=corr.*sp1+sqrt(1-corr.^2).*env_orthog;
        else
            error('Why are we here?')
        end
    end
    
    if ~isempty(TarObject)
        %target is a change in envelope. Apply it here before multipying by carrier
        for tari=1:Ntar
            if isnan(ThisTarParams.TargetChannel(tari)), continue; end
            if ThisTarParams.TargetChannel(tari)==bb
                TarPreStim=get(TarObject,'PreStimSilence');
                TarPostStim=get(TarObject,'PostStimSilence');
                TarObject=set(TarObject,'PreStimSilence',0);
                TarObject=set(TarObject,'PostStimSilence',0);
                [tarenv,tarevent]=env(TarObject,ThisTarParams.ThisTarIdx(tari), 0);%envelope comes out from 0 to 1 for ModDepth=1.
                TarAddTime=PreStimSilence+TarPreStim+ThisTarParams.TarStartBin(tari)/SamplingRate;
                for cnt2 = 1:length(tarevent)
                    tarevent(cnt2).Note = [tarevent(cnt2).Note ' , Target'];
                    tarevent(cnt2).StartTime = tarevent(cnt2).StartTime + TarAddTime;
                    tarevent(cnt2).StopTime = tarevent(cnt2).StopTime + TarAddTime;
                end
                tarevent(1).StartTime=tarevent(1).StartTime-TarPreStim;
                tarevent(3).StopTime=tarevent(end).StopTime+TarPostStim;
                event(length(event)+(1:length(tarevent)))=tarevent;
                tarenv=tarenv*10^(ThisTarParams.RelativeTarRefdB(tari)/20);%Scale peak of envelope (usually to RMS-match).
                tarinds=ThisTarParams.TarStartBin(tari)+round(TarPreStim*SamplingRate)+(1:length(tarenv));
                EnvTarRefRatio=ThisTarParams.EnvTarRefRatio(tari);
                RampDur=ThisTarParams.EnvTarRefRampDur(tari);
                TRRramp=EnvTarRefRatio*ones(size(tarenv));
                sinePeriod = 2*RampDur;
                npts = sinePeriod*o.SamplingRate;
                sineFrequency = 1/sinePeriod;
                
                sineBuffer = -cos(2*pi*sineFrequency*(0:npts-1)/o.SamplingRate);
                sineBuffer = EnvTarRefRatio*(sineBuffer + 1)/2;
                N = round(length(sineBuffer)/2);
                N2=length(sineBuffer)-N;
                TRRramp(1:N)=sineBuffer(1:N);
                TRRramp(end-N2+1:end)=sineBuffer(N+1:end);
                switch o.ForceTargetEnvelope
                    case 'No        '
                        %do nothing
                    case 'Coherent  '
                        spTar=sp;
                        if Ntar>1, error('Fix me'); end
                end
                sp(tarinds)=TRRramp.*tarenv+(1-TRRramp).*sp(tarinds);
                if(0)
                    t=(1:length(sp))/SamplingRate*1000;
                    figure;plot(t,sp);
                    hold on;plot(t(tarinds),TRRramp,'k');
                    ylim([0 1])
                end
            else
                switch o.ForceTargetEnvelope
                    case 'No        '
                        %do nothing
                    case 'Coherent  '
                        %force non-target streams to be coherent with target
                        %stream during the target period for PSTH analysis
                        nf=max(TRRramp);
                        if nf==0, nf=1; end
                        TCramp=TRRramp./nf;%use ramp used for tar:ref ratio, but set tar:ref ratio to 1.
                        sp(tarinds)=TCramp.*spTar(tarinds)+(1-TCramp).*sp(tarinds);
                        if(0)
                            t=(1:length(sp))/SamplingRate*1000;
                            figure;plot(t,[sp,sp2,spTar]);
                            hold on;plot(t(tarinds),TRRramp,'k');
                            ylim([0 1])
                        end
                end
            end
        end
    end
    
    % apply envelope
    tw(:)=sp;
    
    % adjust level relative to other bands
    level_scale=10.^(-RelAttenuatedB(bb)./20);
    e(:,bb)=tw.*level_scale;
    
    % 10ms ramp at onset and offset:
    ramp = hanning(round(.01 * SamplingRate*2));
    ramp = ramp(1:floor(length(ramp)/2));
    e(1:length(ramp),bb) = e(1:length(ramp),bb) .* ramp;
    e(end-length(ramp)+1:end,bb) = e(end-length(ramp)+1:end,bb) .* flipud(ramp);
end

%e=e./max(abs(e(:)));

% Now, put it in the silence:
e = [zeros(PreStimSilence*SamplingRate,bandcount) ; e ;zeros(PostStimSilence*SamplingRate,bandcount)];

% generate the event structure:
if ~isempty(TarObject)
    coh_str=get_target_suffixes(o,index);
    for i=1:length(event)
        comma_inds=strfind(event(i).Note,',');
        event(i).Note=[event(i).Note(1:comma_inds(2)-2),':',coh_str{1},event(i).Note(comma_inds(2)-1:end)];
    end  
end
thisevent(1) = struct('Note',['PreStimSilence , ' Names{index}],...
    'StartTime',0,'StopTime',PreStimSilence,'Trial',[]);
thisevent(2) = struct('Note',['Stim , ' Names{index}],'StartTime'...
    ,PreStimSilence, 'StopTime', PreStimSilence+Duration, 'Trial',[]);
thisevent(3) = struct('Note',['PostStimSilence , ' Names{index}],...
    'StartTime',PreStimSilence+Duration, 'StopTime',PreStimSilence+Duration+PostStimSilence,'Trial',[]);
event=[thisevent event];%if targets exist, put them at the end to keep convention
% return random seed to previous state
%not needed for env?
%rand('seed',saveseed);


