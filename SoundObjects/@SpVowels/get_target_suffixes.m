function coh_str=get_target_suffixes(o,varargin)
%(o,indexes,o_struct)
indexes=getparmC(varargin,1,[]);
o_struct=getparmC(varargin,2,[]);
if ~isempty(o_struct)
    idxset=o_struct.idxset;
else
    idxset=o.idxset;
end
if isempty(indexes)
    do_unique=true;
    indexes=1:size(idxset,1);
else
    do_unique=false;
end
for k=1:length(indexes)
    coh_str{k}=[];
    for i=1:size(idxset,2)
        for j=i+1:size(idxset,2)
            if size(idxset,2) == 2
                str='';
            else
                str=sprintf('S%d-%d',i,j);
            end
            if idxset(indexes(k),i)==idxset(indexes(k),j)
                coh_str{k}=[coh_str{k},[str,'Coh ']];
            elseif idxset(indexes(k),j)==0
                coh_str{k}=[coh_str{k},[str,'SgA ']];
            elseif idxset(indexes(k),i)==0
                coh_str{k}=[coh_str{k},[str,'SgB ']];
            else
                coh_str{k}=[coh_str{k},[str,'Inc ']];
            end
        end
    end
    coh_str{k}(end)=[];
end
if do_unique
    coh_str=unique(coh_str);
end
end