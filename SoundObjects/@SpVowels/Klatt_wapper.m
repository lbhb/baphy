function wav=Klatt_wapper(o,dur,F0,SR,formants)

addpath('U:\luke\Code\Klatt_Synthesizer')
defPars.DU=dur*1000;
defPars.F0=F0*10;
if SR>20000
    defPars.SR=20000;
else
    defPars.SR=SR;
end
defPars.OQ=70;
defPars.B3=70;
defPars.NF=3;
defPars.F1=formants(1);
defPars.F2=formants(2);
defPars.F3=formants(3);
[wav,sRate] = mlsyn(defPars);
if SR>20000
    wav=resample(double(wav),SR,20000);
end

