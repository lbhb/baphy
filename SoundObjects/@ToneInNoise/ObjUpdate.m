function o = ObjUpdate (o);
% This function is used to recalculate the properties of the object that
% depends on other properties. In these cases, user firs changes the
% properties using set, and then calls ObjUpdate. The default is empty, you
% need to write your own.

% SVD 2020-06-30
global FORCESAMPLINGRATE;

par = get(o);

if isempty(FORCESAMPLINGRATE)
   if par.HighFreq > 16000
      o = set(o,'SamplingRate',100000);
   else
      o = set(o,'SamplingRate',100000);
   end
end

NB = NoiseBurst;
NB = set(NB, 'PreStimSilence', par.PreStimSilence);
NB = set(NB, 'PostStimSilence', par.PostStimSilence);
NB = set(NB, 'Duration', par.Duration);
NB = set(NB, 'LowFreq', par.LowFreq);
NB = set(NB, 'HighFreq', par.HighFreq);
NB = set(NB, 'Count', par.Count);
NB = set(NB, 'Bandwidth', par.Bandwidth);
NB = set(NB, 'NoiseBand', par.NoiseBand);
NB = set(NB, 'Frozen', par.Frozen);
NB = set(NB, 'TonesPerBurst', par.TonesPerBurst);
NB = set(NB, 'SimulCount', par.SimulCount);
NB = set(NB, 'RefRepCount', par.RefRepCount);
NB = set(NB, 'SamplingRate', par.SamplingRate);
NB = ObjUpdate(NB);

CenterFreqs = get(NB, 'CenterFrequencies');
uToneFreqs = CenterFreqs(par.ToneBands);

T = Tone();
T = set(T, 'PreStimSilence', par.PreStimSilence);
T = set(T, 'PostStimSilence', par.PostStimSilence);
T = set(T, 'Frequencies', uToneFreqs);
T = set(T, 'Duration', par.Duration);
T = set(T, 'SamplingRate', par.SamplingRate);
T = ObjUpdate(T);

% three ways of specifying tone/snr combos
par.SNRs = ifstr2num(par.SNRs);
if ~isempty(par.SNRtile)
   par.SNRtile = ifstr2num(par.SNRtile);
   SNRs = repmat(par.SNRtile(:),[length(uToneFreqs),1]);
   ToneFreqs = repmat(uToneFreqs(:)',[length(par.SNRtile), 1]);
   ToneBands = repmat(par.ToneBands(:)',[length(par.SNRtile), 1]);
   ToneIndexes = repmat((1:length(uToneFreqs)),[length(par.SNRtile), 1]);
elseif (length(par.SNRs) < length(uToneFreqs))
   SNRs = ones(1,length(uToneFreqs)) * par.SNRs(1);
   ToneBands=par.ToneBands;
   ToneFreqs = uToneFreqs;
   ToneIndexes = 1:length(uToneFreqs);
else
   SNRs = par.SNRs;
   ToneBands=par.ToneBands;
   ToneFreqs = uToneFreqs;
   ToneIndexes = 1:length(uToneFreqs);
end

if isempty(par.ToneFixedBand)
   stimdata=[ToneBands(:) ToneIndexes(:) SNRs(:)];
else
   ToneFixedBand = ifstr2num(par.ToneFixedBand);
   FixedToneIdx=find(par.ToneBands==ToneFixedBand(1),1, 'first');
   stimdata=[ToneBands(:) ones(size(ToneIndexes(:)))*FixedToneIdx SNRs(:)];
end

MaxIndex = size(stimdata,1);

Names=cell(1,MaxIndex);
for ii=1:MaxIndex
   Names{ii}=sprintf('%d+%ddB+Noise', ToneFreqs(ii), SNRs(ii));
end

if MaxIndex>1 && length(unique(Names)) < length(Names)
    for ii=1:MaxIndex
        Names{ii}=sprintf('%s:N%d',Names{ii},ii);
    end
end

o = set(o, 'NoiseBurst', NB);
o = set(o, 'Tone', T);
o = set(o, 'MaxIndex', MaxIndex);
o = set(o, 'ToneFreqs', ToneFreqs);
o = set(o, 'Names', Names);
o = set(o, 'stimdata', stimdata);
