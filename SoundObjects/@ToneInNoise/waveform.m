function [w, event]=waveform(o,index,IsRef);
% function w=waveform(t);
% this function is the waveform generator for object NoiseBurst
%
% created SVD 2007-03-30

event = [];

NB = get(o,'NoiseBurst');
T = get(o,'Tone');
SNRs = get(o,'SNRs');
MaxIndex = get(o,'MaxIndex');
Names = get(o, 'Names');
PreStimSilence = get(o, 'PreStimSilence');
Duration = get(o, 'Duration');
PostStimSilence = get(o, 'PostStimSilence');
stimdata = get(o, 'stimdata');


wNoise = waveform(NB, stimdata(index,1));

wTone = waveform(T, stimdata(index,2));
SNR = stimdata(index,3);
if isinf(SNR) && SNR>0
   w=wTone;
   disp('tone only')
elseif isinf(SNR)
   w=wNoise;
   disp('noise only');
else
   wTone = wTone * 10^(SNR/20);
   w = wNoise + wTone;
end


% and generate the event structure:
event = struct('Note',['PreStimSilence , ' Names{index}],...
    'StartTime',0,'StopTime',PreStimSilence,'Trial',[]);
event(2) = struct('Note',['Stim , ' Names{index}],'StartTime'...
    ,PreStimSilence, 'StopTime', PreStimSilence+Duration,'Trial',[]);
event(3) = struct('Note',['PostStimSilence , ' Names{index}],...
    'StartTime',PreStimSilence+Duration, 'StopTime',PreStimSilence+Duration+PostStimSilence,'Trial',[]);
