function o = ToneInNoise(varargin)
% Tone in NoiseBurst. Combines Tone and NoiseBurst sound objects, with
% exact alignment with NoiseBurst 
%
% Run class: TBP
%
% A set of Count bandpass noise signals, center frequencies spaced
% logorithmically from LowFreq to HighFreq.  Bandwidth 1.0 fills in space
% exactly. Values>1 cause overlap between adjacent bursts, <1 leave space
% in between.  SimulCount is how many bursts to include in each instance.
% So MaxIndex is Count^SimulCount.
%
% Properties: TBD
%
% usage:
% To create a ToneInNoise with default values.
%   t = ToneInNoise;
%
% To get the waveform and events:
%   [w, events] = waveform(t);  
%
% methods: set, get, waveform, ObjUpdate

% SVD created 2007-08-05, based on ComplexChord object.

switch nargin
case 0
    % if no input arguments, create a default object
    s = SoundObject ('ToneInNoise', 100000, 0, 0, 0, ...
        {''}, 1, {'Duration','edit',1,...
        'LowFreq','edit',250,...       
        'HighFreq','edit',8000,...       
        'Count','edit',15,...
        'Bandwidth','edit', 1,...
        'Frozen', 'popupmenu', 'No|Yes',...
        'ToneBands','edit', [7],...
        'ToneFixedBand','edit','',...
        'SNRs','edit', [0],...
        'SNRtile','edit', [],...
        'RefRepCount','edit',1});
    o.Duration = 1;
    o.LowFreq = 250;
    o.HighFreq = 8000;
    o.Count=15;
    o.ToneBands = [7];
    o.ToneFixedBand = [];
    o.SNRs = [0];
    o.SNRtile = [];
    o.Bandwidth=1;   %SVD implemented 2020-08-07. allow the bands to not tightly tile freuqency 
    o.NoiseBand= 1;  % unused, pass to NoiseBurst
    o.Frozen = 'No'; % used, pass to NoiseBurst
    o.TonesPerBurst=20;  % unused, pass to NoiseBurst
    o.SimulCount=1; % unused, pass to NoiseBurst
    o.RefRepCount=1; % unused, pass to NoiseBurst
    o.ToneFreqs = 0;
    o.NoiseBurst = [];
    o.Tone = [];
    o.RefRepCount=1;
    o.stimdata = [];
    o = class(o,'ToneInNoise',s);
    o = ObjUpdate (o);
case 1
    % if single argument of class SoundObject, return it
    if isa(varargin{1},'SoundObject')
        o = varargin{1};
    else
        error('Wrong argument type');
    end
        
otherwise
    error('Wrong number of input arguments');
end