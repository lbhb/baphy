function [e, event]=env(o,index,SamplingRate)
% function w=env(o,index);
% this function is the envelop generator for object NoiseBurst
%
% created SVD 2017-10-31 - Happy Halloween!
%
event = [];

LowFreq=get(o,'LowFreq');
HighFreq=get(o,'HighFreq');
Count=get(o,'Count');
SimulCount=get(o,'SimulCount');
TotalCount=Count.^SimulCount;
if ~exist('SamplingRate','var'),
   SamplingRate = get(o,'SamplingRate');
end
Duration = get(o,'Duration'); % duration is second
PreStimSilence = get(o,'PreStimSilence');
PostStimSilence = get(o,'PostStimSilence');
NoiseBand= get(o,'NoiseBand');
TonesPerBurst=get(o,'TonesPerBurst');
Names = get(o,'Names');

if SimulCount==1,
    toneset=index;
elseif SimulCount==2,
    toneset=[mod((index-1),Count)+1 floor((index-1)./Count)+1];
else
    error('Sorry, SimulCount>2 not supported!')
end

timesamples = (1 : round(Duration*SamplingRate))' / SamplingRate;
e=zeros(length(timesamples),Count);

if Count==1 % added by Serin Atiani 8/17/09 to allow for a single noise burst at the target
    logfreq=[log(LowFreq) log(HighFreq)];
    logfreqdiff=log(HighFreq)-log(LowFreq);
    lfstep=logfreqdiff./TonesPerBurst;
else
    logfreq=linspace(log(LowFreq),log(HighFreq),Count);
    logfreqdiff=logfreq(2)-logfreq(1);
    lfstep=logfreqdiff./TonesPerBurst;
end

for ii=toneset,
   e(:,ii)=e(:,ii)+1;
end

% Now, put it in the silence:
e = [zeros(PreStimSilence*SamplingRate,size(e,2)) ; 
   e ;
   zeros(PostStimSilence*SamplingRate,size(e,2))];

% and generate the event structure:
event = struct('Note',['PreStimSilence , ' Names{index}],...
    'StartTime',0,'StopTime',PreStimSilence,'Trial',[]);
event(2) = struct('Note',['Stim , ' Names{index}],'StartTime'...
    ,PreStimSilence, 'StopTime', PreStimSilence+Duration,'Trial',[]);
event(3) = struct('Note',['PostStimSilence , ' Names{index}],...
    'StartTime',PreStimSilence+Duration, 'StopTime',PreStimSilence+Duration+PostStimSilence,'Trial',[]);
