function [e, event]=env(o,index,IsRef);
% function e=env(t);
% this function is the waveform generator for object NoiseBurst
%
% created SVD 2007-03-30

event = [];
LowFreq=get(o,'LowFreq');
HighFreq=get(o,'HighFreq');
AM=get(o,'AM');
ModDepth=get(o,'ModDepth');
FirstSubsetIdx=get(o,'FirstSubsetIdx');
SecondSubsetIdx=get(o,'SecondSubsetIdx');
SecondRelAtten=get(o,'SecondRelAtten');
Count=get(o,'Count');
SamplingRate = get(o,'SamplingRate');
Duration = get(o,'Duration'); % duration is second
PreStimSilence = get(o,'PreStimSilence');
PostStimSilence = get(o,'PostStimSilence');
TonesPerOctave=get(o,'TonesPerOctave');
Names = get(o,'Names');

Frequencies=round(exp(linspace(log(LowFreq),log(HighFreq),Count+1)));
logfreq=log(Frequencies);

if ~isnumeric(FirstSubsetIdx),
    FirstSubsetIdx=str2num(FirstSubsetIdx);
end
if ~isnumeric(SecondSubsetIdx),
    SecondSubsetIdx=str2num(SecondSubsetIdx);
end

if isempty(SecondRelAtten),
   SecondRelAtten=0;
end
if isempty(AM),
   AM=0;
end
if isempty(ModDepth),
   ModDepth=1;
end
if length(ModDepth)<length(AM),
   ModDepth=ones(size(AM)).*ModDepth(1);
end
NBCount=Count*length(AM);

if isempty(FirstSubsetIdx),
   FirstSubsetIdx=1:NBCount;
end

if isempty(SecondSubsetIdx) || ~ismember(SecondSubsetIdx(1),1:NBCount),
   Count1=length(FirstSubsetIdx);
   Count2=0;
   TotalCount=Count1;
else
   Count1=length(FirstSubsetIdx);
   Count2=length(SecondSubsetIdx);
   TotalCount=Count1*Count2;
end

timesamples = (1 : round(Duration*SamplingRate))' / SamplingRate;

% record state of random number generator
s=rand('state');

rand('state',index.*120);
% To randomize the phase
% rand('state',sum(100*clock));

i1=FirstSubsetIdx(mod((index-1),Count1)+1);
i1freq=mod(i1-1,Count)+1;
i1AM=floor((i1-1)./Count)+1;



% normalize each w0 before adding
if AM(i1AM)>0,
    switch o.ModType
        case 'Rectified'
            e=(1-ModDepth(i1AM) + ModDepth(i1AM) .* ...
                abs(sin(pi*AM(i1AM)*timesamples)) ./ 0.4053);
        case 'Sinusoidal'
            e=(1-ModDepth(i1AM) .*cos(2*pi*AM(i1AM)*timesamples))/2;
        case 'Square    '
            e=(1-ModDepth(i1AM) .*cos(2*pi*AM(i1AM)*timesamples))/2;
            e(e>=.5)=1;e(e<.5)=0;
    end
end

if Count2>0,
   i2=SecondSubsetIdx(floor((index-1)./Count1)+1);
   i2freq=mod(i2-1,Count)+1;
   i2AM=floor((i2-1)./Count)+1;
   
   
   if AM(i2AM)>0,
       switch o.ModType
           case 'Rectified'
               e(:,2)=(1-ModDepth(i2AM) + ModDepth(i2AM) .* ...
                   abs(sin(pi*AM(i2AM)*timesamples)) ./ 0.4053);
           case 'Sinusoidal'
               e=(1-ModDepth(i2AM) .*cos(2*pi*AM(i2AM)*timesamples))/2;
       end
   end
   
   if SecondRelAtten,
      e(:,2)=e(:,2).*10.^(-SecondRelAtten./20);
   end
   
end

%normalize to approx min/max of 5V
% attenuate by 5dB to avoid clipping. so 80=75!!!
%e=10.^(-5./20) .* 5.*e;


% return random number generator to previous state
rand('state',s);


% 10ms ramp at onset and offset:
e = e(:);
ramp = hanning(round(o.RampDur * SamplingRate*2));
ramp = ramp(1:floor(length(ramp)/2));
e(1:length(ramp)) = e(1:length(ramp)) .* ramp;
e(end-length(ramp)+1:end) = e(end-length(ramp)+1:end) .* flipud(ramp);

% Now, put it in the silence:
e = [zeros(PreStimSilence*SamplingRate,1) ; e(:) ;zeros(PostStimSilence*SamplingRate,1)];

% and generate the event structure:
event = struct('Note',['PreStimSilence , ' Names{index}],...
    'StartTime',0,'StopTime',PreStimSilence,'Trial',[]);
event(2) = struct('Note',['Stim , ' Names{index}],'StartTime'...
    ,PreStimSilence, 'StopTime', PreStimSilence+Duration,'Trial',[]);
event(3) = struct('Note',['PostStimSilence , ' Names{index}],...
    'StartTime',PreStimSilence+Duration, 'StopTime',PreStimSilence+Duration+PostStimSilence,'Trial',[]);
