function [w,event]= waveform(o,index,IsRef,TarObject,ThisTarParams)
% function w=waveform(t);
% This function is the waveform generator for object ComplexTone
%
% Mai December 2008
if(nargin<4)
    TarObject=[];
end

% Get parameters of ComplexTone object
SamplingRate= get(o,'SamplingRate');
PreStimSilence= get(o,'PreStimSilence');
PostStimSilence= get(o,'PostStimSilence');

ComplexToneDur= get(o,'ComplexToneDur'); % duration in seconds
GapDur= get(o,'GapDur'); % duration in seconds
AnchorFrequency= get(o,'AnchorFrequency');
ComponentsNumber= get(o,'ComponentsNumber');

ComponentRatios= get(o,'ComponentRatios');
FrequencyOrder= get(o,'FrequencyOrder');
Names= get(o,'Names');

% Calculate stimulus parameters
AMFrequency= get(o,'AMFrequency'); % Message frequency in Herz for AM modulation

GapSamples= round(GapDur*SamplingRate);
ComplexToneSamples= round(ComplexToneDur*SamplingRate);
prestim=zeros(round(PreStimSilence*SamplingRate),1);
poststim=zeros(round(PostStimSilence*SamplingRate),1);

ComponentAmplitudes=get(o,'ComponentAmplitudes');
%ones(ComponentsNumber,1);
%ComponentAmplitudes=hanning(ComponentsNumber);
% if AnchorFrequency== 150
%     ComponentAmplitudes=[1 1 1 1 0 0];
% else
%     ComponentAmplitudes=[0 0 1 1 1 1];
% end
if o.PoolSize > 1 && length(AnchorFrequency)>1
    error('PoolSize and AnchorFrequency cannot both be varied')
end
AnchorFrequency=AnchorFrequency(index);
% Generat complex tone sequence
event=struct('Note','','StartTime',0,'StopTime',0,'Trial',[]);event(1)=[];
% First complex tone
if isempty(TarObject)
    AF=AnchorFrequency;
ComplexTone= createTone(AF,FrequencyOrder(index),...
    ComponentRatios(:,index),rand(1,ComponentsNumber),...
    ComponentAmplitudes,ComplexToneDur,SamplingRate);
else
    TarPreStim=get(TarObject,'PreStimSilence');
    TarPostStim=get(TarObject,'PostStimSilence');
    TarObject=set(TarObject,'PreStimSilence',0);
    TarObject=set(TarObject,'PostStimSilence',0);
    [tarenv,tarevent]=env(TarObject,ThisTarParams.ThisTarIdx, 0);%envelope comes out from 0 to 1 for ModDepth=1.
    FMs=get(TarObject,'AM');
    timesamples = (1 : round(get(TarObject,'Duration')*SamplingRate))' / SamplingRate;
    tarenv=sin(2*pi*FMs(ThisTarParams.ThisTarIdx)*timesamples);
    TarAddTime=PreStimSilence+TarPreStim+ThisTarParams.TarStartBin/SamplingRate;
    for cnt2 = 1:length(tarevent)
        tarevent(cnt2).Note = [tarevent(cnt2).Note ' , Target'];
        tarevent(cnt2).StartTime = tarevent(cnt2).StartTime + TarAddTime;
        tarevent(cnt2).StopTime = tarevent(cnt2).StopTime + TarAddTime;
    end
    tarevent(1).StartTime=tarevent(1).StartTime-TarPreStim;
    tarevent(3).StopTime=tarevent(end).StopTime+TarPostStim;
    event(length(event)+(1:length(tarevent)))=tarevent;
    %tarenv=tarenv*10^(ThisTarParams.RelativeTarRefdB/20);%Scale peak of envelope (usually to RMS-match).
    tarinds=ThisTarParams.TarStartBin+round(TarPreStim*SamplingRate)+(1:length(tarenv));
    EnvTarRefRatio=ThisTarParams.EnvTarRefRatio*.2;
    RampDur=ThisTarParams.EnvTarRefRampDur;
    TRRramp=EnvTarRefRatio*ones(size(tarenv));
    sinePeriod = 2*RampDur;
    npts = sinePeriod*SamplingRate;
    sineFrequency = 1/sinePeriod;
    
    sineBuffer = -cos(2*pi*sineFrequency*(0:npts-1)/SamplingRate);
    sineBuffer = EnvTarRefRatio*(sineBuffer + 1)/2;
    N = round(length(sineBuffer)/2);
    N2=length(sineBuffer)-N;
    TRRramp(1:N)=sineBuffer(1:N);
    TRRramp(end-N2+1:end)=sineBuffer(N+1:end);
    fm=zeros(ComplexToneDur*SamplingRate,1);
    fm(tarinds)=TRRramp.*tarenv;
    ComplexTone= createTone2(AnchorFrequency,FrequencyOrder(index),...
    ComponentRatios(:,index),rand(1,ComponentsNumber),...
    ComponentAmplitudes,ComplexToneDur,SamplingRate,fm);
end
if AMFrequency > 0
    ComplexTone=ComplexTone.*(2+sin(2*pi*AMFrequency*...
        (0:ComplexToneSamples-1)'/SamplingRate));
end
ComplexTone= addenv(ComplexTone, SamplingRate);
% Add complex tone and gap to the stimulus
w=[ComplexTone(:); zeros(GapSamples,1)];
% if AnchorFrequency~=1000
%     sfigure(99);pwelch(ComplexTone,[],[],[],SamplingRate);xlim([0 AnchorFrequency*(ComponentsNumber+1)]/1000)
% pause(.05);
% end
% create event structure
thisevent= struct('Note',['PreStimSilence , ' Names{index}],...
      'StartTime',0,'StopTime',PreStimSilence,'Trial',[]);
thisevent(2)= struct('Note',['Stim , ' Names{index}],'StartTime',...
          PreStimSilence, 'StopTime', PreStimSilence+ComplexToneDur,'Trial',[]);
thisevent(3)= struct('Note',['PostStimSilence , ' Names{index}],...
         'StartTime',PreStimSilence+ComplexToneDur,'StopTime',PreStimSilence+...
         ComplexToneDur+GapDur,'Trial',[]);
event=[thisevent event];%if targets exist, put them at the end to keep convention
%w = 5 * w/max(abs(w));
% Add pre- and post-stimulus silence:
w = [prestim ; w ;poststim];



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Internal Functions  %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function s= createTone(fa,fn,ratios,phase,amp,dur,fs)
%fa anchor frequency
%fn numer of components
%ratio between components
%phase of components
%amp relative amplitudes
%duration
%fs sampling frequency
s= zeros(round(dur*fs),1);
td= ((1:round(dur*fs))-1)/fs ;
ph= phase(:)*pi/180;
rs= [1; ratios];
r= cumprod(rs(1:fn));
r= r(end);
f0= fa/r;
freqs= f0*cumprod(rs);
ph= repmat(ph',length(td),1);

for cnt1= 1:length(freqs)
    s= s+amp(cnt1)*sin(2*pi*freqs(cnt1)*td(:)+ph(:,cnt1));
end

function s= createTone2(fa,fn,ratios,phase,amp,dur,fs,fm)
%fa anchor frequency
%fn numer of components
%ratio between components
%phase of components
%amp relative amplitudes
%duration
%fs sampling frequency
s= zeros(round(dur*fs),1);
td= ((1:round(dur*fs))-1)/fs ;
ph= phase(:)*pi/180;
rs= [1; ratios];
r= cumprod(rs(1:fn));
r= r(end);
f0= fa/r;
freqs= f0*cumprod(rs);
ph= repmat(ph',length(td),1);

for cnt1= 1:length(freqs)
    s= s+amp(cnt1)*sin(2*pi*freqs(cnt1)*td(:)+freqs(cnt1)*fm+ph(:,cnt1));
end

% s= zeros(length(td),length(freqs));
% freqs= repmat(freqs, length(td),1);
% td=repmat(td(:),1,length(phase));
% 
% s(:,1:end)= sin(2*pi*freqs(:,1:end).*td(:,1:end)+ph(:,1:end));
% s= sum(s,2);


%add 5 ms rise/fall time ===================================
function s=addenv(s1,fs);
f=ones(size(s1));
pn=round(fs*0.005);    % 5 ms rise/fall time
up = sin(2*pi*(0:pn-1)/(4*pn)).^2;   %add sinramp
down = sin(2*pi*(pn+1:2*pn)/(4*pn)).^2;
f = [up ones(1,length(s1)-2*pn) down]';
s=s1(:).*f(:);


