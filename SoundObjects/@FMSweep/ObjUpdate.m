function o = ObjUpdate (o);
% This function is used to recalculate the properties of the object that
% depends on other properties. In these cases, user firs changes the
% properties using set, and then calls ObjUpdate. The default is empty, you
% need to write your own.

% Nima, november 2005
StartFrequency=get(o,'StartFrequency');
EndFrequency=get(o,'EndFrequency');

MaxIndex=max(length(StartFrequency),length(EndFrequency));
Names=cell(1,MaxIndex);
for ii=1:MaxIndex
    Names{ii}=[num2str(StartFrequency(ii)) ' - ' num2str(EndFrequency(ii))];
end

o = set(o,'Names',Names);
o = set(o,'MaxIndex',MaxIndex);
