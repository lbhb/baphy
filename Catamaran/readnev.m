function varargout = readnev(filename,varargin)
%readnev: Read Neuralynx Event Record file (.nev)
%
% Syntax:
%
%   [f1,f2,...,fn] = readnev(FILENAME,field1,field2,...,fieldn);
%
% will read fields from the records in the file and put the results in the
% f1, f2, ... variables.  The number of output variables must match the
% number of fields requested.  Fields are specified by strings.  The
% following fields are available:
%   nstx            Reserved
%   npkt_id         ID for the originating system of this packet
%   npkt_data_size  This value should always be two (2).
%   qwTimeStamp     Cheetah timestamp for this record (microseconds).
%   nevent_id       ID value for this event.
%   nttl            Decimal TTL value read from the TTL input port.
%   ncrc            Record CRC check from Cheetah.
%   ndummy1         Reserved
%   ndummy2         Reserved
%   dnExtra         Extra bit values for this event (N-by-8 matrix)
%   EventString     Event string associated with this event record.
%
% Typical usage:
%
%   [EV_Timestamps,EV_TTLs] = readnev(FILENAME,'qwTimeStamp','nttl');
%
% The field name matching is case-insensitive.

% Written by Douglas M. Schwarz, 3 September 2010

% The .nev file consists of a 16384-byte header followed by any number of
% records of the following form:
%
%   data       field
%   -------------------------
%   int16      nstx
%   int16      npkt_id
%   int16      npkt_data_size
%   uint64     qwTimeStamp
%   int16      nevent_id
%   int16      nttl
%   int16      ncrc
%   int16      ndummy1
%   int16      ndummy2
%   8*int32    dnExtra
%   128*char   EventString


% Check number of input arguments.
error(nargchk(1,12,nargin,'struct'))

% Check number of output arguments.
error(nargoutchk(0,nargin-1,nargout,'struct'))

% Define field names.
fields = {'nstx','npkt_id','npkt_data_size','qwTimeStamp','nevent_id',...
	'nttl','ncrc','ndummy1','ndummy2','dnExtra','EventString'};

% Compute fread parameters for each possible field.
count = {inf, inf, inf, inf, inf, inf, inf, inf, inf, [8 inf], [128 inf]};
precision = {'int16','int16','int16','uint64','int16','int16','int16',...
	'int16','int16','8*int32','128*char'};
bytes = [16 16 16 64 16 16 16 16 16 32*8 128*8]/8;
offset = cumsum([0 bytes(1:end-1)]);
record_length = sum(bytes);
skip = record_length - bytes;

% Size of header to be skipped.
header_length = 16384;

% Determine number of fields requested and create output arguments.
n = length(varargin);
varargout = cell(1,n);

% Open the file and check for errors.
[fid,msg] = fopen(filename,'rb','ieee-le');
if fid == -1
	error(msg)
end

% Loop through the requested fields.
for i = 1:n
	% Determine which field is requested.  Handle possible error.
	index = find(strcmpi(fields,varargin{i}));
	if isempty(index)
		fclose(fid);
		error('Unknown field requested: %s',varargin{i})
	end
	
	% Offset into record to the field.
	fseek(fid,offset(index) + header_length,'bof');
	
	% Read the data into the output variable.
	varargout{i} = fread(fid,count{index},precision{index},skip(index));
	
	% Post-processing for special cases.
	if strcmpi(varargin{i},'dnExtra')
		% Result should be an N-by-8 matrix.
		varargout{i} = varargout{i}.';
	elseif strcmpi(varargin{i},'EventString')
		% Result should be a cell array of strings.
		varargout{i} = cellstr(char(varargout{i}.'));
	end
end

% Close the file.
fclose(fid);
