function plot_raster_correlation_across_electrodes(mfile,electrodes,units,options)
%profile on
%options.rawtrace=1;
%options.rasterfs=NaN; % This means don't resmaple raw trace. If a different sampling frequency is desired, set it here.
for ii=1:length(electrodes)
    [R(ii,:,:,:),tags,~,~,options]=raster_load(mfile,electrodes(ii),units(ii),options);
end

for ii=1:length(electrodes)
    for jj=ii:length(electrodes)
        cct=corrcoef(R(ii,:),R(jj,:));
        cc(ii,jj)=cct(2,1);
    end
end
cc=cc+triu(cc,-1).';
cc(logical(eye(size(cc)))) = 0;
imagesc(cc,'Parent',options.ah);
assignin('base','cc',cc)
colormap(options.ah,'hot');
set(options.ah,'CLim',[0 1]);
%profile viewer
axis(options.ah,'square')
a=2;