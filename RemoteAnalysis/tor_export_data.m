% function [stim,resp,strf]=tor_export_data(parmfile,channum,unit)
%
% alternative call:  tor_export_data(cellid)
%    queries cellDB to find first torc file for the specified cellid
%    and uses that file for tuning anlaysis
%
function [stim,resp,strf]=tor_export_data(parmfile,channum,unit)

dbopen;
rasterfs=1000;

if ~exist(parmfile,'file') && ~exist([parmfile '.m'],'file'),
    % try the database,
    cellid=parmfile;
    sql=['SELECT * FROM sCellFile WHERE cellid="',cellid,'"',...
         ' AND runclassid=1'];
    torfiledata=mysql(sql);
    if ~isempty(torfiledata),
        parmfile=[torfiledata(1).stimpath torfiledata(1).stimfile];
        spikefile=[torfiledata(1).path torfiledata(1).respfile];
        channum=torfiledata(1).channum;
        unit=torfiledata(1).unit;
    else
        disp('TORC data not found');
        return
    end
else
   [pp,bb,ee]=fileparts(parmfile);
   sql=['SELECT * FROM sCellFile WHERE stimfile="',bb,'"',...
      ' AND runclassid=1'];
   torfiledata=mysql(sql);
   if ~isempty(torfiledata),
      spikefile=[torfiledata(1).path torfiledata(1).respfile];
   else
      disp('TORC data not found');
      return
   end
end

options=[];
options.rasterfs=rasterfs;
options.includeprestim=1;
options.tag_masks={'Reference'};
options.channel=channum;
options.unit=unit;
fprintf('loading TOR raster for %s\n', basename(spikefile));
[r,tags,trialset,exptevents]=loadspikeraster(spikefile,options);

LoadMFile(parmfile);

if isfield(exptparams.TrialObject,'Torchandle'),
   TorcObject=exptparams.TrialObject.Torchandle;
else
   TorcObject=exptparams.TrialObject.ReferenceHandle;
end

% this may be unnecessary....
TorcNames=TorcObject.Names;
for ii=1:length(TorcNames),
   bb=strsep(TorcNames{ii},' ',0);
   TorcNames{ii}=bb{1};
end

rold=r;

r=zeros(size(r,1),size(r,2),length(TorcNames)).*nan;
for ii=1:length(tags),
   bb=strsep(tags{ii},',',1);
   if ~strcmpi(strtrim(bb{3}),'Target')
      bb=strtrim(bb{2});
      jj=find(strcmp(bb,TorcNames));
      if ~isempty(jj),
         minrep=min(find(isnan(r(1,:,jj))));
         r(:,minrep:end,jj)=rold(:,1:(end-minrep+1),ii);
         %tags{ii}
         %[ii jj minrep]
         %r(:,:,jj)=rold(:,:,ii);
         %fprintf('mapping %d -> %d\n',jj,ii);
      end
   end
end

INC1STCYCLE=0;
[strf,snr,StimParam,strfee]=strf_est_core(r,TorcObject,rasterfs,INC1STCYCLE,16);

pred=strf_torc_pred(strf,StimParam.StStims);
if INC1STCYCLE,
   FirstStimTime=0;
else
   FirstStimTime=250;
end
numreps=size(r,2);
numstims=size(r,3);
[stimX,stimT,numstims] = size(StimParam.StStims);

a=linspace(log(StimParam.lfreq),log(StimParam.hfreq),stimX+1);
FrequencyBins=round(exp(a(1:(end-1))+diff(a)./2));

resp=r;

PreStimSilence=exptparams.TrialObject.ReferenceHandle.PreStimSilence;
Duration=exptparams.TrialObject.ReferenceHandle.Duration;
PostStimSilence=exptparams.TrialObject.ReferenceHandle.PostStimSilence;
respFs=rasterfs;

stimFs=stimT.*1000/StimParam.basep;

stim0=StimParam.StStims;
stim=repmat(stim0,[1 StimParam.ddur./StimParam.basep 1]);
stimpre=zeros(stimX,stimFs.*PreStimSilence,numstims);
stimpost=zeros(stimX,stimFs.*PostStimSilence,numstims);

stim=cat(2,stimpre,stim,stimpost);

outfile=sprintf('tor_data_%s.mat',cellid);

save(outfile,'stim','stim0','resp','stimFs','respFs','PreStimSilence','Duration','PostStimSilence','FrequencyBins');


