% function [bf,bw,lat,offlat]=strf_get_tuning(strf0,StimParam)
%
% pull a bunch of tunign properties out of an STRF
%
function [bf,bw,lat,offlat]=strf_get_tuning(strf0,StimParam,strfee)

% extract tuning properties
if ~exist('strfee','var'),
   strfee=strf0;
end

maxoct=log2(StimParam.hfreq./StimParam.lfreq);
stepsize2=maxoct./(size(strf0,1));

smooth = [100 size(strf0,2)];
tstrf0=gsmooth(strf0,[0.5 0.001]);

strfsmooth = interpft(strf0,smooth(1),1);

strfeesmooth = interpft(strfee,smooth(1),1);

ff=exp(linspace(log(StimParam.lfreq),...
                log(StimParam.hfreq),size(strfsmooth,1)));

mm=mean(strfsmooth(:,1:7).*(strfsmooth(:,1:7)>0),2);
if sum(abs(mm))>0,
   bfidx=median(find(mm==max(mm)));
   bf=round(ff(bfidx));
   bfshiftbins=(maxoct./2-log2(bf./StimParam.lfreq))./stepsize2;
else
   bf=0;
   bfshiftbins=0;
end

bw=sum(mm>=max(mm)./2) ./ length(mm).*(maxoct-1);

mmn=mean(strfsmooth(:,1:7).*(strfsmooth(:,1:7)<0),2);
if sum(abs(mmn))>0,
   wfidx=median(find(mmn==min(mmn)));
   wf=round(ff(wfidx));
   wfshiftbins=(maxoct./2-log2(wf./StimParam.lfreq))./stepsize2;
else
   wf=0;
   wfshiftbins=0;
end

if -mmn(wfidx)>mm(bfidx),
   % if stronger negative component, calculate latency with that
   shiftbins=wfshiftbins;
   irsmooth = -interpft(strfsmooth(wfidx,:),250);
   ireesmooth = interpft(strfeesmooth(wfidx),250);
else
   % otherwise use positive
   shiftbins=bfshiftbins;
   irsmooth = interpft(strfsmooth(bfidx,:),250);
   ireesmooth = interpft(strfeesmooth(bfidx),250);
end

mb=0;
% find significantly modulated time bins
sigmod=find(irsmooth-mb>ireesmooth.*2);
% require latency>=8 ms, max latency less than 125 ms;
sigmod=sigmod(sigmod>=8 & sigmod<125);
if length(sigmod)>3,
   latbin=sigmod(1);
   dd=[diff(sigmod) 41];
   durbin=sigmod(min(find(dd(1:end)>40)));
   %durbin=sigmod(end-2);
   lat=round(latbin.*1000./1000);
   offlat=round(durbin.*1000./1000);
   fprintf('onset/offset latency %d/%d ms\n',lat,offlat);
else
   latbin=0;
   lat=0;
   durbin=0;
   offlat=0;
   fprintf('no significant onset latency\n');
end

