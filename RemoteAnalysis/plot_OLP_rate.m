function plot_OLP_rate(mfile,channels,do_norm, axeshandle)

if nargin<3
    do_norm=0;
end
if nargin<4
    figure;
    axeshandle=gca();
end
figure(get(axeshandle,'Parent'));
axes(axeshandle);

options.sigthreshold=4;
options.useOEPspikes=1;
%global USECOMMONREFERENCE
%USECOMMONREFERENCE=1;
sf=1;

for i=1:length(channels)
    [r,tags,~,~,options_out]=raster_load(mfile,channels(i),[],options);
    display(tags)
    mean_rate_by_rep=shiftdim(mean(r(500:1500,:,:)),1);
    mean_rate=mean(mean_rate_by_rep,1);
    rate_serr=std(mean_rate_by_rep,[],1)./sqrt(size(mean_rate_by_rep,1));
    if do_norm
        sf=max(mean_rate);
    end
    errorbar(mean_rate/sf,rate_serr/sf);
    hold on;
end
for i=1:length(tags)
    tag = split(tags{i});
    tagg = tag{3};
    taggg = split(tagg,'_');
    mask = ~contains(taggg, 'null');
    taggg = taggg(mask);
    if length(taggg) > 1
        taggg = strjoin(taggg);
    end
    name = split(taggg, '-');
    tags{i} = name{1};
end
set(gca, 'XTick', 1:length(tags))
set(gca,'XTickLabels',tags)
xtickangle(90)
legend(arrayfun(@(x)['Channel ',num2str(x)],channels,'Uni',0),'Location','best')