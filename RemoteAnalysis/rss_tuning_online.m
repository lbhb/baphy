% function [rss_stim,meanwts] = rss_tuning_online(mfile,channel,unit,axeshandle,options);
%
%
% LAS 2017-02-16 -- ripped off chord_strf_online and MarmoPhysAnalysis_Server.m
%
function [rss_stim,meanwts] = rss_tuning_online(mfile,channel,unit,axeshandle,options);

if ~exist('channel','var'),
    channel=1;
end
if ~exist('unit','var'),
    unit=1;
end
if ~exist('axeshandle','var'),
    axeshandle=gca;
end
if isnan(axeshandle)
    ff=NaN;
else
    ff=get(axeshandle,'Parent');
end
if ~exist('options','var'),
    options=[];
end
if ~isfield(options,'rasterfs'),
    options.rasterfs=1000;
end
if ~isfield(options,'sigthreshold'),
    options.sigthreshold=4;
end
if ~isfield(options,'datause'),
    options.datause='Reference';
end
if ~isfield(options,'psth'),
    options.psth=0;
end
if ~isfield(options,'psthfs'),
    options.psthfs=20;
end
if ~isfield(options,'lfp'),
    options.lfp=0;
end
if ~isfield(options,'usesorted'),
    options.usesorted=0;
end
if ~isfield(options,'showdetails'),
    options.showdetails=0;
end

options.channel=channel;
options.unit=unit;


fprintf('%s: Analyzing channel %d (rasterfs %d, spike thresh %.1f std)\n',...
    mfilename,channel,options.rasterfs,options.sigthreshold);
LoadMFile(mfile);

ReferencePreStimSilence=max(exptparams.TrialObject.ReferenceHandle.PreStimSilence,0.1);
ReferenceDuration=exptparams.TrialObject.ReferenceHandle.Duration;
ReferencePostStimSilence=max(exptparams.TrialObject.ReferenceHandle.PostStimSilence,0.1);

options.PreStimSilence=ReferencePreStimSilence;
options.PostStimSilence=ReferencePostStimSilence;
options.rasterfs=1000;
disp('chord_strf_online: Loading response...');
[r_ref,tags_ref]=raster_load(mfile,channel,unit,options);
if(isempty(r_ref))
    rss_stim=[];meanwts=[];
    return
end
rmi=cellfun(@(x)~isempty(strfind(x,'nsc'))||isempty(x),tags_ref);
r_ref(:,:,rmi)=[];
tags_ref(rmi)=[];
for i=1:length(tags_ref)
    ref=strsep(tags_ref{i});
    if(length(ref)==5 && strcmp(ref{3}(1),'m') && strcmp(ref{3}(5:end),'.wav'))
        rssi(i)=str2double(ref{3}(2:4));
    else
        error('Unknown reference')
    end
end

stiminds=round(ReferencePreStimSilence*options.rasterfs)+1:round((ReferencePreStimSilence+ReferenceDuration)*options.rasterfs);
rss_rate=squeeze(mean(r_ref(stiminds,1,:)));
usei=~isnan(rss_rate);
rss_rate_zm=nan(size(rss_rate));
rss_rate_zm(usei)=rss_rate(usei)-mean(rss_rate(usei));

%load RSS stimuli
RSSDataPath = fileparts(mfilename('fullpath'));
rss_stim=load([RSSDataPath filesep 'RSS_stim.mat'],'freq','stim');
rss_stim.stim=rss_stim.stim(:,rssi);

%compute linear weights
meanwts = rss_stim.stim(:,usei)'\rss_rate_zm(usei); % slightly faster than above method -MJR

if(~isnan(axeshandle))
    xxs=[0.1:1:40.1]';
    semilogx(rss_stim.freq,meanwts(1:39),'bo-','Parent',axeshandle),
    hold(axeshandle,'on')
    plot(xxs,0*xxs,'k','Parent',axeshandle)
    xlabel(axeshandle,'Frequency, kHz')
    ylabel(axeshandle,'Weight, sp/(s*dB)')
    set(axeshandle,'Xlim',[1 40],'YLim',[min(meanwts) max(meanwts)]*1.1)
    text(exptparams.TrialObject.TargetHandle.Frequencies(1)/1000,max(get(axeshandle,'YLim')),...
        '\downarrow','VerticalAlignment','top','HorizontalAlignment','center',...
        'Parent',axeshandle);
    hold(axeshandle,'off')
    if(1)
        [pval,ploc]=max(meanwts);
        title(axeshandle,sprintf('[Tar,RSS pk] = [%.0f,%.0f] Hz',...
            exptparams.TrialObject.TargetHandle.Frequencies(1),...
            rss_stim.freq(ploc)*1000),'FontSize',12)
    end
end

