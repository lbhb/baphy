% function [on_mat,off_mat,unique_freq,res] = chord_strf_online(mfile,channel,unit,axeshandle,options);
% function [freq level meanonset meansustained meanoffset] = chord_strf_online(mfile,channel,unit,axeshandle,options);
% reverse correlation for complex chord stimuli.
%
% SVD 2007-04-12 -- ripped off raster_online & strf_online
%
function varargout = chord_strf_online(mfile,channel,unit,axeshandle,options);

if ~exist('channel','var'),
    channel=1;
end
if ~exist('unit','var'),
    unit=1;
end
if ~exist('axeshandle','var'),
    axeshandle=gca;
end
if verLessThan('matlab','8.4') && isnan(axeshandle)
    ff=NaN;
elseif ~verLessThan('matlab','8.4') && ~isa(axeshandle,'matlab.graphics.axis.Axes')
    ff=NaN;
else
    ff=get(axeshandle,'Parent');
end

if ~exist('options','var'),
    options=[];
end
if ~isfield(options,'rasterfs'),
    options.rasterfs=1000;
end
if ~isfield(options,'sigthreshold'),
    options.sigthreshold=4;
end
if ~isfield(options,'datause'),
    options.datause='Reference';
end
if ~isfield(options,'psth'),
    options.psth=0;
end
if ~isfield(options,'psthfs'),
    options.psthfs=20;
end
if ~isfield(options,'lfp'),
    options.lfp=0;
end
if ~isfield(options,'usesorted'),
    options.usesorted=0;
end
if ~isfield(options,'showdetails'),
    options.showdetails=0;
end

isFLT=~isempty(strfind(mfile,'_FLT')) || ~isempty(strfind(mfile,'_VWL'));
isVBN=~isempty(strfind(mfile,'_VBN'));
isAMT=~isempty(strfind(mfile,'_AMT'));
isLTC=~isempty(strfind(mfile,'_LTC'));

options.channel=channel;
options.unit=unit;

if isVBN || isAMT || isLTC
    %divide nfreq by this factor to get smoothing sigma
    options.smooth_plot=0;
    options.smooth_factor=inf; %no smoothing
    options.plot_error_bars=1;
    %divide trail count (N frequencies * N levels * N reps) by this factor 
    %to get the number of frequency bins
    options.max_freq_factor=1;    
else
    options.smooth_plot=1;
    %divide nfreq by this factor to get smoothing sigma
    options.smooth_factor=40;
    options.plot_error_bars=1;
    %divide trail count (N frequencies * N levels * N reps) by this factor
    %to get the number of frequency bins
    options.max_freq_factor=8;
    %options.max_freq_factor=1;
end

%set the number of frequency bins to be no more than this number
%options.absolute_max_freq=inf;
options.absolute_max_freq=getparm(options,'absolute_max_freq',20);


options.fontsize_labels=10;
options.fontsize_ticklabels=6;

fprintf('%s: Analyzing channel %d (rasterfs %d, spike thresh %.1f std)\n',...
    mfilename,channel,options.rasterfs,options.sigthreshold);
LoadMFile(mfile);

if ~strcmpi(exptparams.TrialObject.ReferenceClass,'FusionNoise') && ...
    (strcmpi(exptparams.TrialObject.ReferenceClass,'RandomTone') || ...
        ~isfield(exptparams.TrialObject.ReferenceHandle,'SimulCount') || ...
        exptparams.TrialObject.ReferenceHandle.SimulCount==1)
    
    ReferencePreStimSilence=max(exptparams.TrialObject.ReferenceHandle.PreStimSilence,0.1);
    ReferenceDuration=exptparams.TrialObject.ReferenceHandle.Duration;
    ReferencePostStimSilence=max(exptparams.TrialObject.ReferenceHandle.PostStimSilence,0.1);
    
    options.PreStimSilence=ReferencePreStimSilence;
    options.PostStimSilence=ReferencePostStimSilence;
    options.rasterfs=1000;
    disp('chord_strf_online: Loading response...');
    [r_ref,tags_ref]=raster_load(mfile,channel,unit,options);
    if(isempty(r_ref))
        return
    end
    rmi=cellfun(@(x)isempty(x),tags_ref);
    tags_ref(rmi)=[];
    r_ref(:,:,rmi,:)=[];
    r_ref=r_ref.*options.rasterfs;
    
    fset=[];
    lset=[];
    baseset=[];
    fcset=[];
    for ii=1:length(tags_ref),
        tt=strsep(tags_ref{ii},',',1);
        if(isFLT)
            tt2 = strrep(strsep(tt{2},':',1),' ','');
            lset=cat(1,lset,str2double(tt2{2}(1:end-2)));
            fset=cat(1,fset,str2double(tt2{1}));
        elseif isVBN
            tt2=strsep(tt{2},' ',1);
            tt2(cellfun(@isempty,tt2))=[];
            baseset=cat(1,baseset,str2double(tt2{2}));
            fset=cat(1,fset,str2double(tt2{3}));
        elseif isAMT
            tt2=strsep(tt{2},':',1);
            tt2(cellfun(@isempty,tt2))=[];
            fcset=cat(1,fcset,str2double(tt2{1})); %Center frequency
            if length(tt2)==1
                AMf=0;
            else
                AMf=str2double(tt2{3});
            end
            fset=cat(1,fset,AMf); %AM frequency
        else
            fset=cat(1,fset,str2double(tt{2}));
        end
    end
    [unique_freq,~,mapidx]=unique(fset);
    freqcount=length(unique_freq);
    
    TrialCount=sum(~isnan(r_ref(1,:)));
    MaxFreq=min(TrialCount./options.max_freq_factor,options.absolute_max_freq);
    if freqcount>MaxFreq,
        newidx=round(linspace(1,freqcount+1,MaxFreq+1));
        newfreq=zeros(round(MaxFreq),1);
        for jj=1:MaxFreq,
            newfreq(jj)=round(2.^mean(log2(unique_freq(newidx(jj):(newidx(jj+1)-1)))));
            ff=find(ismember(fset,unique_freq(newidx(jj):(newidx(jj+1)-1))));
            fset(ff)=newfreq(jj);
        end
        [unique_freq,~,mapidx]=unique(fset);
        freqcount=length(unique_freq);
    end
    stimuluscount=freqcount;
    repcount=size(r_ref,2);
    stimulusvalues=unique_freq;
    if isFLT
        [unique_levs,~,levmapidx]=unique(lset);
        levcount=length(unique_levs);
    else
        unique_levs=exptparams.TrialObject.OveralldB;
        levcount=1;levmapidx=ones(size(mapidx));
    end
    
    % find the time bins for the pre-stimulus epoch
    spontbins=1:round(ReferencePreStimSilence*options.rasterfs);
    onsetbins=round(ReferencePreStimSilence*options.rasterfs+1):...
        round((ReferencePreStimSilence+0.05)*options.rasterfs);
%     sustbins=round((ReferencePreStimSilence)*options.rasterfs+1):...
%         round((ReferencePreStimSilence+0.1)*options.rasterfs);
    sustbins=round((ReferencePreStimSilence+0)*options.rasterfs+1):...
        round((ReferencePreStimSilence+ReferenceDuration)*options.rasterfs);
    offsetbins=round((ReferencePreStimSilence+ReferenceDuration)*options.rasterfs+1):...
        round((ReferencePreStimSilence+ReferenceDuration+0.05)*options.rasterfs);
    meanspontaneous=zeros(freqcount,levcount);
    semspontaneous=zeros(freqcount,levcount);
    meanonset=zeros(freqcount,levcount);
    semonset=zeros(freqcount,levcount);
    meansustained=zeros(freqcount,levcount);
    semsustained=zeros(freqcount,levcount);
    meanoffset=zeros(freqcount,levcount);
    semoffset=zeros(freqcount,levcount);
    
    if strcmpi(exptparams.TrialObject.ReferenceClass,'NoiseSample'),
        RISVAR=1;  % response = PSTH variance
        %baseline=nanmean(nanstd(nanmean(r_ref(spontbins,:,:),2),0,1));
        %sembaseline=nanstd(nanstd(nanmean(r_ref(spontbins,:,:),2),0,1))./sqrt(stimuluscount);
        globalmean=nanmean(nanmean(r_ref(spontbins,:)));
        baseline=nanmean(nanmean((r_ref(spontbins,:)-globalmean).^2));
        sembaseline=nanmean(nanmean((r_ref(spontbins,:)-globalmean).^2))./sqrt(sum(~isnan(r_ref(1,:))));
    else
        RISVAR=0;  % response = mean firing rate
        baseline=nanmean(nanmean(r_ref(spontbins,:)));
    end
    
    
    for jj=1:levcount
        for ii=1:freqcount,
            if RISVAR,
                meanspontaneous(ii)=baseline;
                semspontaneous(ii)=sembaseline;
                tr=r_ref(onsetbins,:,mapidx==ii);
                meanonset(ii)=nanmean(nanmean((tr(:,:)-globalmean).^2));
                semonset(ii)=nanstd(nanmean((tr(:,:)-globalmean).^2))./sqrt(sum(~isnan(tr(1,:))));
                tr=r_ref(sustbins,:,mapidx==ii);
                meansustained(ii)=nanmean(nanmean((tr(:,:)-globalmean).^2));
                semsustained(ii)=nanstd(nanmean((tr(:,:)-globalmean).^2))./sqrt(sum(~isnan(tr(1,:))));
                tr=r_ref(offsetbins,:,mapidx==ii);
                meanoffset(ii)=nanmean(nanmean((tr(:,:)-globalmean).^2));
                semoffset(ii)=nanstd(nanmean((tr(:,:)-globalmean).^2))./sqrt(sum(~isnan(tr(1,:))));
            else
                tr=r_ref(spontbins,:,mapidx==ii & levmapidx==jj);
                meanspontaneous(ii,jj)=nanmean(tr(:));
                semspontaneous(ii,jj)=nanstd(tr(:))./sqrt(sum(~isnan(tr(:))));
                tr=r_ref(onsetbins,:,mapidx==ii & levmapidx==jj);
                meanonset(ii,jj)=nanmean(tr(:))-meanspontaneous(ii)+baseline;
                semonset(ii,jj)=nanstd(tr(:))./sqrt(sum(~isnan(tr(:))));
                tr=r_ref(sustbins,:,mapidx==ii & levmapidx==jj);
                meansustained(ii,jj)=nanmean(tr(:))-meanspontaneous(ii)+baseline;
                semsustained(ii,jj)=nanstd(tr(:))./sqrt(sum(~isnan(tr(:))));
                tr=r_ref(offsetbins,:,mapidx==ii & levmapidx==jj);
                meanoffset(ii,jj)=nanmean(tr(:))-meanspontaneous(ii)+baseline;
                semoffset(ii,jj)=nanstd(tr(:))./sqrt(sum(~isnan(tr(:))));
            end
            
        end
    end
    bf=0; bf_sust=0;
    smooth_std=freqcount./options.smooth_factor;
    ps=gsmooth(mean(meanonset,2),smooth_std);
    mm=find(ps==max(ps), 1 );
    mm_peak=ps(mm);
    if mean(meanonset(mm,:),2)-2*mean(semonset(mm,:),2)>baseline,
        bf=unique_freq(mm);
    end
    ps=gsmooth(mean(meansustained,2),smooth_std);
    mm_sust=find(ps==max(ps), 1 );
    mm_sust_peak=ps(mm_sust);
    if mean(meansustained(mm_sust,:),2)-2*mean(semsustained(mm_sust,:),2)>baseline,
        bf_sust=unique_freq(mm_sust);
    end
    
    if verLessThan('matlab','8.4') && isnan(axeshandle)
    elseif ~verLessThan('matlab','8.4') && ~isa(axeshandle,'matlab.graphics.axis.Axes')
    else
        sfigure(get(axeshandle,'Parent'));
    axes(axeshandle);
    if levcount>1
        imagesc(1:stimuluscount,1:levcount,meansustained');
        set(axeshandle,'YDir','Normal')
        yt=1:levcount;
        ytl=unique_levs;
        set(axeshandle,'YTick',yt,'YTickLabel',ytl)
        hold on;
        if bf_sust>0,
            line(mm_sust([1 1]),get(gca,'YLim'),'Color','w');
        end
        hold off;
    else
        co=get(gca,'ColorOrder');
        if RISVAR,
            ph=errorbar(repmat((1:stimuluscount)',[1 2]),...
                [meanonset meansustained],...
                [semonset semsustained]);
        else
            v=[meanonset meansustained meanoffset];
            if(options.smooth_plot)
                v=gsmooth(v,smooth_std);
            end
            ph=errorbar(repmat((1:stimuluscount)',[1 3]),...
                v,...
                [semonset semsustained semoffset]);
        end
        if(~options.plot_error_bars)
            for i=1:length(ph)
                ch=get(ph(i),'Children');
                set(ch(2),'Visible','off')
            end
        end
        hold on
        plot([1 stimuluscount],[0 0]+mean(meanspontaneous),'b--');
        if bf>0,
            plot(mm,mm_peak,'o','Color',co(1,:));
        end
        if bf_sust>0,
            plot(mm_sust,mm_sust_peak,'o','Color',co(2,:));
        end
        
        hold off
        set(axeshandle,'XLim',[0 stimuluscount+1]);  % make x axis look nice
    end
    logstimvals_unique_diff = unique(diff(log2(stimulusvalues)));
    logstimvals_unique_diff(isinf(logstimvals_unique_diff))=[];
    log_spaced = length(logstimvals_unique_diff)==1;
    if max(stimulusvalues)>4000,
        stimulusKHz=round(stimulusvalues./100)./10;
        lowi=stimulusvalues<1000;
        stimulusKHz(lowi)=round(stimulusvalues(lowi)./10)./100;
    elseif max(stimulusvalues)>100 && length(stimulusvalues)>20
        stimulusKHz=round(stimulusvalues./10)./100;
    else
        stimulusKHz=stimulusvalues;
    end
    stimlabelidx=1:2:stimuluscount;
    set(axeshandle,'XTick',stimlabelidx,'XTickLabel',stimulusKHz(stimlabelidx));
    
    if levcount==1
        if RISVAR,
            hl=legend('Onset','All');
            ylabel('Spike std (spikes/sec)','FontSize',options.fontsize_labels);
        else
            hl=legend('Onset','All','Offset');
            ylabel('Spike rate (spikes/sec)','FontSize',options.fontsize_labels);
        end
        legend(gca,'boxoff');
        set(hl,'FontSize',5);
    end
    if isAMT
        xlabel('AM frequency (Hz)','FontSize',options.fontsize_labels);
    elseif isLTC
        xlabel('Level (dB)','FontSize',options.fontsize_labels);
    elseif max(stimulusvalues)>100
        xlabel('Stimulus frequency (KHz)','FontSize',options.fontsize_labels);
    else
        xlabel('Stimulus ID','FontSize',options.fontsize_labels);
    end
    if bf>0 || bf_sust>0
        if options.compact
            str=sprintf('%d BF=%.0f, %.0f',channel,bf,bf_sust);
        else
            str=sprintf('E%d Rep%d BF=%.0f (on) %.0f (sust)',channel,repcount,bf,bf_sust);
        end
    else
        if options.compact
            str=sprintf('%d BF=nan',channel,repcount);
        else
            str=sprintf('E%d Rep%d BF=nan',channel,repcount);
        end            
    end
    ht=title(str,'FontSize',options.fontsize_labels);
    set(ht,'Interpreter','none');
    set(gcf,'Name',sprintf('%s(%d)',basename(mfile),repcount));
    
    set(axeshandle,'FontSize',options.fontsize_ticklabels)
    end
    varargout{1}=stimulusvalues;
    varargout{2}=unique_levs;
    varargous{3}=meanonset;
    varargout{4}=meansustained;
    varargout{5}=meanoffset;
    varargout{6}=semonset;
    varargout{7}=semsustained;
    varargout{8}=semoffset;    
    return
end

if ~isfield(options,'PreStimSilence'),
    options.PreStimSilence=0.0;
end
if ~isfield(options,'PostStimSilence'),
    options.PostStimSilence=0.1;
end

PreStimSilence=options.PreStimSilence;
PostStimSilence=options.PostStimSilence;

disp('chord_strf_online: Loading response...');
[r,tags]=raster_load(mfile,channel,unit,options);

for ii=1:length(tags)
    if strcmpi(exptparams.runclass,'FNS')
        tt=strsep(tags{ii},',',1);
        tt=strsep(tt{2},'_',1);
        f1=strsep(tt{1},'-');
        f1=f1{1};
        f2=strsep(tt{2},'-');
        f2=f2{1};
        fset(ii,:)=[f1,f2];
        
    else
        tt=strsep(tags{ii},',',1);
        tt=strsep(tt{2},'+');
        if ~isempty(findstr(':',tt{1})),
            tt{1}=strsep(tt{1},':');
            tt{1}=tt{1}{1};
        end
        if ~isempty(findstr(':',tt{2})),
            tt{2}=strsep(tt{2},':');
            tt{2}=tt{2}{1};
        end
        if ii==1,
            fset=zeros(length(tags),length(tt));
        end
        fset(ii,:)=cat(2,tt{:});
    end
end
unique_freq=unique(fset(:));
freqcount=length(unique_freq);

fprintf('%d unique frequencies, %d simultaneous\n',freqcount,size(fset,2));

disp('Estimating STRF...');
realrepcount=size(r,2);

p=squeeze(nanmean(r,2));
pe=squeeze(nanstd(r,0,2))./sqrt(size(r,2));

baseline=mean(mean(p([1:5 (end-10):end],:)));
filtwidth=round(0.01.*options.rasterfs);
%psth=rconv2(mean(p,2),ones(filtwidth,1)./filtwidth);
psth=mean(p,2);

if 0,
    plot(psth);
    hold on;
    plot([1 length(psth)],baseline.*[1 1],'r');
    plot([1 length(psth)],(baseline+std(psth)).*[1 1],'r--');
    plot([1 length(psth)],(baseline-std(psth)).*[1 1],'r--');
    hold off
end

on_timewindow=round((PreStimSilence.*options.rasterfs+1):(size(r,1)-PostStimSilence.*options.rasterfs));
off_timewindow=(size(r,1)-PostStimSilence.*options.rasterfs+1):size(r,1);

on_timewindow=on_timewindow(find(abs(psth(on_timewindow)-baseline)>std(psth)));
if 1 | isempty(on_timewindow),
    on_timewindow=round(PreStimSilence.*options.rasterfs+(11:100));
else
    on_timewindow=on_timewindow(1):on_timewindow(end);
end

off_timewindow=off_timewindow(find(abs(psth(off_timewindow)-baseline)>std(psth)));
if 1 | isempty(off_timewindow),
    off_timewindow=(size(r,1)-PostStimSilence.*options.rasterfs)+(11:100);
else
    off_timewindow=off_timewindow(1):off_timewindow(end);
end

p_on=nanmean(p(on_timewindow,:));
p_off=nanmean(p(off_timewindow,:));
r_on=squeeze(nanmean(r(on_timewindow,:,:)));
r_off=squeeze(nanmean(r(off_timewindow,:,:)));
e_on=sum(r_on.^2);
e_off=sum(r_off.^2);
e_count=size(r_on,1);

on_mat=zeros(freqcount);
off_mat=zeros(freqcount);
on_mat_a=zeros(freqcount,freqcount,e_count);
off_mat_a=zeros(freqcount,freqcount,e_count);
on_snr=zeros(freqcount);
off_snr=zeros(freqcount);
on_full=zeros(length(psth)./10,freqcount,freqcount);

for ii=1:size(fset,1),
    f1=find(unique_freq==fset(ii,1));
    f2=find(unique_freq==fset(ii,2));
    
    on_mat(f1,f2)=p_on(ii);
    off_mat(f1,f2)=p_off(ii);
    
    tpsth=rconv2(p(:,ii),ones(10,1)./10);
    on_full(:,f1,f2)=tpsth(5:10:end);
    on_full(:,f2,f1)=on_full(:,f1,f2);
    
    if length(e_on)>1,
        on_snr(f1,f2)=e_on(ii);
        off_snr(f1,f2)=e_off(ii);
    end
    
    for jackidx=1:e_count,
        %incidx=[1:(jackidx-1) (jackidx+1):e_count];
        on_mat_a(f1,f2,jackidx)=(r_on(jackidx,ii));
        off_mat_a(f1,f2,jackidx)=(r_off(jackidx,ii));
    end
end

res=[];

% improve SNR by taking advantage of two tones' symmetry
if ~strcmpi(exptparams.runclass,'FNS')
    on_mat=(on_mat+on_mat')./2;
    off_mat=(off_mat+off_mat')./2;
end

e_count2=e_count.*2;
on_snr=((on_snr+on_snr')./e_count2 - on_mat.^2);
off_snr=((off_snr+off_snr')./e_count2 - off_mat.^2);

res.on_full=on_full;
res.baseline=baseline;
res.snr=(sum(on_mat(:).^2)+sum(off_mat(:).^2))./...
    (sum(on_snr(:))+sum(off_snr(:))) - 1;

on_snr=on_mat./(on_snr+(on_snr==0)) - 1;
off_snr=off_mat./(off_snr+(off_snr==0)) - 1;

%jackknifed linearity test
lin_idx_a=zeros(e_count,1);
lin_idx_b=zeros(e_count,1);
l2d=zeros(size(on_mat,1).*2,size(on_mat,2),e_count);
e_half=floor(e_count./2);
for ii=1:e_count,
    incidx=mod(ii+(1:(e_count-e_half))-1,e_count)+1;
    excidx=setdiff(1:e_count,incidx);
    
    mm=nanmean(on_mat_a(:,:,incidx),3);
    mm=(mm+mm')./2;
    mm_off=nanmean(off_mat_a(:,:,incidx),3);
    mm_off=(mm_off+mm_off')./2;
    
    xx=zeros(freqcount.^2,freqcount);
    for jj=1:freqcount,
        xx((jj-1).*freqcount+(1:freqcount),jj)=xx((jj-1).*freqcount+(1:freqcount),jj)+1;
        xx(jj:freqcount:freqcount.^2,jj)=xx(jj:freqcount:freqcount.^2,jj)+1;
    end
    %xx(:,freqcount+1)=0;
    
    yy=mm(:);
    %yy=yy-mean(yy);
    
    % don't include pure 2x tones because of likely nonlinear gain
    dd=eye(size(mm,1));
    keepidx=find(~dd);
    
    b_on=regress(yy(keepidx),xx(keepidx,:));
    on_mat_linpred=repmat(b_on,[1 freqcount])+repmat(b_on',[freqcount 1]);
    
    yy=mm_off(:);
    %yy=yy-mean(yy);
    
    b_off=regress(yy(keepidx),xx(keepidx,:));
    off_mat_linpred=repmat(b_off,[1 freqcount])+repmat(b_off',[freqcount 1]);
    
    t2=[mm(:);mm_off(:)];
    l2=[on_mat_linpred(:);off_mat_linpred(:)];
    
    mmx=mean(on_mat_a(:,:,excidx),3);
    mmx=(mmx+mmx')./2;
    mmx_off=mean(off_mat_a(:,:,excidx),3);
    mmx_off=(mmx_off+mmx_off')./2;
    x2=[mmx(:);mmx_off(:)];
    
    t2=t2(keepidx);
    l2=l2(keepidx);
    x2=x2(keepidx);
    
    l2=l2+shuffle(t2-l2);
    
    % correlation between linear model and other half of 2nd order data
    lin_idx_a(ii)=xcov(x2,l2,0,'coeff');
    % correlation between 2 halves of 2nd order data
    lin_idx_b(ii)=xcov(x2,t2,0,'coeff');
    
    
    %lin_idx_a(ii)=mean((t2-l2).^2)./mean(t2.^2);
    
    l2d(:,:,ii)=[mm;mm_off]-[on_mat_linpred;off_mat_linpred];
    
end
res.lin_idx=mean(lin_idx_b.^2-lin_idx_a.^2);
res.lin_idx_err=sqrt(std(lin_idx_b.^2-lin_idx_a.^2))./sqrt(e_count);
res.psth=psth;

%l2m=mean(l2d,3);
%l2e=std(l2d,0,3).^2 .*(e_count-1);
%lin_idx=mean(l2m(keepidx).^2)./mean(l2e(keepidx))-1;
%res.lin_idx=lin_idx;
%keyboard


% scale to spikes/sec
on_mat=on_mat.*options.rasterfs;
off_mat=off_mat.*options.rasterfs;

mm=[on_mat; off_mat]-baseline;
mmax=max(abs(mm(:)));

if ~options.showdetails
    
    sfigure(get(axeshandle,'Parent'));
    axes(axeshandle);
    imagesc(mm.*abs(mm),[-1 1].*mmax.^2);
    
    xlabel(sprintf('f1 (Hz) -- thresh=%.1f sig -- %d spikes',options.sigthreshold,nansum(r(:))),'FontSize',options.fontsize_labels);
    ylabel(sprintf('f2 (Hz)'),'FontSize',options.fontsize_labels);
    
    aa=axis;
    set(axeshandle,'YTick',floor([linspace(1,freqcount,3) linspace(1,freqcount,3)+freqcount]));
    set(axeshandle,'YTickLabel',repmat(unique_freq(round(linspace(1,freqcount,3))),[2 1]));
    set(axeshandle,'XTick',floor(linspace(1,freqcount,3)));
    set(axeshandle,'XTickLabel',unique_freq(round(linspace(1,freqcount,3))));
    
    if ~isfield(exptparams,'Repetition'),
        exptparams.Repetition=0;
    end
    ht=title(sprintf('%s chan %d rep %d',basename(mfile),channel,realrepcount),'FontSize',options.fontsize_labels);
    set(ht,'Interpreter','none');
    
    set(gcf,'Name',sprintf('%s(%d)',basename(mfile),realrepcount));
    axis image
    drawnow;
    return
    
end


xx=zeros(freqcount.^2,freqcount);
for ii=1:freqcount,
    xx((ii-1).*freqcount+(1:freqcount),ii)=xx((ii-1).*freqcount+(1:freqcount),ii)+1;
    xx(ii:freqcount:freqcount.^2,ii)=xx(ii:freqcount:freqcount.^2,ii)+1;
end
%xx(:,freqcount+1)=0;

yy=on_mat(:);
yy=yy-mean(yy);

% don't include pure 2x tones because of likely nonlinear gain
dd=eye(size(on_mat,1));
keepidx=find(~dd);

b_on=regress(yy(keepidx),xx(keepidx,:));
on_mat_linpred=repmat(b_on,[1 freqcount])+repmat(b_on',[freqcount 1]);

yy=off_mat(:);
yy=yy-mean(yy);
b_off=regress(yy(keepidx),xx(keepidx,:));
off_mat_linpred=repmat(b_off,[1 freqcount])+repmat(b_off',[freqcount 1]);

res.on_mat_linpred=on_mat_linpred;
res.off_mat_linpred=off_mat_linpred;

mm_linpred=[on_mat_linpred; off_mat_linpred];
mmax_linpred=max(abs(mm_linpred(:)));

sfigure(ff);
clf
subplot(1,3,1);
imagesc(mm.*abs(mm),[-1 1].*mmax.^2);

xlabel(sprintf('f1 (Hz) -- thresh=%.1f sig -- %d spikes',options.sigthreshold,nansum(r(:))),'FontSize',options.fontsize_labels);
ylabel(sprintf('f2 (Hz)'),'FontSize',options.fontsize_labels);
ht=title(sprintf('%s chan %d unit %d',basename(mfile),channel,unit),'FontSize',options.fontsize_labels);
set(ht,'Interpreter','none');
set(gca,'YTick',floor([linspace(1,freqcount,4) linspace(1,freqcount,4)+freqcount]));
set(gca,'YTickLabel',repmat(unique_freq(round(linspace(1,freqcount,4))),[2 1]));
set(gca,'XTick',floor(linspace(1,freqcount,3)));
set(gca,'XTickLabel',unique_freq(round(linspace(1,freqcount,3))));

axis image

subplot(1,3,2);
imagesc(mm_linpred.*abs(mm_linpred),[-1 1].*mmax_linpred.^2);
title('linear prediction','FontSize',options.fontsize_labels);
set(gca,'YTick',floor([linspace(1,freqcount,4) linspace(1,freqcount,4)+freqcount]));
set(gca,'YTickLabel',repmat(unique_freq(round(linspace(1,freqcount,4))),[2 1]));
set(gca,'XTick',floor(linspace(1,freqcount,3)));
set(gca,'XTickLabel',unique_freq(round(linspace(1,freqcount,3))));

axis image

d_on=diag(on_mat);
d_on_sum=sum(on_mat,2);
d_on_sum=d_on_sum./sum(d_on_sum).*sum(d_on);
d_off=diag(off_mat);
d_off_sum=sum(off_mat,2);
d_off_sum=d_off_sum./sum(d_off_sum).*sum(d_off);

subplot(2,3,3);
semilogx(unique_freq,d_on);
hold on
semilogx(unique_freq,d_on_sum,'r');
hold off

subplot(2,3,6);
semilogx(unique_freq,d_off);
hold on
semilogx(unique_freq,d_off_sum,'r');
hold off

varargout{1}=on_mat;
varargout{2}=off_mat;
varargout{3}=unique_freq;
varargout{4}=res;
