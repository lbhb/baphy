function r=online_batch(mfile,analysis_name,options)
%% function r=online_batch(mfile,analysis_name,options);
%
% options.Electrodes
% options.showinc
% options.datause
% options.sigthreshold
% options.psth
% options.lfp

global BATCH_FIGURE_HANDLE USECOMMONREFERENCE

%% PARSE PARAMETERS
if ~exist('analysis_name','var')   analysis_name='strf'; end
if ~any(strcmpi(analysis_name,{'strf','raster','rawtrace','correlation'})),  error('unknown analysis.'); end
if ~exist('options','var')  options=[]; end
if ~isfield(options,'showinc')    options.showinc=0;  end
if ~isfield(options,'datause'),    options.datause='Both'; end
if ~isfield(options,'sigthreshold'),  options.sigthreshold=4; end
if ~isfield(options,'psth'),  options.psth=0; end
if ~isfield(options,'psthfs') || isempty(options.psthfs) || options.psthfs<=0,  options.psthfs=30; end
if ~isfield(options,'lfp'),  options.lfp=0; end
if ~isfield(options,'compact'), options.compact=0; end
if ~isfield(options,'ElectrodeMatrix'), options.ElectrodeMatrix=[]; end
if ~isfield(options,'subplot_by_geometry'), options.subplot_by_geometry=false; end
if ~isfield(options,'sortedunit'),    options.sortedunit=[]; end
options.compactFontSize=10;

[pp,filename,ext]=fileparts(mfile); LoadMFile(mfile);
if exist('MFileDone','var') && ~MFileDone,
    warning('MFileDone==0. M file not complete? Pausing 5 seconds'); pause(5); return
end

%% PARSE ELECTRODE SELECTION
chancount=globalparams.NumberOfElectrodes;
if ~isfield(options,'Electrodes')  options.Electrodes=1:chancount;
else  options.Electrodes=options.Electrodes(:)';  end
Electrodes=options.Electrodes;

if options.usesorted && isempty(options.sortedunit)
    UniqueElectrodes = unique(Electrodes);
    for ii = 1:length(UniqueElectrodes);
        ElectrodesUnits{ii}=find(UniqueElectrodes(ii) == Electrodes);
    end
    Electrodes = UniqueElectrodes;
end
if ~isempty(options.ElectrodeMatrix) && sum(ismember(Electrodes,options.ElectrodeMatrix))<length(Electrodes) && ~ isequal(options.ElectrodeMatrix,0),
    errordlg('Channel missing from Grid'); r=1; return;
end
if ~isfield(options,'unit') options.unit=ones(size(Electrodes)); end

%% GET ARRAY OR PLOTTING GEOMETRY
NElectrodes = length(Electrodes); % Number of Electrodes to plot
use_plotinds=0;
if(options.subplot_by_geometry)
    %do nothing
    ElectrodesXY=[];
elseif(isequal(options.ElectrodeMatrix,0) || NElectrodes <=4)
    use_plotinds=1;
    plotinds=1:NElectrodes;
    rows=floor(sqrt(NElectrodes))-1;
    if(rows==0)
        rows=1;
    end
    cols=ceil(sqrt(NElectrodes));
    while(rows*cols<NElectrodes)
        cols=cols+1;
    end
    for i=1:NElectrodes
        ElectrodesXY(Electrodes(i),1)=mod(i-1,cols)+1;
        ElectrodesXY(Electrodes(i),2)=rows-ceil(i/cols)+1;
        plotXY(plotinds(i),1)=mod(i-1,cols)+1;
        plotXY(plotinds(i),2)=rows-ceil(i/cols)+1;
    end
elseif isempty(options.ElectrodeMatrix) % IF USER HAS NOT SPECIFIED A DIFFERENT GRID
    try,
        [ElecGeom,Elec2Chan] = ...
            MD_getElectrodeGeometry('Identifier',filename,'FilePath',pp);
        ChannelsXY = reshape([ElecGeom.ChannelXY],2,numel([ElecGeom.ChannelXY])/2)';
        ElectrodesXY = ChannelsXY(Elec2Chan,:);
        if(size(ChannelsXY,1)==2)
            ChannelsXY(:,2)=1;
        end
        if(size(ElectrodesXY,1)==2)
            ElectrodesXY(:,2)=1;
        end
    catch err
        ChannelsXY=[1 1; 1 2; 2 1; 2 2];
        ElectrodesXY=[1 1; 1 2; 2 1; 2 2];
    end
    NElectrodes = size(ElectrodesXY,1);
else % ELECTRODE MATRIX HAS BEEN SPECIFIED AS GRID
    EM = flipud(options.ElectrodeMatrix);
    for i=1:length(Electrodes)
        [cY,cX] = find(EM==Electrodes(i)); ElectrodesXY(Electrodes(i),1:2) = [cX,cY];
    end
end
if use_plotinds
    Tiling  = max(plotXY,[],1);
else
    Tiling  = max(ElectrodesXY,[],1);
end
if length(ElectrodesXY)<4 && 0,
    Tiling(end)=size(ElectrodesXY,1);
    ElectrodesXY(:,2)=ElectrodesXY(:,2)-min(ElectrodesXY(:,2))+1;
end
%% SETUP FIGURE
if isempty(BATCH_FIGURE_HANDLE)
    SS = get(0,'ScreenSize');
       if verLessThan('matlab','8.4')
           yoffset=50;
       else
           yoffset=80;
       end
       pos=[10,2*SS(4)/3-yoffset,SS(3:4)/3];
    BATCH_FIGURE_HANDLE=figure;  set(gcf,'Pos',pos); ReuseFigure = 0;
else
    figure(BATCH_FIGURE_HANDLE); ReuseFigure = 1;
end

set(BATCH_FIGURE_HANDLE,'Name',sprintf('%s',[filename,ext]),'KeyPressFcn',{@LF_KeyPress});
if(options.subplot_by_geometry)
    %do this now so that UH buttons are not cleared by subplot1
    [AH,DC]=probe_64D_axes(Electrodes,BATCH_FIGURE_HANDLE);
else
    if use_plotinds
        cols=max(plotXY(:,1));  rows=max(plotXY(:,2));
    else
        cols=max(ElectrodesXY(:,1));  rows=max(ElectrodesXY(:,2));
    end
    if Tiling(1)==1
        set(BATCH_FIGURE_HANDLE,'PaperPosition',[1 1 cols*2 rows]);
    elseif max(Tiling)<=4,
        set(BATCH_FIGURE_HANDLE,'PaperPosition',[1 1 cols*2 rows*2]);
    else
        set(BATCH_FIGURE_HANDLE,'PaperPosition',[0 0 cols rows]);
    end
end
global CPOS; if ~isempty(CPOS) set(BATCH_FIGURE_HANDLE,'Position',CPOS); end
UH(1)=uicontrol('style','pushbutton','Units','Normalized','Position',[0.01,0.01,0.08,0.02],...
    'String','Save Size','Callback','global CPOS; CPOS = get(gcf,''Position'');');
UH(2)=uicontrol('style','pushbutton','Units','Normalized','Position',[0.1,0.01,0.08,0.02],...
    'String','Print Color','Foregroundcolor','red','Callback','set(gcf,''PaperSize'',[8.5,11],''PaperPosition'',[0.8,0.8,7,9.4]); print(gcf,''-Pcolor''); set(gco,''ForegroundColor'',''black'');');
UH(3)=uicontrol('style','pushbutton','Units','Normalized','Position',[0.19,0.01,0.08,0.02],...
    'String','Print B/W','Foregroundcolor','red','Callback','set(gcf,''PaperSize'',[8.5,11],''PaperPosition'',[0.8,0.8,7,9.4]); print(gcf,''-Plyra''); set(gco,''ForegroundColor'',''black'');');
UT=uicontrol('style','text','string',sprintf('%s',[filename,ext]),'Units','Normalized', ...
    'Position',[0.8 0.005 0.18 0.04],'HorizontalAlignment','right');

%% SETUP AXES
% DEFINE SEPARATION
if options.compact
    if(NElectrodes==2)
        Sep = [0.2,0.2];
    else
        Sep = [0.2,0.35];
    end
else
    Sep = [0.4,0.8];
end
if(any(strcmp(analysis_name,{'correlation'})) || ...
        (any(strcmp(analysis_name,{'rawtrace'})) && NElectrodes==1 && length(options.StimInds)>1) )
    options.mergesubplots=true;
end

if(~any(strcmp(analysis_name,{'rawtrace','correlation'})))
    if(any(strcmp(analysis_name,{'raster'})) && options.lfp )
    else
        options.mergesubplots=false;
    end
end
%Tiling is [cols,rows]
if(options.mergesubplots)
    if(isempty(options.StimInds))
        Tiling=[1,1];
    else
        Tiling=[1,length(options.StimInds)];
    end
end


%AH is axes handle. Each row is for an electrode, each column is for
%a different plot for that electrode (added for plotting different stimulus
%options.subplot_by_geometry=false;
yticklabel_rotation=30;
if options.subplot_by_geometry
    label_axis=1;
else
    % DEFINE INSET
    OverHang = (1 ./ (Tiling-0.3) .* Sep ./ (1+Sep));
    Range(1:2,1) = 0.7*OverHang;
    Range(1:2,2) = 1-OverHang;
    
    label_axis = (Tiling(2)-1)*Tiling(1)+1;
    if Tiling(1)==1 && Tiling(2)==1,
        DCAll={[0.13 0.11 0.775 0.815]};
    else
        DCAll = HF_axesDivide(Tiling(1),Tiling(2),Range(1,1:2),Range(2,1:2),Sep(1),Sep(2));
    end
    if ~ReuseFigure
        % CREATE NEW AXES
        if(options.mergesubplots)
            figure(BATCH_FIGURE_HANDLE);
            if(isempty(options.StimInds))
                AH(1,1)=axes('Position',DCAll{1},'FontSize',6);
            else
                for ii=1:length(options.StimInds)
                    AH(1,ii)=axes('Position',DCAll{ii},'FontSize',6);
                end
            end
            for ii=2:NElectrodes
                AH(ii,:)=AH(1,:);
            end
        else
            
            for ii=1:NElectrodes
                Electrode = Electrodes(ii);
                if NElectrodes==1,
                    DC{ii}=[0.13 0.11 0.775 0.815];
                else
                    if use_plotinds
                        DC{ii} = DCAll{end-round(plotXY(plotinds(ii),2))+1,round(plotXY(plotinds(ii),1))};
                    else
                        DC{ii} = DCAll{end-round(ElectrodesXY(Electrode,2))+1,round(ElectrodesXY(Electrode,1))};
                    end
                end
                if (NElectrodes < Tiling(1)*Tiling(2)) && (ii > (Tiling(2)-1)*Tiling(1))
                    %If there's space, move plots over one so you can see the labels
                    DC{ii}(1) = DC{ii}(1) + DC{2}(1)-DC{1}(1); 
                    yticklabel_rotation=0;
                end                
                AH(ii,1) = axes('Position',DC{ii},'FontSize',6,'Parent',BATCH_FIGURE_HANDLE);
            end
        end
    else % REUSE AXES
        for ii=1:NElectrodes % Recreate DC for opto (division of AH for Light/NoLight)
            DC{ii} = DCAll{end-round(ElectrodesXY(Electrodes(ii),2))+1,round(ElectrodesXY(Electrodes(ii),1))};
        end
        AH = get(BATCH_FIGURE_HANDLE,'Children');
        Types = get(AH,'Type'); Ind = strcmp(Types,'axes');
        AH = sort(AH(Ind));
    end
    
end
drawnow;

switch analysis_name,
    case 'correlation'
        options.ah=AH(1,:);
        if options.usesorted
            error('Not impemented for sorted cells yet')
        end
        plot_raster_correlation_across_electrodes(mfile,Electrodes,options.unit,options);
        set(BATCH_FIGURE_HANDLE,'ToolBar','figure','Position',[10   50   379   360])
        do_loop=false;
    otherwise
        do_loop=true;
end
%% START PLOTTING LOOP

if(do_loop)
    try clear global GPROPS; end
    for ii=1:NElectrodes
        if(~options.mergesubplots||ii==1)
            cla(AH(ii,:));
        end
        if(options.mergesubplots)
            for j=1:size(AH,2)
                hold(AH(ii,j),'on');
            end
        end
        options_this_plot = options;
        % OPTION FOR SORTED DATA
        if ~options.usesorted
            Electrode=Electrodes(ii);
            unit=options.unit(ii);
        else
            if(~isempty(options.sortedunit))
                Electrode=Electrodes(ii);
                unit= options.sortedunit(ii);
                options_this_plot.sortidx = options.sortidx(ii);
            else
                Electrode=Electrodes(ii);
                SortedUnits = options.unit(ElectrodesUnits{ii});
                if(0)
                    options.sortedunit=SortedUnits(1);
                    unit=SortedUnits(1);
                elseif(isequal(SortedUnits,0))
                    disp(['No Sorted Units on Electrode ', num2str(Electrode), ', skipping.']);
                    r=[];
                    return
                else
                    disp(['Sorted Units on Electrode ', num2str(Electrode), ': ',num2str(SortedUnits)]);
                    if length(SortedUnits) > 1
                        if(strcmp(analysis_name,'rawtrace'))
                            unit=SortedUnits;
                        else
                        unit=[];
                        while(isempty(unit))
                            try
                                unit = input('Choose a unit: ');
                            catch err
                                warning(err)
                                unit=[];
                            end
                        end
                        end
                    else
                        unit=SortedUnits;
                    end
                    options_this_plot.sortedunit = unit;
                end
            end
        end
        
        %% PLOT DIFFERENT ANALYSES
        switch analysis_name,
            case 'strf'
                if strcmpi(options_this_plot.runclass,'ALM'),
                    % for audio-visual stimuli, special analysis
                    alm_online(mfile,Electrode,unit,AH(ii,:),options_this_plot);
                    if ii<NElectrodes && options_this_plot.compact,
                        legend off
                    end
                    
                    % BIASED SHEPARD PAIR
                elseif strcmpi(options_this_plot.runclass,'BSP'),
                    MD_computeShepardTuning('MFile',mfile,'Electrode',Electrode,'Unit',unit,...
                        'Axis',AH(ii,:),'SigmaThreshold',options_this_plot.sigthreshold);
                    
                    % TONE CLOUD
                elseif strcmpi(options_this_plot.runclass,'TMG'),
                    % 14/04-YB: so far, plot a raster w/ trials sorted by FrozenPattern nb
                    %         TMG_ComputeSTRF('MFile',mfile,'Electrode',Electrode,'Unit',unit,...
                    %           'Axis',AH(ii,:),'SigmaThreshold',options_this_plot.sigthreshold);
                    TMG_RasterPlot('MFile',mfile,'Electrode',Electrode,'Unit',unit,...
                        'Axis',AH(ii,:),'SigmaThreshold',options_this_plot.sigthreshold);
                    
                    % Overlapping Nat Sounds, pre-run
                elseif strcmpi(options_this_plot.runclass,'OLP'),
                    % SVD 2022-10-13
                    %  plot_OLP_rate(parmfile, channels, 1);
                     plot_OLP_rate(mfile, Electrode, 1, AH(ii,:));
                     
                    
                elseif strcmpi(options_this_plot.runclass,'SPN') || strcmpi(options_this_plot.ReferenceClass,'SpNoise'),
                    options_this_plot.filtfmt='envelope';
                    options_this_plot.chancount=0;
                    options_this_plot.rasterfs=100;
                    % for speech and sporcs, use boosting to estimate strf
                    boost_online(mfile,Electrode,unit,AH(ii,:),options_this_plot);
                elseif strcmpi(options_this_plot.runclass,'PPS') || strcmpi(options_this_plot.ReferenceClass,'PipSequence'),
                    if(0)
                        
                        options_this_plot.filtfmt='parm';
                        options_this_plot.chancount=0;
                        options_this_plot.rasterfs=100;
                        % for tone pip sequence, use boosting to estimate STRF from Pip
                        % parameters
                        boost_online(mfile,Electrode,unit,AH(ii,:),options_this_plot);
                    else
                        [freqs,levels,FRA]=pps_fra_online(mfile,Electrode,unit,AH(ii,:),options_this_plot);
                        if(length(AH)==1)
                            colorbar
                            assignin('base','freqs',freqs)
                            assignin('base','levels',levels)
                            assignin('base','FRA',FRA)
                        end
                    end
                elseif strcmpi(options_this_plot.runclass,'CCH') || strcmpi(options_this_plot.ReferenceClass,'ComplexChord') || ...
                        strcmpi(options_this_plot.runclass,'FTC') || strcmpi(options_this_plot.runclass,'FLT') || ...
                        strcmpi(options_this_plot.runclass,'CPT') || ...
                        strcmpi(options_this_plot.runclass,'VWL')|| ...
                        strcmpi(options_this_plot.runclass,'VBN') ||...
                        strcmpi(options_this_plot.runclass,'FNS')
                    % two-tone 2nd-order tuning surface
                    chord_strf_online(mfile,Electrode,unit,AH(ii,:),options_this_plot);
                elseif strcmpi(options_this_plot.runclass,'BNB') || strcmpi(options_this_plot.ReferenceClass,'NoiseBurst'),
                    % two-tone 2nd-order tuning surface
                    bnb_online(mfile,Electrode,unit,AH(ii,:),options_this_plot);
                elseif strcmpi(options_this_plot.runclass,'RDT') || ...
                        strcmpi(options_this_plot.ReferenceClass,'NoiseSample'),
                    if strcmp(options_this_plot.datause,'Per trial') || strcmp(options_this_plot.datause,'Per trial pre-target'),
                        options_this_plot.filtfmt='qlspecgram';
                        options_this_plot.rasterfs=100;
                        
                        % for RDT NoiseSamples, use boosting to estimate strf
                        boost_online(mfile,Electrode,unit,AH(ii,:),options_this_plot);
                    else
                        chord_strf_online(mfile,Electrode,unit,AH(ii,:),options_this_plot);
                    end
                elseif strcmpi(options_this_plot.runclass(1:2),'SP') || strcmpi(options_this_plot.runclass,'SNS'),
                    % for speech and sporcs, use boosting to estimate strf
                    boost_online(mfile,Electrode,unit,AH(ii,:),options_this_plot);
                elseif strcmpi(options_this_plot.runclass,'VOC') || strcmpi(options_this_plot.runclass,'NAT'),
                    %options_this_plot.filtfmt='specgramv';
                    options_this_plot.filtfmt='gamma';
                    % for speech and sporcs, use boosting to estimate strf
                    boost_online(mfile,Electrode,unit,AH(ii,:),options_this_plot);
                elseif strcmpi(options_this_plot.runclass,'tst')  %for multi-level tuning
                    mltc_online(mfile,Electrode,unit,AH(ii,:),options_this_plot);
                elseif strcmpi(options_this_plot.runclass,'SSA')  %for multi-level tuning
                    options_this_plot.rasterfs=100;
                    options_this_plot.channel=Electrode;
                    ssa_psth(mfile,options_this_plot,AH(ii,:));
                elseif strcmp(options_this_plot.runclass,'RVT')
                    options_this_plot.rasterfs=1000;
                    rss_tuning_online(mfile,Electrode,unit,AH(ii,:),options_this_plot);
                elseif strcmp(options_this_plot.runclass,'TOR')
                    if ~options_this_plot.usesorted
                        % standard TORC strf
                        options_this_plot.usefirstcycle=0;
                        options_this_plot.tfrac = 1;
                        [strf,snr(ii)]=strf_online(mfile,Electrode,AH(ii,:),options_this_plot,DC{ii});
                    else
                        mfilename = [mfile,'.m'];
                        if isfield(options_this_plot,'spikefile'),
                            spikefile=options_this_plot.spikefile;
                        else
                            spkpath = mfile(1:strfind(mfile,filename)-1);
                            
                            if strcmp(computer,'PCWIN') || strcmp(computer,'PCWIN64')
                                spikefile = [spkpath,'sorted\',filename,'.spk.mat'];
                            else
                                spikefile = [spkpath,'sorted/',filename,'.spk.mat'];
                            end
                        end
                        [strf,snr(ii)]=strf_offline2(mfilename,spikefile,Electrode,unit,AH(ii,:),options_this_plot);
                    end
                else
                    chord_strf_online(mfile,Electrode,unit,AH(ii,:),options_this_plot);
                end
            case 'raster',
                if 0 && options_this_plot.lfp,
                    stim_spectrum(mfile,Electrode,AH(ii,:));
                elseif options_this_plot.lfp
                    if(options_this_plot.mergesubplots && NElectrodes>1)
                        %options_this_plot.varargin={'Color',pickcolor(ii)};
                        colors=copper(NElectrodes);
                        options_this_plot.varargin={'Color',colors(ii,:)};
                        options_this_plot.plot_y_offset=100*(ii-1);
                    end
                    [lfp(:,ii,:),ph(ii,:),options_o]=psth_lfp(mfile,Electrode,unit,AH(ii,:),options_this_plot);
                elseif strcmpi(options_this_plot.runclass,'RDT')
                    options_this_plot.PreStimSilence=0.2;
                    options_this_plot.PostStimSilence=0.2;
                    raster_online(mfile,Electrode,unit,AH(ii,:),options_this_plot);
                elseif strcmpi(options_this_plot.datause,'Light/no light'),
                    % opto line colors:
                    spc={[0.5 0.5 0.5],[0.2 0.2 1]};
                    % shading colors:
                    %ssc={[0.7 0.7 0.7],[0 1 0]};
                    ssc={[0.7 0.7 0.7],[0.75 0.75 1]};  % blue light
                    options_this_plot.spc=spc;
                    options_this_plot.ssc=ssc;
                    raster_online(mfile,Electrode,unit,AH(ii,:),options_this_plot);
                else
                    if any(cellfun(@(x)~isempty(strfind(options_this_plot.runclass,x)),{'FTC','CXT'}))
                        options_this_plot.max_labels=14;
                    end
                    [r,tags,options_o]=raster_online(mfile,Electrode,unit,AH(ii,:),options_this_plot);
                    if isfield(options_o,'absolute_threshold')
                        absolute_thresholds(ii)=options_o.absolute_threshold;
                        if ii == NElectrodes
                            options_o.absolute_thresholds=absolute_thresholds;
                        end
                    end
                    if(isempty(r))
                        if ndims(r)==2
                            R(:,:,ii)=0;
                        elseif ndims(r)==3
                            R(:,:,:,ii)=0;
                        else
                            error('add more cases here if needed')
                        end
                    else
                        if exist('R','var') && isequal(R,0)
                            R=[];
                        end
                        if ndims(r)==2
                            if exist('R','var') && (size(R,1)~=size(r,1) || size(R,2)~=size(r,2))
                                warning('raster for plot #%d not the same size as that for #1, not assigning raster to base workspace.',ii)
                            else
                                R(:,:,ii)=r;
                            end
                        elseif ndims(r)==3
                            R(:,:,:,ii)=r;
                        else
                            error('add more cases here if needed')
                        end
                    end
                end
            case 'rawtrace',
                options_this_plot.rawtrace=1;
                options_this_plot.rasterfs=NaN; % This means don't resmaple raw trace. If a different sampling frequency is desired, set it here.
                if(NElectrodes==1)
                    options_this_plot.Nreps=4;
                else
                    options_this_plot.Nreps=1;
                    options_this_plot.varargin={'Color',pickcolor(ii)};
                end
                raster_online(mfile,Electrode,unit,AH(ii,:),options_this_plot);
        end
        if ~options.subplot_by_geometry && length(AH)>6
            if ii == label_axis
                if options_this_plot.compactFontSize<get(AH(ii,:),'FontSize')
                    set(AH(ii,:),'FontSize',options_this_plot.compactFontSize)
                end
                set(AH(ii,:),'YTickLabelRotation',yticklabel_rotation)
            else
                set(AH(ii,:),'XTickLabel',[],'YTickLabel',[]);
            end
        end
        set([gca;get(gca,'Title');get(gca,'Children')],'ButtonDownFcn',{@AxesClickCallback,AH(label_axis)})
        drawnow;
        if exist('snr','var') fprintf('Electrode %d snr=%.3f\n',ii,snr(ii)); end
    end
end
switch analysis_name
    case 'raster'
        if(exist('R','var'))
            vns={'mfile','R','tags','options_o', 'Electrodes','globalparams','exptparams','exptevents',};
            for i=1:length(vns)
                eval(['assignin(''base'',''' vns{i} ''',' vns{i} ');'])
            end
            vn_str= cellfun(@(x)[x,', '],vns,'Uni',0);
            vn_str = [vn_str{:}]; vn_str(end-1:end)=[];
            fprintf(['Saved variables from this plot to base workspase: {',vn_str,'}\n'])
        end
end
set(BATCH_FIGURE_HANDLE,'Color','w')

if(exist('AH','var'))
    assignin('base','AH',AH)
end

rmi=setdiff(1:length(AH),label_axis);
for i=rmi
    if options.subplot_by_geometry
        title(AH(i),'');
    end
    xlabel(AH(i),'');
    ylabel(AH(i),'');
end
if options.subplot_by_geometry
    set(AH(rmi),'XTickLabel','','YTickLabel','')
end


if isfield(globalparams,'ExperimentComplete'),
    r=globalparams.ExperimentComplete; disp('Experiment complete.');
else r=0;
end

if options.psth && strcmp(analysis_name,'raster') analysis_name = 'psth'; end
if strcmp(options.datause,'Per trial') && strcmp(analysis_name,'raster') analysis_name = 'trial_raster'; end
if options.usesorted && ~options.usetempsorted
    analysis_name=[analysis_name '_SU'];
elseif options.usesorted && options.usetempsorted
    analysis_name=[analysis_name '_SUtemp'];
elseif options.useOEPspikes
    analysis_name=[analysis_name '_OEP'];
end
    
if r && length(AH)>1
    %If not still collecting data, and there is commonality in plot titles,
    %remove commonality and put in figure name
    th=get(AH,'Title');
    titles = cellfun(@(x)get(x,'String'),th,'Uni',0);
    inds = cellfun(@(x)strfind(x,'th'),titles,'Uni',0);
    inds_empty = all(cellfun(@isempty,inds));
    if all(strcmp(titles{1}(inds{1}:end),cellfun(@(x,y)x(y:end),titles,inds,'Uni',0)))
        if inds_empty
            for i=1:length(titles)
                rep_ind = strfind(titles{i},'Rep');
                if ~isempty(rep_ind)
                    space_inds = strfind(titles{i},' ');
                    space_ind = space_inds(find(space_inds>rep_ind,1));
                    set(th{i},'String',titles{i}([1:rep_ind-1 space_ind+1:end]))
                end
            end
        else
            cellfun(@(x,y,z)set(x,'String',y(1:z-1)),th,titles,inds);
        end
        figure(BATCH_FIGURE_HANDLE);
        analysis_string = [basename(mfile) ' ' analysis_name ', ' titles{1}(inds{1}:end)];
        if ~options.usesorted && ~options.usetempsorted && ~options.useOEPspikes && USECOMMONREFERENCE
            analysis_string=[analysis_string ' ComRef'];
        end
        set(BATCH_FIGURE_HANDLE,'name', analysis_string)
    end
end
%% SETUP FOR PRINTING
if(options.save_figs)
    if size(AH,1)>4
        curr_pos=get(BATCH_FIGURE_HANDLE,'Position');
        Maximize(BATCH_FIGURE_HANDLE)
    end
    ah_fontsize=get(AH(1),'FontSize');
    set(AH(:),'FontSize',6)
    xtl=get(AH(label_axis),'XTickLabel');
    if length(xtl)>10 && length(AH)>10
        for i=1:length(AH)
            xtl=get(AH(i),'XTickLabel');
            xtl(2:2:end,:)=' ';
            set(AH(i),'XTickLabel',xtl)
        end
    end
    set(AH(:),'FontSize',6)
    if length(AH)>100
            ytl=get(AH(label_axis),'YTickLabel');
            ne=find(~cellfun(@isempty,ytl));
            if length(ne) > 10
                ytl(ne(2:2:end))='';
            end
            set(AH(label_axis),'YTickLabel',ytl)
    end
    if ~options.usesorted && ~options.usetempsorted && ~options.useOEPspikes && options.sigthreshold~=4
        thresh_str = strrep(sprintf('_%.2gstds',options.sigthreshold),'.','pt');
        analysis_name=[analysis_name,thresh_str];
    end
    baphy_remote_figsave(BATCH_FIGURE_HANDLE,UH,globalparams,analysis_name,options.psth)
    set(AH(:),'FontSize',ah_fontsize)
    if size(AH,1)>4
        set(BATCH_FIGURE_HANDLE,'Position',curr_pos);
    end
end
function LF_KeyPress(Obj,Event)

CC = get(Obj,'CurrentCharacter');
switch CC
    case {'+','-','='};
        if CC=='-'; Increment = -4; else Increment = +4; end
        C = get(Obj,'Children');
        for i=1:length(C)
            if strcmp(get(C(i),'Type'),'axes')
                P = get(C(i),'Children');
                for j=1:length(P)
                    if strcmp(get(P(j),'Type'),'line')
                        set(P(j),'MarkerSize',max([1,get(P(j),'MarkerSize')+Increment]));
                    end;
                end;
            end;
        end
    otherwise
end

% TEMPORARY CODE FOR UNSCRAMBLING EARLY DIGITAL RECORDINGS
% InMANTASelect = setdiff([1:96],[2:3:95]);
% BR = [1:2:63,2:2:64]; % New Bankselection Indices
% RM = [8 16 7 15 6 14 5 13 4 12 3 11 2 10 1 9 [8 16 7 15 6 14 5 13 4 12 3 11 2 10 1 9]+16]; % Within bank unscrambling
% RM64 = [RM,RM+32]; % Unscrambling across two banks
% Rec2ChanInd = BR(RM64); % Selection indices that should make current recorded channels into the unsscrambled channels
% Chan2ElecInd = [17:32,1:16,49:64,33:48]; % Selection indices that shoud transition from chans to electrodes
% ChanData = RawData(Rec2ChanInd); ElecData = ChanData(Rec2ChanInd);

function AxesClickCallback(Obj,Event,label_axes)
    H = gca; 
    xtl = get(label_axes,'XTickLabel');
    ytl = get(label_axes,'YTickLabel');
    xt = get(label_axes,'XTick');
    yt = get(label_axes,'YTick');
    xtlm = get(label_axes,'XTickLabelMode');
    ytlm = get(label_axes,'YTickLabelMode');
    xl = get(label_axes,'XLabel');
    yl = get(label_axes,'YLabel');
    NFig = figure; 
    set(NFig,'KeyPressFcn',{@LF_KeyPress});
    SS = get(0,'ScreenSize');
    set(NFig,'Pos',[10,3*SS(4)/4-100,SS(3:4)/4]);
    NH = copyobj(H,NFig); 
    set(NH,'Position',[0.15,0.1,0.85,0.8]);
    if strcmp(ytlm,'manual')
        set(NH,'YTickLabel',ytl,'YTick',yt);
    else
        set(NH,'YTickLabelMode','auto')
    end
    if strcmp(xtlm,'manual')
        set(NH,'XTickLabel',xtl,'XTick',xt);
    else
        set(NH,'XTickLabelMode','auto')
    end
    set(NH,'XLabel',xl,'YLabel',yl);
    set(NH,'FontSize',10)
    set(NFig,'Name',get(get(H,'Parent'),'Name'))
        
