function csd(mfile,options)
    %% CSD READING GUIDE
    % See references and examples in /auto/data/programs/Open Ephys/CSD/
    %
    % Note: CSDs are inverted in this function to get them to how most labs plot them
    % (Guo et al 2017 , Steinshneider et al 1992, and Pettersen et al 2006; 
    % Kaur et al 2005 does not invert). Thus, sinks are negative, but shown 
    % with light colors in colormaps.
    
    % "Current sinks represent net inward transmembrane current flow
    % associated with local depolarizing excitatory post-synaptic potentials 
    % or passive, circuit-completing current associated with hyperpolarizing 
    % potentials at adjacent sites. Conversely, current sources indicate sites 
    % of net outward current flow associated either with active hyperpolarization 
    % or with passive current return for depolarizing potentials at adjacent sites. 
    % The corresponding MUA profile helps to distinguish these possibilities: 
    % current sinks coincident with increases in MUA in corresponding electrode 
    % channels likely reflect depolarizing post-synaptic activity, whereas 
    % current sources coincident with reductions in MUA likely represent 
    % hyperpolarization." 
    % (Fishman et al 2001 Consonance and Dissonance of Musical Chords)
    
    % "Noise-evoked columnar CSD patterns were used to determine the location of the A1 recording channel. Two CSD signatures were
    % used to identify L4: A brief current sink first occurs approximately 10 ms after the noise onset, which was used to determine the lower
    % border of L4 (Kaur et al., 2005). A triphasic CSD pattern (sink-source-sink from upper to lower channels) occurs between 20 ms and
    % 50 ms, where the border between the upper sink and the source was used to define the upper boundary of L4. Normally, 2 channels
    % were assigned to L4. Other layers were defined relative to the location of L4 (L2/3: 3 channels above L4; L5: 3 channels below L4; L6: 3
    % channels below L5). CSD-derived layer assignments were cross-validated against sound-evoked MUA response patterns, where L4
    % and L5 units responded with higher firing rates and shorter latency."
    % (Guo et al 2017)


%%
if ~isfield(options,'channel'),
    options.channel=1;
end

if ~exist('options','var'),
    options=[];
end
if ~isfield(options,'psthfs'),
    options.psthfs=20;
end
if ~isfield(options,'StimInds'),
    options.StimInds={1};
end
if ~isfield(options,'rasterfs'),
    options.rasterfs=max(1000,options.psthfs);
end
if ~isfield(options,'sigthreshold')
    options.sigthreshold=4;
end
if ~isfield(options,'datause'),
    options.datause='Reference';
end
if ~isfield(options,'psth'),
    options.psth=0;
end
if ~isfield(options,'psthfs'),
    options.psthfs=20;
end
if ~isfield(options,'lfp'),
    options.lfp=0;
end
if ~isfield(options,'usesorted'),
    options.usesorted=0;
end
%% CSD options
options.time_upsamples=3;

options.bad_channels=getparm(options,'bad_channels',[]);
options.varargin=getparm(options,'varargin',{});
options.plot_y_offset=getparm(options,'plot_y_offset',0);
options.add_MUA=getparm(options,'add_MUA',1);
options.spatial_gap=getparm(options,'spatial_gap',50);
options.spatial_smooth_points=getparm(options,'spatial_smooth_points',3);
options.spatial_smooth_passes=getparm(options,'spatial_smooth_passes',2);

bad_channels=false(length(options.channel),1);
LoadMFile(mfile);

%% get tip depth from celldB
if(1)
    dbopen;
    sql=['SELECT gCellMaster.*,gPenetration.numchans,gPenetration.probenotes',...
        ' FROM gPenetration,gCellMaster',...
        ' WHERE gCellMaster.penid=gPenetration.id',...
        ' AND gPenetration.animal like "',globalparams.Ferret,'"',...
        ' AND gCellMaster.siteid like "',globalparams.SiteID,'"'];
    sitedata=mysql(sql);
    if isempty(sitedata.depth)
        warning('Tip depth not found, using 0.')
        tip_depth=0;
    elseif isnumeric(sitedata.depth) && ~isa(sitedata.depth,'uint8')
        if(sitedata.depth>1e5)
            tip_depth=sitedata.depth/1e63;
        else
            tip_depth=sitedata.depth;
        end
    else
        tipdepths=strsep(char(sitedata.depth),',');
        if(isempty(tipdepths))
            warning('Tip depth not found, using 0.')
            tip_depth=0;
        else
            tip_depth=tipdepths{1};
        end
    end
else
    tip_depth=0;
end


options.round_factor=getparm(options,'round_factor',10);
%% Set parameters
param(1).text='Bad Channels';
param(1).style='edit';
param(1).default=options.bad_channels;
param(2).text='Round factor';
param(2).style='edit';
param(2).default=options.round_factor;
param(3).text='Spatial gap';
param(3).style='edit';
param(3).default=options.spatial_gap;
param(4).text='Add MUA';
param(4).style='checkbox';
param(4).default=options.add_MUA;
param(5).text='Probe name';
param(5).style='edit';
param(5).default='Unknown';
param(6).text='Help';
param(6).style='info_button';
param(6).default={'CSD HELP:',...
    'See references and examples in /auto/data/programs/Open Ephys/CSD/',...
    '',...
    'CSDs are inverted in this function to get them to how most labs plot them (Guo et al 2017 , Steinshneider et al 1992, and Pettersen et al 2006;Kaur et al 2005 does not invert). Thus, sinks are negative, but shown with light colors in colormaps.',...
    '',...
    '"Current sinks represent net inward transmembrane current flow associated with local depolarizing excitatory post-synaptic potentials or passive, circuit-completing current associated with hyperpolarizing potentials at adjacent sites. Conversely, current sources indicate sites of net outward current flow associated either with active hyperpolarization or with passive current return for depolarizing potentials at adjacent sites. The corresponding MUA profile helps to distinguish these possibilities: current sinks coincident with increases in MUA in corresponding electrode channels likely reflect depolarizing post-synaptic activity, whereas current sources coincident with reductions in MUA likely represent hyperpolarization." (Fishman et al 2001 Consonance and Dissonance of Musical Chords)',...
    '',...
    '"Noise-evoked columnar CSD patterns were used to determine the location of the A1 recording channel. Two CSD signatures were used to identify L4: A brief current sink first occurs approximately 10 ms after the noise onset, which was used to determine the lower border of L4 (Kaur et al., 2005). A triphasic CSD pattern (sink-source-sink from upper to lower channels) occurs between 20 ms and 50 ms, where the border between the upper sink and the source was used to define the upper boundary of L4. Normally, 2 channels were assigned to L4. Other layers were defined relative to the location of L4 (L2/3: 3 channels above L4; L5: 3 channels below L4; L6: 3 channels below L5). CSD-derived layer assignments were cross-validated against sound-evoked MUA response patterns, where L4 and L5 units responded with higher firing rates and shorter latency." (Guo et al 2017)',...
    };

UserInput = ParameterGUI(param,'CSD Parameters','bold','center');
options.bad_channels=UserInput{1};
options.round_factor=UserInput{2};
options.spatial_gap=UserInput{3};
options.add_MUA=UserInput{4};
options.probe_name=UserInput{5};

bad_channels=false(length(options.channel),1);
bad_channels(options.bad_channels)=true;

fprintf('%s: Analyzing channel %s (rasterfs %d, spike thresh %.1f std)\n',...
    mfilename,num2str(options.channel),options.rasterfs,options.sigthreshold);

evpfile = globalparams.evpfilename;
[pp,bb,ee]=fileparts(evpfile);
bbpref=strsep(bb,'_');
bbpref=bbpref{1};
checkrawevp=[pp filesep 'raw' filesep bbpref filesep bb '.001.1' ee];
if exist(checkrawevp,'file'),
    evpfile=checkrawevp;
end
checktgzevp=[pp filesep 'raw' filesep bbpref '.tgz'];
if exist(checktgzevp,'file'),
    evpfile=checktgzevp;
    if 1 %lfp>0,
        evpv=evpversion(globalparams);
        evpfile=evpmakelocal(checktgzevp,evpv);
    end
end

runinfo=rawgetinfo(evpfile,globalparams);

%% find depth of each electrode
if strcmp(runinfo.electrode, 'unknown')
    if strcmp(options.probe_name, 'Unknown')
        error('Must specify a probe geometry, e.g. 64D_slot1')
    else
        probe_name=options.probe_name; 
    end
else
    probe_name=runinfo.electrode;
end
switch probe_name
    case {'64D_slot1','64D_slot2'}
        [s,UCLA_to_OEP]=probe_64D();
    case {'64D_slot1_bottom', '64D_slot2_bottom'}
        [s, UCLA_to_OEP]=probe_64D_bottom();
    case {'64M_slot1','64M_slot2'}
        [s,UCLA_to_OEP]=probe_64M();
    case {'128D'}
        [s, UCLA_to_OEP]=probe_128D();
    case {'128P_bottom'}
        [s, UCLA_to_OEP]=probe_128P_bottom_csd();
    otherwise
        error([probe_name,' is an unknown electrode. Edit Settings.xml'])
end


% if globalparams.NumberOfElectrodes==64
%     [s,UCLA_to_OEP]=probe_64D();
%     options.round_factor=getparm(options,'round_factor',10);
% elseif globalparams.NumberOfElectrodes==128
%     [s,~]=probe_128D();
%     [~,si]=sort(s.z+s.x/1000);
%     UCLA_to_OEP=si;
%     options.round_factor=getparm(options,'round_factor',100);
% else
%     error('No probe map known for %d electrodes',globalparams.NumberOfElectrodes)
% end


options.PreStimSilence=exptparams.TrialObject.ReferenceHandle.PreStimSilence;
options.PostStimSilence=exptparams.TrialObject.ReferenceHandle.PostStimSilence;
options.includeprestim=1;

disp('Loading response...');
%unit=ones(size(options.channel));
%[lfp(:,ii,tags]=raster_load(mfile,options.channel,unit,options);
% Alogithm: get raw data in [time,electrode], sampled at 1000 Hz, filtered
% 1 Hz to 150 Hz by 2nd order butterworth highpass and lowpass filters.
[~,~,~,~,rl,ltrialidx]=evpread(evpfile,'spikeelecs',[],'lfpelecs',...
    options.channel,'SRlfp',options.psthfs,'runinfo',runinfo,...
    'globalparams',globalparams);
% Alogithm: reformat into [time,electrode, trial] using trial start
% indicies in ltrialidx
LFP = NaN*zeros(max(diff(ltrialidx)),length(options.channel),length(ltrialidx)-1);
for i=1:length(ltrialidx)-1
    LFP(1:diff(ltrialidx(i:i+1)),:,i) = rl(ltrialidx(i):ltrialidx(i+1)-1,:);
end
% Algorithm: average across trials
lfp = nanmean(LFP,3);
freq_str='';
% basestd=nanstd(rl(:,:,:));
% mbasestd=repmat(nanmedian(basestd),1,size(rl,2));
% badidx=find(basestd>mbasestd*1.2);
% fprintf('%d/%d reps marked bad for artifacts\n',length(badidx),numel(basestd));
% rl(:,badidx)=nan;


% if(length(options.freqs)>1)
%                 freq_str=[', ',num2str([options.freqs{options.StimInds{1}}])];
%             else
%                 freq_str='';
%             end
%             xl=[-.01 .1];
%             if options.mergesubplots
%                 str=[strrep(basename(globalparams.mfilename(1:end-1)),'_','\_'),'LFP',freq_str];
%                 legend(ph,cellfun(@(x)['Ch ',num2str(x)],num2cell(options.channel),'Uni',0));
%                 set(AH(1,:),'XLim',xl)
%                 title(AH(1),str)
%             end

stimi=options.StimInds;
stimi_full=options.StimInds;
stimi_plot=1;
L=size(lfp,1);
time=((1:L)./options.rasterfs-options.PreStimSilence)*1000;
fs=18;
fs_a=14;

if(0)
    if(0)%triangular smoothing across channels
        lfp_p=lfp(:,:,stimi_plot);
    else
        lfp_p=fastsmooth(lfp(:,:,stimi_plot)',3,2,1)';
    end
    figure;imagesc(time,tip_depth-depth,lfp_p');
    ax=gca;
    c=parula(100);c(end,:)=1;colormap(c);
    cb=colorbar;
    ylabel(cb,'LFP','FontSize',fs)
    cl=get(gca,'CLim');
    set(ax,'CLim',[-1 1]*max(abs(cl)));
    set(ax,'XLim',xl,'YLim',tip_depth-depth([end 1])+[-1;1]*diff(depth(1:2))/2)
    xlabel('Time (ms)','FontSize',fs);
    ylabel('Depth (\mum)','FontSize',fs);
    set(ax,'FontSize',fs_a);
    set(gca,'TickDir','out','Box','off')
    title(['LFP ',str,', LFP z-smoothed'],'FontSize',fs)
end



%% Compute CSD
depths=s.z(UCLA_to_OEP(options.channel))+s.tipelectrode;
x=s.x(UCLA_to_OEP(options.channel));

x=round(x/options.round_factor)*options.round_factor;
[unx,~,uni]=unique(x);

% Remove columns with just one electrode (for 128D)
unx(arrayfun(@(a)sum(x==a)==1,unx))=[];

%remove all but a single column (for presentation plots)
%unx(unx~=-20)=[];

if(0) %normalize by impedance
    imp=impedance_changes();
    lfpn=lfp./repmat([imp.c(UCLA_to_OEP(options.channel)).magnitude]/1000000,size(lfp,1),1);
elseif(0) %normalize by pre-stim RMS
    nf=UTrms(lfp(1:400,:)');
    nf=nf/mean(nf);
    lfpn=lfp./repmat(nf',size(lfp,1),1);
else
    lfpn=lfp;
end
time=((1:L*options.time_upsamples)/options.time_upsamples./options.rasterfs-options.PreStimSilence)*1000;
xl=[-10 100];%ms
fh=figure;
if length(unx) > 3
    xl=[0 50];
    set(fh,'Position',[ 5 55 1500 1062]);
else
    set(fh,'Position',[ 5 55 948 1062]);
end
ax=subplot1(1,length(unx),'Gap',[0.01 0],'Max',[.87 .95]);
%Alorithm: loop over each column of electrodes
for xi=1:length(unx)
    %Alorithm: find electrodes in this column and their depths
    E_mask=x==unx(xi) & ~bad_channels; %Electrode mask
    depth=depths(E_mask);
    depth_interp_increments=1;%microns
    depth_interp=depth(1):depth_interp_increments:depth(end);
    
    difs=diff(depth_interp(1:end-1),1);
    m='spline';%m='cubic';
    %initialize CSDgs (gap-smoothed CSD), [time,x-position] with an x
    %position for each electrode in the column
%     CSDgs=inf(L*options.time_upsamples,sum(E_mask),length(stimi));
%     for ii=1%:length(stimi)
%         % Algorithm: smooth over space, by default use a 3-point moving average with 2 passes.
%         % Then interploate in 2d (spline): In time, upsample by a factor of 3, in space interpolate to 1 um resolution
%         % This creates R4, which is still the LFP in [time,x-position], but upsampled
%         R4=interp2(1:L,depth',fastsmooth(lfpn(:,E_mask)',options.spatial_smooth_points,options.spatial_smooth_passes,1),[1:L*options.time_upsamples]/options.time_upsamples,depth_interp',m)';
%         %For each electrode in the column:
%         for j=1:sum(E_mask)
%             di=find(depth_interp(2:end-1)==depth(j));
%             if(~isempty(di)&&di>options.spatial_gap&&di<=(size(R4,2)-options.spatial_gap))
%                 %Algoritm: if the data exists to do so (not on edges in space):
%                 % calculate the 2nd derivative over space. Use a "gapped
%                 % derivative", deault 50 um spacing
%                 % so [LFP(x+50)-LFP(x)] - [LFP(x) - LFP(x-50)]
%                 % normalize by the distance squared (50^2=2500)
%                 CSDgs(:,j,ii)=((R4(:,di+options.spatial_gap)-R4(:,di))-(R4(:,di)-R4(:,di-options.spatial_gap))) / (diff(depth_interp(di+[0 options.spatial_gap]))^2);
%             end
%         end
%     end
    
    
    %Algoritm: initialize CSDg (gap-smoothed CSD), [time,x-position] with an x
    %position for each interpolated depth in the column  
    CSDg{xi}=inf(L*options.time_upsamples,length(depth_interp)-2,length(stimi_full));
    for ii=1%:length(stimi_full)
        % Algorithm: (same as above) smooth over space, by default use a 3-point moving average with 2 passes.
        % Then interploate in 2d (spline): In time, upsample by a factor of 3, in space interpolate to 1 um resolution
        % This creates R4, which is still the LFP in [time,x-position], but upsampled
        R4=interp2(1:L,depth',fastsmooth(mean(lfpn(:,E_mask,stimi_full{ii}),3)',options.spatial_smooth_points,options.spatial_smooth_passes,1),[1:L*options.time_upsamples]/options.time_upsamples,depth_interp',m)';
        for j=options.spatial_gap+1:size(R4,2)-options.spatial_gap
                %Algoritm: if the data exists to do so (not on edges in space):
                % calculate the 2nd derivative over space. Use a "gapped
                % derivative", deault 50 um spacing
                % so [LFP(x+50)-LFP(x)] - [LFP(x) - LFP(x-50)]
                % normalize by the distance squared (50^2=2500)
                CSDg{xi}(:,j,ii)=((R4(:,j+options.spatial_gap)-R4(:,j))-(R4(:,j)-R4(:,j-options.spatial_gap))) / (diff(depth_interp(j+[0 options.spatial_gap]))^2);
        end
    end
    
    %Algorithm: convert from uV/(um)^2 to mV/(mm)^2
    %CSDgs=-1*CSDgs*10^6/10^3;
    CSDg{xi}=-1*CSDg{xi}*10^6/10^3;
    

    
    %%
    % Algorithm plot CSDg as an image. Zoom to -10 to 100 ms.
    pi=all(~isinf(CSDg{xi}));
    depth_=tip_depth-depth_interp(2:end-1);
    imagesc(time,depth_(pi),CSDg{xi}(:,pi)','Parent',ax(xi))
    cl(xi,:)=get(ax(xi),'CLim');
    set(ax(xi),'XLim',xl,'YLim',tip_depth-depths([end 1]))
    set(ax(xi),'FontSize',fs_a);
    set(ax(xi),'TickDir','out','Box','off')
    %title(['CSD ',str,', LFP z-smoothed, gapped-derivative: 50 um'],'FontSize',fs)
    title(ax(xi),['X=',num2str(unx(xi)),' \mum'])
    
    %Algorithm: load and plot multi-unit PSTHs for each electrode on top of CSD image
    if(options.add_MUA)
        chs=find(E_mask);
        for ii=1:length(chs)
            options2.channel=options.channel(chs(ii));
            options2.unit=1;
            options2.includeprestim=1;
            options2.rasterfs=2000;
            if ii==1
                [r,tags,trialset,exptevents]=loadevpraster(mfile,options2);
                r(:,:,2:length(options.channel))=nan;
            else
                rt = loadevpraster(mfile,options2);
                % check if multiple stimuli were played, if so, average
                % over all
                if size(rt, 3) > 1
                    warning('averaging over all stimuli in set!')
                    [r(:,:,ii)]=mean(loadevpraster(mfile,options2), 3);
                else
                    [r(:,:,ii)]=loadevpraster(mfile,options2);
                end
            end
        end
        rm=squeeze(mean(r,2));
        time2=((1:size(rm,1))./options2.rasterfs-options.PreStimSilence)*1000;
        if xi==1
            nf=mode(diff(depth))*.9/max(rm(:));
        end
        for ii=1:length(chs)
            ph(ii)=plot(time2,-1*rm(:,ii)*nf+(tip_depth-depth(ii)),'Color','k','LineWidth',1,'Parent',ax(xi));
        end
        a=2;
    end
    if(0)
        figure;imagesc(time,tip_depth-depth(2:end-1),CSDgs')
        ax=gca;
        try
            c=parula(100);
        catch
            c=jet(100);
        end
        c(end,:)=1;colormap(c);
        cb=colorbar;
        ylabel(cb,'CSD','FontSize',fs)
        %cl=get(gca,'CLim');
        set(ax,'CLim',[-1 1]*max(abs(cl)));
        set(ax,'XLim',xl,'YLim',tip_depth-depth([end 1]))
        xlabel('Time (ms)','FontSize',fs);
        ylabel('Depth (\mum)','FontSize',fs);
        set(ax,'FontSize',fs_a);
        set(gca,'TickDir','out','Box','off')
        title(['CSD ',str,', LFP z-smoothed, gapped-derivative: 50 um'],'FontSize',fs)
    end
    
    if(0)
        figure;imagesc(time,tip_depth-depth_interp,R4');
        ax=gca;
        try
            c=parula(100);
        catch
            c=jet(100);
        end
        c(end,:)=1;colormap(c);
        cb=colorbar;
        ylabel(cb,'LFP','FontSize',fs)
        cl=get(gca,'CLim');
        set(ax,'CLim',[-1 1]*max(abs(cl)));
        set(ax,'XLim',xl,'YLim',tip_depth-depth([end 1])+[-1;1]*diff(depth(1:2))/2)
        xlabel('Time (ms)','FontSize',fs);
        ylabel('Depth (\mum)','FontSize',fs);
        set(ax,'FontSize',fs_a);
        set(gca,'TickDir','out','Box','off')
        title(['LFP ',str,', LFP z-smoothed'],'FontSize',fs)
    end
end
%% 
% Algoritm: get  set color limits to -/+ the absolute max CSD over the zoomed time range,
t_inds=time>xl(1) & time<xl(2);
for i=1:length(CSDg)
    vals=CSDg{i}(t_inds,:);
    maxvals(i)=max(abs(vals(~isinf(vals))));
end
%set(ax,'CLim',[-1 1]*1.1*max(abs(cl(:))));
set(ax,'CLim',[-1 1]*max(maxvals));
%c=parula(256);
%c=jet(256);
c=flipud(pmkmp(256,'LinearL'));
%c(end,:)=1;
colormap(c);
axp=get(ax(end),'Position');
cb=colorbar('peer',ax(end),'FontSize',fs_a);
set(cb,'YDir','reverse') %reverse so sink is on top
set(ax(end),'Position',axp);
yl=ylabel(cb,'CSD (mV/mm^2)','FontSize',fs);
pause(.2);
set(yl,'Position',get(yl,'Position').*[.7 1 1])
xlabel(ax(1),'Time (ms)','FontSize',fs);
ylabel(ax(1),'Depth (\mum)','FontSize',fs);
linkaxes(ax)
set(ax,'YDir','reverse')
%set(ax,'XLim',[-10 100]);
E=options.channel(E_mask);
for i=1:sum(E_mask)
    th(i)=text(max(get(ax(end),'XLim')),tip_depth-depth(i),num2str(E(i)),'HorizontalAlignment','Left','VerticalAlignment','middle','FontSize',fs_a);
end
set(th,'Units','normalized');
thsi=text(max(get(ax(end),'XLim'))+diff(get(ax(end),'XLim'))*.26,min(get(ax(end),'YLim')),'Sink','HorizontalAlignment','Left','VerticalAlignment','bottom','FontSize',fs_a);
thso=text(max(get(ax(end),'XLim'))+diff(get(ax(end),'XLim'))*.26,max(get(ax(end),'YLim')),'Source','HorizontalAlignment','Left','VerticalAlignment','top','FontSize',fs_a);

str=[strrep(basename(globalparams.mfilename(1:end-1)),'_','\_'),'CSD',freq_str];
if(length(ax)>1)
    suptitle(str)
else
    title(str,'FontSize',fs)
end
set(fh,'Color','w')

if 0
   %for presentation plots 
   %single-column
    set(gcf,'Position',[5          55         550        1062])
    set(ax,'Position',[0.17    0.1    0.6    0.85])
    set(ax,'YLim',[800 1878])
    set(ax,'YLim',[420 1450])
    set(ax,'CLim',[-6 6])
    delete(th)
    for i=1:sum(E_mask)
        th(i)=text(max(get(ax(end),'XLim')),tip_depth-depth(i),num2str(E(i)),'HorizontalAlignment','Left','VerticalAlignment','middle','FontSize',fs_a);
    end
    saveas(gcf,['/auto/data/web/analysis/tartufo/2017/TAR010b01_p_BNB.CSD.fig'])
end
if(options.save_figs)
    str='';
    if options.round_factor==10
    else
        str=['_rf',num2str(options.round_factor)];
    end
    baphy_remote_figsave(fh,[],globalparams,['CSD',str],options.psth)
    curr_cl=get(ax(1),'CLim');
    set(ax,'CLim',[  -11.1457   11.1457])
    baphy_remote_figsave(fh,[],globalparams,['CSDscaled',str],options.psth)
    set(ax,'CLim',curr_cl)
end