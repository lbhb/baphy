function [freqs,levels,FRA]=pps_fra_online(parmfile,channel,unit,axeshandle,options)
%[freqs,levels,FRA]=pps_fra_online(parmfile,channel,unit,axeshandle,options)

options.channel=channel;
options.unit=unit;
options.filtfmt=getparm(options,'filtfmt','parm');
options.chancount=0;
options.rasterfs=getparm(options,'rasterfs',200);
options.datause='Reference Only';
options.runclass='PPS';

if exist([parmfile,'.m'],'file'),
    cellid=sprintf('%s chan %d unit %d',basename(parmfile),options.channel,options.unit);
    fprintf('%s: Analyzing channel %d (rasterfs %d)\n',...
        parmfile,options.channel,options.rasterfs);
    parmfiles={parmfile};
else
    cellid=parmfile;
    if options.pupil,
        cfd=dbgetscellfile('cellid',cellid,'runclass','PPS','pupil',2);
    else
        cfd=dbgetscellfile('cellid',cellid,'runclass','PPS');
    end
    parmfiles={};
    for ii=1:length(cfd),
        parmfiles{ii}=[cfd(ii).stimpath,cfd(ii).stimfile];
    end
    options.unit=cfd(ii).unit;
    options.channel=cfd(ii).channum;
end

LoadMFile(parmfiles{1});

options.PreStimSilence=exptparams.TrialObject.ReferenceHandle.PreStimSilence;
options.PostStimSilence=exptparams.TrialObject.ReferenceHandle.PostStimSilence;
options.ReferenceClass=exptparams.TrialObject.ReferenceClass;

PreBins=round(options.PreStimSilence.*options.rasterfs);
PostBins=round(options.PostStimSilence.*options.rasterfs);
unit=options.unit;
channel=options.channel;

Ar=[];
Astim=[];
Apr0=[];

for ii=1:length(parmfiles),
    disp('Loading response...');
    [r,tags,trialset]=raster_load(parmfiles{ii},options.channel,options.unit,options);
    realrepcount=size(r,2);
    
    if ~isempty(findstr(lower(options.datause),'per trial')),
        disp('Loading stimulus spectrogram...');
        %  options - struct with optional fields:
        %   filtfmt - currently can be 'wav' or 'specgram' or 'wav2aud' or
        %              'audspectrogram' or 'envelope' (collapsed over frequency)
        %   fsout - output sampling rate
        %   chancount - output number of channels
        toptions=struct('filtfmt',options.filtfmt,'fsout',options.rasterfs,...
            'chancount',options.chancount);
        [stim,stimparam]=loadstimbytrial(parmfiles{ii},toptions);
        stim=stim(:,1:size(r,1),trialset);
        r=permute(r,[1 3 2]);
    else
        disp('Loading stimulus spectrogram...');
        [stim,stimparam]=loadstimfrombaphy(parmfiles{ii},[],[],options.filtfmt, ...
            options.rasterfs,options.chancount,0,0);
    end
    
    stim=reshape(stim,size(stim,1),size(stim,2)*size(stim,3));
    %stim=stim(:,PreBins+1:(end-PostBins),:);
    r=r(PreBins+1:(end-PostBins),:,:);
    stim=stim(:,:)';
    r=permute(r,[2 1 3]);
    r=r(:,:)';
    r=r.*options.rasterfs;
    
    r=nanmean(r,2);
    
    Ar=cat(1,Ar,r);
    Astim=cat(1,Astim,stim);
end

r=Ar;
stim=Astim;
pr0=Apr0;

ustim=unique(stim(:));
ustim=flipud(ustim(ustim>0));
l_count=length(ustim);
freq_count=size(stim,2);

FRA=zeros(l_count,freq_count);
FRAe=zeros(l_count,freq_count);
rwin=round(0.01*options.rasterfs):round(0.04.*options.rasterfs);
cclag=10;
ccwin=zeros(cclag*2+1);
ccn=zeros(cclag*2+1);


for ff=1:freq_count,
    for ll=1:l_count,
        dd=find(stim(:,ff)==ustim(ll));
        %dd(diff(dd)==1)=[]; %LAS: depending on rasterfs, dd can have more than one index for the same stimulus. I added this line to remove these duplicates, leaving only the first.
        dd((dd+max(rwin))>size(r,1))=[];
        iiset=[];
        for rr=1:length(rwin),
            iiset=cat(3,iiset,r(dd+rwin(rr),:));
        end
        rr=nanmean(iiset,3);
        %rs=randperm(20);
        %rr=rr(rs);
        FRA(ll,ff)=nanmean(rr(:));
        FRAe(ll,ff)=nanstd(rr(:))./sqrt(sum(~isnan(rr(:))));
        %psth(ll,ff,:)=nanmean(iiset,1);
        for rr=1:length(ccwin),
            tdd=dd+rr-cclag-1;
            tdd=tdd(tdd>=1 & tdd<=size(r,1));
            ccwin(rr)=ccwin(rr)+nansum(nansum(r(tdd,:)));
            ccn(rr)=ccn(rr)+sum(sum(~isnan(r(tdd,:))));
        end
    end
end
ccwin=ccwin./ccn;

freqs=exptparams.TrialObject.ReferenceHandle.Frequencies;
levels=exptparams.TrialObject.OveralldB-...
    exptparams.TrialObject.ReferenceHandle.AttenuationLevels;

imagesc(1:freq_count,1:l_count,FRA,'Parent',axeshandle);

if(0)
    %smoothed contour plot
    tfra=gsmooth(imresize(FRA,2,'bilinear'),1);
    hold on
    contour((0.5:0.5:freq_count)+0.25,(0.5:0.5:l_count)+0.25,tfra,...
        'k','LineWidth',1,'Parent',axeshandle);
    hold off
end
set(axeshandle,'XLim',[.5 length(freqs)+.5],'YLim',[.5 length(levels)+.5])
xt=get(axeshandle,'XTick');xt(xt==0)=[];
set(axeshandle,'XTick',xt,'XTickLabel',round(freqs(xt)/100)/10,'YTick',1:l_count,'YTickLabel',levels);

%colorbar;
xlabel(axeshandle,'Frequency');
ylabel(axeshandle,'Level (dB SPL)');
set(get(axeshandle,'Parent'),'Name',sprintf('%s(%d)',basename(parmfile),realrepcount));