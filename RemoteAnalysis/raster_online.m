% function raster_online(mfile,channel,unit[1],h,options);
% 
% h - handle of figure where plot should be displayed(default, new figure)
%
% valid options fields
%    .rasterfs [=1000]
%    .sigthreshold [=4]
%    .datause [='Both'] % ie, all data, targets and references
%
function [r,tags,options_out,rh]=raster_online(mfile,channel,unit,h,options)

if ~exist('channel','var'),
    channel=1;
end
if ~exist('unit','var'),
    unit=1;
end
if ~exist('h','var'),
    figure;
    h=axes;
    drawnow;
end
if ~exist('options','var'),
    options=[];
end
options.channel=channel;
options.usesorted=getparm(options,'usesorted',0);

fprintf('raster_online: Analyzing channel %d\n',channel);

if ~isempty(strfind(mfile,'DMS')),
    options.PreStimSilence=0.35;
    options.PostStimSilence=0.35;
elseif ~isempty(strfind(mfile,'MTS')),
    options.PreStimSilence=0.1;
    options.PostStimSilence=1;
end

if options.usesorted && ~ isfield(options,'trialrange')
    [~,mf]=fileparts(mfile);
    sql=['SELECT id,cellid FROM gDataRaw WHERE parmfile like ''',mf,'%'';'];
    dbopen;
    rundata=mysql(sql);
    if channel>100
        cellid=sprintf([mf(1:7),'-%03d-%d'],channel,unit);
    else
        cellid=sprintf([mf(1:7),'-%%%02d-%d'],channel,unit);
        %Matches with or without leading 0 in hundreds place or without (before we had >100 channels)
    end
                
    scf = dbgetscellfile('rawid',rundata.id,'cellid',cellid);
    if length(scf)~=1
        warning('Couldn''t find cellid %s for rawid %d (%s) to get goodtrials (used to mark trials).',cellid,rundata.id,mf)
    end
    if length(scf) == 1
        if ~isempty(scf.goodtrials)
            options.trialrange=eval(['[' scf.goodtrials ']']);
            fprintf(['Good trials marked as ''',scf.goodtrials,''' in celldB (sCellFile). Using only these trials.\n'])
        end
    end
end

% integrate this into gui someday:
%options.includeincorrect=1
load_raw_and_raster=false;
%try
    if(isfield(options,'rawtrace'))
        if(options.rawtrace && options.usesorted)
            load_raw_and_raster=true;
        end
    end
    if(load_raw_and_raster)
        options_raw=options;
        options_raw.usesorted=0;
        options.rawtrace=0;
        options.rasterfs=20000;
        if(length(unit)==1)
            [r,tags,~,~,options_out]=raster_load(mfile,channel,unit,options);
        else
            for i=1:length(unit)
                if(i==1)
                    [r(:,:,:,i),tags,~,~,options_out]=raster_load(mfile,channel,unit(i),options);
                else
                    [rt]=raster_load(mfile,channel,unit(i),options);
                    if(isempty(rt))
                        r(:,:,:,i)=0;
                    else
                        r(:,:,:,i)=rt;
                    end
                end
            end
        end
        
       if(options_out.numChannels==4)
           channel=1;
           options_raw.Electrodes=1;
       end
       if(options_out.numChannels==4 && channel==1)
           %tetrode file
           chan_nums=options_raw.Electrodes(1)+[0:3];
           options_raw.Electrodes=chan_nums;
           [raw,tags_raw,~,~,options_raw_out]=raster_load(mfile,chan_nums,[],options_raw);
       else
           [raw,tags_raw,~,~,options_raw_out]=raster_load(mfile,channel,unit,options_raw);
       end
       rmi=cellfun(@(x)isempty(x),tags_raw);
        tags_raw(rmi)=[];
        raw(:,:,rmi,:)=[];
    else
        [r,tags,~,~,options_out]=raster_load(mfile,channel,unit,options);
    end
    rmi=cellfun(@(x)isempty(x),tags);
        tags(rmi)=[];
        r(:,:,rmi,:)=[];
%catch err
%    disp(['error loading data, pausing and reloading. Error was: ',err.message]);
%    pause(1);
%    [r,tags]=raster_load(mfile,channel,unit,options);
%end
if(isempty(r))
    return
end
repcount=max(sum(~isnan(r(:,:,:,1)),2),[],3);
stopat=max(find(repcount>=max(floor(repcount./5))));
r=r(1:stopat,:,:,:);

if ~isempty(strfind(mfile,'DMS')),
   %keyboard
end
if isa(h,'matlab.graphics.axis.Axes')
elseif isnan(h)
   rh=[];
   return
end
if(isfield(options,'rawtrace'))
    if(options.rawtrace || load_raw_and_raster)
        if(load_raw_and_raster)
            if(0)
                options_temp=options;
                [pp,bb,ee]=fileparts(mfile);
                options_temp.spikefile=[fileparts(tempname) filesep bb '.spk.mat'];
                for i=1:length(unit)
                    if(i==1)
                        [r_temp(:,:,:,i),tags,~,~,options_out_temp]=raster_load(mfile,channel,unit(i),options_temp);
                    else
                        [rt]=raster_load(mfile,channel,unit(i),options);
                        if(isempty(rt))
                            r_temp(:,:,:,i)=0;
                        else
                            r_temp(:,:,:,i)=rt;
                        end
                    end
                end
                raw_trace_plot(mfile,raw,tags,h,options_raw_out,{r,r_temp},{options_out,options_out_temp});
            else
                raw_trace_plot(mfile,raw,tags,h,options_raw_out,{r},{options_out});
            end
            assignin('base','raw',raw)
            assignin('base','r',r)
            assignin('base','options_out',options_out)
            assignin('base','tags',tags)
        else
            raw_trace_plot(mfile,r,tags,h,options_out);
            assignin('base','raw',r)
            assignin('base','options_out',options_out)
            assignin('base','tags',tags)
        end
    end
else
    %% Case-specific mods to r
    if 0
        %Keep just tags with 60dB (for FLT, to plot rasters for just one level)
        rmi=cellfun(@(x)~contains(x,'60dB'),tags);
        tags2=tags(~rmi);r2=r(:,:,~rmi);
        for i=1:length(tags2)
            tag=strsep(tags2{i},',');
            tag_=strsep(tag{2},':');
            f(i)=tag_{1};
        end
        [~,si]=sort(f);
        rh=raster_plot(mfile,r2(:,:,si),tags2(si),h,options_out);
    end
    if 0
        %Keep just tags with nsc in them
        keepi=cellfun(@(x)contains(x,'nsc'),tags);
        rh=raster_plot(mfile,r(:,:,keepi),tags(keepi),h,options_out);
    end
    if contains(mfile,'SPO')
        ri=squeeze(sum(~isnan(r(1,:,:)),2))<=2;
        r(:,:,ri)=[];
        tags(ri)=[];
    end
    %% plot raster
    rh=raster_plot(mfile,r,tags,h,options_out);
end


if(0)
    rmi=cellfun(@(x)isempty(strfind(x,'30dB')),tags);
    tags2=tags(~rmi);r2=r(:,:,~rmi);
    for i=1:length(tags2)
        tag=strsep(tags2{i},',');
        tag_=strsep(tag{2},':');
        f(i)=tag_{1};
    end
    [~,si]=sort(f);
    figure;raster_plot(mfile,r2(:,:,si),tags2(si),gca,options_out);  
end