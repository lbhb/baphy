function raw_trace_plot(mfile,raw,tags,h,options_raw,varargin)
r=getparmC(varargin,1,[]);
options=getparmC(varargin,2,[]);

options_raw.stimi=getparm(options_raw,'StimInds',[1 10 20]);
options_raw.Nreps=getparm(options_raw,'Nreps',4);
options_raw.repi=getparm(options_raw,'repi',[]);
options_raw.varargin=getparm(options_raw,'varargin',{});
%LoadMFile(mfile)
if(isempty(options_raw.repi))
    options_raw.repi=1:min(size(raw,2),options_raw.Nreps);
end
t=((0:(size(raw,1)-1))/options_raw.rasterfs*1000)';
switch options_raw.runclass
    case 'FTC'
        units=' (Hz)';
    otherwise
        units='';
end

for i=1:length(options_raw.stimi)
    ph=[];
    legtxt=[];
    if(all(options_raw.StimInds{i}<=size(raw,3)))
        if(length(options_raw.repi)==1 && size(raw,4)>1)
            %tetrode
            pht=plot(t,squeeze(mean(raw(:,options_raw.repi,options_raw.StimInds{i},:),3)),'Parent',h(i),options_raw.varargin{:});
            ph=[ph pht];
            legtxt=[legtxt arrayfun(@(x)['Ch ',num2str(x)],options{1}.Electrodes(1)+[0:3],'Uni',0)];
        else
            plot(t,mean(raw(:,options_raw.repi,options_raw.StimInds{i},:),3),'Parent',h(i),options_raw.varargin{:});
        end
        if(isequal(options_raw.tag_masks,{'SPECIAL-TRIAL'}))
            title(h(i),['Trial = ',num2str(options_raw.repi)])
        else
            stim_str='[';
            for j=1:length(options_raw.StimInds{i})
                str=strsep(tags{options_raw.StimInds{i}},',');
                stim_str=[stim_str,num2str(str{2}),','];
            end
            stim_str(end)=[']'];
            if(length(options_raw.StimInds{i})==1)
                stim_str([1 end])=[];
            end
            title(h(i),['Stimulus = ',strrep(stim_str,'_','\_'),units],'FontSize',12)
        end
    end
    if(~isempty(ph))
        legend(ph,legtxt,'Location','Best');
    end
end
linkaxes(h)
yl=get(h(1),'YLim');
yp=yl(1)+diff(yl)*.1;
mk={'^','o','+','s','*','^'};

if(~isempty(r))
    co=get(h(1),'ColorOrder');
    for i=1:length(options_raw.stimi)
        hold(h(i),'on')
        if(all(options_raw.StimInds{i}<=size(raw,3)))
            % raster given, overlay spike times
            if(length(options_raw.StimInds{i})==1)
                yl=get(h,'YLim');
                for sort_num=1:length(r)
                    uni=1:size(r{sort_num},4);
                    for un=1:length(uni)    
                    [time_ind,stim_ind]=find(r{sort_num}(:,options_raw.repi,options_raw.StimInds{i},uni(un))>0);
                    for j=1:length(options_raw.repi)
                        if(any(stim_ind==j))
                            if length(options_raw.repi) == 1 || 1
                                col=co(un,:);
                            else
                                col=co(mod(j-1,size(co,1))+1,:);
                            end
                            plot(time_ind(stim_ind==j)/options{sort_num}.rasterfs*1000,repmat(yp,sum(stim_ind==j),1)*(1+.05*(un-1)+.025*(sort_num-1)),'Color',col,'Parent',h(i),'Marker',mk{un},'LineStyle','none','Linewidth',sort_num)                  
                        end
                    end
                    end
                end
            end
        end
        if(isfield(options{sort_num},'detection_threshold'))
            line(get(h(i),'XLim'),-1*options{sort_num}.detection_threshold([1 1]),'LineStyle','--','Color','k','Parent',h(i))
        end
        hold(h(i),'off')
    end
end
end