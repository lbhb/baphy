function [ah,fh,pos]=Multichannel_probe_axes(Electrodes,varargin)
%[ah,fh,pos]=Multichannel_probe_axes(Electrodes,ProbeID,options)
%[ah,fh,pos]=Multichannel_probe_axes(Electrodes,ProbeID)
%[ah,fh,pos]=Multichannel_probe_axes(Electrodes)
% Creates a figure with subplots arranged according to electrode geometry

% Electrodes: which electrode numbers to make subplot for

% ProbeID=getparmC(varargin,1,'64D');
% which probe? (only works for 64D (default) and 128D right now)

% options.extents=getparm(options,'extents',[20 25]);
%[width height] of each subplot (in electrode geometry units (mm))

% options.borders=getparm(options,'borders',[20 25]);
%[x y] border around outside of all subplots (in electrode geometry units (mm))

% options.fig_pos=getparm(options,'fig_pos',[50 100]);
%lower left corner of figure (pixels)

ProbeID=getparmC(varargin,1,'64D');% which probe? (only works for 64D (default) and 128D right now)
options=getparmC(varargin,2,struct);
options.extents=getparm(options,'extents',[20 25]);%[width height] of each subplot (in electrode geometry units (mm))
options.borders=getparm(options,'borders',[20 25]);%[x y] border around outside of all subplots (in electrode geometry units (mm))
options.fig_pos=getparm(options,'fig_pos',[50 100]);%lower left corner of figure (pixels)



probe_fn=str2func(['probe_',ProbeID]);
[s,UCLA_to_OEP]=probe_fn();
if strcmp(ProbeID,'128D')
    [~,si]=sort(s.z+s.x/1000);
    UCLA_to_OEP=si;
end
if ~exist('Electrodes','var'),
   Electrodes=1:length(UCLA_to_OEP);
end
%x=round(s.x/10)*10;
x=s.x;
z=s.z;

if strcmp(ProbeID,'128D')
    % Squish shanks together so plots are easier to see
    xo=x;%original diff is 330
    df=250;
    xi=x>200 & x<350; x(xi)=x(xi)-df*1;
    xi=x>500 & x<700; x(xi)=x(xi)-df*2;
    xi=x>900 ;        x(xi)=x(xi)-df*3;
end
x=x-min(x)+options.extents(1)/2;
z=z-min(z)+options.extents(2)/2;
max_x=max(x)+options.extents(1)/2+options.borders(1)*2;
max_z=max(z)+options.extents(2)/2+options.borders(2)*2;
ss=get(0,'ScreenSize');
scalefactor=max((max_x+options.fig_pos(1))/ss(3),(max_z+options.fig_pos(2))/ss(4))/.9;
if scalefactor<1, scalefactor=1; end
x=x/scalefactor;
z=z/scalefactor;
options.borders=options.borders/scalefactor;
options.extents=options.extents/scalefactor;
max_x=max(x)+options.extents(1)/2+options.borders(1)*2;
max_z=max(z)+options.extents(2)/2+options.borders(2)*2;
fh=figure('Units','pixels','Position',[options.fig_pos max_x max_z]);

pos=zeros(length(Electrodes),4);
ah=zeros(length(Electrodes),1);
for i=1:length(Electrodes)
    tx=x(UCLA_to_OEP(Electrodes(i)));
    tz=z(UCLA_to_OEP(Electrodes(i)));
    pos(i,:)=[tx-options.extents(1)/2+options.borders(1) tz-options.extents(2)/2+options.borders(2) options.extents];
    ah(i)=axes('Units','Pixels','Position',pos(i,:),'Parent',fh);
end

set(ah,'Box','off','TickDir','out')
set(ah,'XTickLabel',[],'YTickLabel',[])
set(ah,'Units','normalized')