function [h]=plot_multichannel_weights(Weights,varargin)
%h=plot_multichannel_weights(Weights,ProbeID,options)
%h=plot_multichannel_weights(Weights,ProbeID)
%h=plot_multichannel_weights(Weights)
% plot with weights onto multichannel geometry

ProbeID=getparmC(varargin,1,'64D');% which probe? (only works for 64D (default) and 128D right now)
options=getparmC(varargin,2,struct);
options.plot_style=getparm(options,'plot_style','dots');%Can be {'dots','text'}
options.auto_resize=getparm(options,'auto_resize',true);


probe_fn=str2func(['probe_',ProbeID]);
[s,UCLA_to_OEP]=probe_fn();
if strcmp(ProbeID,'64D')
    width=238;
else
    width=1000;
end

hold on;

switch options.plot_style
    case 'text'
        for i=1:length(UCLA_to_OEP)
            h(i)=text(s.x(i),s.z(i),num2str(round(Weights(i)*10)/10),'FontSize',9,'HorizontalAlignment','Center');
        end
        
    case 'dots'
        for i=1:length(UCLA_to_OEP)
            h(i)=scatter(s.x(i),s.z(i),100,Weights(i),'Marker','o');
        end
        %set(h,'MarkerSize',15);
        for i=1:length(h)
            set(h(i),'MarkerFaceColor',get(h(i),'MarkerEdgeColor'),'MarkerEdgeColor','k')
        end
        colormap('gray')
end
axis equal
margin=20;
axis([min(s.x)-margin max(s.x)+margin min(s.z)-margin max(s.z)+margin])
if options.auto_resize
    set(gca,'FontSize',10,'TickDir','out')
    set(gcf,'Position',[  5          55         width        1062]);
    set(gca,'Position',[.05 .05 .95 .94]);
end