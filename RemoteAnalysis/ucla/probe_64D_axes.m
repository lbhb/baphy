function [AH,DC,fh]=probe_64D_axes(Electrodes,varargin);

fh=getparmC(varargin,1,[]);
options=getparmC(varargin,2,struct);
options.gap=getparm(options,'gapfactor',[.1 -.5]);
if(isempty(fh))
    fh=figure;
else
    figure(fh);
end
set(fh,'PaperPosition',[0.25 0.25 2 10.5]);

if ~exist('Electrodes','var'),
   Electrodes=1:64;
end

[s,UCLA_to_OEP]=probe_64D();
x=round(s.x/10)*10;
z=s.z;
xs=unique(x);
zs=unique(z);
cols=length(xs);
rows=length(zs);
ax=subplot1(rows,cols,'Gap',[options.gap(1)/cols options.gap(2)./rows]);
used_ax=false(size(ax));
for i=1:length(Electrodes)
   c=find(x(UCLA_to_OEP(Electrodes(i)))==xs);
   r=rows-find(z(UCLA_to_OEP(Electrodes(i)))==zs)+1;
   AH(i,1)= ax((r-1)*cols+c);
   used_ax((r-1)*cols+c)=true;
   DC{i}=get(AH(i,1),'Position');
end
delete(ax(~used_ax));
set(AH,'Box','off','TickDir','out')


