% function [r,tags,trialset,exptevents]=raster_load(mfile,channel,unit[1],options);
% 
% returns:
%  r - raster time X rep X stimid
%  tags - cell array with string describing each stimid
%  trialset - rep X stimid matrix that identifies the trial when r occurred
%
% valid options fields
%    .rasterfs [=1000]
%    .sigthreshold [=4]
%    .datause [='Both'] % ie, all data, targets and references
%    .lfp[=0]
%    .usesorted[=0]
%
function [r,tags,trialset,exptevents,options]=raster_load(mfile,channel,unit,options);

persistent options_last

if ~exist('options','var'),
    options=[];
end
if ~exist('channel','var'),
    options.channel=1;
else 
     options.channel = channel;
end
if ~exist('unit','var'),
    options.unit=1;
else 
  options.unit = unit;
end
if ~isfield(options,'RejShock'),
    RejShock=0;
else
    RejShock = options.RejShock;
end

if ~isfield(options,'rasterfs'),
    options.rasterfs=1000;
end
if ~isfield(options,'sigthreshold'),
    options.sigthreshold=4;
end
if ~isfield(options,'datause'),
    options.datause='Both';
end
if ~isfield(options,'psth'),
    options.psth=0;
end
if ~isfield(options,'psthfs'),
    options.psthfs=20;
end
if ~isfield(options,'lfp'),
    options.lfp=0;
end
if ~isfield(options,'usesorted'),
    options.usesorted=0;
end
if ~isfield(options,'useOEPspikes'),
    options.useOEPspikes=0;
end
if ~isfield(options,'includeincorrect'),
    options.includeincorrect=0;
end
if isfield(options,'verbose')
  verbose = options.verbose;
else
  verbose =1;
end
if ~isfield(options,'runclass'),
    [~,fn]=fileparts(mfile);
    seps=strfind(fn,'_');
    options.runclass=fn(seps(2)+1:end);
end
rasterfs=options.rasterfs;
sigthreshold=options.sigthreshold;
datause=options.datause;
if options.lfp,
    % must calculate average ("psth"), since lfp doesn't give rasters
    options.psth=1;
end

if verbose 
  fprintf('Loading raster for channel %d (rasterfs %d, sigthresh %.1f)\n',...
        options.channel,rasterfs,sigthreshold);
else fprintf('%d ',channel); 
end
switch datause,
    case 'Ref Only',
        options.tag_masks={'Ref'};
    case 'Reference Only',
        options.tag_masks={'Ref'};
    case {'Target Only'},
        options.tag_masks={'Targ'};
    case {'Both'},
        options.tag_masks={'SPECIAL-ALL'};
    case {'Trial by trial'},
        options.tag_masks={'SPECIAL-TRIAL'};
    case {'Collapse reference'},
        options.tag_masks={'SPECIAL-COLLAPSE-REFERENCE'};
    case {'Collapse target'},
        options.tag_masks={'SPECIAL-COLLAPSE-TARGET'};
    case {'Collapse both'},
        options.tag_masks={'SPECIAL-COLLAPSE-BOTH'};
    case {'Collapse keep order','Collapse first ref'},
        options.tag_masks={'SPECIAL-COLLAPSE-ORDER'};
    case {'Collapse/split errors'},
        options.tag_masks={'SPECIAL-COLLAPSE-SPLIT'};
    case {'Light/no light'},
        options.tag_masks={'SPECIAL-COLLAPSE-LIGHT'};
    case {'Light+Collapse reference'},
        options.tag_masks={'SPECIAL-COLLAPSE-LIGHT+REFERENCE'};
    case {'Light+Collapse target'},
        options.tag_masks={'SPECIAL-COLLAPSE-LIGHT+TARGET'};
    case {'Light+Collapse both'},
        options.tag_masks={'SPECIAL-COLLAPSE-LIGHT+BOTH'};
    case {'Light onset'},
        options.tag_masks={'SPECIAL-LIGHT-ONSET'};
    case {'All licks'},
        options.tag_masks={'SPECIAL-LICK-ALL'};
    case {'First lick'},
        options.tag_masks={'SPECIAL-LICK-FIRST'};
    case {'Last lick'},
        options.tag_masks={'SPECIAL-LICK-LAST'};
    case {'Per trial'},
        options.tag_masks={'SPECIAL-TRIAL'};
    case {'Per trial pre-target'},
        options.tag_masks={'SPECIAL-TRIAL-NOTAR'};
    otherwise,
        % just pass on datause
        options.tag_masks={datause};
end

tic;

if isfield(options,'PreStimSilence'),
   options.includeprestim=[options.PreStimSilence options.PostStimSilence];
else
   options.includeprestim=1;  % try to figure it out automatically
end

if ~isempty(options_last)
    if strcmp(options_last.mfile,mfile) && isfield(options_last,'runinfo');
        options.runinfo=options_last.runinfo;
    end
end

if ~options.usesorted && ~options.useOEPspikes
    %Threshold raw data to get spikes
    if verbose disp('Loading EVP...'); end
    fn=mfile;
    load_command=@loadevpraster;
elseif options.usesorted
    %use spk.mat files
    disp('Loading spikes...');
    if isfield(options,'spikefile'),
       fn=options.spikefile;
    else
       [pp,bb,ee]=fileparts(mfile);
       fn=[pp filesep 'sorted' filesep bb '.spk.mat'];
    end
    options.psthonly=-1;
    load_command=@loadspikeraster;
elseif options.useOEPspikes
    %use spikes save during OEP recording
     disp('Loading spikes from OEP data...');
     fn=mfile;
     load_command=@loadspikeraster;
else
    error('No spike generating options checked')
end
if(isempty(strfind(options.runclass,'_')) || ~isempty(strfind(options.tag_masks{1},'SPECIAL')))
    [r,tags,trialset,exptevents,~,options]=load_command(fn,options);
    if(isempty(r))
        return
    end
else
    classes=strsep(options.runclass,'_');
    for i=1:length(classes)
        options.tag_masks={sprintf('Reference%d',i)};
        [r_{i}, tags_{i}, trialset_{i},exptevents,~,options_out(i)] = load_command(fn,options);
    end
    options=options_out(1);
    max_samples=max(cellfun(@(x)size(x,1),r_));
    max_trials=max(cellfun(@(x)size(x,2),r_));
    for i=1:length(r_)
       if(size(r_{i},1)<max_samples)
           r_{i}=[r_{i};NaN(max_samples-size(r_{i},1),size(r_{i},2),size(r_{i},3))];
       end
       if(size(r_{i},2)<max_trials)
           r_{i}=[r_{i},NaN(size(r_{i},1),max_trials-size(r_{i},2),size(r_{i},3))];
           trialset_{i}=[trialset_{i};NaN(max_trials-size(trialset_{i},1),size(trialset_{i},2))];
       end
    end
    r=cat(3,r_{:});
    tags=cat(2,tags_{:});
    trialset=cat(2,trialset_{:});
end

if isfield(options,'MaxStimDuration'),
   
   postbins=round(options.PostStimSilence.*options.rasterfs);
   ss=sum(isnan(r(:,:,1)),1);
   if sum(abs(ss-mean(ss)))==0,
      % fixed duration stim, easy
      % don't do anything
   else
      for ii=1:(size(r,2)*size(r,3)),
         if sum(~isnan(r(:,ii)))>0 && isnan(r(end,ii)),
            maxnotnan=max(find(~isnan(r(:,ii))));
            r((end-postbins+1):end,ii)=r((maxnotnan-postbins+1):maxnotnan,ii);
            r((maxnotnan-postbins+1):(end-postbins+1),ii)=nan;
         end
      end
   end
   
   if size(r,1)>round((options.PreStimSilence+options.MaxStimDuration+...
                       options.PostStimSilence).*options.rasterfs);
      keeprange=[1:round((options.PreStimSilence+options.MaxStimDuration).*...
                         options.rasterfs) ...
                 (size(r,1)-round(options.PostStimSilence.*options.rasterfs)):size(r,1)];
      r=r(keeprange,:,:);
   end
end

options_last = options;
options_last.mfile = mfile;

if verbose fprintf('%s time: %.1f sec\n',mfilename,toc); end
