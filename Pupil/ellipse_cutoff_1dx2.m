function [x,y]=ellipse_cutoff_1dx2(R,params)
    ellipse_pars=params(1:5);
    cutoffs=params(6:7);
    [x,y]=ellipse_fn(R,ellipse_pars);
    x(x<=cutoffs(1))=cutoffs(1);
    x(x>=cutoffs(2))=cutoffs(2);
    