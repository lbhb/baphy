%%Generic MATLAB GUI functions
function varargout = lick_online(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @lick_online_OpeningFcn, ...
                   'gui_OutputFcn',  @lick_online_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

function lick_online_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);

function varargout = lick_online_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

%Set some globals -- Hacked from MANTA M_Defaults.m
global MG
MG=struct();
MG.resize_factor=.5;% factor by which to resize lick video. In new MATLAB, 
%videos can only be recorded at 720x480, we resize to save disk space.
MG.Stim=struct();
MG.Stim.COMterm = 124; % '|'
MG.Stim.MSGterm = 33; % '!' 
MG.Stim.Port = 33333; %  Port to connect to 33333 rather than MANTA 33330, PUPIL 33331, or PHOTOMETRY 33332
MG.recording=0;
MG.frames_written=0;
MG.format='lick';
MG.do_pause=0;
% hard-coding remote (baphy) host for now.
% hard-coding remote (baphy) host for now.

MG.rotation = [0, 90];
switch getenv('computername')
    case {'TATERIL'}
        MG.Stim.Host = 'localhost';
    case 'WEASEL'
        MG.Stim.Host = 'localhost';
    case {'BADGER'}
        MG.Stim.Host = 'localhost';
        % camera is flipped
        MG.rotation = [0, 270];
        MG.rotation = [180, 270];
    case 'MOLE'
        MG.Stim.Host = '10.147.70.6';  % weasel.ohsu.edu
    case 'LBHB-PC'  % photo host
        MG.Stim.Host = '10.147.70.6';  % weasel.ohsu.edu
    otherwise
        error('Unknown computername:%s',getenv('computername'))
        
end

I = ver('instrument');
if ~isempty(I)
  MG.Stim.Package = 'ICT'; % Instrument Control Toolbox
else 
  error(['Instrument Control Toolbox needs to be installed, since there is no free package that supports Callback functions at this point.']);
  MG.Stim.Package = 'jTCP'; % Java TCP by Kevin Bartlett (http://www.mathworks.com/matlabcentral/fileexchange/24524-tcpip-communications-in-matlab)
  I = which('jtcp');
  if isempty(I) 
    MG.Stim.Pacakge = 'None';
    fprintf(['WARNING : NO TCPIP SUITE FOUND!\n'...
      '\tNeither the instrument control toolbox, nor the open source tcpip suite jTCP have been detected.\n '...
      '\tPlease install either of those two, in order to connect to a controller/stimulator\n']); 
  end
end

%Camera Interface
function video_file_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), ...
        get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function preview_video_Callback(hObject, eventdata, handles)
global MG
imaqreset
if isfield(MG, 'cam')
    try
        closepreview(MG.cam)
    end
    %rmfield(handles, 'rect')
end

MG.mode=2;
%MG.cam = videoinput('winvideo', 1,'UYVY_720x480'); August VGB100
%
computername = getenv('computername');
switch computername
    case {'TATERIL','BADGER'}
        info=imaqhwinfo('winvideo');
        %devID=find(strcmp({info.DeviceInfo.DeviceName},'AV TO USB2.0'),1);
        devID=1;
        fprintf('Computer %s, hard-coded camera devID=%d\n',computername, devID);
        format='YUY2_720x480';
    case {'WEASEL'}
        devID=2;
        format='UYVY_360x288';
        format='UYVY_720x480';
    otherwise
        devID=1;
end
if MG.mode==2
    MG.cam = videoinput('winvideo', devID, format);
elseif MG.mode==1
    MG.cam = imaq.VideoDevice('winvideo', devID,format);
else
    MG.cam = videoinput('winvideo', devID,format);
end
%MG.cam = videoinput('winvideo', 1,'UYVY_360x240'); %doesn't work

MG.cam.ReturnedColorSpace = 'RGB';
if MG.mode==1
    MG.cam.ReturnedDataType = 'uint8';
end
ROI=MG.cam.ROI;
res = ROI(3:4);
nbands = 3;
%set(handles.preview_ax,'YDir','reverse')
if MG.resize_factor~=1 && 0
    %Test of resizing. Actual resizing happens in lick_start_recording.m
    %DOESN"T ACTUALLY DO WHAT WE WANT!
    % only takes one snapshot while preview (below) continuously streams the camera
    % to the figure window
    res_resized=res*MG.resize_factor;
    himage = image(zeros(res_resized(2), res_resized(1), nbands), 'Parent', handles.preview_ax);
    MG.im = getsnapshot(MG.cam);
    MG.im_res = imresize(MG.im,MG.resize_factor);
    set(himage,'CData',MG.im_res)
else
    
    if MG.mode==2
        MG.himage = image(zeros(res(2), res(1), nbands), 'Parent', handles.preview_ax);
        hi=preview(MG.cam,MG.himage);
%         start(MG.cam);
%         pause(.5)
%         trigger(MG.cam);
%         [img,ts] = getdata(MG.cam);
%         set(MG.himage, 'CData', img);
    elseif MG.mode==1
        img = step(MG.cam);
        MG.himage = image(zeros(res(2), res(1), nbands), 'Parent', handles.preview_ax);
        set(MG.himage, 'CData', img);
    else
        himage = image(zeros(res(2), res(1), nbands), 'Parent', handles.preview_ax);
        hi=preview(MG.cam,himage);
    end
    
end
set( handles.preview_ax,'View', MG.rotation)
guidata(hObject, handles)
if MG.mode==1
    MG.run_preview=1;
    run_preview(hObject, eventdata, handles)
end

function run_preview(hObject, eventdata, handles)
global MG
while MG.run_preview
        if MG.mode==1
            img = step(MG.cam);
            set(MG.himage, 'CData', img);
        elseif MG.mode==2
            trigger(MG.cam);
            [MG.im,ts] = getdata(MG.cam);
            set(MG.himage, 'CData', MG.im);
        end
end


function set_roi_Callback(hObject, eventdata, handles)
global MG
if isfield(MG, 'cam')
    if MG.mode==2
        %trigger(MG.cam);
        %[MG.im,ts] = getdata(MG.cam);
        MG.im = getsnapshot(MG.cam);
    elseif MG.mode==1
        MG.im = step(MG.cam);
    else
        MG.im = getsnapshot(MG.cam);
    end
else
    disp('lick_online: Error - No active camera.')
end
figure
imshow(MG.im)
set(gca,'View',MG.rotation)

ROI_rect = 0;
try
    MG.FreehandObject=drawfreehand;
    MG.roiMask=createMask(MG.FreehandObject);
catch
    r = imrect;
    MG.roi = getPosition(r); 
    ROI_rect = 1;
end

close
axes(handles.preview_ax)
if ROI_rect
    if isfield(handles, 'rect')
        set(handles.rect, 'Position', MG.roi);
    else
        handles.rect = rectangle('Position', MG.roi, ...
            'Linewidth', 1, ...
            'EdgeColor', 'b');
    end
else
    if isfield(handles, 'ROI')
        set(handles.ROI,'CData', MG.roiMask*365);
    else
        hold(handles.preview_ax,'on')
        handles.ROI = image(MG.roiMask*365,'Parent',handles.preview_ax,'AlphaData',0.2);
        hold(handles.preview_ax,'off')
    end
end
MG.lick_trace=0;
MG.t=0;
MG.lick_threshold=15;
MG.lick_inds=[];
MG.lick=false;
MG.manual_lick=false;
MG.do_reset_baseline=false;
if ~isfield(handles,'lick_threshold')
    handles.lick_threshold=plot(handles.lick_ax,[1 150],MG.lick_threshold*[1 1],'--','Color',cbcolor(3));
else
    set(handles.lick_threshold,'YData',[1 1]*MG.lick_threshold);
end
hold(handles.lick_ax,'on')
if ~isfield(handles,'lick_trace')
    handles.lick_trace=plot(handles.lick_ax,MG.lick_trace,'k');
end
    
set(handles.lick_trace,'HitTest','off')
set(handles.lick_threshold,'HitTest','off')
%set(handles.lick_ax,'NextPlot','add')
CB=@(hObject,eventdata)lick_online('set_lick_threshold_Callback',hObject,eventdata,guidata(hObject));
set(handles.lick_ax,'ButtonDownFcn',CB);
set(handles.lick_ax,'YLim',[0 MG.lick_threshold*1.5])
guidata(hObject, handles)

%Communication with Baphy
function connect_Callback(hObject, eventdata, handles)
global MG
MG.hObject = hObject;
MG.handles = handles;
State = get(hObject,'Value');
set(handles.status_msg, 'String', ['Connect: ',num2str(State)]);
if State M_startTCPIP; else M_stopTCPIP; end


function pause_Callback(hObject, eventdata, handles)
global MG
MG.do_pause=1;
% preview_video_Callback(hObject, eventdata, handles)
% if isfield(handles, 'ROI')
%     hold(handles.preview_ax,'on')
%     handles.ROI = image(MG.roiMask*365,'Parent',handles.preview_ax,'AlphaData',0.2);
%     hold(handles.preview_ax,'off')
% end

function set_high_Callback(hObject, eventdata, handles)
global MG
MG.high = str2num(get(handles.set_high, 'string'));

function set_high_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), ...
                   get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end

function set_low_Callback(hObject, eventdata, handles)
global MG
MG.low = str2num(get(handles.set_low, 'string'));

function set_low_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), ...
                   get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in buttonCancel.
function buttonCancel_Callback(hObject, eventdata, handles)
% hObject    handle to buttonCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global MG
MG.recording=0;

function set_lick_threshold_Callback(hObject, eventdata, handles)
global MG
MG.hObject = hObject;
MG.handles = handles;
if isfield(handles,'lick_threshold')
    MG.lick_threshold=eventdata.IntersectionPoint(2);
    set(handles.lick_threshold,'YData',MG.lick_threshold*[1 1])
    set(handles.lick_ax,'YLim',[0 MG.lick_threshold*1.5])
    fprintf('Threshold set to %0.1f\n',MG.lick_threshold)
else
    warning('Click on lick trace axes detected. Tried to set threshold but couldn''t')
end


% --- Executes on button press in pushbutton4.
function LickButton_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global MG
MG.manual_lick=true;

% --- Executes on button press in resetbaseline.
function ResetBaselineButton_Callback(hObject, eventdata, handles)
% hObject    handle to resetbaseline (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global MG
MG.do_reset_baseline=true;

