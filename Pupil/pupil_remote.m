%pupil_remote
%
%A system for analysis of pupillometry data.
%
%Created ZPS summer 2015
%Rewrite for modularity ZPS 2016-1-11 

%-----------------------------------------------------------------------------
% Initialize GUI
function varargout = pupil_remote(varargin)
  gui_Singleton = 1;
  gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @pupil_remote_OpeningFcn, ...
    'gui_OutputFcn',  @pupil_remote_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
  if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
  end
  if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
  else
    gui_mainfcn(gui_State, varargin{:});
  end
end

function pupil_remote_OpeningFcn(hObject, eventdata, handles, varargin)
  baphy_set_path;
  dbopen;
  handles.output = hObject;
  guidata(hObject, handles);
end

function varargout = pupil_remote_OutputFcn(hObject, eventdata, handles) 
  varargout{1} = handles.output;
end
%-----------------------------------------------------------------------------
% GUI objects to set pupil analyis parameters

%filename
function edit_filename_Callback(hObject, eventdata, handles)
  p = get(hObject, 'string');
  set_param('filename', p)
  load_video(handles)
end

function edit_filename_CreateFcn(hObject, eventdata, handles)
  p = get(hObject, 'string');
  set_param('filename', p)
end

%default analysis method
function edit_default_Callback(hObject, eventdata, handles)
  p = get(hObject, 'string');
  set_param('default', p);
end

function edit_default_CreateFcn(hObject, eventdata, handles)
  p = get(hObject, 'string');
  set_param('default', p)
end

%default analysis variable
function edit_default_var_Callback(hObject, eventdata, handles)
  p = get(hObject, 'string');
  set_param('default_var', p);
  refresh_slider(handles)
end

function edit_default_var_CreateFcn(hObject, eventdata, handles)
  p = get(hObject, 'string');
  set_param('default_var', p);
end

%regions of interest
function set_outer_roi_Callback(hObject, eventdata, handles)

  global MODULE PARAMS 
  params = PARAMS.(MODULE.str);

  im = read(params.vid, params.frame);

  p = get_roi(im);
  set_param('outer_roi', p);

  %keep inner roi inside outer roi
  outer_roi = PARAMS.outer_roi;
  inner_roi = PARAMS.inner_roi;
  if (outer_roi(1) > inner_roi(1)) | ...
      (outer_roi(2) > inner_roi(2)) | ...
      (outer_roi(1) + outer_roi(3)) < (inner_roi(1) + inner_roi(3)) | ...
      (outer_roi(2) + outer_roi(4)) < (inner_roi(2) + inner_roi(4))
    fprintf('%s: Outer roi is inside inner roi. Resetting inner roi.\n', ...
      mfilename)
    set_param('inner_roi', outer_roi);
  end

  refresh_frame(handles)

end

function set_outer_roi_CreateFcn(hObject, eventdata, handles)
  set_param('outer_roi', []);
end

function set_inner_roi_Callback(hObject, eventdata, handles)
  
  global MODULE PARAMS
  params = PARAMS.(MODULE.str);

  im = read(params.vid, params.frame);

  p = get_roi(im);
  set_param('inner_roi', p)
  refresh_frame(handles)

end

function set_inner_roi_CreateFcn(hObject, eventdata, handles)
  set_param('inner_roi', []);
end

function roi = get_roi(im)
  fig = figure;
  imshow(im);
  drawnow;
  r = imrect;
  roi = getPosition(r);
  close(fig)
end
%-----------------------------------------------------------------------------
% Set parameters

function set_algorithm_params_Callback(hObject, eventdata, handles)

	global PARAMS MODULE

	if ~isfield(MODULE,'default_params')
		uiwait(msgbox('There aren''t any more settable parameters for this module.','','modal'));
		return
	end

	defaults=MODULE.default_params();
	params=fieldnames(defaults);
	%remove params that are on the main GUI
	params(cellfun(@(x)isfield(handles,['edit_' x]),params))=[];

	for i=1:length(params)
		param(i).text=params{i};
		param(i).style='edit';
		param(i).default=PARAMS.(MODULE.str).(params{i});
	end
	UserInput = ParameterGUI(param,'Pupil Algorithm Parameters','bold','center',600);
	if iscell(UserInput)
		for i=1:length(params)
			PARAMS.(MODULE.str).(params{i})=UserInput{i};
		end
		refresh_frame(handles)
		refresh_slider(handles)
	end
end

function set_params_from_defaults(handles)

	global MODULE PARAMS

	defaults=MODULE.default_params();
	params=fieldnames(defaults);
	%update global params structure
	for k=1:length(params)
		p = params{k};
		PARAMS.(MODULE.str).(p) = defaults.(p);
		PARAMS.(p) = defaults.(p);
	end

	displayed = {'filename'};

	%update display of parameters on gui
	for k=1:length(displayed)
		p = displayed{k};
		if isfield(PARAMS.(MODULE.str), p)
			h = ['edit_' p];
			set(handles.(h), 'string', PARAMS.(MODULE.str).(p));
		end
	end

	guidata(handles.output, handles)

end

function set_params_from_gui(module)

	global MODULE PARAMS

	params = {'filename', 'inner_roi', 'outer_roi', 'vid', 'frame', ...
		'excluded_frames', 'number_of_frames'};

	%update module-specific params structure
	for k=1:length(params)
		p = params{k};
		if ~isfield(PARAMS.(MODULE.str), p)
			PARAMS.(MODULE.str).(p) = [];
		end
		PARAMS.(MODULE.str).(p) = PARAMS.(p);
	end

end

function set_params_from_module(module, handles)

	global MODULE PARAMS
	
	%check for new params in module, set to default values if not there
	if isfield(MODULE,'default_params')
		defaults = MODULE.default_params();
		params = fieldnames(defaults);
		for k=1:length(params)
			p = params{k};
			if ~isfield(PARAMS.(MODULE.str), p)
				PARAMS.(MODULE.str).(p) = defaults.(p);
				PARAMS.(p) = defaults.(p);
			end
		end
	end

	params = {'filename', 'inner_roi', 'outer_roi', 'vid', 'frame', ...
		'excluded_frames', 'number_of_frames'};

	%update global params structure
	for k=1:length(params)
		p = params{k};
		if isfield(PARAMS.(MODULE.str), p)
			PARAMS.(p) = PARAMS.(MODULE.str).(p);
		end
	end

	displayed = {'filename'};

	%update display of parameters on gui
	for k=1:length(displayed)
		p = displayed{k};
		if isfield(PARAMS.(MODULE.str), p)
			h = ['edit_' p];
			set(handles.(h), 'string', PARAMS.(MODULE.str).(p));
		end
	end

	guidata(handles.output, handles)

end

function set_param(name, value)
	global MODULE PARAMS
	PARAMS.(name) = value;
	if ~isempty(MODULE)
		PARAMS.(MODULE.str).(name) = value;
	end
end

function value = get_param(name)
	global PARAMS
	value = PARAMS.(name);
end
%-----------------------------------------------------------------------------
% GUI objects to set pupil analysis module
function set_analysis_module_Callback(hObject, eventdata, handles)

	modules = get(hObject, 'string');
	idx = get(hObject, 'value');

	set_analysis_module(modules{idx}, handles);

end

function set_analysis_module_CreateFcn(hObject, eventdata, handles)

	global MODULE PARAMS

	%display list of available modules
	modules = {'largest_object', ... 
						 'swindow_ellipse', ...
						 'dwindow_ellipse', ...
						 'dwindow_autothresh', ...
						 'dwindow_varythresh',...
						 'dwindow_varythresh_cut'};
	set(hObject, 'string', modules);
	guidata(hObject, handles)

	MODULE = eval(modules{1}); %initialize default module
	PARAMS.(MODULE.str) = [];

end

function set_analysis_module(module, handles)

	global MODULE PARAMS RESULTS

	if ~strcmp(MODULE.str, module)
		MODULE = eval(module); %create analysis object
	end

	if isfield(PARAMS, MODULE.str) %reload params
		set_params_from_module(module, handles)
	else
		%initialize params from gui
		PARAMS.(module) = [];
		set_params_from_gui(module);
		if isfield(MODULE,'default_params')
			%this module has defaults, use them
			set_params_from_defaults(handles);
		end
	end

	if ~isfield(RESULTS, MODULE.str)
		MODULE.malloc(PARAMS.number_of_frames, MODULE);
	end

	frame = get_last_analyzed_frame(module);
	set_param('frame', frame)

	%reset frame display axes
	delete(get(handles.left,  'children'))
	delete(get(handles.center,'children'))
	delete(get(handles.right, 'children'))
	reset(handles.left)
	reset(handles.center)
	reset(handles.right)

	refresh_frame(handles)
	refresh_slider(handles)

end

function f = get_last_analyzed_frame(module)

	global RESULTS

	f = 1;

	if ~isfield(RESULTS, module) | isempty(RESULTS.(module))
		return
	end

	%ZPS 2018-10-3: this loop is creating buggy behavior
	%because eye speed isn't analyzed at the same time
	%as pupil size
	names = fieldnames(RESULTS.(module));
	for k = 1:length(names)
		s = RESULTS.(module).(names{k});
		if ~strcmp(names{k}, 'eye_speed') %ZPS 2018-10-3: kludgey solution
			if size(s,2) == 1 & ~isstruct(s) & ~iscell(s)
				l = max(find(~isnan(s)));
				if isempty(l)
					f = 1;
				else
					f = max(f, l);
				end
			end
		end
	end

end
%-----------------------------------------------------------------------------
% Load video
function browse(hObject, eventdata, handles)

	global DBISOPEN BAPHYDATAROOT

	%get file name
	[f directory] = uigetfile({'*.mj2', 'Videos';'*.avi', 'AVI'}, ...
		'Choose an experiment:', BAPHYDATAROOT);

	if f ~= 0 %user chose a file

		if ~DBISOPEN
			dbopen; %open celldb in case any operations require database access
		end

		set_param('filename', [directory f]); %update filename parameter

		%update filename display
		set(handles.edit_filename, 'string', [directory f]);
		guidata(handles.output, handles)

		load_video(handles)

	end

end

function load_video(handles)

	clear global RESULTS %clear results
	cla(handles.time_ax), cla(handles.time_ax_local) %clear traces

	global MODULE PARAMS RESULTS

	%remove conflicting module-specific parameters
	filename = get_param('filename');
	current_params = fieldnames(PARAMS);
	for k=1:length(current_params)
		p = PARAMS.(current_params{k});
		if isstruct(p) & ~strcmp(p.filename, filename) 
			PARAMS = rmfield(PARAMS, current_params(k));
		end
	end

	%create video reader object
	vid = VideoReader(filename);
	set_param('vid', vid);

	%set number of frames
	number_of_frames = get(vid, 'numberofframes');
	set(handles.n_frame_msg, 'string', sprintf('of %d', number_of_frames));
	set_param('number_of_frames', number_of_frames);
	set_param('excluded_frames', []);

	%set image to first frame of video
	im = read(vid, 1);
	set_param('frame', 1);
	set(handles.edit_frame, 'string', '1');

	%set region of interest 
	roi_choice = questdlg('Choose a region of interest?', 'Set ROI', ...
		'Yes', 'No - Use entire image', 'No - Keep previous ROI', 'Yes');
	full_image = [1 1 size(im,2)-1 size(im,1)-1];
	switch roi_choice
		case 'Yes'
			outer_roi = get_roi(im); 
			inner_roi = outer_roi;
		case 'No - Use entire image'
			outer_roi = full_image;
			inner_roi = outer_roi;
		case 'No - Keep previous ROI'
			if ~isempty(outer_roi) & ~isempty(inner_roi)
				msgbox('Keeping previous ROI', 'CreateMode', 'modal')
				outer_roi = get_param('outer_roi');
				inner_roi = get_param('inner_roi');
			else
				msgbox('No ROI set - Using entire image', 'CreateMode', 'modal')
				outer_roi = full_image;
				inner_roi = outer_roi;
			end
	end
	set_param('outer_roi', outer_roi)
	set_param('inner_roi', inner_roi)

	MODULE.malloc(number_of_frames, MODULE); %allocate results memory
	set_params_from_defaults(handles); %initialize params in module

	dbopen;

	%get the animal's eye width in millimeters
	[~,f,~] = fileparts(filename);
	sql = ['select * from gAnimal WHERE cellprefix="' f(1:3) '"'];
	animaldata = mysql(sql);
	if ~isempty(animaldata)
		PARAMS.eye_width_mm = animaldata.eyeWidth*10;
	end

	refresh_frame(handles) %display first frame
	guidata(handles.output, handles)

end
%-----------------------------------------------------------------------------
% Analyze frame
function refresh_frame(handles)

	global MODULE PARAMS RESULTS

	%get the current frame
	frame = get_param('frame');
	vid = get_param('vid');
	im = rgb2gray(read(vid, frame));

	set(handles.edit_frame, 'string', num2str(frame)); %update frame display

	set_params_from_gui(MODULE.str) %update params
	MODULE.params = PARAMS.(MODULE.str); %pass params to module

	%update frame/roi display
	axes(handles.im_ax)
	cla
	imshow(im)
	rectangle('position', PARAMS.outer_roi, 'linewidth', 1, 'edgecolor', 'y');
	rectangle('position', PARAMS.inner_roi, 'linewidth', 1, 'edgecolor', 'y');
	if isfield(RESULTS, 'eye_line')
		hold on
		l = RESULTS.eye_line;
		plot(l(:,1), l(:,2), 'y')
		hold off
	end

	%update histogram
	eyeim = imcrop(im, PARAMS.outer_roi);
	axes(handles.hist_ax), cla, hold on
	hist(double(eyeim(:)), 0:255);
	axis tight, axlim = axis;
	if isfield(MODULE.params, 'low')
		plot([MODULE.params.low MODULE.params.low],  [axlim(3) axlim(4)], 'b');
	end
	if isfield(MODULE.params, 'high')
		plot([MODULE.params.high MODULE.params.high], [axlim(3) axlim(4)], 'b');
	end
	if isfield(MODULE.params, 'low') & isfield(MODULE.params, 'high')
		set(handles.hist_ax, ...
			'xtick', MODULE.params.low:10:MODULE.params.high, ...
			'xcolor', 'b')
	end
	xlabel('Image Intensity')
	ylabel('Number of Pixels')
	hold off

	%analyze frame
	MODULE = MODULE.do_frame(im, frame, MODULE); %analyze
	MODULE.plot_frame(im, frame, handles, MODULE); %plot 

end

function edit_frame_Callback(hObject, eventdata, handles)

	%change frame
	frame = max(1, str2num(get(handles.edit_frame, 'string')));
	set_param('frame', frame)

	%refresh display
	refresh_frame(handles)
	refresh_slider(handles)

	guidata(handles.output, handles)

end

function edit_frame_CreateFcn(hObject, eventdata, handles)
end

function frame_slider_Callback(hObject, eventdata, handles)

	%change frame
	frame = max(1, round(get(handles.frame_slider, 'value')));
	set_param('frame', frame)

	%refresh display
	refresh_frame(handles)
	refresh_slider(handles)

	guidata(handles.output, handles)

end

function frame_slider_CreateFcn(hObject, eventdata, handles)
end

function refresh_slider(handles)

	global MODULE PARAMS

	%refresh data display
	cla(handles.time_ax), cla(handles.time_ax_local)
	if isfield(PARAMS.(MODULE.str), 'sweeps')
		sweeps = PARAMS.(MODULE.str).sweeps;
	else
		sweeps = 1;
	end
	if sweeps==1
		MODULE.plot_trace(handles, MODULE)
		plot_excluded_frames(handles)
	else
		if ~isfield(MODULE, 'plot_sweep')
			fprintf('%s: This module does not support multiple sweeps.\n', ...
				mfilename)
			%set(handles.edit_sweeps, 'string', '1')
			%set_param('sweeps', 1)
			return
		end
		MODULE.plot_sweep(handles, MODULE)
	end

	%draw vertical line at displayed frame
	axes(handles.time_ax)
	axlim = axis;
	frame = get_param('frame');
	hold on
	plot([frame frame], [axlim(3) axlim(4)], 'k')
	hold off

	%update size of slider
	analyzed_frames = get_last_analyzed_frame(MODULE.str);
	set(handles.frame_slider, 'string', num2str(frame), ...
		'value', frame, ...
		'max', max(1, analyzed_frames), ...
		'sliderstep', [1 100]./analyzed_frames)

	set(handles.time_ax, 'buttondownfcn', {@time_ax_ButtonDownFcn, handles})
	set(handles.time_ax_local, 'buttondownfcn', {@time_ax_local_ButtonDownFcn, handles})

	guidata(handles.output, handles)

end

function time_ax_ButtonDownFcn(hObject, eventdata, handles)
	coords = get(hObject, 'currentpoint');
	frame = floor(coords(1,1));
	set_param('frame', frame)
	refresh_frame(handles)
	refresh_slider(handles)
end
%-----------------------------------------------------------------------------
% Analyze video
function analyze_video_Callback(hObject, eventdata, handles)

	global PARAMS MODULE STOPPED

	set(handles.stop, 'value', 0);
	STOPPED = 0;

	set_params_from_gui(MODULE.str) %update params
	MODULE.params = PARAMS.(MODULE.str); %pass params to module
	cla(handles.time_ax), cla(handles.time_ax_local) %clear traces

	if isfield(PARAMS.(MODULE.str), 'sweeps')
		sweeps = PARAMS.(MODULE.str).sweeps;
	else
		sweeps = 1;
	end
	done = false(PARAMS.number_of_frames, 1);

	if (sweeps == 1) %plot full trace
		f_end=get_last_analyzed_frame(MODULE.str);
		f_current=min(f_end,max(PARAMS.frame,1));
		if f_current==f_end
			f=f_current;
		else
			s=sprintf('Current frame is %d. Start from here? (If no, start at last frame analyzed: %d',...
				f_current,f_end);
			q=questdlg(s);
			%q='Yes'
			if strcmpi(q,'Yes')
				f=f_current;
			elseif strcmpi(q,'No')
				f=f_end;
			elseif strcmp(q,'Cancel')
				return
			end
		end

		for frame = f:PARAMS.number_of_frames

			im = rgb2gray(read(PARAMS.vid, frame)); %get frame
			MODULE = MODULE.do_frame(im, frame, MODULE); %analyze frame
			MODULE.plot_trace(handles, MODULE) %plot trace

			if STOPPED
				break
			end

		end

	else %plot a few sweeps along the full dataset

		if ~isfield(MODULE, 'plot_sweep')
			fprintf('\n%s: This module does not support multiple sweeps.\n', ...
				mfilename)
			return
		end

		warning off

		frame_intervals = ...
			round(repmat(PARAMS.number_of_frames,1,sweeps)./2.^(1:sweeps));

		for k = 1:length(frame_intervals) 

			interval = frame_intervals(k);
			cla(handles.time_ax), cla(handles.time_ax_local)

			if ~STOPPED
				fprintf('%s: Sweep with frame interval %d\n', mfilename, interval)
				fprintf('%s: Frames: ', mfilename)
			end

			for frame = 1:interval:PARAMS.number_of_frames
				if ~done(frame)
					im = rgb2gray(read(PARAMS.vid, frame)); %get frame
					MODULE = MODULE.do_frame(im, frame, MODULE); %analyze frame
					done(frame) = true;
					fprintf('. ')
				end

				if STOPPED
					break
				end

			end

			if STOPPED
				break
			end

			fprintf('\n')
			MODULE.plot_sweep(handles, MODULE)

		end

		warning on

	end

	set(handles.stop, 'value', 1);
	STOPPED = 1;

	%update frame display
	set_param('frame', frame)
	refresh_frame(handles)
	refresh_slider(handles)

	set(handles.time_ax, 'buttondownfcn', {@time_ax_ButtonDownFcn, handles})
	guidata(handles.output, handles)

end

function stop_Callback(hObject, eventdata, handles)
	global STOPPED
	STOPPED = get(handles.stop, 'value');
end

function stop_CreateFcn(hObject, eventdata, handles)
	global STOPPED
	STOPPED = 1;
	set(hObject, 'value', STOPPED)
end

%-----------------------------------------------------------------------------
% Replay analysis
function replay_Callback(hObject, eventdata, handles)

	global MODULE PARAMS
	MODULE.params = PARAMS; %pass in parameters

	save_choice = questdlg('Save replay?', 'Yes', 'No');
	if strcmp(save_choice, 'Yes')
		path = strrep(MODULE.params.filename, '.mj2', '_demo.avi');
		mov = avifile(path);
		mov.quality = 100;
	end

	fig = sfigure;
	set(fig, 'menubar', 'none')
	iptsetpref('ImShowBorder', 'tight')

	analyzed_frames = get_last_analyzed_frame(MODULE.str)

	im = read(PARAMS.vid, 1);
	ax = imshow(im);

	for frame = 150:150:analyzed_frames %camera runs at about 30 frames/s
		im = rgb2gray(read(PARAMS.vid, frame));
		hold on
		MODULE.plot_replay(im, frame, fig, MODULE) 
		if strcmp(save_choice, 'No')
			l = sprintf('Video Progress: %d%%', round((frame/analyzed_frames)*100));
		else
			l = datestr((frame/30)/24/3600, 'MM:SS');
		end
		text(0, size(im,1)*0.95, l)
		hold off
		if strcmp(save_choice, 'Yes')
			mov = addframe(mov, getframe(gcf));
		end
		pause(0.1)
	end

	if strcmp(save_choice, 'Yes')
		mov = close(mov);
	end

end
%-----------------------------------------------------------------------------
% Save analysis
function save_analysis(hObject, eventdata, handles)

	global MODULE PARAMS RESULTS

	default = [PARAMS.filename(1:end-4) '.pup.mat'];
	options.Resize = 'on';
	f = inputdlg('Save to:', 'Save Pupil Data', 1, {default}, options);

	if ~isempty(f)

		f = f{1};

		%avoid overwriting existing file
		if exist(f, 'file')
			write_choice = questdlg('Overwrite previous analysis?', 'Save');
			switch write_choice
				case 'Yes'
					pupil_data = [];
				case 'No'
					load(f, 'pupil_data');
			end
		else
			pupil_data = [];
		end

		%save
		pupil_data.results = RESULTS;
		pupil_data.params = PARAMS;
		save(f, 'pupil_data');

		unix(['chmod a+w ' f]); %give all users write privileges

	end

end
%-----------------------------------------------------------------------------
% Reload analysis
function load_analysis(hObject, eventdata, handles)

	global BAPHYDATAROOT

	%get file name
	[filename, pathname] = uigetfile({'*.pup.mat', 'Pupil Analysis Files'}, ...
		'Choose a pupil analysis:', ...
		BAPHYDATAROOT);

	if filename ~= 0 %user chose a file

		clear global MODULE PARAMS RESULTS %reset global variables
		global MODULE PARAMS RESULTS

		load([pathname filename]); %load data

		%backwards compatibility check
		if isfield(pupil_data, 'pupil_ellipse')
			old_data_format = true;
		else
			old_data_format = false;
		end

		if old_data_format

			MODULE = eval('largest_object'); %set module
			default = 'largest_object'; %set default module

			%read params from old data format
			params.filename = pupil_data.PupilFilename; 
			params.default = 'largest_object';
			params.default_var = 'ellipse_width';
			params.vid = VideoReader(pupil_data.PupilFilename);
			params.high = pupil_data.params.high;
			params.low = pupil_data.params.low;
			%params.smooth = 1;
			%params.smooth_hist = 3;
			%params.max_jump = 100;
			%params.min_area = 100;
			params.outer_roi = pupil_data.roi;
			params.inner_roi = pupil_data.roi;
			params.number_of_frames = get(params.vid, 'numberofframes');
			params.sweeps = 1;
			params.excluded_frames = pupil_data.excluded_frames;
			params.inner_roi = pupil_data.roi;
			params.frame = 1;

			%set params
			PARAMS = params;
			PARAMS.largest_object = params; 

			%set results
			RESULTS.largest_object.ellipse_width = pupil_data.pupil_diameter;
			if isfield(pupil_data, 'widest_row')
				RESULTS.largest_object.blob_width = pupil_data.widest_row;
			else
				RESULTS.largest_object.blob_width = [];
			end

		else %current data format

			PARAMS = pupil_data.params;  %set params
			RESULTS = pupil_data.results; %set results

			%fix broken video reader objects
			%(deals with platform and matlab version compatibility issues)
			if isempty(PARAMS.vid.UserData) %videoreader is broken
				if contains(PARAMS.filename, '/')
					vidname_regexp = '[^/]*$'; %analysis was saved on linux or mac
				else
					vidname_regexp = '[^\\]*$'; %analysis was saved on windows
				end
				vidname = regexp(PARAMS.filename, vidname_regexp, 'match');
				vidname = [pathname vidname{1}];
				if ~exist(vidname)
					error('Video was not saved in same directory as analysis.') 
				else
					%regenerate the videoreader object
					fprintf('%s: Creating new VideoReader object.\n', mfilename)
					PARAMS.vid = VideoReader(vidname);
					PARAMS.filename = vidname;
					PARAMS.old_filename = vidname;
					%use the new videoreader object in each saved analysis
					module_list = get(handles.set_analysis_module, 'string');
					for k=1:length(module_list)
						m = module_list{k};
						if isfield(PARAMS, m)
							PARAMS.(m).vid = PARAMS.vid;
							PARAMS.(m).filename = PARAMS.filename;
							PARAMS.(m).old_filename = PARAMS.old_filename;
						end
					end
				end
			end

			if not(isfield(PARAMS, 'eye_width_mm'))
				%get the animal's eye width in millimeters
				[~,f,~] = fileparts(filename);
				sql = ['select * from gAnimal WHERE cellprefix="' f(1:3) '"'];
				animaldata = mysql(sql);
				if ~isempty(animaldata)
					PARAMS.eye_width_mm = animaldata.eyeWidth*10;
				end
			end

			%ZPS 2018-09-13: Is this step still necessary now that we're storing
			%default parameter values in the module? Commenting out for now to see 
			%if anything breaks.
			%
			%check for new parameters
			%strparams = {'filename', 'default', 'default_var'};
			%for k=1:length(strparams)
			%	p = strparams{k};
			%	if ~isfield(PARAMS, p)
			%		h = ['edit_' p];
			%		PARAMS.(p) = (get(handles.(h), 'string'));
			%	end
			%end

			%numparams = {'inner_roi', 'outer_roi', 'vid', 'frame', ...
			%	'excluded_frames', 'number_of_frames'};

			%for k=1:length(numparams)
			%	p = numparams{k};
			%	if ~isfield(PARAMS, p)
			%		h = ['edit_' p];
			%		PARAMS.(p) = str2num(get(handles.(h), 'string'));
			%	end
			%end

			%find default module
			default = PARAMS.default;
			MODULE = eval(default);

		end

		%update module menu
		module_list = get(handles.set_analysis_module, 'string');
		idx = find(strcmp(default, module_list));
		set(handles.set_analysis_module, 'value', idx);

		set(handles.edit_default, 'string', default);
		set(handles.edit_default_var, 'string', PARAMS.default_var);

		set(handles.n_frame_msg, ...
			'string', sprintf('of %d', PARAMS.(default).number_of_frames));

		set_analysis_module(MODULE.str, handles)

		guidata(handles.output, handles)

		%get the animal's eye width in millimeters

		global DBISOPEN
		if ~DBISOPEN
			dbopen;
		end

		[~,f,~] = fileparts(filename);
		sql = ['select * from gAnimal WHERE cellprefix="' f(1:3) '"'];
		animaldata = mysql(sql);
		PARAMS.eye_width_mm = animaldata.eyeWidth*10;

	end

end
%-----------------------------------------------------------------------------
% Clear results
function clear_results(hObject, eventdata, handles)

	global MODULE PARAMS RESULTS

	RESULTS.(MODULE.str) = []; %clear results
	MODULE.malloc(PARAMS.number_of_frames, MODULE); %malloc for new results
	cla(handles.time_ax), cla(handles.time_ax_local)

end

function clear_results_CreateFcn(hObject, eventdata, handles)
end
%-----------------------------------------------------------------------------
% Measure eye
function measure_eye_Callback(hObject, eventdata, handles)

	global RESULTS MODULE

	%get the current frame
	frame = get_param('frame');
	vid = get_param('vid');
	im = rgb2gray(read(vid, frame));

	%get eye measurement from user
	fig = figure;
	imshow(im)
	set(fig, 'menubar', 'none')
	iptsetpref('ImShowBorder', 'tight')
	l = imline;
	c = getPosition(l);
	close(fig)

	%save in results structure
	RESULTS.eye_width = sqrt(sum((c(:,1)-c(:,2)).^2));
	RESULTS.eye_line = c;

	refresh_frame(handles)
	MODULE.plot_trace(handles, MODULE)

end
%-----------------------------------------------------------------------------
% Exclude frames from analysis
function time_ax_local_ButtonDownFcn(hObject, eventdata, handles)

	global PARAMS

	%get selected frame
	coords = get(hObject, 'currentpoint');
	frame = floor(coords(1,1));

	%determine if frame begins a new excluded range
	switch eventdata.Button
		case 1
			is_start_frame = true;
		case 2
			is_start_frame = false;
		case 3
			is_start_frame = false;
	end

	%store new excluded frame
	excluded_frames = PARAMS.excluded_frames;
	if is_start_frame
		excluded_frames = cat(1, excluded_frames, [frame nan]);
	else
		[~, closest_start_frame] = min(abs(frame - excluded_frames(:,1)));
		excluded_frames(closest_start_frame, 2) = frame;
	end
	set_param('excluded_frames', excluded_frames)

	plot_excluded_frames(handles)

end

function plot_excluded_frames(handles)

	global PARAMS

	%clear excluded regions from axis
	graphics = handles.time_ax.Children;
	for k=1:length(graphics)
		if strcmp(graphics(k).Type, 'patch')
			delete(graphics(k))
		end
	end

	%replot excluded regions
	excluded_frames = PARAMS.excluded_frames;

	axes(handles.time_ax)
	axlim = axis;
	y1 = axlim(3);
	y2 = axlim(4);
	background = [0.5 0.5 0.5];

	for k=1:size(excluded_frames,1)
		x1 = excluded_frames(k,1);
		x2 = excluded_frames(k,2);
		p = patch([x1 x1 x2 x2], [y1 y2 y2 y1], background);
		alpha(0.1)
		set(p, 'buttondownfcn', {@remove_excluded_region, handles})
	end

end

function remove_excluded_region(hObject, eventdata, handles)

	global PARAMS

	frame = floor(eventdata.IntersectionPoint(1)); %get current frame

	%find and remove excluded region
	excluded_frames = PARAMS.excluded_frames;
	for k=1:size(excluded_frames,1)

		x1 = min(excluded_frames(k,:));
		x2 = max(excluded_frames(k,:));

		if (frame >= x1) & (frame <= x2)
			excluded_frames(k,:) = [];
			break
		end

	end

	if isempty(excluded_frames)
		excluded_frames = [];
	end

	set_param('excluded_frames', excluded_frames)

	refresh_slider(handles) %replot

end
%-----------------------------------------------------------------------------
% Analyze eye movements
%function analyze_eye_movements_Callback(hObject, eventdata, handles)
%
%	global MODULE PARAMS RESULTS
%
%	if not(isfield(MODULE, 'do_eye_movements'))
%		uiwait(msgbox('This module doesn''t support eye movement analysis.','','modal'));
%		return
%	end
%
%	%Update module with eye movements data
%	MODULE = MODULE.do_eye_movements(MODULE);
%	PARAMS.(MODULE.str).display_eye_movements = 1;
%
%	%Display on GUI
%	refresh_frame(handles)
%	refresh_slider(handles)
%
%end
%-----------------------------------------------------------------------------
% Quit
function quit_Callback(hObject, eventdata, handles)
	clear global MODULE PARAMS RESULTS STOPPED
	close(handles.output)
end 
