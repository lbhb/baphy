function a = dwindow_varythresh_cut()
%
%a = dwindow_varythresh_cut()
%
%Like dwindow_varythresh except it uses an iterative fitter to fit an
%ellipse with a cutout on either side to better fit images where the eyelid
%partially obscures the pupil. %Also, finds the minimum pixel value in each
%frame, and subtracts that from the image so that a fixed threshold is more
%reliable in the face of a camera with changing gain. To be more robust to noise,
%minimum is defined as the lowest number that is bigger than at least 10 pixels, minus 1.
% Ellipse minor axis is defined relative to major axis, and is constrained
% to be =/-5 re major axis by default. Change it with the "Set Algorithm Params" buttom.



%
%Inputs (params fields)
% im: Grayscale image
% inner_roi: Rectangular crop window for first frame
%  Format: [x_coord y_coord x_width y_width]
%   x_coord, y_coord: Coordinates of upper left-hand point of rectangle
%   x_width, y_width: Dimensions of rectangle
% high: Maximum upper threshold
%
%Returns (results fields)
% blob_width: Maximum width of blob
% minor_axis: Minor axis of ellipse
% major_axis: Major axis of ellipse
% roi: Rectangular crop window, [x_coord y_coord x_width y_width] format
% ellipse: All other ellipse parameters (see fit_ellipse.m for format)
% high: Upper threshold for each frame.
%
%Plot format
% left: Blobs found in cropped image, pupil outlined in red, non-pupil in blue
% center: Blobs found in full image, pupil outlined in red, non-pupil in blue
% right: Pupil blob overplotted on frame
%
%ZPS 2018-XX-XX: Initial version.
%LAS Aug 2018

a = [];
a.params = []; 
a.results = [];
a.str = 'dwindow_varythresh_cut';
a.malloc = @malloc;
a.do_frame = @do_frame;
a.plot_frame = @plot_frame;
a.plot_trace = @plot_trace;
a.plot_replay = @plot_replay;
a.default_params = @default_params;


end

function params=default_params
    params.min_im_threshold=40;
    params.min_minor_axis_re_major = -5;
    params.max_minor_axis_re_major = 5;
    params.max_major_axis = 75;
    params.low=0;
    params.high=18;
    params.smooth=1;
    params.smooth_hist=2;
		params.min_area=100;
		params.max_jump=120;
		params.fix_threshold=0;
end

% ------------------------------------------------------------------------
% Analysis methods

function malloc(frames, a)
  global RESULTS

  RESULTS.(a.str).roi = nan(frames,4); %region of interest
  RESULTS.(a.str).blob_width = nan(frames,1);
  RESULTS.(a.str).minor_axis = nan(frames,1);
  RESULTS.(a.str).major_axis = nan(frames,1);
  RESULTS.(a.str).low = nan(frames,1);
  RESULTS.(a.str).high = nan(frames,1);
  RESULTS.(a.str).min_im = nan(frames,1);

  RESULTS.(a.str).errors = nan(frames,1);

  RESULTS.(a.str).ellipse(1:frames) = ...
    struct('a',[],...
           'b',[],...
           'phi',[],...
           'X0',[],...
           'Y0',[],...
           'X0_in',[],...
           'Y0_in',[],...
           'long_axis',[],...
           'short_axis',[],...
           'estimates',[],...
           'sse',NaN,...
           'status','');
end

function a = do_frame(im, frame, a)

  global RESULTS

  %a.params.min_im_threshold=40;
  error_frame = false;

  original_im = im;
  
  im = imcrop(im, a.params.outer_roi); %crop to outer roi
  min_im=min(im(:));
  while sum(im(:)<=min_im) < 10
      min_im=min_im+1;
  end
  min_im=max(min_im-1,0);
  %original_im=original_im-min_im;
  im=im-min_im;
  im(im<0)=0;
  %apply gaussian smoothing to remove noise
  im = uint8(gsmooth(double(im),[a.params.smooth a.params.smooth]));

  h = hist(double(im(:)), 0:255); %intensity histogram

  %threshold image
  if a.params.fix_threshold && ~isempty(RESULTS.(a.str).ellipse(frame).a)
    low = RESULTS.(a.str).low(frame);
    high = RESULTS.(a.str).high(frame);
  else
    low = a.params.low;
    high = a.params.high;
  end
  thresh = uint8(ones(size(im)));
  thresh(im>high) = 0;
  thresh(im<low) = 0;

  %set current window
  if frame == 1
    roi = a.params.inner_roi;
  else
    global RESULTS
    roi = RESULTS.(a.str).roi(frame-1,:);
  end
  if isnan(roi)
    fprintf('\n%s: No ROI data for frame %d.\n', mfilename, frame);
    return
  end
  
  % svd expand out search window
  roi(1:2)=roi(1:2)-5;
  roi(3:4)=roi(3:4)+10;

  %correct for crop to outer roi
  roi(1) = ceil(roi(1) - a.params.outer_roi(1));
  roi(2) = ceil(roi(2) - a.params.outer_roi(2));
  roi(1) = max(roi(1), 3);
  roi(2) = max(roi(2), 3);
  roi(3) = min(roi(3), size(im,2)-roi(1)-2);
  roi(4) = min(roi(4), size(im,1)-roi(2)-2);

  %roi
  window = rect2mask(roi, size(im));
  center = [roi(1)+roi(3)/2 roi(2)+roi(4)/2];
  crop = thresh.*window; %crop to dynamic roi

  %find the biggest blob in the cropped image
  [b_crop, l_crop] = bwboundaries(crop, 'noholes'); %moore-neighbor tracing
  n = length(b_crop);
  if n==0 %error - could not find a blob
    big = [1 1];
    fprintf('%s: Could not find blob for frame %d.\n', mfilename, frame);
    error_frame = true;
  else
    areas = zeros(n,1);
    for k=1:n
      areas(k) = sum(l_crop(:)==k);
    end
    [~, idx] = max(areas);
    big = b_crop{idx};
  end
  u = mean(big); %centroid

  %find the blob with the most pixels in the window
  [b, l] = bwboundaries(thresh, 'noholes');
  if n==0 %error - could not find a blob
    blob = [1 1];
    fprintf('\n%s: Could not find pupil for frame %d.\n', mfilename, frame);
    error_frame = true;
  else
    n = length(b);
    areas = zeros(n,1);
    for k=1:n
      v = uint8(l==k);
      v = v.*window;
      areas(k) = sum(v(:)==1);
    end
    [~, idx] = max(areas);
    blob = b{idx};
  end

  %resize window to new blob
  new_window = zeros(size(im));
  x = blob(:,1);
  y = blob(:,2);
  new_window(min(x):max(x), min(y):max(y)) = 1;
  new_window = uint8(new_window);

  new_center = mean(blob); %move center to new blob

  stay = false;
  new_window_area = sum(sum(new_window));
  jump = sqrt(sum((new_center - center).^2));

  %resist disappearing windows
  if new_window_area < a.params.min_area
    stay = true;
    fprintf('\n%s: Small window (%d px)\n', mfilename, new_window_area);
  end

  %resist sudden position changes
  if jump > a.params.max_jump
    stay = true;
    fprintf('\n%s (%d): Large jump (%0.0f px)\n', mfilename, frame, jump);
  end

  if stay
    new_window = window;
  end

  if a.params.fix_threshold && ~isempty(RESULTS.(a.str).ellipse(frame).a)
      ellipse = RESULTS.(a.str).ellipse(frame);
      %high = RESULTS.(a.str).high(frame);
  else
      %fit ellipse
      y = blob(:,1);
      x = blob(:,2);
      try
          k = convhull(x,y);
      catch
          k=true(size(x));
      end
      %first use closed-form fit to get good seed values
      ellipse = fit_ellipse(x(k),y(k));
      
      if not(isempty(ellipse)) && ellipse.long_axis < max(size(im)) && ~isempty(ellipse.a) && min_im < a.params.min_im_threshold
          if ~isempty(ellipse.X0_in)
              %if the closed-form fit found an ellipse, use the parameters as
              %seeds to the iterative fit
              modelname='ellipseR_cutoff_1dx2';
              low_bounds=nan(1,7); high_bounds=nan(1,7);
              low_bounds(4)=a.params.min_minor_axis_re_major;
              high_bounds(4)=a.params.max_minor_axis_re_major;
              high_bounds(3)=a.params.max_major_axis;
              [ellipse,estimates,sse]=fit_ellipse_fminsearch(x(k),y(k),...
                  [ellipse.X0_in,ellipse.Y0_in,ellipse.a,ellipse.b,ellipse.phi+pi/4],...
                  [],modelname,low_bounds,high_bounds);
              ellipse.sse=sse;
              ellipse.estimates=estimates;
          end
          %correct ellipse for crop to outer roi
          ellipse.X0 = ellipse.X0 + a.params.outer_roi(1);
          ellipse.Y0 = ellipse.Y0 + a.params.outer_roi(2);
          ellipse.X0_in = ellipse.X0_in + a.params.outer_roi(1);
          ellipse.Y0_in = ellipse.Y0_in + a.params.outer_roi(2);
          
      else %could not fit ellipse
          
          error_frame = true;
          
          ellipse.a = 0;
          ellipse.b = 0;
          ellipse.phi = 0;
          ellipse.X0 = 0;
          ellipse.Y0 = 0;
          ellipse.X0_in = 0;
          ellipse.Y0_in = 0;
          ellipse.long_axis = 0;
          ellipse.short_axis = 0;
          ellipse.estimates=[];
          ellipse.sse=NaN;
          ellipse.status = '';
          
      end
  end
  minor_axis = ellipse.short_axis;
  major_axis = ellipse.long_axis;
  blob_width = max(max(x)-min(x), max(y)-min(y));

  %correct window for crop to outer roi
  new_roi = mask2rect(new_window);
  new_roi(1) = floor(new_roi(1) + a.params.outer_roi(1));
  new_roi(2) = floor(new_roi(2) + a.params.outer_roi(2));

  %correct blob for crop to outer roi
  im_blob = blob;
  im_blob(:,1) = floor(im_blob(:,1) + a.params.outer_roi(2));
  im_blob(:,2) = floor(im_blob(:,2) + a.params.outer_roi(1));

  %save in temporary results structure
  a.params.frame=frame;
  a.results.im = original_im;
  a.results.big = big;
  a.results.blob = blob;
  a.results.im_blob = im_blob;
  a.results.b_crop = b_crop;
  a.results.l_crop = l_crop;
  a.results.b = b;
  a.results.l = l;
  a.results.ellipse = ellipse;
  a.results.h = h;
  a.results.low = low;
  a.results.high = high;
  a.results.min_im(frame)=min_im;
  %save in permanent results structure
  if ~a.params.fix_threshold || isempty(RESULTS.(a.str).ellipse(frame).a)
      RESULTS.(a.str).roi(frame,:) = new_roi;
      RESULTS.(a.str).blob_width(frame) = blob_width;
      RESULTS.(a.str).major_axis(frame) = major_axis;
      RESULTS.(a.str).minor_axis(frame) = minor_axis;
      RESULTS.(a.str).ellipse(frame) = ellipse;
      RESULTS.(a.str).low(frame) = low;
      RESULTS.(a.str).high(frame) = high;
      RESULTS.(a.str).min_im(frame)=min_im;
      RESULTS.(a.str).errors(frame) = error_frame;
  end
end

% ------------------------------------------------------------------------
% Plot methods

function plot_frame(im, frame, ax, a)

  global RESULTS STOPPED

  %unpack the results structure
  b = a.results.b;
  l = a.results.l;
  b_crop = a.results.b_crop;
  l_crop = a.results.l_crop;
  big = a.results.big;
  blob = a.results.blob;
  im_blob = a.results.im_blob;

  if ~STOPPED
      %convert rois to boundary pixel positions
      inner_roi = rect2mask(RESULTS.(a.str).roi(frame,:), size(im));
      u = bwboundaries(inner_roi);
      outer_roi = rect2mask(a.params.outer_roi, size(im)); 
      v = bwboundaries(outer_roi);
      w = [u{1}; v{1}];
      w=w(w(:,1)<=size(im,1) & w(:,2)<=size(im,2),:);
  end
  %reserve the five lowest values of the colormap for pseudocolors
  pseudo = ([0 0 0;    %black: background
             1 1 1;    %white: blob interiors
             1 1 0;    %yellow: window boundaries
             0 0 1;    %blue: all blob boundaries
             1 0 0;]); %red: big blob boundary
  %use the next 251 values for grays
  grayscale = repmat(0:(1/250):1, 3, 1)';

  %show blobs detected in cropped image
  if STOPPED
    axes(ax.left)
    if 0
        ch=get(ax.left,'Children');
        imi=strcmp(get(ch,'Type'),'image');
        set(ch(imi),'CData',l_crop)
        delete(ch(~imi))
    else
        imshow(l_crop)
    end
    hold on
    for k=1:length(b_crop)
      plot(b_crop{k}(:,2), b_crop{k}(:,1), 'b')
    end
    if sum(big) > 0
      plot(big(:,2), big(:,1), 'r')
    end
    hold off
  else
    c = l_crop; %pixel intensity map
    c(c>=1) = 2; 
    for k=1:length(b_crop)
      c(sub2ind(size(c), b_crop{k}(:,1), b_crop{k}(:,2))) = 4;
    end
    if sum(blob) > 2
      c(sub2ind(size(c), big(:,1), big(:,2))) = 5;
    end
    update_ax(ax.left, c)
  end

  %show blobs detected in uncropped image
  if STOPPED
    axes(ax.center)
    if 0
        ch=get(ax.center,'Children');
        imi=strcmp(get(ch,'Type'),'image');
        set(ch(imi),'CData',l)
        delete(ch(~imi))
    else
        imshow(l)
    end
    hold on
    for k=1:length(b)
      plot(b{k}(:,2), b{k}(:,1), 'b')
    end
    if sum(big) > 0
      plot(blob(:,2), blob(:,1), 'r')
    end
    el = a.results.ellipse;
    if ~isempty(el.estimates)
        R=linspace(0,2*pi,size(blob,1));
        [x2,y2]=ellipseR_cutoff_1dx2(R,[el.X0_in, el.Y0_in, el.estimates(3:end)]);
        ph=plot(x2-a.params.outer_roi(1),y2-a.params.outer_roi(2),'c');
    end
     if sum(big) > 0
         try
             k = convhull(blob(:,2), blob(:,1));
         catch
             k=true(size(blob,1),1);
         end
      plot(blob(k,2), blob(k,1), '--m')
     end
    hold off
  else
    c = l;
    c(c>=1) = 2; 
    for k=1:length(b)
      c(sub2ind(size(c), b{k}(:,1), b{k}(:,2))) = 4;
    end
    if sum(blob) > 2
      c(sub2ind(size(c), blob(:,1), blob(:,2))) = 5;
    end
    update_ax(ax.center, c)
  end

  %show identified pupil
  if STOPPED
    axes(ax.right)
    el = a.results.ellipse;
    xc = blob(:,1) + a.params.outer_roi(2);
    yc = blob(:,2) + a.params.outer_roi(1);
    d = sprintf('%0.f, %0.f', el.short_axis, el.long_axis);
    if 0
        ch=get(ax.right,'Children');
        imi=strcmp(get(ch,'Type'),'image');
        set(ch(imi),'CData',im)
        delete(ch(~imi))
    else
        imshow(im)
    end
    hold on
    %draw_ellipse(el.b, el.a, el.phi, el.Y0_in, el.X0_in, 'r');
    if ~isempty(el.estimates)
        ph=plot(x2,y2,'-c');
    end
    h=draw_ellipse(el.a, el.b, el.phi, el.X0_in, el.Y0_in, 'r'); set(h,'LineStyle','--');
    rectangle('position', a.params.outer_roi, ...
      'linewidth', 1, 'edgecolor', 'y');
    rectangle('position', RESULTS.(a.str).roi(frame,:), ...
      'linewidth', 1, 'edgecolor', 'y');
    text(mean(yc), mean(xc), d, 'color', 'g');
    hold off
  else
    c = im;
    c(c<5)=6; %compress the five darkest intensities to allow pseudocoloring
    if ~isempty(w)
        c(sub2ind(size(c), w(:,1), w(:,2))) = 2;
    end
    if sum(im_blob) > 2
       im_blob = im_blob(im_blob(:,1)<=size(c,1) & im_blob(:,2)<=size(c,2), :);
       c(sub2ind(size(c), im_blob(:,1), im_blob(:,2))) = 4;
    end
    update_ax(ax.right, c) 
  end

  %show frame
  if STOPPED
    axes(ax.im_ax)
    if 1
        ch=get(ax.im_ax,'Children');
        imi=strcmp(get(ch,'Type'),'image');
        set(ch(imi),'CData',im)
        delete(ch(~imi))
    else
        imshow(im)
    end
    rectangle('position', RESULTS.(a.str).roi(frame,:), ...
      'linewidth', 1, 'edgecolor', 'y');
    rectangle('position', a.params.outer_roi, ...
      'linewidth', 1, 'edgecolor', 'y');
    if isfield(RESULTS, 'eye_line')
      hold on
      l = RESULTS.eye_line;
      plot(l(:,1), l(:,2), 'y')
      hold off
    end
  else
    c = im;
    c(c<5)=6; %compress the five darkest intensities to allow pseudocoloring
    c(sub2ind(size(c), w(:,1), w(:,2))) = 2;
    update_ax(ax.im_ax, c);
  end

  if ~STOPPED
    if verLessThan('Matlab', '9.1')
      colormap([pseudo; grayscale])
    else
      colormap(ax.im_ax, [pseudo; grayscale]);
      colormap(ax.left, [pseudo; grayscale]);
      colormap(ax.center, [pseudo; grayscale]);
      colormap(ax.right, [pseudo; grayscale]);
    end
  end

  if STOPPED
    h = a.results.h;
    low = max(1, a.results.low);
    axes(ax.hist_ax)
    cla
    lw = 2;
    hold on
    plot(h, 'r', 'linewidth', lw);
    plot(low,  h(low),  'k.', 'markersize', 20)
    plot(a.results.high, h(a.results.high), 'g.', 'markersize', 20)
    hold off
  end

  if ~STOPPED || 1
      drawnow
  end

end

function plot_trace(ax, a)

  global PARAMS RESULTS STOPPED

  frame = a.params.frame;

  minor_axis = RESULTS.(a.str).minor_axis;
  fstop = max(find(~isnan(minor_axis)));
  minor_axis = minor_axis(1:fstop);
  major_axis = RESULTS.(a.str).major_axis(1:fstop);
  low = RESULTS.(a.str).low(1:fstop);
  high = RESULTS.(a.str).high(1:fstop);
  min_im = RESULTS.(a.str).min_im(1:fstop);
  %fstop=1000;
  %if isfield(RESULTS, 'eye_width') & isfield(PARAMS, 'eye_width_mm')
  %  px_to_mm = PARAMS.eye_width_mm/RESULTS.eye_width;
  %  minor_axis = minor_axis.*px_to_mm;
  %  major_axis = major_axis.*px_to_mm;
  %  ylabel_str = 'Pupil Measurement (mm)';
  %else
  %  ylabel_str = 'Pupil Measurement (px)';
  %end
  
  ylabel_str = 'Pupil Measurement (px)';

  p = get(ax.time_ax, 'children');
  if isempty(p)

    axes(ax.time_ax)
    plot(minor_axis, 'r')
    hold on
    plot(major_axis, 'b')
    plot(low, 'k')
    plot(high, 'g')
    plot(min_im, 'Color',[0 .5 0])
    legend({'Minor Axis', 'Major Axis', 'Low Threshold', 'High Threshold','min(image)'})
    xlabel('Frame')
    ylabel(ylabel_str)
    legend boxoff
    box off
    drawnow
    hold off

  elseif mod(frame, 30) == 0

    set(p(5), 'xdata', 1:fstop, 'ydata', minor_axis);
    set(p(4), 'xdata', 1:fstop, 'ydata', major_axis);
    set(p(3), 'xdata', 1:fstop, 'ydata', low);
    set(p(2), 'xdata', 1:fstop, 'ydata', high);
    set(p(1), 'xdata', 1:fstop, 'ydata', min_im);
    a.plot_frame(a.results.im, frame, ax, a)
  end

  if STOPPED

    axes(ax.time_ax)
    axis tight
    
    axes(ax.time_ax_local)
    region = 60;
    rstart = max(1, frame-region);
    rstop = min(frame+region, fstop);
    rcenter = min(frame, fstop);
    if isfield(RESULTS.(a.str), PARAMS.default_var)
      local_data = RESULTS.(a.str).(PARAMS.default_var);
      local_data = eval(PARAMS.default_var);
      plot(rstart:rstop, local_data(rstart:rstop), 'k')
      hold on
      plot(rcenter, local_data(rcenter), 'r.', 'markersize', 15)
      l = legend(PARAMS.default_var);
      set(l, 'interpreter', 'none')
      legend boxoff
      xlabel('Frame')
      ylabel(ylabel_str)
      box off
      hold off
      axis tight
      drawnow
    else
      fprintf('%s: No data on %s. Change default variable.\n', ...
	mfilename, PARAMS.default_var)
    end

  end

end

function plot_replay(im, frame, fig, a)

  a = a.do_frame(im, frame, a); %analyze frame
  e = a.results.ellipse;

  figure(fig)
  hold on
  imshow(im)
  if ~isempty(e)
    draw_ellipse(e.b, e.a, e.phi, e.Y0_in, e.X0_in, 'r');
  else
    x = size(frame,2)/2;
    y = size(frame,1)/2;
    text(x,y, 'FAIL')
  end

  hold off

end

% ------------------------------------------------------------------------
% Helper functions

function mask = rect2mask(rect, dim)
  mask = uint8(zeros(dim));
  x = ceil((1:rect(4)) + rect(2) - 1);
  y = ceil((1:rect(3)) + rect(1) - 1);
  mask(x,y) = 1;
end

function rect = mask2rect(mask)
  x_coord = min(find(sum(mask,1)));
  y_coord = min(find(sum(mask,2)));
  x_width = max(find(sum(mask,1))) - x_coord + 1;
  y_width = max(find(sum(mask,2))) - y_coord + 1; 
  rect = [x_coord y_coord x_width y_width];
end

function update_ax(ax, c)
  h = get(ax, 'children');
  set(ax, 'nextplot', 'replace')
  if isempty(h) | length(h) > 1
    axes(ax)
    h = imshow(c);
    set(h, 'cdatamapping', 'direct')
  else
    set(h, 'cdata', c, 'cdatamapping', 'direct')
  end
end
