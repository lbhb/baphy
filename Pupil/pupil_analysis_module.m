function a = module_name()
%
%a = module_name()
%
%Description of the algorithm.
%
%Inputs (params fields)
%
%Returns (results fields)
%
%Plot format
% left:
% center:
% right:
%
%ZPS 2016-1-2

a = [];
a.params = []; %parameters
a.results = []; %buffer for results from single frame
a.str = 'module_name'; %module name - used to identify saved data
a.malloc = @malloc; %allocates memory in global results structure
a.do_frame = @do_frame; %analyzes one frame, saves results
a.plot_frame = @plot_frame; %displays analysis of frame in the gui
a.plot_trace = @plot_trace; %plots trace of pupil diameter in the gui
a.plot_replay = @plot_replay; %displays one annotated frame in replay video

end

% ------------------------------------------------------------------------
% Analysis methods

function malloc(frames, a)
  global RESULTS
end

function a = do_frame(im, frame, a)
end

% ------------------------------------------------------------------------
% Plot methods

function plot_frame(im, frame, ax, a)
end

function plot_trace(ax, a)
end

function plot_replay(im, frame, fig, a)
end
