cam = imaqhwinfo; % Get Camera information
cameraName = char(cam.InstalledAdaptors(end));
cameraInfo = imaqhwinfo(cameraName);
cameraId = cameraInfo.DeviceInfo.DeviceID(end);
format_index=3;
cameraFormat = char(cameraInfo.DeviceInfo.SupportedFormats(format_index));

%one way to create a video device:
vidDevice = imaq.VideoDevice(cameraName, cameraId, cameraFormat,'ReturnedColorSpace', 'RGB');
snap=step(vidDevice);
imagesc(snap)

%a different way:
format_index=1;
delete(imaqfind)%close all open cams
cam = videoinput('winvideo', 1,cameraInfo.DeviceInfo.SupportedFormats{format_index});
snap=getsnapshot(cam);
figure;imagesc(snap)

cameraInfo.DeviceInfo.SupportedFormats' %returns:
{ 
    'UYVY_360x240' %Times out
    'UYVY_360x288' %Times out
    'UYVY_720x480' %Works!!!!
    'UYVY_720x576' %Works but has extra blank pixels (and it's bigger!)
    'Y8  _360x240' %Times out
    'Y8  _360x288' %Times out
    'Y8  _720x576' %Works but distorted image
};

%Times out
%cameraInfo.DeviceInfo.SupportedFormats{1}
delete(imaqfind)%close all open cams
cam = videoinput('winvideo', 1,'UYVY_360x240');
snap=getsnapshot(cam);
imagesc(snap)

%Times out
%cameraInfo.DeviceInfo.SupportedFormats{2}
delete(imaqfind)%close all open cams
cam = videoinput('winvideo', 1,'UYVY_360x288');
snap=getsnapshot(cam);
imagesc(snap)

%Works!!!!
%cameraInfo.DeviceInfo.SupportedFormats{3}
delete(imaqfind)%close all open cams
cam = videoinput('winvideo', 1,'UYVY_720x480');
snap=getsnapshot(cam);
imagesc(snap)

%cameraInfo.DeviceInfo.SupportedFormats{4}
delete(imaqfind)%close all open cams
cam = videoinput('winvideo', 1,'UYVY_720x576');
snap=getsnapshot(cam);
imagesc(snap)

%Times out
%cameraInfo.DeviceInfo.SupportedFormats{5}
delete(imaqfind)%close all open cams
cam = videoinput('winvideo', 1,'Y8  _360x240');
snap=getsnapshot(cam);
imagesc(snap)

%Times out
%cameraInfo.DeviceInfo.SupportedFormats{6}
delete(imaqfind)%close all open cams
cam = videoinput('winvideo', 1,'Y8  _360x288');
snap=getsnapshot(cam);
imagesc(snap)

%works but distorted image
%cameraInfo.DeviceInfo.SupportedFormats{7}
delete(imaqfind)%close all open cams
cam = videoinput('winvideo', 1,'Y8  _720x576');
snap=getsnapshot(cam);
imagesc(snap)