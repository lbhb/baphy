function pupil_start_recording(hObject, handles)

global MG

% Determine format (pupil or photometry)
if isfield(MG, 'format')
    format = MG.format;
else
    format = 'pupil';
end
if strcmp(format,'lick')
    lick_start_recording(hObject, handles)
    return
end

f = get(handles.video_file, 'String');
[path,name,ext] = fileparts(f);

if isfield(MG, 'video')
    close(MG.video)
end

%Create a new video object
if strcmp(format, 'photometry')
    MG.video = VideoWriter(f, 'Grayscale AVI');
elseif strcmp(format, 'pupil_dual')
   if strcmpi(ext,'.avi')
       MG.video = VideoWriter(f);
       f2=strrep(f,'.avi','_2.avi');
       MG.video2 = VideoWriter(f2);
   else
       error('fix me')
   end
elseif strcmpi(ext,'.avi')
    MG.video = VideoWriter(f);
elseif strcmpi(ext,'.mj2')
    MG.video = VideoWriter(f, 'Archival');
else
    error(['unknown extension for pupil video format: ',ext]);
end
guidata(hObject, handles);

%Start recording
MG.recording=1;
MG.frames_written=0;
open(MG.video);
if strcmp(format, 'pupil_dual')
    open(MG.video2);
end
while MG.recording
    MG.im = getsnapshot(MG.cam);
    %Crop out everything but the eye
    frame.cdata = imcrop(MG.im, MG.roi);
    frame.colormap = [];
    if MG.resize_factor~=1
        % MG.resize_factor is the factor by which to resize pupil video. 
        % Defined in puil_online.m.In new MATLAB,videos can only be 
        % recorded at 720x480, we resize to save disk space.
        frame.cdata = imresize(frame.cdata,MG.resize_factor);
    end
    %Save video
    if strcmp(format, 'photometry')
        frame.cdata = mean(frame.cdata, 3);
        frame.cdata = frame.cdata / 2^16;
    end
    
    writeVideo(MG.video, frame);
    if strcmp(format, 'pupil_dual')
        MG.im2 = getsnapshot(MG.cam2);
        frame2.cdata = MG.im2;
        frame2.colormap = [];
        writeVideo(MG.video2, frame2);
    end
    
    %Save a timestamp
    MG.timestamp = now;
    MG.frames_written=MG.frames_written+1;
    if get(MG.Stim.TCPIP,'BytesAvailable'),
        M_Pupil_TCPIP(MG.Stim.TCPIP);
    end
    

    
    drawnow;
end
close(MG.video);
if strcmp(format, 'pupil_dual')
    close(MG.video2);
end