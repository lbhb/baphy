function [ellipse,estimates,sse,model,fitx,fity]=fit_ellipse_fminsearch(x,y,seeds,previous,modelname,low_bounds,high_bounds)
if nargin<=3
    previous=[];
end
if nargin<=4
    modelname='ellipseR_cutoff_1dx2';
end
if nargin<=5
    low_bounds=[];
end
if nargin<=6
    high_bounds=[];
end
fitter = @fit_fun;

%chooose which model to fit here
model=str2func(modelname);
switch modelname
    case 'ellipse_fn'
        %standard ellipse
        start_point=[seeds];
        low_bounds=nan(size(start_point));
        high_bounds=nan(size(start_point));
    case 'ellipse_cutoff_1d'
        %ellipse with cutoff on low side
        start_point=[seeds min(x)+5];
        low_bounds=nan(size(start_point));
        high_bounds=nan(size(start_point));
    case 'ellipse_cutoff_1dx2'
        %ellipse with cutoff on both sides
        start_point=[seeds min(x)+5 max(x)-5];
        low_bounds=nan(size(start_point));
        high_bounds=nan(size(start_point));
    case 'ellipseR_cutoff_1dx2'
        %ellipse with cutoff on both sides, minor axis defined relative to major axis
        %parameters are: [center x, center y, major axis, minor axis re major axis, rotation, low side cutoff, high side cutoff]
        start_point=[seeds(1:2) mean(seeds(3:4)) 0  seeds(5) min(y)+5 max(y)-5];
        if isempty(low_bounds)
            low_bounds=nan(size(start_point));
        end
        if isempty(high_bounds)
            high_bounds=nan(size(start_point));
        end
%         low_bounds(4)=-5;
%         high_bounds(4)=5;
%         high_bounds(3)=75;
        if ~isempty(previous)
            %    high_bounds(3)=previous(3)*1.25;
        end
        %     start_point(6)=0;
        %     start_point(7)=10000;
        %     high_bounds(6)=1;
        %     low_bounds(7)=9999;
end

fixed_params=false(size(start_point));
fixed_vals=[];
R=linspace(0,2*pi,length(x));
%err=zeros(size(x));

options.MaxFunEvals=10^4;
options.MaxIter=10^4;
options.OutputFcn=[];
options.TolFun=10^-6;
options.TolX=10^-6;
options.Display='off';
sse_=nan(1e5,1);
ind=0;
[estimates,trash,trash,output] = fminsearchbnd(fitter, start_point,low_bounds,high_bounds,options);

sse=fit_fun(estimates);
ellipse.a=estimates(3);
if strcmp(func2str(model),'ellipseR_cutoff_1dx2')
    ellipse.b=estimates(3)+estimates(4);
else
    ellipse.b=estimates(4);
end
ellipse.phi=estimates(5);
ellipse.X0=estimates(1);
ellipse.Y0=estimates(2);
ellipse.X0_in=estimates(1);
ellipse.Y0_in=estimates(2);
ellipse.status='';

ellipse.long_axis=max(ellipse.a,ellipse.b)*2;
ellipse.short_axis=min(ellipse.a,ellipse.b)*2;



[fitx,fity]=model(R,estimates);
model=func2str(model);

    function [sse] = fit_fun(params_,varargin)
        %params_(2)=1.77;
        this_params(fixed_params)=fixed_vals;
        this_params(~fixed_params)=params_;
        [xfit,yfit] = model(R,this_params);
        sse=0;
        % Find sse for minimization by looping through all data points:
        % For each data point, find the distance to the closest model point.
        % Sum over all these to get the sse
        % This is super slow...
        for i=1:length(x)
            sse=sse+min((xfit-x(i)).^2 + (yfit-y(i)).^2);
        end
        %sse = nansum(err);
        ind=ind+1;
        sse_(ind)=sse;
        %params__(end+1,:)=this_params;
        %        ErrorVector = FittedCurve - ydata;
        %sse = nansum(ErrorVector .^ 2);
    end

end