function [status] = IOReceiveMessageTCPIP(ConnData,callbackfcn)
% LAS: currently hard-coded to only work with Lick connection. Should be able
% to make this generic for MANTA, Pupil and anything else that comes along
global Verbose
global TcpConnections
if ~exist('Output','var') Output = ''; end

status=1;

% CLEAR BUFFER BEFORE READING
conid=['port',num2str(ConnData.Port)];
disp(conid);
set(TcpConnections.(conid),'BytesAvailableFcn',callbackfcn);

%flushinput(Conn);
% jtcp('read',ConData.Server,'maxnumbytes',inf);

% SEND MESSAGE
%disp(MSG);
%fwrite(Conn,MSG);
%jtcp('write',Conn,int8(MSG));

% IF YOU EXPECT A CERTAIN RESPONSE OR WANT THE RESPONSE
% if ~exist('ACK','var') || ~isempty(ACK)
%   % COLLECT RESPONSE
%   tic;
%   while ~get(Conn,'BytesAvailable') && toc<10  pause(0.01); end;  pause(0.1);
%   try
%     RESP = char(fread(Conn,get(Conn, 'BytesAvailable'))');
%   catch e
%       if strcmp(e.message,'SIZE must be greater than 0.') && strcmp(e.identifier,'instrument:fread:invalidSIZEpos')
%           RESP=e.message;
%           status=0;
%           return
%       else
%           rethrow(err)
%       end
%   end
%       
%       
%   % REMOVE LINEFEEDS
%   RESP = RESP(find(int8(RESP)~=10));
%   % GO TO TERMINATOR, AND REMOVE IT
%   Pos = find(int8(RESP)==ConnData.MSGterm);
%   if ~isempty(Pos) RESP = RESP(1:Pos(1)-1); end
%   flushinput(Conn);
%   %   RESP = [ ]; dRESP = [ ]; tic;
%   %   while isempty(RESP) & toc < Conn.TimeOut/1000
%   %     RESP = char(jtcp('read',Conn,'maxnumbytes',2^18));
%   %   end
%   %   while ~isempty(dRESP) & toc < Conn.TimeOut/1000
%   %     pause(0.02);
%   %     dRESP = char(jtcp('read',Conn,'maxnumbytes',2^18));
%   %     RESP = [RESP,dRESP];
%   %   end
%   %   RESP = RESP(1:end-1); % STRIP TRAILING NEWLINE
%   if Verbose fprintf(['Client returned : ',RESP,'\n']); end
%   
%   % NOW CHECK WHETHER THE APPRORIATE ANSWER CAME
%   if exist('ACK','var') && ~isempty(ACK)
%     switch RESP
%       case ACK; if ~isempty(Output) fprintf(Output); end
%       otherwise
%         % ATTEMPT RECONNECTION AFTER MANTA HAS RECOVERED
%         if exist('Reconnect','var') & Reconnect
%           fprintf('No or different Response "',RESP,'" received from MANTA.\n'); Connected = 0;
% %          global globalparams;
% %          while ~Connected
% %            fprintf('Press a button to try to reconnect\n'); pause;
% %            try
% %              [HW,globalparams] = IOConnectPupil(HW,globalparams);
% %              Connected =1; ConData.RepeatLast = 1;
% %            end
% %          end
%           [RESP,ConnData] = IOSendMessageTCPIP(HW.ConnData,MSG,ACK,Output);
%         end
%     end
%   end
% end