function [HW,globalparams] = IOConnectPupil(HW,globalparams,service)          

global Verbose; if isempty(Verbose), Verbose = 1; end
global TcpConnections

if ~exist('service','var')
    service='Pupil';
end

% ESTABLISH SIMPLE TCPIP SERVER TO COMMUNICATE FOR SAVING & SYNCHING
if strcmpi(service,'Pupil')
    SaveFile = globalparams.PupilFilename;
    HW.(service) = struct('COMterm','|','MSGterm',33,'TimeOut',5,'Port',33331);
elseif strcmpi(service,'Photometry')
    SaveFile = globalparams.PhotometryFilename;
    HW.(service) = struct('COMterm','|','MSGterm',33,'TimeOut',5,'Port',33332);
elseif strcmpi(service,'Lick')
    SaveFile = globalparams.LickFilename;
    HW.(service) = struct('COMterm','|','MSGterm',33,'TimeOut',5,'Port',33333);
else
    error('unknown service')
end

conid=['port',num2str(HW.(service).Port)];
if isfield(TcpConnections,conid)
    Conn = TcpConnections.(conid);
else
    Conn = [];
end

% TAKE LAST CONNECTION AS THE ACTIVE ONE
if ~isempty(Conn)
  HW.(service).Connect = strcmp(get(Conn,'Status'),'open');
else
  HW.(service).Connect = 0;
end

% TEST PREVIOUS CONNECTIONS & CONNECT
if HW.(service).Connect > 0
  fprintf(['Checking Connection...']);
  MSG =  ['COMTEST', HW.(service).COMterm, HW.(service).MSGterm];
  try 
    RESP = IOSendMessageTCPIP(HW.(service),MSG);
  catch
    RESP = '';
  end
  switch RESP
    case 'COMTEST OK';  fprintf('OK\n'); HW.(service).Connect = 1;
    otherwise fprintf('dead\n'); HW.(service).Connect = 0; 
  end
end

% CONNECT TO Pupil
if ~HW.(service).Connect
  fprintf(['Waiting for connect from ',service,' ... ']); Connected = 0;
  %HW.(service).Server=jtcp('ACCEPT',HW.(service).Port,'timeout',10000);
  Conn = tcpip('0.0.0.0',HW.(service).Port,'TimeOut',10,'OutputBufferSize',2^18,'InputBufferSize',2^18,'NetworkRole','server');
  fopen(Conn);
  flushinput(Conn); flushoutput(Conn);
  set(Conn,'Terminator',HW.(service).MSGterm);
  fprintf([' TCP IP connection established.\n']);
  TcpConnections.(conid) = Conn;
end

% SEND INITIALIZATION

fprintf('Pupil saving to: %s\n',SaveFile);
MSG = ['INIT',HW.(service).COMterm,SaveFile,HW.(service).MSGterm];
fprintf('Sending pupil init message\n')
Resp=IOSendMessageTCPIP(HW.(service),MSG,'INIT OK','>> Photometry system ready!\n');
fprintf('Pupil responded with: %s\n',Resp)
if ~strcmp(Resp,'INIT OK')
    warning('Expected pupil to respond with ''INIT OK'', waiting 1 sec then trying again.')
    pause(1);
    Resp=IOSendMessageTCPIP(HW.(service),MSG,'INIT OK','>> Photometry system ready!\n');
    fprintf('Pupil responded with: %s\n',Resp)
    if ~strcmp(Resp,'INIT OK')
        keyboard %Figure out why this happened. If you just keep going it will crash. Usually restarting MATLAB fixes this
    end
end