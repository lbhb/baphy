function [x,y]=ellipseR_cutoff_1dx2(R,params)
    ellipse_pars=params(1:5);
    cutoffs=params(6:7);
    [x,y]=ellipseR(R,ellipse_pars);
    y(y<=cutoffs(1))=cutoffs(1);
    y(y>=cutoffs(2))=cutoffs(2);
    