function a = dynamic_window()
%
%a = dynamic_window()
%
%Enhanced static-window pupil detection algorithm. Restricts search for pupil
%to a region of interest whose size and location depend on the size and location
%of pupil in the previous frame of the video.
%
%Inputs (params fields)
% im: Grayscale image
% high, low: Upper and lower threshold
% inner_roi: Rectangular crop window for first frame
%  Format: [x_coord y_coord x_width y_width]
%   x_coord, y_coord: Coordinates of upper left-hand point of rectangle
%   x_width, y_width: Dimensions of rectangle
%
%Returns (results fields)
% blob_width: Maximum width of blob
% ellipse_width: Major axis of ellipse fit to blob
% roi: Rectangular crop window, [x_coord y_coord x_width y_width] format
%
%Plot format
% left: Blobs found in cropped image, pupil outlined in red, non-pupil in blue
% center: Blobs found in full image, pupil outlined in red, non-pupil in blue
% right: Pupil blob overplotted on frame
%
%ZPS 2016-1-8: First version
%ZPS 2016-2-5: Revised search method. Earlier search method:
% (1) Crop/threshold.
% (2) Find largest object in cropped image ("big").
% (3) Find object in full image with centroid closest to "big."
%This method has a tendency to identify noise as pupil, if the noise happens to
%have a center close to "big." The new search method corrects this:
% (1) Crop/threshold.
% (2) Find object with the most pixels that fall in the cropped image.

a = [];
a.params = []; 
a.results = [];
a.str = 'dynamic_window';
a.malloc = @malloc;
a.do_frame = @do_frame;
a.plot_frame = @plot_frame;
a.plot_trace = @plot_trace;
a.plot_replay = @plot_replay;

end

% ------------------------------------------------------------------------
% Analysis methods

function malloc(frames, a)
  global RESULTS
  RESULTS.(a.str).roi = nan(frames,4);
  RESULTS.(a.str).blob_width = nan(frames,1);
  RESULTS.(a.str).ellipse_width = nan(frames,1);
end

function a = do_frame(im, frame, a)

  error_frame = false;

  original_im = im;
  
  im = imcrop(im, a.params.outer_roi); %crop to outer roi 
  
  %apply gaussian smoothing to remove noise
  im=uint8(gsmooth(double(im),[a.params.smooth a.params.smooth]));

  %threshold
  thresh = uint8(ones(size(im)));
  thresh(im>a.params.high) = 0;
  thresh(im<a.params.low) = 0;

  %set current window
  if frame == 1
    roi = a.params.inner_roi;
  else
    global RESULTS
    roi = RESULTS.(a.str).roi(frame-1,:);
  end
  if isnan(roi)
    fprintf('\n%s: No ROI data for frame %d.\n', mfilename, frame);
    return
  end

  %correct for crop to outer roi
  roi(1) = ceil(roi(1) - a.params.outer_roi(1));
  roi(2) = ceil(roi(2) - a.params.outer_roi(2));
  roi(1) = max(roi(1), 1);
  roi(2) = max(roi(2), 1);

  window = rect2mask(roi, size(im));
  center = [roi(1)+roi(3)/2 roi(2)+roi(4)/2];

  crop = thresh.*window; %crop to dynamic roi

  %find the biggest blob in the cropped image
  [b_crop, l_crop] = bwboundaries(crop, 'noholes'); %moore-neighbor tracing
  n = length(b_crop);
  if n==0 %error - could not find a blob
    big = [1 1];
    fprintf('%s: Could not find blob for frame %d.\n', mfilename, frame);
    error_frame = true;
  else
    areas = zeros(n,1);
    for k=1:n
      areas(k) = sum(l_crop(:)==k);
    end
    [~, idx] = max(areas);
    big = b_crop{idx};
  end
  u = mean(big); %centroid

  %find the blob with the most pixels in the window
  [b, l] = bwboundaries(thresh, 'noholes');
  if n==0 %error - could not find a blob
    blob = [1 1];
    fprintf('\n%s: Could not find pupil for frame %d.\n', mfilename, frame);
    error_frame = true;
  else
    n = length(b);
    areas = zeros(n,1);
    for k=1:n
      v = uint8(l==k);
      v = v.*window;
      areas(k) = sum(v(:)==1);
    end
    [~, idx] = max(areas);
    blob = b{idx};
  end

  %resize window to new blob
  new_window = zeros(size(im));
  x = blob(:,1);
  y = blob(:,2);
  new_window(min(x):max(x), min(y):max(y)) = 1;
  new_window = uint8(new_window);

  new_center = mean(blob); %move center to new blob

  stay = false;
  new_window_area = sum(sum(new_window));
  jump = sqrt(sum((new_center - center).^2));

  %resist disappearing windows
  if new_window_area < a.params.min_area
    stay = true;
    fprintf('\n%s: Small window (%d px)\n', mfilename, new_window_area);
  end

  %resist sudden position changes
  if jump > a.params.max_jump
    stay = true;
    fprintf('\n%s: Large jump (%0.0f px)\n', mfilename, jump);
  end

  if stay
    new_window = window;
  end

  %fit ellipse
  x = blob(:,1);
  y = blob(:,2);
  ellipse = fit_ellipse(x,y);
  if not(isempty(ellipse)) & ellipse.long_axis < max(size(im))
    ellipse_width = ellipse.long_axis;
    %correct ellipse for crop to outer roi
    ellipse.X0 = ellipse.X0 + a.params.outer_roi(2);
    ellipse.Y0 = ellipse.Y0 + a.params.outer_roi(1);
    ellipse.X0_in = ellipse.X0_in + a.params.outer_roi(2);
    ellipse.Y0_in = ellipse.Y0_in + a.params.outer_roi(1);
  else
    ellipse_width = 0;
    error_frame = true;
  end
  
  blob_width = max(max(x)-min(x), max(y)-min(y)); %find blob width

  %correct window for crop to outer roi
  new_roi = mask2rect(new_window);
  new_roi(1) = floor(new_roi(1) + a.params.outer_roi(1));
  new_roi(2) = floor(new_roi(2) + a.params.outer_roi(2));

  %correct blob for crop to outer roi
  im_blob = blob;
  im_blob(:,1) = floor(im_blob(:,1) + a.params.outer_roi(2));
  im_blob(:,2) = floor(im_blob(:,2) + a.params.outer_roi(1));

  %save in temporary results structure
  a.results.im = original_im;
  a.results.big = big;
  a.results.blob = blob;
  a.results.im_blob = im_blob;
  a.results.b_crop = b_crop;
  a.results.l_crop = l_crop;
  a.results.b = b;
  a.results.l = l;
  a.results.ellipse = ellipse;

  %save in permanent results structure
  global RESULTS
  RESULTS.(a.str).roi(frame,:) = new_roi;
  RESULTS.(a.str).blob_width(frame) = blob_width;
  RESULTS.(a.str).ellipse_width(frame) = ellipse_width;
  RESULTS.(a.str).errors(frame) = error_frame;

end

% ------------------------------------------------------------------------
% Plot methods

function plot_frame(im, frame, ax, a)

  %optimized for display speed, not prettiness

  global RESULTS

  %convert rois to boundary pixel positions
  inner_roi = rect2mask(RESULTS.(a.str).roi(frame,:), size(im));
  u = bwboundaries(inner_roi);
  outer_roi = rect2mask(a.params.outer_roi, size(im)); 
  v = bwboundaries(outer_roi);
  w = [u{1}; v{1}];

  %reserve the five lowest values of the colormap for pseudocolors
  pseudo = ([0 0 0;    %black: background
             1 1 1;    %white: blob interiors
             1 1 0;    %yellow: window boundaries
	     0 0 1;    %blue: all blob boundaries
	     1 0 0;]); %red: big blob boundary
  %use the next 251 values for grays
  grayscale = repmat(0:(1/250):1, 3, 1)';
  
  %unpack the results structure
  b = a.results.b;
  l = a.results.l;
  b_crop = a.results.b_crop;
  l_crop = a.results.l_crop;
  big = a.results.big;
  blob = a.results.blob;
  im_blob = a.results.im_blob;
  
  %show blobs detected in cropped image
  c = l_crop; %pixel intensity map
  c(c>=1) = 2; 
  for k=1:length(b_crop)
    c(sub2ind(size(c), b_crop{k}(:,1), b_crop{k}(:,2))) = 4;
  end
  if sum(blob) > 2
    c(sub2ind(size(c), big(:,1), big(:,2))) = 5;
  end
  update_ax(ax.left, c)

  %show blobs detected in uncropped image
  c = l;
  c(c>=1) = 2; 
  for k=1:length(b)
    c(sub2ind(size(c), b{k}(:,1), b{k}(:,2))) = 4;
  end
  if sum(blob) > 2
    c(sub2ind(size(c), blob(:,1), blob(:,2))) = 5;
  end
  update_ax(ax.center, c)

  %show identified pupil
  c = im;
  c(c<5)=6; %compress the five darkest intensities to allow pseudocoloring
  c(sub2ind(size(c), w(:,1), w(:,2))) = 2;
  if sum(im_blob) > 2
    c(sub2ind(size(c), im_blob(:,1), im_blob(:,2))) = 4;
  end
  update_ax(ax.right, c) 

  %show frame
  c = im;
  c(c<5)=6; %compress the five darkest intensities to allow pseudocoloring
  c(sub2ind(size(c), w(:,1), w(:,2))) = 2;
  update_ax(ax.im_ax, c);

  drawnow
  colormap([pseudo; grayscale]);

end

function plot_trace(ax, a)

  global RESULTS

  ew = RESULTS.(a.str).ellipse_width;
  fstop =  max(find(~isnan(ew)));
  ew = ew(1:fstop);

  p = get(ax.time_ax, 'children');
  if isempty(p)
    axes(ax.time_ax)
    plot(ew, 'r')
    legend('Ellipse Width')
    xlabel('Frame')
    ylabel('Pupil Measurement (pixels)')
    legend boxoff
    box off
    drawnow
  else
    set(p(1), 'xdata', 1:fstop, 'ydata', ew); %faster than replotting
  end

  drawnow

  %update frame display once every 30 frames
  if mod(length(ew), 30) == 0
    a.plot_frame(a.results.im, length(ew), ax, a)
  end

end

function plot_replay(im, frame, fig, a)

  a = a.do_frame(im, frame, a); %analyze frame
  e = a.results.ellipse;

  figure(fig)
  hold on
  imshow(im)
  if ~isempty(e)
    draw_ellipse(e.b, e.a, e.phi, e.Y0_in, e.X0_in, 'r');
  else
    x = size(frame,2)/2;
    y = size(frame,1)/2;
    text(x,y, 'FAIL')
  end

  hold off

end

% ------------------------------------------------------------------------
% Helper functions

function mask = rect2mask(rect, dim)
  mask = uint8(zeros(dim));
  x = ceil((1:rect(4)) + rect(2) - 1);
  y = ceil((1:rect(3)) + rect(1) - 1);
  mask(x,y) = 1;
end

function rect = mask2rect(mask)
  x_coord = min(find(sum(mask,1)));
  y_coord = min(find(sum(mask,2)));
  x_width = max(find(sum(mask,1))) - x_coord + 1;
  y_width = max(find(sum(mask,2))) - y_coord + 1; 
  rect = [x_coord y_coord x_width y_width];
end

function update_ax(ax, c)
  h = get(ax, 'children');
  set(ax, 'nextplot', 'replace')
  if isempty(h) | length(h) > 1
    axes(ax)
    h = imshow(c);
    set(h, 'cdatamapping', 'direct')
  else
    set(h, 'cdata', c, 'cdatamapping', 'direct')
  end
end
