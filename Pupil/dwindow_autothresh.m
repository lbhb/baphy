function a = dwindow_autothresh()
%
%a = dwindow_autothresh()
%
%Tweak to dynamic window module. Automatically sets threshold for each frame to
%the first valley in the frame's intensity histogram.
%
%Inputs (params fields)
% im: Grayscale image
% inner_roi: Rectangular crop window for first frame
%  Format: [x_coord y_coord x_width y_width]
%   x_coord, y_coord: Coordinates of upper left-hand point of rectangle
%   x_width, y_width: Dimensions of rectangle
% high: Maximum upper threshold
% pupil_center: Center of pupil on each frame
% eye_speed: Distance (in px) between pupil center on each frame
%
%Returns (results fields)
% blob_width: Maximum width of blob
% minor_axis: Minor axis of ellipse
% major_axis: Major axis of ellipse
% roi: Rectangular crop window, [x_coord y_coord x_width y_width] format
% ellipse: All other ellipse parameters (see fit_ellipse.m for format)
% high: Upper threshold for each frame.
%
%Plot format
% left: Blobs found in cropped image, pupil outlined in red, non-pupil in blue
% center: Blobs found in full image, pupil outlined in red, non-pupil in blue
% right: Pupil blob overplotted on frame
%
%ZPS 2017-07-11: Initial version.
%ZPS 2017-08-04: Fix bug that caused a crash when histogram only contained a
%single peak.
%ZPS 2018-09-14: Update to analyze eye movements.

a = [];
a.params = []; 
a.results = [];
a.str = 'dwindow_autothresh';
a.malloc = @malloc;
a.do_frame = @do_frame;
a.plot_frame = @plot_frame;
a.plot_trace = @plot_trace;
a.plot_replay = @plot_replay;
a.default_params = @default_params;

end

function params = default_params
	params.low = 0;
	params.high = 30;
	params.smooth = 3;
	params.smooth_hist = 3;
	params.max_jump = 120;
	params.min_area = 100;
	params.analyze_eye_movements = 0;
	params.display_eye_movements = 0;
end
% ------------------------------------------------------------------------
% Analysis methods

function malloc(frames, a)

  global RESULTS

  RESULTS.(a.str).roi = nan(frames,4); %region of interest
  RESULTS.(a.str).blob_width = nan(frames,1);
  RESULTS.(a.str).minor_axis = nan(frames,1);
  RESULTS.(a.str).major_axis = nan(frames,1);
  RESULTS.(a.str).high = nan(frames,1);

  RESULTS.(a.str).errors = nan(frames,1);

  RESULTS.(a.str).ellipse(1:frames) = ...
    struct('a',[],...
           'b',[],...
           'phi',[],...
           'X0',[],...
           'Y0',[],...
           'X0_in',[],...
           'Y0_in',[],...
           'long_axis',[],...
           'short_axis',[],...
           'status','');

	RESULTS.(a.str).eye_speed = nan(frames,1);
	RESULTS.(a.str).pupil_center = nan(frames,2);

end

function a = do_frame(im, frame, a)

  error_frame = false;

  original_im = im;
  
  im = imcrop(im, a.params.outer_roi); %crop to outer roi

  %apply gaussian smoothing to remove noise
  im = uint8(gsmooth(double(im),[a.params.smooth a.params.smooth]));

  %set upper threshold to the first valley in the histogram
  h = hist(double(im(:)), 0:255);
  h = smooth(h, a.params.smooth_hist);
  [pks, locs] = findpeaks(h);
  if numel(locs) == 1 %histogram is so smooth that there are no valleys
    high = a.params.high; %use default value
  else %set upper threshold to the first valley
    valley = h(locs(1):locs(2));
    valley_minima = locs(1) + find(valley == min(valley), 1) - 1;
    high = min(valley_minima, a.params.high);
  end

  %threshold
  thresh = uint8(ones(size(im)));
  thresh(im>high) = 0;
  %thresh(im<a.params.low) = 0;

  %set current window
  if frame == 1
    roi = a.params.inner_roi;
  else
    global RESULTS
    roi = RESULTS.(a.str).roi(frame-1,:);
  end
  if isnan(roi)
    fprintf('\n%s: No ROI data for frame %d.\n', mfilename, frame);
    return
  end
  
  % svd expand out search window
  roi(1:2)=roi(1:2)-5;
  roi(3:4)=roi(3:4)+10;

  %correct for crop to outer roi
  roi(1) = ceil(roi(1) - a.params.outer_roi(1));
  roi(2) = ceil(roi(2) - a.params.outer_roi(2));
  roi(1) = max(roi(1), 3);
  roi(2) = max(roi(2), 3);
  roi(3) = min(roi(3), size(im,2)-roi(1)-2);
  roi(4) = min(roi(4), size(im,1)-roi(2)-2);

  %roi
  window = rect2mask(roi, size(im));
  center = [roi(1)+roi(3)/2 roi(2)+roi(4)/2];
  %size(thresh)
  %size(window)
  crop = thresh.*window; %crop to dynamic roi

  %find the biggest blob in the cropped image
  [b_crop, l_crop] = bwboundaries(crop, 'noholes'); %moore-neighbor tracing
  n = length(b_crop);
  if n==0 %error - could not find a blob
    big = [1 1];
    fprintf('%s: Could not find blob for frame %d.\n', mfilename, frame);
    error_frame = true;
  else
    areas = zeros(n,1);
    for k=1:n
      areas(k) = sum(l_crop(:)==k);
    end
    [~, idx] = max(areas);
    big = b_crop{idx};
  end
  u = mean(big); %centroid

  %find the blob with the most pixels in the window
  [b, l] = bwboundaries(thresh, 'noholes');
  if n==0 %error - could not find a blob
    blob = [1 1];
    fprintf('\n%s: Could not find pupil for frame %d.\n', mfilename, frame);
    error_frame = true;
  else
    n = length(b);
    areas = zeros(n,1);
    for k=1:n
      v = uint8(l==k);
      v = v.*window;
      areas(k) = sum(v(:)==1);
    end
    [~, idx] = max(areas);
    blob = b{idx};
  end

  %resize window to new blob
  new_window = zeros(size(im));
  x = blob(:,1);
  y = blob(:,2);
  new_window(min(x):max(x), min(y):max(y)) = 1;
  new_window = uint8(new_window);

  new_center = mean(blob); %move center to new blob

  stay = false;
  new_window_area = sum(sum(new_window));
  jump = sqrt(sum((new_center - center).^2));

  %resist disappearing windows
  if new_window_area < a.params.min_area
    stay = true;
    fprintf('\n%s: Small window (%d px)\n', mfilename, new_window_area);
  end

  %resist sudden position changes
  if jump > a.params.max_jump
    stay = true;
    fprintf('\n%s: Large jump (%0.0f px)\n', mfilename, jump);
  end

  if stay
    new_window = window;
  end

  %fit ellipse
  x = blob(:,1);
  y = blob(:,2);
  ellipse = fit_ellipse(x,y);

  if not(isempty(ellipse)) & ellipse.long_axis < max(size(im))

    %correct ellipse for crop to outer roi
    ellipse.X0 = ellipse.X0 + a.params.outer_roi(2);
    ellipse.Y0 = ellipse.Y0 + a.params.outer_roi(1);
    ellipse.X0_in = ellipse.X0_in + a.params.outer_roi(2);
    ellipse.Y0_in = ellipse.Y0_in + a.params.outer_roi(1);

  else %could not fit ellipse

    error_frame = true;

    ellipse.a = 0;
    ellipse.b = 0;
    ellipse.phi = 0;
    ellipse.X0 = 0;
    ellipse.Y0 = 0;
    ellipse.X0_in = 0;
    ellipse.Y0_in = 0;
    ellipse.long_axis = 0;
    ellipse.short_axis = 0;
    ellipse.status = '';

  end

  minor_axis = ellipse.short_axis;
  major_axis = ellipse.long_axis;
  blob_width = max(max(x)-min(x), max(y)-min(y));

  %correct window for crop to outer roi
  new_roi = mask2rect(new_window);
  new_roi(1) = floor(new_roi(1) + a.params.outer_roi(1));
  new_roi(2) = floor(new_roi(2) + a.params.outer_roi(2));

  %correct blob for crop to outer roi
  im_blob = blob;
  im_blob(:,1) = floor(im_blob(:,1) + a.params.outer_roi(2));
  im_blob(:,2) = floor(im_blob(:,2) + a.params.outer_roi(1));

  %save in temporary results structure
  a.results.im = original_im;
  a.results.big = big;
  a.results.blob = blob;
  a.results.im_blob = im_blob;
  a.results.b_crop = b_crop;
  a.results.l_crop = l_crop;
  a.results.b = b;
  a.results.l = l;
  a.results.ellipse = ellipse;
  a.results.h = h;
  a.results.high = high;

  %save in permanent results structure
  global RESULTS
  RESULTS.(a.str).roi(frame,:) = new_roi;
  RESULTS.(a.str).blob_width(frame) = blob_width;
  RESULTS.(a.str).major_axis(frame) = major_axis;
  RESULTS.(a.str).minor_axis(frame) = minor_axis;
  RESULTS.(a.str).ellipse(frame) = ellipse;
  RESULTS.(a.str).high(frame) = high;

  RESULTS.(a.str).errors(frame) = error_frame;

	if a.params.analyze_eye_movements
		a = do_eye_movements(a);
	end

end

% ------------------------------------------------------------------------
% Plot methods

function plot_frame(im, frame, ax, a)

  global RESULTS STOPPED

  %unpack the results structure
  b = a.results.b;
  l = a.results.l;
  b_crop = a.results.b_crop;
  l_crop = a.results.l_crop;
  big = a.results.big;
  blob = a.results.blob;
  im_blob = a.results.im_blob;

  %convert rois to boundary pixel positions
  inner_roi = rect2mask(RESULTS.(a.str).roi(frame,:), size(im));
  u = bwboundaries(inner_roi);
  outer_roi = rect2mask(a.params.outer_roi, size(im)); 
  v = bwboundaries(outer_roi);
  w = [u{1}; v{1}];
  w=w(w(:,1)<=size(im,1) & w(:,2)<=size(im,2),:);
  
  %reserve the five lowest values of the colormap for pseudocolors
  pseudo = ([0 0 0;    %black: background
             1 1 1;    %white: blob interiors
             1 1 0;    %yellow: window boundaries
             0 0 1;    %blue: all blob boundaries
             1 0 0;]); %red: big blob boundary
  %use the next 251 values for grays
  grayscale = repmat(0:(1/250):1, 3, 1)';

  %show blobs detected in cropped image
  if STOPPED
    axes(ax.left)
    imshow(l_crop)
    hold on
    for k=1:length(b_crop)
      plot(b_crop{k}(:,2), b_crop{k}(:,1), 'b')
    end
    if sum(big) > 0
      plot(big(:,2), big(:,1), 'r')
    end
    hold off
  else
    c = l_crop; %pixel intensity map
    c(c>=1) = 2; 
    for k=1:length(b_crop)
      c(sub2ind(size(c), b_crop{k}(:,1), b_crop{k}(:,2))) = 4;
    end
    if sum(blob) > 2
      c(sub2ind(size(c), big(:,1), big(:,2))) = 5;
    end
    update_ax(ax.left, c)
  end

  %show blobs detected in uncropped image
  if STOPPED
    axes(ax.center)
    imshow(l)
    hold on
    for k=1:length(b)
      plot(b{k}(:,2), b{k}(:,1), 'b')
    end
    if sum(big) > 0
      plot(blob(:,2), blob(:,1), 'r')
    end
    hold off
  else
    c = l;
    c(c>=1) = 2; 
    for k=1:length(b)
      c(sub2ind(size(c), b{k}(:,1), b{k}(:,2))) = 4;
    end
    if sum(blob) > 2
      c(sub2ind(size(c), blob(:,1), blob(:,2))) = 5;
    end
    update_ax(ax.center, c)
  end

  %show identified pupil
  if STOPPED
    axes(ax.right)
    el = a.results.ellipse;
    xc = blob(:,1) + a.params.outer_roi(2);
    yc = blob(:,2) + a.params.outer_roi(1);
    d = sprintf('%0.f, %0.f', el.short_axis, el.long_axis);
    imshow(im)
    hold on
    draw_ellipse(el.b, el.a, el.phi, el.Y0_in, el.X0_in, 'r');
    rectangle('position', a.params.outer_roi, ...
      'linewidth', 1, 'edgecolor', 'y');
    rectangle('position', RESULTS.(a.str).roi(frame,:), ...
      'linewidth', 1, 'edgecolor', 'y');
    text(mean(yc), mean(xc), d, 'color', 'g');
    hold off
  else
    c = im;
    c(c<5)=6; %compress the five darkest intensities to allow pseudocoloring
    if ~isempty(w)
        c(sub2ind(size(c), w(:,1), w(:,2))) = 2;
    end
    if sum(im_blob) > 2
       im_blob = im_blob(im_blob(:,1)<=size(c,1) & im_blob(:,2)<=size(c,2), :);
       c(sub2ind(size(c), im_blob(:,1), im_blob(:,2))) = 4;
    end
    update_ax(ax.right, c) 
  end

  %show frame
  if STOPPED
    axes(ax.im_ax)
    imshow(im)
    rectangle('position', RESULTS.(a.str).roi(frame,:), ...
      'linewidth', 1, 'edgecolor', 'y');
    rectangle('position', a.params.outer_roi, ...
      'linewidth', 1, 'edgecolor', 'y');
    if isfield(RESULTS, 'eye_line')
      hold on
      l = RESULTS.eye_line;
      plot(l(:,1), l(:,2), 'y')
      hold off
    end
  else
    c = im;
    c(c<5)=6; %compress the five darkest intensities to allow pseudocoloring
    c(sub2ind(size(c), w(:,1), w(:,2))) = 2;
    update_ax(ax.im_ax, c);
  end

  if ~STOPPED
    if verLessThan('Matlab', '9.1')
      colormap([pseudo; grayscale])
    else
      colormap(ax.im_ax, [pseudo; grayscale]);
      colormap(ax.left, [pseudo; grayscale]);
      colormap(ax.center, [pseudo; grayscale]);
      colormap(ax.right, [pseudo; grayscale]);
    end
  end

	if STOPPED & a.params.display_eye_movements
		pupil_center = RESULTS.(a.str).pupil_center;
		ellipse = RESULTS.(a.str).ellipse;
		axes(ax.right)
		imshow(im)
		hold on
		f = a.params.frame;
    draw_ellipse(ellipse(f).b, ellipse(f).a, ellipse(f).phi, ...
			ellipse(f).Y0_in, ellipse(f).X0_in, 'r');
    draw_ellipse(ellipse(f-1).b, ellipse(f-1).a, ellipse(f-1).phi, ...
			ellipse(f-1).Y0_in, ellipse(f-1).X0_in, 'r');
		plot(pupil_center(2,(f-1):f), pupil_center(1,(f-1):f), ...
			'g.-', 'linewidth', 1)
	end

  if STOPPED
    h = a.results.h;
    axes(ax.hist_ax)
    cla
    lw = 2;
    hold on
    plot(h, 'r', 'linewidth', lw);
    plot(a.results.high, h(a.results.high), 'g.', 'markersize', 20)
    hold off
  end

  drawnow

end

function plot_trace(ax, a)

  global PARAMS RESULTS STOPPED

  frame = a.params.frame;

  minor_axis = RESULTS.(a.str).minor_axis;
  fstop = max(find(~isnan(minor_axis)));
  minor_axis = minor_axis(1:fstop);
  major_axis = RESULTS.(a.str).major_axis(1:fstop);
  high = RESULTS.(a.str).high(1:fstop);

  %if isfield(RESULTS, 'eye_width') & isfield(PARAMS, 'eye_width_mm')
  %  px_to_mm = PARAMS.eye_width_mm/RESULTS.eye_width;
  %  minor_axis = minor_axis.*px_to_mm;
  %  major_axis = major_axis.*px_to_mm;
  %  ylabel_str = 'Pupil Measurement (mm)';
  %else
  %  ylabel_str = 'Pupil Measurement (px)';
  %end
  %
  ylabel_str = 'Pupil Measurement (px)';

  p = get(ax.time_ax, 'children');
  if isempty(p)

    axes(ax.time_ax)
    plot(minor_axis, 'r')
    hold on
    plot(major_axis, 'b')
    plot(high, 'g')
    legend({'Minor Axis', 'Major Axis', 'Threshold'})
    xlabel('Frame')
    ylabel(ylabel_str)
    legend boxoff
    box off
    drawnow
    hold off

  elseif mod(length(minor_axis), 30) == 0

    set(p(3), 'xdata', 1:fstop, 'ydata', minor_axis);
    set(p(2), 'xdata', 1:fstop, 'ydata', major_axis);
    set(p(1), 'xdata', 1:fstop, 'ydata', high);

    a.plot_frame(a.results.im, fstop, ax, a)

  end

	if a.params.display_eye_movements & STOPPED

		eye_speed = RESULTS.(a.str).eye_speed(1:fstop);

		eye_speed = (eye_speed-min(eye_speed))./(max(eye_speed)-min(eye_speed));
		minor_axis = (minor_axis-min(minor_axis))./(max(minor_axis)-min(minor_axis));
		major_axis = (major_axis-min(major_axis))./(max(major_axis)-min(major_axis));

		axes(ax.time_ax)
		plot(minor_axis, 'r')
		hold on
		plot(major_axis, 'b')
		plot(eye_speed, 'k')
		xlabel('Frame')
		ylabel('Pupil or eye movement (normalized)')
		legend({'Minor Axis', 'Major Axis', 'Eye Speed'})
		legend boxoff
		box off
		drawnow
		hold off

	end

  if STOPPED

    axes(ax.time_ax)
    axis tight

    if not(a.params.display_eye_movements)
			display_var = PARAMS.default_var;
		else
			display_var = 'eye_speed';
		end

    axes(ax.time_ax_local)
    region = 60;
    rstart = max(1, frame-region);
    rstop = min(frame+region, fstop);
    rcenter = min(frame, fstop);
    if isfield(RESULTS.(a.str), display_var)
      local_data = RESULTS.(a.str).(display_var);
      plot(rstart:rstop, local_data(rstart:rstop), 'k')
      hold on
      plot(rcenter, local_data(rcenter), 'r.', 'markersize', 15)
      l = legend(display_var);
      set(l, 'interpreter', 'none')
      legend boxoff
      xlabel('Frame')
      ylabel(ylabel_str)
      box off
      hold off
      axis tight
      drawnow
    else
      fprintf('%s: No data on %s. Change default variable.\n', ...
				mfilename, PARAMS.default_var)
    end

  end

end

function plot_replay(im, frame, fig, a)

  a = a.do_frame(im, frame, a); %analyze frame
  e = a.results.ellipse;

  figure(fig)
  hold on
  imshow(im)
  if ~isempty(e)
    draw_ellipse(e.b, e.a, e.phi, e.Y0_in, e.X0_in, 'r');
  else
    x = size(frame,2)/2;
    y = size(frame,1)/2;
    text(x,y, 'FAIL')
  end

  hold off

end

% ------------------------------------------------------------------------
% Eye movement analysis

function a = do_eye_movements(a)

	global RESULTS

	ellipse = RESULTS.(a.str).ellipse;

  for k=1:size(ellipse,2)
    if isempty(ellipse(k).X0_in)
      ellipse(k).X0_in = nan;
    end
    if isempty(ellipse(k).Y0_in)
      ellipse(k).Y0_in = nan;
    end
  end

  x = [ellipse.X0_in];    
  y = [ellipse.Y0_in];      
  eye_speed = sqrt( (diff(x).^2) + (diff(y).^2) );
	eye_speed = [0; eye_speed'];

	if not(isfield(RESULTS.(a.str), 'eye_speed'))
		frames = length(ellipse);
		RESULTS.(a.str).eye_speed = nan(frames,1);
		RESULTS.(a.str).pupil_center = nan(frames,2);
	end

	RESULTS.(a.str).eye_speed = eye_speed;
	RESULTS.(a.str).pupil_center = [x; y];

end

% ------------------------------------------------------------------------
% Helper functions

function mask = rect2mask(rect, dim)
  mask = uint8(zeros(dim));
  x = ceil((1:rect(4)) + rect(2) - 1);
  y = ceil((1:rect(3)) + rect(1) - 1);
  mask(x,y) = 1;
end

function rect = mask2rect(mask)
  x_coord = min(find(sum(mask,1)));
  y_coord = min(find(sum(mask,2)));
  x_width = max(find(sum(mask,1))) - x_coord + 1;
  y_width = max(find(sum(mask,2))) - y_coord + 1; 
  rect = [x_coord y_coord x_width y_width];
end

function update_ax(ax, c)
  h = get(ax, 'children');
  set(ax, 'nextplot', 'replace')
  if isempty(h) | length(h) > 1
    axes(ax)
    h = imshow(c);
    set(h, 'cdatamapping', 'direct')
  else
    set(h, 'cdata', c, 'cdatamapping', 'direct')
  end
end
