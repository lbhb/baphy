function lick_start_recording(hObject, handles,varargin)
global MG

if nargin==3 && varargin{1}==1
    MG.do_trial_start=1;
    MG.trial_start_timestamp=now;
    return
end
isFreehand=isfield(MG,'roiMask');

% Determine format (pupil or photometry or lick)
if isfield(MG, 'format')
    format = MG.format;
else
    format = 'pupil';
end

f = get(handles.video_file, 'String');
[path,name,ext] = fileparts(f);

if isfield(MG, 'video')
    close(MG.video)
end

%Create a new video object
if strcmp(format, 'photometry')
    MG.video = VideoWriter(f, 'Grayscale AVI');
elseif strcmp(format, 'pupil_dual')
    if strcmpi(ext,'.avi')
        MG.video = VideoWriter(f);
        f2=strrep(f,'.avi','_2.avi');
        MG.video2 = VideoWriter(f2);
    else
        error('fix me')
    end
elseif strcmpi(ext,'.avi')
    MG.video = VideoWriter(f);
elseif strcmpi(ext,'.mj2')
    MG.video = VideoWriter(f, 'Archival');
else
    error(['unknown extension for lick video format: ',ext]);
end
guidata(hObject, handles);

[fid,msg]=fopen([path,filesep,name,'_timing_degug.csv'],'w+');
if fid==-1
    error('Error opening timing debug file: %s',msg)
end
MG.run_preview=0;
%Start recording
MG.recording=1;
MG.frames_written=0;
open(MG.video);
if MG.mode==2
    stoppreview(MG.cam)
    stop(MG.cam)
    triggerconfig(MG.cam, 'manual');
    MG.cam.TriggerRepeat = inf;
    MG.cam.FramesPerTrigger = 1;
    start(MG.cam)
    trigger(MG.cam);
    [MG.im,ts] = getdata(MG.cam);
elseif MG.mode==1
    MG.im = step(MG.cam);
else
    MG.im = getsnapshot(MG.cam);
end
if isFreehand
    Lickframe0.cdata = MG.im(MG.roiMask);
else
    Lickframe0.cdata = mean(imcrop(MG.im, MG.roi),3);
end
Lickframe0.colormap = [];
MG.trial_frame=0;
MG.trial_start_timestamp=now;
lick_trace=nan(1e6,1);
MG.t=nan(1e6,1);
MG.timestamp_debug = nan(9,1e6);%now;
MG.getdata_timestamp = nan(1,1e6);%now;
MG.timestamp=now;
MG.lick_times=nan(1e2,1);
MG.lti=0;
MG.do_trial_start=1;
MG.lights_out=0;
MG.lick=false;
MG.trial=0;
MG.do_pause=0;
do_stop=1;
while MG.recording
    %     if MG.do_pause
    %         drawnow;
    %         pause(.5);
    %         MG.do_pause=0;
    %     end
    %%
    MG.trial_frame = MG.trial_frame+1;
    MG.timestamp_debug(1,MG.trial_frame) = now;
    if  0 && MG.trial_frame>10 && all(diff(MG.timestamp_debug(1:2,MG.trial_frame+[-4:-1]))*24*60*60>.07)
        tic;
        profile on;
        toc;
        tic;
        if MG.mode==2
            trigger(MG.cam);
            [MG.im,ts] = getdata(MG.cam);
            MG.getdata_timestamp(MG.trial_frame)=ts;
        elseif MG.mode
            MG.im = step(MG.cam);
        else
            MG.im = getsnapshot(MG.cam);
        end
        toc;
        profile viewer;
        toc;
        aa=2;
    elseif MG.do_pause
        N=1e2;
        tic;
        t3=nan(4,N);
        for i=1:N
            t3(1,i)=toc;
            trigger(MG.cam);
            t3(2,i)=toc;
            [MG.im,ts] = getdata(MG.cam);
            t3(3,i)=toc;
            t3(4,i)=ts;
        end
        keyboard
    else
        if MG.mode==2
            trigger(MG.cam);
            MG.timestamp_debug(2,MG.trial_frame) = now;
            try
                [MG.im,ts] = getdata(MG.cam);
            catch err
                try
                    fprintf('Couldn''t get data from camera, trying again\n')
                    [MG.im,ts] = getdata(MG.cam);
                catch err2
                    keyboard
                end
                keyboard
            end
            MG.timestamp_debug(3,MG.trial_frame) = now;
            MG.getdata_timestamp(MG.trial_frame)=ts;
            %flushdata(MG.cam)
        elseif MG.mode
            MG.im = step(MG.cam);
        else
            MG.im = getsnapshot(MG.cam);
        end
    end
    MG.timestamp_debug(4,MG.trial_frame) = now;
    if MG.mode==1 || MG.mode==2
        set(MG.himage, 'CData', MG.im);
    end
    MG.timestamp_debug(5,MG.trial_frame) = now;
    %fprintf('\nDiff:%0.0f ms ',diff(MG.timestamp_debug([1 5],MG.trial_frame))*24*60*60*1000)
    if MG.do_trial_start
        ss=repmat('%6.12e, ',1,11);ss(end-1:end)=[];
        dat=[repmat(MG.trial,1,MG.trial_frame,1);...
            MG.timestamp_debug(1,1:MG.trial_frame);...
            MG.timestamp_debug(:,1:MG.trial_frame)-MG.timestamp_debug(1,1);...
            MG.getdata_timestamp(1:MG.trial_frame)];
        fprintf(fid,['\n%d, ',ss],dat);
        
        ts_db=MG.timestamp_debug(1:2,MG.trial_frame);
        MG.timestamp_debug(:)=nan;
        ts_gd=MG.getdata_timestamp(MG.trial_frame);
        MG.getdata_timestamp(:)=nan;
        MG.getdata_timestamp(1)=ts_gd;
        MG.timestamp_debug(1:2,1)=ts_db;
        MG.trial=MG.trial+1;
        
        set(handles.lick_trace,'YData',lick_trace)
        MG.trial_frame=1;
        lick_trace(:)=nan;
        MG.t(:)=nan;
        MG.lti=0;
        MG.lick_times(:)=nan;
        MG.lick_inds(:)=nan;
        MG.do_trial_start=0;
        reset_baseline()
    end
    if MG.do_reset_baseline
        fprintf('\n***********MANUALLY RESETTING BASELINE******')
        reset_baseline()
        MG.do_reset_baseline=false;
    end
    if 1
        frame.cdata = MG.im;
        frame.colormap = [];
        if MG.resize_factor~=1
            % MG.resize_factor is the factor by which to resize pupil video.
            % Defined in puil_online.m.In new MATLAB,videos can only be
            % recorded at 720x480, we resize to save disk space.
            frame.cdata = imresize(MG.im,MG.resize_factor);
        end
        %Save video
        if strcmp(format, 'photometry')
            frame.cdata = mean(frame.cdata, 3);
            frame.cdata = frame.cdata / 2^16;
        end
        
        writeVideo(MG.video, frame);
    end
    %Save a timestamp
    %MG.timestamp_diff=(tsnow-MG.timestamp)*24*60*60; %in seconds
    MG.timestamp = now;
    MG.timestamp_debug(6,MG.trial_frame)=MG.timestamp;
    
    MG.frames_written=MG.frames_written+1;
    if get(MG.Stim.TCPIP,'BytesAvailable'),
        M_Pupil_TCPIP(MG.Stim.TCPIP);
    end
    
    
    
    MG.t(MG.trial_frame)=(MG.timestamp-MG.trial_start_timestamp)*24*60*60;
    if 1
        Lickframe.colormap = [];
        MG.timestamp_debug(7,MG.trial_frame) = now;
        if isFreehand
            Lickframe.cdata = MG.im(MG.roiMask);
        else
            Lickframe.cdata = mean(imcrop(MG.im, MG.roi),3);
        end
        
        MAV=mean(abs(Lickframe.cdata(:)-Lickframe0.cdata(:)));
        %fprintf(['\nMAV: ' num2str(MAV)])
        
        lick_trace(MG.trial_frame)=MAV;
        
        MG.timestamp_debug(8,MG.trial_frame) = now;
        %MG.lick_trace_dig=MG.lick_trace>MG.lick_threshold;
        
        if max(Lickframe.cdata(:)) < 0.5*max(Lickframe0.cdata(:))
            lick_trace(MG.trial_frame)=0;
            if MG.lights_out==0
                fprintf('\nLIGHTS OUT')
                MG.lights_out=1;
            end
        else
            MG.lights_out=0;
        end
        if (~MG.lights_out && lick_trace(MG.trial_frame)>MG.lick_threshold) || MG.manual_lick
            if MG.manual_lick
                MG.manual_lick=false;
                fprintf('\n***** MANUAL LICK at %1.2f sec******',MG.t(MG.trial_frame))
            end
            MG.lick=true;
            M_sendMessage('LICK|data_here')
            MG.lti=MG.lti+1;
            MG.lick_times(MG.lti)=MG.t(MG.trial_frame);
            MG.lick_inds(MG.lti)=MG.trial_frame;
            MG.lights_out=0;
        else
            MG.lick=false;
        end
        if mod(MG.trial_frame-1,200)==0
            %set(handles.lick_trace,'YData',lick_trace)
            %drawnow;
        end
        if ~MG.lights_out && MG.trial_frame>20 && all(lick_trace(MG.trial_frame-19:MG.trial_frame)>MG.lick_threshold)
            %Stuck high, reset baseline
            fprintf('\n***********STUCK HIGH, RESETTING BASELINE******')
            reset_baseline
        end
    end
    MG.timestamp_debug(9,MG.trial_frame) = now;
    %     if MG.trial_frame==100
    %         profile viewer
    %         aa=2
    %     end
end
close(MG.video);
fclose(fid);
aa=2;
if MG.mode==2
    stop(MG.cam)
    hi=preview(MG.cam,MG.himage);
elseif MG.mode==1
    MG.run_preview=1;
    while MG.run_preview
        if MG.mode==1
            img = step(MG.cam);
            set(MG.himage, 'CData', img);
        elseif MG.mode==2
            trigger(MG.cam);
            [img,ts] = getdata(MG.cam);
            set(MG.himage, 'CData', img);
        end
    end
end

    function reset_baseline
        if isFreehand
            Lickframe0.cdata = MG.im(MG.roiMask);
        else
            Lickframe0.cdata = mean(imcrop(MG.im, MG.roi),3);
        end
        
    end

end