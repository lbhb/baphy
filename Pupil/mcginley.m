function a = mcginley()
%
%a = mcginley()
%
%Pupil analysis module based on Matt McGinley's heuristic method:
% (1) Threshold image. Threshold boundaries are set by user.
% (2) Find edges using Canny edge detection.
% (3) Mask out the boundary of the eye. This gives better performance
% in ferrets, which have darker eye boundaries than mice.
% (4) 
%
%Inputs
% im: Grayscale image
% high, low: Upper and lower threshold
% slop:
% dist: 
%
%Returns
% ellipse_width: Major axis of ellipse fit to pupil.
%
%Plot format
% left:
% center:
% right:
%
%References
% McGinley, MJ, David, SV, McCormick, DA (2015). Cortical membrane potential
% signature of optimal states for sensory signal detection. Neuron 87: 179-92.
%
%Created ZPS 2015-12-31

a = [];
a.params = [];
a.results = [];
a.str = 'mcginley';
a.malloc = @malloc;
a.do_frame = @do_frame;
a.plot_frame = @plot_frame;
a.plot_trace = @plot_trace;
a.plot_replay = @plot_replay;

end

% ------------------------------------------------------------------------
% Analysis methods

function malloc(frames, a)
  %allocate memory in video-wide results structure
  global RESULTS
  RESULTS.(a.str).ellipse_width = nan(frames,1);
end

function a = do_frame(im, frame, a)

  %threshold
  thresh = im+1;
  thresh(im>a.params.high) = 0;
  thresh(im<a.params.low) = 0;

  %detect edges
  canny_thresh = [0.2 0.3]; %sensitivity threshold for edge detection
  edges = edge(thresh, 'canny', canny_thresh);
  im_edges = double(edges);

  %find eye boundary
  eye_boundary = bwperim(im2bw(thresh, graythresh(thresh)));
  edges(eye_boundary==1) = 0;
  im_edges(eye_boundary==1) = 0.2;

  %mask out eye boundary
  pupil = thresh;
  im_pupil = double(pupil);
  pupil(eye_boundary==1) = 0;
  im_pupil(eye_boundary==1) = 0.2;

  %model thresholded pixels as a circle, find their center and diameter
  [pupil_y, pupil_x] = find(pupil);
  circle.x = round(mean(pupil_x));
  circle.y = round(mean(pupil_y));
  circle.a = length(pupil_y);
  circle.d = sqrt(circle.a*4/pi);
  if ~isnan(circle.x) & ~isnan(circle.y)
    im_pupil(circle.y, circle.x) = 0;
  end

  %remove edges too far from or too close to the center
  [edge_y, edge_x] = find(edges);
  distance_from_center = sqrt((edge_x-circle.x).^2 + (edge_y-circle.y).^2);
  max_dist = (1+a.params.slop)*(circle.d/2);
  min_dist = (1-a.params.slop)*(circle.d/2);
  bad_edges = distance_from_center>max_dist | distance_from_center<min_dist;
  for k = 1:length(bad_edges)
    if bad_edges(k)
      edges(edge_y(k), edge_x(k)) = 0;
      im_edges(edge_y(k), edge_x(k)) = 0.4;
    end
  end

  empty_ellipse = struct('a',[],'b',[],'phi',[],'X0',[],'Y0',[],'X0_in',[],...
    'Y0_in',[],'long_axis',0,'short_axis',0,'status','');

  if isempty(edges)
    ellipse = empty_ellipse;
  else
    [~, nearest_neighbor] = dsearchn([pupil_y,pupil_x], [edge_y,edge_x]);
    for k = 1:length(nearest_neighbor)
      if nearest_neighbor(k) > a.params.dist
	edges(edge_y(k), edge_x(k)) = 0;
	im_edges(edge_y(k), edge_x(k)) = 0.6;
      end
    end

    %fit an ellipse to the pupil
    [x, y] = find(squeeze(edges));
    ellipse = fit_ellipse(x, y);
  end

  %overwrite single-frame results structure
  a.results.circle = circle;
  a.results.ellipse = ellipse;
  a.results.im_edges = im_edges;
  a.results.im_pupil = im_pupil;
  a.results.ellipse_width = ellipse.long_axis;
  
  %save in video-wide results structure
  global RESULTS
  RESULTS.(a.str).ellipse_width(frame) = ellipse.long_axis;

end

% ------------------------------------------------------------------------
% Plot methods

function plot_frame(im, frame, ax, a)

  c = a.results.circle;
  d = a.results.ellipse_width;
  e = a.results.ellipse;

  max_dist = (1+a.params.slop)*(c.d/2);
  min_dist = (1-a.params.slop)*(c.d/2);

  axes(ax.left), cla, hold on
  imshow(a.results.im_pupil)
  draw_ellipse(min_dist, min_dist, 0, c.x, c.y, 'g');
  draw_ellipse(max_dist, max_dist, 0, c.x, c.y, 'g');
  axis tight
  hold off

  axes(ax.center), cla, hold on
  imshow(a.results.im_edges)
  draw_ellipse(min_dist, min_dist, 0, c.x, c.y, 'g');
  draw_ellipse(max_dist, max_dist, 0, c.x, c.y, 'g');
  draw_ellipse(e.b, e.a, e.phi, e.Y0_in, e.X0_in, 'r');
  axis tight
  hold off

  axes(ax.right), cla, hold on
  imshow(im)
  draw_ellipse(e.b, e.a, e.phi, e.Y0_in, e.X0_in, 'r');
  text(c.x, c.y, num2str(round(d)), 'color', 'r');
  axis tight
  hold off

end

function plot_trace(ax, a)

  global RESULTS

  ew = RESULTS.mcginley.ellipse_width;
  fstop =  max(find(~isnan(ew)));
  ew = ew(1:fstop);

  p = get(ax.time_ax, 'children');
  if isempty(p)
    axes(ax.time_ax)
    plot(ew, 'b')
    legend('Ellipse Width')
    xlabel('Frame')
    ylabel('Pupil Measurement (pixels)')
    legend boxoff
    box off
    drawnow
  else
    set(p(1), 'xdata', 1:fstop, 'ydata', ew); %faster than replotting
    drawnow
  end

end

function plot_replay(im, frame, fig, roi, a)

  eyeim = rgb2gray(imcrop(im, roi));
  a = a.do_frame(eyeim, frame, a);

  e = a.results.ellipse;
  e.X0_in = e.X0_in + roi(2);
  e.Y0_in = e.Y0_in + roi(1);

  figure(fig)
  hold on
  imshow(im)
  draw_ellipse(e.b, e.a, e.phi, e.Y0_in, e.X0_in, 'r');
  hold off

end
