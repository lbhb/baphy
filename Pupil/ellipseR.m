function [x,y]=ellipseR(R,params)
    center(1)=params(1);
    center(2)=params(2);
    major_axis=params(3);
    minor_axis=params(3)+params(4);
    tilt=params(5);
    x = center(1) + major_axis*cos(R)*cos(tilt) - minor_axis*sin(R)*sin(tilt);
    y = center(2) + major_axis*cos(R)*sin(tilt) + minor_axis*sin(R)*cos(tilt);
    %X = [x.^2, x.*y, y.^2, x, y ];
    