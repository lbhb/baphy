function a = largest_object()
%
%a = largest_object()
%
%A simple pupil analysis procedure:
% (1) Threshold image. Threshold boundaries are by set by user.
% (2) Trace the boundaries of blobs (connected regions) in the image.
% (2) Find the blob with the largest area.
%
%Inputs (params fields)
% im: Grayscale image
% high, low: Upper and lower threshold
%
%Returns (results fields)
% blob_width: Maximum width of blob.
% ellipse_width: Major axis of ellipse fit to blob.
%
%Plot format
% left: All blobs
% center: Largest blob
% right: Largest blob and fit ellipse overplotted on image
%
%ZPS 2015-12-30

a = []; %module object
a.params = []; %parameters
a.results = []; %buffer for results from single frame

a.str = 'largest_object'; %module name - used to identify saved data
a.malloc = @malloc; %allocates memory in global results structure
a.do_frame = @do_frame; %analyzes one frame, saves results
a.plot_frame = @plot_frame; %displays analysis of frame in the gui
a.plot_sweep = @plot_sweep; %plots sample of pupil diameter data in the gui
a.plot_trace = @plot_trace; %plots trace of pupil diameter in the gui
a.plot_replay = @plot_replay; %displays analysis of one frame in replay video
a.default_params = @default_params;

end

function params = default_params
	params.low = 0;
	params.high = 30;
	params.sweeps = 1;
end

% ------------------------------------------------------------------------
% Analysis methods

function malloc(frames, a)
  %allocate memory in global results structure
  global RESULTS
  RESULTS.(a.str).blob_width = nan(frames,1);
  RESULTS.(a.str).ellipse_width = nan(frames,1);
end

function a = do_frame(im, frame, a)

  im = imcrop(im, a.params.outer_roi); %crop to roi

  %threshold
  thresh = im+1;
  thresh(im>a.params.high) = 0;
  thresh(im<a.params.low) = 0;

  %find the biggest blob in the cropped image
  [b,l] = bwboundaries(thresh); %moore-neighbor boundary tracking
  n = length(b);
  if n == 0; %error - could not find a blob
    blob = [0 0];
  else
    areas = zeros(n,1);
    for k=1:n
      areas(k) = sum(l(:)==k);
    end
    [~, idx] = max(areas);
    blob = b{idx};
  end

  %fit an ellipse to the blob
  x = blob(:,1);
  y = blob(:,2);
  ellipse = fit_ellipse(x,y);

  %handle errors in ellipse-fitting
  empty_ellipse = struct('a',[],'b',[],'phi',[],'X0',[],'Y0',[],'X0_in',[],...
    'Y0_in',[],'long_axis',0,'short_axis',0,'status','');
  if isempty(ellipse) | ellipse.long_axis > max(size(im))
    ellipse = empty_ellipse;
  end

  %find the width of the blob along its longest axis
  blob_width = max(max(y)-min(y), max(x)-min(x));

  %overwrite single-frame results structure
  a.results.blob_image = l;
  a.results.blob = blob;
  a.results.blob_width = blob_width;
  a.results.ellipse = ellipse;
  a.results.ellipse_width = ellipse.long_axis;

  %save in video-wide results structure
  global RESULTS
  RESULTS.(a.str).blob_width(frame) = blob_width;
  RESULTS.(a.str).ellipse_width(frame) = ellipse.long_axis;

end

% ------------------------------------------------------------------------
% Plot methods

function plot_frame(im, frame, ax, a)

  %pull out data to display blobs
  x = a.results.blob(:,1);
  y = a.results.blob(:,2);
  b = a.results.blob_image;
  d = num2str(round(a.results.ellipse_width));

  %correct for display on full image
  xc = x + a.params.outer_roi(2);
  yc = y + a.params.outer_roi(1);
  ec = a.results.ellipse;
  ec.X0_in = ec.X0_in + a.params.outer_roi(2);
  ec.Y0_in = ec.Y0_in + a.params.outer_roi(1);

  axes(ax.left), cla
  imshow(b)

  axes(ax.center), cla, hold on
  imshow(b)
  plot(y, x, 'b', 'linewidth', 2), axis tight
  hold off

  axes(ax.right), cla, hold on
  imshow(im)
  plot(yc, xc, 'b', 'linewidth', 2), axis tight
  draw_ellipse(ec.b, ec.a, ec.phi, ec.Y0_in, ec.X0_in, 'r');
  text(mean(yc), mean(xc), d, 'color', 'r');
  hold off

end

function plot_trace(ax, a)

  global RESULTS

  ew = RESULTS.largest_object.ellipse_width;
  fstop =  max(find(~isnan(ew)));
  ew = ew(1:fstop);
  ew(isnan(ew)) = 0;

  p = get(ax.time_ax, 'children');
  if isempty(p)
    axes(ax.time_ax)
    plot(ew, 'r')
    legend('Ellipse Width')
    xlabel('Frame')
    ylabel('Pupil Measurement (pixels)')
    legend boxoff
    box off
    drawnow
  else
    set(p(1), 'xdata', 1:fstop, 'ydata', ew); %faster than replotting
    drawnow
  end

end

function plot_sweep(ax, a)

  global RESULTS

  ew = RESULTS.largest_object.ellipse_width;

  p = get(ax.time_ax, 'children');
  if isempty(p)
    axes(ax.time_ax)
    plot(ew, 'r.', 'markersize', 20)
    xlim([1 length(RESULTS.largest_object.ellipse_width)])
    legend('Ellipse Width')
    xlabel('Frame')
    ylabel('Pupil Measurement (pixels)')
    legend boxoff
    box off
    drawnow
  else
    set(p(1), 'xdata', 1:fstop, 'ydata', ew); %faster than replotting
    drawnow
  end

end
    

function plot_replay(im, frame, fig, a)

  a = a.do_frame(im, frame, a); %analyze frame
  
  %ellipse is calculated relative to roi, correct for display on full image
  e = a.results.ellipse;
  e.X0_in = e.X0_in + a.params.outer_roi(2);
  e.Y0_in = e.Y0_in + a.params.outer_roi(1);

  figure(fig)
  hold on
  imshow(im)
  draw_ellipse(e.b, e.a, e.phi, e.Y0_in, e.X0_in, 'r');
  hold off
  
end
