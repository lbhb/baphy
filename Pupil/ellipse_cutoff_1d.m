function [x,y]=ellipse_cutoff_1d(R,params)
    ellipse_pars=params(1:5);
    cutoff=params(6);
    [x,y]=ellipse_fn(R,ellipse_pars);
    y(y<=cutoffs(1))=cutoff;
    