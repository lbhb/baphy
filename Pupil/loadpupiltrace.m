function [big_rs, strialidx, itrialbins, all_fs] = ...
    loadpupiltrace(pupilfile, options)
%[BIG_RS, STRIALIDX, ITRIALBINS, ALL_FS] = LOADPUPILTRACE(PUPILFILE, OPTIONS)
%Loads pupil measurements for processing by loadevpraster.
%
%Inputs:
% pupilfile: path to a .pup.mat file generated by pupil_remote
% options: structure, can contain the following fields
%  rasterfs: sampling rate (hz)                    (default: 1000)
%  pupil_algorithm: pupil_remote analysis module
%   hierarchy:
%   (1) module specified by user in load command
%   (2) default module saved in .pup.mat file
%  pupil_variable_name: pupil_remote measurement
%   hierarchy:
%    (1) module specified by user in load command
%    (2) default variable name saved in .pup.mat file
%    (3) 'ellipse_width' (default for old files)
%  pupil_smooth: span of moving-average filter (s) (default: 0, no filter)
%  pupil_highpass: highpass filter cutoff (hz)     (default: 0, no filter) 
%  pupil_lowpass: lowpass filter cutoff (hz)       (default: 0, no filter)
%  pupil_median: median filter order (s)           (default: 0, no filter)
%  pupil_deblink: apply blink filter               (default: 0, no filter)
%  pupil_deblink_dur: maximum duration of blink filter  (default: 1 second)
%  pupil_deblink_sd: rate of pupil size change to mark as blinks
%   (default: 6 SDs from mean)
%  pupil_mm: convert from pixels to mm             (default: 0, return pixels)
%  pupil_exclude_frames: exclude frames selected by user (default: 0, no)
%  pupil_eyespeed: return change in eye position   (default: 0, don't return)
%  verbose: verbose output, 0 = off, 1 = on        (default: 0)
%
%Returns:
% big_rs: vector of pupil diameter measurements
% strialidx: index of bin in which each trial starts
% itrialbins: bins separating each trial
%
%Created ZPS 2015-08-12
%Modifed ZPS 2016-02-18 to return intertrial bins

%parse options
rasterfs = getparm(options,'rasterfs', 1000);
pupil_offset = getparm(options, 'pupil_offset', 0.75);
pupil_smooth = getparm(options, 'pupil_smooth', 0);
pupil_highpass = getparm(options, 'pupil_highpass', 0);
pupil_lowpass = getparm(options, 'pupil_lowpass', 0);
pupil_bandpass = getparm(options, 'pupil_bandpass', []);
pupil_derivative = getparm(options, 'pupil_derivative', '');
pupil_median = getparm(options, 'pupil_median', 0);
pupil_algorithm = getparm(options, 'pupil_algorithm', '');
pupil_method = getparm(options, 'pupil_method', '');
pupil_variable_name = getparm(options, 'pupil_variable_name', '');
pupil_deblink = getparm(options, 'pupil_deblink', 0);
pupil_deblink_dur = getparm(options, 'pupil_deblink_dur', 1);
pupil_deblink_sd = getparm(options, 'pupil_deblink_sd', 6);
pupil_mm = getparm(options, 'pupil_mm', 0);
pupil_exclude_frames = getparm(options, 'pupil_exclude_frames', 0);
pupil_eyespeed = getparm(options, 'pupil_eyespeed', 0);
verbose = getparm(options, 'verbose', 0);
[pathstr,name,ext] = fileparts(pupilfile);
fprintf('%s: loading %s\n', mfilename, [name ext]);

if ~isempty(pupil_method)
    if pupil_method=='cnn'
       % if loading pupil from python
       if contains(name, '.pup')
           n = strrep(name, '.pup', '');
       else
           n = name;
       end
       data = load(fullfile(strcat(pathstr, '/sorted/', strcat(n, '.mat'))));
       % hard code for now to return the minor axis
       pupil_diameter = data.cnn.a(1:end-1)';
    end
else
    load(pupilfile) %load pupil data

    %set analysis module
    if isempty(pupil_algorithm)
      if isfield(pupil_data.params, 'default')
        pupil_algorithm = pupil_data.params.default;
      else
         error('default algorithm not specified');
      end
    end
    %set variable name
    if isempty(pupil_variable_name)
      if isfield(pupil_data.params, 'default_var')
         pupil_variable_name = pupil_data.params.default_var;
      else
         error('default_var not specified');
         %pupil_variable_name = 'ellipse_width';
      end
    end
    %load appropriate data
    fprintf('%s: using algorithm %s and variable %s\n', ...
      mfilename, pupil_algorithm, pupil_variable_name);
    if isfield(pupil_data,'results'), %new data format
       if isfield(pupil_data.results,pupil_algorithm),
          pupil_diameter = pupil_data.results.(pupil_algorithm).(pupil_variable_name);
       else
          fnames=fields(pupil_data.results);
          fprintf('%s: Algorithm %s does not exist in pupil data, using %s instead.', ...
        mfilename, pupil_algorithm, fnames{end})
          pupil_diameter = pupil_data.results.(fnames{end}).(pupil_variable_name);
       end
    else
       pupil_diameter = pupil_data.pupil_diameter; %old data format
    end
end

%return change in eye position - only works if saved in pupil_remote
if pupil_eyespeed
    if strcmp(pupil_method, 'cnn')
        eye_speed = data.cnn.eyespeed(1:end-1);
    else
        eye_speed = pupil_data.results.(pupil_algorithm).eye_speed;
    end

	%old method (now part of pupil_remote)
	
  %ellipse = pupil_data.results.(pupil_algorithm).ellipse;

  %for k=1:size(ellipse,2)
  %  if isempty(ellipse(k).X0_in)
  %    ellipse(k).X0_in = nan;
  %  end
  %  if isempty(ellipse(k).Y0_in)
  %    ellipse(k).Y0_in = nan;
  %  end
  %end

  %x = [ellipse.X0_in];    
  %y = [ellipse.Y0_in];      
  %eye_speed = sqrt( (diff(x).^2) + (diff(y).^2) );
end

% %exclude frames selected by user
% if pupil_exclude_frames
%   if strcmp(pupil_method, 'cnn')
%     excluded_frames = data.cnn.excluded_frames;
%   else
%     excluded_frames = pupil_data.params.(pupil_algorithm).excluded_frames;
%   end
%   
%   if ~isempty(excluded_frames)
%     for k=1:size(excluded_frames,1)
%       x1 = min(excluded_frames(k,:));
%       x2 = max(excluded_frames(k,:));
%       pupil_diameter(x1:x2) = nan;
%     end
%   end
% end


fs_approximate = 30; %approximate framerate of camera
if pupil_deblink > 0
  dp = abs(diff(pupil_diameter)); %take derivative of pupil diameter
  %mark bins with derivative more than 6 standard deviations from the mean
  blink = zeros(size(dp)); blink(dp > mean(dp) + pupil_deblink_sd*std(dp)) = 1;
  %apply a smoothing filter
  blink = smooth(blink, fs_approximate*pupil_deblink_dur); 
  blink = blink > 0;
  %find blink onsets and offsets
  on = zeros(size(blink)); on(diff(blink) == 1) = 1;
  off = zeros(size(blink)); off(diff(blink) == -1) = 1;
  %use linear interpolation to fill in the blinks
  onidx = find(on);
  offidx = find(off);
  deblinked = pupil_diameter;
  if pupil_eyespeed
    deblinked_eye_speed = eye_speed;
  end
  %ignore onsets without matching offsets
  if length(onidx) > length(offidx)
    onidx = onidx(1:length(offidx));
  end
  for k=1:length(onidx)
    x1 = onidx(k);
    x2 = offidx(k);
    deblinked(x1:x2) = linspace(deblinked(x1), deblinked(x2), x2-x1+1);
    if pupil_eyespeed
      deblinked_eye_speed(x1:x2) = nan;
    end
  end
  if verbose
    figure
    hold on
    if pupil_eyespeed
      subplot(2,1,1)
      hold on
    end
      plot(pupil_diameter, 'b')
      plot(deblinked, 'k')
      xlabel('Frame')
      ylabel('Pupil')
      legend('Raw', 'Deblinked')
      title(sprintf('Artifacts detected: %d', sum(on)))
      axis tight
    if pupil_eyespeed
      subplot(2,1,2)
      hold on
      plot(eye_speed, 'b')
      plot(deblinked_eye_speed, 'k')
      xlabel('Frame')
      ylabel('Eye Movement')
      legend('Raw', 'Deblinked')
      title(sprintf('Artifacts detected: %d', sum(on)))
      axis tight
    end
  end
  pupil_diameter = deblinked;
  if pupil_eyespeed
    eye_speed = deblinked_eye_speed;
  end
  fprintf('%s: deblinking trace\n', mfilename)
end


%exclude frames selected by user
if pupil_exclude_frames
  if strcmp(pupil_method, 'cnn')
    excluded_frames = data.cnn.excluded_frames;
  else
    excluded_frames = pupil_data.params.(pupil_algorithm).excluded_frames;
  end
  
  if ~isempty(excluded_frames)
    for k=1:size(excluded_frames,1)
        if excluded_frames(k,:) > 0
           x1 = min(excluded_frames(k,:));
           x2 = max(excluded_frames(k,:));
           pupil_diameter(x1:x2) = nan;
        end
    end
  end
end

if pupil_eyespeed
  pupil_diameter = eye_speed;
  fprintf('%s: returning eye speed, not pupil measurement\n', mfilename)
end

%% resample in time and correct for dropped frames using timestamps
% recorded on each trial by baphy
%

%load experiment events
mfile = strrep(pupilfile, '.pup.mat', '.m');
LoadMFile(mfile);
%find and parse pupil events
[~,~,start_note,~,~] = evtimes(exptevents, 'PUPIL,*');
trials = size(start_note,1);
timestamp = zeros(trials,6);
first_frame = zeros(trials,1);
for ii=1:trials
  n = eval(start_note{ii}(7:end));
  timestamp(ii,:) = datevec(n(1));
  first_frame(ii) = n(2);
end
%calculate frame count and duration of each trial except the last
duration = zeros(trials,1);
frame_count = zeros(trials,1);
for ii=1:trials-1
  duration(ii) = etime(timestamp(ii+1,:), timestamp(ii,:)); 
  frame_count(ii) = first_frame(ii+1) - first_frame(ii);
end
%calculate the frame count and duration of the last trial
[~,~,stop_note,~,~] = evtimes(exptevents, 'PUPILSTOP,*');
n_final = eval(stop_note{trials}(11:end));
duration(trials) = etime(datevec(n_final(1)), timestamp(trials-1,:));
frame_count(trials) = n_final(2) - first_frame(trials);
%warp to account for dropped frames
tl = zeros(trials,1);
big_rs = [];
all_fs = [];
strialidx(1) = 1;
for ii=1:trials
  d = pupil_diameter(first_frame(ii):first_frame(ii)+frame_count(ii)-1);
  fs = frame_count(ii)/duration(ii);
  all_fs(ii) = fs;
  t = ((1/fs)/2):(1/fs):duration(ii)-(1/fs)/2;
  if pupil_eyespeed
    d = d.*fs; %convert to px/s before resampling
  end
  ti = ((1/rasterfs)/2):(1/rasterfs):duration(ii)-(1/rasterfs)/2;
  di = interp1(t, d, ti, 'linear', 'extrap');
  di(di < 0) = 0; %rectify - interpolation was giving values less than 0
  tl(ii) = length(di);
  big_rs = [big_rs di];
end
tl = cumsum(tl);
strialidx = [1; tl(1:end-1)];
%add offset to account for delay in modulation of spike rate by pupil
strialidx = strialidx + floor(pupil_offset*rasterfs) + 1;
%calculate bins between each trial
stop_timestamp = zeros(trials,6);
for ii=1:trials
  n = eval(stop_note{ii}(11:end));
  stop_timestamp(ii,:) = datevec(n(1));
end
intertrial_duration = zeros(trials,1);
for ii=1:trials-1
  intertrial_duration(ii) = etime(timestamp(ii+1,:), stop_timestamp(ii,:));
end
itrialbins = round(intertrial_duration*fs);

%% smoothing/filtering

if pupil_smooth > 0
  pupil_smooth = round(pupil_smooth*rasterfs);
  big_rs = conv(big_rs, ...
    ones(1,pupil_smooth)/pupil_smooth, 'same');
  fprintf('%s: smoothing trace\n', mfilename)
end 
if pupil_median > 0
  big_rs = medfilt1(big_rs, round(pupil_median*rasterfs));
  fprintf('%s: applying median filter\n', mfilename)
  
  % remove edge artifacts
  d1=abs(diff(big_rs(1:10)));
  if d1(1)>max(d1(2:end))
     big_rs(1)=big_rs(2);
  end
end
if pupil_highpass > 0
  f1 = pupil_highpass/rasterfs*2;
  f2 = 14/rasterfs*2;
  [b,a] = ellip(4, 0.5, 20, [f1 f2]);
  valid_idx = find(isfinite(big_rs)); %ignore nans when filtering
  big_rs(valid_idx) = filtfilt(b, a, big_rs(valid_idx));
  fprintf('%s: applying highpass filter\n', mfilename)
end
if pupil_lowpass > 0
  f = pupil_lowpass/rasterfs*2;
  [b, a] = ellip(4, 0.5, 20, f);
  valid_idx = find(isfinite(big_rs)); %ignore nans when filtering
  big_rs(valid_idx) = filtfilt(b, a, big_rs(valid_idx));
  fprintf('%s: applying lowpass filter\n', mfilename)
end
%if ~isempty(pupil_bandpass)
%  pfft = fft(big_rs);
%  freqs = [linspace(0, rasterfs/2, length(pfft)/2)];
%  f.low = min(find(freqs>pupil_bandpass(1)));
%  f.high = max(find(freqs<pupil_bandpass(2)));
%  f.filt = zeros(size(pfft));
%  f.filt(f.low:f.high) = 1;
%  big_rs = abs(ifft(f.filt.*pfft));
%end
if ~isempty(pupil_bandpass)
  f1 = pupil_bandpass(1)/rasterfs*2;
  f2 = pupil_bandpass(2)/rasterfs*2;
  [b,a] = ellip(4, 0.5, 20, [f1 f2]);
  valid_idx = find(isfinite(big_rs)); %ignore nans when filtering
  big_rs(valid_idx) = filtfilt(b, a, big_rs(valid_idx));
  fprintf('%s: applying bandpass filter\n', mfilename)
end
if ~isempty(pupil_derivative)
   dbins=round(rasterfs*1);
   dfilt=[ones(1,dbins) -ones(1,dbins)]./(dbins*2);
   pupil_deriv=rconv2(big_rs,dfilt);
   if strcmpi(pupil_derivative,'pos'),
      pupil_deriv(pupil_deriv<0)=0;
   elseif strcmpi(pupil_derivative,'neg'),
      pupil_deriv(pupil_deriv>0)=0;
   end
   big_rs=pupil_deriv;
end

      
%post-processing
if pupil_mm %convert from pixels to mm
  if ~isfield(pupil_data, 'results')
    fprintf('%s: cannot convert %s to mm\n', mfilename, [name ext]);
  else
    fprintf('%s: converting %s to mm\n', mfilename, [name ext]);
    eye_px = pupil_data.results.eye_width; %eye width in pixels
    animal = globalparams.Ferret;
    sql = ['select * from gAnimal WHERE animal="' animal '"'];
    animaldata = mysql(sql);
    eye_mm = animaldata.eyeWidth*10; %eye width in millimeters
    big_rs = big_rs.*(eye_mm/eye_px);
  end
end
if verbose

  %plot pupil diameter, indicating trial start by vertical lines (for debugging)
  figure
  plot(big_rs)
  axis tight
  hold on
  plot([strialidx strialidx]',repmat(get(gca,'ylim')',1,size(strialidx,1)))
  hold off
  xlabel('Frame')
  ylabel('Pupil Diameter')

  %plot duration of each trial (for debugging)
  figure
  subplot(2,1,1)
  plot(duration)
  axis tight
  ylabel('Duration (s)')
  subplot(2,1,2)
  plot(all_fs)
  axis tight
  xlabel('Trial')
  ylabel('Sampling rate (Hz)')

end
