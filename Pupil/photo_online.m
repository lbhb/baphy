%%Generic MATLAB GUI functions
function varargout = photo_online(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @photo_online_OpeningFcn, ...
                   'gui_OutputFcn',  @photo_online_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

function photo_online_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);

function varargout = photo_online_OutputFcn(hObject, eventdata, handles) 
varargout{1} = handles.output;

%Set some globals -- Hacked from MANTA M_Defaults.m
global MG
MG=struct();
MG.resize_factor=.5;% factor by which to resize pupil video. In new MATLAB, 
%videos can only be recorded at 720x480, we resize to save disk space.
MG.Stim=struct();
MG.Stim.COMterm = 124; % '|'
MG.Stim.MSGterm = 33; % '!' 
MG.Stim.Port = 33332; %  Port to connect to 33332 rather than MANTA 33330 or PUPIL 33331
MG.recording=0;
MG.frames_written=0;
MG.format='photometry';
% hard-coding remote (baphy) host for now.


switch getenv('computername')
    case 'TATERIL'
        MG.Stim.Host = 'localhost';
    case 'MOLE'
        MG.Stim.Host = '10.147.70.6';  % weasel.ohsu.edu
    case 'LBHB-PC'  % photo host
        MG.Stim.Host = '10.147.70.6';  % weasel.ohsu.edu
end

I = ver('instrument');
if ~isempty(I)
  MG.Stim.Package = 'ICT'; % Instrument Control Toolbox
else 
  error(['Instrument Control Toolbox needs to be installed, since there is no free package that supports Callback functions at this point.']);
  MG.Stim.Package = 'jTCP'; % Java TCP by Kevin Bartlett (http://www.mathworks.com/matlabcentral/fileexchange/24524-tcpip-communications-in-matlab)
  I = which('jtcp');
  if isempty(I) 
    MG.Stim.Pacakge = 'None';
    fprintf(['WARNING : NO TCPIP SUITE FOUND!\n'...
      '\tNeither the instrument control toolbox, nor the open source tcpip suite jTCP have been detected.\n '...
      '\tPlease install either of those two, in order to connect to a controller/stimulator\n']); 
  end
end

%Camera Interface
function video_file_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), ...
        get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function preview_video_Callback(hObject, eventdata, handles)
global MG
if isfield(MG, 'cam')
    closepreview(MG.cam)
    %rmfield(handles, 'rect')
end


if 0
  %works for pupil
  disp('Assuming pupil camera')
  MG.cam = videoinput('winvideo', 1,'UYVY_720x480');
  %MG.cam = videoinput('winvideo', 1,'UYVY_360x240'); %doesn't work
else
  %works for photometry system
  disp('Assuming photometry camera')
  MG.cam = videoinput('pointgrey', 1,'F7_Mono16_480x300_Mode5');
  %MG.cam = videoinput('winvideo', 1,'UYVY_640x480');
  %MG.cam = videoinput('winvideo', 1); % old way (uses deafult which is 'UYVY_360x240')
end

MG.cam.ReturnedColorSpace = 'RGB';
res = MG.cam.VideoResolution;
nbands = MG.cam.NumberOfBands;
if MG.resize_factor~=1 && 0
    %Test of resizing. Actual resizing happens in pupil_start_recording.m
    %DOESN"T ACTUALLY DO WHAT WE WANT!
    % only takes one snapshot while preview (below) continuously streams the camera
    % to the figure window
    res_resized=res*MG.resize_factor;
    himage = image(zeros(res_resized(2), res_resized(1), nbands), 'Parent', handles.preview_ax);
    MG.im = getsnapshot(MG.cam);
    MG.im_res = imresize(MG.im,MG.resize_factor);
    set(himage,'CData',MG.im_res)
else
    himage = image(zeros(res(2), res(1), nbands), 'Parent', handles.preview_ax);
    preview(MG.cam, himage);
end
guidata(hObject, handles)

function set_roi_Callback(hObject, eventdata, handles)
global MG
if isfield(MG, 'cam')
    MG.im = getsnapshot(MG.cam);
else
    disp('Pupil_Online: Error - No active camera.')
end
figure
imshow(MG.im)
r = imrect;
MG.roi = getPosition(r);
close
axes(handles.preview_ax)
if isfield(handles, 'rect')
    set(handles.rect, 'Position', MG.roi);
else
    handles.rect = rectangle('Position', MG.roi, ...
                             'Linewidth', 1, ...
                             'EdgeColor', 'b');
end
guidata(hObject, handles)

%Communication with Baphy
function connect_Callback(hObject, eventdata, handles)
global MG
MG.hObject = hObject;
MG.handles = handles;
State = get(hObject,'Value');
set(handles.status_msg, 'String', ['Connect: ',num2str(State)]);
if State M_startTCPIP; else M_stopTCPIP; end


function preview_histogram_Callback(hObject, eventdata, handles)
global MG
im = getsnapshot(MG.cam);
eye_im = imcrop(rgb2gray(im), MG.roi);
figure
hist(double(eye_im(:)), 0:255);
axis tight
xlabel('Image Intensity')
ylabel('Number of Pixels')

%Preview pupil measurement
% function preview_pupil_Callback(hObject, eventdata, handles)
% global MG
% im = getsnapshot(MG.cam);
% eye_im = imcrop(rgb2gray(im), MG.roi);
% if ~isfield(MG, 'high')
% MG.high = str2num(get(handles.set_high, 'string'));
% end
% if ~isfield(MG, 'low')
% MG.low = str2num(get(handles.set_low, 'string'));
% end
% [~, ~, ~, e] = measure_pupil(eye_im, MG.high, MG.low, 0, 0, 'largest_object');
% if isempty(e.a)
%     error('Could not find pupil diameter')
% else
%     handles.pupil_fig = figure('name', 'Pupil Diameter Preview');
%     imshow(im)
%     ec = e;
%     ec.X0_in = e.X0_in + MG.roi(2);
%     ec.Y0_in = e.Y0_in + MG.roi(1);
%     draw_ellipse(ec.b, ec.a, ec.phi, ec.Y0_in, ec.X0_in, 'r');
%     %d = max([ec.b ec.a]);
%     %fprintf('Pupil diameter: %0.2f\n', d)
%     text(ec.Y0_in, ec.X0_in, num2str(round(ec.long_axis)), 'color', 'r')
% end

function set_high_Callback(hObject, eventdata, handles)
global MG
MG.high = str2num(get(handles.set_high, 'string'));

function set_high_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), ...
                   get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end

function set_low_Callback(hObject, eventdata, handles)
global MG
MG.low = str2num(get(handles.set_low, 'string'));

function set_low_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), ...
                   get(0,'defaultUicontrolBackgroundColor'))
  set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in buttonCancel.
function buttonCancel_Callback(hObject, eventdata, handles)
% hObject    handle to buttonCancel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global MG
MG.recording=0;
