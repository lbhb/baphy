# This script generates pubVectmrs for specified pubids OR authorVector for specified pid OR
# runs as a daemon updating new pubids and modified pids

import pandas as pd
from sqlalchemy import create_engine
import numpy as np
import argparse
import sys
import time



if __name__ == '__main__':
    #parser = argparse.ArgumentParser(description='Generetes the topic vector and block of an author')
    #parser.add_argument('action', metavar='ACTION', type=str, nargs=1, help='action')
    #parser.add_argument('updatecount', metavar='COUNT', type=int, nargs=1, help='pubid count')
    #parser.add_argument('offset', metavar='OFFSET', type=int, nargs=1, help='pubid offset')
    #args = parser.parse_args()
    #action=parser.action[0]
    #updatecount=parser.updatecount[0]
    #offset=parser.offset[0]
    
    siteid="eno052b"
    
    if len(sys.argv)>1:
        siteid=sys.argv[1]

    # connect to database
    engine_mysql = create_engine(r'mysql+pymysql://david:nine1997@hyrax.ohsu.edu:3306/cell')

    sql='SELECT *,id as rawid FROM gDataRaw WHERE cellid="{0}"'.format(siteid)

    site_data = pd.read_sql(sql, engine_mysql)

    for ii,rawid in enumerate(site_data.rawid):
        print('Rawid={0} parmfile={1}'.format(rawid,site_data.parmfile[ii]))
        
    engine_mysql.dispose()
