% function [SPKCLASS,newunitmean,newunitstd]=...
%     clustercore(cluster_algorithm,tolerance,sweepout);
function [status,SPKCLASS,newunitmean,newunitstd]=...
    clustercore(cluster_algorithm,tolerance,sweepout);

global SPKRAW UNITMEAN UNITSTD UNITCOUNT XAXIS PCC PCS SPIKESET
global UPROJ SPKCLASS KCOL C0 EXTRAS
status=1;
if ~exist('cluster_algorithm','var'),
    cluster_algorithm='kmeans';
end
if ~exist('sweepout','var'),
    sweepout=0;
end

if sum(abs(C0(:)))==0,
    C0=mean(UPROJ);
end
k=size(C0,1);
if length(tolerance)<k,
    tolerance=[tolerance(:);...
        repmat(tolerance(1),[k-length(tolerance) 1])]
end

switch lower(cluster_algorithm),
    
    case 'kmeans',
        
        if k>1,
            [SPKCLASS,c] = kmeans(UPROJ, k,'Start',C0,'EmptyAction','singleton');
        else
            SPKCLASS=ones(length(UPROJ),1);
            c=mean(UPROJ,1);
            C0=c;
        end
        
        cs=zeros(size(c));
        normfactor=EXTRAS.sigma;
        
        for jj=1:k,
            spmatch=find(SPKCLASS==jj);
            cs(jj,:)=std(UPROJ(spmatch,:));
            
            tt=length(spmatch);
            newunitmean=mean(SPIKESET(:,spmatch),2);
            newunitstd=std(SPIKESET(:,spmatch),0,2).^2;
            %normfactor=std(newunitmean).^2);
            if sweepout,
                dd=sqrt(mean((SPIKESET(:,spmatch)-repmat(newunitmean,[1 tt])).^2))./...
                    normfactor;
                dda=sqrt(mean(SPIKESET(:,spmatch).^2))./sqrt(mean(newunitmean.^2));
                nu=newunitmean./norm(newunitmean);
                ddy=nu'*SPIKESET(:,spmatch)./norm(newunitmean);
                ddx=sqrt(dda.^2-ddy.^2);
                
                %ddt=sort(ddy(dd<tolerance(jj)));
                %mm=mean(ddt(1:ceil(length(ddt)./20)))
                mm=min(ddy(dd<tolerance(jj)));
                %mm=1-2.*std(ddy(dd<tolerance(jj)))
                reqd=mm+2./(4.*(1-mm)).*ddx.^2;
                ll=find(ddy<=reqd);
                
                %kk=find(ddy>reqd);
                %sfigure(24);
                %u=UPROJ(spmatch,:);
                %(u(ll,1),u(ll,2),'r.');
                %hold on
                %plot(u(kk,1),u(kk,2),'.');
                %hold off
                
                SPKCLASS(spmatch(ll))=k+1;
            else
                dd=sqrt(mean((SPIKESET(:,spmatch)-repmat(newunitmean,[1 tt])).^2))./...
                    normfactor;
                SPKCLASS(spmatch(dd>tolerance(jj)))=k+1;
            end
        end
        
    case 'distance',
        centerspikes=PCS*C0';
        
        spikecount=size(SPIKESET,2);
        dd=zeros(spikecount,k);
        
        %normfactor=mean(std(UPROJ));
        normfactor=EXTRAS.sigma;
        for jj=1:k,
            %dd(:,jj)=sqrt(mean((UPROJ-repmat(C0(jj,:),[spikecount 1])).^2,2));
            dd(:,jj)=sqrt(mean((SPIKESET-repmat(centerspikes(:,jj),...
                [1 spikecount])).^2));
            dd(:,jj)=dd(:,jj)./normfactor./tolerance(jj);
        end
        
        [y,xi]=sort(dd,2);
        SPKCLASS=xi(:,1);
        SPKCLASS(y(:,1)>1)=k+1;
        
        %disp('updating UNITMEAN to center of cluster!');
        %for jj=1:k,
        %   spmatch=find(SPKCLASS==jj);
        %   UNITMEAN(:,jj)=mean(SPIKESET(:,spmatch),2);
        %   UNITSTD(:,jj)=std(SPIKESET(:,spmatch),0,2);
        %end
        
    case 'catamaran'
        %write to .ntt file for Catamaran
        global LOCAL_DATA_ROOT  FILEDATA EVENTTIMES CATAMARAN_PARAMS
        if(isempty(CATAMARAN_PARAMS))
            CATAMARAN_PARAMS=struct;
        end
        if(sign(EXTRAS.sigthreshold)==-1)
            inv=0;
        else
            inv=1;
        end
        %EVENTTIMES is in us
        for i=1:length(FILEDATA)
            [bb{i},~]=basename(FILEDATA(i).parmfile);
        end
        para1=[LOCAL_DATA_ROOT [bb{:}] '.ntt'];
        para2=0;    %don't append to existing file
        para3=1;    %extract/write the entire file
        para4=1;    %extraction mode array
        para5=1;%length(Timestamps);   %number of records to write
        %field selection array
        FieldSelection(1) = 1;
        FieldSelection(2) = 0;
        FieldSelection(3) = 0;
        FieldSelection(4) = 0;
        FieldSelection(5) = 1;
        FieldSelection(6) = 1;
        para6=FieldSelection;
        para7=EVENTTIMES(1)/EXTRAS.rate*10^6; % convert to us
        tmp=reshape(SPIKESET(:,:),size(SPIKESET,1)/4,4,size(SPIKESET,2));   %the array of waveforms on each channel
        tmp=tmp*((2^16)/2)/500e-6;
        if(inv)
            tmp=-1*tmp;
        end
        %event triggers (derivative thresholded) occur at sample 10
        switch EXTRAS.rate
            case 20833
                %LB2
                if(inv)
                    tmp=tmp(1:32,:,:);
                    keep_range=[0.2e-3 1.2e-3];
                    shift_range=10/EXTRAS.rate+[0 EXTRAS.detect_options.shadow];
                else
                   tmp=tmp(1:32,:,:);
                    keep_range=[0e-3 1e-3];
                    shift_range=5/EXTRAS.rate+[0 EXTRAS.detect_options.shadow]; 
                end
            case 25000
                %LB1
                if(inv)
                    tmp=tmp(1:32,:,:);
                    keep_range=[0.2e-3 1.2e-3];
                    shift_range=10/EXTRAS.rate+[0 EXTRAS.detect_options.shadow];%theshold is at sample 10
                else
                    tmp=tmp(1:32,:,:);
                    keep_range=[0e-3 1e-3];
                    shift_range=5/EXTRAS.rate+[0 EXTRAS.detect_options.shadow];%theshold is at sample 10
                end
            case 30000
                %Open E-phys
                tmp=tmp(1:32,:,:);
                keep_range=[0.2e-3 1.2e-3];
                shift_range=10/EXTRAS.rate+[0 EXTRAS.detect_options.shadow];
            otherwise
                error('Decide what portion of the waveform to use for this samplign rate')
        end
        para8=tmp(:,:,1);
        %para8=DataPoints;   %the array of waveforms on each channel
        %para9=ascii(header)
        HeaderOut{1} = ['######## Neuralynx'];     %this is REQUIRED as header prefix
        HeaderOut{2} = ['FileExport Mat2NlxTT_SE-urut unix-vers'];
        HeaderOut{3} = [' ' FILEDATA.parmfile];
        if(exist(para1,'file'))
            fprintf([para1,' exists, deleting\n'])
            delete(para1)
        end
        UTmkdir(para1);
        Mat2NlxTT(para1,para2,para3,para4,para5,para6,para7,para8,HeaderOut');
        
        %now run this a bunch of times
        FieldSelection(6) = 0;
        para6=FieldSelection;
        
        for nn=2:length(EVENTTIMES)
            para7=EVENTTIMES(nn)/EXTRAS.rate*10^6;
            para8=tmp(:,:,nn);
            Mat2NlxTT(para1,para2,para3,para4,para5,para6,para7,para8);
        end
        return_to_meska=true;
        [idxs,sort_params]=catamaran(para1,return_to_meska,EXTRAS.rate,keep_range,shift_range,CATAMARAN_PARAMS);
        CATAMARAN_PARAMS=sort_params;
        CATAMARAN_PARAMS.keep_range=keep_range;
        CATAMARAN_PARAMS.shift_range=shift_range;
        CATAMARAN_PARAMS.inv=inv;
        
        if(isempty(idxs))
            status=0;
            return
        end
        UNITCOUNT=max(idxs);
         CATAMARAN_PARAMS.max_entanglement=nan(1,UNITCOUNT);
        for i=1:UNITCOUNT
            CATAMARAN_PARAMS.max_entanglement(i)=max([CATAMARAN_PARAMS.cluster_entanglements(i,:) CATAMARAN_PARAMS.cluster_entanglements(:,i)']);
        end
        EXTRAS.catamaran_params=CATAMARAN_PARAMS;
        idxs(idxs<0)=max(idxs)+1;
        SPKCLASS=idxs;
        
        for i=1:UNITCOUNT
            UNITMEAN(:,i)=mean(SPIKESET(:,SPKCLASS==i),2);
            UNITSTD(:,i)=std(SPIKESET(:,SPKCLASS==i),[],2);
        end
        
    otherwise
        error('algorithm not supported');
        
end

EXTRAS.algorithm=cluster_algorithm;
