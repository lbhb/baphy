function sortidxs=savespikes_do_save(savfile,s,chanNum,extras,baphy_fmt,rate,nrec,fname,nsweep,npoint,stonset,stdur,delay,sort_review_)

%unitSpikes lists trial number, spike time for each spike
%Template is the mean spike waveform
%env is 1:a vector of 1 to length of template, 2: template +3 stds. 3: template -3 stds
if ~exist('sort_review_') sort_review_ = []; end

if exist(savfile,'file') && baphy_fmt==7
    %Only use the first index
    fprintf('Loading existing file\n')
    load(savfile);
    sortidxs(1:length(chanNum))=1;
    for ci=1:length(chanNum)
        for ui = 1:length(s{ci})
            if isempty(s{ci}(ui).sortparameters)
                continue
            end
            if length(sortinfo)>=chanNum(ci) && ~isempty(sortinfo{chanNum(ci)}) && length(sortinfo{chanNum(ci)}{1})>=ui && ~isempty(sortinfo{chanNum(ci)}{1}(ui).sortparameters)
                error(['Trying to overwrite channel',num2str(chanNum(ci)),', unit ',num2str(ui),'. Database messed up?'])
            end
            sortinfo{chanNum(ci)}{1}(ui) = s{ci}(ui);
            sortextras{chanNum(ci)}=extras;
        end
    end
    
elseif exist(savfile,'file') && baphy_fmt==6
    %Old format with multiple sortidxs
    fprintf('Loading existing file\n')
    loadstmt = (['load ' savfile ' sortinfo sortextras sortextras_nonprimary;']);
    warning('off','MATLAB:load:variableNotFound')
    eval(loadstmt);
    warning('on','MATLAB:load:variableNotFound')
    if length(sortinfo)<extras.numChannels,
        sortinfo{extras.numChannels}=[];
    end
    do_all='';
    for ci=1:length(chanNum)
        if length(sortinfo)>=chanNum(ci) && ~isempty(sortinfo{chanNum(ci)})
            un_file=find(arrayfun(@(x)~isempty(x.unitSpikes),sortinfo{chanNum(ci)}{1}),1);% Unit number to use for finding sort name in already-saved file
            un_data=find(arrayfun(@(x)~isempty(x.unitSpikes),s{ci}),1);% Unit number to use for finding sort name in data about to be saved
            if s{ci}(un_data).primary && sortinfo{chanNum(ci)}{1}(un_file).primary
                if s{1}(un_data).sortparameters.SaveSorter == 0
                    if(~strcmp(do_all,'Yes'))
                        ButtonName=questdlg([sortinfo{chanNum(ci)}{1}(un_file).sorter ' is saved as primary sorter, do you still want to be the primary sorter?'], ...
                            'Primary Sorter?',  'Yes','No', 'Yes');
                        if(length(chanNum)>1)
                            do_all=questdlg(['Do this for all channels?'], ...
                            'All?',  'Yes','No', 'Yes');
                        else
                            do_all='Yes';
                        end
                    end
                    switch ButtonName,
                        case 'Yes',
                            sortidx_old=length(sortinfo{chanNum(ci)})+1;
                            sortinfo{chanNum(ci)}{sortidx_old}=sortinfo{chanNum(ci)}{1};
                            sortextras_nonprimary{chanNum(ci)}{sortidx_old}=sortextras{chanNum(ci)};
                            sortidxs(ci)=1;
                        case 'No',
                            sortidxs(ci)=length(sortinfo{chanNum(ci)})+1;
                    end
                else
                    sortidx_old=length(sortinfo{chanNum(ci)})+1;
                    sortinfo{chanNum(ci)}{sortidx_old} =sortinfo{chanNum(ci)}{1};
                    sortextras_nonprimary{chanNum(ci)}{sortidx_old}=sortextras{chanNum(ci)};
                    sortidxs(ci)=1;
                end
            elseif ~sortinfo{chanNum(ci)}{1}(1).primary
                sortidx_old=length(sortinfo{chanNum(ci)})+1;
                sortinfo{chanNum(ci)}{sortidx_old} =sortinfo{chanNum(ci)}{1};
                sortextras_nonprimary{chanNum(ci)}{sortidx_old}=sortextras{chanNum(ci)};
                sortidxs(ci)=1;
            else
                sortidxs(ci)=length(sortinfo{chanNum(ci)})+1;
            end
        else
            sortidxs(ci)=1;
        end
        sortinfo{chanNum(ci)}{sortidxs(ci)} = s{ci};
        if(sortidxs(ci)==1)
            sortextras{chanNum(ci)}=extras;
        else
            sortextras_nonprimary{chanNum(ci)}{sortidxs(ci)}=extras;
        end
    end
else
    sortidxs(1:length(chanNum))=1;
    for ci=1:length(chanNum)
        sortinfo{chanNum(ci)}{1} = s{ci};
        sortextras{chanNum(ci)}=extras;
    end
end

% save(savfile,'unitSpikes','paramdata','fname','rate','nrec','nsweep',...
%     'npoint','ngensweep','Ncl','Template','env','xaxis')
% Channel{chanNum}= sortinfo;
if isfield(extras,'expData'),
    extras=rmfield(extras,'expData');
end
if isfield(extras,'torcList'),
    extras=rmfield(extras,'torcList');
end
if ~exist('sortextras','var'),
    sortextras={};
end

pp=fileparts(savfile);
if ~exist(pp,'dir'),
    mkdir(pp);
    [w,s]=unix(['chmod a+rwx',pp]);
end

if baphy_fmt==7
    savelist={'rate','nrec', 'sortinfo', 'baphy_fmt','sortextras'};
    save(savfile,savelist{:});
elseif baphy_fmt==6
    savelist={'rate','nrec', 'sortinfo', 'baphy_fmt','sortextras'};
    if(exist('sortextras_nonprimary','var'))
        savelist{end+1}='sortextras_nonprimary';
    end
    save(savfile,savelist{:});
elseif baphy_fmt,

    StimTagNames=extras.StimTagNames;
    trialstartidx=extras.trialstartidx;
    tolerance=extras.tolerance;
    sortextras{chanNum}=extras;
    exptevents=extras.exptevents;
    savlist={'fname','rate','nrec','nsweep','npoint', 'sortinfo', ...
        'stonset','stdur','delay','baphy_fmt',...
        'exptevents','StimTagNames','trialstartidx','tolerance','sortextras'};
    if ~isempty(sort_review_)
        sort_review{chanNum}=sort_review_;
        savlist{end+1}='sort_review';
    end
    if(exist('sortextras_nonprimary','var'))
        savlist{end+1}='sortextras_nonprimary';
    end
    save(savfile,savlist{:});
else
    sortextras{chanNum}=extras;
    save(savfile,'fname','rate','nrec','nsweep','npoint', 'sortinfo', ...
        'stonset','stdur','delay','baphy_fmt','sortextras');
end
[w,s]=unix(['chmod a+rw ',savfile]);
[w,s]=unix(['chmod a+rw ',fileparts(savfile)]);

